# Smart Contract Binary Interface Client Library

This repository contains a number of modules, which have the following dependency hierarchy


```
            Abi-model
            /       \
    Abi-codegen    Abi-client
         |              |
      Abi-cli    Contract-standard
         |
    Maven-plugin
```

### Abi Model

This module contains the type definitions of smart contract abi as defined
[here](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#abi-binary-format).
It also provides functionality for parsing and writing to binary files or json.

Eg. to parse a binary abi file:

```
FileAbi abi = new AbiParser(abiBytes).parseAbi();
```

### Abi Client

Provides functionality for building and reading state and rpc for generic ABIs.
An example of how to use the abi client to read contract state and build RPC can be seen in our
[example client](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-tools-overview.html#example-client).

### Abi Codegen

Able to generate code for java and typescript for a specific ABI. The generated code is able to parse state and generate
rpc using proper generated data structures and types.
This can be invoked through the `cargo partisia-contract abi codegen` as seen
[here](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-tools-overview.html#the-abi-codegen-tool-abi-codegen).

### Abi Cli

A cli that can call codegen, print information about an ABI or convert ABIs to and from json.
This cli can be invoked through the `cargo partisia-contract abi` command as seen
[here](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-tools-overview.html#the-abi-tool-abi).

### Abi Generation Maven Plugin

A maven plugin able to call codegen as part of maven lifecycles.
Also contains an annotation processor able to generate java contracts.
See [this](maven-plugin/README.md) for how to use it.

### Abi gen test

A testing module used to test the generated code. It generates the code automatically from abi files using the abi
generation maven plugin.
