package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.RustSyntaxPrettyPrinter;
import com.partisiablockchain.language.abimodel.parser.TypeSpecStringifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class DocBuilderTest {

  private final ArgumentAbi arg1 =
      new ArgumentAbi("arg1", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));
  private final ArgumentAbi arg2 =
      new ArgumentAbi("arg2", new SimpleTypeSpec(TypeSpec.TypeIndex.u32));

  private StringBuilder builder;
  private RustSyntaxPrettyPrinter.DocBuilder docBuilder;

  @BeforeEach
  void setUp() {
    builder = new StringBuilder();
    var stringifier = new TypeSpecStringifier(List.of());
    docBuilder = new RustSyntaxPrettyPrinter.DocBuilder("- ", builder, stringifier);
  }

  @Test
  void line() {
    docBuilder.appendLine("");

    assertContents("- \n");
  }

  @Test
  void section() {
    docBuilder.appendParagraph(List.of("A"));
    assertContents("- A\n- \n");
  }

  @Test
  void emptyParamList() {
    docBuilder.appendGenericParamList("Header", List.of(), Map.of());
    assertContents("");
  }

  @Test
  void emptyParamDesc() {
    docBuilder.appendGenericParamList("Header", List.of(arg1), Map.of());
    assertContents("");
  }

  @Test
  void mismatchParamDesc() {
    docBuilder.appendGenericParamList("Header", List.of(arg1), Map.of("invalid_arg", "ignored"));
    assertContents("");
  }

  @Test
  void nullEmptyParamDesc() {
    var descriptions = new HashMap<String, String>();
    descriptions.put("arg2", "not ignored");

    docBuilder.appendGenericParamList("Header", List.of(arg1, arg2), descriptions);

    assertContents("- Header\n" + "- \n" + "- * `arg2`: [`u32`], not ignored\n" + "- \n");
  }

  @Test
  void nonEmptyParamDesc() {
    docBuilder.appendGenericParamList(
        "Header", List.of(arg1), Map.of("arg1", "the first argument"));

    assertContents("- Header\n" + "- \n" + "- * `arg1`: [`u64`], the first argument\n" + "- \n");
  }

  @Test
  void returns() {
    docBuilder.appendReturns("a line of text");

    assertContents("- # Returns:\n" + "- \n" + "- a line of text\n" + "- \n");
  }

  private void assertContents(String expected) {
    assertThat(docBuilder.build()).isEqualTo(expected);
  }
}
