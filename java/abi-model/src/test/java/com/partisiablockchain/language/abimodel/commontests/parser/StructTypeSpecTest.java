package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

final class StructTypeSpecTest {

  private final FieldAbi field = new FieldAbi("field0", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
  private final StructTypeSpec struct = new StructTypeSpec("name", List.of(field));
  private final NamedTypeRef structType = new NamedTypeRef(0);

  @Test
  void values() {
    assertThat(struct.name()).isEqualTo("name");
    assertThat(struct.fields()).hasSize(1);
    assertThat(structType.typeIndex()).isEqualTo(TypeSpec.TypeIndex.Named);
    assertThat(struct.field(0)).isSameAs(field);
  }

  @Test
  void structTypeSpecValues() {
    StructTypeSpec structOne = new StructTypeSpec("structOne", List.of(field));
    StructTypeSpec structTwo = new StructTypeSpec("structTwo", List.of());
    List<NamedTypeSpec> structAbiList = List.of(structOne, structTwo);
    NamedTypeRef structTypeOne = new NamedTypeRef(0);
    NamedTypeRef structTypeTwo = new NamedTypeRef(1);
    ContractAbi contract =
        new ContractAbi(
            BinderType.Public, List.of(), structAbiList, List.of(), List.of(), structTypeOne);

    assertThat(contract.getNamedType(structTypeOne)).isEqualTo(structOne);
    assertThat(structTypeOne.index()).isEqualTo(0);
    assertThat(contract.getNamedType(structTypeTwo)).isEqualTo(structTwo);
    assertThat(structTypeTwo.index()).isEqualTo(1);
  }

  @Test
  void testMutualRecursion() {
    NamedTypeRef typeOne = new NamedTypeRef(0);
    NamedTypeRef typeTwo = new NamedTypeRef(1);
    StructTypeSpec structOne = new StructTypeSpec("type2", List.of(new FieldAbi("arg1", typeTwo)));
    StructTypeSpec structTwo = new StructTypeSpec("type1", List.of(new FieldAbi("arg2", typeOne)));

    assertThat(structTwo.field(0).type()).isEqualTo(typeOne);
    assertThat(structOne.field(0).type()).isEqualTo(typeTwo);
  }

  @Test
  public void testSelfRecursion() {
    NamedTypeRef selfRecursiveTypeSpec = new NamedTypeRef(0);
    StructTypeSpec struct =
        new StructTypeSpec(
            "SelfRecursiveStruct", List.of(new FieldAbi("argument", selfRecursiveTypeSpec)));
    assertThat(struct.field(0).type()).isEqualTo(selfRecursiveTypeSpec);
  }
}
