package com.partisiablockchain.language.abimodel;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

final class ExampleAbis {

  record TestCase(Path filePath) {

    TestCase {
      requireNonNull(filePath);
    }

    public String filename() {
      return filePath.getFileName().toString();
    }

    public String contractName() {
      final String filename = filename();
      return filename.replaceAll("-SDK-\\d+\\.\\d+\\.\\d+\\.abi$", "");
    }

    public static TestCase of(final Path filePath) {
      return new TestCase(filePath);
    }
  }

  private static final List<TestCase> TEST_CASES;

  static {
    final List<TestCase> tmpTestCases = referenceAbiFiles().map(TestCase::of).toList();
    TEST_CASES = List.copyOf(tmpTestCases);
  }

  public static Stream<Path> referenceAbiFiles() {
    final Path testCaseFolder =
        TranslateToReferenceTest.getResourceSourcePath(
            TranslateToReferenceTest.class, "reference-tests/abi-files");
    final File directory = testCaseFolder.toFile();
    return Arrays.stream(directory.listFiles())
        .map(File::getAbsolutePath)
        .filter(filename -> filename.endsWith(".abi"))
        .map(Path::of);
  }

  public static List<TestCase> abis() {
    return TEST_CASES;
  }
}
