package com.partisiablockchain.language.abimodel.commontests.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BinderAbiTest {

  /**
   * Calling getNamedType with a {@link NamedTypeRef} returns the named type with the index of the
   * type ref.
   */
  @Test
  void getNamedType() {
    var binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(),
            List.of(new StructTypeSpec("Struct", List.of())),
            List.of(),
            new NamedTypeRef(0));
    assertThat(binderAbi.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("Struct");
  }

  /**
   * Calling getNamedType with a String, returns the named type with the given name, or null if none
   * exists.
   */
  @Test
  void getNamedTypeByName() {
    var binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(),
            List.of(new StructTypeSpec("Struct", List.of())),
            List.of(),
            new NamedTypeRef(0));
    assertThat(requireNonNull(binderAbi.getNamedType("Struct")).name()).isEqualTo("Struct");
    assertThat(binderAbi.getNamedType("OtherStruct")).isNull();
  }

  /**
   * Calling getStateStruct when the state is a NamedTypeRef returns the struct which the type ref
   * points to.
   */
  @Test
  void getStateStruct() {
    var binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(),
            List.of(new StructTypeSpec("Struct", List.of())),
            List.of(),
            new NamedTypeRef(0));
    assertThat(requireNonNull(binderAbi.getStateStruct()).name()).isEqualTo("Struct");
  }

  /** If the state type is not a NamedTypeRef, then calling getStateStruct returns null. */
  @Test
  void getStateNull() {
    var binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(),
            List.of(new StructTypeSpec("Struct", List.of())),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    assertThat(binderAbi.getStateStruct()).isNull();
  }
}
