package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abimodel.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abimodel.commontests.TestingHelper.concatBytes;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.commontests.FileAbiBuilder;
import com.partisiablockchain.language.abimodel.commontests.FnAbiBuilder;
import com.partisiablockchain.language.abimodel.commontests.ParserHelper;
import com.partisiablockchain.language.abimodel.commontests.TestingHelper;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParseException;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import com.partisiablockchain.language.abimodel.parser.UnsupportedVersionException;
import org.junit.jupiter.api.Test;

/** Testing-class for testing {@link AbiParser}. */
@SuppressWarnings("PMD.TooManyStaticImports")
public final class AbiParserTest {

  @Test
  void illegalHeaderBytes() {
    // Header bytes encoding "PBCPBC" rather than "PBCABI"
    var parser = new AbiParser(bytesFromHex("50424350424301000001000001"));
    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Malformed header bytes, expecting PBCABI but was PBCPBC");
  }

  /** Abi parser crashes if it encounters an unknown chain component identifier. */
  @Test
  void unknownChainComponent() {
    var parser = new AbiParser(bytesFromHex("504243414249010000060000ff"));
    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Unknown chain component identifier: 0xff");
  }

  @Test
  void parseHeadersOnly() {
    var headerString = "PBCABI";
    var simpleVersionClient = new AbiVersion(1, 0, 0);
    var simpleVersionBinder = new AbiVersion(1, 0, 0);

    // Header bytes, binder version 1.0.0, client version 1.0.0, shortname length 4
    var parser = new AbiParser(bytesFromHex("50424341424901000001000004"));
    var header = parser.parseHeader();
    assertThat(header.header()).isEqualTo(headerString);
    assertThat(header.versionClient()).isEqualTo(simpleVersionClient);
    assertThat(header.versionBinder()).isEqualTo(simpleVersionBinder);
  }

  @Test
  void contractBoolFromFile() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    var parser = TestingHelper.loadAbiParserFromFile("contract_booleans.abi");
    var result = parser.parseAbi();

    var contract = (ContractAbi) result.chainComponent();

    assertThat(result.format().shortnameLength()).isEqualTo(4);
    var firstStruct = contract.namedTypes().get(0);
    assertThat(firstStruct.name()).isEqualTo("ExampleContractState");
    ParserHelper.assertFieldType(
        firstStruct, 0, "my_bool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));

    var init = requireNonNull(contract.init());
    assertThat(init.name()).isEqualTo("initialize");
    ParserHelper.assertActionType(init, 0, "my_bool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));

    var action = requireNonNull(contract.getFunctionByName("update_my_bool"));
    assertThat(action.name()).isEqualTo("update_my_bool");
    ParserHelper.assertActionType(action, 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));

    var stateStruct = (NamedTypeRef) contract.stateType();
    assertThat(contract.getNamedType(stateStruct)).isEqualTo(firstStruct);
  }

  @Test
  void stringsContractTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-strings
    var parser = TestingHelper.loadAbiParserFromFile("contract_strings.abi");

    var headerString = "PBCABI";

    var result = parser.parseAbi();
    var contractAbi = (ContractAbi) result.chainComponent();
    var structTypes = contractAbi.namedTypes();

    assertThat(result.header()).isEqualTo(headerString);
    assertThat(structTypes.size()).isEqualTo(1);

    var struct = (StructTypeSpec) structTypes.get(0);
    assertThat(struct.name()).isEqualTo("ExampleContractState");
    assertThat(struct.fields()).hasSize(1);
    ParserHelper.assertFieldType(
        struct, 0, "my_string", new SimpleTypeSpec(TypeSpec.TypeIndex.String));

    var init = requireNonNull(contractAbi.init());
    assertThat(init.name()).isEqualTo("initialize");
    ParserHelper.assertActionType(
        init, 0, "my_string", new SimpleTypeSpec(TypeSpec.TypeIndex.String));

    var shortname = init.shortname();
    assertThat(shortname).isEqualTo(new byte[4]);

    // assertThat(contractAbi.getFunctionByShortname(new byte[42])).isNull();

    var action = requireNonNull(contractAbi.getFunctionByName("update_my_string"));
    assertThat(action.name()).isEqualTo("update_my_string");
    ParserHelper.assertActionType(
        action, 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.String));

    var stateStruct = (NamedTypeRef) contractAbi.stateType();
    assertThat(contractAbi.getNamedType(stateStruct)).isSameAs(structTypes.get(0));
  }

  @Test
  void contractNumbersTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-numbers
    var parser = TestingHelper.loadAbiParserFromFile("contract_numbers.abi");
    var result = parser.parseAbi();

    var contractAbi = (ContractAbi) result.chainComponent();
    assertThat(result.format().shortnameLength()).isEqualTo(0x04);

    var structTypes = contractAbi.namedTypes();
    assertThat(structTypes).hasSize(1);

    var struct = (StructTypeSpec) structTypes.get(0);
    assertThat(struct.name()).isEqualTo("ExampleContractState");

    assertThat(struct.fields()).hasSize(8);
    ParserHelper.assertFieldType(struct, 0, "my_u8", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    ParserHelper.assertFieldType(struct, 1, "my_u16", new SimpleTypeSpec(TypeSpec.TypeIndex.u16));
    ParserHelper.assertFieldType(struct, 2, "my_u32", new SimpleTypeSpec(TypeSpec.TypeIndex.u32));
    ParserHelper.assertFieldType(struct, 3, "my_u64", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));
    ParserHelper.assertFieldType(struct, 4, "my_i8", new SimpleTypeSpec(TypeSpec.TypeIndex.i8));
    ParserHelper.assertFieldType(struct, 5, "my_i16", new SimpleTypeSpec(TypeSpec.TypeIndex.i16));
    ParserHelper.assertFieldType(struct, 6, "my_i32", new SimpleTypeSpec(TypeSpec.TypeIndex.i32));
    ParserHelper.assertFieldType(struct, 7, "my_i64", new SimpleTypeSpec(TypeSpec.TypeIndex.i64));

    var init = requireNonNull(contractAbi.init());
    assertThat(init.name()).isEqualTo("initialize");

    var functions = contractAbi.contractInvocations();
    var actions = functions.subList(1, functions.size());
    assertThat(actions).hasSize(8); // 7 actions and init
    for (var action : actions) {
      assertThat(action.arguments()).hasSize(1);
    }

    ParserHelper.assertActionType(
        actions.get(0), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    ParserHelper.assertActionType(
        actions.get(1), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.u16));
    ParserHelper.assertActionType(
        actions.get(2), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.u32));
    ParserHelper.assertActionType(
        actions.get(3), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));
    ParserHelper.assertActionType(
        actions.get(4), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.i8));
    ParserHelper.assertActionType(
        actions.get(5), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.i16));
    ParserHelper.assertActionType(
        actions.get(6), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.i32));
    ParserHelper.assertActionType(
        actions.get(7), 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.i64));

    var stateStruct = (NamedTypeRef) contractAbi.stateType();
    assertThat(contractAbi.getNamedType(stateStruct)).isSameAs(structTypes.get(0));
  }

  @Test
  void simpleMapContractTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
    var parser = TestingHelper.loadAbiParserFromFile("contract_simple_map.abi");

    var result = parser.parseAbi();
    var contractAbi = (ContractAbi) result.chainComponent();

    var struct = (StructTypeSpec) contractAbi.namedTypes().get(0);
    var structField = struct.field(0);

    var actual = (MapTypeSpec) structField.type();

    assertThat(actual)
        .isEqualTo(
            new MapTypeSpec(
                new SimpleTypeSpec(TypeSpec.TypeIndex.Address),
                new SimpleTypeSpec(TypeSpec.TypeIndex.u8)));

    assertThat(actual.keyType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.Address);
    assertThat(actual.valueType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.u8);

    var init = requireNonNull(contractAbi.init());
    assertThat(init.arguments().size()).isEqualTo(0);

    var action = requireNonNull(contractAbi.getFunctionByName("insert_in_my_map"));

    assertThat(action.arguments().size()).isEqualTo(2);
    assertThat(action.arguments().get(0).type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.Address);
    assertThat(action.arguments().get(0).name()).isEqualTo("address");
    assertThat(action.arguments().get(1).name()).isEqualTo("value");

    var stateStruct = (NamedTypeRef) contractAbi.stateType();

    assertThat(contractAbi.getNamedType(stateStruct)).isEqualTo(contractAbi.namedTypes().get(0));
  }

  @Test
  void contract128BitInts() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-u128-and-i128
    var parser = TestingHelper.loadAbiParserFromFile("contract_u128_and_i128.abi");
    var result = parser.parseAbi();

    var struct = result.chainComponent().namedTypes().get(0);
    ParserHelper.assertFieldType(struct, 0, "my_u128", new SimpleTypeSpec(TypeSpec.TypeIndex.u128));
    ParserHelper.assertFieldType(struct, 1, "my_i128", new SimpleTypeSpec(TypeSpec.TypeIndex.i128));
  }

  @Test
  void contractWithSetType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-set
    var parser = TestingHelper.loadAbiParserFromFile("contract_set.abi");
    var result = parser.parseAbi();

    var expectedVersionBinder = new AbiVersion(1, 1, 0);
    var expectedVersionClient = new AbiVersion(2, 0, 0);

    assertThat(result.versionBinder()).isEqualTo(expectedVersionBinder);
    assertThat(result.versionClient()).isEqualTo(expectedVersionClient);

    var contractAbi = (ContractAbi) result.chainComponent();
    var structTypeSpec = (StructTypeSpec) contractAbi.namedTypes().get(0);
    var setType = (SetTypeSpec) structTypeSpec.field(0).type();

    assertThat(setType).isEqualTo(new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
    assertThat(setType.valueType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.u64);
    assertThat(structTypeSpec.field(0).name()).isEqualTo("my_set");

    var action = requireNonNull(contractAbi.getFunctionByName("insert_in_my_set"));
    assertThat(action.arguments().get(0).type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.u64);

    var stateStruct = (NamedTypeRef) contractAbi.stateType();
    assertThat(contractAbi.getNamedType(stateStruct)).isEqualTo(contractAbi.namedTypes().get(0));
  }

  @Test
  void simpleOptionContractTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
    var parser = TestingHelper.loadAbiParserFromFile("contract_options.abi");
    var result = parser.parseAbi();

    var contractAbi = (ContractAbi) result.chainComponent();
    var struct = (StructTypeSpec) contractAbi.namedTypes().get(0);
    var fields = struct.fields();

    assertThat(fields).hasSize(5);

    ParserHelper.assertFieldType(
        struct, 0, "option_u64", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
    ParserHelper.assertFieldType(
        struct,
        1,
        "option_string",
        new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String)));
    ParserHelper.assertFieldType(
        struct,
        2,
        "option_address",
        new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.Address)));
    ParserHelper.assertFieldType(
        struct,
        3,
        "option_boolean",
        new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool)));
    ParserHelper.assertFieldType(
        struct,
        4,
        "option_map",
        new OptionTypeSpec(
            new MapTypeSpec(
                new SimpleTypeSpec(TypeSpec.TypeIndex.u64),
                new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    var init = requireNonNull(contractAbi.init());
    assertThat(init.arguments()).hasSize(4);
    ParserHelper.assertActionType(
        init, 0, "option_u64", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
    ParserHelper.assertActionType(
        init,
        1,
        "option_string",
        new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String)));
    ParserHelper.assertActionType(
        init,
        2,
        "option_address",
        new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.Address)));
    ParserHelper.assertActionType(
        init, 3, "option_boolean", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool)));

    assertThat(
            contractAbi.contractInvocations().subList(1, contractAbi.contractInvocations().size()))
        .hasSize(5);
  }

  @Test
  void simpleVecContract() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_vector.abi");
    FileAbi result = parser.parseAbi();

    ContractAbi contractAbi = (ContractAbi) result.chainComponent();
    StructTypeSpec struct = (StructTypeSpec) contractAbi.namedTypes().get(0);
    ParserHelper.assertFieldType(
        struct, 0, "my_vec", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));

    ParserHelper.assertActionType(
        contractAbi.init(),
        0,
        "my_vec",
        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
    ParserHelper.assertActionType(
        contractAbi.getFunctionByName("update_my_vec"),
        0,
        "value",
        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
  }

  @Test
  void simpleArrayContract() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_byte_arrays.abi");
    FileAbi result = parser.parseAbi();

    ContractAbi contractAbi = (ContractAbi) result.chainComponent();
    StructTypeSpec structType = (StructTypeSpec) contractAbi.namedTypes().get(0);

    TypeSpec array16 = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 0x10);
    TypeSpec array5 = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 0x05);

    ParserHelper.assertFieldType(structType, 0, "my_array", array16);
    ParserHelper.assertFieldType(structType, 1, "my_array_2", array5);

    ParserHelper.assertActionType(
        contractAbi.getFunctionByName("update_my_array"), 0, "value", array16);
    ParserHelper.assertActionType(
        contractAbi.getFunctionByName("update_my_array_2"), 0, "value", array5);
  }

  @Test
  void contractStructOfStructTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_struct_of_struct_v3.abi");
    FileAbi result = parser.parseAbi();

    var headerString = "PBCABI";

    assertThat(result.header()).isEqualTo(headerString);
    ContractAbi contract = (ContractAbi) result.chainComponent();
    assertThat(result.versionBinder()).isEqualTo(new AbiVersion(3, 0, 0));
    assertThat(result.versionClient()).isEqualTo(new AbiVersion(3, 0, 0));
    assertThat(contract).isNotNull();

    var structTypes = contract.namedTypes();
    assertThat(structTypes).hasSize(3);

    StructTypeSpec myStructType = (StructTypeSpec) structTypes.get(0);
    assertThat(myStructType.name()).isEqualTo("MyStructType");
    ParserHelper.assertFieldType(
        myStructType, 0, "some_value", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));
    ParserHelper.assertFieldType(
        myStructType,
        1,
        "some_vector",
        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)));
    var initShortname = bytesFromHex("ffffffff0f");

    assertThat(requireNonNull(contract.init()).shortname()).isEqualTo(initShortname);
    assertThat(
            requireNonNull(contract.getFunctionByName("update_my_other_struct"))
                .shortnameAsString())
        .isEqualTo("85f292be0b");
  }

  @Test
  void contractSimpleStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi result = parser.parseAbi();
    ContractAbi contract = (ContractAbi) result.chainComponent();

    assertThat(result.versionBinder()).isEqualTo(new AbiVersion(3, 0, 0));
    assertThat(result.versionClient()).isEqualTo(new AbiVersion(3, 0, 0));

    var structTypes = contract.namedTypes();
    assertThat(structTypes).hasSize(3);
    StructTypeSpec myStructType = (StructTypeSpec) structTypes.get(0);
    ParserHelper.assertFieldType(
        myStructType, 0, "some_value", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

    StructTypeSpec nonStateStructType = (StructTypeSpec) structTypes.get(1);
    ParserHelper.assertFieldType(
        nonStateStructType, 0, "value", new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

    StructTypeSpec exampleContractState = (StructTypeSpec) structTypes.get(2);
    ParserHelper.assertFieldType(exampleContractState, 1, "my_struct", new NamedTypeRef(0));

    var updateMyu64UsingStruct = contract.contractInvocations().get(1);
    ParserHelper.assertActionType(updateMyu64UsingStruct, 0, "value", new NamedTypeRef(1));

    var updateMyStruct = contract.contractInvocations().get(1);
    ParserHelper.assertActionType(updateMyStruct, 0, "value", new NamedTypeRef(1));
  }

  @Test
  void contractCallback() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-callbacks/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi");
    FileAbi result = parser.parseAbi();
    assertThat(result.versionBinder()).isEqualTo(new AbiVersion(3, 0, 0));
    assertThat(result.versionClient()).isEqualTo(new AbiVersion(3, 0, 0));
    ContractAbi contract = (ContractAbi) result.chainComponent();

    StructTypeSpec exampleContractState = (StructTypeSpec) contract.namedTypes().get(0);
    ParserHelper.assertFieldType(
        exampleContractState, 0, "val", new SimpleTypeSpec(TypeSpec.TypeIndex.u32));
    ParserHelper.assertFieldType(
        exampleContractState,
        1,
        "successful_callback",
        new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    ParserHelper.assertFieldType(
        exampleContractState,
        2,
        "callback_results",
        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool)));
    ParserHelper.assertFieldType(
        exampleContractState, 3, "callback_value", new SimpleTypeSpec(TypeSpec.TypeIndex.u32));

    var actions = contract.contractInvocations();
    assertThat(actions).hasSize(5 + 1);
    ContractInvocation setAndCallbackFunction = actions.get(1);
    assertThat(setAndCallbackFunction.arguments()).hasSize(1);
    assertThat(setAndCallbackFunction.shortnameAsString()).isEqualTo("01");
    // Ensure "callback" is not a callable action
    for (var a : actions.subList(0, actions.size() - 1)) {
      assertThat(a.name()).isNotEqualTo("callback");
    }
  }

  @Test
  void contractAllTypes() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-all-types
    var parser = TestingHelper.loadAbiParserFromFile("contract_all_types_v3.abi");
    var result = parser.parseAbi();
    assertThat(result.versionClient()).isEqualTo(new AbiVersion(3, 0, 0));
    assertThat(result.versionBinder()).isEqualTo(new AbiVersion(2, 1, 0));

    ContractAbi contract = (ContractAbi) result.chainComponent();
    var structTypes = contract.namedTypes();
    assertThat(structTypes).hasSize(4);
    ParserHelper.assertFieldType(
        structTypes.get(1), 1, "vector", new VecTypeSpec(new NamedTypeRef(0)));

    NamedTypeRef stateStruct = (NamedTypeRef) contract.stateType();
    assertThat(contract.getNamedType(stateStruct)).isEqualTo(structTypes.get(3));

    var actions = contract.contractInvocations().subList(1, contract.contractInvocations().size());
    assertThat(actions).hasSize(23);
    assertThat(requireNonNull(contract.init()).arguments()).hasSize(19);
    ContractInvocation updateMyOtherStruct = actions.get(22);
    assertThat(updateMyOtherStruct.arguments().get(0).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.Named);
    ParserHelper.assertActionType(updateMyOtherStruct, 0, "value", new NamedTypeRef(0));
  }

  @Test
  void testContractStructOfWrongOrder() {
    // Contract can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/PAR-3123_recursive_types/contract-wrong-order-structs
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_wrong_order_structs.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi contract = (ContractAbi) model.chainComponent();
    var structTypes = contract.namedTypes();
    NamedTypeRef state = (NamedTypeRef) contract.stateType();

    ParserHelper.assertFieldType(structTypes.get(0), 0, "child", new NamedTypeRef(1));
    ParserHelper.assertFieldType(structTypes.get(state.index()), 0, "value", new NamedTypeRef(0));
  }

  @Test
  void testContractRecursiveTypes() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/PAR-3123_recursive_types/contract-recursive-types
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_recursive_types.abi");
    FileAbi model = parser.parseAbi();
    var structTypes = model.chainComponent().namedTypes();
    StructTypeSpec struct = (StructTypeSpec) structTypes.get(0);

    VecTypeSpec fieldType = (VecTypeSpec) struct.field(0).type();
    NamedTypeRef recursiveType = (NamedTypeRef) fieldType.valueType();
    assertThat(struct).isEqualTo(structTypes.get(recursiveType.index()));

    StructTypeSpec nonSelfRecursiveStruct = (StructTypeSpec) structTypes.get(1);
    NamedTypeRef structType = (NamedTypeRef) nonSelfRecursiveStruct.field(0).type();
    assertThat(structTypes.get(structType.index())).isNotEqualTo(nonSelfRecursiveStruct);
  }

  @Test
  void illegalVersionClient() {
    // Header bytes, binder version 3.0.0, client version 80.0.0
    var bytesMajor = bytesFromHex("504243414249030000500000");
    AbiParser parserMajor = new AbiParser(bytesMajor);

    assertThatThrownBy(parserMajor::parseAbi)
        .isInstanceOf(UnsupportedVersionException.class)
        .hasMessage("Unsupported Version 80.0.0 for Version Client.");

    // Header bytes, binder version 3.0.0, client version 1.66.0
    var bytesMinor = bytesFromHex("504243414249030000014200");
    AbiParser parserMinor = new AbiParser(bytesMinor);

    assertThatThrownBy(parserMinor::parseAbi)
        .isInstanceOf(UnsupportedVersionException.class)
        .hasMessage("Unsupported Version 1.66.0 for Version Client.");
  }

  @Test
  void testVersionCombinations() {
    ParserHelper.assertClientVersionFail(2, 1, 1, 66);
    ParserHelper.assertClientVersionFail(2, 0, 2, 66);
    ParserHelper.assertClientVersionFail(1, 0, 3, 2);
  }

  @Test
  void illegalByteAsType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    var bytes = TestingHelper.readBinaryFile("contract_booleans.abi");
    for (int i = 0; i < bytes.length; i++) {
      if (bytes[i] == 0x0c) {
        bytes[i] = 0x55; // Turn SimpleTypeSpec.bool into invalid type
      }
    }

    AbiParser parser = TestingHelper.abiParserFromBytes(bytes);

    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Unknown type index: 0x55");
  }

  @Test
  void illegalByteAsNamedType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
    var bytes = TestingHelper.readBinaryFile("contract_enum.abi");
    // Overwrite the byte denoting that Vehicle is an enum (02)
    // only valid values is (02 - enum) and (01 - struct)
    bytes[16] = 0x55;

    AbiParser parser = TestingHelper.abiParserFromBytes(bytes);

    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Bad byte 0x55 used for namedTypeSpec index should be either 0x01 for a struct or 0x02"
                + " for an enum");
  }

  @Test
  void illegalByteAsEnumVariant() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
    var bytes = TestingHelper.readBinaryFile("contract_enum.abi");
    // Overwrite the byte denoting that the enum variant Bicycle is a named type
    // only valid values is (00 - NamedTypeRef)
    bytes[33] = 0x55;

    AbiParser parser = TestingHelper.abiParserFromBytes(bytes);

    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Non named type Unknown used as an enum variant, each variant should be a reference to"
                + " a struct");

    // Overwrite the byte denoting that the enum variant Bicycle is a named type
    // only valid values is (00 - NamedTypeRef)
    bytes[33] = 0x03;

    AbiParser parser2 = TestingHelper.abiParserFromBytes(bytes);

    assertThatThrownBy(parser2::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Non named type u32 used as an enum variant, each variant should be a reference "
                + "to a struct");
  }

  @Test
  void contractSecretInput() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi");
    var abi = (ContractAbi) parser.parseAbi().chainComponent();
    var onSecretInput = requireNonNull(abi.getFunctionByName("swap"));
    assertThat(onSecretInput.kind())
        .isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE);

    var secretArg = requireNonNull(onSecretInput.secretArgument());

    assertThat(secretArg.type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.Named);

    var type = (NamedTypeRef) secretArg.type();
    var secretStruct = (StructTypeSpec) abi.getNamedType(type);
    assertThat(secretStruct.name()).isEqualTo("AmountAndDirectionSecret");

    assertThat(secretStruct.field(0).name()).isEqualTo("amount");
    assertThat(secretStruct.field(0).type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.i128);
    assertThat(secretStruct.field(1).name()).isEqualTo("direction");
    assertThat(secretStruct.field(1).type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.i8);
  }

  @Test
  void contractSecretInputOldVersion() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi");
    var abi = (ContractAbi) parser.parseAbi().chainComponent();
    var fn = requireNonNull(abi.getFunctionByName("add_vote"));
    assertThat(fn.kind())
        .isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE);
    ArgumentAbi secretArg = requireNonNull(fn.secretArgument());
    assertThat(secretArg.name()).isEqualTo("secret_input");
    assertThat(secretArg.type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.i32);
  }

  @Test
  void invalidLebEncodingInit() {
    var bytes = TestingHelper.readBinaryFile("contract_booleans_v3.abi");
    var initIndex = 70;
    bytes[initIndex + 4] = (byte) 0xFF;

    AbiParser parser = new AbiParser(bytes);
    assertThatThrownBy(parser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int "
                + "(max 5 bytes)");
  }

  @Test
  void shortnamesAreEncodedCorrectly() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_booleans_v3.abi");
    ContractAbi abi = (ContractAbi) parser.parseAbi().chainComponent();

    var initShortname = bytesFromHex("ffffffff0f");
    assertThat(requireNonNull(abi.init()).shortname()).isEqualTo(initShortname);

    var expectedShortName = "b0a1fab30c";

    assertThat(
            requireNonNull(
                    abi.getFunction(
                        bytesFromHex(expectedShortName), ImplicitBinderAbi.DefaultKinds.ACTION))
                .shortnameAsString())
        .isEqualTo(expectedShortName);
  }

  @Test
  void simpleTypesAbiVersion6AreParsedCorrectly() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    AbiParser parser =
        TestingHelper.loadAbiParserFromFile("contract_types_hash_to_bls_signature_v5.abi");
    ContractAbi abi = (ContractAbi) parser.parseAbi().chainComponent();

    assertThat(requireNonNull(abi.getStateStruct().getFieldByName("u256")).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.u256);
    assertThat(requireNonNull(abi.getStateStruct().getFieldByName("hash")).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.Hash);
    assertThat(requireNonNull(abi.getStateStruct().getFieldByName("public_key")).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.PublicKey);
    assertThat(requireNonNull(abi.getStateStruct().getFieldByName("signature")).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.Signature);
    assertThat(
            requireNonNull(abi.getStateStruct().getFieldByName("bls_public_key"))
                .type()
                .typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.BlsPublicKey);
    assertThat(
            requireNonNull(abi.getStateStruct().getFieldByName("bls_signature")).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.BlsSignature);

    assertThat(
            requireNonNull(abi.getFunctionByName("set_u256")).arguments().get(0).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.u256);
    assertThat(
            requireNonNull(abi.getFunctionByName("set_hash")).arguments().get(0).type().typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.Hash);
    assertThat(
            requireNonNull(abi.getFunctionByName("set_public_key"))
                .arguments()
                .get(0)
                .type()
                .typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.PublicKey);
    assertThat(
            requireNonNull(abi.getFunctionByName("set_signature"))
                .arguments()
                .get(0)
                .type()
                .typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.Signature);
    assertThat(
            requireNonNull(abi.getFunctionByName("set_bls_public_key"))
                .arguments()
                .get(0)
                .type()
                .typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.BlsPublicKey);
    assertThat(
            requireNonNull(abi.getFunctionByName("set_bls_signature"))
                .arguments()
                .get(0)
                .type()
                .typeIndex())
        .isEqualTo(TypeSpec.TypeIndex.BlsSignature);
  }

  @Test
  void abiFileTooLong() {
    var validAbi = TestingHelper.readBinaryFile("contract_booleans_v3.abi");
    var tooLongFile = concatBytes(validAbi, new byte[1]);
    AbiParser tooManyBytesParser = TestingHelper.abiParserFromBytes(tooLongFile);
    assertThatThrownBy(tooManyBytesParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected EOF after parsed ABI, but stream had 1 bytes remaining");
  }

  @Test
  void unknownFnKind() {
    var bytes =
        bytesFromHex(
            "50"
                + "42"
                + "43"
                + "41"
                + "42"
                + "49"
                + // Header
                "04"
                + "00"
                + "00"
                + // Binder version 4.0.0
                "04"
                + "00"
                + "00"
                + // Client version 4.0.0
                "00"
                + "00"
                + "00"
                + "00"
                + // No Struct Types
                "00"
                + "00"
                + "00"
                + "01"
                + // A single hook
                "ff"
                + // Weird FnKind
                "00"
                + "00"
                + "00"
                + "00"
                + // fn name: ""
                "00"
                + // Shortname : 0
                "00"
                + "00"
                + "00"
                + "00"
                + // No arguments
                "01" // State type: u8
            );
    AbiParser parserMajor = TestingHelper.abiParserFromBytes(bytes);

    assertThatThrownBy(parserMajor::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Unsupported FnKind type 0xff specified");
  }

  @Test
  void testContractAvlTreeMap() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/token-mapping
    AbiParser parser = TestingHelper.loadAbiParserFromFile("token_mapping.abi");
    ContractAbi contract = (ContractAbi) parser.parseAbi().chainComponent();
    var state = contract.getStateStruct();
    assertThat(state.field(0).name()).isEqualTo("map");
    var type = state.field(0).type();
    assertThat(type.typeIndex()).isEqualTo(TypeSpec.TypeIndex.AvlTreeMap);
    var avlType = (AvlTreeMapTypeSpec) type;
    assertThat(avlType.keyType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.Address);
    assertThat(avlType.valueType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.u128);

    assertThat(state.field(1).name()).isEqualTo("result");
    assertThat(state.field(1).type().typeIndex()).isEqualTo(TypeSpec.TypeIndex.Option);
  }

  /**
   * It is not allowed to have an invocation of some kind with no shortname if there exists another
   * invocation of the same kind with a shortname, and the AbiParser crashes when it encounters
   * this.
   */
  @Test
  void actionAndPassThroughAction() {
    byte[] abiBytes =
        bytesFromHex(
            "504243414249" // Header
                + "000000" // binder version
                + "060000" // client version
                + "20" // binder abi
                + "00" // Public binder
                + "00000000" // no contract callable kinds
                + "00000000" // no named types
                + "00000002" // number of binder invocations
                + "02" // action kind
                + "00000001" // name length
                + "61" // name: a
                + "00" // none shortname
                + "00000000" // nor arguments
                + "0102" // Call contract
                + "02" // action kind
                + "00000001" // name length
                + "62" // name: b
                + "01" // some shortname
                + "00" // Shortname
                + "00000000" // no arguments
                + "00" // no contract call
                + "01" // state type
            );

    AbiParser abiParser = new AbiParser(abiBytes);
    assertThatThrownBy(() -> abiParser.parseAbi())
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Binder cannot contain more than one invocation of kind 'Action', "
                + "when one exists without a shortname");
  }

  /** The binder can specify multiple callback invocations. */
  @Test
  void multipleCallback() {
    byte[] abiBytes =
        bytesFromHex(
            "504243414249" // Header
                + "000000" // binder version
                + "060000" // client version
                + "20" // binder abi
                + "00" // Public binder
                + "00000000" // no contract callable kinds
                + "00000000" // no named types
                + "00000002" // number of binder invocations
                + "03" // callback kind
                + "00000001" // name length
                + "61" // name: a
                + "01" // some shortname
                + "00" // shortname: 1
                + "00000000" // no arguments
                + "0102" // Call contract
                + "03" // callback kind
                + "00000001" // name length
                + "62" // name: b
                + "01" // some shortname
                + "01" // shortname: 0
                + "00000000" // no arguments
                + "00" // no contract call
                + "01" // state type
            );

    BinderAbi binderAbi = (BinderAbi) new AbiParser(abiBytes).parseAbi().chainComponent();
    assertThat(binderAbi.binderInvocations()).hasSize(2);
    assertThat(binderAbi.binderInvocations().get(0).kind()).isEqualTo(TransactionKind.Callback);
    assertThat(binderAbi.binderInvocations().get(1).kind()).isEqualTo(TransactionKind.Callback);
  }

  /** The AbiParser crashes if it encounters an unknown kind. */
  @Test
  void unknownInvocationKind() {
    byte[] abiBytes =
        bytesFromHex(
            "504243414249" // Header
                + "000000" // binder version
                + "060000" // client version
                + "20" // binder abi
                + "00" // Public binder
                + "00000000" // no contract callable kinds
                + "00000000" // no named types
                + "00000001" // number of binder invocations
                + "ff" // unknown binder invocation
                + "00000001" // name length
                + "61" // name: a
                + "0102" // Call contract
                + "01" // state type
            );

    AbiParser abiParser = new AbiParser(abiBytes);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Unsupported TransactionKind type 0xff specified");
  }

  /**
   * The default binder invocation 'openInvocation' of an old zk-contract specifies, specifies the
   * shortname with value 9.
   */
  @Test
  void zkOpenInvocationByte() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi");
    ContractAbi contractAbi = parser.parseContractAbi();
    BinderInvocation functionAbi =
        requireNonNull(contractAbi.getBinderInvocationByName("openInvocation"));
    assertThat(functionAbi.shortname()).isEqualTo((byte) 9);
  }

  /** The default binder invocation 'invoke' for an old pub-contract, does not have a shortname. */
  @Test
  void pubNoOpenInvocationByte() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_booleans.abi");
    ContractAbi contractAbi = parser.parseContractAbi();
    BinderInvocation functionAbi = requireNonNull(contractAbi.getBinderInvocationByName("invoke"));
    assertThat(functionAbi.shortname()).isNull(); // commontests-expect-null
  }

  /** Abi parser crashes if trying to parse a binder abi as a contract abi. */
  @Test
  void getContractAbiForBinderAbi() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("PublicWasmContractBinder.abi");
    assertThatThrownBy(parser::parseContractAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Failed to parse abi as a contract abi");
  }

  /** Abis with old abi versions (<= 5.6.0) gets their functions sorted when parsed. */
  @Test
  void oldAbisAreSortedByKindId() {
    byte[] rawAbi =
        new FileAbiBuilder("old-sorted-functions.abi")
            .binderVersion(new AbiVersion(11, 4, 0))
            .clientVersion(new AbiVersion(5, 6, 0))
            .stateType(new SimpleTypeSpec(TypeSpec.TypeIndex.u8))
            .addFnAbi(
                new FnAbiBuilder()
                    .name("callback")
                    .shortname("01")
                    .kind(ImplicitBinderAbi.DefaultKinds.CALLBACK))
            .addFnAbi(
                new FnAbiBuilder()
                    .name("action")
                    .shortname("02")
                    .kind(ImplicitBinderAbi.DefaultKinds.ACTION))
            .build();

    ContractAbi contractAbi = new AbiParser(rawAbi).parseContractAbi();
    assertThat(contractAbi.contractInvocations().get(0).name()).isEqualTo("action");
    assertThat(contractAbi.contractInvocations().get(1).name()).isEqualTo("callback");
  }
}
