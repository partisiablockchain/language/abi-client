package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.abimodel.commontests.ParserHelper;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ContractInvocationTest {

  private final ArgumentAbi argument =
      new ArgumentAbi("arg1", new SimpleTypeSpec(TypeSpec.TypeIndex.i32));
  private final ContractInvocation fnKindAbiInit =
      new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.INIT,
          "init",
          HexFormat.of().parseHex("fffffffff0"),
          List.of(argument));
  private final ContractInvocation simpleContractInvocation =
      new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.ACTION,
          "fnName",
          HexFormat.of().parseHex("b0a1f1b30c"),
          List.of(argument));

  @Test
  void values() {
    assertThat(fnKindAbiInit.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.INIT);
    assertThat(fnKindAbiInit.name()).isEqualTo("init");
    assertThat(fnKindAbiInit.shortnameAsString()).isEqualTo("fffffffff0");
    assertThat(fnKindAbiInit.arguments().get(0)).isEqualTo(argument);

    assertThat(simpleContractInvocation.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ACTION);
    assertThat(simpleContractInvocation.name()).isEqualTo("fnName");
    assertThat(simpleContractInvocation.shortnameAsString()).isEqualTo("b0a1f1b30c");
    assertThat(simpleContractInvocation.arguments().get(0)).isEqualTo(argument);
  }

  @Test
  void testFnKindTypes() {
    ContractInvocation fnOne = ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ACTION);
    assertThat(fnOne.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ACTION);

    ContractInvocation fnTwo = ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.CALLBACK);
    assertThat(fnTwo.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.CALLBACK);

    ContractInvocation fnThree =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT);
    assertThat(fnThree.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT);

    ContractInvocation fnFour =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_INPUTTED);
    assertThat(fnFour.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_INPUTTED);

    ContractInvocation fnFive =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_REJECTED);
    assertThat(fnFive.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_REJECTED);

    ContractInvocation fnSix =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_COMPUTE_COMPLETE);
    assertThat(fnSix.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_COMPUTE_COMPLETE);

    ContractInvocation fnSeven =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLES_OPENED);
    assertThat(fnSeven.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLES_OPENED);

    ContractInvocation fnEight =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_USER_VARIABLES_OPENED);
    assertThat(fnEight.kind())
        .isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_USER_VARIABLES_OPENED);

    ContractInvocation fn9 =
        ParserHelper.fnAbiWithKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_ATTESTATION_COMPLETE);
    assertThat(fn9.kind()).isEqualTo(ImplicitBinderAbi.DefaultKinds.ZK_ON_ATTESTATION_COMPLETE);
  }

  @Test
  void twoZkVarInputtedFunctions() {
    byte[] twoZkFnKindBytes =
        HexFormat.of()
            .parseHex(
                // Header bytes
                "504243414249030000040000"
                    +
                    // Zero structs
                    "00000000"
                    // Three functions
                    + "00000003"
                    // FnKind: Init, name = init, shortname = 00, 0 arguments
                    + "01"
                    + "00000004696e697400"
                    + "00000000"
                    // FnKind: ZkVarInputted, name = myfn, shortname = 01, 0 arguments
                    + "11"
                    + "000000046d79666e01"
                    + "00000000"
                    // FnKind: ZkVarInputted, name = fntwo, shortname = 02, 0 arguments
                    + "11"
                    + "00000005666e74776f02"
                    + "00000000"
                    // StateType: SimpleTypeSpec u8
                    + "01");
    AbiParser parserZkVarInputted = new AbiParser(twoZkFnKindBytes);

    // Parses without throwing error.
    parserZkVarInputted.parseAbi();
  }
}
