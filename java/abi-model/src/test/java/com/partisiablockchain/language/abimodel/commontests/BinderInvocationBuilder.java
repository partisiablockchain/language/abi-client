package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import java.util.ArrayList;
import java.util.List;

/** Helper class to build {@link BinderInvocation}s for references in test. */
public final class BinderInvocationBuilder {
  private TransactionKind kind;
  private String name;
  private Byte shortname;
  private final List<ArgumentAbi> arguments = new ArrayList<>();
  private Integer contractCallKindId;

  /**
   * Set the kind of this function.
   *
   * @param kind kind of the function
   * @return this builder
   */
  public BinderInvocationBuilder kind(TransactionKind kind) {
    this.kind = kind;
    return this;
  }

  /**
   * Set the name of this function.
   *
   * @param name name of the function
   * @return this builder
   */
  public BinderInvocationBuilder name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Set the shortname of this function.
   *
   * @param shortname hex encoded shortname of the function
   * @return this builder
   */
  public BinderInvocationBuilder shortname(byte shortname) {
    this.shortname = shortname;
    return this;
  }

  /**
   * Set the contract call id of this function.
   *
   * @param kindId id of the kind the binder invocation calls.
   * @return this builder
   */
  public BinderInvocationBuilder contractCallKindId(int kindId) {
    this.contractCallKindId = kindId;
    return this;
  }

  /**
   * Build into a {@link BinderInvocation}.
   *
   * @return the built BinderInvocation
   */
  public BinderInvocation build() {
    return new BinderInvocation(kind, name, shortname, arguments, contractCallKindId);
  }
}
