package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.ArrayList;
import java.util.List;

/** Helper class to build {@link StructTypeSpec}s for reference in a test. */
public final class StructTypeBuilder {

  private String name;
  private final List<FieldAbi> fields = new ArrayList<>();

  /**
   * Set the name of the struct.
   *
   * @param name name of the struct
   * @return this builder
   */
  public StructTypeBuilder name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Add field to the struct.
   *
   * @param name name of the field
   * @param type type of the field
   * @return this builder
   */
  public StructTypeBuilder addField(String name, TypeSpec type) {
    FieldAbi fieldAbi = new FieldAbi(name, type);
    this.fields.add(fieldAbi);
    return this;
  }

  /**
   * Build new {@link StructTypeSpec}.
   *
   * @return new {@link StructTypeSpec}
   */
  public StructTypeSpec build() {
    return new StructTypeSpec(name, fields);
  }
}
