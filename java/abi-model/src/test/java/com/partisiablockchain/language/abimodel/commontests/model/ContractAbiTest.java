package com.partisiablockchain.language.abimodel.commontests.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.commontests.TestingHelper;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ContractAbiTest {

  private final ContractAbi abi =
      new ContractAbi(
          BinderType.Public,
          List.of(),
          List.of(),
          ImplicitBinderAbi.defaultPubInvocations(),
          List.of(
              new ContractInvocation(
                  ImplicitBinderAbi.DefaultKinds.ACTION, "name", new byte[] {0x01}, List.of()),
              new ContractInvocation(
                  ImplicitBinderAbi.DefaultKinds.INIT, "initialize", new byte[] {0x00}, List.of())),
          new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

  @Test
  void getFunctionByName() {
    assertThat(requireNonNull(abi.getFunctionByName("name")).name()).isEqualTo("name");
    assertThat(abi.getFunctionByName("x")).isNull();
  }

  @Test
  void getFunctionByShortname() {
    assertThat(
            requireNonNull(
                    abi.getFunction(new byte[] {0x01}, ImplicitBinderAbi.DefaultKinds.ACTION))
                .name())
        .isEqualTo("name");
    assertThat(abi.getFunction(new byte[] {0x02}, ImplicitBinderAbi.DefaultKinds.ACTION)).isNull();
    assertThat(abi.getFunction(new byte[] {0x01}, ImplicitBinderAbi.DefaultKinds.CALLBACK))
        .isNull();
  }

  @Test
  void init() {
    assertThat(requireNonNull(abi.init()).name()).isEqualTo("initialize");
    assertThat(requireNonNull(abi.init()).shortname()).isEqualTo(new byte[] {0x00});
  }

  /** Can get the init binder invocation from a contract abi. */
  @Test
  void initBinderInvocation() {
    assertThat(requireNonNull(abi.initBinderInvocation()).name()).isEqualTo("create");
    assertThat(requireNonNull(abi.initBinderInvocation()).shortname()).isEqualTo(null);
  }

  /** If no binder invocation with init kind exists return null. */
  @Test
  void initBinderInvocationNull() {
    var abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(new BinderInvocation(TransactionKind.Action, "action", null, List.of(), null)),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    assertThat(abi.initBinderInvocation()).isNull();
  }

  @Test
  void getStateStruct() {
    var stateStruct =
        new StructTypeSpec(
            "test", List.of(new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))));
    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(stateStruct),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(),
            new NamedTypeRef(0));
    assertThat(contractAbi.getStateStruct()).isEqualTo(stateStruct);
  }

  @Test
  void getNamedType() {
    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(new StructTypeSpec("named", List.of())),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(),
            new NamedTypeRef(0));
    assertThat(requireNonNull(contractAbi.getNamedType("named")).name()).isEqualTo("named");
    assertThat(contractAbi.getNamedType("unknown")).isNull();
  }

  @Test
  void isZk() {
    assertThat(abi.isZk()).isFalse();
    assertThat(TestingHelper.getContractAbiFromFile("average_salary.abi").isZk()).isTrue();
    assertThat(TestingHelper.getContractAbiFromFile("voting.abi").isZk()).isFalse();
    assertThat(TestingHelper.getContractAbiFromFile("icrc.abi").isZk()).isTrue();
    assertThat(TestingHelper.getContractAbiFromFile("contract_booleans.abi").isZk()).isFalse();
  }

  /**
   * Calling getBinderInvocation with a shortname and a kind returns a binder invocation with that
   * shortname and kind, or null if it doesn't exist.
   */
  @Test
  void getBinderFunction() {
    var abi = testContractAbi();

    assertThat(requireNonNull(abi.getBinderInvocation((byte) 4, TransactionKind.Action)).name())
        .isEqualTo("binderOnly");
    assertThat(requireNonNull(abi.getBinderInvocation((byte) 9, TransactionKind.Action)).name())
        .isEqualTo("openInvocation");
    assertThat(requireNonNull(abi.getBinderInvocation(null, TransactionKind.Callback)).name())
        .isEqualTo("binderCallback");
    assertThat(abi.getBinderInvocation((byte) 2, TransactionKind.Action)).isNull();
    assertThat(abi.getBinderInvocation((byte) 9, TransactionKind.Callback)).isNull();
  }

  /**
   * Calling getBinderFunctionByName returns a binder invocation with the given name, or null if it
   * doesn't exist.
   */
  @Test
  void getBinderFunctionByName() {
    var abi = testContractAbi();
    assertThat(requireNonNull(abi.getBinderInvocationByName("binderOnly")).name())
        .isEqualTo("binderOnly");
    assertThat(requireNonNull(abi.getBinderInvocationByName("binderCallback")).name())
        .isEqualTo("binderCallback");
    assertThat(abi.getBinderInvocationByName("otherName")).isNull();
  }

  private ContractAbi testContractAbi() {
    return new ContractAbi(
        BinderType.Public,
        List.of(),
        List.of(),
        List.of(
            new BinderInvocation(TransactionKind.Action, "binderOnly", (byte) 4, List.of(), null),
            new BinderInvocation(TransactionKind.Action, "openInvocation", (byte) 9, List.of(), 2),
            new BinderInvocation(TransactionKind.Callback, "binderCallback", null, List.of(), 3)),
        List.of(),
        new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
  }

  /** Can get the contract invocation which are called by specified binder invocation names. */
  @Test
  void getContractInvocationsFromBinderInvocation() {
    ContractAbi abi =
        new ContractAbi(
            BinderType.Zk,
            ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultZkInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION, "action", new byte[0], List.of()),
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION, "action2", new byte[0], List.of()),
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.CALLBACK,
                    "contract_callback",
                    new byte[0],
                    List.of()),
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE,
                    "secret_input",
                    new byte[0],
                    List.of())),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));

    List<ContractInvocation> contractInvocations =
        abi.getContractInvocations(
            List.of("invoke", "openInvocation", "zeroKnowledgeInputOnChain"));
    assertThat(contractInvocations.size()).isEqualTo(3);
    assertThat(contractInvocations.get(0).name()).isEqualTo("action");
    assertThat(contractInvocations.get(1).name()).isEqualTo("action2");
    assertThat(contractInvocations.get(2).name()).isEqualTo("secret_input");

    List<ContractInvocation> contractInvocations2 = abi.getContractInvocations("callback");
    assertThat(contractInvocations2.size()).isEqualTo(1);
    assertThat(contractInvocations2.get(0).name()).isEqualTo("contract_callback");

    // Weird TS stryker case
    List<ContractInvocation> contractInvocations3 =
        abi.getContractInvocations("callback_extra_name");
    assertThat(contractInvocations3.size()).isEqualTo(0);
  }
}
