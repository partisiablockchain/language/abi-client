package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.DocumentationNamedType;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import com.partisiablockchain.language.abimodel.parser.RustSyntaxPrettyPrinter;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RustSyntaxPrettyPrinterTest {

  private final ArgumentAbi argument =
      new ArgumentAbi("arg1", new SimpleTypeSpec(TypeSpec.TypeIndex.i32));
  private final ContractInvocation fnKindAbiInit =
      new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.INIT,
          "init",
          HexFormat.of().parseHex("fffffffff0"),
          List.of(argument));
  private final ContractInvocation simpleFnAbi =
      new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.ACTION,
          "fnName",
          HexFormat.of().parseHex("b0a1f1b30c"),
          List.of(argument));

  private final ContractInvocation callbackFnAbi =
      new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.CALLBACK,
          "fnName",
          HexFormat.of().parseHex("b0a1f1b30c"),
          List.of(argument));

  private final StructTypeSpec stateStruct =
      new StructTypeSpec(
          "ContractState",
          List.of(new FieldAbi("field", new SimpleTypeSpec(TypeSpec.TypeIndex.u32))),
          new DocumentationNamedType(
              List.of("Some description", "Even more description"),
              Map.ofEntries(Map.entry("field", "FieldDoc"))));

  private final FileAbi abi =
      new FileAbi(
          "PBCABI",
          new AbiVersion(11, 0, 0),
          new AbiVersion(5, 4, 0),
          -1,
          new ContractAbi(
              BinderType.Public,
              List.of(),
              List.of(stateStruct),
              List.of(),
              List.of(simpleFnAbi, fnKindAbiInit, callbackFnAbi),
              new NamedTypeRef(0)));

  @Test
  void printState() {
    assertThat(new RustSyntaxPrettyPrinter(abi).printStruct(stateStruct))
        .isEqualTo(
            "/// Some description\n"
                + "/// Even more description\n"
                + "/// \n"
                + "/// ### Fields\n"
                + "/// \n"
                + "/// * `field`: [`u32`], FieldDoc\n"
                + "/// \n"
                + "#[state]\n"
                + "pub struct ContractState {\n"
                + "    field: u32,\n"
                + "}\n\n");
  }

  @Test
  void printTheState() {
    assertThat(new RustSyntaxPrettyPrinter(abi).printState())
        .isEqualTo(
            "/// Some description\n"
                + "/// Even more description\n"
                + "/// \n"
                + "/// ### Fields\n"
                + "/// \n"
                + "/// * `field`: [`u32`], FieldDoc\n"
                + "/// \n"
                + "#[state]\n"
                + "pub struct ContractState {\n"
                + "    field: u32,\n"
                + "}\n\n");
  }

  @Test
  void printActions() {
    assertThat(new RustSyntaxPrettyPrinter(abi).printActions())
        .isEqualTo(
            "#[action(shortname = 0xb0a1f1b30c)]\n"
                + "pub fn fnName (\n"
                + "    arg1: i32,\n"
                + ")\n\n"
                + "#[init(shortname = 0xfffffffff0)]\n"
                + "pub fn init (\n"
                + "    arg1: i32,\n"
                + ")\n"
                + "\n");
  }

  @Test
  void printCallbacks() {
    assertThat(new RustSyntaxPrettyPrinter(abi).printCallbacks())
        .isEqualTo(
            "#[callback(shortname = 0xb0a1f1b30c)]\n"
                + "pub fn fnName (\n"
                + "    arg1: i32,\n"
                + ")\n"
                + "\n");
  }

  @Test
  void printAllFunctions() {
    assertThat(new RustSyntaxPrettyPrinter(abi).printAllInvocations())
        .isEqualTo(
            "#[action(shortname = 0xb0a1f1b30c)]\n"
                + "pub fn fnName (\n"
                + "    arg1: i32,\n"
                + ")\n"
                + "\n"
                + "#[init(shortname = 0xfffffffff0)]\n"
                + "pub fn init (\n"
                + "    arg1: i32,\n"
                + ")\n"
                + "\n"
                + "#[callback(shortname = 0xb0a1f1b30c)]\n"
                + "pub fn fnName (\n"
                + "    arg1: i32,\n"
                + ")\n"
                + "\n");
  }

  @Test
  void emptyFunctionDoc() {
    assertThat(
            new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION, "name", new byte[0], List.of())
                .doc())
        .isNotNull();
  }

  @Test
  void emptyNamedDoc() {
    assertThat(new StructTypeSpec("name", List.of()).doc()).isNotNull();
  }

  /** Can print a binder abi. */
  @Test
  void binderCallbackWithShortname() {
    BinderAbi binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(
                new BinderInvocation(
                    TransactionKind.Callback, "callback", (byte) 3, List.of(), null)),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    FileAbi model =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, binderAbi);
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Version Binder: 12.0.0\n"
                + "// Version Client: 6.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "enum ContractCallableKinds {\n"
                + "}\n"
                + "\n"
                + "#[state]\n"
                + "u8\n\n"
                + "#[binder_callback(shortname = 0x03)]\n"
                + "pub fn callback ()\n\n");
  }

  /** Can print the upgrade invocation of a binder abi. */
  @Test
  void binderUpgrade() {
    BinderAbi binderAbi =
        new BinderAbi(
            BinderType.Sys,
            List.of(),
            List.of(),
            List.of(
                new BinderInvocation(TransactionKind.Upgrade, "upgrade", null, List.of(), null)),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    FileAbi model =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, binderAbi);
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Version Binder: 12.0.0\n"
                + "// Version Client: 6.0.0\n"
                + "\n"
                + "// Binder Type: Sys\n"
                + "\n"
                + "enum ContractCallableKinds {\n"
                + "}\n"
                + "\n"
                + "#[state]\n"
                + "u8\n\n"
                + "#[binder_upgrade]\n"
                + "pub fn upgrade ()\n\n");
  }
}
