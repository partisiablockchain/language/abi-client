package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import java.util.ArrayList;
import java.util.List;

/** Helper class to build {@link EnumTypeSpec}s for reference in a test. */
public final class EnumTypeBuilder {
  private String name;
  private final List<EnumVariant> variants = new ArrayList<>();

  /**
   * Set the name of the enum.
   *
   * @param name enum name
   * @return this builder
   */
  public EnumTypeBuilder name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Add variant to the enum.
   *
   * @param discriminant variant discriminant
   * @param typeRef pointer to the index of the variant struct
   * @return this builder
   */
  public EnumTypeBuilder addVariant(int discriminant, NamedTypeRef typeRef) {
    EnumVariant variant = new EnumVariant(discriminant, typeRef);
    this.variants.add(variant);
    return this;
  }

  /**
   * Build new {@link EnumTypeSpec}.
   *
   * @return new {@link EnumTypeSpec}
   */
  public EnumTypeSpec build() {
    return new EnumTypeSpec(name, variants);
  }
}
