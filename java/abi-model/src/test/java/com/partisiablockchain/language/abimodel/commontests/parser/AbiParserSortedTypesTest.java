package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.commontests.BinderInvocationBuilder;
import com.partisiablockchain.language.abimodel.commontests.EnumTypeBuilder;
import com.partisiablockchain.language.abimodel.commontests.FileAbiBuilder;
import com.partisiablockchain.language.abimodel.commontests.FnAbiBuilder;
import com.partisiablockchain.language.abimodel.commontests.StructTypeBuilder;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParseException;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** AbiParser test for validating types and functions are ordered as expected. */
public final class AbiParserSortedTypesTest {

  private final AbiVersion version = new AbiVersion(5, 7, 0);

  /**
   * Given a binary ABI with a state type other than {@link NamedTypeRef} the ABI still parses as
   * valid.
   */
  @Test
  void abiWithSimpleTypeSpecAsStateTypeIsValid() {
    byte[] rawAbi = baseFileAbiBuilder("simple_type_spec_as_state.abi").build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.stateType()).isEqualTo(new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
  }

  /** Given an ABI with a {@link NamedTypeRef} indexed 0 is invalid if the types list is empty. */
  @Test
  void abiWithNamedTypeRefAsStateTypeMustHaveTheTypeInTypesList() {
    byte[] rawAbi =
        baseFileAbiBuilder("named_state_type_with_empty_types_list.abi")
            .stateType(new NamedTypeRef(0))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("No known types at index 0");
  }

  /**
   * Given an ABI with a {@link NamedTypeRef} indexed 0 as the state type index the ABI parses as
   * valid if the type is found in the list of named types.
   */
  @Test
  void abiWithNamedTypeRefAsStateTypeIsValidIfTypeFoundInTypes() {
    EnumTypeSpec state = new EnumTypeSpec("EnumState", List.of());
    byte[] rawAbi =
        baseFileAbiBuilder("named_enum_state.abi")
            .addNamedType(state)
            .stateType(new NamedTypeRef(0))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.stateType()).isEqualTo(new NamedTypeRef(0));
  }

  /**
   * When an ABI has a state type of {@link NamedTypeRef} where the index is not zero, the parser
   * throws an exception.
   */
  @Test
  void abiWithStateTypeAsNamedTypeIndexedOtherThanZeroIsNotValid() {
    byte[] rawAbi =
        baseFileAbiBuilder("state_type_at_index_1.abi")
            .addNamedType(structType("A"))
            .addNamedType(structType("State").addField("a", new NamedTypeRef(0)))
            .stateType(new NamedTypeRef(1))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected named type 'State' to have index 0 but was 1");
  }

  /**
   * Given ABI with single action with named argument type and state type that is not a named ref,
   * the actions argument type is indexed at zero.
   */
  @Test
  void abiWithDefinedActionArgumentCanHaveTypeIndexedAtZeroIfStateTypeIsNotNamedType() {
    StructTypeSpec actionRpcStruct = new StructTypeSpec("ActionRpc", List.of());
    FnAbiBuilder action =
        actionFn("firstAction").shortname("01").addArgument("a", new NamedTypeRef(0));
    byte[] rawAbi =
        baseFileAbiBuilder("single_action_argument_type.abi")
            .addNamedType(actionRpcStruct)
            .addFnAbi(action)
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getFunctionByName("firstAction")).isNotNull();
  }

  /**
   * Given an ABI with a function that has a named type as it's argument, the type must point to an
   * existing type in the types list.
   */
  @Test
  void abiWithDefinedActionArgumentMustHaveArgumentTypeInTypesList() {
    FnAbiBuilder action =
        actionFn("firstAction").shortname("01").addArgument("a", new NamedTypeRef(1));
    byte[] rawAbi =
        baseFileAbiBuilder("single_action_argument_type_missing_from_types_list.abi")
            .addFnAbi(action)
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("No known types at index 1");
  }

  /**
   * When an ABI has two defined functions, they must be sorted by the identifier of their FnKind.
   */
  @Test
  void functionsMustBeSortedByTheirFnKind() {
    byte[] rawAbi =
        baseFileAbiBuilder("functions_are_not_ordered_properly_by_fn_kind.abi")
            .addFnAbi(externalEventFn("receiveEvent"))
            .addFnAbi(
                secretInputFn("add_bid")
                    .shortname("41")
                    .secretArgument("bid", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ZK_ON_EXTERNAL_EVENT(24) with shortname 9df2b95b appears"
                + " before ZK_ON_SECRET_INPUT(23) with shortname 41");
  }

  /** When the init function is sorted after an action, the parser fails. */
  @Test
  void whenInitIsSortedAfterActionTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilderWithoutInit("init_after_action.abi")
            .addFnAbi(actionFn("foo").shortname("01"))
            .addFnAbi(initFn())
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ACTION(2) with shortname 01 appears before INIT(1) with"
                + " shortname ffffffff0f");
  }

  /** When an action function is sorted after a callback, the parser fails. */
  @Test
  void whenActionIsSortedAfterCallbackTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("action_after_callback.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.CALLBACK)
                    .name("callback")
                    .shortname("02"))
            .addFnAbi(actionFn("foo").shortname("01"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but CALLBACK(3) with shortname 02 appears before ACTION(2) with"
                + " shortname 01");
  }

  /** When callback is sorted after secret input, the parser fails. */
  @Test
  void whenCallbackIsSortedAfterSecretInputTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("callback_after_secret_input.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT)
                    .name("secret")
                    .shortname("02")
                    .secretArgument("input", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.CALLBACK)
                    .name("callback")
                    .shortname("03"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ZK_ON_SECRET_INPUT(23) with shortname 02 appears before"
                + " CALLBACK(3) with shortname 03");
  }

  /** When a var inputted is sorted after a var rejected, the parser fails. */
  @Test
  void whenVarInputtedIsSortedAfterVarRejectedTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("inputted_after_rejected.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_REJECTED)
                    .name("reject")
                    .shortname("03"))
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_INPUTTED)
                    .name("input")
                    .shortname("03"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ZK_ON_VARIABLE_REJECTED(18) with shortname 03 appears"
                + " before ZK_ON_VARIABLE_INPUTTED(17) with shortname 03");
  }

  /** When compute complete is sorted after a var opened, the parser fails. */
  @Test
  void whenComputeCompleteIsSortedAfterVarOpenedTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("complete_after_opened.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLES_OPENED)
                    .name("opened")
                    .shortname("03"))
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_COMPUTE_COMPLETE)
                    .name("complete")
                    .shortname("03"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ZK_ON_VARIABLES_OPENED(20) with shortname 03 appears before"
                + " ZK_ON_COMPUTE_COMPLETE(19) with shortname 03");
  }

  /** When user var opened is sorted after attestation complete, the parser fails. */
  @Test
  void whenUserVarOpenedIsSortedAfterAttestationCompleteTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("user_var_after_attestation.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_ATTESTATION_COMPLETE)
                    .name("complete")
                    .shortname("03"))
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_USER_VARIABLES_OPENED)
                    .name("userVar")
                    .shortname("03"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ZK_ON_ATTESTATION_COMPLETE(22) with shortname 03 appears"
                + " before ZK_ON_USER_VARIABLES_OPENED(21) with shortname 03");
  }

  /** When functions are sorted by their shortnames first and kind id second, the parser fails. */
  @Test
  void functionsMustBeOrderedByTheirKindIdAscendingEvenWhenTheirShortnamesAreNot() {
    byte[] rawAbi =
        baseFileAbiBuilder("fn_sorted_by_shortname_first.abi")
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.CALLBACK)
                    .name("callback")
                    .shortname("01"))
            .addFnAbi(
                new FnAbiBuilder()
                    .kind(ImplicitBinderAbi.DefaultKinds.ACTION)
                    .name("action")
                    .shortname("02"))
            .build();
    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but CALLBACK(3) with shortname 01 appears before ACTION(2) with"
                + " shortname 02");
  }

  /**
   * When an ABI has two functions with the same FnKind identifier, they must be sorted by the
   * shortname.
   */
  @Test
  void functionsMustBeSortedByTheirShortnameWhenTheyHaveTheSameFnKind() {
    byte[] rawAbi =
        baseFileAbiBuilder("functions_are_not_ordered_properly_by_their_shortname.abi")
            .addFnAbi(actionFn("secondAction").shortname("02"))
            .addFnAbi(actionFn("firstAction").shortname("01"))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ACTION(2) with shortname 02 appears before ACTION(2) with"
                + " shortname 01");
  }

  /**
   * When an ABI has two functions that are ordered but not strictly ascending, i.e. they have the
   * same FnKind and shortname, the parser throws an exception.
   */
  @Test
  void functionOrderMustBeStrictlyAscending() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_order_is_not_strictly_ascending.abi")
            .addFnAbi(actionFn("secondAction").shortname("42"))
            .addFnAbi(actionFn("firstAction").shortname("42"))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but ACTION(2) with shortname 42 appears before ACTION(2) with"
                + " shortname 42");
  }

  /**
   * When an ABI has types in its type list that are not reachable from either the state type or any
   * argument types an error is thrown.
   */
  @Test
  void allTypesInNamedTypesListMustBeReachable() {
    StructTypeSpec unreachable = new StructTypeSpec("Unreachable", List.of());
    byte[] rawAbi =
        baseFileAbiBuilder("a_single_unreachable_named_type.abi").addNamedType(unreachable).build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Named Types list contained unreachable type(s) [Unreachable]");
  }

  /**
   * When the parser reports that come types are unreachable, the list is the difference between all
   * types and visited types.
   */
  @Test
  void unreachableTypesIsTheDifferenceBetweenAllTypesAndVisitedTypes() {
    byte[] rawAbi =
        baseFileAbiBuilder("multiple_unreachables.abi")
            .addNamedType(structType("State"))
            .addNamedType(structType("First"))
            .addNamedType(structType("Second"))
            .stateType(new NamedTypeRef(0))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Named Types list contained unreachable type(s) [First, Second]");
  }

  /**
   * Given a struct, where the fields of the struct contains named types, the abi can parse if the
   * types are ordered depth-first.
   *
   * <p>For example, given State {a: A, b: B} where A {c: C}, the types are ordered as [State, A, C,
   * B].
   */
  @Test
  void whenNamedTypesInStructFieldsAreOrderedDepthFirstTheAbiParses() {
    StructTypeBuilder state =
        structType("State").addField("a", new NamedTypeRef(1)).addField("b", new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("depth_first_ordered_struct_fields.abi").addNamedType(state));
  }

  private void validateStateWithThreeStructs(FileAbiBuilder state) {
    byte[] rawAbi =
        state
            .addNamedType(structType("A").addField("c", new NamedTypeRef(2)))
            .addNamedType(structType("C"))
            .addNamedType(structType("B"))
            .stateType(new NamedTypeRef(0))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("A");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("C");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(3)).name()).isEqualTo("B");
  }

  /**
   * Given a struct, where the fields of the struct contains named types, the abi can parse if the
   * types are ordered depth-first.
   *
   * <p>For example, given State {a: A, b: B} where A {c: C}, with types are ordered as [State, A,
   * B, C] parsing will fail.
   */
  @Test
  void whenNamedTypesAreNotOrderedDepthFirstInStructTheParserThrowsException() {
    byte[] rawAbi =
        baseFileAbiBuilder("unordered_struct_fields.abi")
            .addNamedType(
                structType("State")
                    .addField("a", new NamedTypeRef(1))
                    .addField("b", new NamedTypeRef(2)))
            .addNamedType(structType("A").addField("c", new NamedTypeRef(3)))
            .addNamedType(structType("B"))
            .addNamedType(structType("C"))
            .stateType(new NamedTypeRef(0))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected named type 'C' to have index 2 but was 3");
  }

  /**
   * Given an enum with variant structs, when the named types are ordered depth-first, the abi
   * parses.
   *
   * <p>For example given StateEnum {0: A, 1: B} where A is {c: C}, with types ordered as
   * [StateEnum, A, C, B] will parse.
   */
  @Test
  void whenNamedTypesInEnumVariantsAreOrderedDepthFirstTheAbiParses() {
    validateStateWithThreeStructs(
        baseFileAbiBuilder("depth_first_ordered_enum_variants.abi")
            .addNamedType(
                enumType("EnumState")
                    .addVariant(0, new NamedTypeRef(1))
                    .addVariant(1, new NamedTypeRef(3))));
  }

  /**
   * Given an enum with variant structs, when the named types are ordered depth-first, the abi
   * parses.
   *
   * <p>For example given StateEnum {0: A, 1: B} where A is {c: C}, with types ordered as
   * [StateEnum, A, B, C] parsing will fail.
   */
  @Test
  void whenNamedTypesAreNotOrderedDepthFirstInEnumTheParserThrowsException() {
    byte[] rawAbi =
        baseFileAbiBuilder("unordered_enum_variants.abi")
            .addNamedType(
                enumType("EnumState")
                    .addVariant(0, new NamedTypeRef(1))
                    .addVariant(1, new NamedTypeRef(2)))
            .addNamedType(structType("A").addField("c", new NamedTypeRef(3)))
            .addNamedType(structType("B"))
            .addNamedType(structType("C"))
            .stateType(new NamedTypeRef(0))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected named type 'C' to have index 2 but was 3");
  }

  /**
   * Given functions with named types arguments, the abi parses when the arguments are ordered as
   * they appear.
   *
   * <p>For example, given functions A with argument B and C, and function D with arguments E and B,
   * where the types are ordered [B, C, E] the abi parses.
   */
  @Test
  void whenNamedTypesInFunctionArgumentsAreOrderedTheAbiParses() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_arguments_ordered.abi")
            .addFnAbi(
                actionFn("fn_a")
                    .shortname("01")
                    .addArgument("b", new NamedTypeRef(0))
                    .addArgument("c", new NamedTypeRef(1)))
            .addFnAbi(
                actionFn("fn_d")
                    .shortname("02")
                    .addArgument("e", new NamedTypeRef(2))
                    .addArgument("b", new NamedTypeRef(0)))
            .addNamedType(structType("B"))
            .addNamedType(structType("C"))
            .addNamedType(structType("E"))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("B");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("C");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("E");
  }

  /**
   * Given functions with named types arguments, the abi parses when the arguments are ordered as
   * they appear.
   *
   * <p>For example, given functions A with argument B and C, and function D with arguments E and B,
   * where the types are ordered [E, B, C] the abi parses.
   */
  @Test
  void whenNamedTypesInFunctionArgumentsAreNotOrderedTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_arguments_unordered.abi")
            .addFnAbi(
                actionFn("fn_a")
                    .shortname("01")
                    .addArgument("b", new NamedTypeRef(1))
                    .addArgument("c", new NamedTypeRef(2)))
            .addFnAbi(
                actionFn("fn_d")
                    .shortname("02")
                    .addArgument("e", new NamedTypeRef(0))
                    .addArgument("b", new NamedTypeRef(1)))
            .addNamedType(structType("E"))
            .addNamedType(structType("B"))
            .addNamedType(structType("C"))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected named type 'B' to have index 0 but was 1");
  }

  /**
   * Given a function that takes a secret argument, the abi parses when the type of the secret
   * argument is ordered after the other types.
   *
   * <p>For example, given function A that takes arguments B, C, D where D is secret the abi parses
   * with the types are ordered [B, C, D].
   */
  @Test
  void whenTypeOfSecretArgumentIsOrderedAfterOtherArgumentsTheAbiParses() {
    byte[] rawAbi =
        baseFileAbiBuilder("secret_argument_ordered.abi")
            .addFnAbi(
                secretInputFn("fn_a")
                    .addArgument("b", new NamedTypeRef(0))
                    .addArgument("c", new NamedTypeRef(1))
                    .secretArgument("d", new NamedTypeRef(2)))
            .addNamedType(structType("B"))
            .addNamedType(structType("C"))
            .addNamedType(structType("D"))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("B");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("C");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("D");
  }

  /**
   * Given a function that takes a secret argument, the abi parses when the type of the secret
   * argument is ordered after the other types.
   *
   * <p>For example, given function A that takes arguments B, C, D where D is secret the abi fails
   * to parse when the types are ordered [B, D, C].
   */
  @Test
  void whenTypeOfSecretArgumentIsNotOrderedAfterOtherArgumentsTheParserFails() {
    byte[] rawAbi =
        baseFileAbiBuilder("secret_argument_unordered.abi")
            .addFnAbi(
                secretInputFn("fn_a")
                    .addArgument("b", new NamedTypeRef(0))
                    .secretArgument("d", new NamedTypeRef(1))
                    .addArgument("c", new NamedTypeRef(2)))
            .addNamedType(structType("B"))
            .addNamedType(structType("D"))
            .addNamedType(structType("C"))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Expected named type 'C' to have index 1 but was 2");
  }

  /**
   * Given more than one function that takes a secret argument, when the secret argument is ordered
   * after the regular arguments and before the arguments of the next function, the abi parses.
   *
   * <p>For example given functions A, and B, where A takes arguments a1, a2 and a_secret. and B
   * takes arguments b1 and b_secret, when the types are ordered [a1, a2, a_secret, b1, b_secret]
   * the abi parses.
   */
  @Test
  void secretArgumentsAreOrderedBeforeNextFunctionsArguments() {
    byte[] rawAbi =
        baseFileAbiBuilder("multiple_ordered_secret_arguments.abi")
            .addFnAbi(
                secretInputFn("fn_a")
                    .shortname("01")
                    .addArgument("a1", new NamedTypeRef(0))
                    .addArgument("a2", new NamedTypeRef(1))
                    .secretArgument("a_secret", new NamedTypeRef(2)))
            .addFnAbi(
                secretInputFn("fn_b")
                    .shortname("02")
                    .addArgument("b1", new NamedTypeRef(3))
                    .secretArgument("b_secret", new NamedTypeRef(4)))
            .addNamedType(structType("A1"))
            .addNamedType(structType("A2"))
            .addNamedType(structType("SecretA"))
            .addNamedType(structType("B1"))
            .addNamedType(structType("SecretB"))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("A1");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("A2");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("SecretA");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(3)).name()).isEqualTo("B1");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(4)).name()).isEqualTo("SecretB");
  }

  /**
   * Given a function with an argument with a simple type, the order of the arguments with named
   * types are not changed.
   *
   * <p>For example given function A with arguments B, u32 and C, the abi parses when the named
   * types have order [B, C]
   */
  @Test
  void functionWithSimpleTypeArgumentDoesNotChangeOrderOfNamedTypesArguments() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_arguments_ordered_with_simple_type.abi")
            .addFnAbi(
                actionFn("my_func")
                    .addArgument("a", new NamedTypeRef(0))
                    .addArgument("simple", new SimpleTypeSpec(TypeSpec.TypeIndex.u32))
                    .addArgument("b", new NamedTypeRef(1)))
            .addNamedType(structType("A"))
            .addNamedType(structType("B"))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("A");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("B");
  }

  /**
   * Given a function with a secret argument with a simple type, the order of the arguments with
   * named types are not changed.
   *
   * <p>For example given function A with arguments A1 and secret u8, and function B with arguments
   * B1 and SecretB the abi parses when the types have order [A1, B1, SecretB]
   */
  @Test
  void functionWithSimpleTypeSecretArgumentDoesNotChangeOrderOfNamedTypesArguments() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_secret_arguments_ordered_with_simple_type.abi")
            .addFnAbi(
                secretInputFn("first")
                    .shortname("01")
                    .addArgument("a", new NamedTypeRef(0))
                    .secretArgument("secret", new SimpleTypeSpec(TypeSpec.TypeIndex.u32)))
            .addFnAbi(
                secretInputFn("second")
                    .shortname("02")
                    .addArgument("b", new NamedTypeRef(1))
                    .secretArgument("secret", new NamedTypeRef(2)))
            .addNamedType(structType("A1"))
            .addNamedType(structType("B1"))
            .addNamedType(structType("SecretB"))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("A1");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("B1");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("SecretB");
  }

  /**
   * Given a state with a generic Vec field, when the type of the generic argument is depth-first
   * before any following fields, the abi parses.
   *
   * <p>For example given State {a: Vec of A, B} where A is {c: C} the abi parses when the types are
   * ordered [A, C, B]
   */
  @Test
  void genericArgumentInVecIsOrderedDepthFirstBeforeFollowingFields() {
    StructTypeBuilder state =
        structType("State")
            .addField("a", vecType(new NamedTypeRef(1)))
            .addField("b", new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("vec_type_argument_ordered_depth_first.abi").addNamedType(state));
  }

  /**
   * Given a state with a generic Map field, when the type of the generic arguments is depth-first
   * before any following fields, the abi parses.
   *
   * <p>For example given State {a: Map from A to B} where A is {c: C} the abi parses when the types
   * are ordered [A, C, B]
   */
  @Test
  void genericArgumentsInMapIsOrderedDepthFirstInTheOrderTheyAppear() {
    validateStateWithThreeStructs(
        baseFileAbiBuilder("map_type_arguments_ordered_depth_first.abi")
            .addNamedType(
                structType("State")
                    .addField("a", mapType(new NamedTypeRef(1), new NamedTypeRef(3)))));
  }

  /**
   * Given a state with a generic Set field, when the type of the generic argument is depth-first
   * before any following fields, the abi parses.
   *
   * <p>For example given State {a: Set of A, B} where A is {c: C} the abi parses when the types are
   * ordered [A, C, B]
   */
  @Test
  void genericArgumentInSetIsOrderedDepthFirstBeforeFollowingFields() {
    StructTypeBuilder state =
        structType("State")
            .addField("a", new SetTypeSpec(new NamedTypeRef(1)))
            .addField("b", new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("set_type_argument_ordered_depth_first.abi").addNamedType(state));
  }

  /**
   * Given a state with a generic Option field, when the type of the generic argument is depth-first
   * before any following fields, the abi parses.
   *
   * <p>For example given State {a: Option of A, B} where A is {c: C} the abi parses when the types
   * are ordered [A, C, B]
   */
  @Test
  void genericArgumentInOptionIsOrderedDepthFirstBeforeFollowingFields() {
    StructTypeBuilder state =
        structType("State")
            .addField("a", new OptionTypeSpec(new NamedTypeRef(1)))
            .addField("b", new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("option_type_argument_ordered_depth_first.abi").addNamedType(state));
  }

  /**
   * Given a state with a generic AvlTreeMap field, when the type of the generic arguments is
   * depth-first before any following fields, the abi parses.
   */
  @Test
  void genericArgumentsInAvlTreeMapIsOrderedDepthFirstInTheOrderTheyAppear() {
    AvlTreeMapTypeSpec avlTreeMap =
        new AvlTreeMapTypeSpec(new NamedTypeRef(1), new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("avl_tree_map_type_arguments_ordered_depth_first.abi")
            .addNamedType(structType("State").addField("a", avlTreeMap)));
  }

  /**
   * Given a state with a generic Map field, when the type of the generic arguments is depth-first
   * before any following fields, the abi parses.
   *
   * <p>For example given State {a: [A; size], b: B} where A is {c: C} the abi parses when the types
   * are ordered [A, C, B]
   */
  @Test
  void genericArgumentSizedArrayTypeIsOrderedDepthFirst() {
    TypeSpec valueType = new NamedTypeRef(1);
    StructTypeBuilder state =
        structType("State")
            .addField("a", new SizedArrayTypeSpec(valueType, 4))
            .addField("b", new NamedTypeRef(3));
    validateStateWithThreeStructs(
        baseFileAbiBuilder("sized_array_argument_ordered_depth_first.abi").addNamedType(state));
  }

  /**
   * Given a field with simple type in a struct, the order of the other named types is unchanged and
   * abi parses.
   *
   * <p>For example given State {A, u32, B}, the abi parses when types have order [State, A, B]
   */
  @Test
  void whenStructHasSimpleTypeAsFieldOrderOfOtherNamedTypesIsUnchanged() {
    byte[] rawAbi =
        baseFileAbiBuilder("simple_types_in_struct.abi")
            .addNamedType(
                structType("State")
                    .addField("a", new NamedTypeRef(1))
                    .addField("simple", new SimpleTypeSpec(TypeSpec.TypeIndex.Address))
                    .addField("b", new NamedTypeRef(2)))
            .addNamedType(structType("A"))
            .addNamedType(structType("B"))
            .stateType(new NamedTypeRef(0))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("A");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("B");
  }

  /**
   * Given full ABI with both named types in state and function arguments, the abi parses when the
   * function arguments are ordered after the state types.
   *
   * <p>For example, given State {A, B} and function {C, B}, the abi parses when the types are
   * ordered {A, B, C}.
   */
  @Test
  void typesOfFunctionArgumentsAreOrderedAfterTheTypesOfStateFields() {
    byte[] rawAbi =
        baseFileAbiBuilder("state_and_function_types.abi")
            .addNamedType(
                structType("State")
                    .addField("a", new NamedTypeRef(1))
                    .addField("b", new NamedTypeRef(2)))
            .addNamedType(structType("A"))
            .addNamedType(structType("B"))
            .addFnAbi(
                actionFn("my_action")
                    .shortname("42")
                    .addArgument("c", new NamedTypeRef(3))
                    .addArgument("b", new NamedTypeRef(2)))
            .addNamedType(structType("C"))
            .stateType(new NamedTypeRef(0))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("A");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("B");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(3)).name()).isEqualTo("C");
  }

  /**
   * Given a contract with action with a generic vec argument, the order of named types is depth
   * first before any following arguments.
   */
  @Test
  void functionWithGenericArgumentParses() {
    byte[] rawAbi =
        baseFileAbiBuilder("function_with_generic_argument.abi")
            .addNamedType(structType("State"))
            .addNamedType(structType("A"))
            .addNamedType(structType("B"))
            .addFnAbi(
                actionFn("my_action")
                    .shortname("42")
                    .addArgument("a", new VecTypeSpec(new NamedTypeRef(1)))
                    .addArgument("b", new NamedTypeRef(2)))
            .stateType(new NamedTypeRef(0))
            .build();

    ContractAbi parsedContract = new AbiParser(rawAbi).parseContractAbi();

    assertThat(parsedContract.getNamedType(new NamedTypeRef(0)).name()).isEqualTo("State");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(1)).name()).isEqualTo("A");
    assertThat(parsedContract.getNamedType(new NamedTypeRef(2)).name()).isEqualTo("B");
  }

  /**
   * If a binder invocation with a lower kind id comes after a binder invocation with a higher kind
   * id, an error is thrown.
   */
  @Test
  void binderInvocationNotSortedByKind() {
    byte[] rawAbi =
        baseFileAbiBuilder("binder_action_after_callback.abi")
            .clientVersion(new AbiVersion(6, 0, 0))
            .addFunctionKind(ImplicitBinderAbi.DefaultKinds.INIT)
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("callback")
                    .kind(TransactionKind.Callback)
                    .shortname((byte) 1))
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("action")
                    .kind(TransactionKind.Action)
                    .shortname((byte) 2))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "BinderInvocations are unordered: Must be ordered by invocation kind and by shortname"
                + " in ascending order but Callback(3) with shortname 1 appears before Action(2)"
                + " with shortname 2");
  }

  /**
   * If two binder invocations have the same kind then they must be sorted by their shortname,
   * otherwise an error is thrown.
   */
  @Test
  void binderInvocationNotSortedByShortname() {
    byte[] rawAbi =
        baseFileAbiBuilder("binder_shortname_wrong_order.abi")
            .clientVersion(new AbiVersion(6, 0, 0))
            .addFunctionKind(ImplicitBinderAbi.DefaultKinds.INIT)
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("action2")
                    .kind(TransactionKind.Action)
                    .shortname((byte) 2))
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("action1")
                    .kind(TransactionKind.Action)
                    .shortname((byte) 1))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "BinderInvocations are unordered: Must be ordered by invocation kind and by shortname"
                + " in ascending order but Action(2) with shortname 2 appears before Action(2) with"
                + " shortname 1");
  }

  /**
   * When an ABI has two binder invocations that are ordered but not strictly ascending, i.e. they
   * have the same TransactionKind and shortname, the parser throws an exception.
   */
  @Test
  void binderInvocationOrderMustBeStrictlyAscending() {
    byte[] rawAbi =
        baseFileAbiBuilderWithoutInit("binder_invocation_order_is_not_strictly_ascending.abi")
            .clientVersion(new AbiVersion(6, 0, 0))
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .kind(TransactionKind.Action)
                    .name("secondAction")
                    .shortname((byte) 42))
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .kind(TransactionKind.Action)
                    .name("firstAction")
                    .shortname((byte) 42))
            .build();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "BinderInvocations are unordered: Must be ordered by invocation kind and by shortname"
                + " in ascending order but Action(2) with shortname 42 appears before Action(2)"
                + " with shortname 42");
  }

  /** When a binder abi's binder invocations are not sorted, an error is thrown. */
  @Test
  void binderInvocationNotSortedByKindBinderAbi() {
    byte[] rawAbi =
        baseFileAbiBuilderWithoutInit("binder_action_after_callback_binder_abi.abi")
            .clientVersion(new AbiVersion(6, 0, 0))
            .addFunctionKind(ImplicitBinderAbi.DefaultKinds.INIT)
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("callback")
                    .kind(TransactionKind.Callback)
                    .shortname((byte) 1))
            .addBinderInvocation(
                new BinderInvocationBuilder()
                    .name("action")
                    .kind(TransactionKind.Action)
                    .shortname((byte) 2))
            .buildBinderAbi();

    AbiParser abiParser = new AbiParser(rawAbi);
    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage(
            "BinderInvocations are unordered: Must be ordered by invocation kind and by shortname"
                + " in ascending order but Callback(3) with shortname 1 appears before Action(2)"
                + " with shortname 2");
  }

  /**
   * When a binder ABI has types in its type list that are not reachable from either the state type
   * or any argument types an error is thrown.
   */
  @Test
  void allTypesInNamedTypesListMustBeReachableBinderAbi() {
    StructTypeSpec unreachable = new StructTypeSpec("Unreachable", List.of());
    byte[] rawAbi =
        baseFileAbiBuilderWithoutInit("a_single_unreachable_named_type_binder_abi.abi")
            .clientVersion(new AbiVersion(6, 0, 0))
            .addBinderInvocation(
                new BinderInvocationBuilder().kind(TransactionKind.Init).name("init"))
            .addNamedType(unreachable)
            .buildBinderAbi();

    AbiParser abiParser = new AbiParser(rawAbi);

    assertThatThrownBy(abiParser::parseAbi)
        .isInstanceOf(AbiParseException.class)
        .hasMessage("Named Types list contained unreachable type(s) [Unreachable]");
  }

  private FileAbiBuilder baseFileAbiBuilder(String name) {
    return baseFileAbiBuilderWithoutInit(name).addFnAbi(initFn());
  }

  private FileAbiBuilder baseFileAbiBuilderWithoutInit(String name) {
    return new FileAbiBuilder(name)
        .binderVersion(version)
        .clientVersion(version)
        .stateType(new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
  }

  private FnAbiBuilder initFn() {
    return new FnAbiBuilder()
        .kind(ImplicitBinderAbi.DefaultKinds.INIT)
        .name("initialize")
        .shortname(HexFormat.of().parseHex("ffffffff0f"));
  }

  private FnAbiBuilder actionFn(String name) {
    return new FnAbiBuilder().name(name).kind(ImplicitBinderAbi.DefaultKinds.ACTION);
  }

  private FnAbiBuilder externalEventFn(String name) {
    return new FnAbiBuilder().name(name).kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_EXTERNAL_EVENT);
  }

  private FnAbiBuilder secretInputFn(String name) {
    return new FnAbiBuilder()
        .name(name)
        .kind(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE);
  }

  private StructTypeBuilder structType(String name) {
    return new StructTypeBuilder().name(name);
  }

  private EnumTypeBuilder enumType(String name) {
    return new EnumTypeBuilder().name(name);
  }

  private VecTypeSpec vecType(TypeSpec valueType) {
    return new VecTypeSpec(valueType);
  }

  private MapTypeSpec mapType(TypeSpec keyType, TypeSpec valueType) {
    return new MapTypeSpec(keyType, valueType);
  }
}
