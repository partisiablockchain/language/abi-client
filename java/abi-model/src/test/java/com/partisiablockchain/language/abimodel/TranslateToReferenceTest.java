package com.partisiablockchain.language.abimodel;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Parse all abi files in reference-tests/abi-files to FileAbis and compare the pretty printed model
 * to a reference file located in reference-tests/reference-files. All abi files can be compiled
 * using the `generate-abi-for-test.sh` script.
 */
final class TranslateToReferenceTest {

  @ParameterizedTest
  @MethodSource("provideTestCases")
  public void translateAbis(final ExampleAbis.TestCase testCase) {
    final Path rsFilePath = testCase.filePath();

    final byte[] abiBytes =
        WithResource.apply(
            () -> new FileInputStream(rsFilePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + rsFilePath);

    final AbiParser parser = new AbiParser(abiBytes, AbiParser.Strictness.LENIENT);
    final FileAbi fileAbi = parser.parseAbi();

    final String model = AbiParser.printModel(fileAbi);

    // Replace version as we want to test across different client versions
    final String versionRemoved =
        model
            .replaceFirst("//\\sVersion\\sBinder:.*", "// Version Binder:")
            .replaceFirst("//\\sVersion\\sClient:.*", "// Version Client:");

    // Remove Signature and SecretVarId
    final String extraStructsRemoved =
        versionRemoved
            .replaceFirst("pub struct SecretVarId \\{\n\\s*raw_id: u32,\n\\s*}\n\\s*", "")
            .replaceFirst(
                "pub struct Signature \\{\n"
                    + "\\s*recovery_id: u8,\n"
                    + "\\s*value_r: \\[u8; 32],\n"
                    + "\\s*value_s: \\[u8; 32],\n"
                    + "\\s*}\n"
                    + "\\s*",
                "");

    String nameOfAbiFile = getNameOfAbiFile(testCase);

    final Path referencePath =
        getResourceSourcePath(
            TranslateToReferenceTest.class,
            "reference-tests/reference-files/" + nameOfAbiFile + ".model");
    final File referenceFile = referencePath.toFile();

    if (!referenceFile.exists()) {
      writeText(referencePath, extraStructsRemoved);
      System.err.println("Regenerated " + referencePath);
    }

    final String modelExpected = loadText(referencePath);

    assertThat(extraStructsRemoved)
        .describedAs(testCase.filename())
        .isEqualToNormalizingNewlines(modelExpected);
  }

  private static String getNameOfAbiFile(ExampleAbis.TestCase testCase) {
    String contractName = testCase.contractName();
    String filePathString = testCase.filePath().toString();
    int startingIndexOfContractNameSubString = filePathString.indexOf(contractName);
    return filePathString.substring(startingIndexOfContractNameSubString);
  }

  static Stream<Arguments> provideTestCases() {
    return ExampleAbis.abis().stream().map(Arguments::of);
  }

  public static Path getResourceSourcePath(final Class<?> clazz, final String path) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(path);
  }

  public static void writeText(final Path filePath, final String textWithUnixNewline) {
    final String text = textWithUnixNewline.replace("\n", System.lineSeparator());

    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  public static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }
}
