package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.BinaryAbiWriter;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Helper class for building and caching test ABI files. */
public final class FileAbiBuilder {
  private static final Logger logger = LoggerFactory.getLogger(FileAbiBuilder.class);

  private final String abiFileName;

  private AbiVersion binderVersion;
  private AbiVersion clientVersion;
  private final List<ContractInvocationKind> contractInvocationKinds = new ArrayList<>();
  private final List<NamedTypeSpec> namedTypes = new ArrayList<>();
  private final List<BinderInvocation> binderInvocations = new ArrayList<>();
  private final List<ContractInvocation> functions = new ArrayList<>();
  private TypeSpec stateType;

  /**
   * Create new abi builder.
   *
   * @param abiFileName name of the abi file which will be created
   */
  public FileAbiBuilder(String abiFileName) {
    this.abiFileName = abiFileName;
  }

  /**
   * Set binder version.
   *
   * @param binderVersion binder version
   * @return this builder
   */
  public FileAbiBuilder binderVersion(AbiVersion binderVersion) {
    this.binderVersion = binderVersion;
    return this;
  }

  /**
   * Set abi version.
   *
   * @param clientVersion abi version
   * @return this builder
   */
  public FileAbiBuilder clientVersion(AbiVersion clientVersion) {
    this.clientVersion = clientVersion;
    return this;
  }

  /**
   * Add a ContractInvocation.
   *
   * @param builder the builder for a ContractInvocation
   * @return this builder
   */
  public FileAbiBuilder addFnAbi(FnAbiBuilder builder) {
    functions.add(builder.build());
    return this;
  }

  /**
   * Add a named type.
   *
   * @param type the named type
   * @return this builder
   */
  public FileAbiBuilder addNamedType(NamedTypeSpec type) {
    namedTypes.add(type);
    return this;
  }

  /**
   * Add a named struct type.
   *
   * @param builder builder for a struct type
   * @return this builder
   */
  public FileAbiBuilder addNamedType(StructTypeBuilder builder) {
    namedTypes.add(builder.build());
    return this;
  }

  /**
   * Add a named enum type.
   *
   * @param builder builder for an enum type
   * @return this builder
   */
  public FileAbiBuilder addNamedType(EnumTypeBuilder builder) {
    namedTypes.add(builder.build());
    return this;
  }

  /**
   * Set the state type.
   *
   * @param stateType state type
   * @return this builder
   */
  public FileAbiBuilder stateType(TypeSpec stateType) {
    this.stateType = stateType;
    return this;
  }

  /**
   * Add a BinderInvocation.
   *
   * @param builder the builder for a BinderInvocation
   * @return this builder
   */
  public FileAbiBuilder addBinderInvocation(BinderInvocationBuilder builder) {
    binderInvocations.add(builder.build());
    return this;
  }

  /**
   * Add a ContractInvocationKind.
   *
   * @param kind function kind
   * @return this builder
   */
  public FileAbiBuilder addFunctionKind(ContractInvocationKind kind) {
    contractInvocationKinds.add(kind);
    return this;
  }

  /**
   * Build and cache the test ABI. If the cached file already exists, ensure the file matches the
   * expected ABI.
   *
   * @return raw abi bytes
   */
  public byte[] build() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            contractInvocationKinds,
            namedTypes,
            binderInvocations,
            functions,
            stateType);
    FileAbi fileAbi = new FileAbi("PBCABI", binderVersion, clientVersion, null, contractAbi);
    byte[] rawAbi = BinaryAbiWriter.serialize(fileAbi);

    Path path = Paths.get("src", "test", "resources", "testdata", "cached-references");
    Path abiFilePath = path.resolve(abiFileName);
    if (Files.exists(abiFilePath)) {
      assertReferenceFileMatchExpectation(abiFilePath, rawAbi);
    } else {
      cacheFileIfNotInCi(abiFilePath, rawAbi);
    }
    return rawAbi;
  }

  /**
   * Build and cache the test binder ABI. If the cached file already exists, ensure the file matches
   * the expected ABI.
   *
   * @return raw abi bytes
   */
  public byte[] buildBinderAbi() {
    BinderAbi binderAbi =
        new BinderAbi(
            BinderType.Public, contractInvocationKinds, namedTypes, binderInvocations, stateType);
    FileAbi fileAbi = new FileAbi("PBCABI", binderVersion, clientVersion, null, binderAbi);
    byte[] rawAbi = BinaryAbiWriter.serialize(fileAbi);

    Path path = Paths.get("src", "test", "resources", "testdata", "cached-references");
    Path abiFilePath = path.resolve(abiFileName);
    if (Files.exists(abiFilePath)) {
      assertReferenceFileMatchExpectation(abiFilePath, rawAbi);
    } else {
      cacheFileIfNotInCi(abiFilePath, rawAbi);
    }
    return rawAbi;
  }

  private void assertReferenceFileMatchExpectation(Path abiFilePath, byte[] rawAbi) {
    logger.info(
        "Test ABI {} already exists, checking reference file matches expected file", abiFileName);
    byte[] cachedAbi =
        ExceptionConverter.call(
            () -> Files.readAllBytes(abiFilePath), "Failed to read " + abiFileName);
    Assertions.assertThat(cachedAbi)
        .as("Reference abi file '" + abiFileName + "'")
        .isEqualTo(rawAbi);
  }

  private void cacheFileIfNotInCi(Path filepath, byte[] content) {
    if (System.getenv("CI") == null) {
      logger.info("Test ABI {} was not found, caching reference file", abiFileName);
      ExceptionConverter.run(
          () -> Files.write(filepath, content), "Failed to write " + abiFileName);
    } else {
      throw new RuntimeException("Expected cached abi " + abiFileName + " to exist");
    }
  }
}
