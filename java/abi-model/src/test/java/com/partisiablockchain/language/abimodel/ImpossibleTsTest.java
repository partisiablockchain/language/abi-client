package com.partisiablockchain.language.abimodel;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.commontests.TestingHelper;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ImpossibleTsTest {

  @Test
  void complexType() {
    assertThatThrownBy(() -> new SimpleTypeSpec(TypeSpec.TypeIndex.Set))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Given type is not a simple type: Set");
  }

  @Test
  void invalidIdentifiersTest() {
    assertThat(AbiParser.isIdentifierValid("")).isFalse();
    assertThat(AbiParser.isIdentifierValid("-a1")).isFalse();
    assertThat(AbiParser.isIdentifierValid("a.2")).isFalse();
    assertThat(AbiParser.isIdentifierValid("a3.")).isFalse();
  }

  @Test
  void parseSimilarlyNamedTypesInDifferentScopes() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_same_names_different_scopes.abi"))
                .readAllBytes());
    FileAbi fileAbi = parser.parseAbi();
    assertThat(fileAbi.chainComponent().namedTypes().size()).isEqualTo(14);
  }

  @Test
  void parseDuplicateFunctionName() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_function_name.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi)
        .hasMessage("Duplicate function name encountered: initialize");
  }

  @Test
  void parseDuplicateStructFieldName() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_struct_field_name.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi).hasMessage("Duplicate field name encountered: hash");
  }

  @Test
  void parseDuplicateFunctionArgument() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_function_argument.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi).hasMessage("Duplicate argument name encountered: my_bool");
  }

  @Test
  void parseDuplicateStructName() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_struct.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi)
        .hasMessage("Duplicate named type encountered: ReturnEnvelope");
  }

  @Test
  void parseDuplicateEnumName() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_enum.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi).hasMessage("Duplicate named type encountered: Vehicle");
  }

  @Test
  void parseDuplicateStructInEnum() throws IOException {
    var parser =
        TestingHelper.abiParserFromBytes(
            Files.newInputStream(
                    Path.of(
                        "src/test/resources/testdata/invalid_syntax_or_semantics/"
                            + "contract_duplicate_struct_in_enum.abi"))
                .readAllBytes());
    assertThatThrownBy(parser::parseAbi)
        .hasMessage("Duplicate named type encountered: ReturnEnvelope");
  }
}
