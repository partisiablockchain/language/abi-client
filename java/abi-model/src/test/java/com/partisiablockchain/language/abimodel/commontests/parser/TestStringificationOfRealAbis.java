package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.commontests.TestingHelper;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.DocumentationFunction;
import com.partisiablockchain.language.abimodel.model.DocumentationNamedType;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test of to string functions. */
public final class TestStringificationOfRealAbis {

  @Test
  void contractBooleans() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    var model = TestingHelper.loadAbiParserFromFile("contract_booleans.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.0.0\n"
                + "// Client Version: 1.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_bool: bool,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize (\n"
                + "    my_bool: bool,\n"
                + ")\n\n"
                + "#[action(shortname = 0xc67e90b0)]\n"
                + "pub fn update_my_bool (\n"
                + "    value: bool,\n"
                + ")\n\n");
  }

  @Test
  void contractSimpleMap() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
    var model = TestingHelper.loadAbiParserFromFile("contract_simple_map.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.0.0\n"
                + "// Client Version: 1.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_map: Map<Address, u8>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize ()\n\n"
                + "#[action(shortname = 0x282d08fc)]\n"
                + "pub fn insert_in_my_map (\n"
                + "    address: Address,\n"
                + "    value: u8,\n"
                + ")\n\n");
  }

  @Test
  void contractOptions() {
    var model = TestingHelper.loadAbiParserFromFile("contract_options.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.1.0\n"
                + "// Client Version: 2.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    option_u64: Option<u64>,\n"
                + "    option_string: Option<String>,\n"
                + "    option_address: Option<Address>,\n"
                + "    option_boolean: Option<bool>,\n"
                + "    option_map: Option<Map<u64, u64>>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize (\n"
                + "    option_u64: Option<u64>,\n"
                + "    option_string: Option<String>,\n"
                + "    option_address: Option<Address>,\n"
                + "    option_boolean: Option<bool>,\n"
                + ")\n\n"
                + "#[action(shortname = 0xbd3fce4f)]\n"
                + "pub fn update_u64 (\n"
                + "    value: Option<u64>,\n"
                + ")\n\n"
                + "#[action(shortname = 0x871a7573)]\n"
                + "pub fn update_string (\n"
                + "    value: Option<String>,\n"
                + ")\n\n"
                + "#[action(shortname = 0x1d2c55ac)]\n"
                + "pub fn update_address (\n"
                + "    value: Option<Address>,\n"
                + ")\n\n"
                + "#[action(shortname = 0x6b53c77c)]\n"
                + "pub fn update_boolean (\n"
                + "    value: Option<bool>,\n"
                + ")\n\n"
                + "#[action(shortname = 0xcc0ce5db)]\n"
                + "pub fn add_entry_to_map (\n"
                + "    key: u64,\n"
                + "    value: u64,\n"
                + ")\n\n");
  }

  @Test
  void contractSet() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-set
    var model = TestingHelper.loadAbiParserFromFile("contract_set.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.1.0\n"
                + "// Client Version: 2.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_set: Set<u64>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize ()\n\n"
                + "#[action(shortname = 0x4bfabd75)]\n"
                + "pub fn insert_in_my_set (\n"
                + "    value: u64,\n"
                + ")\n\n");
  }

  @Test
  void contractByteArrays() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    var model = TestingHelper.loadAbiParserFromFile("contract_byte_arrays.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.1.0\n"
                + "// Client Version: 2.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_array: [u8; 16],\n"
                + "    my_array_2: [u8; 5],\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize (\n"
                + "    my_array: [u8; 16],\n"
                + "    my_array_2: [u8; 5],\n"
                + ")\n\n"
                + "#[action(shortname = 0xdfb2f24b)]\n"
                + "pub fn update_my_array (\n"
                + "    value: [u8; 16],\n"
                + ")\n\n"
                + "#[action(shortname = 0x0ea9ceec)]\n"
                + "pub fn update_my_array_2 (\n"
                + "    value: [u8; 5],\n"
                + ")\n\n");
  }

  @Test
  void testStringifier() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
    var model = TestingHelper.loadAbiParserFromFile("contract_simple_vector.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 1.1.0\n"
                + "// Client Version: 2.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_vec: Vec<u64>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0x00000000)]\n"
                + "pub fn initialize (\n"
                + "    my_vec: Vec<u64>,\n"
                + ")\n\n"
                + "#[action(shortname = 0x1c998c9d)]\n"
                + "pub fn update_my_vec (\n"
                + "    value: Vec<u64>,\n"
                + ")\n\n");
  }

  @Test
  void contractStructOfStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct/src
    var model = TestingHelper.loadAbiParserFromFile("contract_struct_of_struct_v3.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 3.0.0\n"
                + "// Client Version: 3.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    my_other_struct: MyOtherStructType,\n"
                + "}\n\n"
                + "pub struct MyStructType {\n"
                + "    some_value: u64,\n"
                + "    some_vector: Vec<u64>,\n"
                + "}\n\n"
                + "pub struct MyOtherStructType {\n"
                + "    value: u64,\n"
                + "    vector: Vec<MyStructType>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0xffffffff0f)]\n"
                + "pub fn initialize (\n"
                + "    my_other_struct: MyOtherStructType,\n"
                + ")\n\n"
                + "#[action(shortname = 0x85f292be0b)]\n"
                + "pub fn update_my_other_struct (\n"
                + "    value: MyOtherStructType,\n"
                + ")\n\n");
  }

  @Test
  void contractEnumWithDoc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
    var model = TestingHelper.loadAbiParserFromFile("contract_enum.abi").parseAbi();
    var doc = List.of("Some description");
    var modelWithDoc =
        new FileAbi(
            model.header(),
            model.versionBinder(),
            model.versionClient(),
            model.shortnameLength(),
            model.chainComponent(),
            doc);
    assertThat(AbiParser.printModel(modelWithDoc))
        .isEqualTo(
            "//! Some description\n"
                + "//! \n"
                + "\n"
                + "\n"
                + "// Binder Version: 8.1.0\n"
                + "// Client Version: 5.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct EnumContractState {\n"
                + "    my_enum: Vehicle,\n"
                + "}\n\n"
                + "enum Vehicle {\n"
                + "    #[discriminant(2)]\n"
                + "    Bicycle { wheel_diameter: i32 },\n"
                + "    #[discriminant(5)]\n"
                + "    Car { engine_size: u8, supports_trailer: bool },\n"
                + "}\n\n"
                + "pub struct Signature {\n"
                + "    recovery_id: u8,\n"
                + "    value_r: [u8; 32],\n"
                + "    value_s: [u8; 32],\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0xffffffff0f)]\n"
                + "pub fn initialize (\n"
                + "    my_enum: Vehicle,\n"
                + ")\n\n"
                + "#[action(shortname = 0xffffffff0d)]\n"
                + "pub fn update_enum (\n"
                + "    val: Vehicle,\n"
                + ")\n\n");
  }

  @Test
  void testLebShortnameStringify() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-callbacks/src
    var model = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi").parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Binder Version: 3.0.0\n"
                + "// Client Version: 3.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    val: u32,\n"
                + "    successful_callback: bool,\n"
                + "    callback_results: Vec<bool>,\n"
                + "    callback_value: u32,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n"
                + "#[init(shortname = 0xffffffff0f)]\n"
                + "pub fn initialize (\n"
                + "    val: u32,\n"
                + ")\n\n"
                + "#[action(shortname = 0x01)]\n"
                + "pub fn set_and_callback (\n"
                + "    value: u32,\n"
                + ")\n\n"
                + "#[action(shortname = 0x02)]\n"
                + "pub fn panic_and_callback ()\n\n"
                + "#[action(shortname = 0x03)]\n"
                + "pub fn panicking ()\n\n"
                + "#[action(shortname = 0x04)]\n"
                + "pub fn set (\n"
                + "    value: u32,\n"
                + ")\n\n"
                + "#[action(shortname = 0x06)]\n"
                + "pub fn multiple_events_and_callback (\n"
                + "    value: u32,\n"
                + ")\n\n");
  }

  @Test
  void printZkOnSecretInput() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(AbiParser.printFunction(model, requireNonNull(abi.getFunctionByName("add_vote"))))
        .isEqualTo(
            "#[zk_on_secret_input(shortname = 0x40, secret_type = \"i32\")]\n"
                + "pub fn add_vote ()\n");
  }

  @Test
  void printZkOnSecretInputExplicitType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("secret_input_bit_length_10"))))
        .isEqualTo(
            "#[zk_on_secret_input(shortname = 0x60, secret_type = \"i16\")]\n"
                + "pub fn secret_input_bit_length_10 ()\n");
  }

  @Test
  void printZkOnVariableInputted() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_variable_inputted"))))
        .isEqualTo(
            "#[zk_on_variable_inputted(shortname = 0x71)]\n"
                + "pub fn zk_on_variable_inputted ()\n");
  }

  @Test
  void printZkOnVariableRejected() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_variable_rejected"))))
        .isEqualTo("#[zk_on_variable_rejected]\n" + "pub fn zk_on_variable_rejected ()\n");
  }

  @Test
  void printZkOnComputeComplete() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_compute_complete"))))
        .isEqualTo(
            "#[zk_on_compute_complete(shortname = 0x70)]\n" + "pub fn zk_on_compute_complete ()\n");
  }

  @Test
  void printZkOnVariablesOpened() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_variables_opened"))))
        .isEqualTo("#[zk_on_variables_opened]\n" + "pub fn zk_on_variables_opened ()\n");
  }

  @Test
  void printZkOnUserVariablesOpened() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_contract.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_user_variables_opened"))))
        .isEqualTo(
            "#[zk_on_user_variables_opened]\n"
                + "pub fn zk_on_user_variables_opened (\n"
                + "    rpc1: u8,\n"
                + ")\n");
  }

  @Test
  void printZkOnAttestationComplete() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_attestation_complete"))))
        .isEqualTo("#[zk_on_attestation_complete]\n" + "pub fn zk_on_attestation_complete ()\n");
  }

  @Test
  void printZkOnExternalEvent() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();

    assertThat(
            AbiParser.printFunction(
                model, requireNonNull(abi.getFunctionByName("zk_on_external_event"))))
        .isEqualTo("#[zk_on_external_event]\n" + "pub fn zk_on_external_event ()\n");
  }

  @Test
  void printFunctionOnly() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_set.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(AbiParser.printFunction(model, requireNonNull(abi.init())))
        .isEqualTo("#[init(shortname = 0x00000000)]\n" + "pub fn initialize ()\n");
  }

  @Test
  void printStructOnly() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    StructTypeSpec structTypeSpec = (StructTypeSpec) abi.namedTypes().get(0);
    assertThat(AbiParser.printStruct(model, structTypeSpec))
        .isEqualTo(
            "#[state]\n"
                + "pub struct ExampleContractState {\n"
                + "    val: u32,\n"
                + "    successful_callback: bool,\n"
                + "    callback_results: Vec<bool>,\n"
                + "    callback_value: u32,\n"
                + "}\n\n");
  }

  @Test
  void printStructOnlyNonState() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    StructTypeSpec structTypeSpec = (StructTypeSpec) abi.namedTypes().get(1);
    assertThat(AbiParser.printStruct(model, structTypeSpec))
        .isEqualTo("pub struct NonStateStructType {\n    value: u64,\n}\n\n");
  }

  @Test
  void printNullDocumentation() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    StructTypeSpec structTypeSpec = (StructTypeSpec) abi.namedTypes().get(1);
    StructTypeSpec noDoc = new StructTypeSpec(structTypeSpec.name(), structTypeSpec.fields(), null);
    assertThat(AbiParser.printStruct(model, noDoc))
        .isEqualTo("pub struct NonStateStructType {\n    value: u64,\n}\n\n");

    var func = requireNonNull(abi.getFunctionByName("update_my_struct"));
    ContractInvocation noDocFunc =
        new ContractInvocation(
            func.kind(), func.name(), func.shortname(), func.arguments(), null, null);
    assertThat(AbiParser.printFunction(model, noDocFunc))
        .isEqualTo(
            "#[action(shortname = 0xf3b68ff40e)]\n"
                + "pub fn update_my_struct (\n"
                + "    value: MyStructType,\n"
                + ")\n");
  }

  @Test
  void printFunctionDocumentation() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    var func = requireNonNull(abi.getFunctionByName("update_my_struct"));
    var description = List.of("Some description", "Even more description");
    var argumentDescription =
        Map.ofEntries(Map.entry("value", "FieldOneDoc"), Map.entry("unknown", "FieldTwoDoc"));
    DocumentationFunction doc =
        new DocumentationFunction(description, argumentDescription, "Some return");
    ContractInvocation docFunc =
        new ContractInvocation(
            func.kind(), func.name(), func.shortname(), func.arguments(), null, doc);
    assertThat(AbiParser.printFunction(model, docFunc))
        .isEqualTo(
            "/// Some description\n"
                + "/// Even more description\n"
                + "/// \n"
                + "/// # Parameters\n"
                + "/// \n"
                + "/// * `value`: [`MyStructType`], FieldOneDoc\n"
                + "/// \n"
                + "/// # Returns:\n"
                + "/// \n"
                + "/// Some return\n"
                + "/// \n"
                + "#[action(shortname = 0xf3b68ff40e)]\n"
                + "pub fn update_my_struct (\n"
                + "    value: MyStructType,\n"
                + ")\n");
  }

  @Test
  void printFunctionDocumentationNoReturn() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    var func = requireNonNull(abi.getFunctionByName("update_my_struct"));
    var description = List.of("Some description", "Even more description");
    var argumentDescription = Map.of("value", "this is the new struct type");
    DocumentationFunction doc = new DocumentationFunction(description, argumentDescription, null);
    ContractInvocation docFunc =
        new ContractInvocation(
            func.kind(), func.name(), func.shortname(), func.arguments(), null, doc);
    assertThat(AbiParser.printFunction(model, docFunc))
        .isEqualTo(
            "/// Some description\n"
                + "/// Even more description\n"
                + "/// \n"
                + "/// # Parameters\n"
                + "/// \n"
                + "/// * `value`: [`MyStructType`], this is the new struct type\n"
                + "/// \n"
                + "#[action(shortname = 0xf3b68ff40e)]\n"
                + "pub fn update_my_struct (\n"
                + "    value: MyStructType,\n"
                + ")\n");
  }

  @Test
  void printStructDocumentation() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    StructTypeSpec structTypeSpec = (StructTypeSpec) abi.namedTypes().get(1);
    var description = List.of("Some description", "Even more description");
    var fieldDescription =
        Map.ofEntries(Map.entry("value", "FieldOneDoc"), Map.entry("unknown", "FieldTwoDoc"));
    DocumentationNamedType doc = new DocumentationNamedType(description, fieldDescription);
    StructTypeSpec structWithDoc =
        new StructTypeSpec(structTypeSpec.name(), structTypeSpec.fields(), doc);
    assertThat(AbiParser.printStruct(model, structWithDoc))
        .isEqualTo(
            "/// Some description\n"
                + "/// Even more description\n"
                + "/// \n"
                + "/// ### Fields\n"
                + "/// \n"
                + "/// * `value`: [`u64`], FieldOneDoc\n"
                + "/// \n"
                + "pub struct NonStateStructType {\n"
                + "    value: u64,\n"
                + "}\n\n");
  }

  @Test
  void printCallback() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-zk-actions/-/tree/main/src
    AbiParser parser = TestingHelper.loadAbiParserFromFile("zk_actions.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(AbiParser.printFunction(model, requireNonNull(abi.getFunctionByName("on_callback"))))
        .isEqualTo("#[callback(shortname = 0x40)]\n" + "pub fn on_callback ()\n");
  }

  /** Printing a secret input function prints the zk_on_secret_input kind, and the secret type. */
  @Test
  void printSecretInputBinder() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("average_salary.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(AbiParser.printFunction(model, requireNonNull(abi.getFunctionByName("add_salary"))))
        .isEqualTo(
            "#[zk_on_secret_input(shortname = 0x40, secret_type = \"i32\")]\n"
                + "pub fn add_salary ()\n");
  }

  /** Can print a binder invocation. */
  @Test
  void printBinderOnlyFunction() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("average_salary.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(
            AbiParser.printBinderInvocation(
                model, requireNonNull(abi.getBinderInvocationByName("rejectInput"))))
        .isEqualTo(
            "#[binder_action(shortname = 0x06)]\n"
                + "pub fn rejectInput (\n"
                + "    variableId: i32,\n"
                + ")\n\n");
  }

  /**
   * Printing a binder invocation which calls a contract function prints the contract kind it calls.
   */
  @Test
  void printBinderFunction() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("average_salary.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(
            AbiParser.printBinderInvocation(
                model, requireNonNull(abi.getBinderInvocationByName("zeroKnowledgeInputOffChain"))))
        .isEqualTo(
            "#[binder_action(shortname = 0x04, contract_call = zk_on_secret_input)]\n"
                + "pub fn zeroKnowledgeInputOffChain (\n"
                + "    bitLengths: Vec<i32>,\n"
                + "    commitments: Vec<Hash>,\n"
                + ")\n\n");
  }

  /** Can print a binder abi. */
  @Test
  void printPubBinder() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("PublicWasmContractBinder.abi");
    FileAbi model = parser.parseAbi();
    assertThat(AbiParser.printModel(model))
        .isEqualTo(
            "// Version Binder: 12.0.0\n"
                + "// Version Client: 6.0.0\n"
                + "\n"
                + "// Binder Type: Public\n"
                + "\n"
                + "enum ContractCallableKinds {\n"
                + "  INIT = 1,\n"
                + "  ACTION = 2,\n"
                + "  CALLBACK = 3,\n"
                + "}\n"
                + "\n"
                + "#[state]\n"
                + "pub struct WasmState {\n"
                + "    avlTrees: Option<Map<Option<i32>, Option<AvlTreeWrapper>>>,\n"
                + "    state: Option<Vec<u8>>,\n"
                + "}\n\n"
                + "pub struct AvlTreeWrapper {\n"
                + "    avlTree: Option<Map<Option<ComparableByteArray>, Option<Vec<u8>>>>,\n"
                + "}\n\n"
                + "pub struct ComparableByteArray {\n"
                + "    data: Option<Vec<u8>>,\n"
                + "}\n\n"
                + "#[binder_init(contract_call = init)]\n"
                + "pub fn create ()\n\n"
                + "#[binder_action(contract_call = action)]\n"
                + "pub fn invoke ()\n\n"
                + "#[binder_callback(contract_call = callback)]\n"
                + "pub fn callback ()\n\n"
                + "#[binder_empty_action]\n"
                + "pub fn topUpGas ()\n\n");
  }

  /** Can print the upgrade function. */
  @Test
  void printUpgradeFunction() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("PublicDeployContract.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi abi = (ContractAbi) model.chainComponent();
    assertThat(AbiParser.printFunction(model, requireNonNull(abi.getFunctionByName("upgrade"))))
        .isEqualTo(
            "#[upgrade]\n" + "pub fn upgrade (\n" + "    systemUpdateContract: Address,\n" + ")\n");
  }
}
