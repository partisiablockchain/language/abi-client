package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import com.partisiablockchain.language.abimodel.parser.UnsupportedVersionException;
import java.util.HexFormat;
import java.util.List;

/** Helper class for parser tests. */
public final class ParserHelper {

  /**
   * Helper function to whether a struct has the correct field.
   *
   * @param struct The struct to test
   * @param fieldIndex The index of the field
   * @param fieldName The name of the field
   * @param spec The type of the field
   */
  public static void assertFieldType(
      NamedTypeSpec struct, int fieldIndex, String fieldName, TypeSpec spec) {
    assertThat(struct).isInstanceOf(StructTypeSpec.class);
    FieldAbi argumentAbi = ((StructTypeSpec) struct).field(fieldIndex);
    assertThat(argumentAbi.name()).isEqualTo(fieldName);
    assertThat(argumentAbi.type()).isEqualTo(spec);
  }

  /**
   * Helper function to check whether a function has correct argument.
   *
   * @param action The function to check
   * @param argIndex The index of the argument
   * @param argName The name of the argument
   * @param spec The type of the argument
   */
  public static void assertActionType(
      ContractInvocation action, int argIndex, String argName, TypeSpec spec) {
    ArgumentAbi argumentAbi = action.arguments().get(argIndex);
    assertThat(argumentAbi.name()).isEqualTo(argName);
    assertThat(argumentAbi.type()).isEqualTo(spec);
  }

  /**
   * Helper function to check that different combinations of versions fail.
   *
   * @param binderMajor The major version of the binder
   * @param binderMinor The minor version of the binder
   * @param clientMajor The major version of the client
   * @param clientMinor The minor version of the client
   */
  public static void assertClientVersionFail(
      int binderMajor, int binderMinor, int clientMajor, int clientMinor) {
    byte[] bytesOne = {
      0x50,
      0x42,
      0x43,
      0x41,
      0x42,
      0x49,
      (byte) binderMajor,
      (byte) binderMinor,
      0x00,
      (byte) clientMajor,
      (byte) clientMinor,
      0x00
    };

    AbiParser parserOne = new AbiParser(bytesOne);
    var expectedMessage =
        String.format("Unsupported Version %d.%d.0 for Version Client.", clientMajor, clientMinor);

    assertThatThrownBy(parserOne::parseAbi)
        .isInstanceOf(UnsupportedVersionException.class)
        .hasMessage(expectedMessage);
  }

  /**
   * Helper function to return a fnAbi with a given FnKind.
   *
   * @param kind The given FnKind
   * @return The ContractInvocation to return
   */
  public static ContractInvocation fnAbiWithKind(ContractInvocationKind kind) {
    return new ContractInvocation(
        kind,
        "name",
        HexFormat.of().parseHex("abcdefab0f"),
        List.of(new ArgumentAbi("arg", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))));
  }

  /**
   * Helper function to return a ContractAbi with a given FnKind.
   *
   * @param kind The given FnKind.
   * @return The ContractAbi to return
   */
  public static ContractAbi contractFromKind(ContractInvocationKind kind) {
    return new ContractAbi(
        BinderType.Public,
        List.of(),
        List.of(),
        ImplicitBinderAbi.defaultPubInvocations(),
        List.of(
            new ContractInvocation(
                ImplicitBinderAbi.DefaultKinds.ACTION, "name", new byte[] {0x01}, List.of()),
            new ContractInvocation(
                ImplicitBinderAbi.DefaultKinds.INIT, "initialize", new byte[] {0x00}, List.of()),
            new ContractInvocation(kind, "secret", new byte[] {0x02}, List.of())),
        new NamedTypeRef(0));
  }
}
