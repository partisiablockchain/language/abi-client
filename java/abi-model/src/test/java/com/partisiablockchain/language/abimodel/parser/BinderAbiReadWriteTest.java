package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BinderAbiReadWriteTest {
  /** Writing a binder abi and then reading the bytes results in the same binder abi. */
  @Test
  void writeReadBinder() {
    BinderAbi binderAbi =
        new BinderAbi(
            BinderType.Public,
            List.of(
                new ContractInvocationKind(1, "Init", true),
                new ContractInvocationKind(2, "Action", true)),
            List.of(
                new StructTypeSpec(
                    "BinderState",
                    List.of(new FieldAbi("Field", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))))),
            List.of(
                new BinderInvocation(
                    TransactionKind.Action, "open_invocation", (byte) 0x09, List.of(), 2),
                new BinderInvocation(
                    TransactionKind.Action,
                    "extend_deadline",
                    (byte) 0x13,
                    List.of(new ArgumentAbi("arg", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))),
                    null),
                new BinderInvocation(
                    TransactionKind.EmptyAction, "topUpGas", null, List.of(), null)),
            new NamedTypeRef(0));
    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, binderAbi);

    byte[] bytes = BinaryAbiWriter.serialize(abi);
    FileAbi readAbi = new AbiParser(bytes).parseAbi();

    Assertions.assertThat(abi).isEqualTo(readAbi);
  }

  /** Writing a contract abi and then reading the bytes results in the same contract abi. */
  @Test
  void writeReadContract() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(
                new ContractInvocationKind(1, "Init", true),
                new ContractInvocationKind(2, "Action", true)),
            List.of(
                new StructTypeSpec(
                    "ContractState",
                    List.of(new FieldAbi("Field", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))))),
            List.of(
                new BinderInvocation(TransactionKind.Action, "invoke", (byte) 0x09, List.of(), 2),
                new BinderInvocation(
                    TransactionKind.Action,
                    "extend_deadline",
                    (byte) 0x13,
                    List.of(
                        new ArgumentAbi("argument", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))),
                    null),
                new BinderInvocation(
                    TransactionKind.EmptyAction, "topUpGas", null, List.of(), null)),
            List.of(
                new ContractInvocation(
                    new ContractInvocationKind(2, "Action", true),
                    "vote",
                    new byte[] {1},
                    List.of(new ArgumentAbi("vote", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))))),
            new NamedTypeRef(0));
    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, contractAbi);

    byte[] bytes = BinaryAbiWriter.serialize(abi);
    FileAbi readAbi = new AbiParser(bytes).parseAbi();

    Assertions.assertThat(abi).usingRecursiveComparison().isEqualTo(readAbi);
  }
}
