package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class TypeSpecStringParserTest {

  private TypeSpecStringParser parser;
  private final List<String> namedTypes = List.of("TypeA", "TypeB", "TypeC");
  private TypeSpecStringifier typeSpecStringifier;

  @BeforeEach
  void setUp() {
    parser = new TypeSpecStringParser(namedTypes, true);
    typeSpecStringifier = new TypeSpecStringifier(namedTypes);
  }

  @Test
  void attemptToParseNonSimpleType() {
    assertThatThrownBy(() -> parser.parse("Map")).hasMessage("Invalid type: Map");
  }

  @Test
  void parseInvalidArray() {
    assertThat(parser.parseArray("u64")).isNull();
  }

  @Test
  void parseInvalidGeneric() {
    assertThat(parser.parseGeneric("u64")).isNull();
    assertThatThrownBy(() -> parser.parseGeneric("Struct<u64>"))
        .hasMessage("Invalid generic type: Struct");
  }

  @Test
  void parseDontCheckNamedTypeIndices() {
    parser = new TypeSpecStringParser(namedTypes, false);
    assertThat(parser.parse("TypeB"))
        .isInstanceOf(NamedTypeRef.class)
        .isEqualTo(new NamedTypeRef(1));
    assertThat(parser.parse("TypeD"))
        .isInstanceOf(NamedTypeRef.class)
        .isEqualTo(new NamedTypeRef(-1));
  }

  @Test
  void parseGenerics() {
    assertStringRoundTrip(
        "Map<Option<Vec<Set<Map<Option<Set<Option<Set<i8>>>>, Set<Set<Map<Map<Map<Option<Map<TypeC,"
            + " [u8; 7]>>, Address>, Option<Map<TypeA, Vec<u32>>>>, u8>>>>>>>,"
            + " Vec<Set<Option<Option<u128>>>>>");
    assertStringRoundTrip(
        "Set<Map<Set<Vec<Map<Vec<TypeA>, Map<Option<Map<u64, i64>>, Set<[u8; 2]>>>>>,"
            + " Map<Map<Vec<Vec<Vec<Option<Map<[u8; 13], [u8; 7]>>>>>, Option<Vec<TypeA>>>,"
            + " i16>>>");
    assertStringRoundTrip(
        "Map<[u8; 19], Map<Set<Set<Vec<u64>>>, Map<Map<Map<i64, Vec<Vec<Set<Option<Option<[u8;"
            + " 4]>>>>>>, i128>, Option<Vec<Set<Option<TypeB>>>>>>>");
    assertStringRoundTrip(
        "Map<Map<Option<Vec<u8>>, Vec<Map<[u8; 26], Vec<Map<Map<Option<Option<Set<Set<[u8; 23]>>>>,"
            + " Set<u64>>, [u8; 14]>>>>>, [u8; 7]>");
    assertStringRoundTrip(
        "Map<Map<Set<u8>, Map<Vec<Map<i64, Option<Option<Vec<TypeA>>>>>, Option<Map<u64, Map<i64,"
            + " Option<Set<[u8; 26]>>>>>>>, u8>");
    assertStringRoundTrip(
        "Vec<Option<Vec<Map<u128, Map<Map<Map<Map<Option<String>, [u8; 2]>, Set<[u8; 20]>>, i8>,"
            + " Option<TypeB>>>>>>");
    assertStringRoundTrip(
        "Map<Map<Vec<Vec<Map<Vec<i64>, Option<Vec<Map<[u8; 28], Option<[u8; 22]>>>>>>>, Option<[u8;"
            + " 1]>>, TypeC>");
    assertStringRoundTrip(
        "Map<Map<Vec<TypeC>, Vec<Option<TypeC>>>, Set<Map<[u8; 4], Map<Option<Set<Set<TypeB>>>,"
            + " u32>>>>");
    assertStringRoundTrip(
        "Vec<Option<Set<Map<Option<Map<Set<Set<i128>>, TypeA>>, Map<i16, Option<[u8; 24]>>>>>>");
    assertStringRoundTrip(
        "Option<Map<Map<TypeB, [u8; 6]>, Vec<Vec<Set<Set<Map<i8, Option<Vec<String>>>>>>>>>");
    assertStringRoundTrip(
        "Map<Option<TypeB>, Vec<Option<Map<TypeC, Map<Vec<Vec<Vec<TypeB>>>, Vec<bool>>>>>>");
    assertStringRoundTrip(
        "Map<Option<Vec<Option<i8>>>, Set<Map<Option<TypeA>, Set<Option<TypeC>>>>>");
    assertStringRoundTrip(
        "Option<Option<Map<Map<TypeA, Option<u8>>, Map<Option<[u8; 17]>, u128>>>>");
    assertStringRoundTrip("Map<Set<Option<[u8; 15]>>, Set<Option<Vec<Option<Option<[u8; 4]>>>>>>");
    assertStringRoundTrip("Option<Map<Vec<Option<Vec<String>>>, Option<Option<Set<[u8; 23]>>>>>");
    assertStringRoundTrip("Option<Option<Map<Map<Address, Vec<Option<Option<Address>>>>, i64>>>");
    assertStringRoundTrip("Vec<Vec<Map<Map<Vec<Option<[u8; 8]>>, Option<[u8; 8]>>, Vec<u128>>>>");
    assertStringRoundTrip("Map<Vec<[u8; 7]>, Map<Option<Map<Vec<i128>, Set<[u8; 21]>>>, bool>>");
    assertStringRoundTrip("Set<Map<TypeA, Map<Option<[u8; 17]>, Map<Vec<String>, Set<i16>>>>>");
    assertStringRoundTrip("Map<Map<i128, Vec<TypeB>>, Option<Vec<Map<Vec<u8>, Option<u8>>>>>");
    assertStringRoundTrip("Map<String,Map<String,Map<u64,Vec<u32>>>>");

    assertStringRoundTrip("Set<u32>");
    assertStringRoundTrip("Option<u32>");
    assertStringRoundTrip("Option<Vec<u32>>");

    assertStringRoundTrip("AvlTreeMap<String,u32>");
    assertStringRoundTrip("AvlTreeMap<String,AvlTreeMap<String,AvlTreeMap<u64,Vec<u32>>>>");
  }

  private void assertStringRoundTrip(String string) {
    var spec = parser.parse(string);
    assertThat(typeSpecStringifier.stringify(spec)).isEqualToIgnoringWhitespace(string);
  }
}
