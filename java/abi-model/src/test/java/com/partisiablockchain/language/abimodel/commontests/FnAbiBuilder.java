package com.partisiablockchain.language.abimodel.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.util.NumericConversion;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;

/** Helper class to build {@link ContractInvocation}s for references in test. */
public final class FnAbiBuilder {
  private ContractInvocationKind kind;
  private String name;
  private byte[] shortname;
  private final List<ArgumentAbi> arguments = new ArrayList<>();
  private ArgumentAbi secretArgument;

  /**
   * Set the function kind.
   *
   * @param kind function kind
   * @return this builder
   */
  public FnAbiBuilder kind(ContractInvocationKind kind) {
    this.kind = kind;
    return this;
  }

  /**
   * Set the name of the function.
   *
   * @param name name of the function
   * @return this builder
   */
  public FnAbiBuilder name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Set the shortname of the function.
   *
   * @param shortname the function shortname
   * @return this builder
   */
  public FnAbiBuilder shortname(byte[] shortname) {
    this.shortname = shortname;
    return this;
  }

  /**
   * Set the shortname of the function.
   *
   * @param shortname hex encoded shortname
   * @return this builder
   */
  public FnAbiBuilder shortname(String shortname) {
    this.shortname = HexFormat.of().parseHex(shortname);
    return this;
  }

  /**
   * Add argument.
   *
   * @param name name of the argument
   * @param spec type spec of the argument
   * @return this builder
   */
  public FnAbiBuilder addArgument(String name, TypeSpec spec) {
    this.arguments.add(new ArgumentAbi(name, spec));
    return this;
  }

  /**
   * Add secret argument.
   *
   * @param name name of the secret argument
   * @param spec type spec of the secret argument
   * @return this builder
   */
  public FnAbiBuilder secretArgument(String name, TypeSpec spec) {
    this.secretArgument = new ArgumentAbi(name, spec);
    return this;
  }

  /**
   * Build the {@link ContractInvocation} from the input.
   *
   * @return new {@link ContractInvocation}
   */
  public ContractInvocation build() {
    if (secretArgument == null) {
      return new ContractInvocation(kind, name, effectiveShortname(), arguments);
    } else {
      return new ContractInvocation(kind, name, effectiveShortname(), arguments, secretArgument);
    }
  }

  private byte[] effectiveShortname() {
    if (shortname == null) {
      return nameDigest();
    } else {
      return shortname;
    }
  }

  private byte[] nameDigest() {
    return ExceptionConverter.call(
        () -> {
          MessageDigest md = MessageDigest.getInstance("SHA-256");
          byte[] result = md.digest(name.getBytes(StandardCharsets.UTF_8));

          int shortnameAsInt = NumericConversion.intFromBytes(result, 0);
          return leb128Encode(shortnameAsInt);
        });
  }

  private byte[] leb128Encode(int value) {
    if (value == 0) {
      return new byte[] {0};
    }

    ByteArrayOutputStream result = new ByteArrayOutputStream();
    while (value != 0) {
      int lowerSeven = value & 0x7f;

      value >>= 7;

      int highBit = value != 0 ? 0x80 : 0;
      result.write(lowerSeven | highBit);
    }
    return result.toByteArray();
  }
}
