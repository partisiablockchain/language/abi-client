package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abimodel.commontests.TestingHelper.bytesFromHex;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.parser.AbiParser;
import org.junit.jupiter.api.Test;

/** Testing of {@link AbiParser#parseTypeSpec} in {@link AbiParser}. */
public final class TypeSpecParsingTest {

  /** Deprecated Byte Arrays Parse Same As New. */
  @Test
  public void deprecatedByteArraysParseToSameAsNew() {
    final var typeSpec1 = new AbiParser(bytesFromHex("1105")).parseTypeSpec();
    final var typeSpec2 = new AbiParser(bytesFromHex("1A0105")).parseTypeSpec();

    assertThat(typeSpec1).isEqualTo(typeSpec2);
  }
}
