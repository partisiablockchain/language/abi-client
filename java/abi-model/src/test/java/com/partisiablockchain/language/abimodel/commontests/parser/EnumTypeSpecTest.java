package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import java.util.List;
import org.junit.jupiter.api.Test;

final class EnumTypeSpecTest {

  private final EnumVariant enumVariant = new EnumVariant(0, new NamedTypeRef(0));
  private final EnumTypeSpec enumType = new EnumTypeSpec("enum", List.of(enumVariant));

  @Test
  void values() {
    assertThat(enumType.name()).isEqualTo("enum");
    assertThat(enumType.variants()).isEqualTo(List.of(enumVariant));
    assertThat(enumType.variant(0)).isEqualTo(enumVariant);
    assertThat(enumType.variant(1)).isEqualTo(null);
  }
}
