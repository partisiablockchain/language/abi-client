package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.FileAbi;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Tests read/write of binary ABIs. */
final class BinaryAbiWriterTest {

  @ParameterizedTest
  @MethodSource("listBinaryFiles")
  void readWriteRoundTrip(Path abiPath) {
    readWriteRoundTripForFile(abiPath, AbiParser.Strictness.STRICT);
  }

  /**
   * List binary test files.
   *
   * @return the list of files in the testdata/binary folder
   */
  public static List<Path> listBinaryFiles() {
    try (Stream<Path> stream =
        Files.list(
            Paths.get(
                "src/test/resources/com/partisiablockchain/language/"
                    + "abimodel/reference-tests/abi-files"))) {
      return stream.toList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /** Throw AbiParseException when invalid identifier encountered. */
  @Test
  void invalidIdentifierBeginningCharStrict() {
    Assertions.assertThrows(
        AbiParseException.class,
        () ->
            new AbiParser(
                    Files.readAllBytes(
                        Paths.get(
                            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                                + "contract_invalid_identifier_beginning_char.abi")))
                .parseAbi());
  }

  /** Throw AbiParseException when invalid identifier encountered. */
  @Test
  void invalidIdentifierMiddleCharStrict() {
    Assertions.assertThrows(
        AbiParseException.class,
        () ->
            new AbiParser(
                    Files.readAllBytes(
                        Paths.get(
                            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                                + "contract_invalid_identifier_middle_char.abi")))
                .parseAbi());
  }

  /** Don't throw AbiParseException when illegal identifier encountered in lenient mode. */
  @Test
  void invalidIdentifierBeginningCharLenient() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_beginning_char.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  /** Don't throw AbiParseException when illegal identifier encountered in lenient mode. */
  @Test
  void invalidIdentifierMiddleCharLenient() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_middle_char.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierNamedTypeName() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_struct.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierActionName() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_action.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierParameterName() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_parameter.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierFieldName() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_struct_field.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierInitName() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_init.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  @Test
  void invalidIdentifierEnum() {
    var path =
        Paths.get(
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_enum.abi");
    readWriteRoundTripForFile(path, AbiParser.Strictness.LENIENT);
  }

  /**
   * Read the ABI from the given path, write it, read it again and test that the results are the
   * same.
   */
  private void readWriteRoundTripForFile(Path input, AbiParser.Strictness strictness) {
    try {
      System.out.println("Testing " + input.normalize());

      FileAbi original = new AbiParser(Files.readAllBytes(input), strictness).parseAbi();

      var firstWritten = BinaryAbiWriter.serialize(original);

      FileAbi firstRoundTrip = new AbiParser(firstWritten, strictness).parseAbi();

      var secondWriteStream = new OutputStreamWithReference(firstWritten);
      new BinaryAbiWriter(firstRoundTrip, secondWriteStream).write();

      var secondWritten = secondWriteStream.out.toByteArray();
      FileAbi actual = new AbiParser(secondWritten, strictness).parseAbi();

      assertThat(actual).usingRecursiveComparison().isEqualTo(original).isEqualTo(firstRoundTrip);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * An input stream that has a reference byte array and fails if the caller writes a bytes that
   * does not match the reference.
   */
  private static final class OutputStreamWithReference extends OutputStream {

    private final byte[] reference;
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();

    private int offset;

    private OutputStreamWithReference(byte[] reference) {
      this.reference = reference;
    }

    @Override
    public void write(int actual) {
      if (actual != reference[offset]) {
        var msg =
            String.format(
                "Failed reference write at offset %d (%x), actual != ref: %d (%x) != %d (%x)",
                offset, offset, actual, actual, reference[offset], reference[offset]);

        throw new RuntimeException(msg);
      }

      out.write(actual);
      offset++;
    }
  }
}
