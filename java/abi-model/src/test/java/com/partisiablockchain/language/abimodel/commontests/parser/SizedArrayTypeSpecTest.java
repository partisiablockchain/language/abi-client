package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link SizedArrayTypeSpec} for the special case where values are {@link
 * TypeSpec.TypeIndex.u8}.
 */
public final class SizedArrayTypeSpecTest {

  /** Can create {@link SizedArrayTypeSpec}. */
  @Test
  public void values() {
    var spec = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 42);
    assertThat(spec.typeIndex()).isEqualTo(TypeSpec.TypeIndex.SizedArray);
    assertThat(spec.valueType().typeIndex()).isEqualTo(TypeSpec.TypeIndex.u8);
    assertThat(spec.length()).isEqualTo(42);
  }
}
