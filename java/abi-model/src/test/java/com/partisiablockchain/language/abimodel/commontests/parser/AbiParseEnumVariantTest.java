package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.commontests.FileAbiBuilder;
import com.partisiablockchain.language.abimodel.commontests.FnAbiBuilder;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test that enum variants can be placed anywhere in the ABI file. */
public final class AbiParseEnumVariantTest {

  /**
   * In an ABI file, the variant structs of an enum does not need to appear directly after the enum
   * type.
   */
  @Test
  void enumVariantsDoNotNeedToAppearDirectlyAfterEnumInAbi() {
    FnAbiBuilder fnAbi =
        new FnAbiBuilder()
            .kind(ImplicitBinderAbi.DefaultKinds.INIT)
            .name("Foo")
            .shortname("ffffffff0f");
    AbiVersion version = new AbiVersion(5, 6, 0);
    StructTypeSpec variantA = new StructTypeSpec("VariantA", List.of()); // index 0
    StructTypeSpec innerVariantStruct =
        new StructTypeSpec("InnerStructVariantB", List.of()); // index 1
    StructTypeSpec variantB =
        new StructTypeSpec(
            "VariantB", List.of(new FieldAbi("inner", new NamedTypeRef(1)))); // index 2
    EnumTypeSpec myEnum =
        new EnumTypeSpec(
            "MyEnum",
            List.of(
                new EnumVariant(0, new NamedTypeRef(0)), new EnumVariant(1, new NamedTypeRef(2))));

    byte[] rawAbi =
        new FileAbiBuilder("variant_types_placed_before_enum.abi")
            .addFnAbi(fnAbi)
            .clientVersion(version)
            .binderVersion(version)
            .stateType(new SimpleTypeSpec(TypeSpec.TypeIndex.u8))
            .addNamedType(variantA)
            .addNamedType(innerVariantStruct)
            .addNamedType(variantB)
            .addNamedType(myEnum)
            .build();

    ContractAbi contractAbi = new AbiParser(rawAbi).parseContractAbi();

    assertThat(contractAbi.getNamedType(new NamedTypeRef(3))).isEqualTo(myEnum);
  }
}
