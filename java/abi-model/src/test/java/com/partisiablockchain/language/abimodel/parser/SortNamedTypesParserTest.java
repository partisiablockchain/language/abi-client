package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Testing sorting of named types when parsing using {@link AbiParser.Strictness#SORTING}. */
public final class SortNamedTypesParserTest {
  /** Unreachable named types are removed when sorting the named types while parsing the abi. */
  @Test
  void unreachableTypesAreRemoved() {
    byte[] bytes =
        readBinaryFile("src/test/resources/testdata/cached-references/multiple_unreachables.abi");
    FileAbi fileAbi = new AbiParser(bytes, AbiParser.Strictness.SORTING).parseAbi();
    byte[] newBytes = BinaryAbiWriter.serialize(fileAbi);
    ContractAbi contractAbi = new AbiParser(newBytes).parseContractAbi();
    Assertions.assertThat(contractAbi.namedTypes().size()).isEqualTo(1);
  }

  /**
   * The order of the named types are sorted based on a depth first search when sorting the named
   * types.
   */
  @Test
  void namedTypesAreSorted() {
    byte[] bytes =
        readBinaryFile("src/test/resources/testdata/cached-references/unordered_struct_fields.abi");
    FileAbi fileAbi = new AbiParser(bytes, AbiParser.Strictness.SORTING).parseAbi();
    byte[] newBytes = BinaryAbiWriter.serialize(fileAbi);
    ContractAbi contractAbi = new AbiParser(newBytes).parseContractAbi();
    Assertions.assertThat(contractAbi.namedTypes().size()).isEqualTo(4);
  }

  /**
   * When the named types have been sorted, all named type refs in the abi is replaced with their
   * new sorted index.
   */
  @Test
  void namedTypeRefsAreReplacedRecursively() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(
                new StructTypeSpec("Unreachable", List.of()),
                new StructTypeSpec("State", allFieldTypes())),
            List.of(
                new BinderInvocation(
                    TransactionKind.Action,
                    "binderInvocation",
                    (byte) 1,
                    allArgumentTypes(),
                    null)),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "contractFunction",
                    new byte[] {0},
                    allArgumentTypes(),
                    new ArgumentAbi("secretArg", new NamedTypeRef(1)))),
            new NamedTypeRef(1));

    ContractAbi checkedCorrectSorting = sortAbi(contractAbi);

    Assertions.assertThat(checkedCorrectSorting.namedTypes().size()).isEqualTo(1);
    Assertions.assertThat(checkedCorrectSorting.namedTypes().get(0).name()).isEqualTo("State");
    Assertions.assertThat(((NamedTypeRef) checkedCorrectSorting.stateType()).index()).isEqualTo(0);
  }

  /** Named enum types have their enum variant definitions updated to the new sorted value. */
  @Test
  void enumVariantDefsAreReplaced() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(
                new StructTypeSpec("Unreachable", List.of()),
                new StructTypeSpec("State", List.of(new FieldAbi("enum", new NamedTypeRef(2)))),
                new EnumTypeSpec("Enum", List.of(new EnumVariant(1, new NamedTypeRef(3)))),
                new StructTypeSpec("EnumVariant", List.of())),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.INIT, "init", new byte[] {0}, List.of())),
            new NamedTypeRef(1));

    ContractAbi checkedCorrectSorting = sortAbi(contractAbi);

    Assertions.assertThat(checkedCorrectSorting.namedTypes().size()).isEqualTo(3);
    Assertions.assertThat(checkedCorrectSorting.namedTypes().get(0).name()).isEqualTo("State");
    EnumTypeSpec enumType = (EnumTypeSpec) checkedCorrectSorting.namedTypes().get(1);
    Assertions.assertThat(enumType.variants().get(0).def().index()).isEqualTo(2);
  }

  /** Sorts the abi, and parses the abi again to check correct sorting. */
  private static ContractAbi sortAbi(ContractAbi contractAbi) {
    FileAbi unsortedAbi =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, contractAbi);
    byte[] unsortedAbiBytes = BinaryAbiWriter.serialize(unsortedAbi);
    FileAbi sortedAbi = new AbiParser(unsortedAbiBytes, AbiParser.Strictness.SORTING).parseAbi();
    byte[] sortedAbiBytes = BinaryAbiWriter.serialize(sortedAbi);
    return new AbiParser(sortedAbiBytes).parseContractAbi();
  }

  private static List<FieldAbi> allFieldTypes() {
    List<TypeSpec> allTypes = allTypes();
    return IntStream.range(0, allTypes.size())
        .mapToObj(i -> new FieldAbi("field" + i, allTypes.get(i)))
        .toList();
  }

  private static List<ArgumentAbi> allArgumentTypes() {
    List<TypeSpec> allTypes = allTypes();
    return IntStream.range(0, allTypes.size())
        .mapToObj(i -> new ArgumentAbi("field" + i, allTypes.get(i)))
        .toList();
  }

  private static List<TypeSpec> allTypes() {
    return List.of(
        new NamedTypeRef(1),
        new MapTypeSpec(new NamedTypeRef(1), new NamedTypeRef(1)),
        new AvlTreeMapTypeSpec(new NamedTypeRef(1), new NamedTypeRef(1)),
        new VecTypeSpec(new NamedTypeRef(1)),
        new SetTypeSpec(new NamedTypeRef(1)),
        new OptionTypeSpec(new NamedTypeRef(1)),
        new SizedArrayTypeSpec(new NamedTypeRef(1), 4),
        new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
  }

  private static byte[] readBinaryFile(String fileName) {
    try (InputStream in = Files.newInputStream(Path.of(fileName))) {
      return in.readAllBytes();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
