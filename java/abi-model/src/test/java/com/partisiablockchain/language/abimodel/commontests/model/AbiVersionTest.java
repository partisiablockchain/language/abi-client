package com.partisiablockchain.language.abimodel.commontests.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import org.junit.jupiter.api.Test;

final class AbiVersionTest {
  @Test
  void valueToString() {
    var version = new AbiVersion(1, 2, 3);
    assertThat(version.toString()).isEqualTo("1.2.3");
  }

  @Test
  void parseFromString() {
    var expected = new AbiVersion(1, 2, 3);
    assertThat(AbiVersion.parseFromString("1.2.3")).isEqualTo(expected);
  }

  @Test
  void parseLargeVersionsFromString() {
    var expected = new AbiVersion(141, 212, 35);
    assertThat(AbiVersion.parseFromString("141.212.35")).isEqualTo(expected);
  }

  @Test
  void failParseMajor() {
    var invalidVersion = "a.2.3";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }

  @Test
  void failParseMajorWithPrefix() {
    var invalidVersion = "alpha-2.2.3";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }

  @Test
  void failParseMinor() {
    var invalidVersion = "1.u.3";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }

  @Test
  void failParseMinorWithInfix() {
    var invalidVersion = "1.1u.3";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }

  @Test
  void failParsePatch() {
    var invalidVersion = "1.2.x";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }

  @Test
  void failParsePatchWithSuffix() {
    var invalidVersion = "1.2.3-beta";
    assertThatThrownBy(() -> AbiVersion.parseFromString(invalidVersion))
        .hasMessage("Invalid version: " + invalidVersion);
  }
}
