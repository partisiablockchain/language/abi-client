package com.partisiablockchain.language.abimodel.commontests.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ImplicitBinderAbiTest {

  /** The default binder invocations for a pub binder has the correct names and arguments. */
  @Test
  void defaultPubInvocations() {
    var defaultPubFunctions = ImplicitBinderAbi.defaultPubInvocations();
    assertThat(defaultPubFunctions.size()).isEqualTo(4);

    assertThat(defaultPubFunctions.get(0).kind()).isEqualTo(TransactionKind.Init);
    assertThat(defaultPubFunctions.get(0).name()).isEqualTo("create");
    assertThat(defaultPubFunctions.get(0).arguments().size()).isEqualTo(0);

    assertThat(defaultPubFunctions.get(1).name()).isEqualTo("invoke");
    assertThat(defaultPubFunctions.get(1).shortname()).isEqualTo(null);
    assertThat(defaultPubFunctions.get(1).arguments().size()).isEqualTo(0);

    assertThat(defaultPubFunctions.get(2).kind()).isEqualTo(TransactionKind.Callback);
    assertThat(defaultPubFunctions.get(2).name()).isEqualTo("callback");
    assertThat(defaultPubFunctions.get(2).arguments().size()).isEqualTo(0);

    assertThat(defaultPubFunctions.get(3).kind()).isEqualTo(TransactionKind.EmptyAction);
    assertThat(defaultPubFunctions.get(3).name()).isEqualTo("topUpGas");
    assertThat(defaultPubFunctions.get(3).arguments().size()).isEqualTo(0);
  }

  /** The default binder invocations for a zk binder has the correct names and arguments. */
  @Test
  void defaultZkFunctions() {
    var defaultZkFunctions = ImplicitBinderAbi.defaultZkInvocations();
    assertThat(defaultZkFunctions.size()).isEqualTo(23);

    assertThat(defaultZkFunctions.get(0).kind()).isEqualTo(TransactionKind.Init);
    assertThat(defaultZkFunctions.get(0).name()).isEqualTo("create");
    assertThat(defaultZkFunctions.get(0).arguments().size()).isEqualTo(0);

    assertThat(defaultZkFunctions.get(1).name()).isEqualTo("commitResultVariable");
    assertThat(defaultZkFunctions.get(1).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(1).arguments().get(0).name()).isEqualTo("calculationFor");
    assertThat(defaultZkFunctions.get(1).arguments().get(1).name()).isEqualTo("restOfBinderArgs");

    assertThat(defaultZkFunctions.get(2).name()).isEqualTo("onChainOutput");
    assertThat(defaultZkFunctions.get(2).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(2).arguments().get(0).name()).isEqualTo("outputId");
    assertThat(defaultZkFunctions.get(2).arguments().get(1).name()).isEqualTo("engineShares");

    assertThat(defaultZkFunctions.get(3).name()).isEqualTo("unableToCalculate");
    assertThat(defaultZkFunctions.get(3).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(3).arguments().get(0).name()).isEqualTo("calculationFor");

    assertThat(defaultZkFunctions.get(4).name()).isEqualTo("openMaskedInput");
    assertThat(defaultZkFunctions.get(4).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(4).arguments().get(0).name()).isEqualTo("variableId");
    assertThat(defaultZkFunctions.get(4).arguments().get(1).name()).isEqualTo("maskedInput");

    assertThat(defaultZkFunctions.get(5).name()).isEqualTo("zeroKnowledgeInputOffChain");
    assertThat(defaultZkFunctions.get(5).arguments().size()).isEqualTo(5);
    assertThat(defaultZkFunctions.get(5).arguments().get(0).name()).isEqualTo("bitLengths");
    assertThat(defaultZkFunctions.get(5).arguments().get(1).name()).isEqualTo("commitment1");
    assertThat(defaultZkFunctions.get(5).arguments().get(2).name()).isEqualTo("commitment2");
    assertThat(defaultZkFunctions.get(5).arguments().get(3).name()).isEqualTo("commitment3");
    assertThat(defaultZkFunctions.get(5).arguments().get(4).name()).isEqualTo("commitment4");

    assertThat(defaultZkFunctions.get(6).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(6).name()).isEqualTo("zeroKnowledgeInputOnChain");
    assertThat(defaultZkFunctions.get(6).arguments().size()).isEqualTo(3);
    assertThat(defaultZkFunctions.get(6).arguments().get(0).name()).isEqualTo("bitLengths");
    assertThat(defaultZkFunctions.get(6).arguments().get(1).name()).isEqualTo("publicKey");
    assertThat(defaultZkFunctions.get(6).arguments().get(2).name()).isEqualTo("encryptedShares");

    assertThat(defaultZkFunctions.get(7).name()).isEqualTo("rejectInput");
    assertThat(defaultZkFunctions.get(7).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(7).arguments().get(0).name()).isEqualTo("variableId");

    assertThat(defaultZkFunctions.get(8).name()).isEqualTo("openInvocation");
    assertThat(defaultZkFunctions.get(8).shortname()).isEqualTo((byte) 9);
    assertThat(defaultZkFunctions.get(8).arguments().size()).isEqualTo(0);

    assertThat(defaultZkFunctions.get(9).name()).isEqualTo("addAttestationSignature");
    assertThat(defaultZkFunctions.get(9).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(9).arguments().get(0).name()).isEqualTo("attestationId");
    assertThat(defaultZkFunctions.get(9).arguments().get(1).name()).isEqualTo("signature");

    assertThat(defaultZkFunctions.get(10).name()).isEqualTo("getComputationDeadline");
    assertThat(defaultZkFunctions.get(10).arguments().size()).isEqualTo(0);

    assertThat(defaultZkFunctions.get(11).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(11).name()).isEqualTo("onComputeComplete");
    assertThat(defaultZkFunctions.get(11).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(11).arguments().get(0).name()).isEqualTo("ids");

    assertThat(defaultZkFunctions.get(12).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(12).name()).isEqualTo("onVariablesOpened");
    assertThat(defaultZkFunctions.get(12).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(12).arguments().get(0).name()).isEqualTo("ids");

    assertThat(defaultZkFunctions.get(13).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(13).name()).isEqualTo("onAttestationComplete");
    assertThat(defaultZkFunctions.get(13).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(13).arguments().get(0).name()).isEqualTo("id");

    assertThat(defaultZkFunctions.get(14).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(14).name()).isEqualTo("onVariableInputted");
    assertThat(defaultZkFunctions.get(14).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(14).arguments().get(0).name()).isEqualTo("variableId");

    assertThat(defaultZkFunctions.get(15).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(15).name()).isEqualTo("onVariableRejected");
    assertThat(defaultZkFunctions.get(15).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(15).arguments().get(0).name()).isEqualTo("variableId");

    assertThat(defaultZkFunctions.get(16).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(16).name()).isEqualTo("onUserVariablesOpened");
    assertThat(defaultZkFunctions.get(16).arguments().size()).isEqualTo(1);
    assertThat(defaultZkFunctions.get(16).arguments().get(0).name()).isEqualTo("ids");

    assertThat(defaultZkFunctions.get(17).name()).isEqualTo("addBatches");
    assertThat(defaultZkFunctions.get(17).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(17).arguments().get(0).name()).isEqualTo("batchType");
    assertThat(defaultZkFunctions.get(17).arguments().get(1).name()).isEqualTo("batchId");

    assertThat(defaultZkFunctions.get(18).name()).isEqualTo("extendZkComputationDeadline");
    assertThat(defaultZkFunctions.get(18).arguments().size()).isEqualTo(4);
    assertThat(defaultZkFunctions.get(18).arguments().get(0).name()).isEqualTo("msPerGasNumerator");
    assertThat(defaultZkFunctions.get(18).arguments().get(1).name())
        .isEqualTo("msPerGasDenominator");
    assertThat(defaultZkFunctions.get(18).arguments().get(2).name()).isEqualTo("minExtension");
    assertThat(defaultZkFunctions.get(18).arguments().get(3).name()).isEqualTo("maxExtension");

    assertThat(defaultZkFunctions.get(19).name()).isEqualTo("addExternalEvent");
    assertThat(defaultZkFunctions.get(19).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(19).arguments().get(0).name()).isEqualTo("subscriptionId");
    assertThat(defaultZkFunctions.get(19).arguments().get(1).name()).isEqualTo("restOfBinderArgs");

    assertThat(defaultZkFunctions.get(20).kind()).isEqualTo(TransactionKind.Action);
    assertThat(defaultZkFunctions.get(20).name()).isEqualTo("onExternalEvent");
    assertThat(defaultZkFunctions.get(20).arguments().size()).isEqualTo(2);
    assertThat(defaultZkFunctions.get(20).arguments().get(0).name()).isEqualTo("subscriptionId");
    assertThat(defaultZkFunctions.get(20).arguments().get(1).name()).isEqualTo("eventId");

    assertThat(defaultZkFunctions.get(21).kind()).isEqualTo(TransactionKind.Callback);
    assertThat(defaultZkFunctions.get(21).name()).isEqualTo("callback");
    assertThat(defaultZkFunctions.get(21).arguments().size()).isEqualTo(0);

    assertThat(defaultZkFunctions.get(22).kind()).isEqualTo(TransactionKind.EmptyAction);
    assertThat(defaultZkFunctions.get(22).name()).isEqualTo("topUpGas");
    assertThat(defaultZkFunctions.get(22).arguments().size()).isEqualTo(0);
  }

  /** All the zk-kinds correctly gets reported as a zk-kind. */
  @Test
  void isKindZk() {
    assertThat(
            ImplicitBinderAbi.isZkKind(ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_COMPUTE_COMPLETE.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_USER_VARIABLES_OPENED.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_INPUTTED.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLES_OPENED.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_VARIABLE_REJECTED.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_ATTESTATION_COMPLETE.kindId()))
        .isTrue();
    assertThat(
            ImplicitBinderAbi.isZkKind(
                ImplicitBinderAbi.DefaultKinds.ZK_ON_EXTERNAL_EVENT.kindId()))
        .isTrue();
    assertThat(ImplicitBinderAbi.isZkKind(ImplicitBinderAbi.DefaultKinds.ACTION.kindId()))
        .isFalse();
    assertThat(ImplicitBinderAbi.isZkKind(50)).isFalse();
  }
}
