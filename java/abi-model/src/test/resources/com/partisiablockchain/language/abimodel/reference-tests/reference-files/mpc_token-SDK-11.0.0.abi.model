// Binder Version: 8.1.0
// Client Version: 5.0.0

// Binder Type: Public

#[state]
pub struct MpcTokenContractState {
    transfers: Map<[u8; 32], TransferInformation>,
}

pub struct TransferInformation {
    sender: Address,
    recipient: Address,
    amount: i64,
    symbol: String,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn init (
    locked: u8,
)

#[action(shortname = 0x00)]
pub fn stake_tokens (
    amount: u64,
)

#[action(shortname = 0x01)]
pub fn unstake_tokens (
    amount: u64,
)

#[action(shortname = 0x02)]
pub fn disassociate_tokens (
    address: Address,
)

#[action(shortname = 0x03)]
pub fn transfer (
    recipient: Address,
    amount: u64,
)

#[action(shortname = 0x04)]
pub fn retry (
    transaction_id: [u8; 32],
)

#[action(shortname = 0x05)]
pub fn check_pending_unstakes ()

#[action(shortname = 0x06)]
pub fn check_vested_tokens ()

#[action(shortname = 0x0d)]
pub fn transfer_small_memo (
    recipient: Address,
    amount: u64,
    unused: u64,
)

#[action(shortname = 0x17)]
pub fn transfer_large_memo (
    recipient: Address,
    amount: u64,
    unused: String,
)

#[action(shortname = 0x18)]
pub fn delegate_stakes (
    recipient: Address,
    amount: u64,
)

#[action(shortname = 0x19)]
pub fn retract_delegated_stakes (
    recipient: Address,
    amount: u64,
)

#[action(shortname = 0x1a)]
pub fn retry_delegate_stakes (
    transaction_id: [u8; 32],
)

#[action(shortname = 0x1b)]
pub fn accept_delegated_stakes (
    sender: Address,
    amount: u64,
)

#[action(shortname = 0x1c)]
pub fn reduce_delegated_stakes (
    sender: Address,
    amount: u64,
)

#[action(shortname = 0x1d)]
pub fn byoc_transfer (
    recipient: Address,
    amount: u64,
    symbol: String,
)

