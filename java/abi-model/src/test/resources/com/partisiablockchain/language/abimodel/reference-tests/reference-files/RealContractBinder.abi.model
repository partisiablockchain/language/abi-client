// Version Binder:
// Version Client:

// Binder Type: Zk

enum ContractCallableKinds {
  INIT = 1,
  ACTION = 2,
  CALLBACK = 3,
  ZK_ON_COMPUTE_COMPLETE = 19,
  ZK_ON_VARIABLES_OPENED = 20,
  ZK_ON_ATTESTATION_COMPLETE = 22,
  ZK_ON_SECRET_INPUT = 23,
  ZK_ON_VARIABLE_REJECTED = 18,
  ZK_ON_VARIABLE_INPUTTED = 17,
  ZK_ON_EXTERNAL_EVENT = 24,
}

#[state]
pub struct ZkStateImmutable {
    attestations: Option<Map<Option<i32>, Option<AttestationImpl>>>,
    calculationStatus: Option<CalculationStatus>,
    computationState: Option<ComputationStateImpl>,
    defaultOptimisticMode: bool,
    engines: Option<EngineState>,
    externalEvents: Option<ExternalEventState>,
    finishedComputations: Option<Vec<Option<FinishedComputationImpl>>>,
    nextAttestationId: i32,
    nextVariableId: i32,
    nodeRegistryContract: Option<Address>,
    openState: Option<WasmRealContractState>,
    pendingInput: Option<Map<Option<i32>, Option<PendingInputImpl>>>,
    pendingOnChainOpen: Option<Map<Option<i32>, Option<PendingOnChainOpenImpl>>>,
    preProcessMaterials: Option<Map<Option<PreProcessMaterialType>, Option<PreProcessMaterialState>>>,
    preprocessContract: Option<Address>,
    variables: Option<Map<Option<i32>, Option<ZkClosedImpl>>>,
    zkComputationDeadline: i64,
}

pub struct AttestationImpl {
    data: Option<Vec<u8>>,
    id: i32,
    signatures: Option<Vec<Option<Signature>>>,
}

enum CalculationStatus {
    #[discriminant(0)]
    CalculationStatus$WAITING {},
    #[discriminant(1)]
    CalculationStatus$CALCULATING {},
    #[discriminant(2)]
    CalculationStatus$OUTPUT {},
    #[discriminant(3)]
    CalculationStatus$MALICIOUS_BEHAVIOUR {},
    #[discriminant(4)]
    CalculationStatus$DONE {},
}

pub struct ComputationStateImpl {
    calculateFor: i64,
    computationOutput: Option<ComputationOutput>,
    confirmedInputs: i32,
    multiplicationCount: i32,
    offsetIntoFirstTripleBatch: i32,
    onComputeCompleteShortname: Option<Vec<u8>>,
    openState: Option<WasmRealContractState>,
    optimisticMode: bool,
    tripleBatches: Option<Vec<Option<i32>>>,
    variables: Option<Map<Option<i32>, Option<ZkClosedImpl>>>,
}

pub struct ComputationOutput {
    engines: i8,
    outputInformation: Option<Vec<Option<EngineOutputInformation>>>,
    variables: Option<Vec<Option<PendingOutputVariable>>>,
}

pub struct EngineOutputInformation {
    linearCombinationElement: Option<Vec<u8>>,
    partialOpenings: Option<Hash>,
    verificationSeed: Option<Vec<u8>>,
}

pub struct PendingOutputVariable {
    bitLength: i32,
    information: Option<Vec<u8>>,
    variableId: i32,
}

pub struct WasmRealContractState {
    avlTrees: Option<Map<Option<i32>, Option<AvlTreeWrapper>>>,
    computationInput: Option<ComputationInput>,
    openState: Option<Vec<u8>>,
}

pub struct AvlTreeWrapper {
    avlTree: Option<Map<Option<ComparableByteArray>, Option<Vec<u8>>>>,
}

pub struct ComparableByteArray {
    data: Option<Vec<u8>>,
}

pub struct ComputationInput {
    calledFunctionArguments: Option<Vec<Option<Vec<u8>>>>,
    calledFunctionShortname: i32,
}

pub struct ZkClosedImpl {
    id: i32,
    information: Option<Vec<u8>>,
    inputMaskOffset: i32,
    maskedInputShare: Option<Vec<u8>>,
    openValue: Option<Vec<u8>>,
    owner: Option<Address>,
    sealed: bool,
    shareBitLengths: Option<Vec<Option<i32>>>,
    transaction: Option<Hash>,
}

pub struct EngineState {
    engines: Option<Vec<Option<EngineInformation>>>,
}

pub struct EngineInformation {
    identity: Option<Address>,
    publicKey: Option<PublicKey>,
    restInterface: Option<String>,
}

pub struct ExternalEventState {
    confirmedEvents: Option<Map<Option<i32>, Option<ConfirmedEventLog>>>,
    nextEventId: i32,
    nextSubscriptionId: i32,
    subscriptions: Option<Map<Option<i32>, Option<EvmEventSubscription>>>,
}

pub struct ConfirmedEventLog {
    eventId: i32,
    eventLog: Option<EvmEventLog>,
    subscriptionId: i32,
}

pub struct EvmEventLog {
    data: Option<Vec<u8>>,
    metadata: Option<EventMetadata>,
    topics: Option<TopicList>,
}

pub struct EventMetadata {
    blockNumber: Option<u256>,
    logIndex: Option<u256>,
    transactionIndex: Option<u256>,
}

pub struct TopicList {
    topics: Option<Vec<Option<Topic>>>,
}

pub struct Topic {
    topic: Option<String>,
}

pub struct EvmEventSubscription {
    chainId: Option<String>,
    contractAddress: Option<EvmAddress>,
    fromBlock: Option<u256>,
    id: i32,
    isCancelled: bool,
    latestConfirmedEvent: Option<EventMetadata>,
    pendingEvent: Option<PendingEventLog>,
    pendingHeartbeatBlock: Option<PendingHeartbeat>,
    topics: Option<Vec<Option<TopicList>>>,
}

pub struct EvmAddress {
    address: Option<String>,
}

pub struct PendingEventLog {
    candidates: Option<Vec<Option<EvmEventLog>>>,
    eventId: i32,
    subscriptionId: i32,
}

pub struct PendingHeartbeat {
    candidates: Option<Vec<Option<u256>>>,
}

pub struct FinishedComputationImpl {
    calculateFor: i64,
    engineState: i8,
    optimistic: bool,
    partialOpens: Option<Hash>,
}

pub struct PendingInputImpl {
    commitments: Option<Vec<Option<Hash>>>,
    encryptedShares: Option<Vec<Option<Vec<u8>>>>,
    engines: i8,
    nodeOpenings: Option<Vec<Option<Vec<u8>>>>,
    onInputtedShortname: Option<Vec<u8>>,
    publicKey: Option<PublicKey>,
    rejected: bool,
    variable: Option<ZkClosedImpl>,
}

pub struct PendingOnChainOpenImpl {
    bitLengths: Option<Map<Option<i32>, Option<BitLengths>>>,
    outputId: Option<OpenId>,
    shares: Option<Map<Option<i32>, Option<binder_Shares>>>,
    variables: Option<Vec<Option<i32>>>,
}

pub struct BitLengths {
    bitLengths: Option<Vec<Option<i32>>>,
}

pub struct OpenId {
    id: i32,
    receivedForEngine: i8,
}

pub struct binder_Shares {
    shares: Option<Vec<Option<Vec<u8>>>>,
}

enum PreProcessMaterialType {
    #[discriminant(0)]
    PreProcessMaterialType$BINARY_INPUT_MASK {},
    #[discriminant(1)]
    PreProcessMaterialType$BINARY_TRIPLE {},
}

pub struct PreProcessMaterialState {
    batchIds: Option<Vec<Option<i32>>>,
    numElementsToPrefetch: i32,
    numRequestedBatches: i32,
    numUsedElements: i64,
}

pub struct EngineStateRpc {
    engine1: EngineInformationRpc,
    engine2: EngineInformationRpc,
    engine3: EngineInformationRpc,
    engine4: EngineInformationRpc,
}

pub struct EngineInformationRpc {
    identity: Address,
    publicKey: PublicKey,
    restInterface: String,
}

pub struct RealContractBinder_Shares {
    bytes: Vec<u8>,
}

pub struct EvmEventLogRpc {
    metadata: EventMetadataRpc,
    data: Vec<u8>,
    topics: TopicListRpc,
}

pub struct EventMetadataRpc {
    logIndex: u256,
    transactionIndex: u256,
    blockNumber: u256,
}

pub struct TopicListRpc {
    topics: Vec<TopicRpc>,
}

pub struct TopicRpc {
    topic: [u8; 32],
}

#[binder_init(contract_call = init)]
pub fn create (
    engines: EngineStateRpc,
    preprocessContract: Address,
    nodeRegistryContract: Address,
    zkComputationDeadline: i64,
    initialStakingFees: i64,
    numInputMasksToPrefetch: i32,
    numTriplesToPrefetch: i32,
    defaultOptimisticMode: bool,
)

#[binder_action(shortname = 0x00)]
pub fn commitResultVariableOptimistic (
    calculationFor: i64,
    partialOpenings: Hash,
    verificationSeed: [u8; 32],
    linearCombinationElement: i8,
)

#[binder_action(shortname = 0x01)]
pub fn onChainOutput (
    outputId: i32,
    engineShares: Vec<RealContractBinder_Shares>,
)

#[binder_action(shortname = 0x02)]
pub fn unableToCalculate (
    calculationFor: i64,
)

#[binder_action(shortname = 0x03)]
pub fn openMaskedInput (
    variableId: i32,
    maskedInput: Vec<u8>,
)

#[binder_action(shortname = 0x04, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOffChain (
    bitLengths: Vec<i32>,
    commitments: Vec<Hash>,
)

#[binder_action(shortname = 0x05, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOnChain (
    bitLengths: Vec<i32>,
    publicKey: PublicKey,
    encryptedShares: Vec<RealContractBinder_Shares>,
)

#[binder_action(shortname = 0x06)]
pub fn rejectInput (
    variableId: i32,
)

#[binder_action(shortname = 0x09, contract_call = action)]
pub fn openInvocation ()

#[binder_action(shortname = 0x0a)]
pub fn addAttestationSignature (
    attestationId: i32,
    signature: Signature,
)

#[binder_action(shortname = 0x0b)]
pub fn getComputationDeadline ()

#[binder_action(shortname = 0x0c, contract_call = zk_on_compute_complete)]
pub fn onComputeComplete (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0d, contract_call = zk_on_variables_opened)]
pub fn onVariablesOpened (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0e, contract_call = zk_on_attestation_complete)]
pub fn onAttestationComplete (
    id: i32,
)

#[binder_action(shortname = 0x0f, contract_call = zk_on_variable_inputted)]
pub fn onVariableInputted (
    variableId: i32,
)

#[binder_action(shortname = 0x10, contract_call = zk_on_variable_rejected)]
pub fn onVariableRejected (
    variableId: i32,
)

#[binder_action(shortname = 0x12)]
pub fn addBatches (
    batchType: PreProcessMaterialType,
    batchId: i32,
)

#[binder_action(shortname = 0x13)]
pub fn extendZkComputationDeadline (
    msPerGasNumerator: i64,
    msPerGasDenominator: i64,
    minExtension: i64,
    maxExtension: i64,
)

#[binder_action(shortname = 0x14)]
pub fn addExternalEvent (
    subscriptionId: i32,
    observedEvent: EvmEventLogRpc,
)

#[binder_action(shortname = 0x15, contract_call = zk_on_external_event)]
pub fn onExternalEvent (
    subscriptionId: i32,
    eventId: i32,
)

#[binder_action(shortname = 0x16)]
pub fn registerExternalHeartbeatBlock (
    subscriptionId: i32,
    heartbeat: u256,
)

#[binder_action(shortname = 0x18)]
pub fn commitResultVariableOptimisticVerification (
    calculationFor: i64,
    linearCombinationElement: i8,
)

#[binder_action(shortname = 0x19)]
pub fn commitResultVariableFull (
    calculationFor: i64,
    partialOpenings: Hash,
)

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

