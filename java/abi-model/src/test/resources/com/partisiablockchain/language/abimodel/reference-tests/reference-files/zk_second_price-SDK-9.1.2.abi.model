// Binder Version: 7.0.0
// Client Version: 4.1.0

// Binder Type: Zk

#[state]
pub struct ContractState {
    owner: Address,
    registered_bidders: Vec<RegisteredBidder>,
    auction_result: Option<AuctionResult>,
}

pub struct BidderId {
    id: i32,
}

pub struct AuctionResult {
    winner: BidderId,
    second_highest_bid: i32,
}

pub struct RegisteredBidder {
    bidder_id: BidderId,
    address: Address,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(shortname = 0x00)]
pub fn commitResultVariable (
    calculationFor: i64,
    restOfBinderArgs: Vec<u8>,
)

#[binder_action(shortname = 0x01)]
pub fn onChainOutput (
    outputId: i32,
    engineShares: Vec<u8>,
)

#[binder_action(shortname = 0x02)]
pub fn unableToCalculate (
    calculationFor: i64,
)

#[binder_action(shortname = 0x03)]
pub fn openMaskedInput (
    variableId: i32,
    maskedInput: Vec<u8>,
)

#[binder_action(shortname = 0x04, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOffChain (
    bitLengths: Vec<i32>,
    commitment1: Hash,
    commitment2: Hash,
    commitment3: Hash,
    commitment4: Hash,
)

#[binder_action(shortname = 0x05, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOnChain (
    bitLengths: Vec<i32>,
    publicKey: PublicKey,
    encryptedShares: Vec<u8>,
)

#[binder_action(shortname = 0x06)]
pub fn rejectInput (
    variableId: i32,
)

#[binder_action(shortname = 0x09, contract_call = action)]
pub fn openInvocation ()

#[binder_action(shortname = 0x0a)]
pub fn addAttestationSignature (
    attestationId: i32,
    signature: Signature,
)

#[binder_action(shortname = 0x0b)]
pub fn getComputationDeadline ()

#[binder_action(shortname = 0x0c, contract_call = zk_on_compute_complete)]
pub fn onComputeComplete (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0d, contract_call = zk_on_variables_opened)]
pub fn onVariablesOpened (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0e, contract_call = zk_on_attestation_complete)]
pub fn onAttestationComplete (
    id: i32,
)

#[binder_action(shortname = 0x0f, contract_call = zk_on_variable_inputted)]
pub fn onVariableInputted (
    variableId: i32,
)

#[binder_action(shortname = 0x10, contract_call = zk_on_variable_rejected)]
pub fn onVariableRejected (
    variableId: i32,
)

#[binder_action(shortname = 0x11, contract_call = zk_on_user_variables_opened)]
pub fn onUserVariablesOpened (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x12)]
pub fn addBatches (
    batchType: u8,
    batchId: i32,
)

#[binder_action(shortname = 0x13)]
pub fn extendZkComputationDeadline (
    msPerGasNumerator: i64,
    msPerGasDenominator: i64,
    minExtension: i64,
    maxExtension: i64,
)

#[binder_action(shortname = 0x14)]
pub fn addExternalEvent (
    subscriptionId: i32,
    restOfBinderArgs: Vec<u8>,
)

#[binder_action(shortname = 0x15, contract_call = zk_on_secret_input)]
pub fn onExternalEvent (
    subscriptionId: i32,
    eventId: i32,
)

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn initialize ()

#[action(shortname = 0x30)]
pub fn register_bidder (
    bidder_id: i32,
    address: Address,
)

#[action(shortname = 0x01)]
pub fn compute_winner ()

#[zk_on_compute_complete(shortname = 0xbec7c4e409)]
pub fn auction_compute_complete ()

#[zk_on_variables_opened]
pub fn open_auction_variable ()

#[zk_on_attestation_complete]
pub fn auction_results_attested ()

#[zk_on_secret_input(shortname = 0x40, secret_type = "i32")]
pub fn add_bid ()

