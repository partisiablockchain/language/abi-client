// Binder Version: 9.0.0
// Client Version: 5.1.0

// Binder Type: Public

#[state]
pub struct FeeDistributionContractState {
    bp_orchestration_contract: Address,
    block_producers: Vec<Address>,
    external_coins: Vec<ConvertedCoin>,
    fees_to_distribute: Map<Address, [u8; 32]>,
    epochs: Map<i64, EpochInformation>,
    last_epoch_collected: i64,
    shard_ids: Vec<Option<String>>,
    gas_reward_pool: Signed256,
}

pub struct Signed256 {
    value: [u8; 32],
    sign: bool,
}

pub struct ConvertedCoin {
    converted_coins: [u8; 32],
    converted_gas_sum: [u8; 32],
}

pub struct EpochMetrics {
    signature_frequencies: Vec<i32>,
}

pub struct ShardInformation {
    notifying_block_producers: Vec<Address>,
    metrics: Map<Address, EpochMetrics>,
}

pub struct EpochInformation {
    shards: Map<Option<String>, ShardInformation>,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn init (
    bp_orchestration_contract: Address,
    shards: Vec<Option<String>>,
)

#[action(shortname = 0x04)]
pub fn notify_with_metrics (
    shard: Option<String>,
    ended_epoch: i64,
    metrics: Vec<u16>,
)

#[action(shortname = 0x01)]
pub fn update_committee (
    new_block_producers: Vec<Address>,
)

