// Binder Version: 7.0.0
// Client Version: 4.1.0

// Binder Type: Public

#[state]
pub struct AuctionContractState {
    contract_owner: Address,
    start_time_millis: i64,
    end_time_millis: i64,
    token_amount_for_sale: u64,
    token_for_sale: Address,
    token_for_bidding: Address,
    highest_bidder: Bid,
    reserve_price: u64,
    min_increment: u64,
    claim_map: Map<Address, TokenClaim>,
    status: u8,
}

pub struct Bid {
    bidder: Address,
    amount: u64,
}

pub struct TokenClaim {
    tokens_for_bidding: u64,
    tokens_for_sale: u64,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn initialize (
    token_amount_for_sale: u64,
    token_for_sale: Address,
    token_for_bidding: Address,
    reserve_price: u64,
    min_increment: u64,
    auction_duration_hours: u32,
)

#[action(shortname = 0x01)]
pub fn start ()

#[action(shortname = 0x03)]
pub fn bid (
    bid_amount: u64,
)

#[action(shortname = 0x05)]
pub fn claim ()

#[action(shortname = 0x06)]
pub fn execute ()

#[action(shortname = 0x07)]
pub fn cancel ()

#[callback(shortname = 0x02)]
pub fn start_callback ()

#[callback(shortname = 0x04)]
pub fn bid_callback (
    bid: Bid,
)

