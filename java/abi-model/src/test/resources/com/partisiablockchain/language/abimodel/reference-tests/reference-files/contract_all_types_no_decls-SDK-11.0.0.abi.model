// Binder Version: 8.1.0
// Client Version: 5.0.0

// Binder Type: Public

#[state]
pub struct ExampleContractState {
    my_u8: u8,
    my_u16: u16,
    my_u32: u32,
    my_u64: u64,
    my_u128: u128,
    my_i8: i8,
    my_i16: i16,
    my_i32: i32,
    my_i64: i64,
    my_i128: i128,
    my_string: String,
    my_bool: bool,
    my_address: Address,
    my_vec: Vec<u64>,
    my_vec_vec: Vec<Vec<u64>>,
    my_map: Map<Address, u8>,
    my_set: Set<u64>,
    my_complex_map: Map<u64, Vec<u64>>,
    my_array: [u8; 16],
    my_array_2: [u8; 5],
    my_struct: MyStructType,
}

pub struct MyStructType {
    some_value: u64,
    some_vector: Vec<u64>,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn initialize (
    my_u8: u8,
    my_u16: u16,
    my_u32: u32,
    my_u64: u64,
    my_u128: u128,
    my_i8: i8,
    my_i16: i16,
    my_i32: i32,
    my_i64: i64,
    my_i128: i128,
    my_string: String,
    my_bool: bool,
    my_address: Address,
    my_vec: Vec<u64>,
    my_vec_vec: Vec<Vec<u64>>,
    my_array: [u8; 16],
    my_array_2: [u8; 5],
    my_struct: MyStructType,
)

#[action(shortname = 0xf5faeadf04)]
pub fn insert_in_my_set (
    value: u64,
)

#[action(shortname = 0xfc91b4c102)]
pub fn insert_in_my_map (
    address: Address,
    value: u8,
)

#[action(shortname = 0x90b7e3f10c)]
pub fn insert_in_my_complex_map (
    key: u64,
    value: Vec<u64>,
)

#[action(shortname = 0x80a1b19e07)]
pub fn update_my_u8 (
    value: u8,
)

#[action(shortname = 0xb2f8b59804)]
pub fn update_my_u16 (
    value: u16,
)

#[action(shortname = 0xefe5ed810e)]
pub fn update_my_u32 (
    value: u32,
)

#[action(shortname = 0xe4f68de503)]
pub fn update_my_u64 (
    value: u64,
)

#[action(shortname = 0x8fc4c2a607)]
pub fn update_my_u128 (
    value: u128,
)

#[action(shortname = 0xf495f18c0a)]
pub fn update_my_i8 (
    value: i8,
)

#[action(shortname = 0xb9dadd8e0e)]
pub fn update_my_i16 (
    value: i16,
)

#[action(shortname = 0xb3cc80960a)]
pub fn update_my_i32 (
    value: i32,
)

#[action(shortname = 0xadc6ccdc05)]
pub fn update_my_i64 (
    value: i64,
)

#[action(shortname = 0x8ce7ebef06)]
pub fn update_my_i128 (
    value: i128,
)

#[action(shortname = 0x97c18ca406)]
pub fn update_my_string (
    value: String,
)

#[action(shortname = 0xb0a1fab30c)]
pub fn update_my_bool (
    value: bool,
)

#[action(shortname = 0xa7e192a009)]
pub fn update_my_address (
    value: Address,
)

#[action(shortname = 0x9d99e6e401)]
pub fn update_my_vec (
    value: Vec<u64>,
)

#[action(shortname = 0xccc0d6db04)]
pub fn update_my_vec_vec (
    value: Vec<Vec<u64>>,
)

#[action(shortname = 0xcbe4cbfd0d)]
pub fn update_my_array (
    value: [u8; 16],
)

#[action(shortname = 0xec9da775)]
pub fn update_my_array_2 (
    value: [u8; 5],
)

#[action(shortname = 0xf3b68ff40e)]
pub fn update_my_struct (
    value: MyStructType,
)

