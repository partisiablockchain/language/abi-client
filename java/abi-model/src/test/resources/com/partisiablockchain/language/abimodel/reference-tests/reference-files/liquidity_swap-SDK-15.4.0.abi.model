// Binder Version: 9.4.0
// Client Version: 5.2.0

// Binder Type: Public

#[state]
pub struct LiquiditySwapContractState {
    contract: Address,
    token_a_address: Address,
    token_b_address: Address,
    swap_fee_per_mille: u128,
    token_balances: Map<Address, TokenBalance>,
}

enum Token {
    #[discriminant(0)]
    TokenA {},
    #[discriminant(1)]
    TokenB {},
    #[discriminant(2)]
    LiquidityToken {},
}

pub struct TokenBalance {
    a_tokens: u128,
    b_tokens: u128,
    liquidity_tokens: u128,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn initialize (
    token_a_address: Address,
    token_b_address: Address,
    swap_fee_per_mille: u128,
)

#[action(shortname = 0x01)]
pub fn deposit (
    token_address: Address,
    amount: u128,
)

#[action(shortname = 0x02)]
pub fn swap (
    token_address: Address,
    amount: u128,
)

#[action(shortname = 0x03)]
pub fn withdraw (
    token_address: Address,
    amount: u128,
)

#[action(shortname = 0x04)]
pub fn provide_liquidity (
    token_address: Address,
    amount: u128,
)

#[action(shortname = 0x05)]
pub fn reclaim_liquidity (
    liquidity_token_amount: u128,
)

#[action(shortname = 0x06)]
pub fn provide_initial_liquidity (
    token_a_amount: u128,
    token_b_amount: u128,
)

#[callback(shortname = 0x10)]
pub fn deposit_callback (
    token: Token,
    amount: u128,
)

