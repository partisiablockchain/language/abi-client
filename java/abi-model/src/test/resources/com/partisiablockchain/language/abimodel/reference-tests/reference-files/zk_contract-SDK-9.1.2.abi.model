// Binder Version: 7.0.0
// Client Version: 4.1.0

// Binder Type: Zk

#[state]
pub struct ContractState {
    own_variable_id: u64,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(shortname = 0x00)]
pub fn commitResultVariable (
    calculationFor: i64,
    restOfBinderArgs: Vec<u8>,
)

#[binder_action(shortname = 0x01)]
pub fn onChainOutput (
    outputId: i32,
    engineShares: Vec<u8>,
)

#[binder_action(shortname = 0x02)]
pub fn unableToCalculate (
    calculationFor: i64,
)

#[binder_action(shortname = 0x03)]
pub fn openMaskedInput (
    variableId: i32,
    maskedInput: Vec<u8>,
)

#[binder_action(shortname = 0x04, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOffChain (
    bitLengths: Vec<i32>,
    commitment1: Hash,
    commitment2: Hash,
    commitment3: Hash,
    commitment4: Hash,
)

#[binder_action(shortname = 0x05, contract_call = zk_on_secret_input)]
pub fn zeroKnowledgeInputOnChain (
    bitLengths: Vec<i32>,
    publicKey: PublicKey,
    encryptedShares: Vec<u8>,
)

#[binder_action(shortname = 0x06)]
pub fn rejectInput (
    variableId: i32,
)

#[binder_action(shortname = 0x09, contract_call = action)]
pub fn openInvocation ()

#[binder_action(shortname = 0x0a)]
pub fn addAttestationSignature (
    attestationId: i32,
    signature: Signature,
)

#[binder_action(shortname = 0x0b)]
pub fn getComputationDeadline ()

#[binder_action(shortname = 0x0c, contract_call = zk_on_compute_complete)]
pub fn onComputeComplete (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0d, contract_call = zk_on_variables_opened)]
pub fn onVariablesOpened (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x0e, contract_call = zk_on_attestation_complete)]
pub fn onAttestationComplete (
    id: i32,
)

#[binder_action(shortname = 0x0f, contract_call = zk_on_variable_inputted)]
pub fn onVariableInputted (
    variableId: i32,
)

#[binder_action(shortname = 0x10, contract_call = zk_on_variable_rejected)]
pub fn onVariableRejected (
    variableId: i32,
)

#[binder_action(shortname = 0x11, contract_call = zk_on_user_variables_opened)]
pub fn onUserVariablesOpened (
    ids: Vec<i32>,
)

#[binder_action(shortname = 0x12)]
pub fn addBatches (
    batchType: u8,
    batchId: i32,
)

#[binder_action(shortname = 0x13)]
pub fn extendZkComputationDeadline (
    msPerGasNumerator: i64,
    msPerGasDenominator: i64,
    minExtension: i64,
    maxExtension: i64,
)

#[binder_action(shortname = 0x14)]
pub fn addExternalEvent (
    subscriptionId: i32,
    restOfBinderArgs: Vec<u8>,
)

#[binder_action(shortname = 0x15, contract_call = zk_on_secret_input)]
pub fn onExternalEvent (
    subscriptionId: i32,
    eventId: i32,
)

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn initialize (
    mode: u8,
)

#[action(shortname = 0x00)]
pub fn some_action ()

#[action(shortname = 0x01)]
pub fn to_contract_done ()

#[action(shortname = 0x02)]
pub fn start_computation_one_output ()

#[action(shortname = 0x0b)]
pub fn start_computation_no_outputs ()

#[action(shortname = 0x0c)]
pub fn start_computation_two_outputs ()

#[action(shortname = 0x03)]
pub fn new_computation ()

#[action(shortname = 0x04)]
pub fn output_complete_delete_variable ()

#[action(shortname = 0x05)]
pub fn contract_done ()

#[action(shortname = 0x06)]
pub fn transfer_variable_one ()

#[action(shortname = 0x07)]
pub fn delete_variable_one ()

#[action(shortname = 0x0f)]
pub fn delete_pending_input_one ()

#[action(shortname = 0x08)]
pub fn open_variable_one ()

#[action(shortname = 0x09)]
pub fn delete_variable_three ()

#[action(shortname = 0x0a)]
pub fn verify_one_pending_input ()

#[action(shortname = 0x10)]
pub fn verify_one_variable ()

#[action(shortname = 0x0d)]
pub fn update_state (
    current_state: u64,
    next_state: u64,
)

#[callback(shortname = 0x40)]
pub fn on_callback ()

#[zk_on_variable_inputted(shortname = 0x9a8995c30d)]
pub fn zk_on_variable_inputted ()

#[zk_on_variable_rejected]
pub fn zk_on_variable_rejected ()

#[zk_on_compute_complete(shortname = 0xcff2f5810d)]
pub fn zk_on_compute_complete ()

#[zk_on_variables_opened]
pub fn zk_on_variables_opened ()

#[zk_on_user_variables_opened]
pub fn zk_on_user_variables_opened (
    rpc1: u8,
)

#[zk_on_secret_input(shortname = 0x60, secret_type = "i32")]
pub fn secret_input_bit_length_10 ()

#[zk_on_secret_input(shortname = 0x61, secret_type = "i32")]
pub fn secret_input_bit_length_10_10 ()

#[zk_on_secret_input(shortname = 0x62, secret_type = "i32")]
pub fn secret_input_bit_length_1 ()

#[zk_on_secret_input(shortname = 0x63, secret_type = "i32")]
pub fn secret_input_bit_length_1001x1 ()

#[zk_on_secret_input(shortname = 0x64, secret_type = "i32")]
pub fn secret_input_bit_length_32_10_10 ()

#[zk_on_secret_input(shortname = 0x65, secret_type = "i32")]
pub fn secret_input_bit_length_32_32 ()

#[zk_on_secret_input(shortname = 0x66, secret_type = "i32")]
pub fn secret_input_bit_length_12 ()

#[zk_on_secret_input(shortname = 0x67, secret_type = "i32")]
pub fn secret_input_bit_length_32_1_1 ()

#[zk_on_secret_input(shortname = 0x68, secret_type = "i32")]
pub fn secret_input_bit_length_one_variadic (
    bitlength: u32,
)

#[zk_on_secret_input(shortname = 0x69, secret_type = "i32")]
pub fn secret_input_sealed_single_bit_with_info ()

