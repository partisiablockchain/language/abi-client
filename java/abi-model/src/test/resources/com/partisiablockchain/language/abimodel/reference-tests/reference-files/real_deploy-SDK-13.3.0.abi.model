// Binder Version: 9.2.0
// Client Version: 5.0.0

// Binder Type: Public

#[state]
pub struct RealDeployContractState {
    binding_jar: Vec<u8>,
    pre_process: Address,
    zk_node_registry: Address,
    supported_protocol_version: SemanticVersion,
}

pub struct SemanticVersion {
    major: u32,
    minor: u32,
    patch: u32,
}

pub struct BlockchainPublicKey {
    ec_point: [u8; 33],
}

pub struct EngineInformation {
    identity: Address,
    public_key: BlockchainPublicKey,
    rest_endpoint: String,
}

#[binder_init(contract_call = init)]
pub fn create ()

#[binder_action(contract_call = action)]
pub fn invoke ()

#[binder_callback(contract_call = callback)]
pub fn callback ()

#[binder_empty_action]
pub fn topUpGas ()

#[init(shortname = 0xffffffff0f)]
pub fn init (
    binding_jar: Vec<u8>,
    pre_process: Address,
    zk_node_registry: Address,
    supported_protocol_version: SemanticVersion,
)

#[action(shortname = 0x00)]
pub fn deploy_contract_v1 (
    contract_jar: Vec<u8>,
    initialization: Vec<u8>,
    abi: Vec<u8>,
    required_stakes: i64,
)

#[action(shortname = 0x01)]
pub fn deploy_contract_v2 (
    contract_jar: Vec<u8>,
    initialization: Vec<u8>,
    abi: Vec<u8>,
    required_stakes: i64,
    jurisdictions: Vec<Vec<i32>>,
)

#[callback(shortname = 0x00)]
pub fn node_request_callback (
    contract_address: Address,
    contract_jar: Vec<u8>,
    initialization: Vec<u8>,
    abi: Vec<u8>,
    tokens_locked_to_contract: i64,
)

