package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * Abi-specification of a binder. Only used internally while building the contract ABI.
 *
 * <p>The binder contains information about binder state, binder invocations, and which kinds of
 * contract invocations an underlying contract can have.
 *
 * @param binderType type of binder
 * @param invocationKinds list of callable contract invocation kinds
 * @param namedTypes list of named types
 * @param binderInvocations list of binderInvocations
 * @param stateType type of the binder state
 */
public record BinderAbi(
    BinderType binderType,
    List<ContractInvocationKind> invocationKinds,
    List<NamedTypeSpec> namedTypes,
    List<BinderInvocation> binderInvocations,
    TypeSpec stateType)
    implements ChainComponent {

  /**
   * Returns the named type given by the specified reference.
   *
   * @param namedTypeRef the reference to the named type
   * @return the named type given by the reference
   */
  @Override
  public NamedTypeSpec getNamedType(NamedTypeRef namedTypeRef) {
    return namedTypes.get(namedTypeRef.index());
  }

  /**
   * Returns the named type given by the specified name.
   *
   * @param name the name of the named type
   * @return the named type with the given name or null if no such named type exist.
   */
  @Override
  public NamedTypeSpec getNamedType(String name) {
    return namedTypes().stream()
        .filter((NamedTypeSpec namedType) -> namedType.name().equals(name))
        .findFirst()
        .orElse(null);
  }

  /**
   * Returns the state struct or null if the state is not a struct.
   *
   * @return the state struct
   */
  public StructTypeSpec getStateStruct() {
    if (stateType instanceof NamedTypeRef namedTypeRef) {
      return (StructTypeSpec) namedTypes.get(namedTypeRef.index());
    } else {
      return null;
    }
  }
}
