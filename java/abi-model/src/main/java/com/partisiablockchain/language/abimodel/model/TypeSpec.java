package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.function.Function.identity;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Interface for the possible types of the contract. Subtypes implement this interface to mark them
 * as valid TypeSpec types as specified by the abi grammar
 */
public interface TypeSpec {

  /**
   * The typeIndex of this type spec object.
   *
   * @return the typeIndex
   */
  TypeIndex typeIndex();

  /** This enum encodes all the typeIndexes in the ABI. */
  @SuppressWarnings("Immutable")
  enum TypeIndex {
    /** u8 representation. */
    u8(0x01),
    /** u16 representation. */
    u16(0x02),
    /** u32 representation. */
    u32(0x03),
    /** u64 representation. */
    u64(0x04),
    /** u128 representation. */
    u128(0x05),
    /** i8 representation. */
    i8(0x06),
    /** i16 representation. */
    i16(0x07),
    /** i32 representation. */
    i32(0x08),
    /** i64 representation. */
    i64(0x09),
    /** i128 representation. */
    i128(0x0a),
    /** String representation. */
    String(0x0b),
    /** bool representation. */
    bool(0x0c),
    /** Address representation. */
    Address(0x0d),
    /** Named type representation. */
    Named(0x00),
    /** Vector representation. */
    Vec(0x0e),
    /** Map representation. */
    Map(0x0f),
    /** Set representation. */
    Set(0x10),
    /**
     * Byte array representation.
     *
     * @deprecated Replaced by {@link SizedArray}.
     */
    @Deprecated
    SizedByteArray(0x11),
    /** Option representation. */
    Option(0x12),
    /** Hash representation. */
    Hash(0x13),
    /** PublicKey representation. */
    PublicKey(0x14),
    /** Signature representation. */
    Signature(0x15),
    /** BlsPublicKey representation. */
    BlsPublicKey(0x16),
    /** BlsSignature representation. */
    BlsSignature(0x17),
    /** u256 representation. */
    u256(0x18),
    /** AvlTreeMap representation. */
    AvlTreeMap(0x19),
    /** Generic array representation. Super-representation of {@link SizedByteArray}. */
    SizedArray(0x1A),
    /** Any unknown typeIndex. */
    Unknown(0xFF);

    private final int value;

    TypeIndex(int value) {
      this.value = value;
    }

    /**
     * Retrieves the id of the type, as specified by the abi spec.
     *
     * @return the id of the type
     */
    public int getValue() {
      return value;
    }

    private static final java.util.Map<Integer, TypeIndex> TYPE_INDEX_MAP =
        Arrays.stream(values()).collect(Collectors.toMap(TypeIndex::getValue, identity()));

    /**
     * Parse the type from the given byte value.
     *
     * @param value the value to parse
     * @return the corresponding {@link TypeIndex} or {@link #Unknown}
     */
    public static TypeIndex fromValue(int value) {
      return TYPE_INDEX_MAP.getOrDefault(value, Unknown);
    }
  }
}
