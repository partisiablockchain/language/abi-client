package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.Configuration.ChainComponentFormat;
import java.util.List;

/**
 * Upgrade a contract abi parsed with {@link ChainComponentFormat#ONLY_CONTRACT} to the new binder
 * ABI format using a default binder abi.
 */
public final class ImplicitBinderAbi {

  private ImplicitBinderAbi() {}

  /** Default function kinds used before binder ABI. */
  public static final class DefaultKinds {
    private DefaultKinds() {}

    /** Init function kind. */
    public static final ContractInvocationKind INIT =
        new ContractInvocationKind(0x01, "INIT", true);

    /** Action function kind. */
    public static final ContractInvocationKind ACTION =
        new ContractInvocationKind(0x02, "ACTION", true);

    /** Callback function kind. */
    public static final ContractInvocationKind CALLBACK =
        new ContractInvocationKind(0x03, "CALLBACK", true);

    /** On secret input function kind. */
    public static final ContractInvocationKind ZK_ON_SECRET_INPUT =
        new ContractInvocationKind(0x10, "ZK_ON_SECRET_INPUT", true);

    /** On variable inputted function kind. */
    public static final ContractInvocationKind ZK_ON_VARIABLE_INPUTTED =
        new ContractInvocationKind(0x11, "ZK_ON_VARIABLE_INPUTTED", true);

    /** On variable rejected function kind. */
    public static final ContractInvocationKind ZK_ON_VARIABLE_REJECTED =
        new ContractInvocationKind(0x12, "ZK_ON_VARIABLE_REJECTED", false);

    /** On compute complete function kind. */
    public static final ContractInvocationKind ZK_ON_COMPUTE_COMPLETE =
        new ContractInvocationKind(0x13, "ZK_ON_COMPUTE_COMPLETE", true);

    /** On variable opened function kind. */
    public static final ContractInvocationKind ZK_ON_VARIABLES_OPENED =
        new ContractInvocationKind(0x14, "ZK_ON_VARIABLES_OPENED", false);

    /** On user variable opened function kind. */
    public static final ContractInvocationKind ZK_ON_USER_VARIABLES_OPENED =
        new ContractInvocationKind(0x15, "ZK_ON_USER_VARIABLES_OPENED", false);

    /** On attestation complete function kind. */
    public static final ContractInvocationKind ZK_ON_ATTESTATION_COMPLETE =
        new ContractInvocationKind(0x16, "ZK_ON_ATTESTATION_COMPLETE", false);

    /** On secret input with explicit type function kind. */
    public static final ContractInvocationKind ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE =
        new ContractInvocationKind(0x17, "ZK_ON_SECRET_INPUT", true);

    /** On external event function kind. */
    public static final ContractInvocationKind ZK_ON_EXTERNAL_EVENT =
        new ContractInvocationKind(0x18, "ZK_ON_EXTERNAL_EVENT", false);
  }

  /** Default zk fn kinds as function kinds. */
  public static final List<ContractInvocationKind> DEFAULT_ZK_FUNCTION_KINDS =
      List.of(
          DefaultKinds.INIT,
          DefaultKinds.ACTION,
          DefaultKinds.CALLBACK,
          DefaultKinds.ZK_ON_SECRET_INPUT,
          DefaultKinds.ZK_ON_VARIABLE_INPUTTED,
          DefaultKinds.ZK_ON_VARIABLE_REJECTED,
          DefaultKinds.ZK_ON_COMPUTE_COMPLETE,
          DefaultKinds.ZK_ON_VARIABLES_OPENED,
          DefaultKinds.ZK_ON_USER_VARIABLES_OPENED,
          DefaultKinds.ZK_ON_ATTESTATION_COMPLETE,
          DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE,
          DefaultKinds.ZK_ON_EXTERNAL_EVENT);

  /** Default zk fn kinds as function kinds. */
  public static final List<ContractInvocationKind> DEFAULT_PUB_FUNCTION_KINDS =
      List.of(DefaultKinds.INIT, DefaultKinds.ACTION, DefaultKinds.CALLBACK);

  /**
   * Test if a kind id is a zk kind.
   *
   * @param kindId the id of the kind to test
   * @return true if the kind is a zk kind
   */
  public static boolean isZkKind(int kindId) {
    return kindId == DefaultKinds.ZK_ON_SECRET_INPUT.kindId()
        || kindId == DefaultKinds.ZK_ON_COMPUTE_COMPLETE.kindId()
        || kindId == DefaultKinds.ZK_ON_USER_VARIABLES_OPENED.kindId()
        || kindId == DefaultKinds.ZK_ON_VARIABLE_INPUTTED.kindId()
        || kindId == DefaultKinds.ZK_ON_VARIABLES_OPENED.kindId()
        || kindId == DefaultKinds.ZK_ON_VARIABLE_REJECTED.kindId()
        || kindId == DefaultKinds.ZK_ON_ATTESTATION_COMPLETE.kindId()
        || kindId == DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId()
        || kindId == DefaultKinds.ZK_ON_EXTERNAL_EVENT.kindId();
  }

  /**
   * Get default pub binder invocations for contracts compiled before binder abi were introduced.
   *
   * @return list of all default pub binder invocations
   */
  public static List<BinderInvocation> defaultPubInvocations() {
    return List.of(
        new BinderInvocation(TransactionKind.Init, "create", null, List.of(), 0x01),
        new BinderInvocation(TransactionKind.Action, "invoke", null, List.of(), 0x02),
        new BinderInvocation(TransactionKind.Callback, "callback", null, List.of(), 0x03),
        new BinderInvocation(TransactionKind.EmptyAction, "topUpGas", null, List.of(), null));
  }

  /**
   * Get default zk binder invocations for contracts compiled before binder abi were introduced.
   *
   * @return list of all default zk binder invocations
   */
  public static List<BinderInvocation> defaultZkInvocations() {

    List<BinderInvocation> binderInvocations =
        List.of(
            new BinderInvocation(TransactionKind.Init, "create", null, List.of(), 0x01),
            new BinderInvocation(
                TransactionKind.Action,
                "commitResultVariable",
                (byte) 0x00,
                List.of(
                    new ArgumentAbi("calculationFor", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
                    new ArgumentAbi(
                        "restOfBinderArgs",
                        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "onChainOutput",
                (byte) 0x01,
                List.of(
                    new ArgumentAbi("outputId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                    new ArgumentAbi(
                        "engineShares",
                        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "unableToCalculate",
                (byte) 0x02,
                List.of(
                    new ArgumentAbi("calculationFor", new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "openMaskedInput",
                (byte) 0x03,
                List.of(
                    new ArgumentAbi("variableId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                    new ArgumentAbi(
                        "maskedInput", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "zeroKnowledgeInputOffChain",
                (byte) 0x04,
                List.of(
                    new ArgumentAbi(
                        "bitLengths", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                    new ArgumentAbi("commitment1", new SimpleTypeSpec(TypeSpec.TypeIndex.Hash)),
                    new ArgumentAbi("commitment2", new SimpleTypeSpec(TypeSpec.TypeIndex.Hash)),
                    new ArgumentAbi("commitment3", new SimpleTypeSpec(TypeSpec.TypeIndex.Hash)),
                    new ArgumentAbi("commitment4", new SimpleTypeSpec(TypeSpec.TypeIndex.Hash))),
                0x17),
            new BinderInvocation(
                TransactionKind.Action,
                "zeroKnowledgeInputOnChain",
                (byte) 0x05,
                List.of(
                    new ArgumentAbi(
                        "bitLengths", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                    new ArgumentAbi("publicKey", new SimpleTypeSpec(TypeSpec.TypeIndex.PublicKey)),
                    new ArgumentAbi(
                        "encryptedShares",
                        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                0x17),
            new BinderInvocation(
                TransactionKind.Action,
                "rejectInput",
                (byte) 0x06,
                List.of(new ArgumentAbi("variableId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                null),
            new BinderInvocation(
                TransactionKind.Action, "openInvocation", (byte) 0x09, List.of(), 0x02),
            new BinderInvocation(
                TransactionKind.Action,
                "addAttestationSignature",
                (byte) 0x0a,
                List.of(
                    new ArgumentAbi("attestationId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                    new ArgumentAbi("signature", new SimpleTypeSpec(TypeSpec.TypeIndex.Signature))),
                null),
            new BinderInvocation(
                TransactionKind.Action, "getComputationDeadline", (byte) 0x0b, List.of(), null),
            new BinderInvocation(
                TransactionKind.Action,
                "onComputeComplete",
                (byte) 0x0c,
                List.of(
                    new ArgumentAbi(
                        "ids", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))),
                0x13),
            new BinderInvocation(
                TransactionKind.Action,
                "onVariablesOpened",
                (byte) 0x0d,
                List.of(
                    new ArgumentAbi(
                        "ids", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))),
                0x14),
            new BinderInvocation(
                TransactionKind.Action,
                "onAttestationComplete",
                (byte) 0x0e,
                List.of(new ArgumentAbi("id", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                0x16),
            new BinderInvocation(
                TransactionKind.Action,
                "onVariableInputted",
                (byte) 0x0f,
                List.of(new ArgumentAbi("variableId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                0x11),
            new BinderInvocation(
                TransactionKind.Action,
                "onVariableRejected",
                (byte) 0x10,
                List.of(new ArgumentAbi("variableId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                0x12),
            new BinderInvocation(
                TransactionKind.Action,
                "onUserVariablesOpened",
                (byte) 0x11,
                List.of(
                    new ArgumentAbi(
                        "ids", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))),
                0x15),
            new BinderInvocation(
                TransactionKind.Action,
                "addBatches",
                (byte) 0x12,
                List.of(
                    new ArgumentAbi("batchType", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)),
                    new ArgumentAbi("batchId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "extendZkComputationDeadline",
                (byte) 0x13,
                List.of(
                    new ArgumentAbi(
                        "msPerGasNumerator", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
                    new ArgumentAbi(
                        "msPerGasDenominator", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
                    new ArgumentAbi("minExtension", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
                    new ArgumentAbi("maxExtension", new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "addExternalEvent",
                (byte) 0x14,
                List.of(
                    new ArgumentAbi("subscriptionId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                    new ArgumentAbi(
                        "restOfBinderArgs",
                        new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                null),
            new BinderInvocation(
                TransactionKind.Action,
                "onExternalEvent",
                (byte) 0x15,
                List.of(
                    new ArgumentAbi("subscriptionId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                    new ArgumentAbi("eventId", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                0x17),
            new BinderInvocation(TransactionKind.Callback, "callback", null, List.of(), 0x03),
            new BinderInvocation(TransactionKind.EmptyAction, "topUpGas", null, List.of(), null));

    return binderInvocations;
  }
}
