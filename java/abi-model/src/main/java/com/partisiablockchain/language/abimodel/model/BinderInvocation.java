package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * The binder part of an invocation.
 *
 * <p>Defines how to build/read the RPC of this invocation. A binder invocation can either be a
 * standalone invocation or it can call a contract function defined by a {@link ContractInvocation}.
 * It specifies which type of contract function to call through a function kind. The rest of the RPC
 * can then be built/read by the contract function.
 *
 * @param kind the kind of invocation
 * @param name the name of the invocation
 * @param shortname the shortname of the invocation. Null if the invocation has no shortname
 * @param arguments the arguments of the invocation.
 * @param contractCallKindId if non-null the binder invocation calls a contract function with the
 *     kind id
 */
public record BinderInvocation(
    TransactionKind kind,
    String name,
    Byte shortname,
    List<ArgumentAbi> arguments,
    Integer contractCallKindId) {}
