package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Pretty printer for printing a contract abi in a rust-like syntax. Given a FileAbi, the pretty
 * printer offers methods to display the entire abi or a specified function/struct, in a rust-like
 * syntax.
 */
public final class RustSyntaxPrettyPrinter {

  private final FileAbi model;
  private final ContractAbi contract;
  private final TypeSpecStringifier typeStringifier;

  /**
   * Constructor for generating a RustSyntaxPrettyPrinter.
   *
   * @param model the model to pretty print.
   */
  public RustSyntaxPrettyPrinter(FileAbi model) {
    this.model = model;
    this.contract = model.contract();
    this.typeStringifier =
        new TypeSpecStringifier(contract.namedTypes().stream().map(NamedTypeSpec::name).toList());
  }

  /**
   * Prints the actions and init function.
   *
   * @return string representation of the functions in a rust-like syntax
   */
  public String printActions() {
    StringBuilder builder = new StringBuilder();
    printFunctions(
        builder,
        f ->
            f.kind().kindId() == ImplicitBinderAbi.DefaultKinds.ACTION.kindId()
                || f.kind().kindId() == ImplicitBinderAbi.DefaultKinds.INIT.kindId());
    return builder.toString();
  }

  /**
   * Prints all functions.
   *
   * @return string representation of the functions in a rust-like syntax
   */
  public String printAllInvocations() {
    StringBuilder builder = new StringBuilder();
    printFunctions(builder, f -> true);
    return builder.toString();
  }

  /** Helper function for printing functions specified by a given predicate. */
  private void printFunctions(StringBuilder builder, Predicate<ContractInvocation> functionKinds) {
    ContractAbi contract = model.contract();
    List<ContractInvocation> functions =
        contract.contractInvocations().stream().filter(functionKinds).toList();
    for (final ContractInvocation function : functions) {
      buildFunction(builder, function);
      builder.append("\n");
    }
  }

  /**
   * Prints the callback functions.
   *
   * @return string representation of the callback functions in a rust-like syntax
   */
  public String printCallbacks() {
    StringBuilder builder = new StringBuilder();
    printFunctions(
        builder, f -> f.kind().kindId() == ImplicitBinderAbi.DefaultKinds.CALLBACK.kindId());
    return builder.toString();
  }

  /**
   * Prints the specified contract state.
   *
   * @return string representation of the state in a rust-like syntax
   */
  public String printState() {
    StringBuilder builder = new StringBuilder();
    BinderRustSyntaxPrettyPrinter.buildStateDefinition(
        builder, typeStringifier, contract.namedTypes(), contract.stateType());
    return builder.toString();
  }

  /**
   * Prints the binder version.
   *
   * @return string representation of the binder version in a rust-like syntax
   */
  public String printBinderVersion() {
    StringBuilder builder = new StringBuilder();
    builder.append("// Binder Version: ");
    buildVersion(builder, model.versionBinder());
    return builder.toString();
  }

  /**
   * Prints the client version.
   *
   * @return string representation of the client version in a rust-like syntax
   */
  public String printClientVersion() {
    StringBuilder builder = new StringBuilder();
    builder.append("// Client Version: ");
    buildVersion(builder, model.versionClient());
    return builder.toString();
  }

  /**
   * Prints the specified model.
   *
   * @return string representation of the model in a rust-like syntax
   */
  public String printModel() {
    StringBuilder builder = new StringBuilder();

    var headerBuilder = new DocBuilder("//! ", builder, typeStringifier);
    for (String line : model.description()) {
      headerBuilder.appendLine(line);
    }

    if (model.description().size() > 0) {
      headerBuilder.appendLine("\n\n");
    }

    // Build version information
    builder.append(printBinderVersion());
    builder.append(printClientVersion());
    builder.append("\n");

    // Build binder type
    builder.append("// Binder Type: ").append(contract.binderType());
    builder.append("\n\n");

    // Build list of struct definitions
    BinderRustSyntaxPrettyPrinter.buildStateDefinition(
        builder, typeStringifier, contract.namedTypes(), contract.stateType());

    BinderRustSyntaxPrettyPrinter.buildNamedTypes(
        builder, typeStringifier, contract.namedTypes(), contract.stateType());
    for (final BinderInvocation invocation : contract.binderInvocations()) {
      BinderRustSyntaxPrettyPrinter.buildBinderInvocation(
          builder, typeStringifier, contract.invocationKinds(), invocation);
    }

    builder.append(printAllInvocations());

    return builder.toString();
  }

  private void buildFunction(StringBuilder builder, ContractInvocation function) {
    buildFunctionCall(builder, function, function.arguments());
  }

  private void buildVersion(StringBuilder builder, AbiVersion version) {
    builder
        .append(version.major())
        .append(".")
        .append(version.minor())
        .append(".")
        .append(version.patch())
        .append("\n");
  }

  /**
   * Generates a string representation of a contract function.
   *
   * @param function the function to print
   * @return string representation of the function
   */
  public String printFunction(ContractInvocation function) {
    StringBuilder builder = new StringBuilder();
    buildFunction(builder, function);
    return builder.toString();
  }

  /**
   * Generates a string representation of a binder invocation.
   *
   * @param binderInvocation the binder invocation to print
   * @return string representation of the function
   */
  public String printBinderInvocation(BinderInvocation binderInvocation) {
    StringBuilder builder = new StringBuilder();
    BinderRustSyntaxPrettyPrinter.buildBinderInvocation(
        builder, typeStringifier, contract.invocationKinds(), binderInvocation);
    return builder.toString();
  }

  private void buildFunctionCall(
      StringBuilder builder, ContractInvocation contractCall, List<ArgumentAbi> arguments) {
    BinderRustSyntaxPrettyPrinter.buildDocumentationForFn(
        builder, typeStringifier, contractCall, contractCall.doc());
    String contractKindName = fnKindToRustName(contractCall.kind());
    builder.append("#[").append(contractKindName);
    if (contractCall.kind().hasShortname()) {
      builder.append("(shortname = 0x").append(contractCall.shortnameAsString());
      if (contractCall.secretArgument() != null) {
        builder.append(", secret_type = \"");
        typeStringifier.stringify(builder, contractCall.secretArgument().type());
        builder.append("\"");
      }
      builder.append(")");
    }
    builder.append("]\n");
    builder.append("pub fn ").append(contractCall.name()).append(" (");
    if (!arguments.isEmpty()) {
      builder.append("\n");
    }
    for (final ArgumentAbi arg : arguments) {
      BinderRustSyntaxPrettyPrinter.buildArgument(builder, typeStringifier, arg);
    }
    builder.append(")\n");
  }

  /**
   * Generates a string representation of a struct.
   *
   * @param struct the struct to print
   * @return string representation of the struct
   */
  public String printStruct(StructTypeSpec struct) {
    StringBuilder builder = new StringBuilder();
    StructTypeSpec state = contract.getStateStruct();
    if (struct.equals(state)) {
      BinderRustSyntaxPrettyPrinter.buildStateDefinition(
          builder, typeStringifier, contract.namedTypes(), contract.stateType());
    } else {
      BinderRustSyntaxPrettyPrinter.buildStructDefinition(builder, typeStringifier, struct);
    }
    return builder.toString();
  }

  private String fnKindToRustName(ContractInvocationKind kind) {
    return kind.name().toLowerCase(Locale.ENGLISH);
  }

  /** Builder for documentation. */
  public static final class DocBuilder {

    private final String prefix;
    private final StringBuilder builder;
    private final TypeSpecStringifier typeSpecPrinter;

    /**
     * Constructs a documentation builder.
     *
     * @param prefix the prefix to add to each line
     * @param builder the string builder to build upon
     * @param typeSpecPrinter the typeSpecPrinter used for printing type specifications
     */
    public DocBuilder(String prefix, StringBuilder builder, TypeSpecStringifier typeSpecPrinter) {
      this.prefix = prefix;
      this.builder = builder;
      this.typeSpecPrinter = typeSpecPrinter;
    }

    /**
     * Append a paragraph of text with a trailing newline.
     *
     * @param paragraph the paragraph
     */
    public void appendParagraph(List<String> paragraph) {
      for (String line : paragraph) {
        appendLine(line);
      }

      if (paragraph.size() > 0) {
        appendLine("");
      }
    }

    /**
     * Append a line of text.
     *
     * @param line the line
     */
    public void appendLine(String line) {
      builder.append(prefix).append(line).append("\n");
    }

    /**
     * Append a list of function arguments.
     *
     * @param arguments the arguments of the function
     * @param descriptions the descriptions for the arguments
     */
    void appendArguments(List<ArgumentAbi> arguments, Map<String, String> descriptions) {
      appendGenericParamList("# Parameters", arguments, descriptions);
    }

    /**
     * Append a list of struct fields.
     *
     * @param fields the fields of the struct
     * @param descriptions the descriptions for the fields
     */
    void appendFields(List<FieldAbi> fields, Map<String, String> descriptions) {
      appendGenericParamList("### Fields", fields, descriptions);
    }

    /**
     * Append a section to the documentation. The sections will start with the section header,
     * followed by the params and their individual descriptions.
     *
     * @param sectionHeader the header of the section
     * @param params the parameters for the section
     * @param paramDescriptions the description of each parameter in the section.
     */
    public void appendGenericParamList(
        String sectionHeader,
        List<? extends NameAndType> params,
        Map<String, String> paramDescriptions) {

      if (countParamLines(params, paramDescriptions) > 0) {
        appendLine(sectionHeader);
        appendLine("");

        for (NameAndType arg : params) {
          String argName = arg.name();
          TypeSpec argTypeSpec = arg.type();

          String description = paramDescriptions.get(argName);
          if (description != null) {
            appendArgOrField(argName, argTypeSpec, description);
          }
        }
        appendLine("");
      }
    }

    int countParamLines(List<? extends NameAndType> params, Map<String, String> descriptions) {
      int count = 0;
      for (NameAndType arg : params) {
        String argName = arg.name();
        if (descriptions.get(argName) != null) {
          count++;
        }
      }

      return count;
    }

    /**
     * Append a description of an argument/field.
     *
     * @param name the name
     * @param type the type
     * @param description the description
     */
    void appendArgOrField(String name, TypeSpec type, String description) {
      builder.append(prefix).append("* `").append(name).append("`: ");

      builder.append("[`");
      typeSpecPrinter.stringify(builder, type);
      builder.append("`], ");

      builder.append(description).append("\n");
    }

    /**
     * Append a return section.
     *
     * @param returns The string to write in the return section.
     */
    public void appendReturns(String returns) {
      appendLine("# Returns:");
      appendLine("");
      appendLine(returns);
      appendLine("");
    }

    /**
     * Build the documentation.
     *
     * @return The documentation as a string.
     */
    public String build() {
      return builder.toString();
    }
  }
}
