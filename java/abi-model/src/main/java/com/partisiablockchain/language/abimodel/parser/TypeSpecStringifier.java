package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpecWithGenerics;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/** A small class that creates Rust-like string representations of a {@link TypeSpec}. */
public final class TypeSpecStringifier {

  private final List<String> namedTypes;

  /**
   * Create a new stringifier.
   *
   * @param namedTypes the list of valid named types
   */
  public TypeSpecStringifier(List<String> namedTypes) {
    this.namedTypes = Objects.requireNonNull(namedTypes);
  }

  /**
   * Create a string representation of a {@link TypeSpec}.
   *
   * @param type the {@link TypeSpec}
   * @return a human-readable string describing the type
   */
  public String stringify(TypeSpec type) {
    var builder = new StringBuilder();
    stringify(builder, type);
    return builder.toString();
  }

  /**
   * Create a string representation of a {@link TypeSpec} appending it to a string builder.
   *
   * @param builder the builder to append to
   * @param uncheckedType the {@link TypeSpec} to format.
   */
  public void stringify(final StringBuilder builder, final TypeSpec uncheckedType) {
    if (uncheckedType instanceof SizedArrayTypeSpec type) {
      builder.append("[");
      stringify(builder, type.valueType());
      builder.append("; ").append(type.length()).append("]");

    } else if (uncheckedType instanceof TypeSpecWithGenerics type) {
      buildGenericPart(builder, type);

    } else if (uncheckedType instanceof NamedTypeRef type) {
      builder.append(namedTypes.get(type.index()));

    } else /* if (uncheckedType instanceof SimpleTypeSpec simpleTypeSpec)*/ {
      builder.append(uncheckedType.typeIndex());
    }
  }

  private void buildGenericPart(StringBuilder builder, TypeSpecWithGenerics type) {
    String joined =
        type.getGenerics().stream().map(this::stringify).collect(Collectors.joining(", "));

    builder.append(type.typeName()).append("<").append(joined).append(">");
  }
}
