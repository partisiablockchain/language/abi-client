package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;

/** Type of binder. */
public enum BinderType {
  /** Public binder. */
  Public(0),
  /** ZK binder. */
  Zk(1),
  /** Sys binder. */
  Sys(2);

  private final int id;

  BinderType(int id) {
    this.id = id;
  }

  /**
   * Create from serialized id.
   *
   * @param id id of binder type
   * @return binder type
   */
  public static BinderType fromId(int id) {
    return Arrays.stream(values()).filter(p -> p.id == id).findAny().orElseThrow();
  }

  /**
   * Get the serialized id for the binder type.
   *
   * @return serialized id
   */
  public int getId() {
    return id;
  }
}
