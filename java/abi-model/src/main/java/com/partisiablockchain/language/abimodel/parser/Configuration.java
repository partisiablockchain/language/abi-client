package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration of the parser depending on binder and client versions.
 *
 * @param shortnameType represents the parser's strategy used for parsing shortnames; HASH or
 *     LEB128.
 * @param fnType represents the parser's strategy for parsing actions; InitSeparately or FnKind.
 * @param shortnameLength describes the length of the shortname is using HASH - otherwise this is
 *     Null.
 * @param namedTypesFormat indicates whether enums are supported as named types; OnlyStructs or
 *     StructsAndEnum.
 * @param typeAndFunctionOrder whether the parser should check the order of contract invocations and
 *     named types; Unordered or DepthFirst.
 * @param chainComponentFormat indicates whether different types of ABIs are supported or;
 *     ONLY_CONTRACT or CHAIN_COMPONENTS.
 */
public record Configuration(
    ShortnameType shortnameType,
    FunctionFormat fnType,
    Integer shortnameLength,
    NamedTypesFormat namedTypesFormat,
    TypeAndFunctionOrder typeAndFunctionOrder,
    ChainComponentFormat chainComponentFormat) {

  /**
   * Create a configuration from a client version.
   *
   * @param version the version
   * @param shortnameLength the shortname length if applicable
   * @return the configuration
   */
  public static Configuration fromClientVersion(AbiVersion version, Integer shortnameLength) {
    final ConfigOptions options = getOptionsOrThrow(version);
    return new Configuration(
        options.shortnameType(),
        options.fnType(),
        shortnameLength,
        options.namedTypesFormat(),
        options.typeAndFunctionOrder(),
        options.chainComponentFormat());
  }

  /**
   * Enumerates the supported versions and checks if current version is valid.
   *
   * @param clientVersion the version to be checked
   * @return non-null if the version is supported, null otherwise
   */
  public static ConfigOptions getOptionsOrThrow(final AbiVersion clientVersion) {
    var options = SUPPORTED_CLIENT_VERSIONS.get(clientVersion.withZeroPatch());
    if (options == null) {
      throw new UnsupportedVersionException(
          "Unsupported Version %s for Version Client.", clientVersion);
    } else {
      return options;
    }
  }

  /**
   * Checks if a given {@link AbiVersion} is valid in this configuration.
   *
   * @param clientVersion the version to be checked.
   * @return whether the version is supported.
   */
  public static boolean isSupportedVersion(final AbiVersion clientVersion) {
    return SUPPORTED_CLIENT_VERSIONS.containsKey(clientVersion.withZeroPatch());
  }

  /** Map of valid client versions to their configuration options. */
  private static final Map<AbiVersion, ConfigOptions> SUPPORTED_CLIENT_VERSIONS = new HashMap<>();

  static {
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(1, 0, 0),
        new ConfigOptions(
            ShortnameType.HASH,
            FunctionFormat.InitSeparately,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(2, 0, 0),
        new ConfigOptions(
            ShortnameType.HASH,
            FunctionFormat.InitSeparately,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(3, 0, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.InitSeparately,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(3, 1, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.InitSeparately,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(4, 0, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(4, 1, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.OnlyStructs,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 0, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 1, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 2, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 3, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 4, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 5, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 6, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.Unordered,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(5, 7, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.DepthFirst,
            ChainComponentFormat.ONLY_CONTRACT));
    SUPPORTED_CLIENT_VERSIONS.put(
        new AbiVersion(6, 0, 0),
        new ConfigOptions(
            ShortnameType.LEB128,
            FunctionFormat.FnKind,
            NamedTypesFormat.StructsAndEnum,
            TypeAndFunctionOrder.DepthFirst,
            ChainComponentFormat.CHAIN_COMPONENTS));
  }

  /** Stores configuration variants based on version. */
  record ConfigOptions(
      ShortnameType shortnameType,
      FunctionFormat fnType,
      NamedTypesFormat namedTypesFormat,
      TypeAndFunctionOrder typeAndFunctionOrder,
      ChainComponentFormat chainComponentFormat) {}

  /** Describes the format for parsing chain components. */
  public enum ChainComponentFormat {
    /** Only contract abi exists. */
    ONLY_CONTRACT,
    /** A chain component is prefixed by a byte denoting the type chain component to parse. */
    CHAIN_COMPONENTS
  }

  /** Possible types of shortname. */
  public enum ShortnameType {
    /** Using HASH as ShortnameType. */
    HASH,
    /** Using LEB128 as ShortnameType. */
    LEB128
  }

  /** Describes the format for parsing contract invocations. */
  public enum FunctionFormat {
    /**
     * Denotes ABI version 3.1, where Init FnKind were placed before the list of contract
     * invocations.
     *
     * @see <a href="https://partisiablockchain.gitlab.io/documentation/abiv3_1.html">ABI v3.1</a>
     */
    InitSeparately,

    /**
     * Denotes ABI version 4.0+, where Init FnKind placed along with other contract invocations in
     * the hook list.
     *
     * @see <a href="https://partisiablockchain.gitlab.io/documentation/abiv_latest.html">ABI,
     *     latest</a>
     */
    FnKind
  }

  /** Describes the format parsing named types. */
  public enum NamedTypesFormat {
    /**
     * Denotes ABI version {@literal <}=4.1, where only structs were supported and no byte before
     * each named type declaration.
     *
     * @see <a href="https://partisiablockchain.gitlab.io/documentation/abiv_latest.html">ABI
     *     v4.1</a>
     */
    OnlyStructs,
    /**
     * Denotes ABI version 5.0+, where support for different named types was added specifically
     * enums was added.
     *
     * @see <a href="https://partisiablockchain.gitlab.io/documentation/abiv_latest.html">ABI
     *     v5.0</a>
     */
    StructsAndEnum,
  }

  /** Describes how named types and contract invocations must be ordered. */
  public enum TypeAndFunctionOrder {
    /**
     * For ABI version {@literal <}= 5.6.0, named types and contract invocations are not expected to
     * have any specific ordering.
     */
    Unordered,
    /**
     * For ABI version >= 5.7.0 named types are expected to have been ordered depth first when
     * traversing the graph of type definitions. Functions are expected to be sorted by the id of
     * their {@link ContractInvocationKind} first and second by the lexicographical order of the hex
     * representation of their shortname if more than one function of a certain kind exists.
     */
    DepthFirst,
  }
}
