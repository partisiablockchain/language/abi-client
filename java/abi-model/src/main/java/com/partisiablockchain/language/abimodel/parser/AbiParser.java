package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ChainComponent;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.DocumentationNamedType;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.Header;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpecWithGenerics;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.secata.stream.BigEndianByteInput;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Parser for parsing abi files as byte stream to Java model. The parser creates a Java
 * representation of the contract from the abi byte stream by first verifying the headers and
 * thereafter parsing the body of the contract.
 *
 * <p>The abi specification specifies the typing relations of the structs and invocations in the
 * contract by modelling each struct, function, field and argument, specifying their types. As such,
 * the Java model provides a full typing model of the entries in the smart contract. The parser
 * supports several versions of the abi model, for more information on each see the respective
 * reference below: <a href="https://partisiablockchain.gitlab.io/documentation/abiv3_1.html">ABI
 * v3.1</a> <a href="https://partisiablockchain.gitlab.io/documentation/abiv_latest.html">ABI latest
 * </a>
 */
public final class AbiParser {

  /** Indicates how strict the parser is when validating identifiers. */
  public enum Strictness {
    /** In strict mode invalid identifiers will cause an error. This is the default mode. */
    STRICT,
    /** In lenient mode invalid identifiers are allowed. */
    LENIENT,
    /** In sorting mode the named types of a contract abi is sorted. */
    SORTING,
  }

  private final BigEndianByteInput stream;
  private final Scopes scopes;
  private final Strictness strictness;

  /**
   * Initializes the Abi Parser with a byte stream.
   *
   * @param stream stream of abi bytes
   * @param strictness how strict to be when validating identifiers
   */
  public AbiParser(BigEndianByteInput stream, Strictness strictness) {
    this.stream = Objects.requireNonNull(stream);
    this.strictness = Objects.requireNonNull(strictness);
    this.scopes = new Scopes();
  }

  /**
   * Creates a parser object from a byte array.
   *
   * @param bytes the bytes to be parsed
   * @param strictness how strict to be when validating identifiers
   */
  public AbiParser(byte[] bytes, Strictness strictness) {
    this(new BigEndianByteInput(new DataInputStream(new ByteArrayInputStream(bytes))), strictness);
  }

  /**
   * Creates a parser object from a byte stream with strict parsing of identifiers.
   *
   * @param stream stream of abi bytes
   */
  public AbiParser(BigEndianByteInput stream) {
    this(stream, Strictness.STRICT);
  }

  /**
   * Creates a parser object from a byte array with strict parsing of identifiers.
   *
   * @param bytes the bytes to be parsed
   */
  public AbiParser(byte[] bytes) {
    this(bytes, Strictness.STRICT);
  }

  /**
   * Parse only the headers of the abi file and returns a Header.
   *
   * @return the Header of the parser
   */
  public Header parseHeader() {
    String header = new String(stream.readBytes(6), StandardCharsets.UTF_8);
    byte[] versionBinder = stream.readBytes(3);
    byte[] versionClient = stream.readBytes(3);

    return new Header(
        header,
        new AbiVersion(versionBinder[0], versionBinder[1], versionBinder[2]),
        new AbiVersion(versionClient[0], versionClient[1], versionClient[2]));
  }

  /**
   * Parses the body of the contract given a parsing configuration.
   *
   * @param header the configuration of the parser obtained from parsing the headers
   * @return model of the contract as specified by the abi spec
   */
  public FileAbi parseChainComponent(final Header header) {
    final Integer shortnameLength = readShortnameLength(header);
    final Configuration configuration =
        Configuration.fromClientVersion(header.versionClient(), shortnameLength);
    final ChainComponent chainComponent;
    if (configuration.chainComponentFormat()
        == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      byte componentIdentifier = stream.readU8();
      switch (componentIdentifier) {
        case 0x10 -> chainComponent = parseContractAbiInternal(configuration);
        case 0x20 -> chainComponent = parseBinderAbiInternal(configuration);
        default ->
            throw new AbiParseException(
                "Unknown chain component identifier: 0x%2x", componentIdentifier);
      }
    } else {
      chainComponent = parseContractAbiInternal(configuration);
    }
    return new FileAbi(
        header.header(),
        header.versionBinder(),
        header.versionClient(),
        shortnameLength,
        chainComponent);
  }

  private BinderAbi parseBinderAbiInternal(Configuration configuration) {
    final BinderType binderType = BinderType.fromId(stream.readU8());
    final List<ContractInvocationKind> contractCallableKinds = parseFunctionKinds();
    final List<NamedTypeSpec> namedTypes = parseNamedTypes(configuration);

    final List<BinderInvocation> invocations = parseBinderInvocations();
    validateInvocations(invocations);

    TypeSpec stateType = parseTypeSpec(stream);

    // Validate contractInvocations are ordered by KindId and then shortname
    validateBinderInvocationsSorted(invocations);
    // Validate the types are ordered depth-first when traversing the type tree from each root
    // (state type and function argument types).
    List<TypeSpec> typeRoots = findTypeRoots(stateType, invocations, List.of());
    SortedTypes sortedTypes = getSortedTypes(namedTypes, typeRoots);
    validateTypesSortedDepthFirst(namedTypes, sortedTypes.sortedTypes);
    return new BinderAbi(binderType, contractCallableKinds, namedTypes, invocations, stateType);
  }

  private void validateInvocations(List<BinderInvocation> invocations) {
    Map<TransactionKind, List<BinderInvocation>> grouped =
        invocations.stream().collect(Collectors.groupingBy(BinderInvocation::kind));
    for (Map.Entry<TransactionKind, List<BinderInvocation>> entry : grouped.entrySet()) {
      if (entry.getValue().stream().anyMatch(invocation -> invocation.shortname() == null)
          && entry.getValue().size() != 1) {
        throw new AbiParseException(
            "Binder cannot contain more than one invocation of kind '%s', when one exists without a"
                + " shortname",
            entry.getKey());
      }
    }
  }

  private List<BinderInvocation> parseBinderInvocations() {
    final List<BinderInvocation> invocations = new ArrayList<>();
    int invocationsSize = readInt();

    for (int i = 0; i < invocationsSize; i++) {
      BinderInvocation binderInvocation = parseBinderInvocation();
      invocations.add(binderInvocation);
    }
    return invocations;
  }

  private BinderInvocation parseBinderInvocation() {
    byte kindId = stream.readU8();
    TransactionKind kind = TransactionKind.fromKindId(kindId);
    if (kind == null) {
      throw new AbiParseException("Unsupported TransactionKind type 0x%02x specified", kindId);
    }
    String name = readIdentifier();

    Byte shortname = null;
    if (stream.readBoolean()) {
      shortname = stream.readU8();
    }

    // Parse arguments
    final List<ArgumentAbi> arguments = parseArguments();

    // Parse contract call id
    Integer contractCallKindId = null;
    if (stream.readBoolean()) {
      contractCallKindId = (int) stream.readU8();
    }

    return new BinderInvocation(kind, name, shortname, arguments, contractCallKindId);
  }

  private List<ContractInvocationKind> parseFunctionKinds() {
    List<ContractInvocationKind> callableKinds = new ArrayList<>();
    int size = readInt();
    for (int i = 0; i < size; i++) {
      int id = stream.readU8();
      String name = stream.readString();
      boolean hasShortname = stream.readBoolean();
      callableKinds.add(new ContractInvocationKind(id, name, hasShortname));
    }
    return callableKinds;
  }

  /**
   * Parses the headers of the abi file, generates a configuration and uses this to parse the body.
   *
   * @return Java model representing the abi file
   */
  public FileAbi parseAbi() {
    Header header = parseHeader();
    FileAbi fileAbi = parseChainComponent(header);

    // Check that entire stream have been read
    int remainingLength = stream.readRemaining().length;
    if (remainingLength != 0) {
      throw new AbiParseException(
          "Expected EOF after parsed ABI, but stream had %s bytes remaining", remainingLength);
    }

    return fileAbi;
  }

  /**
   * Parses the headers of the contract abi file, generates a configuration and uses this to parse
   * the body. Throws if abi is not a contract abi.
   *
   * @return Java model representing the contract abi file
   */
  public ContractAbi parseContractAbi() {
    FileAbi fileAbi = parseAbi();
    if (fileAbi.chainComponent() instanceof ContractAbi contractAbi) {
      return contractAbi;
    } else {
      throw new AbiParseException("Failed to parse abi as a contract abi");
    }
  }

  private ContractAbi parseContractAbiInternal(Configuration config) {
    List<ContractInvocationKind> contractInvocationKinds = List.of();
    BinderType binderType = null;
    if (config.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      binderType = BinderType.fromId(stream.readU8());
      contractInvocationKinds = parseFunctionKinds();
    }
    List<NamedTypeSpec> namedTypes = parseNamedTypes(config);

    List<BinderInvocation> binderInvocations;
    List<ContractInvocation> contractCalls;
    if (config.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      binderInvocations = parseBinderInvocations();
      contractCalls = parseFunctions(config, contractInvocationKinds);
    } else {
      contractCalls = parseFunctions(config, ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS);
      boolean isZk =
          contractCalls.stream().anyMatch(p -> ImplicitBinderAbi.isZkKind(p.kind().kindId()));
      if (isZk) {
        binderType = BinderType.Zk;
        contractInvocationKinds = ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS;
        binderInvocations = ImplicitBinderAbi.defaultZkInvocations();
      } else {
        binderType = BinderType.Public;
        contractInvocationKinds = ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS;
        binderInvocations = ImplicitBinderAbi.defaultPubInvocations();
      }
    }

    // Parse state type
    TypeSpec stateType = parseTypeSpec();

    if (config.typeAndFunctionOrder() == Configuration.TypeAndFunctionOrder.DepthFirst) {
      // Validate contract invocations are ordered by KindId and then shortname
      validateBinderInvocationsSorted(binderInvocations);
      validateFunctionsSorted(contractCalls);
      // Validate the types are ordered depth-first when traversing the type tree from each root
      // (state type and function argument types).
      List<TypeSpec> typeRoots = findTypeRoots(stateType, binderInvocations, contractCalls);
      SortedTypes sortedTypes = getSortedTypes(namedTypes, typeRoots);
      if (strictness == Strictness.SORTING) {
        namedTypes =
            sortedTypes.sortedTypes.stream()
                .map(named -> replaceNamedRefTypes(sortedTypes.indexMapping, named))
                .toList();
        binderInvocations =
            binderInvocations.stream()
                .map(invocation -> replaceNamedRefTypes(sortedTypes.indexMapping, invocation))
                .toList();
        contractCalls =
            contractCalls.stream()
                .map(fn -> replaceNamedRefTypes(sortedTypes.indexMapping, fn))
                .toList();
        stateType = replaceNamedRefTypes(sortedTypes.indexMapping, stateType);
      } else {
        validateTypesSortedDepthFirst(namedTypes, sortedTypes.sortedTypes);
      }
    } else {
      // Sort actions
      contractCalls.sort(FN_ABI_OLD_SORT_ODER);
    }

    return new ContractAbi(
        binderType,
        contractInvocationKinds,
        namedTypes,
        binderInvocations,
        contractCalls,
        stateType);
  }

  private void validateBinderInvocationsSorted(List<BinderInvocation> binderInvocations) {
    Iterator<BinderInvocation> functionIter = binderInvocations.iterator();
    BinderInvocation previous = functionIter.next();
    while (functionIter.hasNext()) {
      BinderInvocation next = functionIter.next();
      if (!binderInvocationOrderStrictlyAscending(previous, next)) {
        String errorMessage =
            "BinderInvocations are unordered: Must be ordered by invocation kind and by shortname"
                + " in ascending order but %s appears before %s";
        throw new AbiParseException(
            errorMessage, formatForErrorMessage(previous), formatForErrorMessage(next));
      }
      previous = next;
    }
  }

  /** Sort FnABI for client version 5.6.0 and older. */
  private static final Comparator<ContractInvocation> FN_ABI_OLD_SORT_ODER =
      Comparator.comparingInt(fn -> fn.kind().kindId());

  private void validateFunctionsSorted(List<ContractInvocation> functions) {
    Iterator<ContractInvocation> functionIter = functions.iterator();
    ContractInvocation previous = functionIter.next();
    while (functionIter.hasNext()) {
      ContractInvocation next = functionIter.next();
      if (!functionOrderStrictlyAscending(previous, next)) {
        String errorMessage =
            "Functions are unordered: Must be ordered by function kind and by shortname in"
                + " ascending order but %s appears before %s";
        throw new AbiParseException(
            errorMessage, formatForErrorMessage(previous), formatForErrorMessage(next));
      }
      previous = next;
    }
  }

  private String formatForErrorMessage(ContractInvocation contractInvocation) {
    return String.format(
        "%s(%d) with shortname %s",
        contractInvocation.kind().name(),
        contractInvocation.kindIdentifier(),
        contractInvocation.shortnameAsString());
  }

  private String formatForErrorMessage(BinderInvocation binderInvocation) {
    return String.format(
        "%s(%d) with shortname %s",
        binderInvocation.kind(), binderInvocation.kind().getKindId(), binderInvocation.shortname());
  }

  private boolean functionOrderStrictlyAscending(
      ContractInvocation previous, ContractInvocation next) {
    return FN_ABI_SORT_ORDER_KIND_ID_AND_SHORTNAME.compare(previous, next) < 0;
  }

  private boolean binderInvocationOrderStrictlyAscending(
      BinderInvocation previous, BinderInvocation next) {
    return BINDER_INVOCATION_SORT_ORDER_KIND_ID_AND_SHORTNAME.compare(previous, next) < 0;
  }

  /**
   * Compare {@link ContractInvocation}s, first by the identifier of their kind, next by the hex
   * string representation of their shortname.
   */
  public static final Comparator<ContractInvocation> FN_ABI_SORT_ORDER_KIND_ID_AND_SHORTNAME =
      Comparator.comparingInt(ContractInvocation::kindIdentifier)
          .thenComparing(ContractInvocation::shortnameAsString);

  /**
   * Compare {@link ContractInvocation}s, first by the identifier of their kind, next by the hex
   * string representation of their shortname.
   */
  public static final Comparator<BinderInvocation>
      BINDER_INVOCATION_SORT_ORDER_KIND_ID_AND_SHORTNAME =
          Comparator.comparingInt((BinderInvocation i) -> i.kind().getKindId())
              .thenComparing(BinderInvocation::shortname);

  private static List<TypeSpec> findTypeRoots(
      TypeSpec stateType,
      List<BinderInvocation> binderInvocations,
      List<ContractInvocation> functions) {
    List<TypeSpec> typeRoots = new ArrayList<>();
    typeRoots.add(stateType);
    for (BinderInvocation binderInvocation : binderInvocations) {
      for (ArgumentAbi argument : binderInvocation.arguments()) {
        typeRoots.add(argument.type());
      }
    }
    for (ContractInvocation function : functions) {
      for (ArgumentAbi argument : function.arguments()) {
        typeRoots.add(argument.type());
      }
      if (function.secretArgument() != null) {
        typeRoots.add(function.secretArgument().type());
      }
    }
    return typeRoots;
  }

  private SortedTypes getSortedTypes(List<NamedTypeSpec> namedTypes, List<TypeSpec> roots) {
    List<NamedTypeSpec> visitedTypes = new ArrayList<>();
    Map<Integer, Integer> indexMapping = new HashMap<>();
    for (TypeSpec rootType : roots) {
      visitTypeSpec(namedTypes, visitedTypes, indexMapping, rootType);
    }
    return new SortedTypes(visitedTypes, indexMapping);
  }

  private record SortedTypes(List<NamedTypeSpec> sortedTypes, Map<Integer, Integer> indexMapping) {}

  private void validateTypesSortedDepthFirst(
      List<NamedTypeSpec> namedTypes, List<NamedTypeSpec> sortedTypes) {
    for (int i = 0; i < sortedTypes.size(); i++) {
      NamedTypeSpec sortedNamedType = sortedTypes.get(i);
      if (!sortedNamedType.equals(namedTypes.get(i))) {
        throw new AbiParseException(
            "Expected named type '%s' to have index %d but was %d",
            sortedNamedType.name(), i, namedTypes.indexOf(sortedNamedType));
      }
    }
    if (namedTypes.size() != sortedTypes.size()) {
      namedTypes.removeAll(sortedTypes);
      String additionalTypes =
          namedTypes.stream().map(NamedTypeSpec::name).collect(Collectors.joining(", ", "[", "]"));
      throw new AbiParseException(
          "Named Types list contained unreachable type(s) %s", additionalTypes);
    }
  }

  private void visitTypeSpec(
      List<NamedTypeSpec> allTypes,
      List<NamedTypeSpec> visitedTypes,
      Map<Integer, Integer> indexMapping,
      TypeSpec currentSpec) {
    if (currentSpec instanceof NamedTypeRef namedTypeRef) {
      int index = namedTypeRef.index();
      visitIndex(allTypes, visitedTypes, indexMapping, index);
    } else if (currentSpec instanceof TypeSpecWithGenerics typeSpec) {
      for (TypeSpec generic : typeSpec.getGenerics()) {
        visitTypeSpec(allTypes, visitedTypes, indexMapping, generic);
      }
    } else if (currentSpec instanceof SizedArrayTypeSpec arraySpec) {
      visitTypeSpec(allTypes, visitedTypes, indexMapping, arraySpec.valueType());
    }
  }

  private void visitIndex(
      List<NamedTypeSpec> allTypes,
      List<NamedTypeSpec> visitedTypes,
      Map<Integer, Integer> indexMapping,
      int index) {
    if (allTypes.size() <= index) {
      throw new AbiParseException("No known types at index " + index);
    }
    NamedTypeSpec namedType = allTypes.get(index);
    if (!visitedTypes.contains(namedType)) {
      int expectedIndex = visitedTypes.size();
      indexMapping.put(index, expectedIndex);
      visitedTypes.add(namedType);
      visitNamedType(allTypes, visitedTypes, indexMapping, namedType);
    }
  }

  private void visitNamedType(
      List<NamedTypeSpec> allTypes,
      List<NamedTypeSpec> visitedTypes,
      Map<Integer, Integer> indexMapping,
      NamedTypeSpec currentNamedType) {
    if (currentNamedType instanceof StructTypeSpec structSpec) {
      for (FieldAbi field : structSpec.fields()) {
        TypeSpec type = field.type();
        visitTypeSpec(allTypes, visitedTypes, indexMapping, type);
      }
    } else { // currentNamedType instanceof EnumTypeSpec
      EnumTypeSpec enumSpec = (EnumTypeSpec) currentNamedType;
      for (EnumVariant variant : enumSpec.variants()) {
        NamedTypeRef type = variant.def();
        visitTypeSpec(allTypes, visitedTypes, indexMapping, type);
      }
    }
  }

  private List<NamedTypeSpec> parseNamedTypes(Configuration config) {
    final List<NamedTypeSpec> namedTypes = new ArrayList<>();
    int namedTypesSize = readInt();

    for (int i = 0; i < namedTypesSize; i++) {
      if (config.namedTypesFormat() == Configuration.NamedTypesFormat.StructsAndEnum) {
        byte namedType = stream.readU8();
        if (namedType == NamedTypeSpec.NamedTypeIndex.Struct.getValue()) {
          namedTypes.add(parseStructTypeSpec(i));
        } else if (namedType == NamedTypeSpec.NamedTypeIndex.Enum.getValue()) {
          namedTypes.add(parseEnumTypeSpec());
        } else {
          throw new AbiParseException(
              "Bad byte 0x%02x used for namedTypeSpec index should be either 0x%02x for a struct or"
                  + " 0x%02x for an enum",
              namedType,
              NamedTypeSpec.NamedTypeIndex.Struct.getValue(),
              NamedTypeSpec.NamedTypeIndex.Enum.getValue());
        }
      } else {
        namedTypes.add(parseStructTypeSpec(i));
      }
    }
    return namedTypes;
  }

  private StructTypeSpec parseStructTypeSpec(int currentIndex) {
    String name = readIdentifier();
    Scope currentScope =
        scopes.scopeFor(currentIndex); // Struct may be contained inside an enum variant
    if (strictness != Strictness.LENIENT && isDuplicateIdentifier(currentScope, name)) {
      throw new AbiParseException("Duplicate named type encountered: %s", name);
    }
    int fieldsSize = readInt();
    List<FieldAbi> fields = new ArrayList<>();

    Scope structScope = Scope.withoutTypeIndices();
    for (int j = 0; j < fieldsSize; j++) {
      String fieldName = readIdentifier();
      if (strictness != Strictness.LENIENT && isDuplicateIdentifier(structScope, fieldName)) {
        throw new AbiParseException("Duplicate field name encountered: %s", fieldName);
      }
      TypeSpec type = parseTypeSpec();
      fields.add(new FieldAbi(fieldName, type));
    }

    return new StructTypeSpec(name, fields, DocumentationNamedType.empty());
  }

  private EnumTypeSpec parseEnumTypeSpec() {
    String name = readIdentifier();
    Scope baseScope = scopes.baseTypes(); // All enums are contained in the base scope.
    if (strictness != Strictness.LENIENT && isDuplicateIdentifier(baseScope, name)) {
      throw new AbiParseException("Duplicate named type encountered: %s", name);
    }
    int numVariants = readInt();
    List<Integer> typeIndices = new ArrayList<>();
    List<EnumVariant> variants = new ArrayList<>();
    for (int i = 0; i < numVariants; i++) {
      int discriminant = stream.readU8();
      TypeSpec.TypeIndex typeIndex = TypeSpec.TypeIndex.fromValue(stream.readU8());
      if (typeIndex != TypeSpec.TypeIndex.Named) {
        throw new AbiParseException(
            "Non named type %s used as an enum variant, each variant should be a reference to a"
                + " struct",
            typeIndex.name());
      }
      int index = stream.readU8();
      variants.add(new EnumVariant(discriminant, new NamedTypeRef(index)));
      typeIndices.add(index);
    }
    scopes.addVariantScopeWith(typeIndices);
    return new EnumTypeSpec(name, variants);
  }

  private ContractInvocation parseContractCall(
      final Configuration config,
      final ContractInvocationKind defaultKind,
      List<ContractInvocationKind> contractKinds) {

    // Parse kind
    // The old InitSeparately hook format did not possess a FnKind field; all
    // contract invocations in the list were of kind Action.
    ContractInvocationKind kind;
    if (config.fnType() == Configuration.FunctionFormat.FnKind) {
      int kindId = stream.readU8();
      kind = ContractInvocationKind.getKind(contractKinds, kindId);
      if (kind == null) {
        throw new AbiParseException("Unsupported FnKind type 0x%2x specified", (byte) kindId);
      }

    } else {
      kind = defaultKind;
    }

    // Parse name
    final String name = readIdentifier();
    Scope functionScope = scopes.functions();
    if (strictness != Strictness.LENIENT && isDuplicateIdentifier(functionScope, name)) {
      throw new AbiParseException("Duplicate function name encountered: %s", name);
    }

    // Parse shortname
    byte[] shortname;
    if (config.shortnameType() == Configuration.ShortnameType.LEB128) {
      shortname = parseLeb128(stream);
    } else {
      if (kind.kindId() == ImplicitBinderAbi.DefaultKinds.INIT.kindId()) {
        shortname = new byte[] {0, 0, 0, 0};
      } else {
        shortname = parseHashShortname(config.shortnameLength(), name);
      }
    }

    // Parse arguments
    final List<ArgumentAbi> arguments = parseArguments();

    ArgumentAbi secretArgument = null;
    if (config.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      if (stream.readBoolean()) {
        secretArgument = readArgumentAbi(Scope.withoutTypeIndices());
      }
    } else {
      if (kind.kindId()
          == ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId()) {
        secretArgument = readArgumentAbi(Scope.withoutTypeIndices());
      } else if (kind.kindId() == ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT.kindId()) {
        // Use default secret argument type
        kind = ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE;
        secretArgument =
            new ArgumentAbi("secret_input", new SimpleTypeSpec(TypeSpec.TypeIndex.i32));
      }
    }
    return new ContractInvocation(kind, name, shortname, List.copyOf(arguments), secretArgument);
  }

  private List<ArgumentAbi> parseArguments() {
    final List<ArgumentAbi> arguments = new ArrayList<>();
    final int argumentsSize = readInt();
    Scope functionArgumentScope = Scope.withoutTypeIndices();
    for (int i = 0; i < argumentsSize; i++) {
      arguments.add(readArgumentAbi(functionArgumentScope));
    }
    return arguments;
  }

  private ArgumentAbi readArgumentAbi(Scope argumentScope) {
    final String argName = readIdentifier();
    if (strictness != Strictness.LENIENT && isDuplicateIdentifier(argumentScope, argName)) {
      throw new AbiParseException("Duplicate argument name encountered: %s", argName);
    }
    final TypeSpec argType = parseTypeSpec(stream);
    return new ArgumentAbi(argName, argType);
  }

  /**
   * Generates the shortname of an action given its name, by computing the SHA256 hash and using the
   * appropriate amount of bytes as specified by the shortname length.
   *
   * @param shortnameLength the amount of bytes used for the shortname
   * @param name the name of the action as a String
   * @return the generated shortname
   */
  private byte[] parseHashShortname(int shortnameLength, String name) {
    MessageDigest digest =
        ExceptionConverter.call(
            () -> MessageDigest.getInstance("SHA-256"),
            "An error occurred while getting the SHA-256 Algorithm");
    byte[] hash = digest.digest(name.getBytes(StandardCharsets.UTF_8));
    byte[] shortname = new byte[shortnameLength];
    System.arraycopy(hash, 0, shortname, 0, shortnameLength);
    return shortname;
  }

  /**
   * Parses a LEB128 unsigned integer from the stream.
   *
   * @return the byte array representation of the parsed LEB128 unsigned integer
   */
  static byte[] parseLeb128(BigEndianByteInput stream) {
    byte[] buffer = new byte[5];
    for (int i = 0; i < buffer.length; i++) {
      var currentByte = stream.readU8();
      buffer[i] = currentByte;

      if ((currentByte & 0x80) == 0) {
        return Arrays.copyOf(buffer, i + 1);
      }
    }

    throw new AbiParseException(
        "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5"
            + " bytes)");
  }

  private List<ContractInvocation> parseFunctions(
      Configuration config, List<ContractInvocationKind> contractInvocationKinds) {
    final ArrayList<ContractInvocation> functions = new ArrayList<>();
    if (config.fnType() == Configuration.FunctionFormat.InitSeparately) {
      // First hook is init in InitSeparately
      final ContractInvocation init =
          parseContractCall(config, ImplicitBinderAbi.DefaultKinds.INIT, contractInvocationKinds);
      functions.add(init);
    }

    int actionsSize = readInt();
    for (int i = 0; i < actionsSize; i++) {
      final ContractInvocation action =
          parseContractCall(config, ImplicitBinderAbi.DefaultKinds.ACTION, contractInvocationKinds);
      functions.add(action);
    }
    return functions;
  }

  /**
   * Parse a single {@link TypeSpec} from the internal stream.
   *
   * @return the resulting {@link TypeSpec}
   */
  public TypeSpec parseTypeSpec() {
    return parseTypeSpec(stream);
  }

  /**
   * Parse a single {@link TypeSpec} from the given stream.
   *
   * @param stream the input stream
   * @return the resulting {@link TypeSpec}
   */
  @SuppressWarnings("LeftCurlyNl")
  static TypeSpec parseTypeSpec(final BigEndianByteInput stream) {
    final int identifierByte = stream.readU8();
    final var typeIndex = TypeSpec.TypeIndex.fromValue(identifierByte);
    switch (typeIndex) {
      case u8,
          u16,
          u32,
          u64,
          u128,
          i8,
          i16,
          i32,
          i64,
          i128,
          u256,
          String,
          bool,
          Address,
          Hash,
          PublicKey,
          Signature,
          BlsPublicKey,
          BlsSignature -> {
        return new SimpleTypeSpec(typeIndex);
      }
      case Named -> {
        int index = stream.readU8();
        return new NamedTypeRef(index);
      }
      case Vec -> {
        TypeSpec vecType = parseTypeSpec(stream);
        return new VecTypeSpec(vecType);
      }
      case Map -> {
        TypeSpec key = parseTypeSpec(stream);
        TypeSpec value = parseTypeSpec(stream);
        return new MapTypeSpec(key, value);
      }
      case Set -> {
        TypeSpec setType = parseTypeSpec(stream);
        return new SetTypeSpec(setType);
      }
      case SizedByteArray -> {
        int length = stream.readU8();
        return SizedArrayTypeSpec.byteArray(length);
      }
      case SizedArray -> {
        TypeSpec value = parseTypeSpec(stream);
        int length = stream.readU8();
        return new SizedArrayTypeSpec(value, length);
      }
      case Option -> {
        TypeSpec optionType = parseTypeSpec(stream);
        return new OptionTypeSpec(optionType);
      }
      case AvlTreeMap -> {
        TypeSpec key = parseTypeSpec(stream);
        TypeSpec value = parseTypeSpec(stream);
        return new AvlTreeMapTypeSpec(key, value);
      }
      default -> throw new AbiParseException("Unknown type index: 0x%02X", identifierByte);
    }
  }

  /**
   * Checks whether the given scope already contains the given identifier and adds the identifier if
   * not.
   *
   * @param scope the scope to for duplicates in
   * @param identifier the identifier to be checked
   * @return whether the identifier is duplicated
   */
  private boolean isDuplicateIdentifier(Scope scope, String identifier) {
    boolean exists = scope.containsIdentifier(identifier);
    if (!exists) {
      scope.addIdentifier(identifier);
    }
    return exists;
  }

  /**
   * Checks whether the given identifier is a valid Java identifier.
   *
   * @param identifier the identifier to be checked
   * @return whether the identifier is valid
   */
  public static boolean isIdentifierValid(String identifier) {
    if (identifier.isEmpty()) {
      return false;
    }
    if (!Character.isJavaIdentifierStart(identifier.charAt(0))) {
      return false;
    }
    for (int i = 1; i < identifier.length(); i++) {
      if (!Character.isJavaIdentifierPart(identifier.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  /**
   * Verifies the configuration, generated from parsing the headers. Throws a ParsingException if
   * the header is not "PBCABI. Also verifies the versions and ensures they are supported. Throws
   * UnsupportedVersionException if this is not the case."
   */
  private Integer readShortnameLength(Header header) {

    if (!header.header().equals("PBCABI")) {
      throw new AbiParseException(
          "Malformed header bytes, expecting PBCABI but was %s", header.header());
    }

    // Verify that current version is supported
    final Configuration.ConfigOptions options =
        Configuration.getOptionsOrThrow(header.versionClient());

    final Integer shortnameLength;
    if (options.shortnameType() == Configuration.ShortnameType.HASH) {
      shortnameLength = (int) stream.readU8();
    } else {
      shortnameLength = null;
    }
    return shortnameLength;
  }

  private int readInt() {
    return stream.readI32();
  }

  private String readIdentifier() {
    var str = stream.readString();
    if (strictness != Strictness.LENIENT && !isIdentifierValid(str)) {
      throw new AbiParseException("Invalid identifier: %s is not a valid Java identifier", str);
    }
    return str;
  }

  /**
   * Generates a string representation of the Abi model using {@link RustSyntaxPrettyPrinter}.
   *
   * @param model the model to pretty print
   * @return string containing the pretty printed model
   */
  public static String printModel(FileAbi model) {
    if (model.chainComponent() instanceof BinderAbi) {
      return BinderRustSyntaxPrettyPrinter.create(model).printModel();
    } else {
      return new RustSyntaxPrettyPrinter(model).printModel();
    }
  }

  /**
   * Generates a string representation of a given contract action in the abi model using {@link
   * RustSyntaxPrettyPrinter}.
   *
   * @param model the model of the contract
   * @param action the action to pretty print
   * @return string containing the pretty printed action
   */
  public static String printFunction(FileAbi model, ContractInvocation action) {
    return new RustSyntaxPrettyPrinter(model).printFunction(action);
  }

  /**
   * Generates a string representation of a given binder invocation in the abi model using {@link
   * RustSyntaxPrettyPrinter}.
   *
   * @param model the model of the contract
   * @param binderInvocation the binder invocation to pretty print
   * @return string containing the pretty printed action
   */
  public static String printBinderInvocation(FileAbi model, BinderInvocation binderInvocation) {
    return new RustSyntaxPrettyPrinter(model).printBinderInvocation(binderInvocation);
  }

  /**
   * Generates a string representation of a given struct in the abi model using {@link
   * RustSyntaxPrettyPrinter}.
   *
   * @param model the model of the contract
   * @param struct the struct to pretty print
   * @return string containing the pretty printed struct
   */
  public static String printStruct(FileAbi model, StructTypeSpec struct) {
    return new RustSyntaxPrettyPrinter(model).printStruct(struct);
  }

  /**
   * Collection of type scopes for the ABI.
   *
   * <p>The collection consists of a base scopes for types and invocations and a scope for each enum
   * to contain its variants.
   */
  private static final class Scopes {
    private final Scope baseTypes;
    private final Scope functions;
    private final Collection<Scope> enumVariants;

    private Scopes() {
      baseTypes = Scope.withoutTypeIndices();
      functions = Scope.withoutTypeIndices();
      enumVariants = new ArrayList<>();
    }

    Scope baseTypes() {
      return baseTypes;
    }

    Scope functions() {
      return functions;
    }

    Scope scopeFor(int currentIndex) {
      return enumVariants.stream()
          .filter(s -> s.containsIndex(currentIndex))
          .findFirst()
          .orElse(baseTypes);
    }

    void addVariantScopeWith(List<Integer> indices) {
      Scope variantScope = Scope.withTypeIndices(indices);
      enumVariants.add(variantScope);
    }
  }

  /**
   * Information for a single scope.
   *
   * <p>A scope contains the identifiers of the items contained in the scope. It may optionally
   * contain the type indexes of the items contained in the scope.
   */
  private static final class Scope {
    private final Collection<String> names;
    private final Collection<Integer> indices;

    private Scope(Collection<Integer> indices) {
      this.names = new HashSet<>();
      this.indices = indices;
    }

    private static Scope withTypeIndices(Collection<Integer> indices) {
      return new Scope(indices);
    }

    private static Scope withoutTypeIndices() {
      return new Scope(Collections.emptyList());
    }

    void addIdentifier(String name) {
      names.add(name);
    }

    boolean containsIdentifier(String name) {
      return names.contains(name);
    }

    boolean containsIndex(int currentIndex) {
      return indices.contains(currentIndex);
    }
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private TypeSpec replaceNamedRefTypes(Map<Integer, Integer> indexMapping, TypeSpec type) {
    if (type instanceof NamedTypeRef namedTypeRef) {
      return new NamedTypeRef(indexMapping.get(namedTypeRef.index()));
    } else if (type instanceof MapTypeSpec mapTypeSpec) {
      return new MapTypeSpec(
          replaceNamedRefTypes(indexMapping, mapTypeSpec.keyType()),
          replaceNamedRefTypes(indexMapping, mapTypeSpec.valueType()));
    } else if (type instanceof AvlTreeMapTypeSpec avlTypeSpec) {
      return new AvlTreeMapTypeSpec(
          replaceNamedRefTypes(indexMapping, avlTypeSpec.keyType()),
          replaceNamedRefTypes(indexMapping, avlTypeSpec.valueType()));
    } else if (type instanceof VecTypeSpec vecTypeSpec) {
      return new VecTypeSpec(replaceNamedRefTypes(indexMapping, vecTypeSpec.valueType()));
    } else if (type instanceof SetTypeSpec setTypeSpec) {
      return new SetTypeSpec(replaceNamedRefTypes(indexMapping, setTypeSpec.valueType()));
    } else if (type instanceof OptionTypeSpec optionTypeSpec) {
      return new OptionTypeSpec(replaceNamedRefTypes(indexMapping, optionTypeSpec.valueType()));
    } else if (type instanceof SizedArrayTypeSpec sizedTypeSpec) {
      return new SizedArrayTypeSpec(
          replaceNamedRefTypes(indexMapping, sizedTypeSpec.valueType()), sizedTypeSpec.length());
    } else {
      return type;
    }
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private NamedTypeSpec replaceNamedRefTypes(
      Map<Integer, Integer> indexMapping, NamedTypeSpec namedType) {
    if (namedType instanceof StructTypeSpec structType) {
      List<FieldAbi> fields =
          structType.fields().stream().map(f -> replaceNamedRefTypes(indexMapping, f)).toList();
      return new StructTypeSpec(structType.name(), fields, structType.doc());
    } else {
      EnumTypeSpec enumType = (EnumTypeSpec) namedType;
      List<EnumVariant> variants =
          enumType.variants().stream()
              .map(
                  variant ->
                      new EnumVariant(
                          variant.discriminant(),
                          new NamedTypeRef(indexMapping.get(variant.def().index()))))
              .toList();
      return new EnumTypeSpec(enumType.name(), variants);
    }
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private FieldAbi replaceNamedRefTypes(Map<Integer, Integer> indexMapping, FieldAbi fieldType) {
    return new FieldAbi(fieldType.name(), replaceNamedRefTypes(indexMapping, fieldType.type()));
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private BinderInvocation replaceNamedRefTypes(
      Map<Integer, Integer> indexMapping, BinderInvocation binderInvocation) {
    List<ArgumentAbi> arguments =
        binderInvocation.arguments().stream()
            .map(arg -> replaceNamedRefTypes(indexMapping, arg))
            .toList();
    return new BinderInvocation(
        binderInvocation.kind(),
        binderInvocation.name(),
        binderInvocation.shortname(),
        arguments,
        binderInvocation.contractCallKindId());
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private ContractInvocation replaceNamedRefTypes(
      Map<Integer, Integer> indexMapping, ContractInvocation fn) {
    List<ArgumentAbi> arguments =
        fn.arguments().stream().map(arg -> replaceNamedRefTypes(indexMapping, arg)).toList();
    ArgumentAbi secretArg =
        fn.secretArgument() != null
            ? replaceNamedRefTypes(indexMapping, fn.secretArgument())
            : null;
    return new ContractInvocation(
        fn.kind(), fn.name(), fn.shortname(), arguments, secretArg, fn.doc());
  }

  /**
   * Recursive replace named type references with new index value according to the given mapping.
   */
  private ArgumentAbi replaceNamedRefTypes(
      Map<Integer, Integer> indexMapping, ArgumentAbi argumentType) {
    return new ArgumentAbi(
        argumentType.name(), replaceNamedRefTypes(indexMapping, argumentType.type()));
  }
}
