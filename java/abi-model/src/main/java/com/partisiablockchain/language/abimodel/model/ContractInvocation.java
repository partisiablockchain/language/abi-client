package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.HexFormat;
import java.util.List;

/**
 * Representation of a contract invocation. The contract part of an invocation to the blockchain.
 *
 * <p>Defines how to build/read rpc for a contract invocation on the blockchain. A contract
 * invocation is always called by a {@link BinderInvocation}.
 *
 * @param kind the kind of invocation.
 * @param name the name of the invocation.
 * @param shortname shortname of the invocation.
 * @param arguments argument definitions of the invocation.
 * @param secretArgument secret argument of the invocation. Null unless the contract invocation has
 *     a secret argument.
 * @param doc the documentation for the invocation.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ContractInvocation(
    ContractInvocationKind kind,
    String name,
    byte[] shortname,
    List<ArgumentAbi> arguments,
    ArgumentAbi secretArgument,
    DocumentationFunction doc) {

  /**
   * Creates {@link ContractInvocation}.
   *
   * @param kind the kind of invocation. Never null.
   * @param name the name of the invocation. Never null.
   * @param shortname shortname of the invocation. Never null.
   * @param arguments argument definitions of the invocation. Never null.
   * @param secretArgument secret argument of the invocation. Null unless kind is ZkSecretInputNew.
   * @param doc the documentation for the invocation.
   * @throws NullPointerException if any field except secretArgument or doc is {@code null}.
   */
  public ContractInvocation {
    requireNonNull(kind);
    requireNonNull(name);
    requireNonNull(shortname);
    requireNonNull(arguments);
  }

  /**
   * Creates {@link ContractInvocation}.
   *
   * @param kind the kind of the invocation. Never null.
   * @param name the name of the invocation. Never null.
   * @param shortname shortname of the invocation.
   * @param arguments argument definitions of the invocation. Never null.
   * @throws NullPointerException if any field is {@code null}.
   */
  public ContractInvocation(
      ContractInvocationKind kind, String name, byte[] shortname, List<ArgumentAbi> arguments) {
    this(kind, name, shortname, arguments, null, DocumentationFunction.empty());
  }

  /**
   * Creates {@link ContractInvocation}.
   *
   * @param kind the kind of the invocation. Never null.
   * @param name the name of the invocation. Never null.
   * @param shortname shortname of the invocation.
   * @param arguments argument definitions of the invocation. Never null.
   * @param secretArgument secret argument of the invocation. Null unless kind is ZkSecretInputNew.
   * @throws NullPointerException if any field is {@code null}.
   */
  public ContractInvocation(
      ContractInvocationKind kind,
      String name,
      byte[] shortname,
      List<ArgumentAbi> arguments,
      ArgumentAbi secretArgument) {
    this(kind, name, shortname, arguments, secretArgument, DocumentationFunction.empty());
  }

  /**
   * Get the integer identifier for the invocation kind.
   *
   * @return invocation kind identifier
   */
  public int kindIdentifier() {
    return kind().kindId();
  }

  /**
   * Get the shortname as a lowercase hex string.
   *
   * @return the string representation
   */
  public String shortnameAsString() {
    return HexFormat.of().formatHex(shortname);
  }
}
