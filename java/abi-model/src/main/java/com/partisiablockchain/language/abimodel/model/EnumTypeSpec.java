package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.List;

/**
 * Type specification representing an enum specified by a name and a list of enum variants. Each
 * variant should be a reference to a struct defining the name and fields of the variant.
 *
 * @param name Name of the enum type.
 * @param variants List of variants.
 */
public record EnumTypeSpec(String name, List<EnumVariant> variants) implements NamedTypeSpec {

  /**
   * Creates EnumTypeSpec.
   *
   * @param name Name of the enum type. Never null.
   * @param variants List of variants. Never null.
   * @throws NullPointerException if any non-null field is {@code null}.
   */
  public EnumTypeSpec {
    requireNonNull(name);
    requireNonNull(variants);
  }

  /**
   * Gets a variant by discriminant, null if no such variant exists.
   *
   * @param discriminant the discriminant of the variant
   * @return the variant with the given discriminant.
   */
  public EnumVariant variant(int discriminant) {
    return variants.stream()
        .filter(variant -> variant.discriminant() == discriminant)
        .findAny()
        .orElse(null);
  }
}
