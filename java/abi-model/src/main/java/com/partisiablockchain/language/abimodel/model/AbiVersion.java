package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.regex.Pattern;

/**
 * Stores information regarding versioning of the abi using semantic versioning.
 *
 * @param major the major (backwards incompatible) version
 * @param minor the minor (backwards compatible) version
 * @param patch the patch (backwards and forwards compatible) version.
 */
public record AbiVersion(int major, int minor, int patch) {
  @Override
  public String toString() {
    return String.format("%d.%d.%d", major(), minor(), patch());
  }

  /**
   * Produces a version of the ABI version with the patch set to 0.
   *
   * @return never null
   */
  public AbiVersion withZeroPatch() {
    return new AbiVersion(major(), minor(), 0);
  }

  /**
   * Parse the ABI version on x.y.z form.
   *
   * @param stringVersion the version
   * @return the resulting instance
   */
  public static AbiVersion parseFromString(String stringVersion) {
    var pattern = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)");
    var matcher = pattern.matcher(stringVersion);
    if (matcher.matches()) {
      var major = Integer.parseInt(matcher.group(1));
      var minor = Integer.parseInt(matcher.group(2));
      var patch = Integer.parseInt(matcher.group(3));

      return new AbiVersion(major, minor, patch);
    } else {
      throw new IllegalArgumentException("Invalid version: " + stringVersion);
    }
  }
}
