package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.DocumentationFunction;
import com.partisiablockchain.language.abimodel.model.DocumentationNamedType;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/** Pretty printer for printing a binder abi in a rust-like syntax. */
public final class BinderRustSyntaxPrettyPrinter {

  private static final Map<TransactionKind, String> INVOCATION_KIND_MACRO_NAMES =
      Map.ofEntries(
          Map.entry(TransactionKind.Init, "init"),
          Map.entry(TransactionKind.Action, "action"),
          Map.entry(TransactionKind.Callback, "callback"),
          Map.entry(TransactionKind.Upgrade, "upgrade"),
          Map.entry(TransactionKind.EmptyAction, "empty_action"));

  private final FileAbi model;
  private final BinderAbi binderAbi;
  private final TypeSpecStringifier binderTypeStringifier;

  private BinderRustSyntaxPrettyPrinter(
      FileAbi model, BinderAbi binderAbi, TypeSpecStringifier binderTypeStringifier) {
    this.model = model;
    this.binderAbi = binderAbi;
    this.binderTypeStringifier = binderTypeStringifier;
  }

  /**
   * Create a BinderRustSyntaxPrettyPrinter.
   *
   * @param model the model to pretty print.
   * @return the pretty printer
   */
  public static BinderRustSyntaxPrettyPrinter create(FileAbi model) {
    BinderAbi binderAbi = (BinderAbi) model.chainComponent();
    return new BinderRustSyntaxPrettyPrinter(
        model,
        binderAbi,
        new TypeSpecStringifier(binderAbi.namedTypes().stream().map(NamedTypeSpec::name).toList()));
  }

  /**
   * Prints the specified model.
   *
   * @return string representation of the model in a rust-like syntax
   */
  public String printModel() {
    StringBuilder builder = new StringBuilder();

    // Build version information
    builder.append("// Version Binder: ");
    buildVersion(builder, model.versionBinder());
    builder.append("// Version Client: ");
    buildVersion(builder, model.versionClient());
    builder.append("\n");

    // Build binder type
    builder.append("// Binder Type: ").append(binderAbi.binderType());
    builder.append("\n\n");

    // Build list of struct definitions

    buildBinderOnly(builder);
    return builder.toString();
  }

  static void buildInvocationKindHeader(
      StringBuilder builder,
      List<ContractInvocationKind> contractInvocationKinds,
      BinderInvocation invocation) {
    TransactionKind kind = invocation.kind();
    builder.append("#[binder_").append(INVOCATION_KIND_MACRO_NAMES.get(kind));
    ArrayList<String> modifiers = new ArrayList<>();
    if (invocation.shortname() != null) {
      modifiers.add("shortname = 0x%02x".formatted(invocation.shortname()));
    }
    if (invocation.contractCallKindId() != null) {
      String callKindName =
          ContractInvocationKind.getKind(contractInvocationKinds, invocation.contractCallKindId())
              .name()
              .toLowerCase(Locale.ENGLISH);
      modifiers.add("contract_call = " + callKindName);
    }
    if (!modifiers.isEmpty()) {
      builder.append("(");
      builder.append(String.join(", ", modifiers));
      builder.append(")");
    }
    builder.append("]\n");
  }

  static void buildNamedTypes(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<NamedTypeSpec> namedTypes,
      TypeSpec stateType) {
    List<NamedTypeSpec> namedTypesCopy = new ArrayList<>(namedTypes);
    if (stateType instanceof NamedTypeRef stateRef) {
      namedTypesCopy.remove(stateRef.index());
    }
    for (NamedTypeSpec namedTypeSpec : namedTypesCopy) {
      buildNamedDefinition(builder, typeStringifier, namedTypes, namedTypeSpec);
    }
  }

  private void buildBinderOnly(StringBuilder builder) {
    buildCallableKinds(builder, binderAbi.invocationKinds());
    buildStateDefinition(
        builder, binderTypeStringifier, binderAbi.namedTypes(), binderAbi.stateType());
    buildNamedTypes(builder, binderTypeStringifier, binderAbi.namedTypes(), binderAbi.stateType());
    for (final BinderInvocation invocation : binderAbi.binderInvocations()) {
      buildBinderInvocation(
          builder, binderTypeStringifier, binderAbi.invocationKinds(), invocation);
    }
  }

  static void buildBinderInvocation(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<ContractInvocationKind> contractInvocationKinds,
      BinderInvocation invocation) {
    buildInvocationKindHeader(builder, contractInvocationKinds, invocation);
    buildBinderInvocationSignature(builder, typeStringifier, invocation);
  }

  static void buildBinderInvocationSignature(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      BinderInvocation binderInvocation) {
    builder.append("pub fn ").append(binderInvocation.name()).append(" (");
    if (!binderInvocation.arguments().isEmpty()) {
      builder.append("\n");
    }
    for (final ArgumentAbi arg : binderInvocation.arguments()) {
      buildArgument(builder, typeStringifier, arg);
    }
    builder.append(")\n\n");
  }

  private static void buildCallableKinds(
      StringBuilder builder, List<ContractInvocationKind> contractInvocationKinds) {
    builder.append("enum ContractCallableKinds {\n");
    for (ContractInvocationKind contractInvocationKind : contractInvocationKinds) {
      builder
          .append("  ")
          .append(contractInvocationKind.name())
          .append(" = ")
          .append(contractInvocationKind.kindId())
          .append(",\n");
    }
    builder.append("}\n\n");
  }

  private static void buildVersion(StringBuilder builder, AbiVersion version) {
    builder
        .append(version.major())
        .append(".")
        .append(version.minor())
        .append(".")
        .append(version.patch())
        .append("\n");
  }

  static void buildArgument(
      StringBuilder builder, TypeSpecStringifier typeStringifier, ArgumentAbi argument) {
    builder.append("    ").append(argument.name()).append(": ");
    typeStringifier.stringify(builder, argument.type());
    builder.append(",\n");
  }

  static void buildNamedDefinition(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<NamedTypeSpec> namedTypes,
      NamedTypeSpec namedTypeSpec) {
    if (namedTypeSpec instanceof StructTypeSpec structTypeSpec
        && !isEnumVariant(structTypeSpec, namedTypes)) {
      buildStructDefinition(builder, typeStringifier, structTypeSpec);
    } else if (namedTypeSpec instanceof EnumTypeSpec enumTypeSpec) {
      buildEnumDefinition(builder, typeStringifier, namedTypes, enumTypeSpec);
    }
  }

  private static boolean isEnumVariant(
      StructTypeSpec structTypeSpec, List<NamedTypeSpec> namedTypes) {
    for (NamedTypeSpec namedTypeSpec : namedTypes) {
      if (namedTypeSpec instanceof EnumTypeSpec enumTypeSpec) {
        for (EnumVariant variant : enumTypeSpec.variants()) {
          if (namedTypes.get(variant.def().index()).equals(structTypeSpec)) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private static void buildEnumDefinition(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<NamedTypeSpec> namedTypes,
      EnumTypeSpec enumTypeSpec) {
    builder.append("enum ").append(enumTypeSpec.name()).append(" {\n");
    for (EnumVariant variant : enumTypeSpec.variants()) {

      buildEnumVariant(builder, typeStringifier, namedTypes, variant);
    }
    builder.append("}\n\n");
  }

  private static void buildEnumVariant(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<NamedTypeSpec> namedTypes,
      EnumVariant variant) {
    StructTypeSpec namedTypeSpec = (StructTypeSpec) namedTypes.get(variant.def().index());
    builder.append("    ").append("#[discriminant(").append(variant.discriminant()).append(")]\n");
    builder.append("    ").append(namedTypeSpec.name()).append(" {");

    for (Iterator<FieldAbi> iterator = namedTypeSpec.fields().iterator(); iterator.hasNext(); ) {
      FieldAbi field = iterator.next();
      builder.append(" ").append(field.name()).append(": ");
      typeStringifier.stringify(builder, field.type());
      if (iterator.hasNext()) {
        builder.append(",");
      } else {
        builder.append(" ");
      }
    }
    builder.append("},\n");
  }

  static void buildStateDefinition(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      List<NamedTypeSpec> namedTypes,
      TypeSpec stateType) {
    if (stateType instanceof NamedTypeRef stateRef) {
      StructTypeSpec state = (StructTypeSpec) namedTypes.get(stateRef.index());
      buildDocumentationForStruct(builder, typeStringifier, state, state.doc());
      builder.append("#[state]\n");
      builder.append("pub struct ").append(state.name()).append(" {\n");
      for (FieldAbi field : state.fields()) {
        buildFields(builder, typeStringifier, field);
      }
      builder.append("}\n\n");
    } else {
      builder.append("#[state]\n");
      typeStringifier.stringify(builder, stateType);
      builder.append("\n\n");
    }
  }

  private static void buildDocumentationForStruct(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      StructTypeSpec structAbi,
      DocumentationNamedType doc) {
    if (doc == null) {
      return;
    }

    var docBuilder = new RustSyntaxPrettyPrinter.DocBuilder("/// ", builder, typeStringifier);
    docBuilder.appendParagraph(doc.description());
    docBuilder.appendFields(structAbi.fields(), doc.fields());
  }

  static void buildDocumentationForFn(
      StringBuilder builder,
      TypeSpecStringifier typeStringifier,
      ContractInvocation contractInvocation,
      DocumentationFunction doc) {
    if (doc == null) {
      return;
    }

    var docBuilder = new RustSyntaxPrettyPrinter.DocBuilder("/// ", builder, typeStringifier);
    docBuilder.appendParagraph(doc.description());
    docBuilder.appendArguments(contractInvocation.arguments(), doc.arguments());

    if (doc.returns() != null) {
      docBuilder.appendReturns(doc.returns());
    }
  }

  static void buildStructDefinition(
      StringBuilder builder, TypeSpecStringifier typeStringifier, StructTypeSpec struct) {

    buildDocumentationForStruct(builder, typeStringifier, struct, struct.doc());
    builder.append("pub struct ").append(struct.name()).append(" {\n");
    for (FieldAbi field : struct.fields()) {
      buildFields(builder, typeStringifier, field);
    }
    builder.append("}\n\n");
  }

  static void buildFields(
      StringBuilder builder, TypeSpecStringifier typeStringifier, FieldAbi field) {
    builder.append("    ").append(field.name()).append(": ");
    typeStringifier.stringify(builder, field.type());
    builder.append(",\n");
  }
}
