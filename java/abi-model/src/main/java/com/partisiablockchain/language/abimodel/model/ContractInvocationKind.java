package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * The kind for a contract invocation definition.
 *
 * <p>Used to determine which {@link ContractInvocation} a {@link BinderInvocation} will call. The
 * binder defines the list of possible contract invocation kinds.
 *
 * @param kindId id of the kind
 * @param name name of the kind
 * @param hasShortname whether a function with this kind has a shortname
 */
public record ContractInvocationKind(int kindId, String name, boolean hasShortname) {
  /**
   * Get a specific function kind from a list of function kinds by the kind id.
   *
   * @param kinds list of possible function kinds
   * @param kindId the id of the kind to get
   * @return the function kind matching the kind id
   */
  public static ContractInvocationKind getKind(List<ContractInvocationKind> kinds, int kindId) {
    return kinds.stream().filter(kind -> kind.kindId == kindId).findFirst().orElse(null);
  }
}
