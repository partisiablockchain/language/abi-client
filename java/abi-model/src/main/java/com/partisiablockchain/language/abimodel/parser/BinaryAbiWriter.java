package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.secata.stream.BigEndianByteOutput;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/** Writes an ABI to the binary form. */
public final class BinaryAbiWriter {

  private final FileAbi abi;
  private final BigEndianByteOutput out;
  private final Configuration format;

  /**
   * Create a new writer.
   *
   * @param abi the abi to write
   * @param out the output
   */
  public BinaryAbiWriter(FileAbi abi, BigEndianByteOutput out) {
    this.abi = abi;
    this.out = out;
    format = abi.format();
  }

  /**
   * Create a new writer.
   *
   * @param abi the abi to write
   * @param out the output
   */
  public BinaryAbiWriter(FileAbi abi, OutputStream out) {
    this(abi, new BigEndianByteOutput(out));
  }

  /**
   * Write the ABI and return the byte array.
   *
   * @param abi the abi to write
   * @return the resulting byte array
   */
  public static byte[] serialize(FileAbi abi) {
    var stream = new ByteArrayOutputStream();
    new BinaryAbiWriter(abi, stream).write();

    return stream.toByteArray();
  }

  /** Write the ABI to the stream. */
  public void write() {
    writeHeader();
    if (format.shortnameType() == Configuration.ShortnameType.HASH) {
      out.writeU8(format.shortnameLength());
    }
    if (format.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      if (abi.chainComponent() instanceof ContractAbi contractAbi) {
        out.writeU8(0x10);
        writeContract(contractAbi);
      } else {
        BinderAbi binderAbi = (BinderAbi) abi.chainComponent();
        out.writeU8(0x20);
        writeBinder(binderAbi);
      }
    } else {
      writeContract((ContractAbi) abi.chainComponent());
    }
  }

  private void writeHeader() {
    out.writeBytes(abi.header().getBytes(StandardCharsets.UTF_8));
    writeVersion(abi.versionBinder());
    writeVersion(abi.versionClient());
  }

  private void writeContract(ContractAbi contract) {
    if (format.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      out.writeU8(contract.binderType().getId());
      out.writeI32(contract.invocationKinds().size());
      for (ContractInvocationKind kind : contract.invocationKinds()) {
        writeFunctionKind(kind);
      }
    }

    out.writeI32(contract.namedTypes().size());
    for (NamedTypeSpec namedType : contract.namedTypes()) {
      writeNamedTypeSpec(namedType);
    }

    if (format.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      out.writeI32(contract.binderInvocations().size());
      for (BinderInvocation binderInvocation : contract.binderInvocations()) {
        writeBinderInvocation(binderInvocation);
      }
    }

    var writtenFunctions = new ArrayList<>(contract.contractInvocations());
    if (format.fnType() == Configuration.FunctionFormat.InitSeparately) {
      writeContractCall(contract.init());
      writtenFunctions.remove(contract.init());
    }

    out.writeI32(writtenFunctions.size());
    for (ContractInvocation fn : writtenFunctions) {
      writeContractCall(fn);
    }

    writeTypeSpec(contract.stateType());
  }

  private void writeBinderInvocation(BinderInvocation binderInvocation) {
    out.writeU8(binderInvocation.kind().getKindId());
    out.writeString(binderInvocation.name());

    if (binderInvocation.shortname() != null) {
      out.writeBoolean(true);
      out.writeU8(binderInvocation.shortname());
    } else {
      out.writeBoolean(false);
    }

    out.writeI32(binderInvocation.arguments().size());
    for (ArgumentAbi argument : binderInvocation.arguments()) {
      writeNameAndTypeSpec(argument);
    }
    if (binderInvocation.contractCallKindId() != null) {
      out.writeBoolean(true);
      out.writeU8(binderInvocation.contractCallKindId());
    } else {
      out.writeBoolean(false);
    }
  }

  private void writeBinder(BinderAbi binder) {
    out.writeU8(binder.binderType().getId());
    out.writeI32(binder.invocationKinds().size());
    for (ContractInvocationKind kind : binder.invocationKinds()) {
      writeFunctionKind(kind);
    }

    out.writeI32(binder.namedTypes().size());
    for (NamedTypeSpec namedType : binder.namedTypes()) {
      writeNamedTypeSpec(namedType);
    }

    out.writeI32(binder.binderInvocations().size());
    for (BinderInvocation invocation : binder.binderInvocations()) {
      writeBinderInvocation(invocation);
    }

    writeTypeSpec(binder.stateType());
  }

  private void writeTypeSpec(TypeSpec uncheckedType) {
    out.writeI8(uncheckedType.typeIndex().getValue());

    if (uncheckedType instanceof VecTypeSpec type) {
      writeTypeSpec(type.valueType());

    } else if (uncheckedType instanceof SetTypeSpec type) {
      writeTypeSpec(type.valueType());

    } else if (uncheckedType instanceof OptionTypeSpec type) {
      writeTypeSpec(type.valueType());

    } else if (uncheckedType instanceof SizedArrayTypeSpec type) {
      writeTypeSpec(type.valueType());
      out.writeU8(type.length());

    } else if (uncheckedType instanceof NamedTypeRef type) {
      out.writeU8(type.index());

    } else if (uncheckedType instanceof MapTypeSpec type) {
      writeTypeSpec(type.keyType());
      writeTypeSpec(type.valueType());

    } else if (uncheckedType instanceof AvlTreeMapTypeSpec type) {
      writeTypeSpec(type.keyType());
      writeTypeSpec(type.valueType());
    }
  }

  private void writeContractCall(ContractInvocation fn) {
    if (format.fnType() == Configuration.FunctionFormat.FnKind) {
      out.writeU8(fn.kind().kindId());
    }

    out.writeString(fn.name());
    if (format.shortnameType() == Configuration.ShortnameType.LEB128) {
      out.writeBytes(fn.shortname());
    }

    out.writeI32(fn.arguments().size());
    for (ArgumentAbi argument : fn.arguments()) {
      writeNameAndTypeSpec(argument);
    }

    if (format.chainComponentFormat() == Configuration.ChainComponentFormat.CHAIN_COMPONENTS) {
      if (fn.secretArgument() != null) {
        out.writeBoolean(true);
        writeNameAndTypeSpec(fn.secretArgument());
      } else {
        out.writeBoolean(false);
      }
    } else {
      if (fn.kind().kindId()
          == ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId()) {
        writeNameAndTypeSpec(fn.secretArgument());
      }
    }
  }

  private void writeNamedTypeSpec(NamedTypeSpec spec) {
    if (spec instanceof EnumTypeSpec enumSpec) {
      out.writeU8(NamedTypeSpec.NamedTypeIndex.Enum.getValue());

      out.writeString(spec.name());
      out.writeI32(enumSpec.variants().size());
      for (EnumVariant variant : enumSpec.variants()) {
        writeEnumVariant(variant);
      }
    } else {
      StructTypeSpec struct = (StructTypeSpec) spec;
      if (format.namedTypesFormat() == Configuration.NamedTypesFormat.StructsAndEnum) {
        out.writeU8(NamedTypeSpec.NamedTypeIndex.Struct.getValue());
      }

      out.writeString(spec.name());
      out.writeI32(struct.fields().size());
      for (FieldAbi field : struct.fields()) {
        writeNameAndTypeSpec(field);
      }
    }
  }

  private void writeEnumVariant(EnumVariant variant) {
    out.writeU8(variant.discriminant());
    writeTypeSpec(variant.def());
  }

  private void writeNameAndTypeSpec(NameAndType nameAndType) {
    out.writeString(nameAndType.name());
    writeTypeSpec(nameAndType.type());
  }

  private void writeVersion(AbiVersion version) {
    out.writeI8(version.major());
    out.writeI8(version.minor());
    out.writeI8(version.patch());
  }

  private void writeFunctionKind(ContractInvocationKind kind) {
    out.writeU8(kind.kindId());
    out.writeString(kind.name());
    out.writeBoolean(kind.hasShortname());
  }
}
