package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.function.Function.identity;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/** Different types of transactions called by the blockchain to a binder. */
public enum TransactionKind {
  /** Init transaction. */
  Init(0x01),

  /** Action transaction. */
  Action(0x02),

  /** Callback transaction. */
  Callback(0x03),

  /** Upgrade transaction. */
  Upgrade(0x04),

  /**
   * Special action transaction. This is called by the same type of transaction that calls the
   * action kind, but it contains an empty rpc stream.
   */
  EmptyAction(0x05);

  /** ID of the TransactionKind as specified in the ABI specification. */
  private final int kindId;

  /**
   * Retrieves id of the TransactionKind as specified in the ABI specification.
   *
   * @return id
   */
  public int getKindId() {
    return kindId;
  }

  TransactionKind(final int kindId) {
    this.kindId = kindId;
  }

  private static final Map<Integer, TransactionKind> KIND_BY_VALUE =
      Arrays.stream(values()).collect(Collectors.toMap(TransactionKind::getKindId, identity()));

  /**
   * Determines TransactionKind from the given id.
   *
   * @param kindId ID to determine kind from.
   * @return the given kind, or null
   */
  public static TransactionKind fromKindId(final int kindId) {
    return KIND_BY_VALUE.get(kindId);
  }
}
