package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.List;

/**
 * Representation of a StructTypeSpec, specified by a name and a list of fields. Each field is
 * responsible for storing its own type.
 *
 * <p>name is the name of the struct. fields is a list of the fields present within the struct
 */
public record StructTypeSpec(String name, List<FieldAbi> fields, DocumentationNamedType doc)
    implements NamedTypeSpec {

  /**
   * Creates StructTypeSpec.
   *
   * @param name Name of the struct type. Never null.
   * @param fields List of struct fields. Never null. Order is important.
   * @param doc the documentation for the struct.
   * @throws NullPointerException if any non-null field is {@code null}.
   */
  public StructTypeSpec {
    requireNonNull(name);
    requireNonNull(fields);
  }

  /**
   * Creates StructTypeSpec.
   *
   * @param name Name of the struct type. Never null.
   * @param fields List of struct fields. Never null. Order is important.
   * @throws NullPointerException if any non-null field is {@code null}.
   */
  public StructTypeSpec(String name, List<FieldAbi> fields) {
    this(name, fields, DocumentationNamedType.empty());
  }

  /**
   * Gets a field by index, does not check bounds.
   *
   * @param index the index of the field
   * @return the field at said index
   */
  public FieldAbi field(int index) {
    return fields.get(index);
  }

  /**
   * Get a field by name.
   *
   * @param name the name of the field
   * @return the field if exists, null otherwise
   */
  public FieldAbi getFieldByName(String name) {
    return fields.stream().filter(field -> field.name().equals(name)).findAny().orElse(null);
  }
}
