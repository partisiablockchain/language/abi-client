package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Map;

/** Documentation for a function. */
public record DocumentationFunction(
    List<String> description, Map<String, String> arguments, String returns) {

  /**
   * Create documentation for a function.
   *
   * @param description the description of the function
   * @param arguments a map from argument name to argument description
   * @param returns what the function returns
   */
  public DocumentationFunction {}

  /**
   * Create a new, empty documentation holder.
   *
   * @return the empty documentation
   */
  static DocumentationFunction empty() {
    return new DocumentationFunction(List.of(), Map.of(), null);
  }
}
