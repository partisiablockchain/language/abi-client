package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Representation of a contract on the blockchain.
 *
 * <p>A contract is compiled with a binder, and this record contains the combined information of the
 * binder and the contract. This includes all the information required to build and read rpc calls
 * against the contract, as well as read the state of the contract.
 *
 * @param binderType type of binder. PUBLIC or ZK
 * @param invocationKinds list of callable contract invocation kinds. Defined by the binder and is
 *     used to determine which contract function a binder invocation will call.
 * @param namedTypes list of named type declarations. Includes both types defined by the binder and
 *     the contract
 * @param binderInvocations list of binder invocations. Defined by the binder
 * @param contractInvocations list of contract invocations. Defined by the contract
 * @param stateType contract's state type. Includes both the binder state and the contract state
 */
public record ContractAbi(
    BinderType binderType,
    List<ContractInvocationKind> invocationKinds,
    List<NamedTypeSpec> namedTypes,
    List<BinderInvocation> binderInvocations,
    List<ContractInvocation> contractInvocations,
    TypeSpec stateType)
    implements ChainComponent {

  /**
   * Creates ContractAbi.
   *
   * @param binderType type of binder. PUBLIC or ZK
   * @param invocationKinds list of callable contract invocation kinds
   * @param namedTypes List of named type declarations. Never null.
   * @param binderInvocations List of binder invocations. Never null.
   * @param contractInvocations List of contract invocations . Never null.
   * @param stateType Contract's state type. Never null.
   * @throws NullPointerException if any non-null field is {@code null}.
   */
  public ContractAbi {
    requireNonNull(invocationKinds);
    requireNonNull(namedTypes);
    requireNonNull(binderInvocations);
    requireNonNull(contractInvocations);
    requireNonNull(stateType);
  }

  /**
   * Finds the init contract invocation.
   *
   * @return null if not init not defined
   */
  public ContractInvocation init() {
    BinderInvocation initBinderInvocation = initBinderInvocation();
    Integer initKindId = initBinderInvocation.contractCallKindId();
    return contractInvocations.stream()
        .filter(h -> h.kind().kindId() == initKindId)
        .findAny()
        .orElse(null);
  }

  /**
   * Finds the init binder invocation.
   *
   * @return init or null if it is not defined
   */
  public BinderInvocation initBinderInvocation() {
    return binderInvocations.stream()
        .filter(h -> h.kind() == TransactionKind.Init)
        .findAny()
        .orElse(null);
  }

  /**
   * Returns the contract invocation with the given name, or null if it doesn't exist.
   *
   * @param name the name of the function
   * @return the found action or null
   */
  public ContractInvocation getFunctionByName(String name) {
    return contractInvocations.stream().filter(h -> h.name().equals(name)).findAny().orElse(null);
  }

  /**
   * Returns the contract invocation with the given shortname, or null if it doesn't exist.
   *
   * @param shortname string representation of the shortname
   * @param kind the FnKind of the function
   * @return action with the given shortname
   */
  public ContractInvocation getFunction(byte[] shortname, ContractInvocationKind kind) {
    return contractInvocations.stream()
        .filter(h -> Arrays.equals(h.shortname(), shortname))
        .filter(h -> h.kind().kindId() == kind.kindId())
        .findAny()
        .orElse(null);
  }

  /**
   * Returns the binder invocation with the given shortname, or null if it doesn't exist.
   *
   * @param binderShortname shortname of the binder invocation
   * @param kind the kind of the invocation
   * @return function matching the shortnames
   */
  public BinderInvocation getBinderInvocation(Byte binderShortname, TransactionKind kind) {
    return binderInvocations.stream()
        .filter(h -> Objects.equals(h.shortname(), binderShortname) && h.kind().equals(kind))
        .findAny()
        .orElse(null);
  }

  /**
   * Returns the binder invocation with the given shortnames, or null if it doesn't exist.
   *
   * @param name name of the binder invocation
   * @return function matching the shortnames
   */
  public BinderInvocation getBinderInvocationByName(String name) {
    return binderInvocations.stream().filter(h -> h.name().equals(name)).findAny().orElse(null);
  }

  /**
   * Returns the named type given by the specified reference.
   *
   * @param namedTypeRef the reference to the named type
   * @return the named type given by the reference
   */
  @Override
  public NamedTypeSpec getNamedType(NamedTypeRef namedTypeRef) {
    return namedTypes.get(namedTypeRef.index());
  }

  /**
   * Returns the named type given by the specified name.
   *
   * @param name the name of the named type
   * @return the named type with the given name or null if no such named type exist.
   */
  @Override
  public NamedTypeSpec getNamedType(String name) {
    return namedTypes().stream()
        .filter((NamedTypeSpec namedType) -> namedType.name().equals(name))
        .findFirst()
        .orElse(null);
  }

  /**
   * Returns the state struct.
   *
   * @return the state struct
   */
  public StructTypeSpec getStateStruct() {
    return (StructTypeSpec) getNamedType((NamedTypeRef) stateType);
  }

  /**
   * Returns a boolean determining if the ABI contains a zk function.
   *
   * @return a boolean determining if the ABI contains a zk function
   */
  public boolean isZk() {
    return binderType.equals(BinderType.Zk);
  }

  /**
   * Get all the contract invocations which can be called by one of the given binder invocations.
   *
   * <p>If a there does not exist a binder invocation for a given binder invocation name, the name
   * is ignored.
   *
   * @param binderInvocationNames list of binder invocation names for which to find the called
   *     contract invocations
   * @return the contract invocations which can be called by the given binder invocations
   */
  public List<ContractInvocation> getContractInvocations(List<String> binderInvocationNames) {
    var contractKinds =
        binderInvocations.stream()
            .filter(p -> binderInvocationNames.contains(p.name()))
            .map(BinderInvocation::contractCallKindId)
            .filter(Objects::nonNull)
            .toList();
    return contractInvocations.stream()
        .filter(p -> contractKinds.contains(p.kind().kindId()))
        .toList();
  }

  /**
   * Get all the contract invocations which can be called by given binder invocation.
   *
   * <p>If a there does not exist a binder invocation for the given binder invocation name, no
   * contract invocations are returned.
   *
   * @param binderInvocationName name of the binder invocation for which to find the called contract
   *     invocations
   * @return the contract invocations which can be called by the given binder invocation
   */
  public List<ContractInvocation> getContractInvocations(String binderInvocationName) {
    return getContractInvocations(List.of(binderInvocationName));
  }
}
