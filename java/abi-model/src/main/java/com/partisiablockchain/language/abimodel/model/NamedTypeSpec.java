package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Interface for the possible named types of the contract. Subtypes implement this interface to mark
 * them as valid named types as specified by the abi grammar.
 */
public interface NamedTypeSpec {

  /**
   * The name of this type spec object.
   *
   * @return the name
   */
  String name();

  /**
   * Mapping from the byte read when parsing a named type. The byte denotes whether the value to be
   * read is an enum or a struct.
   */
  enum NamedTypeIndex {
    /** Struct representation. */
    Struct((byte) 0x01),
    /** Enum representation. */
    Enum((byte) 0x02);

    private final byte value;

    NamedTypeIndex(byte value) {
      this.value = value;
    }

    /**
     * Retrieves the id of the named type, as specified by the abi spec.
     *
     * @return the id of the named type
     */
    public byte getValue() {
      return value;
    }
  }
}
