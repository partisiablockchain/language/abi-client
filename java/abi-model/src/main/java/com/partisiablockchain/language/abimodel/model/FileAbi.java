package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.parser.Configuration;
import java.util.List;

/**
 * Representation of a FileAbi. The FileAbi holds all information concerning the chain component, as
 * well as metadata about versioning and headers.
 *
 * @param header the header of the ABI format, expected to always be "PBCABI"
 * @param versionBinder the semantic version of the binder this contract is compiled with
 * @param versionClient the ABI version this contract is compiled with
 * @param shortnameLength length of shortnames. If null shortnames uses the LEB128 encoding. All new
 *     contracts uses LEB128.
 * @param chainComponent the parsed data of the chain component. Can be either a contract or a
 *     binder
 * @param description additional documentation of the chain component
 */
public record FileAbi(
    String header,
    AbiVersion versionBinder,
    AbiVersion versionClient,
    Integer shortnameLength,
    ChainComponent chainComponent,
    List<String> description) {

  /**
   * Create a new ABI.
   *
   * @param header the header
   * @param versionBinder the binder version
   * @param versionClient the client version
   * @param shortnameLength describes the length of the shortname is using HASH, null otherwise
   * @param chainComponent the contract abi
   */
  public FileAbi(
      String header,
      AbiVersion versionBinder,
      AbiVersion versionClient,
      Integer shortnameLength,
      ChainComponent chainComponent) {
    this(header, versionBinder, versionClient, shortnameLength, chainComponent, List.of());
  }

  /**
   * A configuration object describing the format of the ABI.
   *
   * @return the configuration
   */
  public Configuration format() {
    return Configuration.fromClientVersion(versionClient, shortnameLength);
  }

  /**
   * Get the chain component as a contract.
   *
   * <p>Throws a cast exception if the chain component is not a contract
   *
   * @return the contract
   */
  public ContractAbi contract() {
    return (ContractAbi) chainComponent;
  }
}
