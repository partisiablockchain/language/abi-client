package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A small parser that can read Rust-like type descriptions written using {@link
 * TypeSpecStringifier}.
 */
public final class TypeSpecStringParser {

  private static final Pattern PATTERN_GENERIC =
      Pattern.compile("^(?<base>\\w+)<(?<genericParams>.*)>$");
  private static final Pattern PATTERN_ARRAY =
      Pattern.compile("^\\[\\s*u8\\s*;\\s*(?<length>\\d+)\\s*]$");

  private final Map<String, TypeSpec> typeEnvironment;
  private final boolean checkNamedTypeIndices;

  /**
   * Construct a new parser.
   *
   * @param namedTypes the type names to use as a reference for indexed type refs
   * @param checkNamedTypeIndices whether to validate that indexed refs point to an existing type
   */
  public TypeSpecStringParser(List<String> namedTypes, boolean checkNamedTypeIndices) {
    this.typeEnvironment = new HashMap<>();
    this.checkNamedTypeIndices = checkNamedTypeIndices;

    // Primitive types
    for (TypeSpec.TypeIndex value : SimpleTypeSpec.validTypes) {
      typeEnvironment.put(value.name(), new SimpleTypeSpec(value));
    }
    /*
    Named types.
    Note:
    A named type with the same name as a built-in type (simple type) will take
    precedence in the type environment, meaning that the ambiguous name will be of type NamedType
    and not SimpleType.
    */
    for (int typeIndex = 0; typeIndex < namedTypes.size(); typeIndex++) {
      typeEnvironment.put(namedTypes.get(typeIndex), new NamedTypeRef(typeIndex));
    }
  }

  private static final TypeSpec UNKNOWN_TYPE = new NamedTypeRef(-1);

  /**
   * Parse a type spec from a string.
   *
   * @param typeName the string to parse
   * @return a type spec
   */
  public TypeSpec parse(String typeName) {
    if (typeEnvironment.containsKey(typeName)) {
      return typeEnvironment.get(typeName);
    }

    if (isGeneric(typeName)) {
      return parseGeneric(typeName);
    } else if (isArray(typeName)) {
      return parseArray(typeName);
    } else if (checkNamedTypeIndices) {
      throw new RuntimeException("Invalid type: " + typeName);
    }

    return UNKNOWN_TYPE;
  }

  /**
   * Parse an array type spec.
   *
   * @param string the string to parse
   * @return the resulting type
   */
  TypeSpec parseArray(String string) {
    Matcher matcher = PATTERN_ARRAY.matcher(string);

    if (!matcher.matches()) {
      return null;
    }

    int length = Integer.parseInt(matcher.group("length"));
    return SizedArrayTypeSpec.byteArray(length);
  }

  private boolean isArray(String string) {
    Matcher array = PATTERN_ARRAY.matcher(string);
    return array.matches();
  }

  /**
   * Parse a type spec with generics.
   *
   * @param string the string to parse
   * @return the resulting type
   */
  TypeSpec parseGeneric(String string) {
    Matcher matcher = PATTERN_GENERIC.matcher(string);

    if (!matcher.matches()) {
      return null;
    }

    String genericString = matcher.group("genericParams");
    final List<TypeSpec> generics =
        semanticSplitAtComma(genericString).stream().map(this::parse).toList();

    String base = matcher.group("base");
    return switch (base) {
      case "Map" -> new MapTypeSpec(generics.get(0), generics.get(1));
      case "Set" -> new SetTypeSpec(generics.get(0));
      case "Vec" -> new VecTypeSpec(generics.get(0));
      case "Option" -> new OptionTypeSpec(generics.get(0));
      case "AvlTreeMap" -> new AvlTreeMapTypeSpec(generics.get(0), generics.get(1));
      default -> throw new IllegalArgumentException("Invalid generic type: " + base);
    };
  }

  private boolean isGeneric(String string) {
    Matcher generic = PATTERN_GENERIC.matcher(string);
    return generic.matches();
  }

  /**
   * Split the generic part of a type description at the correct comma. The naive approach does not
   * work for nested generics such as Map&lt;String, Map&lt;String, String&gt;&gt;.
   *
   * @param generic the generic part of a type
   * @return the list of types
   */
  private static List<String> semanticSplitAtComma(String generic) {
    int splitIndex = findSemanticSplitIndex(generic);

    List<String> split;
    if (splitIndex != -1) {
      String left = generic.substring(0, splitIndex).trim();
      String right = generic.substring(splitIndex + 1).trim();
      split = List.of(left, right);
    } else {
      split = List.of(generic);
    }
    return split;
  }

  /**
   * Find the index at which the part between &lt; and &gt; should be split.
   *
   * @param generic the generic to split
   * @return the split index
   */
  private static int findSemanticSplitIndex(String generic) {
    int nesting = 0;
    for (int i = 0; i < generic.length(); i++) {
      char c = generic.charAt(i);
      if (c == '<') {
        nesting++;
      } else if (c == '>') {
        nesting--;
      } else if (c == ',' && nesting == 0) {
        return i;
      }
    }
    return -1;
  }
}
