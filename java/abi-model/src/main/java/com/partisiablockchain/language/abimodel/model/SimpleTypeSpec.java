package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Set;

/** Enum representing possible simple types in the contract. */
public record SimpleTypeSpec(TypeIndex typeIndex) implements TypeSpec {

  /** The set of types classified as simple. */
  public static final Set<TypeIndex> validTypes =
      Set.of(
          TypeIndex.u8,
          TypeIndex.u16,
          TypeIndex.u32,
          TypeIndex.u64,
          TypeIndex.u128,
          TypeIndex.u256,
          TypeIndex.i8,
          TypeIndex.i16,
          TypeIndex.i32,
          TypeIndex.i64,
          TypeIndex.i128,
          TypeIndex.String,
          TypeIndex.bool,
          TypeIndex.Address,
          TypeIndex.Hash,
          TypeIndex.PublicKey,
          TypeIndex.Signature,
          TypeIndex.BlsPublicKey,
          TypeIndex.BlsSignature);

  /**
   * Construct a simple type spec.
   *
   * @param typeIndex the values typeIndex
   */
  public SimpleTypeSpec {
    if (!validTypes.contains(typeIndex)) {
      throw new IllegalArgumentException("Given type is not a simple type: " + typeIndex);
    }
  }
}
