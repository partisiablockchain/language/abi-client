package com.partisiablockchain.language.abimodel.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.Serial;

/** Runtime exception representation of an error occurring during the parsing of an abi file. */
public final class AbiParseException extends RuntimeException {

  @Serial private static final long serialVersionUID = 1L;

  /**
   * Constructor for runtime exception of parsing error using {@link String#format} as formatting.
   *
   * @param message error message
   * @param args arguments for formatting
   */
  @SuppressWarnings("AnnotateFormatMethod")
  public AbiParseException(String message, Object... args) {
    super(String.format(message, args));
  }
}
