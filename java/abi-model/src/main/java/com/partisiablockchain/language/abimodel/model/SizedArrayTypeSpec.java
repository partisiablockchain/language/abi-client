package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Representation of a SizedArrayType, specified by the type of the valueType within the
 * SizedArrayType.
 *
 * <p>{@link #valueType} is the type of the elements which the SizedArrayType holds
 */
public record SizedArrayTypeSpec(TypeSpec valueType, int length) implements TypeSpec {

  /**
   * Create new byte array type.
   *
   * @param length Length of byte array type.
   * @return Byte array type
   */
  public static SizedArrayTypeSpec byteArray(int length) {
    return new SizedArrayTypeSpec(new SimpleTypeSpec(TypeIndex.u8), length);
  }

  @Override
  public TypeIndex typeIndex() {
    return TypeIndex.SizedArray;
  }
}
