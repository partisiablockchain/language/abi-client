package com.partisiablockchain.language.abimodel.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * A chain component on the blockchain.
 *
 * <p>Common interface for all components on the blockchain capable of carrying an ABI. Currently,
 * this can be either a {@link ContractAbi} or {@link BinderAbi}.
 */
public interface ChainComponent {

  /**
   * The named types of the component.
   *
   * @return the named types
   */
  List<NamedTypeSpec> namedTypes();

  /**
   * The type of the state.
   *
   * @return type of state
   */
  TypeSpec stateType();

  /**
   * Returns the named type given by the specified name.
   *
   * @param name the name of the named type
   * @return the named type with the given name or null if no such named type exist.
   */
  NamedTypeSpec getNamedType(String name);

  /**
   * Returns the named type given by the specified reference.
   *
   * @param namedTypeRef the reference to the named type
   * @return the named type given by the reference
   */
  NamedTypeSpec getNamedType(NamedTypeRef namedTypeRef);
}
