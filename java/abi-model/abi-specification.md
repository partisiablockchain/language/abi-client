# ABI format Partisia

````

<File> := {
    Header:             0xnn*6,         
    Binder version:     0xnn*3,         
    Version ABI:        0xnn*3,         
    Chain Component:    ChainComponent
}

<ChainComponent> := 
    | 0x10 Contract
    | 0x20 Binder               

<Binder> := {
    BinderType:             BinderType,
    InvocationKinds:        List<ContractInvocationKind>,
    NamedTypes:             List<NamedType>,
    BinderInvocations:      List<BinderInvocation>,
    State:                  Type
} 	

<Contract> := {
    BinderType:             BinderType,
    InvocationKinds:        List<ContractInvocationKind>,
    Named Types:            List<NamedType>,
    BinderInvocations:      List<BinderInvocation>,
    ContractInvocations:    List<ContractInvocation>,
    State type:             Type
}

<BinderType> := 
    | 0x00 => Public
    | 0x01 => ZK
    | 0x02 => Sys

<ContractInvocation> := {
    kindId:             0xnn,
    Name:               Identifier,
    Shortname:          LEB128,
    Arguments:          List<Argument>,
    Secret Argument:    Option<Argument>
}

<BinderInvocation> := {
    Kind:           TransactionKind,
    Name:           Identifier,
    Shortname:      Option<0xnn>,
    Arguments:      List<Argument>,
    contractCallId: Option<0xnn>
}

<ContractInvocationKind> := {
    id:             0xnn,
    name:           String,
    hasShortname:   0xnn
}

<TransactionKind> := 
    | 0x01 => Init
    | 0x02 => Action
    | 0x03 => Callback
    | 0x04 => Upgrade
    | 0x05 => EmptyAction

<NamedType> := 
    | 0x01 StructType
    | 0x02 EnumType

<StructType> := {
    Name:       Identifier,
    Fields:     List<Field>
}


<EnumType> := {
    Name:       Identifier,
    Variants:   List<EnumVariants>
}

<EnumVariant> := {
    Discriminant:   0xnn,
    Definition:     NamedTypeReference
}

<Field> := {
    Name:   Identifier,
    Type:   Type
}

<Argument> := {
    Name:   Identifier,
    Type:   Type
}

<Identifier> := Length: 0xnn*4 UTF8-Identifier: 0xnn*Length 	
{ Length is encoded in Big Endian. }
{ UTF8-Identifier must be a Rust identifier.}

<LEB128> := A LEB128 encoded unsigned 32-bit integer (1-5 bytes)

````

## Function Kind

````
<FunctionKindOld> :=  
    | 0x01 => Init
    | 0x02 => Action
    | 0x03 => Callback
    | 0x10 => ZkSecretInput
    | 0x11 => ZkVariableInputted
    | 0x12 => ZkVariableRejected
    | 0x13 => ComputeComplete 
    | 0x14 => ZkVariableOpened
    | 0x15 => ZkUserVariableOpened
    | 0x16 => ZkAttestationComplete
    | 0x17 => ZkSecretInputWithExplicitType
    | 0x18 => ExternalEvent

````



## Type Specification

````
<Type> := 
    | BaseType
    | ComposedType
    | NamedTypeReference

<NamedTypeReference> :=
    0x00 Index: 0xnn => NamedTypes(Index)

<BaseType> :=
    | 0x01 => u8
    | 0x02 => u16
    | 0x03 => u32
    | 0x04 => u64
    | 0x05 => u128
    | 0x18 => u256
    | 0x06 => i8
    | 0x07 => i16
    | 0x08 => i32
    | 0x09 => i64 
    | 0x0a => i128
    | 0x0b => String
    | 0x0c => bool
    | 0x0d => Address
    | 0x13 => Hash
    | 0x14 => PublicKey
    | 0x15 => Signature
    | 0x16 => BlsPublicKey
    | 0x17 => BlsSignature

<ComposedType> :=   
    | 0x0e T:Type           => Vec<T>
    | 0x0f K:Type V:Type    => Map<K, V> 
    | 0x10 T:Type           => Set<T> 
    | 0x11 L:0xnn           => [u8; L] 				{ 0x00 <= L <= 0x7f } 
    | 0x1a T:Type L:0xnn    => [T; L] 				{ 0x00 <= L <= 0x7f } 
    | 0x12 T:Type           => Option<T> 
    | 0x19 K:Type V:Type    => AvlTree<K, V> 
````
