package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.HexFormat;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/** Testing writing and reading bytes using {@link AbiByteOutput} and {@link AbiByteInput}. */
public final class AbiByteStreamTest {

  /** Can write and read bytes for all numbers. */
  @Test
  void numbers() {
    byte[] bytes =
        AbiByteOutput.serializeBigEndian(
            out -> {
              out.writeU8(1);
              out.writeU8((byte) 1);
              out.writeU16(2);
              out.writeU16((short) 2);
              out.writeU32(3);
              out.writeU64(4);
              out.writeI8(5);
              out.writeI8((byte) 5);
              out.writeI16(6);
              out.writeI16((short) 6);
              out.writeI32(7);
              out.writeI64(8);
              out.writeUnsignedBigInteger(BigInteger.valueOf(9), 16);
              out.writeSignedBigInteger(BigInteger.valueOf(10), 16);
            });
    AbiByteInput input = AbiByteInput.createBigEndian(bytes);
    assertThat(input.readU8()).isEqualTo((byte) 1);
    assertThat(input.readU8()).isEqualTo((byte) 1);
    assertThat(input.readU16()).isEqualTo((short) 2);
    assertThat(input.readU16()).isEqualTo((short) 2);
    assertThat(input.readU32()).isEqualTo(3);
    assertThat(input.readU64()).isEqualTo(4);
    assertThat(input.readI8()).isEqualTo((byte) 5);
    assertThat(input.readI8()).isEqualTo((byte) 5);
    assertThat(input.readI16()).isEqualTo((short) 6);
    assertThat(input.readI16()).isEqualTo((short) 6);
    assertThat(input.readI32()).isEqualTo(7);
    assertThat(input.readI64()).isEqualTo(8);
    assertThat(input.readUnsignedBigInteger(16)).isEqualTo(BigInteger.valueOf(9));
    assertThat(input.readSignedBigInteger(16)).isEqualTo(BigInteger.valueOf(10));
  }

  /** Can write and read bytes for numbers in both little and big endian. */
  @Test
  void endianess() {
    byte[] leByte = AbiByteOutput.serializeLittleEndian(out -> out.writeI32(1));
    assertThat(HexFormat.of().formatHex(leByte)).isEqualTo("01000000");
    byte[] beByte = AbiByteOutput.serializeBigEndian(out -> out.writeI32(1));
    assertThat(HexFormat.of().formatHex(beByte)).isEqualTo("00000001");

    assertThat(AbiByteInput.createLittleEndian(leByte).readI32()).isEqualTo(1);
    assertThat(AbiByteInput.createBigEndian(leByte).readI32()).isEqualTo(0x01000000);
  }

  /** Can write and read bytes for strings. */
  @Test
  void string() {
    String string = "Hello";
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeString(string));
    assertThat(AbiByteInput.createBigEndian(bytes).readString()).isEqualTo(string);
  }

  /** Can write and read bytes for booleans. */
  @Test
  void booleans() {
    boolean bool = true;
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeBoolean(bool));
    assertThat(AbiByteInput.createBigEndian(bytes).readBoolean()).isEqualTo(bool);

    boolean bool2 = false;
    byte[] bytes2 = AbiByteOutput.serializeBigEndian(out -> out.writeBoolean(bool2));
    assertThat(AbiByteInput.createBigEndian(bytes2).readBoolean()).isEqualTo(bool2);
  }

  /** Can write and read byte arrays. */
  @Test
  void bytes() {
    byte[] bytes1 = new byte[] {1, 2, 3, 4, 5};
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeBytes(bytes1));
    assertThat(AbiByteInput.createBigEndian(bytes).readBytes(5)).isEqualTo(bytes1);
  }

  /** Can write and read bytes for blockchain addresses. */
  @Test
  void address() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeAddress(address));
    assertThat(address.writeAsString()).isEqualTo(HexFormat.of().formatHex(bytes));
    assertThat(AbiByteInput.createBigEndian(bytes).readAddress()).isEqualTo(address);
  }

  /** Can write and read bytes for blockchain public keys. */
  @Test
  void publicKey() {
    byte[] keyBytes = Base64.decode("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
    BlockchainPublicKey publicKey =
        SafeDataInputStream.readFully(keyBytes, BlockchainPublicKey::read);
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writePublicKey(publicKey));
    assertThat(publicKey.toString()).isEqualTo("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
    assertThat(AbiByteInput.createBigEndian(bytes).readPublicKey()).isEqualTo(publicKey);
  }

  /** Can write and read bytes for signatures. */
  @Test
  void signature() {
    Signature signature =
        Signature.fromString(
            "01000000000000000000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000000000000000000000");
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeSignature(signature));
    assertThat(signature.writeAsString()).isEqualTo(HexFormat.of().formatHex(bytes));
    assertThat(AbiByteInput.createBigEndian(bytes).readSignature()).isEqualTo(signature);
  }

  /** Can write and read bytes for hashes. */
  @Test
  void hash() {
    Hash hash = Hash.fromString("0100000000000000000000000000000000000000000000000000000000000000");
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeHash(hash));
    assertThat(hash.getBytes()).isEqualTo(bytes);
    assertThat(AbiByteInput.createBigEndian(bytes).readHash()).isEqualTo(hash);
  }

  /** Can write and read bytes for bls signatures. */
  @Test
  void blsSignature() {
    BlsSignature blsSignature =
        new BlsKeyPair(BigInteger.valueOf(42)).sign(Hash.create(out -> out.writeString("Hi")));
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeBlsSignature(blsSignature));
    assertThat(SafeDataOutputStream.serialize(blsSignature::write)).isEqualTo(bytes);
    assertThat(AbiByteInput.createBigEndian(bytes).readBlsSignature()).isEqualTo(blsSignature);
  }

  /** Can write and read bytes for bls public keys. */
  @Test
  void blsPublicKey() {
    BlsPublicKey blsPublicKey = new BlsKeyPair(BigInteger.valueOf(42)).getPublicKey();
    byte[] bytes = AbiByteOutput.serializeBigEndian(out -> out.writeBlsPublicKey(blsPublicKey));
    assertThat(SafeDataOutputStream.serialize(blsPublicKey::write)).isEqualTo(bytes);
    assertThat(AbiByteInput.createBigEndian(bytes).readBlsPublicKey()).isEqualTo(blsPublicKey);
  }

  /** Can read leb encoded shortnames. */
  @Test
  void shortname() {
    byte[] initial = new byte[] {1, -1, -1};
    String shortname = AbiByteInput.createBigEndian(initial).readShortnameString();
    assertThat(shortname).isEqualTo("01");

    initial = new byte[] {-1, -1, -1, -1, 0};
    shortname = AbiByteInput.createBigEndian(initial).readShortnameString();
    assertThat(shortname).isEqualTo("ffffffff00");

    initial = new byte[] {-1, -1, -1, -1, -1, -1};
    byte[] finalInitial = initial;
    assertThatThrownBy(() -> AbiByteInput.createBigEndian(finalInitial).readShortnameString())
        .hasMessage(
            "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5"
                + " bytes)");
  }

  /** Can read the remaining of the stream as bytes. */
  @Test
  void readRemaining() {
    AbiByteInput input = AbiByteInput.createBigEndian(new byte[] {1, 2, 3, 4, 5});
    input.readBoolean();
    assertThat(input.readRemaining()).isEqualTo(new byte[] {2, 3, 4, 5});
  }
}
