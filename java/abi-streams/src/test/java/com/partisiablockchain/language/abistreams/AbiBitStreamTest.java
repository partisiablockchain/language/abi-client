package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.CompactBitArray;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.HexFormat;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/** Testing writing and reading bits using {@link AbiBitOutput} and {@link AbiBitInput}. */
public final class AbiBitStreamTest {

  /** Can write and read bits for all numbers. */
  @Test
  void numbers() {
    CompactBitArray bits =
        AbiBitOutput.serialize(
            out -> {
              out.writeI8(1);
              out.writeI8((byte) 1);
              out.writeI16(2);
              out.writeI16((short) 2);
              out.writeI32(3);
              out.writeI64(4);
              out.writeU8(5);
              out.writeU8((byte) 5);
              out.writeU16(6);
              out.writeU16((short) 6);
              out.writeU32(7);
              out.writeU64(8);
              out.writeSignedBigInteger(BigInteger.valueOf(9), 16);
              out.writeUnsignedBigInteger(BigInteger.valueOf(10), 16);
            });
    AbiBitInput input = AbiBitInput.create(bits);
    assertThat(input.readI8()).isEqualTo((byte) 1);
    assertThat(input.readI8()).isEqualTo((byte) 1);
    assertThat(input.readI16()).isEqualTo((short) 2);
    assertThat(input.readI16()).isEqualTo((short) 2);
    assertThat(input.readI32()).isEqualTo(3);
    assertThat(input.readI64()).isEqualTo(4);
    assertThat(input.readU8()).isEqualTo((byte) 5);
    assertThat(input.readU8()).isEqualTo((byte) 5);
    assertThat(input.readU16()).isEqualTo((short) 6);
    assertThat(input.readU16()).isEqualTo((short) 6);
    assertThat(input.readU32()).isEqualTo(7);
    assertThat(input.readU64()).isEqualTo(8);
    assertThat(input.readSignedBigInteger(16)).isEqualTo(BigInteger.valueOf(9));
    assertThat(input.readUnsignedBigInteger(16)).isEqualTo(BigInteger.valueOf(10));
  }

  /** Can't write bits for strings. */
  @Test
  void cantWriteString() {
    String string = "Hello";
    assertThatThrownBy(() -> AbiBitOutput.serialize(out -> out.writeString(string)))
        .hasMessage("Unsupported write string for a bit output");
  }

  /** Can't read bits for strings. */
  @Test
  void cantReadString() {
    assertThatThrownBy(() -> AbiBitInput.create(new CompactBitArray(new byte[0], 0)).readString())
        .hasMessage("Unsupported read string for a bit input");
  }

  /** Can write and read bits for booleans. Booleans are packed when writing bits. */
  @Test
  void booleans() {
    boolean bool = true;
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeBoolean(bool));
    assertThat(AbiBitInput.create(bits).readBoolean()).isEqualTo(bool);

    boolean bool2 = false;
    CompactBitArray bits2 = AbiBitOutput.serialize(out -> out.writeBoolean(bool2));
    assertThat(AbiBitInput.create(bits2).readBoolean()).isEqualTo(bool2);
  }

  /** Can write and read byte arrays. */
  @Test
  void bits() {
    byte[] bits1 = new byte[] {1, 2, 3, 4, 5};
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeBytes(bits1));
    assertThat(AbiBitInput.create(bits).readBytes(5)).isEqualTo(bits1);
  }

  /** Can write and read bits for blockchain addresses. */
  @Test
  void address() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeAddress(address));
    assertThat(address.writeAsString()).isEqualTo(HexFormat.of().formatHex(bits.data()));
    assertThat(AbiBitInput.create(bits).readAddress()).isEqualTo(address);
  }

  /** Can write and read bits for blockchain public keys. */
  @Test
  void publicKey() {
    byte[] keyBytes = Base64.decode("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
    BlockchainPublicKey publicKey =
        SafeDataInputStream.readFully(keyBytes, BlockchainPublicKey::read);
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writePublicKey(publicKey));
    assertThat(publicKey.toString()).isEqualTo("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
    assertThat(AbiBitInput.create(bits).readPublicKey()).isEqualTo(publicKey);
  }

  /** Can write and read bits for signatures. */
  @Test
  void signature() {
    Signature signature =
        Signature.fromString(
            "01000000000000000000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000000000000000000000");
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeSignature(signature));
    assertThat(signature.writeAsString()).isEqualTo(HexFormat.of().formatHex(bits.data()));
    assertThat(AbiBitInput.create(bits).readSignature()).isEqualTo(signature);
  }

  /** Can write and read bits for hashes. */
  @Test
  void hash() {
    Hash hash = Hash.fromString("0100000000000000000000000000000000000000000000000000000000000000");
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeHash(hash));
    assertThat(hash.getBytes()).isEqualTo(bits.data());
    assertThat(AbiBitInput.create(bits).readHash()).isEqualTo(hash);
  }

  /** Can write and read bits for bls signatures. */
  @Test
  void blsSignature() {
    BlsSignature blsSignature =
        new BlsKeyPair(BigInteger.valueOf(42)).sign(Hash.create(out -> out.writeString("Hi")));
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeBlsSignature(blsSignature));
    assertThat(SafeDataOutputStream.serialize(blsSignature::write)).isEqualTo(bits.data());
    assertThat(AbiBitInput.create(bits).readBlsSignature()).isEqualTo(blsSignature);
  }

  /** Can write and read bits for bls public keys. */
  @Test
  void blsPublicKey() {
    BlsPublicKey blsPublicKey = new BlsKeyPair(BigInteger.valueOf(42)).getPublicKey();
    CompactBitArray bits = AbiBitOutput.serialize(out -> out.writeBlsPublicKey(blsPublicKey));
    assertThat(SafeDataOutputStream.serialize(blsPublicKey::write)).isEqualTo(bits.data());
    assertThat(AbiBitInput.create(bits).readBlsPublicKey()).isEqualTo(blsPublicKey);
  }

  /** Reading the remaining bits as bytes is not possible. */
  @Test
  void readRemainingUnsupported() {
    var input = AbiBitInput.create(new CompactBitArray(new byte[] {}, 0));

    assertThatThrownBy(input::readRemaining)
        .hasMessage("Cannot read remaining bytes of input when reading bits.");
  }
}
