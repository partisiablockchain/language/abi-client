package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;

/** Common interface for writing ABI types. */
public interface AbiOutput {

  /**
   * Writes a byte array.
   *
   * @param values the byte array to write to the stream
   */
  void writeBytes(byte[] values);

  /**
   * Writes a boolean.
   *
   * @param value the boolean to write to the stream
   */
  void writeBoolean(boolean value);

  /**
   * Writes a signed byte.
   *
   * @param value the byte to write to the stream
   */
  void writeI8(byte value);

  /**
   * Writes a signed byte from an int.
   *
   * @param value the int to write as a byte to the stream. Must be between -128 and 127
   */
  void writeI8(int value);

  /**
   * Writes a signed short.
   *
   * @param value the short to write to the stream
   */
  void writeI16(short value);

  /**
   * Writes a signed short from an int.
   *
   * @param value the int to write as a short to the stream. Must be between -32768 and 32767
   */
  void writeI16(int value);

  /**
   * Writes a signed int.
   *
   * @param value the int to write to the stream
   */
  void writeI32(int value);

  /**
   * Writes a signed long.
   *
   * @param value the long to write to the stream
   */
  void writeI64(long value);

  /**
   * Writes a signed BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  void writeSignedBigInteger(BigInteger value, int noBytes);

  /**
   * Writes an unsigned byte.
   *
   * @param value the byte to write to the stream
   */
  void writeU8(byte value);

  /**
   * Writes an unsigned byte from an int.
   *
   * @param value the int to write as a byte to the stream. Must be between 0 and 255
   */
  void writeU8(int value);

  /**
   * Writes an unsigned short.
   *
   * @param value the short to write to the stream
   */
  void writeU16(short value);

  /**
   * Writes an unsigned short from an int.
   *
   * @param value the int to write as a short to the stream. Must be between 0 and 65535
   */
  void writeU16(int value);

  /**
   * Writes an unsigned int.
   *
   * @param value the int to write to the stream
   */
  void writeU32(int value);

  /**
   * Writes an unsigned long.
   *
   * @param value the long to write to the stream
   */
  void writeU64(long value);

  /**
   * Writes an unsigned BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  void writeUnsignedBigInteger(BigInteger value, int noBytes);

  /**
   * Writes a string.
   *
   * @param value the string to write to the stream
   */
  void writeString(String value);

  /**
   * Writes a blockchain address.
   *
   * @param address the address to write to the stream
   */
  default void writeAddress(BlockchainAddress address) {
    writeBytes(SafeDataOutputStream.serialize(address::write));
  }

  /**
   * Writes a signature.
   *
   * @param signature the signature to write to the stream
   */
  default void writeSignature(Signature signature) {
    writeBytes(SafeDataOutputStream.serialize(signature::write));
  }

  /**
   * Writes a hash.
   *
   * @param hash the hash to write to the stream
   */
  default void writeHash(Hash hash) {
    writeBytes(SafeDataOutputStream.serialize(hash::write));
  }

  /**
   * Writes a public key.
   *
   * @param publicKey the public key to write to the stream
   */
  default void writePublicKey(BlockchainPublicKey publicKey) {
    writeBytes(SafeDataOutputStream.serialize(publicKey::write));
  }

  /**
   * Writes a bls public key.
   *
   * @param blsPublicKey the bls public key to write to the stream
   */
  default void writeBlsPublicKey(BlsPublicKey blsPublicKey) {
    writeBytes(SafeDataOutputStream.serialize(blsPublicKey::write));
  }

  /**
   * Writes a bls signature.
   *
   * @param blsSignature the bls signature to write to the stream
   */
  default void writeBlsSignature(BlsSignature blsSignature) {
    writeBytes(SafeDataOutputStream.serialize(blsSignature::write));
  }
}
