package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.BigEndianByteInput;
import com.secata.stream.ByteInput;
import com.secata.stream.LittleEndianByteInput;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HexFormat;

/** Byte input for reading abi types. */
public final class AbiByteInput implements AbiInput {
  private final ByteInput input;

  /**
   * Constructor.
   *
   * @param input underlying ByteInput
   */
  public AbiByteInput(ByteInput input) {
    this.input = input;
  }

  /**
   * Create a little-endian input stream from bytes.
   *
   * @param bytes input bytes
   * @return input stream
   */
  public static AbiByteInput createLittleEndian(byte[] bytes) {
    return new AbiByteInput(new LittleEndianByteInput(new ByteArrayInputStream(bytes)));
  }

  /**
   * Create a big-endian input stream from bytes.
   *
   * @param bytes input bytes
   * @return input stream
   */
  public static AbiByteInput createBigEndian(byte[] bytes) {
    return new AbiByteInput(new BigEndianByteInput(new ByteArrayInputStream(bytes)));
  }

  @Override
  public byte[] readBytes(int noBytes) {
    return input.readBytes(noBytes);
  }

  @Override
  public boolean readBoolean() {
    return input.readBoolean();
  }

  @Override
  public byte readI8() {
    return input.readI8();
  }

  @Override
  public short readI16() {
    return input.readI16();
  }

  @Override
  public int readI32() {
    return input.readI32();
  }

  @Override
  public long readI64() {
    return input.readI64();
  }

  @Override
  public BigInteger readSignedBigInteger(int noBytes) {
    return input.readSignedBigInteger(noBytes);
  }

  @Override
  public byte readU8() {
    return input.readU8();
  }

  @Override
  public short readU16() {
    return input.readU16();
  }

  @Override
  public int readU32() {
    return input.readU32();
  }

  @Override
  public long readU64() {
    return input.readU64();
  }

  @Override
  public BigInteger readUnsignedBigInteger(int noBytes) {
    return input.readUnsignedBigInteger(noBytes);
  }

  @Override
  public String readString() {
    return input.readString();
  }

  /**
   * Reads a shortname as a leb128 encoding.
   *
   * @return the read shortname in a hex encoding
   */
  public String readShortnameString() {
    return HexFormat.of().formatHex(readShortname());
  }

  /**
   * Reads a shortname as a leb128 encoding.
   *
   * @return the read shortname as a byte array
   */
  public byte[] readShortname() {
    byte[] buffer = new byte[5];
    for (int i = 0; i < buffer.length; i++) {
      var currentByte = input.readU8();
      buffer[i] = currentByte;

      if ((currentByte & 0x80) == 0) {
        return Arrays.copyOf(buffer, i + 1);
      }
    }

    throw new RuntimeException(
        "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5"
            + " bytes)");
  }

  @Override
  public byte[] readRemaining() {
    return input.readRemaining();
  }
}
