package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import java.math.BigInteger;

/** Common interface for reading ABI types. */
public interface AbiInput {
  /**
   * Reads an array of bytes.
   *
   * @param noBytes the number of bytes
   * @return the read bytes
   */
  byte[] readBytes(int noBytes);

  /**
   * Reads a single byte from the stream as a boolean.
   *
   * @return false if the read byte is 0 and true otherwise
   */
  boolean readBoolean();

  /**
   * Reads a signed 8-bit number from the stream.
   *
   * @return the read number as a java byte
   */
  byte readI8();

  /**
   * Reads a signed 16-bit number from the stream.
   *
   * @return the read number as a java short
   */
  short readI16();

  /**
   * Reads a signed 32-bit number from the stream.
   *
   * @return the read number as a java int
   */
  int readI32();

  /**
   * Reads a signed 64-bit number from the stream.
   *
   * @return the read number as a java long
   */
  long readI64();

  /**
   * Reads a signed BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  BigInteger readSignedBigInteger(int noBytes);

  /**
   * Reads an unsigned 8-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java byte
   */
  byte readU8();

  /**
   * Reads an unsigned 16-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java short
   */
  short readU16();

  /**
   * Reads an unsigned 32-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java int
   */
  int readU32();

  /**
   * Reads an unsigned 64-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java long
   */
  long readU64();

  /**
   * Reads an unsigned BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  BigInteger readUnsignedBigInteger(int noBytes);

  /**
   * Reads a string.
   *
   * @return the read string
   */
  String readString();

  /**
   * Reads 21 bytes from the stream as a Blockchain address.
   *
   * @return the read blockchain address
   */
  default BlockchainAddress readAddress() {
    return SafeDataInputStream.readFully(readBytes(21), BlockchainAddress::read);
  }

  /**
   * Reads 65 bytes from the stream as a Signature.
   *
   * @return the read signature
   */
  default Signature readSignature() {
    return SafeDataInputStream.readFully(readBytes(65), Signature::read);
  }

  /**
   * Reads 32 bytes from the stream as a hash.
   *
   * @return the read hash
   */
  default Hash readHash() {
    return SafeDataInputStream.readFully(readBytes(32), Hash::read);
  }

  /**
   * Reads 33 bytes from the stream as a public key.
   *
   * @return the read public key
   */
  default BlockchainPublicKey readPublicKey() {
    return SafeDataInputStream.readFully(readBytes(33), BlockchainPublicKey::read);
  }

  /**
   * Reads 96 bytes from the stream as a bls public key.
   *
   * @return the read bls public key
   */
  default BlsPublicKey readBlsPublicKey() {
    return SafeDataInputStream.readFully(readBytes(96), BlsPublicKey::read);
  }

  /**
   * Reads 48 bytes from the stream as a bls signature.
   *
   * @return the read bls signature
   */
  default BlsSignature readBlsSignature() {
    return SafeDataInputStream.readFully(readBytes(48), BlsSignature::read);
  }

  /**
   * Read the remaining input into a byte array.
   *
   * @return the remaining data in a byte array.
   */
  byte[] readRemaining();
}
