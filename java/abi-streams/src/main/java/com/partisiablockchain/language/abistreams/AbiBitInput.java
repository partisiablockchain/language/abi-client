package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.BitInput;
import com.secata.stream.CompactBitArray;
import java.math.BigInteger;

/** BitInput for reading ABI types. */
public final class AbiBitInput implements AbiInput {
  private final BitInput input;

  /**
   * Constructor.
   *
   * @param input underlying BitInput
   */
  public AbiBitInput(BitInput input) {
    this.input = input;
  }

  /**
   * Create a bit input stream from bits.
   *
   * @param bits input bits
   * @return input stream
   */
  public static AbiBitInput create(CompactBitArray bits) {
    return new AbiBitInput(BitInput.create(bits.data()));
  }

  @Override
  public byte[] readBytes(int noBytes) {
    return input.readBytes(noBytes);
  }

  @Override
  public boolean readBoolean() {
    return input.readBoolean();
  }

  @Override
  public byte readI8() {
    return (byte) input.readSignedInt(8);
  }

  @Override
  public short readI16() {
    return (short) input.readSignedInt(16);
  }

  @Override
  public int readI32() {
    return input.readSignedInt(32);
  }

  @Override
  public long readI64() {
    return input.readSignedLong(64);
  }

  @Override
  public BigInteger readSignedBigInteger(int noBytes) {
    return input.readSignedBigInteger(noBytes * 8);
  }

  @Override
  public byte readU8() {
    return (byte) input.readUnsignedInt(8);
  }

  @Override
  public short readU16() {
    return (short) input.readUnsignedInt(16);
  }

  @Override
  public int readU32() {
    return input.readUnsignedInt(32);
  }

  @Override
  public long readU64() {
    return input.readUnsignedLong(64);
  }

  @Override
  public BigInteger readUnsignedBigInteger(int noBytes) {
    return input.readUnsignedBigInteger(noBytes * 8);
  }

  @Override
  public String readString() {
    throw new RuntimeException("Unsupported read string for a bit input");
  }

  @Override
  public byte[] readRemaining() {
    throw new UnsupportedOperationException(
        "Cannot read remaining bytes of input when reading bits.");
  }
}
