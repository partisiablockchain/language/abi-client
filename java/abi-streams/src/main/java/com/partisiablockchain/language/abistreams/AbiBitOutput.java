package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import java.math.BigInteger;
import java.util.function.Consumer;

/** BitOutput for writing ABI types. */
public final class AbiBitOutput implements AbiOutput {

  private final BitOutput out;

  /**
   * Creates an AbiBitOutput, runs the serialization and returns the bits as a compact bit array.
   *
   * @param serializer the serialization to happen
   * @return the serialized bits
   */
  public static CompactBitArray serialize(Consumer<AbiBitOutput> serializer) {
    return BitOutput.serializeBits(
        out -> {
          AbiBitOutput abiBitOutput = new AbiBitOutput(out);
          serializer.accept(abiBitOutput);
        });
  }

  /**
   * Constructor.
   *
   * @param out underlying output stream
   */
  public AbiBitOutput(BitOutput out) {
    this.out = out;
  }

  @Override
  public void writeBytes(byte[] values) {
    out.writeBytes(values);
  }

  @Override
  public void writeBoolean(boolean value) {
    out.writeBoolean(value);
  }

  @Override
  public void writeI8(byte value) {
    out.writeSignedInt(value, 8);
  }

  @Override
  public void writeI8(int value) {
    out.writeSignedInt(value, 8);
  }

  @Override
  public void writeI16(short value) {
    out.writeSignedInt(value, 16);
  }

  @Override
  public void writeI16(int value) {
    out.writeSignedInt(value, 16);
  }

  @Override
  public void writeI32(int value) {
    out.writeSignedInt(value, 32);
  }

  @Override
  public void writeI64(long value) {
    out.writeSignedLong(value, 64);
  }

  @Override
  public void writeSignedBigInteger(BigInteger value, int noBytes) {
    out.writeSignedBigInteger(value, noBytes * 8);
  }

  @Override
  public void writeU8(byte value) {
    out.writeUnsignedInt(value, 8);
  }

  @Override
  public void writeU8(int value) {
    out.writeUnsignedInt(value, 8);
  }

  @Override
  public void writeU16(short value) {
    out.writeUnsignedInt(value, 16);
  }

  @Override
  public void writeU16(int value) {
    out.writeUnsignedInt(value, 16);
  }

  @Override
  public void writeU32(int value) {
    out.writeUnsignedInt(value, 32);
  }

  @Override
  public void writeU64(long value) {
    out.writeUnsignedLong(value, 64);
  }

  @Override
  public void writeUnsignedBigInteger(BigInteger value, int noBytes) {
    out.writeUnsignedBigInteger(value, noBytes * 8);
  }

  @Override
  public void writeString(String value) {
    throw new RuntimeException("Unsupported write string for a bit output");
  }
}
