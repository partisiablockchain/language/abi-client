package com.partisiablockchain.language.abistreams;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.BigEndianByteOutput;
import com.secata.stream.ByteOutput;
import com.secata.stream.LittleEndianByteOutput;
import java.math.BigInteger;
import java.util.function.Consumer;

/** ByteOutput for writing abi types. */
public final class AbiByteOutput implements AbiOutput {

  private final ByteOutput out;

  /**
   * Constructor.
   *
   * @param out underlying output stream
   */
  public AbiByteOutput(ByteOutput out) {
    this.out = out;
  }

  /**
   * Creates an AbiByteOutput, runs the serialization and returns the bytes as little-endian.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static byte[] serializeLittleEndian(Consumer<AbiByteOutput> serializer) {
    return LittleEndianByteOutput.serialize(
        out -> {
          AbiByteOutput abiOut = new AbiByteOutput(out);
          serializer.accept(abiOut);
        });
  }

  /**
   * Creates an AbiByteOutput, runs the serialization and returns the bytes as big-endian.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static byte[] serializeBigEndian(Consumer<AbiByteOutput> serializer) {
    return BigEndianByteOutput.serialize(
        out -> {
          AbiByteOutput abiOut = new AbiByteOutput(out);
          serializer.accept(abiOut);
        });
  }

  @Override
  public void writeBytes(byte[] values) {
    out.writeBytes(values);
  }

  @Override
  public void writeBoolean(boolean value) {
    out.writeBoolean(value);
  }

  @Override
  public void writeI8(byte value) {
    out.writeI8(value);
  }

  @Override
  public void writeI8(int value) {
    out.writeI8(value);
  }

  @Override
  public void writeI16(short value) {
    out.writeI16(value);
  }

  @Override
  public void writeI16(int value) {
    out.writeI16(value);
  }

  @Override
  public void writeI32(int value) {
    out.writeI32(value);
  }

  @Override
  public void writeI64(long value) {
    out.writeI64(value);
  }

  @Override
  public void writeSignedBigInteger(BigInteger value, int noBytes) {
    out.writeSignedBigInteger(value, noBytes);
  }

  @Override
  public void writeU8(byte value) {
    out.writeU8(value);
  }

  @Override
  public void writeU8(int value) {
    out.writeU8(value);
  }

  @Override
  public void writeU16(short value) {
    out.writeU16(value);
  }

  @Override
  public void writeU16(int value) {
    out.writeU16(value);
  }

  @Override
  public void writeU32(int value) {
    out.writeU32(value);
  }

  @Override
  public void writeU64(long value) {
    out.writeU64(value);
  }

  @Override
  public void writeUnsignedBigInteger(BigInteger value, int noBytes) {
    out.writeUnsignedBigInteger(value, noBytes);
  }

  @Override
  public void writeString(String value) {
    out.writeString(value);
  }
}
