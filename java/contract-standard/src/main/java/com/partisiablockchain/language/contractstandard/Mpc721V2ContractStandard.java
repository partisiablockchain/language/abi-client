package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.contractstandard.typematcher.StructEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeEqualityMatcher;
import java.util.List;

/**
 * MPC-721-v2 contract standard definition.
 *
 * @see <a
 *     href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/mpc-721-nft-contract.html#version-2">
 *     MPC-721-v2 Specification</a>
 */
@Immutable
public final class Mpc721V2ContractStandard implements ContractStandard<Mpc721V2ContractState> {

  private final boolean uriExtension;

  static final List<FieldDefinition> FIELDS =
      List.of(
          new RecursiveFieldDefinition("name", TypeSpecs.STRING),
          new RecursiveFieldDefinition("symbol", TypeSpecs.STRING),
          new RecursiveFieldDefinition(
              "owners", new AvlTreeMapTypeSpec(TypeSpecs.U128, TypeSpecs.ADDRESS)),
          new RecursiveFieldDefinition(
              "token_approvals", new AvlTreeMapTypeSpec(TypeSpecs.U128, TypeSpecs.ADDRESS)),
          new RecursiveFieldDefinition(
              "operator_approvals",
              new TypeEqualityMatcher(
                  new AvlTreeMapTypeSpec(new NamedTypeRef(0), new NamedTypeRef(1))),
              List.of(
                  StructEqualityMatcher.fromNamedTypeSpec(
                      new StructTypeSpec(
                          "OperatorApproval",
                          List.of(
                              new FieldAbi("owner", TypeSpecs.ADDRESS),
                              new FieldAbi("operator", TypeSpecs.ADDRESS))),
                      false),
                  StructEqualityMatcher.fromNamedTypeSpec(
                      new StructTypeSpec("Unit", List.of()), false))));

  static final List<FieldDefinition> URI_FIELDS =
      List.of(
          new RecursiveFieldDefinition("uri_template", TypeSpecs.STRING),
          new RecursiveFieldDefinition(
              "token_uri_details", new AvlTreeMapTypeSpec(TypeSpecs.U128, TypeSpecs.STRING)));

  static final List<ActionDefinition> ACTIONS = Mpc721ContractStandard.ACTIONS;

  /**
   * Constructor.
   *
   * @param uriExtension true if the standard checks for uri extension
   */
  public Mpc721V2ContractStandard(boolean uriExtension) {
    this.uriExtension = uriExtension;
  }

  @Override
  public boolean check(ContractAbi contractAbi) {
    boolean checkActions =
        ACTIONS.stream().allMatch(actionDefinition -> actionDefinition.check(contractAbi));
    boolean checkFields = FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));

    if (uriExtension) {
      boolean checkUri =
          URI_FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));
      return checkFields && checkUri && checkActions;
    } else {
      return checkFields && checkActions;
    }
  }

  @Override
  public Mpc721V2ContractState getStandardState(byte[] state, ContractAbi contractAbi) {
    if (!check(contractAbi)) {
      throw new RuntimeException(
          "Contract abi does not follow the MPC721-V2%s standard"
              .formatted(uriExtension ? "-URI" : ""));
    }
    try {
      ScValueStruct stateStruct = StateReader.create(state, contractAbi).readState();

      return new Mpc721V2ContractState(
          FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue(),
          FIELDS.get(1).getFieldValue(contractAbi, stateStruct).stringValue(),
          FIELDS.get(2).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId(),
          FIELDS.get(3).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId(),
          FIELDS.get(4).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId(),
          uriExtension
              ? URI_FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue()
              : null,
          uriExtension
              ? URI_FIELDS.get(1).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId()
              : null);

    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize state.", e);
    }
  }
}
