package com.partisiablockchain.language.contractstandard.typematcher;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpecWithGenerics;
import java.util.List;

/** A type matcher which checks for type equality. */
public final class TypeEqualityMatcher implements TypeMatcher {

  private final TypeSpec expectedType;

  /**
   * Constructor.
   *
   * @param expectedType the expected type
   */
  public TypeEqualityMatcher(TypeSpec expectedType) {
    this.expectedType = expectedType;
  }

  @Override
  public boolean matches(
      TypeSpec type, List<NamedTypeSpec> namedTypes, List<NamedTypeMatcher> expectedNamedTypes) {
    if (type.typeIndex() != expectedType.typeIndex()) {
      return false;
    }

    if (type instanceof NamedTypeRef namedTypeRef) {
      NamedTypeRef expectedNamedRef = (NamedTypeRef) expectedType;
      NamedTypeMatcher expectedNamedTypeMatcher = expectedNamedTypes.get(expectedNamedRef.index());
      NamedTypeSpec structToCheck = namedTypes.get(namedTypeRef.index());
      return expectedNamedTypeMatcher.matches(structToCheck, namedTypes, expectedNamedTypes);
    } else if (type instanceof TypeSpecWithGenerics withGenerics) {
      TypeSpecWithGenerics expectedWithGenerics = (TypeSpecWithGenerics) expectedType;
      for (int i = 0; i < withGenerics.getGenerics().size(); i++) {
        if (!new TypeEqualityMatcher(expectedWithGenerics.getGenerics().get(i))
            .matches(withGenerics.getGenerics().get(i), namedTypes, expectedNamedTypes)) {
          return false;
        }
      }
      return true;
    } else {
      return type.equals(expectedType);
    }
  }
}
