package com.partisiablockchain.language.contractstandard.typematcher;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;

/** Check if a type matches some criteria. */
public interface TypeMatcher {

  /**
   * Check if a type matches some criteria.
   *
   * @param type the type to check
   * @param namedTypes the named types of the type to check
   * @param expectedNamedTypes the expected named type matchers
   * @return true if the type matches the criteria
   */
  boolean matches(
      TypeSpec type, List<NamedTypeSpec> namedTypes, List<NamedTypeMatcher> expectedNamedTypes);
}
