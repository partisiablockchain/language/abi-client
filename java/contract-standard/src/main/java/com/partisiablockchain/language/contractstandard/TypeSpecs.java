package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;

/** Type specifications for use in contract standards tests. */
final class TypeSpecs {

  static final SimpleTypeSpec STRING = new SimpleTypeSpec(TypeSpec.TypeIndex.String);
  static final SimpleTypeSpec ADDRESS = new SimpleTypeSpec(TypeSpec.TypeIndex.Address);
  static final SimpleTypeSpec U128 = new SimpleTypeSpec(TypeSpec.TypeIndex.u128);
  static final SimpleTypeSpec U8 = new SimpleTypeSpec(TypeSpec.TypeIndex.u8);
  static final SimpleTypeSpec BOOL = new SimpleTypeSpec(TypeSpec.TypeIndex.bool);

  private TypeSpecs() {}
}
