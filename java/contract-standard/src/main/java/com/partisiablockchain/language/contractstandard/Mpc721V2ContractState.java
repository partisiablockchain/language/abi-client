package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Record holding the field values for an MPC-721-v2 standard contract state.
 *
 * <p>An MPC-721-v2 nft contract is a standardized digital asset inspired by the guidelines outlined
 * in the EIP721. Allows for the implementation of NFTs within smart contracts.
 *
 * @param name the name of the nft.
 * @param symbol the symbol of the nft.
 * @param ownersTreeId tree id of the owners map of all minted nfts
 * @param tokenApprovalsTreeId tree id of the approvals map given for others to transfer a given nft
 * @param operatorApprovalsTreeId tree id of the approvals map given for an operator to handle all
 *     of an owners nfts
 * @param uriTemplate template for a URI. Replaces '{}' with the token uri details of a given nft
 * @param tokenUriDetailsTreeId tree id of the token uri details map of each nft
 */
public record Mpc721V2ContractState(
    String name,
    String symbol,
    int ownersTreeId,
    int tokenApprovalsTreeId,
    int operatorApprovalsTreeId,
    String uriTemplate,
    Integer tokenUriDetailsTreeId)
    implements StandardState {}
