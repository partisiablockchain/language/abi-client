package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ContractAbi;

/**
 * Contract Standard Interface for smart contracts.
 *
 * <p>Contract standards outline the required actions, state fields, and interfaces that the
 * contract should implement. Satisfying the standard enables integration and compatibility with
 * existing decentralized applications (DApps). All standards can be found <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/introduction-to-standard-contract-interfaces.html">here</a>.
 */
public interface ContractStandard<T extends StandardState> {

  /**
   * Tests if a given ABI satisfies the contract standard.
   *
   * @param contractAbi the ABI to test.
   * @return true if the ABI is a contract ABI and it satisfies the contract standard.
   */
  boolean check(ContractAbi contractAbi);

  /**
   * Deserialize the state of a contract, extracting the values defined by the standard, returning
   * the standard state representation.
   *
   * @param state the full contract state.
   * @param contractAbi the contract abi.
   * @return A state object holding all the relevant fields for the standard.
   */
  T getStandardState(byte[] state, ContractAbi contractAbi);
}
