package com.partisiablockchain.language.contractstandard.typematcher;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpecWithGenerics;
import java.util.List;

/**
 * A type matcher which checks whether the type index matches as well as recursive type matchers for
 * generic types.
 */
public final class TypeIndexMatcher implements TypeMatcher {
  private final TypeSpec.TypeIndex expectedType;
  private final TypeMatcher[] expectedGenerics;

  /** Type matcher for byte arrays. */
  public static TypeIndexMatcher SIZED_BYTE_ARRAY =
      new TypeIndexMatcher(
          TypeSpec.TypeIndex.SizedArray, new TypeIndexMatcher(TypeSpec.TypeIndex.u8));

  /**
   * Constructor for {@link TypeIndexMatcher}.
   *
   * @param expectedType expected type index
   * @param expectedGenerics type matchers for generic types
   */
  public TypeIndexMatcher(TypeSpec.TypeIndex expectedType, TypeMatcher... expectedGenerics) {
    this.expectedType = expectedType;
    this.expectedGenerics = expectedGenerics;
  }

  @Override
  public boolean matches(
      TypeSpec type, List<NamedTypeSpec> namedTypes, List<NamedTypeMatcher> expectedNamedTypes) {

    if (type.typeIndex() != expectedType) {
      return false;
    }

    if (type instanceof TypeSpecWithGenerics withGenerics) {
      for (int i = 0; i < withGenerics.getGenerics().size(); i++) {
        if (!expectedGenerics[i].matches(
            withGenerics.getGenerics().get(i), namedTypes, expectedNamedTypes)) {
          return false;
        }
      }
    }
    return true;
  }
}
