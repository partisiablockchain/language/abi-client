package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.secata.stream.SafeDataInputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * MPC-20 contract standard definition.
 *
 * @see <a
 *     href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/mpc-20-token-contract.html">
 *     MPC-20 Specification</a>
 */
@Immutable
public final class Mpc20ContractStandard implements ContractStandard<Mpc20ContractState> {
  static final List<FieldDefinition> FIELDS =
      List.of(
          new RecursiveFieldDefinition("name", TypeSpecs.STRING),
          new RecursiveFieldDefinition("symbol", TypeSpecs.STRING),
          new RecursiveFieldDefinition("decimals", TypeSpecs.U8),
          new RecursiveFieldDefinition(
              "balances", new MapTypeSpec(TypeSpecs.ADDRESS, TypeSpecs.U128)),
          new RecursiveFieldDefinition(
              "allowed",
              new MapTypeSpec(
                  TypeSpecs.ADDRESS, new MapTypeSpec(TypeSpecs.ADDRESS, TypeSpecs.U128))));

  static final List<ActionDefinition> ACTIONS =
      List.of(
          new ActionDefinition(
              "transfer",
              new byte[] {1},
              List.of(
                  new ArgumentAbi("to", TypeSpecs.ADDRESS),
                  new ArgumentAbi("amount", TypeSpecs.U128))),
          new ActionDefinition(
              "transfer_from",
              new byte[] {3},
              List.of(
                  new ArgumentAbi("from", TypeSpecs.ADDRESS),
                  new ArgumentAbi("to", TypeSpecs.ADDRESS),
                  new ArgumentAbi("amount", TypeSpecs.U128))),
          new ActionDefinition(
              "approve",
              new byte[] {5},
              List.of(
                  new ArgumentAbi("spender", TypeSpecs.ADDRESS),
                  new ArgumentAbi("amount", TypeSpecs.U128))));

  @Override
  public boolean check(ContractAbi contractAbi) {
    boolean checkFields = FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));
    boolean checkActions =
        ACTIONS.stream().allMatch(actionDefinition -> actionDefinition.check(contractAbi));

    return checkFields && checkActions;
  }

  @Override
  public Mpc20ContractState getStandardState(byte[] state, ContractAbi contractAbi) {
    if (!check(contractAbi)) {
      throw new RuntimeException("Contract abi does not follow the MPC20 standard");
    }
    try {
      ScValueStruct stateStruct = StateReader.create(state, contractAbi).readState();
      return new Mpc20ContractState(
          FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue(),
          FIELDS.get(1).getFieldValue(contractAbi, stateStruct).stringValue(),
          (short) FIELDS.get(2).getFieldValue(contractAbi, stateStruct).asInt(),
          getBalances(FIELDS.get(3).getFieldValue(contractAbi, stateStruct).mapValue()),
          getAllowed(FIELDS.get(4).getFieldValue(contractAbi, stateStruct).mapValue()));

    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize state.", e);
    }
  }

  private static Map<BlockchainAddress, BigInteger> getBalances(ScValueMap scValueMap) {
    return scValueMap.map().entrySet().stream()
        .collect(
            Collectors.toMap(
                e ->
                    SafeDataInputStream.readFully(
                        e.getKey().addressValue().bytes(), BlockchainAddress::read),
                e -> e.getValue().u128Value()));
  }

  private static Map<BlockchainAddress, Map<BlockchainAddress, BigInteger>> getAllowed(
      ScValueMap scValueMap) {
    return scValueMap.map().entrySet().stream()
        .collect(
            Collectors.toMap(
                e ->
                    SafeDataInputStream.readFully(
                        e.getKey().addressValue().bytes(), BlockchainAddress::read),
                e -> getBalances(e.getValue().mapValue())));
  }
}
