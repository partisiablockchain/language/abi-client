package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.contractstandard.typematcher.StructEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeIndexMatcher;
import com.secata.stream.SafeDataInputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * MPC-721 contract standard definition.
 *
 * @see <a
 *     href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/mpc-721-nft-contract.html">
 *     MPC-721 Specification</a>
 */
@Immutable
public final class Mpc721ContractStandard implements ContractStandard<Mpc721ContractState> {

  private final boolean uriExtension;

  static final List<FieldDefinition> FIELDS =
      List.of(
          new RecursiveFieldDefinition("name", TypeSpecs.STRING),
          new RecursiveFieldDefinition("symbol", TypeSpecs.STRING),
          new RecursiveFieldDefinition(
              "owners", new MapTypeSpec(TypeSpecs.U128, TypeSpecs.ADDRESS)),
          new RecursiveFieldDefinition(
              "token_approvals", new MapTypeSpec(TypeSpecs.U128, TypeSpecs.ADDRESS)),
          new RecursiveFieldDefinition(
              "operator_approvals",
              new TypeEqualityMatcher(new VecTypeSpec(new NamedTypeRef(0))),
              List.of(
                  StructEqualityMatcher.fromNamedTypeSpec(
                      new StructTypeSpec(
                          "OperatorApproval",
                          List.of(
                              new FieldAbi("owner", TypeSpecs.ADDRESS),
                              new FieldAbi("operator", TypeSpecs.ADDRESS))),
                      true))));

  static final List<FieldDefinition> URI_FIELDS =
      List.of(
          new RecursiveFieldDefinition("uri_template", TypeSpecs.STRING),
          new RecursiveFieldDefinition(
              "token_uri_details",
              new TypeIndexMatcher(
                  TypeSpec.TypeIndex.Map,
                  new TypeIndexMatcher(TypeSpec.TypeIndex.u128),
                  TypeIndexMatcher.SIZED_BYTE_ARRAY)));

  static final List<ActionDefinition> ACTIONS =
      List.of(
          new ActionDefinition(
              "transfer_from",
              new byte[] {3},
              List.of(
                  new ArgumentAbi("from", TypeSpecs.ADDRESS),
                  new ArgumentAbi("to", TypeSpecs.ADDRESS),
                  new ArgumentAbi("token_id", TypeSpecs.U128))),
          new ActionDefinition(
              "approve",
              new byte[] {5},
              List.of(
                  new ArgumentAbi("approved", new OptionTypeSpec(TypeSpecs.ADDRESS)),
                  new ArgumentAbi("token_id", TypeSpecs.U128))),
          new ActionDefinition(
              "set_approval_for_all",
              new byte[] {7},
              List.of(
                  new ArgumentAbi("operator", TypeSpecs.ADDRESS),
                  new ArgumentAbi("approved", TypeSpecs.BOOL))));

  /**
   * Constructor.
   *
   * @param uriExtension true if the standard checks for uri extension
   */
  public Mpc721ContractStandard(boolean uriExtension) {
    this.uriExtension = uriExtension;
  }

  @Override
  public boolean check(ContractAbi contractAbi) {
    boolean checkFields = FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));
    boolean checkActions =
        ACTIONS.stream().allMatch(actionDefinition -> actionDefinition.check(contractAbi));

    if (uriExtension) {
      boolean checkUri =
          URI_FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));
      return checkFields && checkUri && checkActions;
    } else {
      return checkFields && checkActions;
    }
  }

  @Override
  public Mpc721ContractState getStandardState(byte[] state, ContractAbi contractAbi) {
    if (!check(contractAbi)) {
      throw new RuntimeException(
          "Contract abi does not follow the MPC721%s standard"
              .formatted(uriExtension ? "-URI" : ""));
    }
    try {
      ScValueStruct stateStruct = StateReader.create(state, contractAbi).readState();

      return new Mpc721ContractState(
          FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue(),
          FIELDS.get(1).getFieldValue(contractAbi, stateStruct).stringValue(),
          getIdAddressMap(FIELDS.get(2).getFieldValue(contractAbi, stateStruct).mapValue()),
          getIdAddressMap(FIELDS.get(3).getFieldValue(contractAbi, stateStruct).mapValue()),
          getOperatorApprovals(FIELDS.get(4).getFieldValue(contractAbi, stateStruct).vecValue()),
          uriExtension
              ? URI_FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue()
              : null,
          uriExtension
              ? getUriDetails(URI_FIELDS.get(1).getFieldValue(contractAbi, stateStruct).mapValue())
              : null);

    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize state.", e);
    }
  }

  private static Map<BigInteger, BlockchainAddress> getIdAddressMap(ScValueMap scValueMap) {
    return scValueMap.map().entrySet().stream()
        .collect(
            Collectors.toMap(e -> e.getKey().u128Value(), e -> addressFromScValue(e.getValue())));
  }

  private static Map<BigInteger, byte[]> getUriDetails(ScValueMap scValueMap) {
    return scValueMap.map().entrySet().stream()
        .collect(Collectors.toMap(e -> e.getKey().u128Value(), e -> e.getValue().vecU8Value()));
  }

  private static BlockchainAddress addressFromScValue(ScValue scValue) {
    return SafeDataInputStream.readFully(scValue.addressValue().bytes(), BlockchainAddress::read);
  }

  private static List<Mpc721ContractState.OperatorApproval> getOperatorApprovals(
      ScValueVector scValueVec) {
    return scValueVec.values().stream()
        .map(ScValue::structValue)
        .map(
            struct ->
                new Mpc721ContractState.OperatorApproval(
                    addressFromScValue(struct.getFieldValue("owner")),
                    addressFromScValue(struct.getFieldValue("operator"))))
        .toList();
  }
}
