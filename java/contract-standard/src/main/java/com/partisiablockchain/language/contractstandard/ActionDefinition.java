package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.Arrays;
import java.util.List;

/**
 * ABI contract action checker. Used to check if actions of a contract ABI matches that of the
 * specification.
 */
public final class ActionDefinition {

  private final String name;
  private final byte[] shortName;
  private final List<ArgumentAbi> arguments;

  /**
   * Constructor.
   *
   * @param name name of the action
   * @param shortName shortname of the action
   * @param arguments arguments of the action
   */
  public ActionDefinition(String name, byte[] shortName, List<ArgumentAbi> arguments) {
    this.name = name;
    this.shortName = shortName;
    this.arguments = arguments;
  }

  /**
   * Check if there is an action with the given signature. The action must match with the name,
   * shortname and arguments to be a match.
   *
   * @param abi the contract ABI to test.
   * @return true if the contract ABI has an action with the given name, shortname, and arguments.
   */
  public boolean check(ContractAbi abi) {
    ContractInvocation function = abi.getFunctionByName(name);

    if (function == null) {
      return false;
    }

    if (function.kind().kindId() != ImplicitBinderAbi.DefaultKinds.ACTION.kindId()) {
      return false;
    }

    if (!Arrays.equals(function.shortname(), shortName)) {
      return false;
    }

    List<ArgumentAbi> givenArguments = function.arguments();
    if (givenArguments.size() != arguments.size()) {
      return false;
    }

    for (int i = 0; i < arguments.size(); i++) {
      ArgumentAbi argument = givenArguments.get(i);
      ArgumentAbi expectedArgument = arguments.get(i);
      if (!argument.equals(expectedArgument)) {
        return false;
      }
    }

    return true;
  }
}
