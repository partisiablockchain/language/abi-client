package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Collection of defined Contract Standards.
 *
 * <p>Contract standards outline the required actions, state fields, and interfaces that the
 * contract should implement. Satisfying the standard enables integration and compatibility with
 * existing decentralized applications (DApps). All standards can * be found <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/introduction-to-standard-contract-interfaces.html">here</a>.
 */
public final class ContractStandards {

  /** See {@link Mpc20ContractStandard}. */
  public static final Mpc20ContractStandard MPC20 = new Mpc20ContractStandard();

  /** See {@link Mpc20V2ContractStandard}. */
  public static final Mpc20V2ContractStandard MPC20_V2 = new Mpc20V2ContractStandard();

  /** See {@link Mpc721ContractStandard}. */
  public static final Mpc721ContractStandard MPC721 = new Mpc721ContractStandard(false);

  /** See {@link Mpc721ContractStandard}. */
  public static final Mpc721ContractStandard MPC721_URI = new Mpc721ContractStandard(true);

  /** See {@link Mpc721V2ContractStandard}. */
  public static final Mpc721V2ContractStandard MPC721_V2 = new Mpc721V2ContractStandard(false);

  /** See {@link Mpc721V2ContractStandard}. */
  public static final Mpc721V2ContractStandard MPC721_V2_URI = new Mpc721V2ContractStandard(true);

  private ContractStandards() {}
}
