package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.ContractAbi;

/** Checks that the state struct satisfies some condition for a standard state field. */
public interface FieldDefinition {

  /**
   * Check that the given abi contains a field satisfying this field definition.
   *
   * @param abi given contract abi to test
   * @return true if the abi satisfies the field definition
   */
  boolean check(ContractAbi abi);

  /**
   * Get the value of the field matching this field definition from a given state struct.
   *
   * @param contractAbi abi of the contract to extract the field value from
   * @param stateStruct read state struct to extract the field from
   * @return the field value matching this field definition
   */
  ScValue getFieldValue(ContractAbi contractAbi, ScValueStruct stateStruct);
}
