package com.partisiablockchain.language.contractstandard.typematcher;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import java.util.List;

/**
 * Check if a struct matches this specification where names must match and fields must match given
 * field matchers.
 */
public final class StructEqualityMatcher implements NamedTypeMatcher {

  private final String name;
  private final List<FieldMatcher> expectedFields;
  private final boolean allowExtraFields;

  /**
   * Constructor.
   *
   * @param name name of the struct
   * @param expectedFields expected field matchers
   * @param allowExtraFields whether to allow extra fields than the required
   */
  public StructEqualityMatcher(
      String name, List<FieldMatcher> expectedFields, boolean allowExtraFields) {
    this.name = name;
    this.expectedFields = expectedFields;
    this.allowExtraFields = allowExtraFields;
  }

  /**
   * Create an instance from a named type spec using type equality for the field checkers.
   *
   * @param namedTypeSpec the named type spec to create from
   * @param allowExtraFields whether to allow extra fields than the required
   * @return the struct equality matcher
   */
  public static StructEqualityMatcher fromNamedTypeSpec(
      NamedTypeSpec namedTypeSpec, boolean allowExtraFields) {
    StructTypeSpec structTypeSpec = (StructTypeSpec) namedTypeSpec;
    return new StructEqualityMatcher(
        structTypeSpec.name(),
        structTypeSpec.fields().stream()
            .map(
                fieldAbi ->
                    new FieldMatcher(fieldAbi.name(), new TypeEqualityMatcher(fieldAbi.type())))
            .toList(),
        allowExtraFields);
  }

  /**
   * Create an instance from a named type spec using type equality for the field checkers.
   *
   * @param namedTypeSpec the named type spec to create from
   * @return the struct equality matcher
   */
  public static StructEqualityMatcher fromNamedTypeSpec(NamedTypeSpec namedTypeSpec) {
    return fromNamedTypeSpec(namedTypeSpec, false);
  }

  @Override
  public boolean matches(
      NamedTypeSpec namedType,
      List<NamedTypeSpec> namedTypes,
      List<NamedTypeMatcher> expectedNamedTypes) {
    if (namedType instanceof StructTypeSpec struct) {
      if (!name.equals(struct.name())) {
        return false;
      }
      if (!allowExtraFields && expectedFields.size() != struct.fields().size()) {
        return false;
      }
      for (FieldMatcher expectedField : expectedFields) {
        FieldAbi field = struct.getFieldByName(expectedField.name());
        if (field == null) {
          return false;
        }

        if (!expectedField.expectedType().matches(field.type(), namedTypes, expectedNamedTypes)) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }
}
