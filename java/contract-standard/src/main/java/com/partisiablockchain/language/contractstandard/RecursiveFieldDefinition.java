package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.contractstandard.typematcher.NamedTypeMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.StructEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeMatcher;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Checks whether a field with name and type exists in the state struct specification, either in the
 * root state struct or in a sub-struct that has a 1-1 composition.
 */
public final class RecursiveFieldDefinition implements FieldDefinition {

  private final String fieldName;
  private final TypeMatcher expectedTypeMatcher;
  private final List<NamedTypeMatcher> expectedNamedTypeMatchers;

  /**
   * Constructor.
   *
   * @param fieldName name of the field to check
   * @param expectedTypeMatcher expected type matcher of the field
   * @param expectedNamedTypeMatchers expected named type matchers
   */
  public RecursiveFieldDefinition(
      String fieldName,
      TypeMatcher expectedTypeMatcher,
      List<NamedTypeMatcher> expectedNamedTypeMatchers) {
    this.fieldName = fieldName;
    this.expectedTypeMatcher = expectedTypeMatcher;
    this.expectedNamedTypeMatchers = expectedNamedTypeMatchers;
  }

  /**
   * Constructor.
   *
   * @param fieldName name of the field to check
   * @param expectedTypeMatcher expected type matcher of the field
   */
  public RecursiveFieldDefinition(String fieldName, TypeMatcher expectedTypeMatcher) {
    this(fieldName, expectedTypeMatcher, List.of());
  }

  /**
   * Constructor.
   *
   * @param fieldName name of the field to check
   * @param expectedType expected type of the field
   * @param expectedNamedTypes expected named types
   */
  public RecursiveFieldDefinition(
      String fieldName, TypeSpec expectedType, List<NamedTypeSpec> expectedNamedTypes) {
    this(
        fieldName,
        new TypeEqualityMatcher(expectedType),
        expectedNamedTypes.stream()
            .map(StructEqualityMatcher::fromNamedTypeSpec)
            .collect(Collectors.toList()));
  }

  /**
   * Constructor.
   *
   * @param fieldName name of the field to check
   * @param expectedType expected type of the field
   */
  public RecursiveFieldDefinition(String fieldName, TypeSpec expectedType) {
    this(fieldName, expectedType, List.of());
  }

  /**
   * Check that a given struct contains a specific field.
   *
   * @param contractAbi the contract abi to check
   * @param struct the struct to be checked.
   * @return whether the struct complies to the given check function.
   */
  public boolean checkField(ContractAbi contractAbi, StructTypeSpec struct) {
    FieldAbi fieldByName = struct.getFieldByName(fieldName);

    if (fieldByName == null) {
      return false;
    }

    return expectedTypeMatcher.matches(
        fieldByName.type(), contractAbi.namedTypes(), expectedNamedTypeMatchers);
  }

  private int countFieldsInStateOrSubStruct(ContractAbi abi, StructTypeSpec struct) {
    int count = 0;
    if (checkField(abi, struct)) {
      count++;
    }

    for (FieldAbi field : struct.fields()) {
      if (field.type() instanceof NamedTypeRef namedFieldRef) {
        NamedTypeSpec namedFieldType = abi.getNamedType(namedFieldRef);
        if (namedFieldType instanceof StructTypeSpec subStruct) {
          count += countFieldsInStateOrSubStruct(abi, subStruct);
        }
      }
    }
    return count;
  }

  /**
   * Check that a given contract's state contains a single field complying with this field checker,
   * either directly in the root struct or in a sub struct with a 1-1 composition.
   *
   * @param contractAbi the contract to test
   * @return true if the contract has a compliant field
   */
  @Override
  public boolean check(ContractAbi contractAbi) {
    StructTypeSpec stateStruct = contractAbi.getStateStruct();

    int numberOfFields = countFieldsInStateOrSubStruct(contractAbi, stateStruct);
    return numberOfFields == 1;
  }

  /**
   * Traverse through a state struct given a list of field accesses.
   *
   * @param stateStruct the state struct
   * @param fieldPath the list of field accesses
   * @return The found struct
   */
  private static ScValueStruct traverseFieldPath(
      ScValueStruct stateStruct, Deque<String> fieldPath) {
    ScValueStruct resultStruct = stateStruct;
    for (String pathPart : fieldPath) {
      resultStruct = (ScValueStruct) resultStruct.getFieldValue(pathPart);
    }
    return resultStruct;
  }

  /**
   * Get the full field path through the contract state to a specific field.
   *
   * @param contractAbi the contract ABI
   * @return a deque of field accesses
   */
  Deque<String> getValidFieldPath(ContractAbi contractAbi) {
    StructTypeSpec stateStruct = contractAbi.getStateStruct();
    return getPartialFieldPath(contractAbi, stateStruct);
  }

  /**
   * Get the field path from a given struct to a specific field.
   *
   * @param abi the contract ABI
   * @param struct the struct to start from
   * @return a deque of field accesses
   */
  private Deque<String> getPartialFieldPath(ContractAbi abi, StructTypeSpec struct) {
    if (checkField(abi, struct)) {
      return new ArrayDeque<>();
    }

    for (FieldAbi field : struct.fields()) {
      if (field.type() instanceof NamedTypeRef namedFieldRef) {
        NamedTypeSpec namedFieldType = abi.getNamedType(namedFieldRef);
        if (namedFieldType instanceof StructTypeSpec subStruct) {
          Deque<String> partialFieldPath = getPartialFieldPath(abi, subStruct);
          if (partialFieldPath != null) {
            partialFieldPath.addFirst(field.name());
            return partialFieldPath;
          }
        }
      }
    }
    return null;
  }

  /**
   * Get a standard field value from a contract state, possible traversing sub structs to find it.
   *
   * @param contractAbi the contract ABI
   * @param stateStruct the state struct
   * @return the value of the standard field
   */
  @Override
  public ScValue getFieldValue(ContractAbi contractAbi, ScValueStruct stateStruct) {
    Deque<String> namePath = getValidFieldPath(contractAbi);
    ScValueStruct nameStruct = traverseFieldPath(stateStruct, namePath);
    return nameStruct.getFieldValue(fieldName);
  }
}
