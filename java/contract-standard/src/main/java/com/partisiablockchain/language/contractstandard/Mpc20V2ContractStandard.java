package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import java.util.List;

/**
 * MPC-20-V2 contract standard definition.
 *
 * @see <a
 *     href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/mpc-20-token-contract.html#version-2">
 *     MPC-20-v2 Specification</a>
 */
@Immutable
public final class Mpc20V2ContractStandard implements ContractStandard<Mpc20V2ContractState> {
  static final List<FieldDefinition> FIELDS =
      List.of(
          new RecursiveFieldDefinition("name", TypeSpecs.STRING),
          new RecursiveFieldDefinition("symbol", TypeSpecs.STRING),
          new RecursiveFieldDefinition("decimals", TypeSpecs.U8),
          new RecursiveFieldDefinition(
              "balances", new AvlTreeMapTypeSpec(TypeSpecs.ADDRESS, TypeSpecs.U128)),
          new RecursiveFieldDefinition(
              "allowed",
              new AvlTreeMapTypeSpec(new NamedTypeRef(0), TypeSpecs.U128),
              List.of(
                  new StructTypeSpec(
                      "AllowedAddress",
                      List.of(
                          new FieldAbi("owner", TypeSpecs.ADDRESS),
                          new FieldAbi("spender", TypeSpecs.ADDRESS))))));

  static final List<ActionDefinition> ACTIONS = Mpc20ContractStandard.ACTIONS;

  @Override
  public boolean check(ContractAbi contractAbi) {
    boolean checkFields = FIELDS.stream().allMatch(fieldChecker -> fieldChecker.check(contractAbi));
    boolean checkActions =
        ACTIONS.stream().allMatch(actionDefinition -> actionDefinition.check(contractAbi));

    return checkFields && checkActions;
  }

  @Override
  public Mpc20V2ContractState getStandardState(byte[] state, ContractAbi contractAbi) {
    if (!check(contractAbi)) {
      throw new RuntimeException("Contract abi does not follow the MPC20-V2 standard");
    }
    try {
      ScValueStruct stateStruct = StateReader.create(state, contractAbi).readState();
      return new Mpc20V2ContractState(
          FIELDS.get(0).getFieldValue(contractAbi, stateStruct).stringValue(),
          FIELDS.get(1).getFieldValue(contractAbi, stateStruct).stringValue(),
          (short) FIELDS.get(2).getFieldValue(contractAbi, stateStruct).asInt(),
          FIELDS.get(3).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId(),
          FIELDS.get(4).getFieldValue(contractAbi, stateStruct).avlTreeMapValue().treeId());

    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize state.", e);
    }
  }
}
