package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.math.BigInteger;
import java.util.Map;

/**
 * Record holding the field values for an MPC-20 standard contract state.
 *
 * <p>An MPC-20 token contract is a standardized digital asset inspired by the guidelines outlined
 * in the EIP20. Allows for the implementation of tokens within smart contracts.
 *
 * @param name the name of the token.
 * @param symbol the symbol of the token.
 * @param decimals the number of decimals the token uses.
 * @param balances the token balances of other accounts.
 * @param allowed the allowances given between accounts.
 */
public record Mpc20ContractState(
    String name,
    String symbol,
    short decimals,
    Map<BlockchainAddress, BigInteger> balances,
    Map<BlockchainAddress, Map<BlockchainAddress, BigInteger>> allowed)
    implements StandardState {}
