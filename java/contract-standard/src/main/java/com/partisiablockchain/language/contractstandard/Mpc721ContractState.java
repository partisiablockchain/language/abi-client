package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * Record holding the field values for an MPC-721 standard contract state.
 *
 * <p>An MPC-721 nft contract is a standardized digital asset inspired by the guidelines outlined in
 * the EIP721. Allows for the implementation of NFTs within smart contracts.
 *
 * @param name the name of the nft.
 * @param symbol the symbol of the nft.
 * @param owners owners of all minted nfts
 * @param tokenApprovals approvals given for others to transfer a given nft
 * @param operatorApprovals approvals given for an operator to handle all of an owners nfts
 * @param uriTemplate template for a URI. Replaces '{}' with the token uri details of a given nft
 * @param tokenUriDetails the token uri details of each nft
 */
public record Mpc721ContractState(
    String name,
    String symbol,
    Map<BigInteger, BlockchainAddress> owners,
    Map<BigInteger, BlockchainAddress> tokenApprovals,
    List<OperatorApproval> operatorApprovals,
    String uriTemplate,
    Map<BigInteger, byte[]> tokenUriDetails)
    implements StandardState {

  /**
   * Get the full URI of a given token id, by inserting the uri details of the token into the URI
   * template.
   *
   * @param tokenId token id
   * @return the full URI
   */
  public String getUri(BigInteger tokenId) {
    byte[] uriDetails = tokenUriDetails.get(tokenId);
    String uriDetailsString = new String(uriDetails, StandardCharsets.UTF_8).replaceAll("\0", "");
    return uriTemplate.replace("{}", uriDetailsString);
  }

  /**
   * Approval given from an owner to an operator to handle all the owners nfts.
   *
   * @param owner owner of the nfts
   * @param operator operator
   */
  public record OperatorApproval(BlockchainAddress owner, BlockchainAddress operator) {}
}
