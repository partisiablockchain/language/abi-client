package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
final class ActionDefinitionTest {

  /** Check succeeds on correct action. */
  @Test
  void checkSucceeds() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition =
        new ActionDefinition(
            "transfer",
            new byte[] {1},
            List.of(
                new ArgumentAbi("to", TypeSpecs.ADDRESS),
                new ArgumentAbi("amount", TypeSpecs.U128)));

    assertThat(actionDefinition.check(tokenAbi)).isTrue();
  }

  /** Check fails on wrong binderInvocation kind. */
  @Test
  void checkFailsWrongKind() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition =
        new ActionDefinition("initialize", new byte[] {1}, List.of());

    assertThat(actionDefinition.check(tokenAbi)).isFalse();
  }

  /** Check fails on wrong shortname. */
  @Test
  void checkFailsWrongShortname() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition = new ActionDefinition("transfer", new byte[] {3}, List.of());

    assertThat(actionDefinition.check(tokenAbi)).isFalse();
  }

  /** Check fails on missing argument. */
  @Test
  void checkFailsMissingArgument() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition =
        new ActionDefinition(
            "transfer", new byte[] {1}, List.of(new ArgumentAbi("to", TypeSpecs.ADDRESS)));

    assertThat(actionDefinition.check(tokenAbi)).isFalse();
  }

  /** Check fails on wrong argument order. */
  @Test
  void checkFailsWrongArgumentOrder() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition =
        new ActionDefinition(
            "transfer",
            new byte[] {1},
            List.of(
                new ArgumentAbi("amount", TypeSpecs.U128),
                new ArgumentAbi("to", TypeSpecs.ADDRESS)));

    assertThat(actionDefinition.check(tokenAbi)).isFalse();
  }

  /** Check fails on wrong name. */
  @Test
  void checkFailsWrongName() {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ActionDefinition actionDefinition = new ActionDefinition("unknown", new byte[] {1}, List.of());

    assertThat(actionDefinition.check(tokenAbi)).isFalse();
  }
}
