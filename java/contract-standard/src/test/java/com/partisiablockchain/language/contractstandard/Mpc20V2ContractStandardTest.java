package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.ContractAbi;
import org.junit.jupiter.api.Test;

/** Test. */
public final class Mpc20V2ContractStandardTest {

  /** Check for correct mpc 20 v2 abi succeeds. */
  @Test
  void mpc20v2() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token_v2.abi");
    assertThat(ContractStandards.MPC20_V2.check(abi)).isTrue();
  }

  /** Incorrect mpc 20 v2 abi state correctly fails. */
  @Test
  void mpc20Error() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");
    assertThat(ContractStandards.MPC20_V2.check(abi)).isFalse();
  }

  /** Mpc-20 v2 check fails on missing action. */
  @Test
  void missingAction() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token_v2.abi");
    ContractAbi missingAction = ContractStandardsTestUtil.removeAction(abi, "approve");
    assertThat(ContractStandards.MPC20_V2.check(missingAction)).isFalse();
  }

  /** Correctly get standard state, when abi match standard. */
  @Test
  void getStandardState() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token_v2.abi");
    byte[] stateBytes = ContractStandardsTestUtil.getSerializedState("token_v2.state");
    Mpc20V2ContractState standardState =
        ContractStandards.MPC20_V2.getStandardState(stateBytes, abi);

    assertThat(standardState.name()).isEqualTo("Test example");
    assertThat(standardState.symbol()).isEqualTo("S");
    assertThat(standardState.decimals()).isEqualTo((short) 0);
    assertThat(standardState.balancesTreeId()).isEqualTo(0);
    assertThat(standardState.allowedTreeId()).isEqualTo(1);
  }

  /** Get standard state fails if given abi doesn't follow the standard. */
  @Test
  void getStandardStateWrongError() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");
    byte[] stateBytes = ContractStandardsTestUtil.getSerializedState("token_v2.state");
    assertThatThrownBy(() -> ContractStandards.MPC20_V2.getStandardState(stateBytes, abi))
        .hasMessageContaining("Contract abi does not follow the MPC20-V2 standard");
  }

  /** Unable to parse standard state when given wrong state bytes. */
  @Test
  void getStandardStateError() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token_v2.abi");
    byte[] stateBytes = new byte[42];
    assertThatThrownBy(() -> ContractStandards.MPC20_V2.getStandardState(stateBytes, abi))
        .hasMessageContaining("Could not deserialize state.");
  }

  /**
   * Mpc 20 v2 check succeeds even when the expected named type of AllowedAddress has a different
   * named type index.
   */
  @Test
  void testing() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("sub_struct_token_v2.abi");
    assertThat(ContractStandards.MPC20_V2.check(abi)).isTrue();
  }
}
