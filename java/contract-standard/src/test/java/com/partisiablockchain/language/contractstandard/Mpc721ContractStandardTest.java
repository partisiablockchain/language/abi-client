package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Test. */
public final class Mpc721ContractStandardTest {

  /** Check for mpc 721 without uri extension succeeds on abi with uri extension. */
  @Test
  void mpc721() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");
    assertThat(ContractStandards.MPC721.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 with uri extension succeeds on abi with uri extension. */
  @Test
  void mpc721Uri() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");
    assertThat(ContractStandards.MPC721_URI.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 without uri extension succeeds on abi without uri extension. */
  @Test
  void mpc721NoUri() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract_without_uri.abi");
    assertThat(ContractStandards.MPC721.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 with uri extension fails on abi without uri extension. */
  @Test
  void mpc721NoUriFails() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract_without_uri.abi");
    assertThat(ContractStandards.MPC721_URI.check(mpc721Abi)).isFalse();
  }

  /** Check for mpc 721 fails when missing required action. */
  @Test
  void missingActions() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");
    ContractAbi missingActionAbi =
        ContractStandardsTestUtil.removeAction(mpc721Abi, "transfer_from");

    assertThat(ContractStandards.MPC721_URI.check(missingActionAbi)).isFalse();
    assertThat(ContractStandards.MPC721.check(missingActionAbi)).isFalse();
  }

  /** Check for mpc 721 fails when missing required field. */
  @Test
  void missingFields() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");
    ContractAbi missingFieldAbi = ContractStandardsTestUtil.removeStateField(mpc721Abi, "name");

    assertThat(ContractStandards.MPC721_URI.check(missingFieldAbi)).isFalse();
    assertThat(ContractStandards.MPC721.check(missingFieldAbi)).isFalse();
  }

  /** Successfully get standard state from mpc 721 with uri state. */
  @Test
  void getStateWithUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");

    Mpc721ContractState state =
        ContractStandards.MPC721_URI.getStandardState(
            ContractStandardsTestUtil.getSerializedState("nft.state"), abi);

    assertThat(state.name()).isEqualTo("SomeNFT");
    assertThat(state.symbol()).isEqualTo("SNFT");
    assertThat(state.owners()).hasSize(3);
    assertThat(state.owners().get(BigInteger.ONE))
        .isEqualTo(BlockchainAddress.fromString("00c5dcb3bcf6f048b0a765184b55b3f8d89dea7377"));
    assertThat(state.owners().get(BigInteger.TWO))
        .isEqualTo(BlockchainAddress.fromString("00ceca763afc2c2e6933db580d4f788e4df7d6e8e6"));
    assertThat(state.owners().get(BigInteger.valueOf(3)))
        .isEqualTo(BlockchainAddress.fromString("00a79f6a95f769e404764095a48d9c830e63510b88"));

    assertThat(state.tokenApprovals()).hasSize(0);
    assertThat(state.operatorApprovals()).hasSize(0);

    assertThat(state.uriTemplate()).isEqualTo("www.snft/{}");
    assertThat(state.getUri(BigInteger.ONE)).isEqualTo("www.snft/0x01");
    assertThat(state.getUri(BigInteger.TWO)).isEqualTo("www.snft/0x02");
    assertThat(state.getUri(BigInteger.valueOf(3))).isEqualTo("www.snft/0x03");
  }

  /** Successfully get standard state from mpc 721 without uri state. */
  @Test
  void getStateWithoutUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_contract_without_uri.abi");

    Mpc721ContractState state =
        ContractStandards.MPC721.getStandardState(
            ContractStandardsTestUtil.getSerializedState("nft_without_uri.state"), abi);

    assertThat(state.name()).isEqualTo("No URI NFT");
    assertThat(state.symbol()).isEqualTo("NU");
    assertThat(state.owners()).hasSize(1);
    assertThat(state.owners().get(BigInteger.ONE))
        .isEqualTo(BlockchainAddress.fromString("004eb8ff45f564d7e46f556456671548eb8013adac"));

    assertThat(state.tokenApprovals()).hasSize(1);
    assertThat(state.tokenApprovals().get(BigInteger.ONE))
        .isEqualTo(BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5"));

    assertThat(state.operatorApprovals()).hasSize(1);
    assertThat(state.operatorApprovals().get(0))
        .isEqualTo(
            new Mpc721ContractState.OperatorApproval(
                BlockchainAddress.fromString("004eb8ff45f564d7e46f556456671548eb8013adac"),
                BlockchainAddress.fromString("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed")));

    assertThat(state.uriTemplate()).isNull();
    assertThat(state.tokenUriDetails()).isNull();
  }

  /** Trying to get standard state for mpc 721 with uri fails if abi doesn't follow the standard. */
  @Test
  void cannotGetUriStateFromNoUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_contract_without_uri.abi");

    assertThatThrownBy(
            () ->
                ContractStandards.MPC721_URI.getStandardState(
                    ContractStandardsTestUtil.getSerializedState("nft_without_uri.state"), abi))
        .hasMessageContaining("Contract abi does not follow the MPC721-URI standard");
  }

  /**
   * Trying to get standard state for mpc 721 without uri fails if abi doesn't follow the standard.
   */
  @Test
  void cannotGetStateFromWrongAbi() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");

    assertThatThrownBy(
            () ->
                ContractStandards.MPC721.getStandardState(
                    ContractStandardsTestUtil.getSerializedState("nft_without_uri.state"), abi))
        .hasMessageContaining("Contract abi does not follow the MPC721 standard");
  }

  /** Trying to deserialize incorrect state bytes fails with error. */
  @Test
  void garbageStateBytes() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_contract.abi");
    assertThatThrownBy(() -> ContractStandards.MPC721.getStandardState(new byte[20], abi))
        .hasMessageContaining("Could not deserialize state.");
  }
}
