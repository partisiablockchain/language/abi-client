package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.Objects;

/** Test. */
final class ContractStandardsTestUtil {

  static ContractAbi removeAction(ContractAbi abi, String actionName) {
    List<ContractInvocation> functions =
        abi.contractInvocations().stream().filter(f -> !f.name().equals(actionName)).toList();

    List<FieldAbi> stateFields = abi.getStateStruct().fields();
    return buildSubAbi(abi, functions, stateFields);
  }

  static ContractAbi changeActionShortname(ContractAbi abi, String actionName, byte shortname) {
    List<ContractInvocation> newFunctions = new ArrayList<>();
    for (ContractInvocation function : abi.contractInvocations()) {
      if (function.name().equals(actionName)) {
        ContractInvocation contractInvocation =
            new ContractInvocation(
                function.kind(), function.name(), new byte[] {shortname}, function.arguments());
        newFunctions.add(contractInvocation);
      } else {
        newFunctions.add(function);
      }
    }

    List<FieldAbi> stateFields = abi.getStateStruct().fields();
    return buildSubAbi(abi, newFunctions, stateFields);
  }

  static ContractAbi changeActionArguments(
      ContractAbi abi, String actionName, List<ArgumentAbi> actionArguments) {
    List<ContractInvocation> newFunctions = new ArrayList<>();
    for (ContractInvocation function : abi.contractInvocations()) {
      if (function.name().equals(actionName)) {
        ContractInvocation contractInvocation =
            new ContractInvocation(
                function.kind(), function.name(), function.shortname(), actionArguments);
        newFunctions.add(contractInvocation);
      } else {
        newFunctions.add(function);
      }
    }

    List<FieldAbi> stateFields = abi.getStateStruct().fields();
    return buildSubAbi(abi, newFunctions, stateFields);
  }

  static ContractAbi changeStateFieldType(ContractAbi abi, String fieldName, TypeSpec typeSpec) {
    List<FieldAbi> newStateFields = new ArrayList<>();
    for (FieldAbi fieldAbi : abi.getStateStruct().fields()) {
      if (fieldAbi.name().equals(fieldName)) {
        newStateFields.add(new FieldAbi(fieldName, typeSpec));
      } else {
        newStateFields.add(fieldAbi);
      }
    }

    List<ContractInvocation> functions = abi.contractInvocations();
    return buildSubAbi(abi, functions, newStateFields);
  }

  static ContractAbi removeStateField(ContractAbi abi, String fieldName) {
    List<FieldAbi> stateFields =
        abi.getStateStruct().fields().stream().filter(f -> !f.name().equals(fieldName)).toList();
    List<ContractInvocation> functions = abi.contractInvocations();
    return buildSubAbi(abi, functions, stateFields);
  }

  static ContractAbi buildSubAbi(
      ContractAbi abi, List<ContractInvocation> functions, List<FieldAbi> stateFields) {
    String name = abi.getStateStruct().name();

    NamedTypeRef stateType = null;
    List<NamedTypeSpec> newNamedTypes = new ArrayList<>();
    List<NamedTypeSpec> existingNamedTypes = abi.namedTypes();
    for (int i = 0; i < existingNamedTypes.size(); i++) {
      NamedTypeSpec namedType = existingNamedTypes.get(i);
      if (namedType.name().equals(name)) {
        stateType = new NamedTypeRef(i);
        StructTypeSpec stateTypeSpec = new StructTypeSpec(name, stateFields);
        newNamedTypes.add(stateTypeSpec);
      } else {
        newNamedTypes.add(namedType);
      }
    }

    return new ContractAbi(
        BinderType.Public,
        List.of(),
        newNamedTypes,
        List.of(),
        functions,
        Objects.requireNonNull(stateType));
  }

  /**
   * Gets the serialized ABI.
   *
   * @return the serialized ABI
   */
  public static ContractAbi getAbi(String name) {
    URL url = Mpc20ContractStandardTest.class.getClassLoader().getResource("abi/" + name);
    assertThat(url).isNotNull();
    URI uri = ExceptionConverter.call(url::toURI);

    byte[] abiBytes = ExceptionConverter.call(() -> Files.readAllBytes(Path.of(uri)));
    return (ContractAbi) new AbiParser(abiBytes).parseAbi().chainComponent();
  }

  /**
   * Get serialized state.
   *
   * @return the serialized token MPC-20 state
   */
  public static byte[] getSerializedState(String name) {
    URL url = Mpc20ContractStandardTest.class.getClassLoader().getResource("state/" + name);
    assertThat(url).isNotNull();
    URI uri = ExceptionConverter.call(url::toURI);

    return ExceptionConverter.call(() -> HexFormat.of().parseHex(Files.readString(Path.of(uri))));
  }
}
