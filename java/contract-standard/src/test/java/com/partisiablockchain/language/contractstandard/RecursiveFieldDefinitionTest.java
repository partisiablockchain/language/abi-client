package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.contractstandard.typematcher.StructEqualityMatcher;
import com.partisiablockchain.language.contractstandard.typematcher.TypeEqualityMatcher;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Testing FieldChecker. */
public final class RecursiveFieldDefinitionTest {

  /** FieldChecker finds field in sub struct. */
  @Test
  void findRootField() {
    ContractAbi abi = createAbi();

    RecursiveFieldDefinition fieldDefinition =
        new RecursiveFieldDefinition("root_field", TypeSpecs.U8);

    assertThat(fieldDefinition.check(abi)).isTrue();
    assertThat(fieldDefinition.getValidFieldPath(abi)).hasSize(0);
  }

  /** FieldChecker finds field in sub struct. */
  @Test
  void findSubField() {
    ContractAbi abi = createAbi();

    RecursiveFieldDefinition fieldDefinition =
        new RecursiveFieldDefinition("sub_field", TypeSpecs.STRING);

    assertThat(fieldDefinition.check(abi)).isTrue();
    assertThat(fieldDefinition.getValidFieldPath(abi)).containsExactly("sub_struct");
  }

  /** FieldChecker doesn't find missing field. */
  @Test
  void missingField() {
    ContractAbi abi = createAbi();

    RecursiveFieldDefinition fieldDefinition =
        new RecursiveFieldDefinition("missing_field", TypeSpecs.STRING);

    assertThat(fieldDefinition.check(abi)).isFalse();
    assertThat(fieldDefinition.getValidFieldPath(abi)).isNull();
  }

  /** FieldChecker doesn't find field in an enum variant. */
  @Test
  void enumVariantField() {
    ContractAbi abi = createAbi();

    RecursiveFieldDefinition fieldDefinition =
        new RecursiveFieldDefinition("variant_field", TypeSpecs.U128);

    assertThat(fieldDefinition.check(abi)).isFalse();
    assertThat(fieldDefinition.getValidFieldPath(abi)).isNull();
  }

  /** Field checker fails when multiple required fields are present. */
  @Test
  void multipleRequiredFields() {
    StructTypeSpec state =
        new StructTypeSpec(
            "State",
            List.of(
                new FieldAbi("field", TypeSpecs.U8),
                new FieldAbi("sub_struct", new NamedTypeRef(1))));
    StructTypeSpec subStruct =
        new StructTypeSpec("SubStruct", List.of(new FieldAbi("field", TypeSpecs.U8)));

    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(state, subStruct),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    RecursiveFieldDefinition fieldDefinition = new RecursiveFieldDefinition("field", TypeSpecs.U8);

    assertThat(fieldDefinition.check(abi)).isFalse();
  }

  /** FieldChecker finds correct fields when the struct matches the expected. */
  @ParameterizedTest
  @MethodSource("correctStructs")
  void hasStructField(NamedTypeSpec struct) {
    ContractAbi tokenV2Abi = ContractStandardsTestUtil.getAbi("token_v2.abi");

    assertThat(
            new RecursiveFieldDefinition(
                    "allowed",
                    new AvlTreeMapTypeSpec(new NamedTypeRef(0), TypeSpecs.U128),
                    List.of(struct))
                .check(tokenV2Abi))
        .isTrue();
  }

  /** FieldChecker fails on fields where the struct does not match expected. */
  @ParameterizedTest
  @MethodSource("wrongStructs")
  void hasStructFieldNegative(NamedTypeSpec struct) {
    ContractAbi tokenV2Abi = ContractStandardsTestUtil.getAbi("token_v2.abi");

    assertThat(
            new RecursiveFieldDefinition(
                    "allowed",
                    new AvlTreeMapTypeSpec(new NamedTypeRef(0), TypeSpecs.U128),
                    List.of(struct))
                .check(tokenV2Abi))
        .isFalse();
  }

  /** Struct equality only succeeds for structs, not enums. */
  @Test
  void structEqualityNotEnum() {
    StructTypeSpec struct = new StructTypeSpec("Struct", List.of());
    EnumTypeSpec enumType = new EnumTypeSpec("Enum", List.of());
    assertThat(
            StructEqualityMatcher.fromNamedTypeSpec(struct).matches(struct, List.of(), List.of()))
        .isTrue();
    assertThat(
            StructEqualityMatcher.fromNamedTypeSpec(struct).matches(enumType, List.of(), List.of()))
        .isFalse();
  }

  /** Sized byte arrays are equal iff their lengths are the same. */
  @Test
  void typeEqualitySizedByteArray() {
    final SizedArrayTypeSpec sized1 = new SizedArrayTypeSpec(TypeSpecs.U8, 32);
    final SizedArrayTypeSpec sized2 = new SizedArrayTypeSpec(TypeSpecs.U8, 16);
    assertThat(new TypeEqualityMatcher(sized1).matches(sized1, List.of(), List.of())).isTrue();
    assertThat(new TypeEqualityMatcher(sized1).matches(sized2, List.of(), List.of())).isFalse();
  }

  private ContractAbi createAbi() {
    StructTypeSpec state =
        new StructTypeSpec(
            "State",
            List.of(
                new FieldAbi("root_field", TypeSpecs.U8),
                new FieldAbi("sub_struct", new NamedTypeRef(1)),
                new FieldAbi("sub_enum", new NamedTypeRef(2))));
    StructTypeSpec subStruct =
        new StructTypeSpec("SubStruct", List.of(new FieldAbi("sub_field", TypeSpecs.STRING)));
    EnumTypeSpec enumTypeSpec =
        new EnumTypeSpec("Enum", List.of(new EnumVariant(0, new NamedTypeRef(3))));
    StructTypeSpec enumVariant =
        new StructTypeSpec("Variant", List.of(new FieldAbi("variant_field", TypeSpecs.U128)));
    return new ContractAbi(
        BinderType.Public,
        List.of(),
        List.of(state, subStruct, enumTypeSpec, enumVariant),
        List.of(),
        List.of(),
        new NamedTypeRef(0));
  }

  private static Stream<NamedTypeSpec> correctStructs() {
    return Stream.of(
        new StructTypeSpec(
            "AllowedAddress",
            List.of(
                new FieldAbi("owner", TypeSpecs.ADDRESS),
                new FieldAbi("spender", TypeSpecs.ADDRESS))),
        new StructTypeSpec(
            "AllowedAddress",
            List.of(
                new FieldAbi("spender", TypeSpecs.ADDRESS),
                new FieldAbi("owner", TypeSpecs.ADDRESS))));
  }

  private static Stream<NamedTypeSpec> wrongStructs() {
    return Stream.of(
        new StructTypeSpec(
            "WrongName",
            List.of(
                new FieldAbi("owner", TypeSpecs.ADDRESS),
                new FieldAbi("spender", TypeSpecs.ADDRESS))),
        new StructTypeSpec(
            "AllowedAddress",
            List.of(
                new FieldAbi("wrongFieldName", TypeSpecs.ADDRESS),
                new FieldAbi("spender", TypeSpecs.ADDRESS))),
        new StructTypeSpec(
            "AllowedAddress",
            List.of(
                new FieldAbi("owner", TypeSpecs.U128), new FieldAbi("spender", TypeSpecs.ADDRESS))),
        new StructTypeSpec("AllowedAddress", List.of(new FieldAbi("owner", TypeSpecs.ADDRESS))),
        new StructTypeSpec(
            "AllowedAddress",
            List.of(
                new FieldAbi("owner", TypeSpecs.ADDRESS),
                new FieldAbi("spender", TypeSpecs.ADDRESS),
                new FieldAbi("extra", TypeSpecs.ADDRESS))));
  }
}
