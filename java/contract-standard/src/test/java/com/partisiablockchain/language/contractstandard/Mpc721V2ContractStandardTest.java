package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.ContractAbi;
import org.junit.jupiter.api.Test;

/** Test. */
public final class Mpc721V2ContractStandardTest {

  /** Check for mpc 721 v2 without uri extension succeeds on abi with uri extension. */
  @Test
  void mpc721() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");
    assertThat(ContractStandards.MPC721_V2.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 v2 with uri extension succeeds on abi with uri extension. */
  @Test
  void mpc721Uri() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");
    assertThat(ContractStandards.MPC721_V2_URI.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 v2 without uri extension succeeds on abi without uri extension. */
  @Test
  void mpc721NoUri() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract_without_uri.abi");
    assertThat(ContractStandards.MPC721_V2.check(mpc721Abi)).isTrue();
  }

  /** Check for mpc 721 v2 with uri extension fails on abi without uri extension. */
  @Test
  void mpc721NoUriFails() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract_without_uri.abi");
    assertThat(ContractStandards.MPC721_V2_URI.check(mpc721Abi)).isFalse();
  }

  /** Check for mpc 721 v2 fails when missing required action. */
  @Test
  void missingActions() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");
    ContractAbi missingActionAbi =
        ContractStandardsTestUtil.removeAction(mpc721Abi, "transfer_from");

    assertThat(ContractStandards.MPC721_V2_URI.check(missingActionAbi)).isFalse();
    assertThat(ContractStandards.MPC721_V2.check(missingActionAbi)).isFalse();
  }

  /** Check for mpc 721 v2 fails when missing required field. */
  @Test
  void missingFields() {
    ContractAbi mpc721Abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");
    ContractAbi missingFieldAbi = ContractStandardsTestUtil.removeStateField(mpc721Abi, "name");

    assertThat(ContractStandards.MPC721_V2_URI.check(missingFieldAbi)).isFalse();
    assertThat(ContractStandards.MPC721_V2.check(missingFieldAbi)).isFalse();
  }

  /** Successfully get standard state from mpc 721 v2 with uri state. */
  @Test
  void getStateWithUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");

    Mpc721V2ContractState state =
        ContractStandards.MPC721_V2_URI.getStandardState(
            ContractStandardsTestUtil.getSerializedState("nft_v2.state"), abi);

    assertThat(state.name()).isEqualTo("SomeNFT");
    assertThat(state.symbol()).isEqualTo("SNFT");
    assertThat(state.ownersTreeId()).isEqualTo(0);
    assertThat(state.tokenApprovalsTreeId()).isEqualTo(1);
    assertThat(state.operatorApprovalsTreeId()).isEqualTo(2);
    assertThat(state.uriTemplate()).isEqualTo("www.snft/{}");
    assertThat(state.tokenUriDetailsTreeId()).isEqualTo(3);
  }

  /** Successfully get standard state from mpc 721 v2 without uri state. */
  @Test
  void getStateWithoutUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");

    Mpc721V2ContractState state =
        ContractStandards.MPC721_V2.getStandardState(
            ContractStandardsTestUtil.getSerializedState("nft_v2.state"), abi);

    assertThat(state.name()).isEqualTo("SomeNFT");
    assertThat(state.symbol()).isEqualTo("SNFT");
    assertThat(state.ownersTreeId()).isEqualTo(0);
    assertThat(state.tokenApprovalsTreeId()).isEqualTo(1);
    assertThat(state.operatorApprovalsTreeId()).isEqualTo(2);
    assertThat(state.uriTemplate()).isNull();
    assertThat(state.tokenUriDetailsTreeId()).isNull();
  }

  /**
   * Trying to get standard state for mpc 721 v2 with uri fails if abi doesn't follow the standard.
   */
  @Test
  void cannotGetUriStateFromNoUri() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_v2_contract_without_uri.abi");

    assertThatThrownBy(
            () ->
                ContractStandards.MPC721_V2_URI.getStandardState(
                    ContractStandardsTestUtil.getSerializedState("nft_without_uri.state"), abi))
        .hasMessageContaining("Contract abi does not follow the MPC721-V2-URI standard");
  }

  /**
   * Trying to get standard state for mpc 721 v2 without uri fails if abi doesn't follow the
   * standard.
   */
  @Test
  void cannotGetStateFromWrongAbi() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");

    assertThatThrownBy(
            () ->
                ContractStandards.MPC721_V2.getStandardState(
                    ContractStandardsTestUtil.getSerializedState("nft_without_uri.state"), abi))
        .hasMessageContaining("Contract abi does not follow the MPC721-V2 standard");
  }

  /** Trying to deserialize incorrect state bytes fails with error. */
  @Test
  void garbageStateBytes() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("nft_v2_contract.abi");
    assertThatThrownBy(() -> ContractStandards.MPC721_V2.getStandardState(new byte[20], abi))
        .hasMessageContaining("Could not deserialize state.");
  }
}
