package com.partisiablockchain.language.contractstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec.TypeIndex;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Testing MPC-20 contract standard. */
public final class Mpc20ContractStandardTest {

  /** Check for correct mpc 20 abi succeeds. */
  @Test
  void mpc20() {
    ContractAbi mpc20Abi = ContractStandardsTestUtil.getAbi("token.abi");
    assertThat(ContractStandards.MPC20.check(mpc20Abi)).isTrue();
  }

  /** Mpc 20 check fails when missing required state field. */
  @ParameterizedTest
  @MethodSource("requiredFieldNames")
  void mpc20MissingField(String fieldName) {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ContractAbi newAbi = ContractStandardsTestUtil.removeStateField(tokenAbi, fieldName);
    assertThat(ContractStandards.MPC20.check(newAbi)).isFalse();
  }

  /** Mpc 20 check fails when required state field have wrong type. */
  @ParameterizedTest
  @MethodSource("requiredFieldNames")
  void mpc20WrongType(String fieldName) {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ContractAbi newAbi =
        ContractStandardsTestUtil.changeStateFieldType(
            tokenAbi, fieldName, new SimpleTypeSpec(TypeIndex.bool));
    assertThat(ContractStandards.MPC20.check(newAbi)).isFalse();
  }

  /** Mpc 20 check fails when missing required action. */
  @ParameterizedTest
  @MethodSource("requiredActionNames")
  void mpc20MissingAction(String actionName) {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ContractAbi missingActionName = ContractStandardsTestUtil.removeAction(tokenAbi, actionName);
    assertThat(ContractStandards.MPC20.check(missingActionName)).isFalse();
  }

  /** Mpc 20 check fails when required action has wrong shortname. */
  @ParameterizedTest
  @MethodSource("requiredActionNames")
  void mpc20WrongShortname(String actionName) {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ContractAbi missingActionName =
        ContractStandardsTestUtil.changeActionShortname(tokenAbi, actionName, (byte) 127);
    assertThat(ContractStandards.MPC20.check(missingActionName)).isFalse();
  }

  /** Mpc 20 check fails when required action has wrong arguments. */
  @ParameterizedTest
  @MethodSource("requiredActionNames")
  void mpc20WrongArguments(String actionName) {
    ContractAbi tokenAbi = ContractStandardsTestUtil.getAbi("token.abi");

    ContractAbi missingActionName =
        ContractStandardsTestUtil.changeActionArguments(tokenAbi, actionName, List.of());
    assertThat(ContractStandards.MPC20.check(missingActionName)).isFalse();
  }

  /** Mpc 20 check succeeds when required fields are found in sub structs. */
  @Test
  void foundInSubStruct() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("sub_struct_token.abi");
    assertThat(ContractStandards.MPC20.check(abi)).isTrue();
  }

  /** Mpc 20 check fails when there exists multiple of a required field in different sub structs. */
  @Test
  void mpc20DuplicateRequiredFields() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("sub_struct_token.abi");

    StructTypeSpec newState =
        new StructTypeSpec(
            "TokenState",
            List.of(
                new FieldAbi("middle_struct_1", new NamedTypeRef(0)),
                new FieldAbi("middle_struct_2", new NamedTypeRef(0))));
    List<NamedTypeSpec> newNamed = new ArrayList<>(abi.namedTypes());
    newNamed.add(newState);
    ContractAbi newAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            newNamed,
            List.of(),
            abi.contractInvocations(),
            new NamedTypeRef(newNamed.size() - 1));

    assertThat(ContractStandards.MPC20.check(newAbi)).isFalse();
  }

  /** Successfully get standard state for abi where required fields are in the root. */
  @Test
  void getStandardState() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");

    Mpc20ContractState state =
        ContractStandards.MPC20.getStandardState(
            ContractStandardsTestUtil.getSerializedState("token.state"), abi);

    assertThat(state.name()).isEqualTo("MyToken");
    assertThat(state.symbol()).isEqualTo("MT");
    assertThat(state.decimals()).isEqualTo((short) 8);
    assertThat(state.balances())
        .hasSize(2)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5"),
                BigInteger.valueOf(20),
                BlockchainAddress.fromString("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed"),
                BigInteger.valueOf(9999980)));
    assertThat(state.allowed()).hasSize(1);
    Map<BlockchainAddress, BigInteger> allowed =
        state
            .allowed()
            .get(BlockchainAddress.fromString("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed"));
    assertThat(allowed)
        .hasSize(1)
        .containsExactly(
            Map.entry(
                BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5"),
                BigInteger.valueOf(1000)));
  }

  /** Successfully get standard state for abi where required fields are in sub structs. */
  @Test
  void getStandardStateSubStruct() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");
    ContractAbi subStructAbi = ContractStandardsTestUtil.getAbi("sub_struct_token.abi");

    Mpc20ContractState state =
        ContractStandards.MPC20.getStandardState(
            ContractStandardsTestUtil.getSerializedState("token.state"), abi);
    Mpc20ContractState subStructState =
        ContractStandards.MPC20.getStandardState(
            ContractStandardsTestUtil.getSerializedState("token.state"), subStructAbi);

    assertThat(state).isEqualTo(subStructState);
  }

  /** Get standard state fails if given abi doesn't follow the standard. */
  @Test
  void getStandardStateWrongError() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token_v2.abi");
    byte[] stateBytes = ContractStandardsTestUtil.getSerializedState("token.state");
    assertThatThrownBy(() -> ContractStandards.MPC20.getStandardState(stateBytes, abi))
        .hasMessageContaining("Contract abi does not follow the MPC20 standard");
  }

  /** Unable to parse standard state when given wrong state bytes. */
  @Test
  void unableToParseState() {
    ContractAbi abi = ContractStandardsTestUtil.getAbi("token.abi");

    assertThatThrownBy(() -> ContractStandards.MPC20.getStandardState(new byte[42], abi))
        .hasMessageContaining("Could not deserialize state.");
  }

  /**
   * Names of the required fields.
   *
   * @return names of the required fields
   */
  private static Stream<String> requiredFieldNames() {
    return Stream.of("name", "symbol", "decimals", "balances", "allowed");
  }

  /**
   * Names of the required actions.
   *
   * @return names of the required actions
   */
  private static Stream<String> requiredActionNames() {
    return Stream.of("transfer", "transfer_from", "approve");
  }
}
