package com.partisiablockchain.language.contractstandard.typematcher;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TypeIndexMatcherTest {

  /** Returns true when matching the correct type. */
  @Test
  void matchPositive() {
    TypeIndexMatcher matcher =
        new TypeIndexMatcher(
            TypeSpec.TypeIndex.Map,
            new TypeIndexMatcher(TypeSpec.TypeIndex.u128),
            TypeIndexMatcher.SIZED_BYTE_ARRAY);
    var mapType =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.u128),
            new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 16));
    assertThat(matcher.matches(mapType, List.of(), List.of())).isTrue();
  }

  /** Returns false when matching an incorrect type. */
  @Test
  void matchNegative() {
    TypeIndexMatcher matcher =
        new TypeIndexMatcher(
            TypeSpec.TypeIndex.Map,
            new TypeIndexMatcher(TypeSpec.TypeIndex.u128),
            TypeIndexMatcher.SIZED_BYTE_ARRAY);
    assertThat(matcher.matches(new SimpleTypeSpec(TypeSpec.TypeIndex.u128), List.of(), List.of()))
        .isFalse();
    var mapType =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.u128),
            new SimpleTypeSpec(TypeSpec.TypeIndex.String));
    assertThat(matcher.matches(mapType, List.of(), List.of())).isFalse();
  }
}
