# contract-standard

Check if a deployed contract follows a given standard. 

Command structure:
```
cargo pbc contract check-standard --format=<standard> <address or abi>
```
