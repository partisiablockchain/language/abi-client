# abi-client

Smart Contract Binary Interface Client Library

## Common testing framework with Typescript
This library shares its tests with the abi-client-ts library.
Read more [here](src/test/java/com/partisiablockchain/language/abiclient/commontests/README.md).