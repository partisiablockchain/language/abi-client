package com.partisiablockchain.language.abiclient.commontests.plugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.AccountPluginInvocationReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class AccountStateLocalTest {

  private AccountPluginInvocationReader reader;

  @BeforeEach
  void setup() {
    reader = new AccountPluginInvocationReader();
  }

  /** 353a767756d85f0e6d336955327d73f8b03a58c17fd591ed0e8d659f1f0a724c - 0x00 - add_coin_balance. */
  @Test
  void readAddCoinBalance() {
    var sender = BlockchainAddress.fromString("002c599c68c7611fe6d68ed1679a4a84efbe0cacec");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "addCoinBalance-6a4c80e6e2712cb0645ffe9bcc896209285ec34e14380b6a3fd8e566799caad5.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("add_coin_balance");
  }

  /**
   * 45d29ab8e40b4d49585c5f2755963ac1570747636c551ba5cc13a0ea29323086 - 0x01 - deduct_coin_balance.
   */
  @Test
  void readDeductCoinBalance() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "deductCoinBalance-f4234f9236ee81a0"
                + "9cba14690674697c1ff3917357380a511ad251c6eaec9489.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("deduct_coin_balance");
  }

  /** 0x02 - faked add_mpc_token_balance. */
  @Test
  void addMpcTokenBalance() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes = TestingHelper.readPayloadFile("faked_add_mpc_token_balance.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("add_mpc_token_balance");
  }

  /** 0x03 - faked deduct_mpc_token_balance. */
  @Test
  void deductMpcTokenBalance() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes = TestingHelper.readPayloadFile("faked_deduct_mpc_token_balance.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("deduct_mpc_token_balance");
  }

  /**
   * 271b4080b4b7929e541d518624790af800ba7517e0264cdf99d63e3543318428 - 0x04 -
   * get_staked_tokens_old.
   */
  @Test
  void getStakedTokenBalance() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "getStakedTokenBalance-271b4080b4b7929e541d"
                + "518624790af800ba7517e0264cdf99d63e3543318428.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(0);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("get_staked_token_balance");
  }

  /** 61af322c5bba589c823764fa4eaefe6c1f1985a1b6e2b2e14bd6ff41f265be40 - 0x05 stake_tokens_old. */
  @Test
  void stakeTokensOld() {
    var sender = BlockchainAddress.fromString("005dc22421d1f634a69848ea8087d1cc7b29cf9b1f");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "stakeTokensOld-61af322c5bba589c823764fa4"
                + "eaefe6c1f1985a1b6e2b2e14bd6ff41f265be40.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("stake_tokens_old");
  }

  /** 36c864f15961d10e872062b6b04e55c8a75209d94978da17883c3e6ad27af8d2 - 0x06 - associate_old. */
  @Test
  void associateOld() {
    var sender = BlockchainAddress.fromString("0064c70ed8d46a777e1b38d87dc60af98e153f144a");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "associateOld-36c864f15961d10e872062b6b0"
                + "4e55c8a75209d94978da17883c3e6ad27af8d2.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("associate_old");
  }

  /**
   * b289f960822679b85985ade00fb492c207b20c37d073aebe1555bd9fc2042dbb - 0x07 -
   * disassociate_all_tokens_for_contract_old.
   */
  @Test
  void disassociateAllTokensForContractOld() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "disassociateAllTokensForContractOld-b289f9"
                + "60822679b85985ade00fb492c207b20c37d073aebe1555bd9fc2042dbb.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("disassociate_all_tokens_for_contract_old");
  }

  /**
   * ed538c53e28a03f2d909d5bc88722634de8f91daff265a3e7ffcc6d0044a4565 - 0x08 - unstake_tokens_old.
   */
  @Test
  void unstakeTokensOld() {
    var sender = BlockchainAddress.fromString("00a62841a8ba87da9b185c733087768d61208e1296");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "unstakeTokensOld-ed538c53e28a03f2d"
                + "909d5bc88722634de8f91daff265a3e7ffcc6d0044a4565.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("unstake_tokens_old");
  }

  /** 7b6550afee56a6f70f8f167cf571616801c1a41edeea6f9ca04749a9cf93d4f0 - 0x0a - burn_mpc_tokens. */
  @Test
  void burnMpcTokens() {
    var sender = BlockchainAddress.fromString("004c2118d2012c41b06f5bafcc1297731afe1a6232");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "burn_mpc_tokens-7b6550afee56a6f70f8f167"
                + "cf571616801c1a41edeea6f9ca04749a9cf93d4f0.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("burn_mpc_tokens");
  }

  /** b0e98eb584c94062b96b6e7ccc0a585747b8ccef8cb87b8756f5fb7594853bed - 0x0c - initiate_deposit. */
  @Test
  void initiateDeposit() {
    var sender = BlockchainAddress.fromString("00e7cf8afad851499ba3396691eab941e1c327e6e3");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiateDeposit-b0e98eb584c94062b96b6"
                + "e7ccc0a585747b8ccef8cb87b8756f5fb7594853bed.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("initiate_deposit");
  }

  /** 9d6a46e48c0d712582b221fe5be4c3e554e27a14aed6567ba2bd7f630499acda - 0x0d - commit_transfer. */
  @Test
  void commitTransfer() {
    var sender = BlockchainAddress.fromString("0022471b311d1637f37c9c0ea8a32192f9e744c31f");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "commit_transfer-9d6a46e48c0d712582b22"
                + "1fe5be4c3e554e27a14aed6567ba2bd7f630499acda.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("commit_transfer");
  }

  /** 02c415d23f7c2f3c996a08f6e0a62ec403c50b6ec41f92828a5c2eff39bdf9dc - 0x0e - abort_transfer. */
  @Test
  void abortTransfer() {
    var sender = BlockchainAddress.fromString("00f1d56f7e2bc33c3e0101bc4d45bd00a7df3dc874");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "abort_transfer-02c415d23f7c2f3c996a08"
                + "f6e0a62ec403c50b6ec41f92828a5c2eff39bdf9dc.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("abort_transfer");
  }

  /**
   * 417e65870060653e838fc16d2cfc38c28adc0f13ce50d2e98e79b513adcb096d - 0x0f -
   * create_vesting_account.
   */
  @Test
  void createVestingAccount() {
    var sender = BlockchainAddress.fromString("00118e7fb9270f0c16f5f65c2cd8a9a003358223a5");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "create_vesting_account-417e65870060653"
                + "e838fc16d2cfc38c28adc0f13ce50d2e98e79b513adcb096d.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("create_vesting_account");
  }

  /**
   * d97dfc1de3e0c6103aefdd7fa584c1b9fa98b2788a6b83cebd554cad9a64c91c - 0x10 -
   * check_vested_tokens_old.
   */
  @Test
  void checkVestedTokensOld() {
    var sender = BlockchainAddress.fromString("00664dc6fee6fb7f92978fd5dbd012db24659c05ae");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "check_vested_tokens_old-d97dfc1de3e0"
                + "c6103aefdd7fa584c1b9fa98b2788a6b83cebd554cad9a64c91c.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(0);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("check_vested_tokens_old");
  }

  /**
   * 1e0f125725a4ce26352c52a4dec1a9bca2d52b885adfb3ac64b346d9217c16e9 - 0x11 -
   * check_pending_unstakes_old.
   */
  @Test
  void checkPendingUnstakesOld() {
    var sender = BlockchainAddress.fromString("00476a2e6cb857f19befd052d72daf939dad8aab2d");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "check_pending_unstakes_old-1e0f12572"
                + "5a4ce26352c52a4dec1a9bca2d52b885adfb3ac64b346d9217c16e9.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(0);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("check_pending_unstakes_old");
  }

  /** 0x18 - faked make_account_non_stakeable. */
  @Test
  void makeAccountNonStakeable() {
    var sender = BlockchainAddress.fromString("00476a2e6cb857f19befd052d72daf939dad8aab2d");
    var invocationBytes = TestingHelper.readPayloadFile("faked_make_account_non_stakeable.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(0);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("make_account_non_stakeable");
  }

  /**
   * f6caac9cbadd112c3ad208fe0c77147905fab37e07cb6f857f8ec27e651d2025 - 0x15 -
   * initiate_delegate_stakes_old.
   */
  @Test
  void initiateDelegateStakesOld() {
    var sender = BlockchainAddress.fromString("00a9062b40eb2dde7f243d77b571fb822967280115");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_delegate_stakes_old-f6caac"
                + "9cbadd112c3ad208fe0c77147905fab37e07cb6f857f8ec27e651d2025.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_delegate_stakes_old");
  }

  /**
   * 9dd57975ed7818873e094bbca864ae8ded4b631a3ac3ffaa8f13ec0ead0de601 - 0x16 -
   * initiate_receive_delegated_stakes.
   */
  @Test
  void initiateReceiveDelegatedStakes() {
    var sender = BlockchainAddress.fromString("006679b71f43483a402f79719f1ba6a9b8a2ced062");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_receive_delegated_stakes-9dd"
                + "57975ed7818873e094bbca864ae8ded4b631a3ac3ffaa8f13ec0ead0de601.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_receive_delegated_stakes");
  }

  /**
   * 100657078aadda8df7079212934109ca001a6a877b655dab49c7b4645a6d4eca - 0x17 -
   * initiate_retract_delegated_stakes_old.
   */
  @Test
  void initiateRetractDelegatedStakesOld() {
    var sender = BlockchainAddress.fromString("00959095b44083c071c9bc578cd9dac2a09c984ec4");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_retract_delegated_stakes_old-"
                + "100657078aadda8df7079212934109ca001a6a877b655dab49c7b4645a6d4eca.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_retract_delegated_stakes_old");
  }

  /**
   * 77ab9b89d178cdc5fc631e9c534a67ad88ff2115ff009f8eb703d94e077e5995 - 0x18 -
   * initiate_return_delegated_stakes.
   */
  @Test
  void initiateReturnDelegatedStakes() {
    var sender = BlockchainAddress.fromString("006679b71f43483a402f79719f1ba6a9b8a2ced062");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_return_delegated_stakes-"
                + "77ab9b89d178cdc5fc631e9c534a67ad88ff2115ff009f8eb703d94e077e5995.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_return_delegated_stakes");
  }

  /**
   * 6dabc4927f724187c9f3472b91d8f71594eb5763e110185ca1a5710f1848f7ac - 0x19 -
   * commit_delegated_stakes.
   */
  @Test
  void commitDelegateStakes() {
    var sender = BlockchainAddress.fromString("00d8419628326c2a112ecbe2869a59ce16cb9bf98e");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "commit_delegate_stakes-"
                + "6dabc4927f724187c9f3472b91d8f71594eb5763e110185ca1a5710f1848f7ac.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("commit_delegate_stakes");
  }

  /**
   * e84be86f2a12da3f21e987b9173dc1a1b3053310f4dabe30af0df7139d235d08 - 0x1a -
   * abort_delegate_stakes.
   */
  @Test
  void abortDelegateStakes() {
    var sender = BlockchainAddress.fromString("006c6546e22aa72b7b2ea90b1ceefe6bb2e0e6f791");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "abort_delegate_stakes-"
                + "e84be86f2a12da3f21e987b9173dc1a1b3053310f4dabe30af0df7139d235d08.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("abort_delegate_stakes");
  }

  /**
   * d6b8e27b26dff3a81ad94c0b00adc1fb1ca3b9d3abee3a85f77d5399904617ca - 0x1b -
   * accept_delegated_stakes_old.
   */
  @Test
  void acceptDelegatedStakesOld() {
    var sender = BlockchainAddress.fromString("007a2a9a4eff7cc34bb4150b83c15d5f792d2158aa");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "accept_delegated_stakes_old-"
                + "d6b8e27b26dff3a81ad94c0b00adc1fb1ca3b9d3abee3a85f77d5399904617ca.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("accept_delegated_stakes_old");
  }

  /**
   * 58d9d3d7d637af9b0c8f97570a1b928c782ca8afe5dd09e6b37354b169e9a381 - 0x1c -
   * reduce_delegated_stakes_old.
   */
  @Test
  void reduceDelegatedStakesOld() {
    var sender = BlockchainAddress.fromString("00f9bbefae773b5a9aec5a01e0005139af1dddf7ed");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "reduce_delegated_stakes_old-"
                + "58d9d3d7d637af9b0c8f97570a1b928c782ca8afe5dd09e6b37354b169e9a381.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("reduce_delegated_stakes_old");
  }

  /**
   * 29210a058b4c89f8d93a1b0d773aa62624222e26ac9a40a6920bfdcc87495124 - 0x1f -
   * initiate_byoc_transfer_sender_old.
   */
  @Test
  void initiateByocTransferSenderOld() {
    var sender = BlockchainAddress.fromString("00527092bfb4b35a0331fe066199a41d45c213c368");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_byoc_transfer_sender_old-"
                + "29210a058b4c89f8d93a1b0d773aa62624222e26ac9a40a6920bfdcc87495124.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_byoc_transfer_sender_old");
  }

  /**
   * 75611c35cfc2d95d7ce03c7202985314bda4473a3337ace7845447a3bc65dec8 - 0x20 -
   * initiate_byoc_transfer_recipient.
   */
  @Test
  void initiateByocTransferRecipient() {
    var sender = BlockchainAddress.fromString("00be79ef0d3420be7c401f92f8a07c37138a367632");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_byoc_transfer_recipient-"
                + "75611c35cfc2d95d7ce03c7202985314bda4473a3337ace7845447a3bc65dec8.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_byoc_transfer_recipient");
  }

  /** 4149e629f96c3a6b2a8ce6c56ad2c7518b08bb2b7f709cef72dcf51533f232fe - 0x21 - associate. */
  @Test
  void associate() {
    var sender = BlockchainAddress.fromString("00fc28a78dea16c765b3d5b52f34faa94861d52f9b");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "associate-" + "4149e629f96c3a6b2a8ce6c56ad2c7518b08bb2b7f709cef72dcf51533f232fe.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("associate");
  }

  /** e70636a9b1e9f2d354be1b666d3610685f9f44df1ebe907de013cf044f5b93e8 - 0x22 - disassociate. */
  @Test
  void disassociate() {
    var sender = BlockchainAddress.fromString("001d112b141056df59222685dd37dc919f0efa4d59");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "disassociate-"
                + "e70636a9b1e9f2d354be1b666d3610685f9f44df1ebe907de013cf044f5b93e8.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("disassociate");
  }

  /**
   * a381dd2d968477f2eeab565a96c33014b6528fea933430b0fffe8ee4f5e4d1c1 - 0x27 - appoint_custodian.
   */
  @Test
  void appointCustodian() {
    var sender = BlockchainAddress.fromString("004f82f870ecc9d5e595956edfe438f86741a94fbf");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "appoint_custodian-"
                + "a381dd2d968477f2eeab565a96c33014b6528fea933430b0fffe8ee4f5e4d1c1.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("appoint_custodian");
  }

  /**
   * bb0014b46d3760c2e5b757cac05c5398fc354c08d838ba5c34fae654ec790809 - 0x28 -
   * cancel_appointed_custodian.
   */
  @Test
  void cancelAppointedCustodian() {
    var sender = BlockchainAddress.fromString("00d40529fb4415a20791a39d4695e6d38f901b0166");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "cancel_appointed_custodian-"
                + "bb0014b46d3760c2e5b757cac05c5398fc354c08d838ba5c34fae654ec790809.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("cancel_appointed_custodian");
  }

  /** ca580fe56295cd82824f565b0064e67de1984c75356e1d879a18c260fd9933f1 - 0x29 - reset_custodian. */
  @Test
  void resetCustodian() {
    var sender = BlockchainAddress.fromString("008f3485af6ee219dd3094e205dba1b902ee3ab5e9");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "reset_custodian-"
                + "ca580fe56295cd82824f565b0064e67de1984c75356e1d879a18c260fd9933f1.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("reset_custodian");
  }

  /**
   * 81d7bdd7dfa40073a49f9658d9d20c0ed6fdfc9a24cf20f9cbb9cd006e59145e - 0x2a - accept_custodianship.
   */
  @Test
  void acceptCustodianship() {
    var sender = BlockchainAddress.fromString("004f82f870ecc9d5e595956edfe438f86741a94fbf");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "accept_custodianship-"
                + "81d7bdd7dfa40073a49f9658d9d20c0ed6fdfc9a24cf20f9cbb9cd006e59145e.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("accept_custodianship");
  }

  /** 545c145392d9d06f7bf93e134e32df49968ef98d92b22917160a780c63ddeb21 - 0x2e - stake_tokens. */
  @Test
  void stakeTokens() {
    var sender = BlockchainAddress.fromString("00fd22d65f98eb5a7bb4aa633d9560e94b11a1ebc3");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "stake_tokens-"
                + "545c145392d9d06f7bf93e134e32df49968ef98d92b22917160a780c63ddeb21.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("stake_tokens");
  }

  /** 4ee854ae0f3e24fbbb8196b374599d40abd28b6c2173ec5de411528ac2eb5c9d - 0x2f - unstake_tokens. */
  @Test
  void unstakeTokens() {
    var sender = BlockchainAddress.fromString("00fd22d65f98eb5a7bb4aa633d9560e94b11a1ebc3");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "unstake_tokens-"
                + "4ee854ae0f3e24fbbb8196b374599d40abd28b6c2173ec5de411528ac2eb5c9d.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("unstake_tokens");
  }

  /**
   * ab4bd79bccc50098c79a296de7f109d5a0774e54e3393828ac5f4afe6338a8d4 - 0x31 - initiate_withdraw.
   */
  @Test
  void initiateWithdraw() {
    var sender = BlockchainAddress.fromString("00ae10746323413d63a99baf8277335eed70590bd8");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_withdraw-"
                + "ab4bd79bccc50098c79a296de7f109d5a0774e54e3393828ac5f4afe6338a8d4.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("initiate_withdraw");
  }

  /**
   * 80802fa94663f5c19373acf477a696331d136dc5378be77d2d9eec2509431695 - 0x35 - check_vested_tokens.
   */
  @Test
  void checkVestedTokens() {
    var sender = BlockchainAddress.fromString("00dac63fbb760d82c1c05cdf7c5074ed622fcd358d");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "check_vested_tokens-"
                + "80802fa94663f5c19373acf477a696331d136dc5378be77d2d9eec2509431695.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("check_vested_tokens");
  }

  /**
   * e88df53cd8ce06303414a8970712c2acf40190b1a7fe0d645ec5bb3c88730ffc - 0x36 -
   * check_pending_unstakes.
   */
  @Test
  void checkPendingUnstakes() {
    var sender = BlockchainAddress.fromString("00fcfec3affe0d1b6f6655795cfe3d99cfa26abb37");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "check_pending_unstakes-"
                + "e88df53cd8ce06303414a8970712c2acf40190b1a7fe0d645ec5bb3c88730ffc.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("check_pending_unstakes");
  }

  /**
   * 07b5c5804e78804f466e3a7dc2178315c789b2fe761d065ce8e7e4ca458bdafe - 0x37 -
   * initiate_delegate_stakes.
   */
  @Test
  void initiateDelegateStakes() {
    var sender = BlockchainAddress.fromString("00d8419628326c2a112ecbe2869a59ce16cb9bf98e");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_delegate_stakes-"
                + "07b5c5804e78804f466e3a7dc2178315c789b2fe761d065ce8e7e4ca458bdafe.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_delegate_stakes");
  }

  /**
   * 48375079d0c8287d3b35b5f43ba4f6de522cf198ca9c5a7add8d94de433f5374 - 0x39 -
   * initiate_retract_delegated_stakes.
   */
  @Test
  void initiateRetractDelegatedStakes() {
    var sender = BlockchainAddress.fromString("00dac63fbb760d82c1c05cdf7c5074ed622fcd358d");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_retract_delegated_stakes-"
                + "48375079d0c8287d3b35b5f43ba4f6de522cf198ca9c5a7add8d94de433f5374.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_retract_delegated_stakes");
  }

  /**
   * 82fc64fe5b69e4ee952571814a5afd66e90ac51ddf49c00d480f265377d1a6b0 - 0x3d -
   * accept_delegated_stakes.
   */
  @Test
  void acceptDelegatedStakes() {
    var sender = BlockchainAddress.fromString("006679b71f43483a402f79719f1ba6a9b8a2ced062");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "accept_delegated_stakes-"
                + "82fc64fe5b69e4ee952571814a5afd66e90ac51ddf49c00d480f265377d1a6b0.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("accept_delegated_stakes");
  }

  /**
   * be990092c37bd5a0a61e1b35ef1fc889b4ff70defe18c7859b90ef04eccd9ae8 - 0x3e -
   * reduce_delegated_stakes.
   */
  @Test
  void reduceDelegatedStakes() {
    var sender = BlockchainAddress.fromString("006679b71f43483a402f79719f1ba6a9b8a2ced062");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "reduce_delegated_stake-b"
                + "e990092c37bd5a0a61e1b35ef1fc889b4ff70defe18c7859b90ef04eccd9ae8.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("reduce_delegated_stakes");
  }

  /**
   * e143eab9a7a11d2119f911f99255f3695421d9afc306aca0409f715cabd5cf81 - 0x3f -
   * initiate_byoc_transfer_sender.
   */
  @Test
  void readInitiateByocTransfer() {
    var sender = BlockchainAddress.fromString("0022471b311d1637f37c9c0ea8a32192f9e744c31f");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiateByocTransfer-"
                + "e143eab9a7a11d2119f911f99255f3695421d9afc306aca0409f715cabd5cf81.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_byoc_transfer_sender");
  }
}
