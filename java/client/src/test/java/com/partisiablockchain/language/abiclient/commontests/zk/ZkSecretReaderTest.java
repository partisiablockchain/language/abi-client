package com.partisiablockchain.language.abiclient.commontests.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.zk.ZkSecretReader;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.secata.stream.BitInput;
import com.secata.stream.BitOutput;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ZkSecretReaderTest {

  /** The Zk reader can be created with a CompactBitArray. */
  @Test
  void deserializeCompactBitArray() {
    var bits =
        BitOutput.serializeBits(
            bitOutput -> {
              var wrap = new AbiBitOutput(bitOutput);
              wrap.writeBoolean(true);
            });

    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var reader = new ZkSecretReader(bits, contractAbi);

    assertThat(reader.readSecret(new SimpleTypeSpec(TypeSpec.TypeIndex.bool)).boolValue())
        .isEqualTo(true);
  }

  /** Deserialize packed secret booleans. */
  @Test
  void deserializeSecretBoolean() {
    var bits =
        BitOutput.serializeBits(
            bitOutput -> {
              var wrap = new AbiBitOutput(bitOutput);
              wrap.writeBoolean(true);
              wrap.writeBoolean(false);
              wrap.writeBoolean(true);
            });

    var structType =
        new StructTypeSpec(
            "PackedBooleans",
            List.of(
                new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                new FieldAbi("f2", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                new FieldAbi("f3", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))));

    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structType),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var reader = new ZkSecretReader(BitInput.create(bits.data()), contractAbi);

    ScValueStruct value = reader.readSecret(new NamedTypeRef(0)).structValue();
    assertThat(requireNonNull(value.getFieldValue("f1")).boolValue()).isEqualTo(true);
    assertThat(requireNonNull(value.getFieldValue("f2")).boolValue()).isEqualTo(false);
    assertThat(requireNonNull(value.getFieldValue("f3")).boolValue()).isEqualTo(true);
  }

  /** Deserialize a secret I8. */
  @Test
  void deserializeSecretI8() {
    var bits =
        BitOutput.serializeBits(
            bitOutput -> {
              var wrap = new AbiBitOutput(bitOutput);
              wrap.writeI8((byte) 12);
            });

    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var reader = new ZkSecretReader(BitInput.create(bits.data()), contractAbi);

    assertThat(reader.readSecret(new SimpleTypeSpec(TypeSpec.TypeIndex.i8)).asInt()).isEqualTo(12);
  }

  /** Deserialize a secret struct, with multiple fields. */
  @Test
  void deserializeSecretStruct() {
    var bits =
        BitOutput.serializeBits(
            bitOutput -> {
              var wrap = new AbiBitOutput(bitOutput);
              wrap.writeI8((byte) 12);
              wrap.writeI32(100000);
            });

    var secretStruct =
        new StructTypeSpec(
            "SecretType",
            List.of(
                new FieldAbi("identifier", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)),
                new FieldAbi("salary", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))));
    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(secretStruct),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var reader = new ZkSecretReader(BitInput.create(bits.data()), contractAbi);
    var secretValue = reader.readSecret(new NamedTypeRef(0));

    assertThat(secretValue.structValue().name()).isEqualTo("SecretType");
    assertThat(requireNonNull(secretValue.structValue().getFieldValue("identifier")).asInt())
        .isEqualTo(12);
    assertThat(requireNonNull(secretValue.structValue().getFieldValue("salary")).asInt())
        .isEqualTo(100000);
  }

  /** The secret reader cannot deserialize a Map, since maps are not allowed as secret variables. */
  @Test
  void unsupportedMapType() {

    var bytes = new byte[] {};
    var abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var reader = new ZkSecretReader(BitInput.create(bytes), abi);

    assertThatThrownBy(
            () ->
                reader.readSecret(
                    new MapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.String),
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u64))))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Type Map is not supported in secret variables");
  }

  /** The secret reader cannot deserialize a Set, since sets are not allowed as secret variables. */
  @Test
  void unsupportedSetType() {

    var abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.i8));
    var bytes = new byte[] {};
    var reader = new ZkSecretReader(BitInput.create(bytes), abi);

    assertThatThrownBy(
            () -> reader.readSecret(new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String))))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Type Set is not supported in secret variables");
  }

  /**
   * The secret reader cannot deserialize an AvlTree, since avlTrees are not allowed as secret
   * variables.
   */
  @Test
  void unsupportedAvlTreeType() {

    var abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var bytes = new byte[] {};
    var reader = new ZkSecretReader(BitInput.create(bytes), abi);

    assertThatThrownBy(
            () ->
                reader.readSecret(
                    new AvlTreeMapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.String),
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u64))))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Type AvlTreeMap is not supported in secret variables");
  }
}
