package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValueU64;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import org.junit.jupiter.api.Test;

final class ScValueOptionTest {

  private final ScValueOption some = new ScValueOption(new ScValueU8((byte) 2));
  private final ScValueOption none = new ScValueOption(null);

  @Test
  void type() {
    assertThat(some.getType()).isEqualTo(TypeSpec.TypeIndex.Option);
    assertThat(none.getType()).isEqualTo(TypeSpec.TypeIndex.Option);
  }

  @Test
  void typeTest() {
    ScValueOption some = new ScValueOption(new ScValueBool(true));
    ScValueOption none = new ScValueOption(null);
    assertThat(some.isSome()).isTrue();
    assertThat(none.isSome()).isFalse();
    assertThat(some.getType()).isEqualTo(TypeSpec.TypeIndex.Option);
    assertThat(none.getType()).isEqualTo(TypeSpec.TypeIndex.Option);
  }

  @Test
  public void testOptionSome() {
    var argument = new ScValueU64(17);
    ScValueOption some = new ScValueOption(argument);
    assertThat(some.isSome()).isTrue();
    assertThat(some.innerValue()).isEqualTo(argument);
  }

  @Test
  void valueOrNull() {
    var some = new ScValueOption(new ScValueI32(42));
    var result = some.valueOrNull(sc -> sc.asInt() * 2);
    assertThat(result).isEqualTo(84);
    var none = new ScValueOption(null);
    var result2 = none.valueOrNull(sc -> sc.asInt() * 2);
    assertThat(result2).isNull();
  }
}
