package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.StateReaderHelper;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

final class ImpossibleTsTest {

  @Test
  void readStateValueFail() {
    assertThatThrownBy(() -> StateReaderHelper.wrap("fe").readStateValue(null))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void readSimpleInvalid() {
    assertThatThrownBy(() -> StateReaderHelper.wrap("ff").readSimpleType(TypeSpec.TypeIndex.Vec))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void getFieldByName() {
    var field = new FieldAbi("field0", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var struct = new StructTypeSpec("name", List.of(field));
    assertThat(struct.getFieldByName("asdf")).isNull();
    assertThat(struct.getFieldByName("field0")).isSameAs(field);
  }

  @Test
  void scValueAddressTestHashCode() {
    var rawValue1 = HexFormat.of().parseHex("030000000000000000000000000000000000000020");
    var rawValue2 = HexFormat.of().parseHex("040000000000000000000000000000000000000020");

    var address1 = new ScValueAddress(rawValue1);
    var address2 = new ScValueAddress(rawValue2);

    assertThat(address1.hashCode()).isEqualTo(new ScValueAddress(rawValue1).hashCode());
    assertThat(address1.hashCode()).isNotEqualTo(address2.hashCode());
  }

  @Test
  void scValueArrayTestHashCode() {
    var one = ScValueVector.fromBytes(new byte[] {1, 2, 3, 4});
    var two = ScValueVector.fromBytes(new byte[] {2, 3, 4, 5});

    assertThat(one.hashCode()).isNotEqualTo(two.hashCode());
  }

  @Test
  void scValueSetIterator() {
    var val = new ScValueSet(List.of(new ScValueI32(2), new ScValueI32(4)));
    var iterator = val.iterator();
    assertThat(iterator.next().asInt()).isEqualTo(2);
    assertThat(iterator.next().asInt()).isEqualTo(4);
  }

  @Test
  void iterator() {
    var val = new ScValueVector(List.of(new ScValueI32(2), new ScValueI32(3)));
    var iterator = val.iterator();
    assertThat(iterator.next().asInt()).isEqualTo(2);
    assertThat(iterator.next().asInt()).isEqualTo(3);
  }

  @Test
  void getValue() {
    var index = TypeSpec.TypeIndex.u8;
    assertThat(index.getValue()).isEqualTo(1);
  }

  @Test
  void scValueAddressEquals() {
    var rawValue1 = HexFormat.of().parseHex("010000000000000000000000000000000000000020");
    var rawValue2 = HexFormat.of().parseHex("020000000000000000000000000000000000000020");

    EqualsVerifier.forClass(ScValueAddress.class)
        .withNonnullFields("bytes")
        .withPrefabValues(byte[].class, rawValue1, rawValue2)
        .verify();
  }
}
