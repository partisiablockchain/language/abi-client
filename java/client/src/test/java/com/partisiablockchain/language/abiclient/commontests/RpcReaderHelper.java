package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;

/** Helper class for rpc tests. */
public final class RpcReaderHelper {

  /**
   * Helper function for wrapping a hex string into a RpcReader under a given model.
   *
   * @param rpcHex The hex string to wrap
   * @param fileAbi The model
   * @return The RpcReader for the given hex string
   */
  public static RpcReader wrap(String rpcHex, FileAbi fileAbi) {
    return RpcReader.create(HexFormat.of().parseHex(rpcHex), fileAbi, TransactionKind.Action);
  }

  /**
   * Helper function for wrapping a hex string into a RpcReader with a single argument with the
   * given type.
   *
   * @param rpcHex The hex string to wrap
   * @param typeSpec The type of the argument
   * @return The RpcReader for the given hex string
   */
  public static RpcReader wrap(String rpcHex, TypeSpec typeSpec) {
    var version = new AbiVersion(4, 0, 0);
    return wrap(rpcHex, typeSpec, version);
  }

  /**
   * Helper function for wrapping a hex string into a RpcReader with a single argument with the
   * given type and configuration.
   *
   * @param rpcHex The hex string to wrap
   * @param typeSpec The type of the argument
   * @param version the binder and client version for the FileAbi
   * @return The RpcReader for the given hex string
   */
  public static RpcReader wrap(String rpcHex, TypeSpec typeSpec, AbiVersion version) {
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "test",
                    HexFormat.of().parseHex("dddddd0d"),
                    List.of(new ArgumentAbi("valueType", typeSpec)))),
            new NamedTypeRef(0));
    var fileAbi = new FileAbi("PBCABI", version, version, null, contract);
    return wrap(rpcHex, fileAbi);
  }
}
