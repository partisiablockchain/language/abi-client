package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueI128;
import com.partisiablockchain.language.abiclient.value.ScValueI16;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueI8;
import com.partisiablockchain.language.abiclient.value.ScValueU128;
import com.partisiablockchain.language.abiclient.value.ScValueU16;
import com.partisiablockchain.language.abiclient.value.ScValueU256;
import com.partisiablockchain.language.abiclient.value.ScValueU32;
import com.partisiablockchain.language.abiclient.value.ScValueU64;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Tests for all StateValue representation of number values. */
final class ScValueNumberTest {

  ScValueU8 u8Min = new ScValueU8((byte) 0);
  ScValueU8 u8Max = new ScValueU8((byte) 255);
  ScValueI8 i8Min = new ScValueI8(Byte.MIN_VALUE);
  ScValueI8 i8Max = new ScValueI8(Byte.MAX_VALUE);

  ScValueU16 u16Min = new ScValueU16((short) 0);
  ScValueU16 u16Max = new ScValueU16((short) 65535);
  ScValueI16 i16Min = new ScValueI16(Short.MIN_VALUE);
  ScValueI16 i16Max = new ScValueI16(Short.MAX_VALUE);

  ScValueU32 u32Min = new ScValueU32(0);
  ScValueU32 u32Max = new ScValueU32(Integer.parseUnsignedInt("4294967295"));
  ScValueI32 i32Min = new ScValueI32(Integer.MIN_VALUE);
  ScValueI32 i32Max = new ScValueI32(Integer.MAX_VALUE);

  ScValueU64 u64Min = new ScValueU64(0L);
  ScValueU64 u64Max = new ScValueU64(Long.parseUnsignedLong("18446744073709551615"));
  ScValueI64 i64Min = new ScValueI64(Long.MIN_VALUE);
  ScValueI64 i64Max = new ScValueI64(Long.MAX_VALUE);

  ScValueU128 u128Min = new ScValueU128(BigInteger.ZERO);
  ScValueU128 u128Max = new ScValueU128(new BigInteger("340282366920938463463374607431768211455"));
  ScValueI128 i128Min = new ScValueI128(new BigInteger("-170141183460469231731687303715884105728"));
  ScValueI128 i128Max = new ScValueI128(new BigInteger("170141183460469231731687303715884105728"));

  ScValueU256 u256Min = new ScValueU256(BigInteger.ZERO);
  ScValueU256 u256Max =
      new ScValueU256(
          new BigInteger(
              "115792089237316195423570985008687907853269984665640564039457584007913129639935"));

  @Test
  void testU8() {
    var u8 = new ScValueU8((byte) 100);
    assertThat(u8.u8Value()).isEqualTo((byte) 100);

    assertThat(u8Min.u8Value()).isEqualTo((byte) 0);
    assertThat(u8Max.u8Value()).isEqualTo((byte) 255);

    assertThat(u8Min.asInt()).isEqualTo(0);
    assertThat(u8Max.asInt()).isEqualTo(255);

    assertThat(u8Min.asLong()).isEqualTo(0L);
    assertThat(u8Max.asLong()).isEqualTo(255);

    assertThat(u8Max.asBigInteger()).isEqualTo(BigInteger.valueOf(255));
  }

  @Test
  void testI8() {
    assertThat(i8Min.i8Value()).isEqualTo((byte) -128);
    assertThat(i8Max.i8Value()).isEqualTo((byte) 127);

    assertThat(i8Min.asInt()).isEqualTo(Byte.MIN_VALUE);
    assertThat(i8Max.asLong()).isEqualTo(Byte.MAX_VALUE);
    assertThat(i8Max.asBigInteger()).isEqualTo(BigInteger.valueOf(Byte.MAX_VALUE));
  }

  @Test
  void testU16() {
    var value = new ScValueU16((short) 1234);
    assertThat(value.u16Value()).isEqualTo((short) 1234);
    assertThat(value.asInt()).isEqualTo(1234);

    assertThat(u16Min.u16Value()).isEqualTo((short) 0);
    assertThat(u16Max.u16Value()).isEqualTo((short) 65535);

    assertThat(u16Min.asInt()).isEqualTo(0);
    assertThat(u16Max.asInt()).isEqualTo(65535);
    assertThat(u16Min.asLong()).isEqualTo(0L);
    assertThat(u16Max.asLong()).isEqualTo(65535L);
    assertThat(u16Max.asBigInteger()).isEqualTo(BigInteger.valueOf(65535));
  }

  @Test
  void testI16() {
    assertThat(i16Min.i16Value()).isEqualTo(Short.MIN_VALUE);
    assertThat(i16Max.i16Value()).isEqualTo(Short.MAX_VALUE);

    assertThat(i16Min.asInt()).isEqualTo(Short.MIN_VALUE);
    assertThat(i16Max.asLong()).isEqualTo(Short.MAX_VALUE);
    assertThat(i16Max.asBigInteger()).isEqualTo(BigInteger.valueOf(Short.MAX_VALUE));
  }

  @Test
  void testU32() {
    var value = new ScValueU32(54321);
    assertThat(value.u32Value()).isEqualTo(54321);
    assertThat(value.asLong()).isEqualTo(54321L);

    assertThat(u32Min.u32Value()).isEqualTo(0);
    assertThat(u32Min.asLong()).isEqualTo(0);
    assertThat(u32Max.u32Value()).isEqualTo(Integer.parseUnsignedInt("4294967295"));

    assertThat(u32Max.asLong()).isEqualTo(4294967295L);
    assertThat(u32Max.asBigInteger()).isEqualTo(BigInteger.valueOf(4294967295L));
  }

  @Test
  void testI32() {
    assertThat(i32Min.i32Value()).isEqualTo(Integer.MIN_VALUE);
    assertThat(i32Max.i32Value()).isEqualTo(Integer.MAX_VALUE);

    assertThat(i32Min.asInt()).isEqualTo(Integer.MIN_VALUE);
    assertThat(i32Max.asLong()).isEqualTo(Integer.MAX_VALUE);
    assertThat(i32Max.asBigInteger()).isEqualTo(BigInteger.valueOf(Integer.MAX_VALUE));
  }

  @Test
  void testU64() {
    assertThat(u64Min.u64Value()).isEqualTo(0);
    assertThat(u64Max.u64Value()).isEqualTo(0xffff_ffff_ffff_ffffL);
    assertThat(u64Max.asBigInteger()).isEqualTo(new BigInteger("18446744073709551615"));
  }

  @Test
  void testI64() {
    assertThat(i64Min.i64Value()).isEqualTo(Long.MIN_VALUE);
    assertThat(i64Max.i64Value()).isEqualTo(Long.MAX_VALUE);

    assertThatThrownBy(() -> i64Min.asInt()).isInstanceOf(ScReadException.class);
    assertThat(i64Max.asLong()).isEqualTo(Long.MAX_VALUE);
    assertThat(i64Max.asBigInteger()).isEqualTo(BigInteger.valueOf(Long.MAX_VALUE));
  }

  @Test
  void testU128() {
    assertThat(u128Min.u128Value()).isEqualTo(BigInteger.ZERO);
    assertThat(u128Max.u128Value())
        .isEqualTo(new BigInteger("340282366920938463463374607431768211455"));

    assertThatThrownBy(
            () -> new ScValueU128(new BigInteger("340282366920938463463374607431768211456")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value specified for u128 exceeds allowed range of unsigned 128-bit value");

    assertThatThrownBy(() -> new ScValueU128(new BigInteger("-1")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value specified for u128 exceeds allowed range of unsigned 128-bit value");
  }

  @Test
  void testI128() {
    assertThat(i128Min.i128Value())
        .isEqualTo(new BigInteger("-170141183460469231731687303715884105728"));
    assertThat(i128Max.i128Value())
        .isEqualTo(new BigInteger("170141183460469231731687303715884105728"));

    assertThatThrownBy(
            () -> new ScValueI128(new BigInteger("-170141183460469231731687303715884105729")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Specified value for i128 exceeds allowed range for signed 128-bit value");
    assertThatThrownBy(
            () -> new ScValueI128(new BigInteger("170141183460469231731687303715884105729")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Specified value for i128 exceeds allowed range for signed 128-bit value");

    assertThat(i128Min.asBigInteger())
        .isEqualTo(new BigInteger("-170141183460469231731687303715884105728"));
  }

  @Test
  void testU256() {
    assertThat(u256Min.u256Value()).isEqualTo(BigInteger.ZERO);
    assertThat(u256Max.u256Value())
        .isEqualTo(
            new BigInteger(
                "115792089237316195423570985008687907853269984665640564039457584007913129639935"));

    assertThatThrownBy(
            () ->
                new ScValueU256(
                    new BigInteger(
                        "1157920892373161954235709850086879078"
                            + "53269984665640564039457584007913129639936")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value specified for u256 exceeds allowed range of unsigned 256-bit value");

    assertThatThrownBy(() -> new ScValueU256(new BigInteger("-1")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value specified for u256 exceeds allowed range of unsigned 256-bit value");
  }

  @Test
  void assertTypes() {
    assertThat(u8Max.getType()).isEqualTo(TypeSpec.TypeIndex.u8);
    assertThat(i8Max.getType()).isEqualTo(TypeSpec.TypeIndex.i8);

    assertThat(u16Max.getType()).isEqualTo(TypeSpec.TypeIndex.u16);
    assertThat(i16Max.getType()).isEqualTo(TypeSpec.TypeIndex.i16);

    assertThat(u32Max.getType()).isEqualTo(TypeSpec.TypeIndex.u32);
    assertThat(i32Max.getType()).isEqualTo(TypeSpec.TypeIndex.i32);

    assertThat(u64Max.getType()).isEqualTo(TypeSpec.TypeIndex.u64);
    assertThat(i64Max.getType()).isEqualTo(TypeSpec.TypeIndex.i64);

    assertThat(u128Max.getType()).isEqualTo(TypeSpec.TypeIndex.u128);
    assertThat(i128Max.getType()).isEqualTo(TypeSpec.TypeIndex.i128);

    assertThat(u256Max.getType()).isEqualTo(TypeSpec.TypeIndex.u256);
  }

  @Test
  void testFromReader() {
    var value = valueFromBytes("ffffffff", TypeSpec.TypeIndex.u32);
    assertThat(value.u32Value()).isEqualTo(0xffff_ffff);
    assertThat(value.asLong()).isEqualTo(4294967295L);

    var zero = valueFromBytes("00000000", TypeSpec.TypeIndex.u32);
    assertThat(zero.u32Value()).isEqualTo(0);
    assertThat(zero.asLong()).isEqualTo(0L);

    var positive = valueFromBytes("ffffff7f", TypeSpec.TypeIndex.u32);
    assertThat(positive.asLong()).isEqualTo(Integer.MAX_VALUE);
  }

  private ScValue valueFromBytes(String hexBytes, TypeSpec.TypeIndex type) {
    byte[] in = HexFormat.of().parseHex(hexBytes);
    StateReader reader = StateReader.create(in, null);
    return reader.readSimpleType(type);
  }
}
