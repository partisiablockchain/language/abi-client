package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.BuilderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class MapRpcProducerTest {

  @Test
  public void assertTypeError() {
    var mapType =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.String),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

    var arg = new ArgumentAbi("valueType", mapType);
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "action", new byte[1], List.of(arg));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(fn), new NamedTypeRef(0));
    var builder = new RpcContractBuilder(simpleContractAbi, "action");
    assertThatThrownBy(() -> builder.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In action/valueType, Expected type Map, but got bool");

    var mapStateProducer = builder.addMap();
    mapStateProducer.addString("test");
    mapStateProducer.addU64(42);
    assertThatThrownBy(() -> mapStateProducer.addMap().addStruct())
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In action/valueType/1/key, Expected type String, but got Map");

    assertThatThrownBy(() -> mapStateProducer.addString("test").addStruct())
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In action/valueType/1/value, Expected type u64, but got Named");
  }

  @Test
  public void testMap() {
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_map.abi");

    var structProducer = new StateBuilder(contractAbi);
    var mapStateProducer = structProducer.addMap();
    mapStateProducer.addAddress("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff").addU8((byte) 0);
    mapStateProducer.addAddress("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd").addU8((byte) 1);

    byte[] state = structProducer.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("02000000"),
            HexFormat.of().parseHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
            HexFormat.of().parseHex("00"),
            HexFormat.of().parseHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd"),
            HexFormat.of().parseHex("01"));
    assertThat(state).isEqualTo(expected);

    // Testing with no type checking
    var producerNoTypeCheck = new StateBuilder(null);
    var mapStateProducerNoTypeCheck = producerNoTypeCheck.addMap();
    mapStateProducerNoTypeCheck
        .addAddress("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff")
        .addU8((byte) 0);
    mapStateProducerNoTypeCheck
        .addAddress("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd")
        .addU8((byte) 1);
    byte[] stateNoTypeCheck = BuilderHelper.builderToBytesLe(mapStateProducerNoTypeCheck);
    assertThat(stateNoTypeCheck).isEqualTo(expected);
  }

  @Test
  void missingValueWrite() {
    var producer = new RpcContractBuilder(new byte[] {1});
    producer.addMap().addString("k1");
    assertThatThrownBy(producer::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In , Missing value for key");
  }
}
