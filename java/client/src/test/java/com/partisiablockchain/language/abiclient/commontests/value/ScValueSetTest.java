package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ScValueSetTest {

  private final ScValueSet val = new ScValueSet(List.of(new ScValueI32(2), new ScValueI32(4)));

  @Test
  void value() {
    assertThat(val.size()).isEqualTo(2); // commontests-ignore-array
  }

  @Test
  void getType() {
    assertThat(val.getType()).isEqualTo(TypeSpec.TypeIndex.Set);
  }

  @Test
  void getElement() {
    assertThat(val.get(0)).isEqualTo(new ScValueI32(2)); // commontests-ignore-array
    assertThat(val.isEmpty()).isFalse();
  }

  @Test
  void emptySet() {
    ScValueSet empty = new ScValueSet(List.of());
    assertThat(empty.isEmpty()).isTrue();
  }
}
