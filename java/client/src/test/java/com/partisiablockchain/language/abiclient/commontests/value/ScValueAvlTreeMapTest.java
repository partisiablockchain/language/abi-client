package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.LinkedHashMap;
import org.junit.jupiter.api.Test;

final class ScValueAvlTreeMapTest {
  @Test
  void getType() {
    var map = new LinkedHashMap<ScValue, ScValue>();
    var val = new ScValueAvlTreeMap(0, map);

    assertThat(val.treeId()).isEqualTo(0);
    assertThat(val.getType()).isEqualTo(TypeSpec.TypeIndex.AvlTreeMap);
  }

  @Test
  void valueOrNull() {
    var scMap = new LinkedHashMap<ScValue, ScValue>();
    scMap.put(new ScValueU8((byte) 1), new ScValueBool(true));
    scMap.put(new ScValueU8((byte) 2), new ScValueBool(false));
    var val = new ScValueAvlTreeMap(0, scMap);

    var map =
        requireNonNull(
            val.mapKeysValues(scValue -> scValue.asInt(), scValue1 -> scValue1.boolValue()));
    assertThat(map.get(1)).isTrue(); // commontests-ignore-array
    assertThat(map.get(2)).isFalse(); // commontests-ignore-array

    var val2 = new ScValueAvlTreeMap(0, null);

    var map2 = val2.mapKeysValues(scValue -> scValue.asInt(), scValue1 -> scValue1.boolValue());
    assertThat(map2).isNull();
  }
}
