package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.RpcReaderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.RpcReadException;
import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

@SuppressWarnings("PMD.TooManyStaticImports")
final class RpcReaderTest {

  @Test
  void readBool() {
    var rpcValue =
        RpcReaderHelper.wrap("dddddd0d01", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)).readRpc();
    assertThat(requireNonNull(rpcValue.contractInvocation()).name()).isEqualTo("test");
    var argSize = rpcValue.arguments().size(); // commontests-ignore-arg
    assertThat(argSize).isEqualTo(rpcValue.size()); // commontests-ignore-array
    assertThat(rpcValue.get(0).boolValue()).isTrue(); // commontests-ignore-array
  }

  @Test
  void readNumbers() {
    var rpcValueU8 =
        RpcReaderHelper.wrap("dddddd0d02", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)).readRpc();
    assertThat(rpcValueU8.get(0).asInt()).isEqualTo(2); // commontests-ignore-array

    var rpcValueI8 =
        RpcReaderHelper.wrap("dddddd0dff", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)).readRpc();
    assertThat(rpcValueI8.get(0).asInt()).isEqualTo(-1); // commontests-ignore-array

    var rpcValueU16 =
        RpcReaderHelper.wrap("dddddd0d0201", new SimpleTypeSpec(TypeSpec.TypeIndex.u16)).readRpc();
    assertThat(rpcValueU16.get(0).asInt()).isEqualTo(0x0201); // commontests-ignore-array

    var rpcValueI16 =
        RpcReaderHelper.wrap("dddddd0dffff", new SimpleTypeSpec(TypeSpec.TypeIndex.i16)).readRpc();
    assertThat(rpcValueI16.get(0).asInt()).isEqualTo(-1); // commontests-ignore-array

    var rpcValueU32 =
        RpcReaderHelper.wrap("dddddd0d04030201", new SimpleTypeSpec(TypeSpec.TypeIndex.u32))
            .readRpc();
    assertThat(rpcValueU32.get(0).asLong()).isEqualTo(0x04030201L); // commontests-ignore-array

    var rpcValueI32 =
        RpcReaderHelper.wrap("dddddd0dffffffff", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))
            .readRpc();
    assertThat(rpcValueI32.get(0).asInt()).isEqualTo(-1); // commontests-ignore-array

    var rpcValueU64 =
        RpcReaderHelper.wrap("dddddd0d0807060504030201", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))
            .readRpc();
    assertThat(
            rpcValueU64
                .get(0) // commontests-ignore-array
                .asBigInteger()
                .equals(new BigInteger("0807060504030201", 16)))
        .isTrue();

    var rpcValueI64 =
        RpcReaderHelper.wrap("dddddd0dffffffffffffffff", new SimpleTypeSpec(TypeSpec.TypeIndex.i64))
            .readRpc();
    assertThat(rpcValueI64.get(0).asLong()).isEqualTo(-1L); // commontests-ignore-array

    var rpcValueU128 =
        RpcReaderHelper.wrap(
                "dddddd0d08070605040302010807060504030201",
                new SimpleTypeSpec(TypeSpec.TypeIndex.u128))
            .readRpc();
    assertThat(
            rpcValueU128
                .get(0) // commontests-ignore-array
                .asBigInteger()
                .equals(new BigInteger("08070605040302010807060504030201", 16)))
        .isTrue();

    var rpcValueI128 =
        RpcReaderHelper.wrap(
                "dddddd0dffffffffffffffffffffffffffffffff",
                new SimpleTypeSpec(TypeSpec.TypeIndex.i128))
            .readRpc();
    assertThat(
            rpcValueI128
                .get(0) // commontests-ignore-array
                .asBigInteger()
                .equals(new BigInteger("-1")))
        .isTrue();
  }

  @Test
  void readAddress() {
    var address = "010000000000000000000000000000000000000003";
    var rpcVal =
        RpcReaderHelper.wrap("dddddd0d" + address, new SimpleTypeSpec(TypeSpec.TypeIndex.Address))
            .readRpc();
    assertThat(rpcVal.get(0).addressValue().bytes()) // commontests-ignore-array
        .isEqualTo(bytesFromHex(address));
  }

  @Test
  void readString() {
    var rpcVal =
        RpcReaderHelper.wrap(
                "dddddd0d0000000568656c6c6f", new SimpleTypeSpec(TypeSpec.TypeIndex.String))
            .readRpc();
    assertThat(rpcVal.get(0).stringValue()).isEqualTo("hello"); // commontests-ignore-array
  }

  @Test
  void readSizedByteArray() {
    final var typeSpec = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 5);
    var rpcVal = RpcReaderHelper.wrap("dddddd0d68656c6c6f", typeSpec).readRpc();
    assertThat(rpcVal.get(0).vecU8Value()) // commontests-ignore-array
        .isEqualTo(bytesFromHex("68656c6c6f"));
  }

  @Test
  void readOptionSome() {
    var rpcVal =
        RpcReaderHelper.wrap(
                "dddddd0d010000000568656c6c6f",
                new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String)))
            .readRpc();
    var optionValue = rpcVal.get(0).optionValue(); // commontests-ignore-array
    assertThat(optionValue.isSome()).isTrue();
    assertThat(requireNonNull(optionValue.innerValue()).stringValue()).isEqualTo("hello");
  }

  @Test
  void readOptionNone() {
    var rpcVal =
        RpcReaderHelper.wrap(
                "dddddd0d00", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String)))
            .readRpc();
    assertThat(rpcVal.get(0).optionValue().isSome()).isFalse(); // commontests-ignore-array
    // Here it is also null in TS, but Sed changes isNull to toBeUndefined as that is used other
    // places
    assertThat(rpcVal.get(0).optionValue().innerValue()) // commontests-ignore-array
        .isNull(); // commontests-expect-null
  }

  @Test
  void readVector() {
    var rpcVal =
        RpcReaderHelper.wrap(
                "dddddd0d00000003000000010000000200000003",
                new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))
            .readRpc();
    var vecVal = rpcVal.get(0).vecValue(); // commontests-ignore-array
    assertThat(vecVal.size()).isEqualTo(3); // commontests-ignore-array
    assertThat(vecVal.get(0).asInt()).isEqualTo(1); // commontests-ignore-array
    assertThat(vecVal.get(1).asInt()).isEqualTo(2); // commontests-ignore-array
    assertThat(vecVal.get(2).asInt()).isEqualTo(3); // commontests-ignore-array
  }

  @Test
  void readVecU8() {
    var rpcVal =
        RpcReaderHelper.wrap(
                "dddddd0d00000003010203",
                new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))
            .readRpc();
    var vecVal = rpcVal.get(0).vecValue(); // commontests-ignore-array
    assertThat(vecVal.size()).isEqualTo(3); // commontests-ignore-array
    assertThat(vecVal.get(0).asInt()).isEqualTo(1); // commontests-ignore-array
    assertThat(vecVal.get(1).asInt()).isEqualTo(2); // commontests-ignore-array
    assertThat(vecVal.get(2).asInt()).isEqualTo(3); // commontests-ignore-array
  }

  @Test
  void readStruct() {
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(
                new StructTypeSpec(
                    "MyStruct",
                    List.of(
                        new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)),
                        new FieldAbi("f2", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))))),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "test",
                    bytesFromHex("dddddd0d"),
                    List.of(new ArgumentAbi("valueType", new NamedTypeRef(0))))),
            new NamedTypeRef(0));
    var version = new AbiVersion(4, 0, 0);
    var fileAbi = new FileAbi("PBCABI", version, version, null, contract);

    var rpcVal = RpcReaderHelper.wrap("dddddd0d1400000020", fileAbi).readRpc();
    var structVal = rpcVal.get(0).structValue(); // commontests-ignore-array
    assertThat(requireNonNull(structVal.getFieldValue("f1")).asInt()).isEqualTo(20);
    assertThat(requireNonNull(structVal.getFieldValue("f2")).asInt()).isEqualTo(32);
  }

  @Test
  void multipleArguments() {
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "test",
                    HexFormat.of().parseHex("dddddd0d"),
                    List.of(
                        new ArgumentAbi("value1", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                        new ArgumentAbi(
                            "value2", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i8))),
                        new ArgumentAbi("value3", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))))),
            new NamedTypeRef(0));
    var version = new AbiVersion(4, 0, 0);

    var fileAbi = new FileAbi("PBCABI", version, version, null, contract);

    var rpcVal = RpcReaderHelper.wrap("dddddd0d00000014000000040102030400", fileAbi).readRpc();
    assertThat(rpcVal.size()).isEqualTo(3); // commontests-ignore-array
    assertThat(rpcVal.get(0).asInt()).isEqualTo(20); // commontests-ignore-array
    assertThat(rpcVal.get(1).vecValue().get(2).asInt()).isEqualTo(3); // commontests-ignore-array
    assertThat(rpcVal.get(2).boolValue()).isFalse(); // commontests-ignore-array
  }

  @Test
  void invalidLebShortname() {
    var rpcReader =
        RpcReaderHelper.wrap("dddddddddd0d01", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    assertThatThrownBy(rpcReader::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage(
            "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5"
                + " bytes)");
  }

  @Test
  void validLebShortname() {
    var rpcReader = RpcReaderHelper.wrap("dddd807f", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    assertThatThrownBy(rpcReader::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("No contract invocation with kind 'action' and shortname '0xdddd807f'");
  }

  @Test
  void noShortname() {
    var rpcReader = RpcReaderHelper.wrap("eeeeee0e01", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    assertThatThrownBy(rpcReader::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("No contract invocation with kind 'action' and shortname '0xeeeeee0e'");
  }

  @Test
  void shortnameTypeHash() {
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "test",
                    HexFormat.of().parseHex("7777dddddd0d"),
                    List.of(
                        new ArgumentAbi(
                            "valueType", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))))),
            new NamedTypeRef(0));
    var version = new AbiVersion(1, 0, 0);

    var fileAbi = new FileAbi("PBCABI", version, version, 6, contract);
    var rpcVal = RpcReaderHelper.wrap("7777dddddd0d01", fileAbi).readRpc();
    assertThat(rpcVal.get(0).boolValue()).isTrue(); // commontests-ignore-array
  }

  @Test
  void readSet() {
    var rpc =
        RpcReaderHelper.wrap(
            "dddddd0d00000003000000010000000200000003",
            new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u32)));
    assertThatThrownBy(rpc::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("Type Set is not supported in rpc");
  }

  @Test
  void readMap() {
    var rpc =
        RpcReaderHelper.wrap(
            "dddddd0d00000003010000000102000000020300000003",
            new MapTypeSpec(
                new SimpleTypeSpec(TypeSpec.TypeIndex.u8),
                new SimpleTypeSpec(TypeSpec.TypeIndex.u32)));
    assertThatThrownBy(rpc::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("Type Map is not supported in rpc");
  }

  @Test
  void readAvlTreeMap() {
    var rpc =
        RpcReaderHelper.wrap(
            "dddddd0d00000000",
            new AvlTreeMapTypeSpec(
                new SimpleTypeSpec(TypeSpec.TypeIndex.u8),
                new SimpleTypeSpec(TypeSpec.TypeIndex.u32)));
    assertThatThrownBy(rpc::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("Type AvlTreeMap is not supported in rpc");
  }

  @Test
  void readEnum() {
    var namedTypes =
        List.<NamedTypeSpec>of(
            new StructTypeSpec(
                "MyStruct",
                List.of(
                    new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)),
                    new FieldAbi("f2", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))),
            new EnumTypeSpec("MyEnum", List.of(new EnumVariant(0, new NamedTypeRef(0)))));
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            namedTypes,
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "test",
                    bytesFromHex("dddddd0d"),
                    List.of(new ArgumentAbi("valueType", new NamedTypeRef(1))))),
            new NamedTypeRef(0));
    var version = new AbiVersion(5, 0, 0);
    var fileAbi = new FileAbi("PBCABI", version, version, null, contract);

    var rpcVal = RpcReaderHelper.wrap("dddddd0d001400000020", fileAbi).readRpc();
    var enumVal = rpcVal.get(0).enumValue(); // commontests-ignore-array
    var structVal = enumVal.item();
    assertThat(enumVal.name()).isEqualTo("MyStruct");
    assertThat(requireNonNull(structVal.getFieldValue("f1")).asInt()).isEqualTo(20);
    assertThat(requireNonNull(structVal.getFieldValue("f2")).asInt()).isEqualTo(32);

    var rpcReaderUndefinedEnum = RpcReaderHelper.wrap("dddddd0d011400000020", fileAbi);
    assertThatThrownBy(rpcReaderUndefinedEnum::readRpc)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Undefined EnumVariant 1 used in MyEnum");
  }

  @Test
  void zkExtraByte() {
    var abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi();
    var rpc = RpcReaderHelper.wrap("0901", abi).readRpc();
    assertThat(requireNonNull(rpc.contractInvocation()).name()).isEqualTo("start_vote_counting");
    var rpcError = RpcReaderHelper.wrap("0801", abi);
    assertThatThrownBy(rpcError::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("No binder invocation with kind 'Action' and shortname '8'");
  }

  @Test
  void zkSecretInputOffChain() {
    var abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi();
    var identifier = new byte[] {0x04};
    var secretRpc = new byte[4 + 4 + 4 * 32];
    secretRpc[3] = 1;
    var publicRpc = HexFormat.of().parseHex("40");
    var rpc = concatBytes(identifier, secretRpc, publicRpc);
    var rpcFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_vote");
  }

  @Test
  void zkSecretInputOnChainNoExplicitType() {
    var abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi();
    var identifier = new byte[] {0x05};
    var secretRpc = new byte[4 + 4 + 33 + 4];
    secretRpc[3] = 1;
    var publicRpc = HexFormat.of().parseHex("40");
    var rpc = concatBytes(identifier, secretRpc, publicRpc);
    var rpcFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_vote");
  }

  @Test
  void zkSecretInputOnChain() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseAbi();
    var identifier = new byte[] {0x05};
    var secretRpc = new byte[4 + 4 + 33 + 4 + 32];
    secretRpc[3] = 1;
    secretRpc[4 + 4 + 33 + 4 - 1] = 32;
    var publicRpc = HexFormat.of().parseHex("1301");
    var rpc = concatBytes(identifier, secretRpc, publicRpc);
    var rpcFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("swap");
    assertThat(rpcFn.contractArguments().get(0).boolValue()).isTrue();
  }

  /**
   * Can't read rpc for an invocation if the contract does not contain a function for the expected
   * called kind.
   */
  @Test
  void missingContractFunctionWithKind() {
    var contractAbi =
        new ContractAbi(
            BinderType.Zk,
            ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultZkInvocations(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var fileAbi =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, contractAbi);

    var rpc = RpcReaderHelper.wrap("0e00000000", fileAbi);
    assertThatThrownBy(rpc::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("No contract invocation with kind 'zk_on_attestation_complete'");
  }
}
