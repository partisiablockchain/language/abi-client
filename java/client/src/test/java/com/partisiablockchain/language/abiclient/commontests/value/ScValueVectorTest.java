package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test of {@link ScValueVector}. */
public final class ScValueVectorTest {

  private final ScValueVector val =
      new ScValueVector(List.of(new ScValueI32(2), new ScValueI32(3)));

  @Test
  void size() {
    assertThat(val.values()).hasSize(2);
    assertThat(val.values()).hasSize(val.vecValue().size()); // commontests-ignore-array
  }

  @Test
  void get() {
    assertThat(val.get(0)).isEqualTo(new ScValueI32(2)); // commontests-ignore-array
    assertThat(val.isEmpty()).isFalse();
  }

  @Test
  void getType() {
    assertThat(val.getType()).isEqualTo(TypeSpec.TypeIndex.Vec);
    assertThat(val.getType()).isEqualTo(TypeSpec.TypeIndex.Vec);
  }

  @Test
  void emptyVec() {
    ScValueVector empty = new ScValueVector(List.of());
    assertThat(empty.isEmpty()).isTrue();
  }

  @Test
  void nonVecU8() {
    assertThatThrownBy(val::vecU8Value)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Vec u8 for current type");
  }

  @Test
  void emptyVecU8() {
    ScValueVector empty = new ScValueVector(List.of());
    assertThat(empty.vecU8Value()).isEqualTo(new byte[0]);
  }

  @Test
  void vecU8() {
    ScValueVector vecU8 =
        new ScValueVector(
            List.of(new ScValueU8((byte) 1), new ScValueU8((byte) 2), new ScValueU8((byte) 3)));
    assertThat(vecU8.vecU8Value()).isEqualTo(new byte[] {1, 2, 3});
  }
}
