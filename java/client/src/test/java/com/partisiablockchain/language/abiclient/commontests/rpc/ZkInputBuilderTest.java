package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.zk.ZkInputBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import com.secata.stream.BitInput;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ZkInputBuilderTest {

  @Test
  void secretInputType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
    var abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseContractAbi();
    var zkInputBuilder = ZkInputBuilder.createZkInputBuilder("swap", abi);

    assertThatThrownBy(() -> zkInputBuilder.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In secret_input, Expected type Named, but got bool");

    var structBuilder = zkInputBuilder.addStruct();
    structBuilder.addI128(new BigInteger("123"));
    structBuilder.addI8((byte) 1);
    var bits = zkInputBuilder.getBits();
    assertThat(bits.data())
        .isEqualTo(HexFormat.of().parseHex("7b00000000000000000000000000000001"));
  }

  @Test
  void secretInputOld() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
    var abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseContractAbi();
    var zkInputBuilder = ZkInputBuilder.createZkInputBuilder("add_vote", abi);

    assertThatThrownBy(() -> zkInputBuilder.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In secret_input, Expected type i32, but got bool");

    zkInputBuilder.addI32(123);
    var bits = zkInputBuilder.getBits();
    assertThat(bits.data()).isEqualTo(HexFormat.of().parseHex("7b000000"));
  }

  @Test
  void nonSecretFunction() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
    var abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseContractAbi();
    assertThatThrownBy(() -> ZkInputBuilder.createZkInputBuilder("non_existent", abi))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Contract does not have function with name non_existent");
    assertThatThrownBy(() -> ZkInputBuilder.createZkInputBuilder("deposit", abi))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Function deposit is not a secret input function");
  }

  @Test
  void addOtherThanOneElement() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
    var abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseContractAbi();
    var zkInputBuilder = ZkInputBuilder.createZkInputBuilder("swap", abi);

    assertThatThrownBy(zkInputBuilder::getBits)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("Missing secret input");
    zkInputBuilder.addStruct();
    assertThatThrownBy(zkInputBuilder::addStruct)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot add secret input twice");
  }

  @Test
  void addBoolean() {
    var abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                new StructTypeSpec(
                    "SecretStruct",
                    List.of(
                        new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                        new FieldAbi("f2", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))))),
            List.of(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE,
                    "secret",
                    new byte[] {1},
                    List.of(),
                    new ArgumentAbi("secret_input", new NamedTypeRef(0)))),
            new NamedTypeRef(0));

    var zkInputBuilder = ZkInputBuilder.createZkInputBuilder("secret", abi);
    zkInputBuilder.addStruct().addBool(true).addU8((byte) 42);
    var bits = zkInputBuilder.getBits();
    assertThat(bits.size()).isEqualTo(9);
    var reader = BitInput.create(bits.data());
    assertThat(reader.readBoolean()).isTrue();
    assertThat(reader.readUnsignedInt(8)).isEqualTo(42);
  }

  @Test
  void binderFunction() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary.abi").parseContractAbi();
    assertThatThrownBy(() -> ZkInputBuilder.createZkInputBuilder("rejectInput", abi))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Contract does not have function with name rejectInput");
  }
}
