package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

final class ScValueBlsSignatureTest {
  @Test
  void invalidByteLength() {
    assertThatThrownBy(() -> new ScValueBlsSignature(new byte[0]))
        .isInstanceOf(ScReadException.class)
        .hasMessage("BlsSignature expects exactly 48 bytes, but found 0");

    assertThatThrownBy(() -> new ScValueBlsSignature(new byte[49]))
        .isInstanceOf(ScReadException.class)
        .hasMessage("BlsSignature expects exactly 48 bytes, but found 49");
  }

  @Test
  void getType() {
    var signature = new ScValueBlsSignature(new byte[48]);
    assertThat(signature.getType()).isEqualTo(TypeSpec.TypeIndex.BlsSignature);
  }

  @Test
  void value() {
    var rawBytes = HexFormat.of().parseHex(ValidTestHexValues.BLS_SIGNATURE);

    var blsSignatureFromRaw = new ScValueBlsSignature(rawBytes);
    assertThat(blsSignatureFromRaw.bytes()).isEqualTo(rawBytes);
    assertThat(blsSignatureFromRaw.blsSignatureValue()).isEqualTo(blsSignatureFromRaw);
  }
}
