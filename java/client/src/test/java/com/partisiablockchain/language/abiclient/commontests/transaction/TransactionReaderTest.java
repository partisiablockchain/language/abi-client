package com.partisiablockchain.language.abiclient.commontests.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper.ExecutedTransaction;
import com.partisiablockchain.language.abiclient.transaction.TransactionReadException;
import com.partisiablockchain.language.abiclient.transaction.TransactionReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.stream.BigEndianByteInput;
import java.io.ByteArrayInputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/**
 * Test for the transaction reader and the corresponding abi. The reader is tested using existing
 * transactions and events on the testnet.
 */
final class TransactionReaderTest {

  @Test
  void readSet() {
    var transactionReader =
        new TransactionReader(
            new byte[] {1, 2, 3, 4}, TestingHelper.getContractAbiFromFile("contract_set.abi"));
    var setTypeSpec = new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    assertThatThrownBy(() -> transactionReader.readGeneric(setTypeSpec))
        .isInstanceOf(TransactionReadException.class)
        .hasMessage("Type Set is not supported in rpc");
  }

  @Test
  void readMap() {
    var transactionReader =
        new TransactionReader(
            new BigEndianByteInput(new ByteArrayInputStream(new byte[] {1, 2, 3, 4})),
            TestingHelper.getContractAbiFromFile("contract_simple_map.abi"));
    var mapTypeSpec =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.Address),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    assertThatThrownBy(() -> transactionReader.readGeneric(mapTypeSpec))
        .isInstanceOf(TransactionReadException.class)
        .hasMessage("Type Map is not supported in rpc");
  }

  @Test
  void readAvlTreeMap() {
    var transactionReader =
        new TransactionReader(
            new BigEndianByteInput(new ByteArrayInputStream(new byte[] {1, 2, 3, 4})),
            TestingHelper.getContractAbiFromFile("contract_simple_map.abi"));
    var mapTypeSpec =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.Address),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    assertThatThrownBy(() -> transactionReader.readGeneric(mapTypeSpec))
        .isInstanceOf(TransactionReadException.class)
        .hasMessage("Type AvlTreeMap is not supported in rpc");
  }

  @Test
  void testReadAllBytes() {
    // Payload from
    // https://reader.partisiablockchain.com/shards/Shard1/blockchain/transaction/3a7b48fc2cc9d2ea79fbb29819d68ca0b16effb070f11d00964f67ec8b7e05cf?requireFinal=false
    var validTransactionPayload =
        Base64.decode(
            "Ad3YrH4KCiTDxvHXrKltp9uRYIycRRzgHnxB9liarF4RfTiQMQ9ptLfsFLViZd9rywZ3+yKYJ7G2puAYj"
                + "pnTVhUAAAAAAAA8PAAAAYMho9FYAAAAAAAAAAAE/hfRAJNyyO06xbeQsy40k1nCx+kAAADIAwEAAAAGU"
                + "2hhcmQxAAAAAAAMGQwAWQBXAAAARABXAFcAAAA/ABcAVwBWAAAAAABXAFcAVwBPAEYAPgBPACIAAAAAA"
                + "DwAAAAAAAAAQwBXABoAVwBAAFAAVwAVAFcAVwBUAEsAJwAAAFcAFwBJAAAAVwBXAFQAVgAeAFYAPwAAA"
                + "FcAVwBXABYAVQBVADsACABXAD8AVwAAAFMARwBXAFcAVQBXAAAAVwBXACQACQAZAFEAVwBUAEQAAwBVA"
                + "FcAQQBXAAAAVgBXAEU=");
    var tooLongTransactionPayload = new byte[validTransactionPayload.length + 10];
    System.arraycopy(
        validTransactionPayload, 0, tooLongTransactionPayload, 0, validTransactionPayload.length);
    assertThatThrownBy(
            () -> TransactionReader.deserializeTransactionPayload(tooLongTransactionPayload))
        .isInstanceOf(TransactionReadException.class)
        .hasMessage("The input wasn't fully read, 10 remaining bytes.");

    // Payload from
    // https://node1.testnet.partisiablockchain.com/shards/Shard2/blockchain/transaction/1109b57d07a8a6e96b231dbcf33c301b364d452ad2b1dabba4e8191839d5a9dc?requireFinal=false
    var validEventPayload =
        Base64.decode(
            "AJxumr8xIrjBEKFmjLmU4rqZaZriTgDAgcfD+9WPmUkGAgIAAAApQ0hFQ0tfRVhJU1RFTkNFX0ZPUl9DT"
                + "05UUkFDVF9PUEVOX0FDQ09VTlQBAAAABHRydWUBAAAABlNoYXJkMgAAAAAAAAADAAAAAAAAAAEAAAAAA"
                + "AAAAgIA");
    var tooLongEventPayload = new byte[validEventPayload.length + 10];
    System.arraycopy(validEventPayload, 0, tooLongEventPayload, 0, validEventPayload.length);
    assertThatThrownBy(() -> TransactionReader.deserializeEventPayload(tooLongEventPayload))
        .isInstanceOf(TransactionReadException.class)
        .hasMessage("The input wasn't fully read, 10 remaining bytes.");
  }

  @Test
  void deserializationOfTransactionAndEvents() {

    List<ExecutedTransaction> transactions = TestingHelper.loadExecutedTransactions();

    Set<String> namedTypesFound = new HashSet<>();

    for (var transaction : transactions) {
      var deserialized = deserializeExecutedTransaction(transaction);
      addAllNamedTypes(deserialized, namedTypesFound);
    }

    // Did not encounter
    // InnerEvent::Sync,
    // InnerEvent::Callback,
    // InnerSystemEvent::CreateAccount,
    // InnerSystemEvent::RemoveContract,
    // InnerSystemEvent::CreateShard,
    // InnerSystemEvent::RemoveShard,
    // on testnet

    Set<String> expectedNamedTypesFound =
        new HashSet<>(
            List.of(
                "Account",
                "InteractContract",
                "UpdateGlobalPluginState",
                "CheckExistence",
                "UpgradeSystemContract",
                "InnerTransaction",
                "CallbackEvent",
                "EventTransaction",
                "System",
                "UpdatePlugin",
                "CoreTransactionPart",
                "UpdateGlobalPluginStateEvent",
                "SetFeatureEvent",
                "UpdatePluginEvent",
                "ReturnEnvelope",
                "UpdateContextFreePluginState",
                "UpgradeSystemContractEvent",
                "SignedTransaction",
                "GlobalPluginStateUpdate",
                "SetFeature",
                "InteractWithContractTransaction",
                "CreateContractTransaction",
                "Consensus",
                "InnerPart",
                "Callback",
                "Transaction",
                "LocalPluginStateUpdate",
                "UpdateLocalPluginStateEvent",
                "DeployContract",
                "CheckExistenceEvent",
                "ExecutableEvent",
                "ShardRoute",
                "UpdateLocalPluginState"));
    assertThat(namedTypesFound).isEqualTo(expectedNamedTypesFound);
  }

  private ScValue deserializeExecutedTransaction(ExecutedTransaction executedTransaction) {
    if (executedTransaction.isEvent()) {
      return TransactionReader.deserializeEventPayload(
          TestingHelper.transactionPayloadFromExecutedTransaction(executedTransaction));
    } else {
      return TransactionReader.deserializeTransactionPayload(
          TestingHelper.transactionPayloadFromExecutedTransaction(executedTransaction));
    }
  }

  private void addAllNamedTypes(ScValue value, Set<String> namedTypesFound) {
    if (value instanceof ScValueEnum) {
      var scValueEnum = (ScValueEnum) value;
      addAllNamedTypes(scValueEnum.item(), namedTypesFound);
    } else if (value instanceof ScValueStruct) {
      var scValueStruct = (ScValueStruct) value;
      namedTypesFound.add(scValueStruct.name()); // commontests-ignore-array

      for (var field : scValueStruct.fieldsMap().values()) {
        addAllNamedTypes(field, namedTypesFound);
      }
    }
  }

  @Test
  void contractAbi() {
    var abi = requireNonNull(TransactionReader.getContractAbi());
    var signedTransaction = abi.getNamedType("SignedTransaction");
    assertThat(signedTransaction).isNotNull();
    var executableEvent = abi.getNamedType("ExecutableEvent");
    assertThat(executableEvent).isNotNull();
  }
}
