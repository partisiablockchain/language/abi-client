package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.BigEndianReader;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

final class BigEndianReaderTest {

  @Test
  void mapSetNotSupported() {
    var reader =
        new BigEndianReader(
            new byte[] {1},
            new ContractAbi(
                BinderType.Public,
                List.of(),
                List.of(),
                List.of(),
                List.of(),
                new SimpleTypeSpec(TypeSpec.TypeIndex.bool)));
    assertThatThrownBy(
            () ->
                reader.read(
                    new MapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u8),
                        new SimpleTypeSpec(TypeSpec.TypeIndex.bool))))
        .hasMessage("Type Map is not supported in rpc");
    assertThatThrownBy(
            () -> reader.read(new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8))))
        .hasMessage("Type Set is not supported in rpc");
    assertThat(reader.read(new SimpleTypeSpec(TypeSpec.TypeIndex.bool)).boolValue()).isTrue();
    assertThatThrownBy(
            () ->
                reader.read(
                    new AvlTreeMapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u8),
                        new SimpleTypeSpec(TypeSpec.TypeIndex.bool))))
        .hasMessage("Type AvlTreeMap is not supported in rpc");
  }
}
