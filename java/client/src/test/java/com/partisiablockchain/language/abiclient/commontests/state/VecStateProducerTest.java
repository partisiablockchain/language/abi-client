package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.BuilderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

final class VecStateProducerTest {

  @Test
  public void assertTypeErrorInStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    assertThatThrownBy(
            () -> stateProducer.addU64(1).addStruct().addU64(2).addVec().addString("valueType"))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_struct/some_vector/0, Expected type u64, but got String");

    var noVecBuilder = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> noVecBuilder.addU64(1).addVec())
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_struct, Expected type Named, but got Vec");
  }

  @Test
  public void testVec() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_vector_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addVec().addU64(1).addU64(2).addU64(3);
    byte[] state = stateProducer.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("03000000"),
            HexFormat.of().parseHex("0100000000000000"),
            HexFormat.of().parseHex("0200000000000000"),
            HexFormat.of().parseHex("0300000000000000"));
    assertThat(state).isEqualTo(expected);

    // Testing with no type checking
    var noTypeCheckBuilder = new StateBuilder(null);
    noTypeCheckBuilder.addVec().addU64(1).addU64(2).addU64(3);
    assertThat(BuilderHelper.builderToBytesLe(noTypeCheckBuilder)).isEqualTo(expected);
  }
}
