package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromStringLe;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.commontests.BuilderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

/** Test for {@link AbstractBuilder} using LittleEndianByteOutput. */
@SuppressWarnings("PMD.TooManyStaticImports")
public final class AbstractStateBuilderTest {

  @Test
  void buildEveryThing() {
    var mapType =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.String),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

    StructTypeSpec mapEntryType =
        new StructTypeSpec(
            "MapEntry$String$u64",
            List.of(
                new FieldAbi("keyType", mapType.keyType()),
                new FieldAbi("valueType", mapType.valueType())));

    var fieldAbiList =
        List.of(
            new FieldAbi("field1", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)),
            new FieldAbi("field2", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)),
            new FieldAbi("field3", new SimpleTypeSpec(TypeSpec.TypeIndex.i16)),
            new FieldAbi("field4", new SimpleTypeSpec(TypeSpec.TypeIndex.u16)),
            new FieldAbi("field5", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
            new FieldAbi("field6", new SimpleTypeSpec(TypeSpec.TypeIndex.u32)),
            new FieldAbi("field7", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
            new FieldAbi("field8", new SimpleTypeSpec(TypeSpec.TypeIndex.u64)),
            new FieldAbi("field9", new SimpleTypeSpec(TypeSpec.TypeIndex.i128)),
            new FieldAbi("field10", new SimpleTypeSpec(TypeSpec.TypeIndex.u128)),
            new FieldAbi("field11", new SimpleTypeSpec(TypeSpec.TypeIndex.String)),
            new FieldAbi("field12", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
            new FieldAbi("field13", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
            new FieldAbi("field14", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
            new FieldAbi("field15", new NamedTypeRef(0)),
            new FieldAbi("field16", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
            new FieldAbi("field17", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64))),
            new FieldAbi("field18", mapType),
            new FieldAbi("field19", new NamedTypeRef(3)),
            new FieldAbi("field20", new NamedTypeRef(3)));

    var structOfU64 =
        new StructTypeSpec(
            "", List.of(new FieldAbi("field", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));
    var stateStruct = new StructTypeSpec("name", fieldAbiList);

    var structRef = new NamedTypeRef(1);

    var enumStruct1 =
        new StructTypeSpec(
            "enumVariant1",
            List.of(new FieldAbi("field", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));
    var enumStruct2 = new StructTypeSpec("enumVariant2", List.of());

    var enumType =
        new EnumTypeSpec(
            "myEnum",
            List.of(
                new EnumVariant(1, new NamedTypeRef(4)), new EnumVariant(2, new NamedTypeRef(5))));

    var stateProducer =
        new StateBuilder(
            new ContractAbi(
                BinderType.Public,
                List.of(),
                List.of(structOfU64, stateStruct, mapEntryType, enumType, enumStruct1, enumStruct2),
                List.of(),
                List.of(),
                structRef));

    Consumer<AbstractBuilder> fn =
        b -> {
          AbstractBuilder builder =
              b.addI8((byte) -128)
                  .addU8((byte) 127)
                  .addI16((short) -32768)
                  .addU16((short) 32767)
                  .addI32(-2147483648)
                  .addU32(2147483647)
                  .addI64(-9223372036854775808L)
                  .addU64(9223372036854775807L)
                  .addI128(new BigInteger("-170141183460469231731687303715884105728"))
                  .addU128(new BigInteger("170141183460469231731687303715884105727"))
                  .addString("Test string")
                  .addBool(true)
                  .addBool(false);
          builder.addOption().addI64(42);
          builder.addStruct().addU64(1);
          builder.addVec().addI64(2);
          builder.addSet().addU64(3);
          builder.addMap().addString("key").addU64(4);
          builder.addEnumVariant(1).addU64(2);
          builder.addEnumVariant(2);
        };

    fn.accept(stateProducer);

    var actual = stateProducer.getBytes();

    var expected =
        HexFormat.of()
            .parseHex(
                ""
                    + "80" // Hex format of Byte.MIN_VALUE
                    + "7f" // Hex format of Byte.MAX_VALUE
                    + "0080" // Hex format of Short.MIN_VALUE
                    + "ff7f" // Hex format of Short.MAX_VALUE
                    + "00000080" // Hex format of Integer.MIN_VALUE
                    + "ffffff7f" // Hex format of Integer.MAX_VALUE
                    + "0000000000000080" // Hex format of Long.MIN_VALUE
                    + "ffffffffffffff7f" // Hex format of Long.MAX_VALUE
                    + "00000000000000000000000000000080" // Hex format of
                    // BigEndianDataOutput.MIN_128_BIT_VALUE
                    + "ffffffffffffffffffffffffffffff7f" // Hex format of
                    // BigEndianDataOutput.MAX_128_BIT_VALUE
                    + "0b0000005465737420737472696e67" // Hex format of String "Test string"
                    + "01" // Hex format of Boolean True valueType
                    + "00" // Hex format of Boolean False Value
                    + "01" // Hex format of an Option
                    + "2a00000000000000" // Hex format of the option's u64 valueType of 42
                    + "0100000000000000" // Hex format of the struct's u64 valueType of 1
                    + "01000000" // Hex format of a vector of one entry
                    + "0200000000000000" // Hex format of the vector's u64 valueType of 2
                    + "01000000" // Hex format of set of one entry
                    + "0300000000000000" // Hex format of the set's u64 valueType of 3
                    + "01000000" // Hex format of a map of one entry
                    + "030000006b6579" // Hex format of the keyType of the map's entry
                    + "0400000000000000" // Hex format of the valueType of the map's entry
                    + "01" // Hex format of the byte for the Enum variants discriminant
                    + "0200000000000000" // Hex format of the enum variant's u64 valueType of 2
                    + "02"); // Hex format of the byte for the Enum variants discriminant

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void simpleBoolTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-booleans
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_booleans_v3.abi");

    StateBuilder stateBuilder = new StateBuilder(contractAbi);
    stateBuilder.addBool(false);
    byte[] state = stateBuilder.getBytes();
    byte[] expected = new byte[] {0x00};
    assertThat(state).isEqualTo(expected);

    var stateProducerTrue = new StateBuilder(contractAbi);
    stateProducerTrue.addBool(true);
    byte[] stateTrue = stateProducerTrue.getBytes();
    byte[] expectedTrue = new byte[] {0x01};
    assertThat(stateTrue).isEqualTo(expectedTrue);
  }

  @Test
  void contractAddress() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-address
    var address = "b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_address_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addAddress(address);
    byte[] state = stateProducer.getBytes();
    byte[] expected = HexFormat.of().parseHex(address);
    assertThat(state).isEqualTo(expected);

    var invalidProducer = new StateBuilder(contractAbi);

    assertThatThrownBy(() -> invalidProducer.addAddress(new byte[4]))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In /my_address, Address must have length 21 bytes, got length = 4, value = 00000000");
  }

  @Test
  void canBuildStateWithAbiVersion6Types() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/rust/contract-types-hash-to-bls-signature
    ContractAbi contractAbi =
        TestingHelper.getContractAbiFromFile("contract_types_hash_to_bls_signature_v5.abi");
    var stateProducer = new StateBuilder(contractAbi);
    BigInteger u256Value = BigInteger.valueOf(123);
    byte[] hashBytes = bytesFromHex(ValidTestHexValues.HASH);
    byte[] publicKeyBytes = bytesFromHex(ValidTestHexValues.PUBLIC_KEY);
    byte[] signatureBytes = bytesFromHex(ValidTestHexValues.SIGNATURE);
    byte[] blsPublicKeyBytes = bytesFromHex(ValidTestHexValues.BLS_PUBLIC_KEY);
    byte[] blsSignatureBytes = bytesFromHex(ValidTestHexValues.BLS_SIGNATURE);

    var stateProducer2 =
        stateProducer
            .addU256(u256Value)
            .addHash(ValidTestHexValues.HASH)
            .addPublicKey(ValidTestHexValues.PUBLIC_KEY)
            .addSignature(ValidTestHexValues.SIGNATURE)
            .addBlsPublicKey(ValidTestHexValues.BLS_PUBLIC_KEY)
            .addBlsSignature(ValidTestHexValues.BLS_SIGNATURE);
    assertThat(stateProducer2).isNotNull();
    var structProducer = stateProducer.addStruct();
    structProducer.addVec().addU256(u256Value);
    structProducer.addVec().addHash(hashBytes);
    structProducer.addVec().addPublicKey(publicKeyBytes);
    structProducer.addVec().addSignature(signatureBytes);
    structProducer.addVec().addBlsPublicKey(blsPublicKeyBytes);
    structProducer.addVec().addBlsSignature(blsSignatureBytes);
    byte[] state = stateProducer.getBytes();

    byte[] zeroPadding = new byte[31];
    byte[] length1Vector = new byte[4];
    length1Vector[0] = 1;
    byte[] concatenatedArguments =
        concatBytes(
            concatBytes(u256Value.toByteArray(), zeroPadding),
            hashBytes,
            publicKeyBytes,
            signatureBytes,
            blsPublicKeyBytes,
            blsSignatureBytes,
            length1Vector,
            concatBytes(u256Value.toByteArray(), zeroPadding),
            length1Vector,
            hashBytes,
            length1Vector,
            publicKeyBytes,
            length1Vector,
            signatureBytes,
            length1Vector,
            blsPublicKeyBytes,
            length1Vector,
            blsSignatureBytes);
    assertThat(state).isEqualTo(concatenatedArguments);
  }

  @Test
  void contractEnum() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/rust/contract-enum
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_enum.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addEnumVariant(5).addU8((byte) 0x00).addBool(false);
    byte[] state = stateProducer.getBytes();
    byte[] expected = HexFormat.of().parseHex("050000");
    assertThat(state).isEqualTo(expected);

    var invalidProducer = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> BuilderHelper.builderToBytesLe(invalidProducer.addEnumVariant(9)))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In /my_enum, Undefined variant discriminant 9 for Vehicle");
  }

  @Test
  void assertTypeError() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-address
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_address_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> stateProducer.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_address, Expected type Address, but got bool");
  }

  @Test
  void contractString() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-strings
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_strings_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addString("hello");
    byte[] state = stateProducer.getBytes();
    assertThat(state).isEqualTo(bytesFromStringLe("hello"));
  }

  @Test
  void canDelayGeneration() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-nested-vector
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_nested_vector_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    var outerVecStateProducer = stateProducer.addVec();
    var innerVecStateProducer = outerVecStateProducer.addVec();
    outerVecStateProducer.addVec().addU64(1);
    innerVecStateProducer.addU64(10).addU64(11);

    byte[] state = stateProducer.getBytes();

    // We expect the following: [[10, 11]], [1]]
    byte[] expected =
        concatBytes(
            // Two elements in outer vec
            HexFormat.of().parseHex("02000000"),
            // First valueType has 10, 11
            HexFormat.of().parseHex("02000000" + "0A000000000000000B00000000000000"),
            HexFormat.of().parseHex("010000000100000000000000"));
    assertThat(state).isEqualTo(expected);
  }

  @Test
  void noTypeChecking() {
    var stateProducer = new StateBuilder(null);
    stateProducer.addBool(false).addU64(2).addString("hello");
    byte[] state = stateProducer.getBytes();
    byte[] expected = HexFormat.of().parseHex("0002000000000000000500000068656c6c6f");
    assertThat(state).isEqualTo(expected);
  }

  @Test
  void noTypeCheckingEnum() {
    var builder = new StateBuilder(null);
    builder.addEnumVariant(0).addBool(false).addU64(2).addString("hello");
    byte[] myEnum = BuilderHelper.builderToBytesLe(builder);
    byte[] expected = HexFormat.of().parseHex("000002000000000000000500000068656c6c6f");
    assertThat(myEnum).isEqualTo(expected);
    var nullContract = new StateBuilder(null);
    nullContract.addEnumVariant(0).addBool(true);
    byte[] actual = nullContract.getBytes();
    assertThat(actual).isEqualTo(HexFormat.of().parseHex("0001"));
  }

  @Test
  void tooFewArguments() {
    var structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));
    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    assertThatThrownBy(() -> new StateBuilder(simpleContractAbi).getBytes())
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In root, Missing argument 'f'");
  }

  @Test
  void tooManyArguments() {
    var structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    var stateProducer = new StateBuilder(simpleContractAbi);
    stateProducer.addU64(1);

    assertThatThrownBy(() -> stateProducer.addU8((byte) 0))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In root, Cannot add more arguments than the struct has fields.");
  }

  @Test
  void addSizedByteArrayWithType() {
    final var spec = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 4);
    var structAbi = new StructTypeSpec("name", List.of(new FieldAbi("f", spec)));
    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    var structProducer = new StateBuilder(simpleContractAbi);
    structProducer.addSizedByteArray(new byte[] {2, 2, 2, 2});

    assertThat(structProducer.getBytes()).isEqualTo(HexFormat.of().parseHex("02020202"));
  }

  /** Arrays can contain non-byte elements. */
  @Test
  void addSizedObjectArrayWithType() {
    final var spec = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String), 2);
    final var structAbi = new StructTypeSpec("name", List.of(new FieldAbi("f", spec)));
    final var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    final var structProducer = new StateBuilder(simpleContractAbi);
    structProducer.addSizedArray().addString("Hello").addString("World");

    assertThat(structProducer.getBytes())
        .isEqualTo(HexFormat.of().parseHex("0500000048656C6C6F05000000576F726C64"));
  }

  /** Arrays can contain non-byte elements. */
  @Test
  void addMatrixWithType() {
    final var spec =
        new SizedArrayTypeSpec(
            new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32), 2), 2);
    final var structAbi = new StructTypeSpec("name", List.of(new FieldAbi("f", spec)));
    final var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    final var structProducer = new StateBuilder(simpleContractAbi);
    final var arrayProducer = structProducer.addSizedArray();
    arrayProducer.addSizedArray().addI32(1).addI32(2);
    arrayProducer.addSizedArray().addI32(3).addI32(4);

    assertThat(structProducer.getBytes())
        .isEqualTo(HexFormat.of().parseHex("01000000020000000300000004000000"));
  }

  /** Arrays can contain non-byte elements. */
  @Test
  void addMatrixWithWrongType() {
    final var spec =
        new SizedArrayTypeSpec(
            new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u32), 2), 2);
    final var structAbi = new StructTypeSpec("name", List.of(new FieldAbi("f", spec)));
    final var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    final var structProducer = new StateBuilder(simpleContractAbi);
    final var arrayProducer = structProducer.addSizedArray();
    assertThatThrownBy(() -> arrayProducer.addSizedArray().addString("Wrong type"))
        .isInstanceOf(TypeCheckingException.class);
  }

  @Test
  void addSizedByteArrayWithoutType() {
    var stateProducer = new StateBuilder(null);
    stateProducer.addSizedByteArray(new byte[] {1, 3}).addSizedByteArray(new byte[] {5});
    byte[] state = stateProducer.getBytes();
    assertThat(state).isEqualTo(HexFormat.of().parseHex("010305"));
  }

  @Test
  void mapState() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/rust/contract-map-of-struct
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_map_of_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    var mapStateProducer = stateProducer.addMap();
    mapStateProducer.addString("key1").addStruct().addU64(2).addVec().addU64(3);
    mapStateProducer.addString("key2").addStruct().addU64(4).addVec().addU64(6);
    byte[] state = stateProducer.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("02000000"),
            bytesFromStringLe("key1"),
            HexFormat.of().parseHex("0200000000000000"),
            HexFormat.of().parseHex("01000000"),
            HexFormat.of().parseHex("0300000000000000"),
            bytesFromStringLe("key2"),
            HexFormat.of().parseHex("0400000000000000"),
            HexFormat.of().parseHex("01000000"),
            HexFormat.of().parseHex("0600000000000000"));
    assertThat(state).isEqualTo(expected);
  }

  @Test
  void validateDepthFirst() {
    var innerStruct =
        new StructTypeSpec(
            "inner", List.of(new FieldAbi("a", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))));
    var structAbi =
        new StructTypeSpec(
            "name",
            List.of(
                new FieldAbi("b", new NamedTypeRef(1)),
                new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi, innerStruct),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    var stateBuilder = new StateBuilder(simpleContractAbi);
    stateBuilder.addStruct();

    assertThatThrownBy(stateBuilder::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In /b, Missing argument 'a'");
  }

  @Test
  void addAvlTreeMap() {
    var avl1 =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8), new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var avl2 =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.String),
            new SimpleTypeSpec(TypeSpec.TypeIndex.Address));
    var structAbi =
        new StructTypeSpec("name", List.of(new FieldAbi("f1", avl1), new FieldAbi("f2", avl2)));
    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    var structProducer = new StateBuilder(simpleContractAbi);
    structProducer.addAvlTreeMap(1).addAvlTreeMap(2);

    assertThat(structProducer.getBytes()).isEqualTo(HexFormat.of().parseHex("0100000002000000"));
  }
}
