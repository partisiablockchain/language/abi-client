package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.secata.stream.BigEndianByteOutput;
import com.secata.stream.LittleEndianByteOutput;
import java.io.ByteArrayOutputStream;
import java.util.function.Consumer;

/** Helper class for working with builders. */
public final class BuilderHelper {

  /**
   * Serializes the big endian producer to a bytearray.
   *
   * @param builder the big endian producer.
   * @return the serialized bytearray.
   */
  public static byte[] builderToBytesBe(AbstractBuilder builder) {
    return serializeBe(builder::write);
  }

  static byte[] serializeBe(Consumer<AbiOutput> consumer) {
    var out = new ByteArrayOutputStream();
    consumer.accept(new AbiByteOutput(new BigEndianByteOutput(out)));
    return out.toByteArray();
  }

  /**
   * Serializes the little endian producer to a bytearray.
   *
   * @param builder the little endian producer.
   * @return the serialized bytearray.
   */
  public static byte[] builderToBytesLe(AbstractBuilder builder) {
    return serializeLe(builder::write);
  }

  static byte[] serializeLe(Consumer<AbiOutput> consumer) {
    var out = new ByteArrayOutputStream();
    consumer.accept(new AbiByteOutput(new LittleEndianByteOutput(out)));
    return out.toByteArray();
  }
}
