package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromStringBe;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcBuilder;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

@SuppressWarnings("PMD.TooManyStaticImports")
final class RpcContractBuilderTest {

  @Test
  void buildEverything() {
    var builder = new RpcContractBuilder(HexFormat.of().parseHex("fffffffff0"));
    Consumer<AbstractBuilder> fn =
        b ->
            b.addI8((byte) -128)
                .addU8((byte) 128)
                .addI16((short) -32768)
                .addU16((short) 32767)
                .addI32(-2147483648)
                .addU32(2147483647)
                .addI64(-9223372036854775808L)
                .addU64(9223372036854775807L)
                .addI128(new BigInteger("-170141183460469231731687303715884105728"))
                .addU128(new BigInteger("170141183460469231731687303715884105727"))
                .addString("Test string")
                .addBool(true)
                .addBool(false)
                .addOption()
                .addU64(42);

    fn.accept(builder);
    fn.accept(builder.addStruct());
    fn.accept(builder.addVec());
    fn.accept(builder.addSet());

    var actual = builder.getBytes();
    assertThat(actual)
        .isEqualTo(
            bytesFromHex(
                ""
                    + "fffffffff0808080007fff800000007fffffff"
                    + "80000000000000007fffffffffffffff800000"
                    + "000000000000000000000000007fffffffffff"
                    + "ffffffffffffffffffff0000000b5465737420"
                    + "737472696e67010001000000000000002a8080"
                    + "80007fff800000007fffffff80000000000000"
                    + "007fffffffffffffff80000000000000000000"
                    + "0000000000007fffffffffffffffffffffffff"
                    + "ffffff0000000b5465737420737472696e6701"
                    + "0001000000000000002a0000000e808080007f"
                    + "ff800000007fffffff80000000000000007fff"
                    + "ffffffffffff80000000000000000000000000"
                    + "0000007fffffffffffffffffffffffffffffff"
                    + "0000000b5465737420737472696e6701000100"
                    + "0000000000002a0000000e808080007fff8000"
                    + "00007fffffff80000000000000007fffffffff"
                    + "ffffff80000000000000000000000000000000"
                    + "7fffffffffffffffffffffffffffffff000000"
                    + "0b5465737420737472696e67010001000000000"
                    + "000002a"));
  }

  @Test
  void simpleBoolTest() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    AbiParser parser = TestingHelper.loadAbiParserFromFile("contract_booleans_v3.abi");
    FileAbi model = parser.parseAbi();
    ContractAbi contract = (ContractAbi) model.chainComponent();
    var shortname = bytesFromHex("b0a1fab30c");

    RpcContractBuilder rpcBuilder =
        new RpcContractBuilder(contract, shortname, ImplicitBinderAbi.DefaultKinds.ACTION);
    rpcBuilder.addBool(false);
    var rpc = rpcBuilder.getBytes();
    var expected = concatBytes(shortname, bytesFromHex("00"));
    assertThat(rpc).isEqualTo(expected);

    var builderTrue =
        new RpcContractBuilder(contract, shortname, ImplicitBinderAbi.DefaultKinds.ACTION);
    builderTrue.addBool(true);
    var rpcTrue = builderTrue.getBytes();
    var expectedTrue = concatBytes(shortname, bytesFromHex("01"));
    assertThat(rpcTrue).isEqualTo(expectedTrue);
  }

  @Test
  void contractAddress() {
    var address = "b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";

    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-address
    var builder =
        TestingHelper.createBuilderFromFile("contract_address_v3.abi", "update_my_address");
    builder.addAddress(bytesFromHex(address));
    var rpc = builder.getBytes();
    var expected = concatBytes(bytesFromHex("a7e192a009"), bytesFromHex(address));
    assertThat(rpc).isEqualTo(expected);

    var invalidBuilder =
        TestingHelper.createBuilderFromFile("contract_address_v3.abi", "update_my_address");
    assertThatThrownBy(() -> invalidBuilder.addAddress(new byte[4]))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In update_my_address/value, Address must have length 21 bytes, got length = 4, value ="
                + " 00000000");
  }

  @Test
  void assertTypeError() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-address
    var builder =
        TestingHelper.createBuilderFromFile("contract_address_v3.abi", "update_my_address");
    assertThatThrownBy(() -> builder.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_address/value, Expected type Address, but got bool");
  }

  @Test
  void contractString() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-strings
    var builder =
        TestingHelper.createBuilderFromFile("contract_strings_v3.abi", "update_my_string");
    builder.addString("hello");
    var rpc = builder.getBytes();
    assertThat(rpc).isEqualTo(concatBytes(bytesFromHex("97c18ca406"), bytesFromStringBe("hello")));
  }

  @Test
  void complexStructure() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-map-of-struct
    var builder =
        TestingHelper.createBuilderFromFile("contract_map_of_struct_v3.abi", "insert_in_my_map");
    builder.addString("keyType").addStruct().addU64(2).addVec().addU64(3);
    var rpc = builder.getBytes();
    var expected =
        concatBytes(
            bytesFromHex("fc91b4c102"),
            bytesFromStringBe("keyType"),
            bytesFromHex("0000000000000002"),
            // 0001 --> one element, u64 with valueType 3
            bytesFromHex("00000001"),
            bytesFromHex("0000000000000003"));
    assertThat(rpc).isEqualTo(expected);
  }

  @Test
  void mapProducer() {
    var mapType =
        new MapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.String),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u64));

    var mapEntryType =
        new StructTypeSpec(
            "MapEntry$String$u64",
            List.of(
                new FieldAbi("keyType", mapType.keyType()),
                new FieldAbi("valueType", mapType.valueType())));

    var arg = new ArgumentAbi("valueType", mapType);
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "action", new byte[1], List.of(arg));

    var contract =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(mapEntryType),
            List.of(),
            List.of(fn),
            new NamedTypeRef(0));
    var builder = new RpcContractBuilder(contract, "action");
    var mapProducer = builder.addMap();
    mapProducer.addString("key1").addU64(4);
    mapProducer.addString("key2").addU64(8);

    var bytes = builder.getBytes();
    var expected =
        concatBytes(
            bytesFromHex("0000000002"),
            bytesFromStringBe("key1"),
            bytesFromHex("0000000000000004"),
            bytesFromStringBe("key2"),
            bytesFromHex("0000000000000008"));
    assertThat(bytes).isEqualTo(expected);
  }

  @Test
  void buildVecU8() {
    var arg =
        new ArgumentAbi("valueType", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)));
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "action", new byte[1], List.of(arg));
    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(fn), new NamedTypeRef(0));
    var builder1 = new RpcContractBuilder(contract, "action");
    var builder2 = new RpcContractBuilder(contract, "action");
    builder1.addVec().addU8((byte) 1).addU8((byte) 2).addU8((byte) 3);
    builder2.addVecU8(new byte[] {1, 2, 3});
    assertThat(builder1.getBytes()).isEqualTo(builder2.getBytes());

    var noTypeCheckBuilder1 = new RpcContractBuilder(new byte[1]);
    var noTypeCheckBuilder2 = new RpcContractBuilder(new byte[1]);
    noTypeCheckBuilder1.addVec().addU8((byte) 1).addU8((byte) 2).addU8((byte) 3);
    noTypeCheckBuilder2.addVecU8(new byte[] {1, 2, 3});
    assertThat(noTypeCheckBuilder1.getBytes()).isEqualTo(noTypeCheckBuilder2.getBytes());

    var returnBuilder = new RpcContractBuilder(new byte[1]);
    returnBuilder.addVecU8(new byte[] {1}).addVecU8(new byte[] {2});
    assertThat(returnBuilder.getBytes()).isEqualTo(bytesFromHex("0000000001010000000102"));
  }

  @Test
  void buildVecU8TypeError() {
    var arg1 = new ArgumentAbi("bool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var arg2 = new ArgumentAbi("vec", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u16)));
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "action", new byte[1], List.of(arg1, arg2));
    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(fn), new NamedTypeRef(0));

    var builder = new RpcContractBuilder(contract, "action");
    assertThatThrownBy(() -> builder.addVecU8(new byte[1]))
        .hasMessage("In action/bool, Expected type bool, but got Vec");
    builder.addBool(true);
    assertThatThrownBy(() -> builder.addVecU8(new byte[1]))
        .hasMessage("In action/vec, Expected type Vec<u16>, but got Vec<u8>");
  }

  @Test
  void shortnameHashContract() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    var builderFalse =
        TestingHelper.createBuilderFromFile("contract_booleans.abi", "update_my_bool");
    builderFalse.addBool(false);
    var rpc = builderFalse.getBytes();
    var expected =
        bytesFromHex("c67e90b0" + "00"); // HASH Shortname for update_my_bool + 0x00 since false
    assertThat(rpc).isEqualTo(expected);

    var builderTrue =
        TestingHelper.createBuilderFromFile("contract_booleans.abi", "update_my_bool");
    builderTrue.addBool(true);
    var rpcTrue = builderTrue.getBytes();
    var expectedTrue = bytesFromHex("c67e90b0" + "01");
    assertThat(rpcTrue).isEqualTo(expectedTrue);
  }

  @Test
  void canDelayGeneration() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-nested-vector
    var builder =
        TestingHelper.createBuilderFromFile("contract_nested_vector_v3.abi", "update_my_vec_vec");
    var outerVecRpcProducer = builder.addVec();
    var innerVecRpcProducer = outerVecRpcProducer.addVec(); // First outer
    outerVecRpcProducer.addVec().addU64(1); // Second outer
    innerVecRpcProducer.addU64(10).addU64(11);
    var rpc = builder.getBytes();

    // We expect the following: [[10, 11]], [1]]
    var expectedBytes =
        concatBytes(
            bytesFromHex("ccc0d6db04"),
            // Two elements in outer vec
            bytesFromHex("00000002"),
            // First valueType has 10, 11
            bytesFromHex("00000002" + "000000000000000A000000000000000B"),
            bytesFromHex("000000010000000000000001"));
    assertThat(rpc).isEqualTo(expectedBytes);
  }

  @Test
  void noTypeChecking() {
    var builder = new RpcContractBuilder(new byte[4]);
    builder.addBool(false).addU64(2).addString("hello");
    var rpc = builder.getBytes();
    var expected = bytesFromHex("000000000000000000000000020000000568656c6c6f");
    assertThat(rpc).isEqualTo(expected);
  }

  @Test
  void toFewArguments() {
    var arg = new ArgumentAbi("valueType", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "fn2", new byte[1], List.of(arg));

    var contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(fn), new NamedTypeRef(0));

    assertThatThrownBy(() -> new RpcContractBuilder(contractAbi, "fn2").getBytes())
        .isInstanceOf(MissingElementException.class)
        .hasMessage("Missing argument 'valueType'");
  }

  @Test
  void tooManyArguments() {
    var arg = new ArgumentAbi("field1", new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var fn =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "fn1", new byte[1], List.of(arg));

    var contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(fn), new NamedTypeRef(0));

    var builder = new RpcContractBuilder(contractAbi, "fn1");
    builder.addU8((byte) 0);

    assertThatThrownBy(builder::addVec)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot add more arguments than the action expects.");
  }

  @Test
  void addSizedByteArrayWithType() {
    var spec = new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 1);
    var fnAbiWithType =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION,
            "action",
            new byte[] {2, 2, 2, 2},
            List.of(new ArgumentAbi("map", spec)));

    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(fnAbiWithType),
            new NamedTypeRef(0));

    var builder = new RpcContractBuilder(contractAbi, "action");
    builder.addSizedByteArray(new byte[] {1});

    assertThat(builder.getBytes()).isEqualTo(bytesFromHex("0202020201"));
  }

  @Test
  void addSizedByteArrayWithoutType() {
    var builder = new RpcContractBuilder(new byte[] {2, 2, 2, 2});
    builder.addSizedByteArray(new byte[] {1, 3});

    assertThat(builder.getBytes()).isEqualTo(bytesFromHex("020202020103"));
  }

  @Test
  void sameShortname() {
    var fnAbiAction =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION,
            "action",
            new byte[] {0},
            List.of(new ArgumentAbi("valueType", new SimpleTypeSpec(TypeSpec.TypeIndex.u32))));
    var fnAbiCallback =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.CALLBACK,
            "callback",
            new byte[] {0},
            List.of(new ArgumentAbi("valueType", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))));
    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(fnAbiAction, fnAbiCallback),
            new NamedTypeRef(0));
    var builderAction =
        new RpcContractBuilder(contractAbi, new byte[] {0}, ImplicitBinderAbi.DefaultKinds.ACTION);
    builderAction.addU32(1);
    var builderCallback =
        new RpcContractBuilder(
            contractAbi, new byte[] {0}, ImplicitBinderAbi.DefaultKinds.CALLBACK);
    builderCallback.addU8((byte) 1);
    assertThat(builderAction.getBytes()).isEqualTo(bytesFromHex("0000000001"));
    assertThat(builderCallback.getBytes()).isEqualTo(bytesFromHex("0001"));
  }

  @Test
  void cantFindFunction() {
    var fnAbiCallback =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.CALLBACK,
            "callback",
            new byte[] {0},
            List.of(new ArgumentAbi("valueType", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))));
    var contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(),
            List.of(fnAbiCallback),
            new NamedTypeRef(0));
    assertThatThrownBy(() -> new RpcContractBuilder(contractAbi, "action"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Contract does not contain contract action with name 'action'");
    assertThatThrownBy(
            () ->
                new RpcContractBuilder(
                    contractAbi, new byte[] {0}, ImplicitBinderAbi.DefaultKinds.ACTION))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract does not contain contract action with kind 'action' and shortname '0x00'");
  }

  @Test
  void zkExtraByte() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
    var abi = TestingHelper.getContractAbiFromFile("contract_secret_voting.abi");
    var builder = new RpcBuilder(abi, "openInvocation");
    builder.contractCall("start_vote_counting");
    var bytes = builder.getBytes();
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("0901"));
  }

  @Test
  void zkNotAction() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
    var builder = TestingHelper.createBuilderFromFile("contract_secret_voting.abi", "add_vote");
    var bytes = builder.getBytes();
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("40"));
  }

  @Test
  void canBuildU256Rpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_u256");
    BigInteger u256ValueBytes = BigInteger.valueOf(123);
    builder.addU256(u256ValueBytes);

    var bytes = builder.getBytes();
    var zeroPadding = new byte[31];
    var setU256ShortName = bytesFromHex("01");
    assertThat(bytes)
        .isEqualTo(
            concatBytes(setU256ShortName, concatBytes(zeroPadding, u256ValueBytes.toByteArray())));
  }

  @Test
  void throwsErrorOnTooLargeU256Length() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_u256");

    String u256MaxValueString =
        "115792089237316195423570985008687907853269984665640564039457584007913129639935";
    String singleByteString = "01";
    BigInteger thirtyThreeByteInteger = new BigInteger(u256MaxValueString + singleByteString);
    assertThat(thirtyThreeByteInteger.toByteArray().length).isEqualTo(33);
    builder.addU256(thirtyThreeByteInteger);
    assertThatThrownBy(builder::getBytes)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot write BigInteger as 32 bytes; requires at least 33 bytes");
  }

  @Test
  void canBuildHashRpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_hash");
    builder.addHash(bytesFromHex(ValidTestHexValues.HASH));

    var bytes = builder.getBytes();

    var setHashShortName = bytesFromHex("02");
    assertThat(bytes)
        .isEqualTo(concatBytes(setHashShortName, bytesFromHex(ValidTestHexValues.HASH)));
  }

  @Test
  void throwsErrorOnIncorrectHashLength() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_hash");

    assertThatThrownBy(() -> builder.addHash(bytesFromHex("010203")))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In set_hash/hash, Hash must have length 32 bytes, got length = 3, value = 010203");
  }

  @Test
  void canBuildPublicKeyRpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_public_key");
    builder.addPublicKey(bytesFromHex(ValidTestHexValues.PUBLIC_KEY));

    var bytes = builder.getBytes();

    var setPublicKeyShortName = bytesFromHex("03");
    assertThat(bytes)
        .isEqualTo(concatBytes(setPublicKeyShortName, bytesFromHex(ValidTestHexValues.PUBLIC_KEY)));
  }

  @Test
  void throwsErrorOnIncorrectPublicKeyLength() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_public_key");

    assertThatThrownBy(() -> builder.addPublicKey(bytesFromHex("010203")))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In set_public_key/public_key, Public key must have length 33 bytes, got length = 3,"
                + " value = 010203");
  }

  @Test
  void canBuildSignatureRpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_signature");
    builder.addSignature(bytesFromHex(ValidTestHexValues.SIGNATURE));

    var bytes = builder.getBytes();

    var setSignatureShortName = bytesFromHex("04");
    assertThat(bytes)
        .isEqualTo(concatBytes(setSignatureShortName, bytesFromHex(ValidTestHexValues.SIGNATURE)));
  }

  @Test
  void throwsErrorOnIncorrectSignatureLength() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_signature");

    assertThatThrownBy(() -> builder.addSignature(bytesFromHex("010203")))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In set_signature/signature, Signature must have length 65 bytes, got length = 3, value"
                + " = 010203");
  }

  @Test
  void canBuildBlsPublicKeyRpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_bls_public_key");
    builder.addBlsPublicKey(bytesFromHex(ValidTestHexValues.BLS_PUBLIC_KEY));

    var bytes = builder.getBytes();

    var setBlsPublicKeyShortName = bytesFromHex("05");
    assertThat(bytes)
        .isEqualTo(
            concatBytes(setBlsPublicKeyShortName, bytesFromHex(ValidTestHexValues.BLS_PUBLIC_KEY)));
  }

  @Test
  void throwsErrorOnIncorrectBlsPublicKeyLength() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_bls_public_key");

    assertThatThrownBy(() -> builder.addBlsPublicKey(bytesFromHex("010203")))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In set_bls_public_key/bls_public_key, Bls public key must have length 96 bytes, got"
                + " length = 3, value = 010203");
  }

  @Test
  void canBuildBlsSignatureRpc() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_bls_signature");
    builder.addBlsSignature(bytesFromHex(ValidTestHexValues.BLS_SIGNATURE));

    var bytes = builder.getBytes();

    var setBlsSignatureShortName = bytesFromHex("06");
    assertThat(bytes)
        .isEqualTo(
            concatBytes(setBlsSignatureShortName, bytesFromHex(ValidTestHexValues.BLS_SIGNATURE)));
  }

  @Test
  void throwsErrorOnIncorrectBlsSignatureLength() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_types_hash_to_bls_signature_v5.abi", "set_bls_signature");

    assertThatThrownBy(() -> builder.addBlsSignature(bytesFromHex("010203")))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage(
            "In set_bls_signature/bls_signature, Bls signature must have length 48 bytes, got"
                + " length = 3, value = 010203");
  }

  @Test
  void zkSecretInput() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
    RpcContractBuilder builder =
        TestingHelper.createBuilderFromFile("zk_liquidity_swap.abi", "swap");
    builder.addBool(true);
    var bytes = builder.getBytes();
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("1301"));
  }

  @Test
  void cannotBuildRpcForBinderOnlyInvocation() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    assertThatThrownBy(() -> new RpcContractBuilder(abi, "rejectInput"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Contract does not contain contract action with name 'rejectInput'");
  }
}
