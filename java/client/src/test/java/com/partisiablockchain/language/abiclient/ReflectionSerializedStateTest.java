package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.server.rest.StateAbiSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

@SuppressWarnings({"unused"})
final class ReflectionSerializedStateTest {
  private static final byte[] ENCODED_EC_POINT =
      Curve.CURVE.getG().multiply(BigInteger.valueOf(777)).getEncoded(false);
  private static final BlsKeyPair BLS_KEY_PAIR = new BlsKeyPair(BigInteger.TEN);

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class AllTypesState implements StateSerializable {
    // Fields are sorted alphabetically
    private final byte aaByte;
    private final Byte bbByte;
    private final short ccShort;
    private final Short ddShort;
    private final int eeInt;
    private final Integer ffInt;
    private final long ggLong;
    private final Long hhLong;
    private final StateLong iiStateLong;
    private final String jjString;
    private final StateString kkStateString;
    private final boolean llBool;
    private final Boolean mmBool;
    private final StateBoolean nnStateBool;
    private final BlockchainAddress ooAddress;
    private final LargeByteArray ppLargeArray;
    private final Unsigned256 qqUnsigned256;
    private final BlockchainPublicKey rrBlockchainPublicKey;
    private final BlsPublicKey ssBlsPublicKey;
    private final BlsSignature ttBlsSignature;
    private final Signature uuSignature;
    private final Hash vvHash;
    private final String wwNullString;

    AllTypesState(
        byte aaByte,
        Byte bbByte,
        short ccShort,
        Short ddShort,
        int eeInt,
        Integer ffInt,
        long ggLong,
        Long hhLong,
        StateLong iiStateLong,
        String jjString,
        StateString kkStateString,
        boolean llBool,
        Boolean mmBool,
        StateBoolean nnStateBool,
        BlockchainAddress ooAddress,
        LargeByteArray ppLargeArray,
        Unsigned256 qqUnsigned256,
        BlockchainPublicKey rrBlockchainPublicKey,
        BlsPublicKey ssBlsPublicKey,
        BlsSignature ttBlsSignature,
        Signature uuSignature,
        Hash vvHash,
        String wwNullString) {
      this.aaByte = aaByte;
      this.bbByte = bbByte;
      this.ccShort = ccShort;
      this.ddShort = ddShort;
      this.eeInt = eeInt;
      this.ffInt = ffInt;
      this.ggLong = ggLong;
      this.hhLong = hhLong;
      this.iiStateLong = iiStateLong;
      this.jjString = jjString;
      this.kkStateString = kkStateString;
      this.llBool = llBool;
      this.mmBool = mmBool;
      this.nnStateBool = nnStateBool;
      this.ooAddress = ooAddress;
      this.ppLargeArray = ppLargeArray;
      this.qqUnsigned256 = qqUnsigned256;
      this.rrBlockchainPublicKey = rrBlockchainPublicKey;
      this.ssBlsPublicKey = ssBlsPublicKey;
      this.ttBlsSignature = ttBlsSignature;
      this.uuSignature = uuSignature;
      this.vvHash = vvHash;
      this.wwNullString = wwNullString;
    }
  }

  @Test
  void contractAllTypes() {
    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));
    AllTypesState exampleContractState =
        new AllTypesState(
            (byte) 1,
            (byte) 2,
            (short) 3,
            (short) 4,
            5,
            6,
            7L,
            8L,
            new StateLong(9L),
            "Hello",
            new StateString("World"),
            true,
            false,
            new StateBoolean(true),
            BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
            new LargeByteArray(new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}),
            Unsigned256.create(16L),
            BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT),
            BLS_KEY_PAIR.getPublicKey(),
            BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))),
            signature,
            Hash.fromString("0400000000000000000000000000000000000000000000000000000000000004"),
            null);

    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleContractState);
            });

    StructTypeSpec structTypeSpec =
        new StructTypeSpec(
            "AllTypes",
            List.of(
                new FieldAbi("aaByte", new SimpleTypeSpec(TypeSpec.TypeIndex.i8)),
                new FieldAbi(
                    "bbByte", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i8))),
                new FieldAbi("ccShort", new SimpleTypeSpec(TypeSpec.TypeIndex.i16)),
                new FieldAbi(
                    "ddShort", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i16))),
                new FieldAbi("eeInt", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                new FieldAbi(
                    "ffInt", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
                new FieldAbi("ggLong", new SimpleTypeSpec(TypeSpec.TypeIndex.i64)),
                new FieldAbi(
                    "hhLong", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
                new FieldAbi(
                    "iiStateLong", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i64))),
                new FieldAbi(
                    "jjString", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String))),
                new FieldAbi(
                    "kkStateString",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String))),
                new FieldAbi("llBool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                new FieldAbi(
                    "mmBool", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool))),
                new FieldAbi(
                    "nnStateBool", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool))),
                new FieldAbi(
                    "ooAddress",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.Address))),
                new FieldAbi(
                    "ppLargeArray",
                    new OptionTypeSpec(new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                new FieldAbi(
                    "qqUnsigned256",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u256))),
                new FieldAbi(
                    "rrBlockchainPublicKey",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.PublicKey))),
                new FieldAbi(
                    "ssBlsPublicKey",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.BlsPublicKey))),
                new FieldAbi(
                    "ttBlsSignature",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.BlsSignature))),
                new FieldAbi(
                    "uuSignature",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.Signature))),
                new FieldAbi(
                    "vvHash", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.Hash))),
                new FieldAbi(
                    "wwNullString",
                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.String)))));

    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structTypeSpec),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    StateReader stateReader = StateReader.create(bytes, abi);
    ScValueStruct value = stateReader.readState();

    assertThat(value.getFieldValue("aaByte").i8Value()).isEqualTo((byte) 1);
    assertThat(value.getFieldValue("bbByte").optionValue().innerValue().i8Value())
        .isEqualTo((byte) 2);
    assertThat(value.getFieldValue("ccShort").i16Value()).isEqualTo((short) 3);
    assertThat(value.getFieldValue("ddShort").optionValue().innerValue().i16Value())
        .isEqualTo((short) 4);
    assertThat(value.getFieldValue("eeInt").i32Value()).isEqualTo(5);
    assertThat(value.getFieldValue("ffInt").optionValue().innerValue().i32Value()).isEqualTo(6);
    assertThat(value.getFieldValue("ggLong").i64Value()).isEqualTo(7L);
    assertThat(value.getFieldValue("hhLong").optionValue().innerValue().i64Value()).isEqualTo(8L);
    assertThat(value.getFieldValue("iiStateLong").optionValue().innerValue().i64Value())
        .isEqualTo(9L);
    assertThat(value.getFieldValue("jjString").optionValue().innerValue().stringValue())
        .isEqualTo("Hello");
    assertThat(value.getFieldValue("kkStateString").optionValue().innerValue().stringValue())
        .isEqualTo("World");
    assertThat(value.getFieldValue("llBool").boolValue()).isEqualTo(true);
    assertThat(value.getFieldValue("mmBool").optionValue().innerValue().boolValue())
        .isEqualTo(false);
    assertThat(value.getFieldValue("nnStateBool").optionValue().innerValue().boolValue())
        .isEqualTo(true);
    assertThat(value.getFieldValue("ooAddress").optionValue().innerValue().addressValue().bytes())
        .isEqualTo(HexFormat.of().parseHex("000000000000000000000000000000000000000042"));
    assertThat(value.getFieldValue("qqUnsigned256").optionValue().innerValue().u256Value())
        .isEqualTo(new BigInteger("16"));
    assertThat(
            value
                .getFieldValue("rrBlockchainPublicKey")
                .optionValue()
                .innerValue()
                .publicKeyValue()
                .bytes())
        .isEqualTo(BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT).asBytes());
    assertThat(
            value
                .getFieldValue("ssBlsPublicKey")
                .optionValue()
                .innerValue()
                .blsPublicKeyValue()
                .bytes())
        .isEqualTo(BLS_KEY_PAIR.getPublicKey().getPublicKeyValue().serialize(true));
    assertThat(
            value
                .getFieldValue("ttBlsSignature")
                .optionValue()
                .innerValue()
                .blsSignatureValue()
                .bytes())
        .isEqualTo(
            BLS_KEY_PAIR
                .sign(Hash.create(s -> s.writeString("message")))
                .getSignatureValue()
                .serialize(true));
    assertThat(
            value.getFieldValue("uuSignature").optionValue().innerValue().signatureValue().bytes())
        .isEqualTo(HexFormat.of().parseHex(signature.writeAsString()));
    assertThat(value.getFieldValue("vvHash").optionValue().innerValue().hashValue().bytes())
        .isEqualTo(
            HexFormat.of()
                .parseHex("0400000000000000000000000000000000000000000000000000000000000004"));
    assertThat(value.getFieldValue("wwNullString").optionValue().isSome()).isEqualTo(false);

    ScValueVector valueVector =
        value.getFieldValue("ppLargeArray").optionValue().innerValue().vecValue();
    assertThat(valueVector.size()).isEqualTo(16);
    for (int i = 0; i < valueVector.size(); i++) {
      assertThat(valueVector.get(i).u8Value()).isEqualTo((byte) i);
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleClass implements StateSerializable {
    private final boolean myBool;
    private final int state;

    ExampleClass(boolean myBool, int state) {
      this.myBool = myBool;
      this.state = state;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleClassField implements StateSerializable {
    private final ExampleClass myFieldClass;
    private final int myInt;

    ExampleClassField(ExampleClass myFieldClass, int myInt) {
      this.myFieldClass = myFieldClass;
      this.myInt = myInt;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleListClass implements StateSerializable {
    private final boolean myBool;
    private final FixedList<Integer> myList;

    ExampleListClass(boolean myBool, FixedList<Integer> myList) {
      this.myBool = myBool;
      this.myList = myList;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleTreeClass implements StateSerializable {
    private final boolean myBool;
    private final AvlTree<Integer, Boolean> myTree;

    ExampleTreeClass(boolean myBool, AvlTree<Integer, Boolean> myTree) {
      this.myBool = myBool;
      this.myTree = myTree;
    }
  }

  @Test
  void fieldClass() {
    ExampleClass exampleClass = new ExampleClass(true, 0x42);
    ExampleClassField exampleClassField = new ExampleClassField(exampleClass, 1);
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClassField);
            });

    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                new StructTypeSpec(
                    "ExampleClass",
                    List.of(
                        new FieldAbi("myBool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                        new FieldAbi("myInt", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))),
                new StructTypeSpec(
                    "ExampleClassField",
                    List.of(
                        new FieldAbi("myFieldClass", new OptionTypeSpec(new NamedTypeRef(0))),
                        new FieldAbi("myInt", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))))),
            List.of(),
            List.of(),
            new NamedTypeRef(1));

    StateReader stateReader = StateReader.create(bytes, abi);
    ScValueStruct value = stateReader.readState();

    assertThat(value.getFieldValue("myInt").i32Value()).isEqualTo(1);
    ScValueStruct fieldValue =
        value.getFieldValue("myFieldClass").optionValue().innerValue().structValue();
    assertThat(fieldValue.getFieldValue("myBool").boolValue()).isEqualTo(true);
    assertThat(fieldValue.getFieldValue("myInt").i32Value()).isEqualTo(0x42);
  }

  @Test
  void listClass() {
    ExampleListClass exampleClass = new ExampleListClass(true, FixedList.create(List.of(1, 2, 3)));
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClass);
            });

    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                new StructTypeSpec(
                    "ExampleListClass",
                    List.of(
                        new FieldAbi("myBool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                        new FieldAbi(
                            "myList",
                            new OptionTypeSpec(
                                new VecTypeSpec(
                                    new OptionTypeSpec(
                                        new SimpleTypeSpec(TypeSpec.TypeIndex.i32)))))))),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    StateReader stateReader = StateReader.create(bytes, abi);
    ScValueStruct value = stateReader.readState();
    assertThat(value.getFieldValue("myBool").boolValue()).isEqualTo(true);

    ScValueVector vecValue = value.getFieldValue("myList").optionValue().innerValue().vecValue();
    assertThat(vecValue.size()).isEqualTo(3);
    assertThat(vecValue.get(0).optionValue().innerValue().i32Value()).isEqualTo(1);
    assertThat(vecValue.get(1).optionValue().innerValue().i32Value()).isEqualTo(2);
    assertThat(vecValue.get(2).optionValue().innerValue().i32Value()).isEqualTo(3);
  }

  @Test
  void treeClass() {
    AvlTree<Integer, Boolean> tree = AvlTree.create();
    tree = tree.set(1, false);
    tree = tree.set(2, false);
    tree = tree.set(3, true);
    ExampleTreeClass exampleClass = new ExampleTreeClass(true, tree);
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClass);
            });
    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                new StructTypeSpec(
                    "ExampleListClass",
                    List.of(
                        new FieldAbi("myBool", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                        new FieldAbi(
                            "myTree",
                            new OptionTypeSpec(
                                new MapTypeSpec(
                                    new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                                    new OptionTypeSpec(
                                        new SimpleTypeSpec(TypeSpec.TypeIndex.bool)))))))),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    StateReader stateReader = StateReader.create(bytes, abi);
    ScValueStruct value = stateReader.readState();
    assertThat(value.getFieldValue("myBool").boolValue()).isEqualTo(true);

    ScValueMap mapValue = value.getFieldValue("myTree").optionValue().innerValue().mapValue();
    assertThat(mapValue.size()).isEqualTo(3);
    assertThat(
            mapValue
                .get(new ScValueOption(new ScValueI32(1)))
                .optionValue()
                .innerValue()
                .boolValue())
        .isFalse();
    assertThat(
            mapValue
                .get(new ScValueOption(new ScValueI32(2)))
                .optionValue()
                .innerValue()
                .boolValue())
        .isFalse();
    assertThat(
            mapValue
                .get(new ScValueOption(new ScValueI32(3)))
                .optionValue()
                .innerValue()
                .boolValue())
        .isTrue();
  }
}
