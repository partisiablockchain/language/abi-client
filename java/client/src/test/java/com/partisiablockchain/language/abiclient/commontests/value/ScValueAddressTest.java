package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

final class ScValueAddressTest {

  @Test
  void invalidAddress() {
    assertThatThrownBy(() -> new ScValueAddress(new byte[2]))
        .isInstanceOf(ScReadException.class)
        .hasMessage("Address expects exactly 21 bytes, but found 2");
  }

  @Test
  void getType() {
    var address = new ScValueAddress(new byte[21]);
    assertThat(address.getType()).isEqualTo(TypeSpec.TypeIndex.Address);

    var addressTest = new ScValueAddress(new byte[21]);
    assertThat(addressTest.getType()).isEqualTo(TypeSpec.TypeIndex.Address);
  }

  @Test
  void value() {
    var rawValue = HexFormat.of().parseHex("010000000000000000000000000000000000000020");

    var addressTest = new ScValueAddress(rawValue);
    assertThat(addressTest.addressValue().bytes()).isEqualTo(rawValue);
  }
}
