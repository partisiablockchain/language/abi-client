package com.partisiablockchain.language.abiclient.blockchaintypes;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.jupiter.api.Test;

/** Tests for {@link AbstractBuilder} methods using blockchain types. */
public final class BlockchainTypesBuilderTest {

  /** The builder can add an address using the {@link BlockchainAddress} type. */
  @Test
  void buildStateWithAddress() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.Address);

    String validAddress = "0002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(validAddress);
    byte[] expected = bytesFromHex(validAddress);

    AbstractBuilder newBuilder = stateBuilder.addAddress(blockchainAddress);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder can add a signature using the {@link Signature} type. */
  @Test
  void buildStateWithSignature() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.Signature);

    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));
    String validSignature = signature.writeAsString();
    byte[] expected = bytesFromHex(validSignature);

    AbstractBuilder newBuilder = stateBuilder.addSignature(signature);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder can add a hash using the {@link Hash} type. */
  @Test
  void buildStateWithHash() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.Hash);

    String validHash = ValidTestHexValues.HASH;
    Hash hash = Hash.fromString(validHash);
    byte[] expected = bytesFromHex(validHash);

    AbstractBuilder newBuilder = stateBuilder.addHash(hash);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder can add a public key using the {@link BlockchainPublicKey} type. */
  @Test
  void buildStateWithPublicKey() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.PublicKey);

    ECPoint ecPoint = Curve.CURVE.getG().multiply(BigInteger.valueOf(777));
    String validPublicKey = HexFormat.of().formatHex(ecPoint.getEncoded(true));
    BlockchainPublicKey blockchainPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(ecPoint.getEncoded(false));
    byte[] expected = bytesFromHex(validPublicKey);

    AbstractBuilder newBuilder = stateBuilder.addPublicKey(blockchainPublicKey);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder can add a BLS signature using the {@link BlsSignature} type. */
  @Test
  void buildStateWithBlsSignature() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.BlsSignature);

    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.TEN);
    BlsSignature blsSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("message")));
    String validBlsSignature =
        HexFormat.of().formatHex(blsSignature.getSignatureValue().serialize(true));
    byte[] expected = bytesFromHex(validBlsSignature);

    AbstractBuilder newBuilder = stateBuilder.addBlsSignature(blsSignature);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder can add a BLS public key using the {@link BlsPublicKey} type. */
  @Test
  void buildStateWithBlsPublicKey() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.BlsPublicKey);

    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.TEN);
    BlsPublicKey blsPublicKey = blsKeyPair.getPublicKey();
    String validBlsPublicKey =
        HexFormat.of().formatHex(blsPublicKey.getPublicKeyValue().serialize(true));
    byte[] expected = bytesFromHex(validBlsPublicKey);

    AbstractBuilder newBuilder = stateBuilder.addBlsPublicKey(blsPublicKey);
    assertThat(newBuilder).isNotNull();
    byte[] actual = stateBuilder.getBytes();

    assertThat(actual).isEqualTo(expected);
  }

  /** The builder will fail when given the wrong type. */
  @Test
  void testNonExpectedTypes() {
    StateBuilder stateBuilder = stateBuilderFromTypeIndex(TypeSpec.TypeIndex.String);

    String validAddress = "0002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(validAddress);
    assertThatThrownBy(() -> stateBuilder.addAddress(blockchainAddress))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got Address");

    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));
    assertThatThrownBy(() -> stateBuilder.addSignature(signature))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got Signature");

    Hash hash = Hash.fromString(ValidTestHexValues.HASH);
    assertThatThrownBy(() -> stateBuilder.addHash(hash))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got Hash");

    ECPoint ecPoint = Curve.CURVE.getG().multiply(BigInteger.valueOf(777));
    BlockchainPublicKey blockchainPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(ecPoint.getEncoded(false));
    assertThatThrownBy(() -> stateBuilder.addPublicKey(blockchainPublicKey))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got PublicKey");

    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.TEN);
    BlsSignature blsSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("message")));
    assertThatThrownBy(() -> stateBuilder.addBlsSignature(blsSignature))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got BlsSignature");

    BlsPublicKey blsPublicKey = blsKeyPair.getPublicKey();
    assertThatThrownBy(() -> stateBuilder.addBlsPublicKey(blsPublicKey))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessageContaining("Expected type String, but got BlsPublicKey");
  }

  private StateBuilder stateBuilderFromTypeIndex(TypeSpec.TypeIndex typeIndex) {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                new StructTypeSpec("", List.of(new FieldAbi("", new SimpleTypeSpec(typeIndex))))),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    return new StateBuilder(contractAbi);
  }
}
