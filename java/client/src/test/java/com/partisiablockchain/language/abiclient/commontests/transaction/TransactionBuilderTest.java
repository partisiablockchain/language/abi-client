package com.partisiablockchain.language.abiclient.commontests.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.transaction.TransactionBuilder;
import java.util.HexFormat;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

final class TransactionBuilderTest {

  @Test
  void buildExecutableEvent() {
    var eventBuilder = TransactionBuilder.createEventBuilder();

    eventBuilder.addOption();

    var eventTransactionBuilder = eventBuilder.addStruct();
    eventTransactionBuilder.addHash(
        "740736d2ceae6405ab1d09351b05860d9c02228d953a4e0192e03a027f18ce90");

    var callbackEvent = eventTransactionBuilder.addEnumVariant(2).addEnumVariant(6).addStruct();
    callbackEvent.addStruct().addAddress("04fe17d1009372c8ed3ac5b790b32e349359c2c7e9");
    callbackEvent.addHash("fd51a6a025d01467ebc8de0d76aa4e7054e4f42ca64aa7f7d3d48c92319d42f8");
    callbackEvent.addBool(true);
    callbackEvent.addVecU8(
        HexFormat.of()
            .parseHex(
                "00000000000000000000000000000000000000000000000000000000"
                    + "00000000000000000000000000000000000000000000000000000000"));

    var shardRouteBuilder = eventTransactionBuilder.addStruct();
    shardRouteBuilder.addOption().addString("Shard1");
    shardRouteBuilder.addI64(1L);

    eventTransactionBuilder.addI64(0L);
    eventTransactionBuilder.addI64(0L);
    eventTransactionBuilder.addU8((byte) 3);

    eventTransactionBuilder.addOption();

    var bytes = eventBuilder.getBytes();
    assertThat(bytes)
        .isEqualTo(
            Base64.decode(
                "AHQHNtLOrmQFqx0JNRsFhg2cAiKNlTpOAZLgOgJ/GM6QAgYE/hfRAJNyyO06"
                    + "xbeQsy40k1nCx+n9UaagJdAUZ+vI3g12qk5wVOT0LKZKp/fT1IySMZ1C+AEA"
                    + "AAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                    + "AAAAAAAAAAAAAAAAAAABAAAABlNoYXJkMQAAAAAAAAABAAAAAAAAAAAAAAAA"
                    + "AAAAAAMA"));
  }

  @Test
  void buildSignedTransaction() {
    var transactionBuilder = TransactionBuilder.createTransactionBuilder();

    transactionBuilder.addSignature(
        "011c78637143139a10f13a57de898d6265f2a6a8d7e22c71096ea4e485fb37460a"
            + "77ae89d51e2d2cc92232fb835486dd8c81ad65f8df1cfe54ba8c218b1dffd409");

    var innerPartBuilder = transactionBuilder.addStruct();
    var coreBuilder = innerPartBuilder.addStruct();
    coreBuilder.addI64(1);
    coreBuilder.addI64(1661947817871L);
    coreBuilder.addI64(0);

    var interactBuilder = innerPartBuilder.addStruct();
    interactBuilder.addAddress("04203b77743ad0ca831df9430a6be515195733ad91");
    interactBuilder.addVecU8(HexFormat.of().parseHex("04000000010f"));

    var bytes = transactionBuilder.getBytes();
    assertThat(bytes)
        .isEqualTo(
            Base64.decode(
                "ARx4Y3FDE5oQ8TpX3omNYmXypqjX4ixxCW6k5IX7N0YKd66J1R4tLMkiMvuD"
                    + "VIbdjIGtZfjfHP5Uuowhix3/1AkAAAAAAAAAAQAAAYLzzy+PAAAAAAAAAAAE"
                    + "IDt3dDrQyoMd+UMKa+UVGVczrZEAAAAGBAAAAAEP"));
  }

  @Test
  void transactionBuilderErrorMessages() {
    var transactionBuilder = TransactionBuilder.createTransactionBuilder();
    assertThatThrownBy(() -> transactionBuilder.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /signature, Expected type Signature, but got bool");

    transactionBuilder.addSignature(
        "011c78637143139a10f13a57de898d6265f2a6a8d7e22c71096ea4e485fb37460a"
            + "77ae89d51e2d2cc92232fb835486dd8c81ad65f8df1cfe54ba8c218b1dffd409");

    var innerPartBuilder = transactionBuilder.addStruct();
    innerPartBuilder.addStruct();
    innerPartBuilder.addStruct();
    assertThatThrownBy(() -> transactionBuilder.addBool(false))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In root, Cannot add more arguments than the struct has fields.");
  }

  @Test
  void eventBuilderErrorMessages() {
    var eventBuilder = TransactionBuilder.createEventBuilder();
    assertThatThrownBy(() -> eventBuilder.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /origin_shard, Expected type Option, but got bool");

    eventBuilder.addOption();

    var eventTransactionBuilder = eventBuilder.addStruct();
    eventTransactionBuilder.addHash(
        "740736d2ceae6405ab1d09351b05860d9c02228d953a4e0192e03a027f18ce90");

    var callbackEvent = eventTransactionBuilder.addEnumVariant(2).addEnumVariant(6).addStruct();
    callbackEvent.addStruct().addAddress("04fe17d1009372c8ed3ac5b790b32e349359c2c7e9");

    eventTransactionBuilder.addStruct();
    assertThatThrownBy(() -> eventBuilder.addBool(false))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In root, Cannot add more arguments than the struct has fields.");
  }
}
