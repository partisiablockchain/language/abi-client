package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromStringLe;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.BuilderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class OptionProducerTest {

  @Test
  public void contractOption() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_options_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addOption().addU64(1);
    stateProducer.addOption().addString("option");
    stateProducer
        .addOption()
        .addAddress(HexFormat.of().parseHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"));
    stateProducer.addOption().addBool(false);
    stateProducer.addOption();
    byte[] state = stateProducer.getBytes();
    byte[] expected =
        concatBytes(
            new byte[] {0x01},
            HexFormat.of().parseHex("0100000000000000"),
            new byte[] {0x01},
            bytesFromStringLe("option"),
            new byte[] {0x01},
            HexFormat.of().parseHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
            new byte[] {0x01},
            new byte[] {0x00},
            new byte[] {0x00});
    assertThat(state).isEqualTo(expected);

    // Test missing element
    var producerNoOption = new StateBuilder(contractAbi);
    producerNoOption.addOption();
    assertThatThrownBy(producerNoOption::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In root, Missing argument 'option_string'");

    // Test without type checking
    var producerNoTypeCheck = new StateBuilder(null);
    producerNoTypeCheck.addOption().addString("hello");
    byte[] stateNoTypeCheck = BuilderHelper.builderToBytesLe(producerNoTypeCheck);
    byte[] expectedNoTypeCheck = concatBytes(new byte[] {0x01}, bytesFromStringLe("hello"));
    assertThat(stateNoTypeCheck).isEqualTo(expectedNoTypeCheck);

    // Test without type checking and without option
    var producerNoTypeCheckNoOption = new StateBuilder(null);
    producerNoTypeCheckNoOption.addOption();
    byte[] stateNoTypeCheckNoOption = BuilderHelper.builderToBytesLe(producerNoTypeCheckNoOption);
    assertThat(stateNoTypeCheckNoOption).isEqualTo(new byte[] {0x00});
  }

  @Test
  public void noTypeCheckState() {
    var stateProducer = new StateBuilder(null);
    stateProducer.addOption().addU8((byte) 0x0A);
    stateProducer.addOption();
    byte[] state = BuilderHelper.builderToBytesLe(stateProducer);
    byte[] expected = HexFormat.of().parseHex("010A00");
    assertThat(state).isEqualTo(expected);

    var producerWithTwoOptions = new StateBuilder(null);
    producerWithTwoOptions.addOption().addOption().addI8((byte) -1);
    byte[] stateTwoOptions = BuilderHelper.builderToBytesLe(producerWithTwoOptions);
    byte[] expectedTwoOptions = HexFormat.of().parseHex("0101ff");
    assertThat(stateTwoOptions).isEqualTo(expectedTwoOptions);
  }

  @Test
  public void doubleAdd() {
    var structAbi =
        new StructTypeSpec(
            "name",
            List.of(
                new FieldAbi("f", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u32)))));
    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    var stateProducer = new StateBuilder(simpleContractAbi);
    assertThatThrownBy(() -> stateProducer.addOption().addU32(1).addU32(1))
        .hasMessage("In /f, Cannot set option value twice.");
  }

  @Test
  public void invalidType() {
    var structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    var stateProducer = new StateBuilder(simpleContractAbi);

    assertThatThrownBy(() -> stateProducer.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /f, Expected type u64, but got bool");
  }

  @Test
  public void typeCheck() {
    var structAbi =
        new StructTypeSpec(
            "name",
            List.of(
                new FieldAbi("f", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)))));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(structAbi),
            List.of(),
            List.of(),
            new NamedTypeRef(0));

    StateBuilder stateBuilder = new StateBuilder(simpleContractAbi);

    assertThatThrownBy(() -> stateBuilder.addOption().addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /f, Expected type u64, but got bool");

    assertThatThrownBy(
            () ->
                BuilderHelper.builderToBytesLe(
                    new StateBuilder(simpleContractAbi).addOption().addOption()))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /f, Expected type u64, but got Option");
  }
}
