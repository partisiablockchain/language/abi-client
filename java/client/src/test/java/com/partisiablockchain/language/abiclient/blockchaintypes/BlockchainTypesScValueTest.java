package com.partisiablockchain.language.abiclient.blockchaintypes;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueBlsPublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abiclient.value.ScValueHash;
import com.partisiablockchain.language.abiclient.value.ScValuePublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test for {@link ScValue} accessors for blockchain types. */
public final class BlockchainTypesScValueTest {

  /** The user cannot convert a {@link ScValue} to an incompatible type. */
  @Test
  public void assertErrorsForBlockchainTypes() {
    var input = HexFormat.of().parseHex("010000000000000000000000000000000000000010" + "00");
    ContractAbi contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));
    StateReader reader = StateReader.create(input, contract);

    ScValue boolValue = reader.readStateValue(new SimpleTypeSpec(TypeSpec.TypeIndex.bool));

    assertThatThrownBy(boolValue::asBlockchainAddress)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read blockchain address for type: bool");
    assertThatThrownBy(boolValue::asSignature)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read signature for type: bool");
    assertThatThrownBy(boolValue::asHash)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read hash for type: bool");
    assertThatThrownBy(boolValue::asBlockchainPublicKey)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read blockchain public key for type: bool");
    assertThatThrownBy(boolValue::asBlsSignature)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read BLS signature for type: bool");
    assertThatThrownBy(boolValue::asBlsPublicKey)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read BLS public key for type: bool");
  }

  /** The user can convert a {@link ScValueAddress} to a {@link BlockchainAddress}. */
  @Test
  void asBlockchainAddressValid() {
    String validAddress = "0002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";
    BlockchainAddress address = BlockchainAddress.fromString(validAddress);

    ScValueAddress scValueAddress = new ScValueAddress(HexFormat.of().parseHex(validAddress));

    assertThat(scValueAddress.asBlockchainAddress()).isEqualTo(address);
  }

  /** The user can convert a {@link ScValueSignature} to a {@link Signature}. */
  @Test
  void asSignatureValid() {
    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));

    ScValueSignature scValueSignature =
        new ScValueSignature(HexFormat.of().parseHex(signature.writeAsString()));

    assertThat(scValueSignature.asSignature()).isEqualTo(signature);
  }

  /** The user can convert a {@link ScValueHash} to a {@link Hash}. */
  @Test
  void asHashValid() {
    String validHash = ValidTestHexValues.HASH;
    Hash hash = Hash.fromString(validHash);

    ScValueHash scValueHash = new ScValueHash(HexFormat.of().parseHex(validHash));

    assertThat(scValueHash.asHash()).isEqualTo(hash);
  }

  /** The user can convert a {@link ScValuePublicKey} to a {@link BlockchainPublicKey}. */
  @Test
  void asBlockchainPublicKeyValid() {
    byte[] ecPointEncoding = Curve.CURVE.getG().multiply(BigInteger.valueOf(777)).getEncoded(true);
    BlockchainPublicKey blockchainPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(ecPointEncoding);

    ScValuePublicKey scValuePublicKey = new ScValuePublicKey(ecPointEncoding);

    assertThat(scValuePublicKey.asBlockchainPublicKey()).isEqualTo(blockchainPublicKey);
  }

  /** The user can convert a {@link ScValueBlsSignature} to a {@link BlsSignature}. */
  @Test
  void asBlsSignatureValid() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.TEN);
    BlsSignature blsSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("message")));

    ScValueBlsSignature scValueBlsSignature =
        new ScValueBlsSignature(blsSignature.getSignatureValue().serialize(true));

    assertThat(scValueBlsSignature.asBlsSignature()).isEqualTo(blsSignature);
  }

  /** The user can convert a {@link ScValueBlsPublicKey} to a {@link BlsPublicKey}. */
  @Test
  void asBlsPublicKeyValid() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.TEN);
    BlsPublicKey blsPublicKey = blsKeyPair.getPublicKey();

    ScValueBlsPublicKey scValueBlsPublicKey =
        new ScValueBlsPublicKey(blsPublicKey.getPublicKeyValue().serialize(true));

    assertThat(scValueBlsPublicKey.asBlsPublicKey()).isEqualTo(blsPublicKey);
  }
}
