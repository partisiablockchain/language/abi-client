package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import org.junit.jupiter.api.Test;

final class ArrayStateProducerTest {

  @Test
  public void contractArray() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    final var contractAbi = TestingHelper.getContractAbiFromFile("contract_byte_arrays_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addSizedByteArray(
        new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
    stateProducer.addSizedByteArray(new byte[] {1, 2, 3, 4, 5});
    byte[] state = stateProducer.getBytes();
    byte[] expected =
        concatBytes(
            new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
            new byte[] {1, 2, 3, 4, 5});
    assertThat(state).isEqualTo(expected);

    var invalidStateProducer1 = new StateBuilder(contractAbi);
    invalidStateProducer1.addSizedByteArray(
        new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
    assertThatThrownBy(invalidStateProducer1::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In root, Missing argument 'my_array_2'");
  }

  /** {@code addSizedByteArray} must fail when given the wrong number of bytes. */
  @Test
  public void failWhenAddingTooMany() {
    final var contractAbi = TestingHelper.getContractAbiFromFile("contract_byte_arrays_v3.abi");
    var builder = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> builder.addSizedByteArray(new byte[100]))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage(
            "Array builder at /my_array/16: Array is already at max size 16, cannot add" + " more");
  }

  /** {@code addSizedByteArray} must fail when given the wrong number of bytes. */
  @Test
  public void failWhenAddingTooFew() {
    final var contractAbi = TestingHelper.getContractAbiFromFile("contract_byte_arrays_v3.abi");
    var builder = new StateBuilder(contractAbi);
    builder.addSizedByteArray(new byte[10]);
    assertThatThrownBy(() -> builder.getBytes())
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage(
            "Array builder at /my_array: Array with size 10 did not have the expected size 16");
  }

  @Test
  public void errorIfNoArrayType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    final var contractAbi = TestingHelper.getContractAbiFromFile("contract_booleans_v3.abi");
    var noArrayType = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> noArrayType.addSizedByteArray(new byte[] {1}))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_bool, Expected type bool, but got SizedArray");
  }
}
