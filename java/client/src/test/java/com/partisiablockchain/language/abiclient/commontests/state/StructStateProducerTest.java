package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.BuilderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class StructStateProducerTest {

  @Test
  void tooFew() {
    var structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    NamedTypeRef structRef = new NamedTypeRef(0);
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(structAbi), List.of(), List.of(), structRef);
    var producer = new StateBuilder(contractAbi);

    assertThatThrownBy(producer::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In root, Missing argument 'f'");
  }

  @Test
  void tooManyElements() {
    var structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));
    NamedTypeRef structRef = new NamedTypeRef(0);
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(structAbi), List.of(), List.of(), structRef);
    var producer = new StateBuilder(contractAbi);
    producer.addU64(1);

    assertThatThrownBy(() -> producer.addU64(1))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In root, Cannot add more arguments than the struct has fields.");
  }

  @Test
  void testSimpleStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addU64(2).addStruct().addU64(2).addVec().addU64(1).addU64(1);

    // Encoding the element: 2
    byte[] expectedForU64 = HexFormat.of().parseHex("0200000000000000");
    // 2 arguments (4 bytes) of u64 (8 bytes) each has valueType 01
    byte[] expectedForVec =
        HexFormat.of().parseHex("02000000" + "0100000000000000" + "0100000000000000");

    byte[] state = stateProducer.getBytes();
    byte[] expected = concatBytes(expectedForU64, expectedForU64, expectedForVec);
    assertThat(state).isEqualTo(expected);
  }

  @Test
  public void assertTypeError() {
    // Contract can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> stateProducer.addU64(1).addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_struct, Expected type Named, but got bool");

    // Contract can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    ContractAbi noStructTypeBinderAbi =
        TestingHelper.getContractAbiFromFile("contract_booleans_v3.abi");
    var noStructType = new StateBuilder(noStructTypeBinderAbi);
    assertThatThrownBy(noStructType::addStruct)
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_bool, Expected type bool, but got Named");
  }

  @Test
  public void assertTypeErrorInStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_simple_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    assertThatThrownBy(() -> stateProducer.addU64(1).addStruct().addString("string"))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_struct/some_value, Expected type u64, but got String");
  }

  @Test
  public void structOfStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
    ContractAbi contractAbi =
        TestingHelper.getContractAbiFromFile("contract_struct_of_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    stateProducer.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
    byte[] state = stateProducer.getBytes();
    byte[] structInBytes =
        concatBytes(
            HexFormat.of().parseHex("0100000000000000"),
            // One element
            bytesFromHex(
                "01000000"
                    + "0200000000000000"
                    // One element
                    + "01000000"
                    + "0300000000000000"));
    assertThat(state).isEqualTo(structInBytes);
  }

  @Test
  public void withoutProvidingTypeChecking() {
    var stateProducer = new StateBuilder(null);
    stateProducer.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
    byte[] state = BuilderHelper.builderToBytesLe(stateProducer);
    byte[] structInBytes =
        concatBytes(
            HexFormat.of().parseHex("0100000000000000"),
            // One element
            bytesFromHex(
                "01000000"
                    + "0200000000000000"
                    // One element
                    + "01000000"
                    + "0300000000000000"));
    assertThat(state).isEqualTo(structInBytes);
  }

  @Test
  public void moreStructureInVec() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
    ContractAbi contractAbi =
        TestingHelper.getContractAbiFromFile("contract_struct_of_struct_v3.abi");
    var stateProducer = new StateBuilder(contractAbi);
    var vecStateProducer = stateProducer.addStruct().addU64(0xFFFF_FFFF_FFFF_FFFFL).addVec();
    vecStateProducer.addStruct().addU64(1).addVec().addU64(2);
    vecStateProducer.addStruct().addU64(3).addVec().addU64(4);

    byte[] state = stateProducer.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("ffffffffffffffff"),
            // Two elements in Vec
            HexFormat.of().parseHex("02000000"),
            // First is struct with 1, Vec<2>
            HexFormat.of().parseHex("0100000000000000" + "01000000" + "0200000000000000"),
            // Second is struct with 3, <Vec<4>
            HexFormat.of().parseHex("0300000000000000" + "01000000" + "0400000000000000"));
    assertThat(state).isEqualTo(expected);
  }
}
