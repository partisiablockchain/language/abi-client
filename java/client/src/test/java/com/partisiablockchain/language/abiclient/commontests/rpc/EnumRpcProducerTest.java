package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.builder.EnumVariantProducer;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

final class EnumRpcProducerTest {

  @Test
  void fieldNameIsEmptyWhenTypeSpecIsNull() {
    EnumVariantProducer producer = new EnumVariantProducer(null, 0, "");
    assertThat(producer.getFieldName()).isEqualTo("/");
  }

  @Test
  public void assertTypeErrorInEnum() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
    var builder = TestingHelper.createBuilderFromFile("contract_enum.abi", "update_enum");
    assertThatThrownBy(() -> builder.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_enum/val, Expected type Named, but got bool");

    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    var noEnumType =
        TestingHelper.createBuilderFromFile("contract_booleans_v3.abi", "update_my_bool");
    assertThatThrownBy(() -> noEnumType.addEnumVariant(0))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_bool/value, Expected type bool, but got Named");

    ContractAbi contractAbi = TestingHelper.getContractAbiFromFile("contract_enum.abi");
    var stateProducer = new StateBuilder(contractAbi);

    assertThatThrownBy(() -> stateProducer.addEnumVariant(2).addU64(10))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In /my_enum/Bicycle/wheel_diameter, Expected type i32, but got u64");
  }

  @Test
  void missingArgument() {
    StructTypeSpec enumVariant =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    NamedTypeRef enumVariantRef = new NamedTypeRef(0);

    EnumTypeSpec enumTypeSpec =
        new EnumTypeSpec("enum", List.of(new EnumVariant(2, enumVariantRef)));

    StructTypeSpec state =
        new StructTypeSpec("State", List.of(new FieldAbi("my_enum", new NamedTypeRef(1))));

    NamedTypeRef stateRef = new NamedTypeRef(2);

    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(enumVariant, enumTypeSpec, state),
            List.of(),
            List.of(),
            stateRef);
    var producer = new StateBuilder(contractAbi);
    producer.addEnumVariant(2);

    assertThatThrownBy(producer::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In /my_enum, Missing argument 'f1'");
  }
}
