package com.partisiablockchain.language.abiclient.commontests.plugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.AccountPluginInvocationReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class ContractStateLocalTest {

  private AccountPluginInvocationReader reader;

  @BeforeEach
  void setup() {
    reader = new AccountPluginInvocationReader();
  }

  /**
   * 0x1f - initiate_byoc_transfer_sender -
   * d797e7765939515d172c7d21248fcae97ddf454153cc850c20330683cbec4778.
   */
  @Test
  void initiateByocTransferSender() {
    var sender = BlockchainAddress.fromString("0228fa167f6f85f2527476489257bca7dcbcc1de95");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiateByocTransferSender-"
                + "d797e7765939515d172c7d21248fcae97ddf454153cc850c20330683cbec4778.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_byoc_transfer_sender");
  }

  /**
   * 0x20 - initiate_byoc_transfer_recipient -
   * fe0aa17dfd26f8d53351f7d3161a5cbbe884839a0d405f6b619080ce36ed90e3.
   */
  @Test
  void initiateByocTransferRecipient() {
    var sender = BlockchainAddress.fromString("01d9f82e98a22b319aa371e752f3e0d85bd96c9545");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "initiate_byoc_transfer_recipient-"
                + "fe0aa17dfd26f8d53351f7d3161a5cbbe884839a0d405f6b619080ce36ed90e3.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_byoc_transfer_recipient");
  }

  /** 0x0d - commit_transfer - 01e3daa0bb94d41cae174b03a36bc5bf34f87a469a834304853e621e07f9661d. */
  @Test
  void commitTransfer() {
    var sender = BlockchainAddress.fromString("0226acb185bc2f1654112910f5bb15ee5e866ae4b2");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "commit_transfer-"
                + "01e3daa0bb94d41cae174b03a36bc5bf34f87a469a834304853e621e07f9661d.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("commit_transfer");
  }

  /** 0x0e - abort_transfer - 02c415d23f7c2f3c996a08f6e0a62ec403c50b6ec41f92828a5c2eff39bdf9dc. */
  @Test
  void abortTransfer() {
    var sender = BlockchainAddress.fromString("0264d53f0fc449f3f3c1da413ab501d63170e324e9");
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "abort_transfer-"
                + "02c415d23f7c2f3c996a08f6e0a62ec403c50b6ec41f92828a5c2eff39bdf9dc.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("abort_transfer");
  }

  /** 0x41 - faked initiate_ice_associate. */
  @Test
  void initiateIceAssociate() {
    var sender = BlockchainAddress.fromString("008d4296154ed5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_initiate_ice_associate.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_ice_associate");
  }

  /** 0x42 - faked initiate_ice_disassociate. */
  @Test
  void initiateIceDisassociate() {
    var sender = BlockchainAddress.fromString("008d4296123fd5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_initiate_ice_disassociate.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(4);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("initiate_ice_disassociate");
  }

  /** 0x25 - faked commit_ice. */
  @Test
  void commitIce() {
    var sender = BlockchainAddress.fromString("008d4296123fd5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_commit_ice.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("commit_ice");
  }

  /** 0x26 - faked abort_ice. */
  @Test
  void abortIce() {
    var sender = BlockchainAddress.fromString("008d4296123fd5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_abort_ice.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("abort_ice");
  }

  /** 0x49 - faked initiate_withdraw. */
  @Test
  void initiateMpcTokenWithdraw() {
    var sender = BlockchainAddress.fromString("008d4296123fd5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_initiate_mpc_token_withdraw.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("initiate_withdraw");
  }

  /** 0x12 - faked initiate_deposit. */
  @Test
  void initiateMpcTokenDeposit() {
    var sender = BlockchainAddress.fromString("008d4296123fd5cdb25130d7e188ca6b7e74032a17");
    var invocationBytes = TestingHelper.readPayloadFile("faked_initiate_mpc_token_deposit.txt");
    var value = reader.deserializeLocalStateUpdate(sender, invocationBytes);

    assertThat(value.contractArguments()).hasSize(2);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("initiate_deposit");
  }
}
