package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

final class VecRpcProducerTest {

  @Test
  public void assertTypeErrorInStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    var builder =
        TestingHelper.createBuilderFromFile("contract_simple_struct_v3.abi", "update_my_struct");
    assertThatThrownBy(() -> builder.addStruct().addU64(2).addVec().addString("valueType"))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_struct/value/some_vector/0, Expected type u64, but got String");

    var noVecBuilder =
        TestingHelper.createBuilderFromFile(
            "contract_simple_struct_v3.abi", "update_my_u64_using_struct");
    assertThatThrownBy(noVecBuilder::addVec)
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_u64_using_struct/value, Expected type Named, but got Vec");
  }

  @Test
  public void testVec() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
    var builder =
        TestingHelper.createBuilderFromFile("contract_simple_vector_v3.abi", "update_my_vec");
    builder.addVec().addU64(1).addU64(2).addU64(3);
    byte[] rpc = builder.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("9d99e6e401"),
            HexFormat.of().parseHex("00000003"),
            HexFormat.of().parseHex("0000000000000001"),
            HexFormat.of().parseHex("0000000000000002"),
            HexFormat.of().parseHex("0000000000000003"));
    assertThat(rpc).isEqualTo(expected);

    // Testing with no type checking
    var noTypeCheckBuilder = new RpcContractBuilder(HexFormat.of().parseHex("9d99e6e401"));
    noTypeCheckBuilder.addVec().addU64(1).addU64(2).addU64(3);
    assertThat(noTypeCheckBuilder.getBytes()).isEqualTo(expected);
  }
}
