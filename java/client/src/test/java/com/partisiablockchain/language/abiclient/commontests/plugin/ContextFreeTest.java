package com.partisiablockchain.language.abiclient.commontests.plugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.AccountPluginInvocationReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class ContextFreeTest {
  private AccountPluginInvocationReader reader;

  @BeforeEach
  void setup() {
    reader = new AccountPluginInvocationReader();
  }

  /** Collect fees for distribution. 0x09 - collect_fees_for_distribution - */
  @Test
  void collectFeesForDistribution() {
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "collectFeesForDistribution-"
                + "02610c0b0917e035f57aba7a6cadbf17176a0ce62420a35cc2a293e44639e4f5.txt");
    var value = reader.deserializeContextFreeInvocation(invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("collect_fees_for_distribution");
  }

  /**
   * Collect fees for distribution for both service and infrastructure. 0x14 -
   * collect_service_and_infrastructure_fees_for_distribution -
   */
  @Test
  void collectServiceAndInfrastructureFeesForDistribution() {
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "collectServiceAndInfrastructureFeesForDistribution-"
                + "0000c747975eeaaa98458ed9bffbef907bc93b5780ac576351130ed1d51acfaa.txt");
    var value = reader.deserializeContextFreeInvocation(invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("collect_service_and_infrastructure_fees_for_distribution");
  }
}
