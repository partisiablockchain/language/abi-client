package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcBuilder;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RpcBuilderTest {

  /** The pub binder invocation 'invoke' does not produce any shortname. */
  @Test
  void pubInvokeContractActionName() {
    var abi = TestingHelper.getContractAbiFromFile("voting.abi");
    var builder = new RpcBuilder(abi, "invoke");
    builder.contractCall("vote").addBool(true);
    assertThat(builder.getBytes()).isEqualTo(new byte[] {1, 1});
  }

  /** Can make a rpc builder by giving the shortname and kind of the binder invocation. */
  @Test
  void pubShortname() {
    var abi = TestingHelper.getContractAbiFromFile("voting.abi");
    var builder = new RpcBuilder(abi, null, TransactionKind.Action);
    builder.contractCall(new byte[] {1}).addBool(true);
    assertThat(builder.getBytes()).isEqualTo(new byte[] {1, 1});
  }

  /** The zk binder invocation 'openInvocation' produces the shortname '0x09'. */
  @Test
  void zkInvokeContractActionName() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "openInvocation");
    builder.contractCall("compute_average_salary");
    assertThat(builder.getBytes()).isEqualTo(new byte[] {9, 1});
  }

  /** The topUpGas invocation does not produce any rpc. */
  @Test
  void topUpGas() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "topUpGas");
    assertThat(builder.getBytes()).isEqualTo(new byte[] {});
  }

  /** Can build rpc for binder invocations without any contract calls. */
  @Test
  void zkRejectInput() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "rejectInput");
    builder.addI32(4);
    assertThat(builder.getBytes()).isEqualTo(new byte[] {6, 0, 0, 0, 4});
  }

  /** Arguments can be added to the binder invocation when building rpc. */
  @Test
  void zkSecretInput() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "zeroKnowledgeInputOnChain");
    builder.addVec().addI32(4);
    builder.addPublicKey("111111111111111111111111111111111111111111111111111111111111111111");
    builder.addVec().addStruct().addVecU8(new byte[] {1, 2, 3, 4});
    byte[] expected =
        HexFormat.of()
            .parseHex(
                "05" // binder shortname
                    + "0000000100000004" // bit lengths
                    // public key
                    + "111111111111111111111111111111111111111111111111111111111111111111"
                    + "000000010000000401020304" // encrypted shares
                    + "40"); // contract shortname
    builder.contractCall("add_salary");
    assertThat(builder.getBytes()).isEqualTo(expected);
  }

  /** Adding more arguments than the binder invocation expects crashes. */
  @Test
  void tooManyArguments() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "rejectInput");
    builder.addI32(4);
    assertThatThrownBy(builder::addVec)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot add more arguments than the invocation expects");
  }

  /** Fails to create a RpcBuilder if the binder invocation does not exist. */
  @Test
  void unknownInvocation() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    assertThatThrownBy(() -> new RpcBuilder(abi, (byte) 0x0f, TransactionKind.Callback))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract does not contain function with binder shortname '0x0f' and kind 'Callback'");
    assertThatThrownBy(() -> new RpcBuilder(abi, null, TransactionKind.Action))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract does not contain function with no binder shortname and kind 'Action'");
    assertThatThrownBy(() -> new RpcBuilder(abi, "unknownAction"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Contract does not contain function with name 'unknownAction'");
    assertThatThrownBy(() -> new RpcBuilder(abi, (byte) 0x0f, TransactionKind.Callback))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract does not contain function with binder shortname '0x0f' and kind 'Callback'");
    assertThatThrownBy(() -> new RpcBuilder(abi, null, TransactionKind.Action))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract does not contain function with no binder shortname and kind 'Action'");
  }

  /** Building rpc fails if not all arguments to the binder invocation are supplied. */
  @Test
  void missingBinderArgument() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "onVariableInputted");
    assertThatThrownBy(builder::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("Missing argument 'variableId'");
  }

  /** Adding an argument of the wrong type to a binder invocation fails. */
  @Test
  void wrongArgumentType() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "rejectInput");
    assertThatThrownBy(() -> builder.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In rejectInput/variableId, Expected type i32, but got bool");
  }

  /** Adding an argument of the wrong type to a contract call fails. */
  @Test
  void wrongContractArgumentType() {
    var abi = TestingHelper.getContractAbiFromFile("voting.abi");
    var builder = new RpcBuilder(abi, "invoke");
    var contractBuilder = builder.contractCall("vote");
    assertThatThrownBy(() -> contractBuilder.addI32(1))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In contractCall/vote/vote, Expected type bool, but got i32");
  }

  /** Building a contract call without any shortname, does not produce any shortname bytes. */
  @Test
  void writeActionWithoutShortname() {
    var abi = TestingHelper.getContractAbiFromFile("PublicDeployContract.abi");
    var builder = new RpcBuilder(abi, "upgrade");
    var address = HexFormat.of().parseHex("010000000000000000000000000000000000000001");
    builder.contractCall("upgrade").addAddress(address);
    assertThat(builder.getBytes()).isEqualTo(address);
  }

  /**
   * Trying to make a contract call to an action with different kind than the binder invocation
   * expects throws an error.
   */
  @Test
  void wrongContractCallKind() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "openInvocation");
    assertThatThrownBy(() -> builder.contractCall("add_salary"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Binder invocation 'openInvocation' attempts to call kind '2', "
                + "but action 'add_salary' has kind '23'");
  }

  /**
   * Trying to make a contract call for a binder invocation which expects no contract call throws an
   * error.
   */
  @Test
  void didNotExpectContractCall() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "rejectInput");
    assertThatThrownBy(() -> builder.contractCall("compute_average_salary"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Binder invocation 'rejectInput' does not call contract");
    assertThatThrownBy(() -> builder.contractCall(new byte[] {1}))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Binder invocation 'rejectInput' does not call contract");
  }

  /** Forgetting to make a contract call throws an error. */
  @Test
  void missingContractCall() {
    var abi = TestingHelper.getContractAbiFromFile("average_salary.abi");
    var builder = new RpcBuilder(abi, "openInvocation");
    assertThatThrownBy(builder::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("Missing contract call");
  }
}
