package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.LinkedHashMap;
import org.junit.jupiter.api.Test;

final class ScValueStructTest {

  @Test
  void getType() {
    var map = new LinkedHashMap<String, ScValue>();
    var val = new ScValueStruct("struct", map);
    assertThat(val.getType()).isEqualTo(TypeSpec.TypeIndex.Named);
  }

  @Test
  void values() {
    var map = new LinkedHashMap<String, ScValue>();
    map.put("a", new ScValueBool(true));
    map.put("b", new ScValueBool(false));
    var val = new ScValueStruct("struct", map);
    assertThat(val.name()).isEqualTo("struct");
    assertThat(val.size()).isEqualTo(2); // commontests-ignore-array
    assertThat(val.getFieldValue("a")).isNotNull();
    assertThat(val.getFieldValue("b")).isNotNull();
    assertThat(val.getFieldValue("c")).isNull();
  }
}
