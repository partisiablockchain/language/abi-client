package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromStringBe;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

@SuppressWarnings("PMD.TooManyStaticImports")
final class OptionProducerTest {

  @Test
  public void contractOption() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
    var builder = TestingHelper.createBuilderFromFile("contract_options_v3.abi", "update_u64");
    builder.addOption();
    byte[] rpc = builder.getBytes();
    byte[] expected = concatBytes(bytesFromHex("cf9cffe90b"), new byte[] {0x00});
    assertThat(rpc).isEqualTo(expected);
    var builderWithOption =
        TestingHelper.createBuilderFromFile("contract_options_v3.abi", "update_string");
    builderWithOption.addOption().addString("hello");

    byte[] hasOptionRpc = builderWithOption.getBytes();
    byte[] hasOptionExpected =
        concatBytes(bytesFromHex("f3eae9b808"), new byte[] {0x01}, bytesFromStringBe("hello"));
    assertThat(hasOptionRpc).isEqualTo(hasOptionExpected);

    // Test without type checking
    var builderNoTypeCheck = new RpcContractBuilder(HexFormat.of().parseHex("f3eae9b808"));
    builderNoTypeCheck.addOption().addString("hello");
    byte[] rpcNoTypeCheck = builderWithOption.getBytes();
    assertThat(rpcNoTypeCheck).isEqualTo(hasOptionExpected);

    var builderNoTypeCheckNoOption = new RpcContractBuilder(HexFormat.of().parseHex("cf9cffe90b"));
    builderNoTypeCheckNoOption.addOption();
    byte[] test = builderNoTypeCheckNoOption.getBytes();
    assertThat(test).isEqualTo(expected);
  }

  @Test
  public void noTypeCheckRpc() {
    var builder = new RpcContractBuilder(new byte[4]);
    builder.addOption().addU8((byte) 0x0A);
    builder.addOption();
    byte[] rpc = builder.getBytes();
    byte[] expected = HexFormat.of().parseHex("00000000010A00");
    assertThat(rpc).isEqualTo(expected);

    var builderWithTwoOptions = new RpcContractBuilder(new byte[4]);
    builderWithTwoOptions.addOption().addOption().addI8((byte) -1);
    byte[] rpcTwoOptions = builderWithTwoOptions.getBytes();
    byte[] expectedTwoOptions = HexFormat.of().parseHex("000000000101ff");
    assertThat(rpcTwoOptions).isEqualTo(expectedTwoOptions);
  }

  @Test
  public void doubleAdd() {
    var builder = new RpcContractBuilder(new byte[4]);
    assertThatThrownBy(() -> builder.addOption().addU8((byte) 0x0A).addU32(1))
        .hasMessage("In , Cannot set option value twice.");
  }

  @Test
  public void invalidType() {
    byte[] initShortname = HexFormat.of().parseHex("fffffffff0");
    var abi =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.INIT,
            "init",
            initShortname,
            List.of(new ArgumentAbi("int64", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    var simpleContractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(abi), new NamedTypeRef(0));
    var builder = new RpcContractBuilder(simpleContractAbi, "init");
    assertThatThrownBy(() -> builder.addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In init/int64, Expected type u64, but got bool");
  }

  @Test
  public void typeCheck() {
    byte[] initShortname = HexFormat.of().parseHex("fffffffff0");
    var abi =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.INIT,
            "init",
            initShortname,
            List.of(
                new ArgumentAbi(
                    "option", new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64)))));

    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(abi), new NamedTypeRef(0));

    assertThatThrownBy(() -> new RpcContractBuilder(contract, "init").addOption().addBool(true))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In init/option, Expected type u64, but got bool");

    assertThatThrownBy(() -> new RpcContractBuilder(contract, "init").addOption().addOption())
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In init/option, Expected type u64, but got Option");
  }
}
