package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import org.junit.jupiter.api.Test;

final class AccountPluginInvocationReaderTest {

  /**
   * Reading the account contract invocation abi file returns the bytes for the contract invocation
   * abi.
   */
  @Test
  void testReadAccountContractInvocation() {
    byte[] abiBytes = AccountPluginInvocationReader.readAbiFile("/account_contract_invocation.abi");
    assertNotNull(abiBytes);
    ContractAbi abi = new AbiParser(abiBytes).parseContractAbi();
    assertNotNull(abi.getNamedType("ContractInvocation"));
  }

  /** Reading the account invocation abi file returns the bytes for the account invocation abi. */
  @Test
  void testReadAccountInvocationBytes() {
    byte[] abiBytes = AccountPluginInvocationReader.readAbiFile("/account_invocation.abi");
    assertNotNull(abiBytes);
    ContractAbi abi = new AbiParser(abiBytes).parseContractAbi();
    assertNotNull(abi.getNamedType("AccountInvocation"));
  }

  /**
   * Reading the account state global update abi file returns the bytes for the account state global
   * abi.
   */
  @Test
  void testReadAccountGlobalState() {
    byte[] abiBytes = AccountPluginInvocationReader.readAbiFile("/account_state_global.abi");
    assertNotNull(abiBytes);
    ContractAbi abi = new AbiParser(abiBytes).parseContractAbi();
    assertNotNull(abi.getNamedType("AccountStateGlobal"));
  }

  /** Reading the account context free abi file returns the bytes for the context free abi. */
  @Test
  void testReadAccountContextFree() {
    byte[] abiBytes = AccountPluginInvocationReader.readAbiFile("/account_context_free.abi");
    assertNotNull(abiBytes);
    ContractAbi abi = new AbiParser(abiBytes).parseContractAbi();
    assertNotNull(abi.getNamedType("ContextFree"));
  }
}
