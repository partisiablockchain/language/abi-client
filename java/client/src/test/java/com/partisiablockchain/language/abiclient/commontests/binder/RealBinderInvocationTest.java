package com.partisiablockchain.language.abiclient.commontests.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.RealBinderInvocationReader;
import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Testing Real Binder invocation deserialization of contracts with default binder invocations. */
final class RealBinderInvocationTest {

  @Test
  void parseZkOnChainOutput() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("onChainOutputRpc.txt");
    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("onChainOutput");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(5);
    assertThat(rpcFn.binderArguments().get(1).vecU8Value().length).isEqualTo(32);
  }

  @Test
  void unableToCalculate() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("unableToCalculateRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("unableToCalculate");
    assertThat(rpcFn.binderArguments().get(0).asLong()).isEqualTo(1L);
  }

  @Test
  void openMaskedInput() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("openMaskedInputRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("openMaskedInput");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(3);
    assertThat(rpcFn.binderArguments().get(1).vecU8Value().length).isEqualTo(32);
  }

  @Test
  void zkInputOffChain() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("zkInputOffChainRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOffChain");
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_vote");
  }

  @Test
  void zkInputOnChain() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("zkInputOnChainRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOnChain");
    assertThat(rpcFn.binderArguments().size()).isEqualTo(3);
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_vote");
  }

  @Test
  void zkInputOnChainBid() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "addBidOnChainInput-"
                + "13bff76ba288b18b269c5a52104e063880c21f313aca8f0ddb865f98a670631f.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOnChain");
    assertThat(rpcFn.binderArguments().size()).isEqualTo(3);
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_bid");
  }

  @Test
  void zkInputOnChainIcrc() {
    var abi = TestingHelper.loadAbiParserFromFile("icrc.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "onSecretInputOnChain-"
                + "f6c3b0a19a057c56919c1875256641cd2c3af1589c5669a4c42bc65dc2f6bc79.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOnChain");
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("on_secret_input");
    assertThat(rpcFn.binderArguments().size()).isEqualTo(3);
    assertThat(rpcFn.contractArguments().size()).isEqualTo(2);
    assertThat(rpcFn.arguments().size()).isEqualTo(5); // commontests-ignore-arg
  }

  @Test
  void offChainInputBid() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "addBidOffChainInput-"
                + "a80593b37f98f4e3ed0de39361e4bd1b2f97f9242fc2c7c8b22bd127eb564a59.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOffChain");
    assertThat(rpcFn.binderArguments().size()).isEqualTo(5);
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_bid");
  }

  @Test
  void commitResultVariable() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "commitResultVariable-"
                + "09670cb2477596ab2254b5eea21331729e98f8cbd0215b55944efb88658709a3.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("commitResultVariable");
    assertThat(rpcFn.binderArguments().get(0).asLong()).isEqualTo(2110674L);
    assertThat(rpcFn.binderArguments().get(1).vecValue().values().size()).isEqualTo(1);
  }

  @Test
  void extendZkComputation() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "extendZkComputation-"
                + "4c5c658984b097efe2e250bba74d812eab74e10028f72d64ea9306b6f95789dd.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("extendZkComputationDeadline");
    assertThat(rpcFn.binderArguments().get(0).asBigInteger()).isEqualTo(BigInteger.valueOf(3024L));
    assertThat(rpcFn.binderArguments().get(1).asBigInteger()).isEqualTo(BigInteger.valueOf(1L));
    assertThat(rpcFn.binderArguments().get(2).asBigInteger())
        .isEqualTo(BigInteger.valueOf(86400000L));
    assertThat(rpcFn.binderArguments().get(3).asBigInteger())
        .isEqualTo(BigInteger.valueOf(15724800000L));
  }

  @Test
  void rejectInput() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("rejectInputRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("rejectInput");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(66);
  }

  @Test
  void onComputeComplete() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary_v5.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "onComputeComplete-"
                + "3c9349bf60aea72830f5b039709f87db26bdc75fd18f6dbd2f5bbc24bc87317b.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("onComputeComplete");
    assertThat(rpcFn.binderArguments().get(0).vecValue().values().size()).isEqualTo(2);
    assertThat(rpcFn.binderArguments().get(0).vecValue().values().get(0).asInt()).isEqualTo(40);
    assertThat(rpcFn.binderArguments().get(0).vecValue().values().get(1).asInt()).isEqualTo(41);
  }

  @Test
  void addAttestationSignature() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes = TestingHelper.readPayloadFile("addAttestationSignatureRpc.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("addAttestationSignature");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(1);
    assertThat(rpcFn.binderArguments().get(1).signatureValue().bytes().length).isEqualTo(65);
  }

  @Test
  void addAttestationSignatureTx() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "addAttestationSignature-"
                + "eb2222f6b49f0eda0b080f0510c24edfa149d2bbe5c2331b2dbd72604eeb3fe2.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("addAttestationSignature");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(1);
    assertThat(rpcFn.binderArguments().get(1).signatureValue().bytes().length).isEqualTo(65);
  }

  @Test
  void onAttestationComplete() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "onAttestationComplete-"
                + "5ded07939ccaff8f7275b4d46f1838954e92c7d6ddb299976ed1e679e70ab7e2.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("onAttestationComplete");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(1);
  }

  @Test
  void onVariableInputted() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary_v5.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "onVariableInputted-"
                + "8e0352e5a99316685c7bb99b67414c2bb9473b26a0ed386a83c299f13926f9a4.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("onVariableInputted");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(2);
  }

  @Test
  void onSecretInputOffChainWithSecret() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "addBidOffChainInput-"
                + "a80593b37f98f4e3ed0de39361e4bd1b2f97f9242fc2c7c8b22bd127eb564a59.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOffChain");
    assertThat(requireNonNull(rpcFn.contractInvocation()).name()).isEqualTo("add_bid");
    assertThat(rpcFn.binderArguments().get(0).vecValue().values().get(0).asInt()).isEqualTo(32);

    assertThat(rpcFn.binderArguments().get(1).hashValue().bytes().length).isEqualTo(32);
    assertThat(rpcFn.binderArguments().get(2).hashValue().bytes().length).isEqualTo(32);
    assertThat(rpcFn.binderArguments().get(3).hashValue().bytes().length).isEqualTo(32);
    assertThat(rpcFn.binderArguments().get(4).hashValue().bytes().length).isEqualTo(32);
  }

  @Test
  void getComputationDeadline() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "getComputationDeadline-"
                + "5e869d0fa21c67aaeeb466dc3adcf39132b4e9760b4c4323974c18c6baf5e252.txt");

    var rpcFn = RpcReader.create(invocationBytes, abi, TransactionKind.Action).readRpc();

    assertThat(rpcFn.binderInvocation().name()).isEqualTo("getComputationDeadline");
  }

  @Test
  void onVariablesOpened() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary_v5.abi").parseAbi();
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "onVariablesOpened-"
                + "9dc5c3835e084f83173ac5c865fb817b511a83352d68ad51783f74f8c487bde8.txt");

    var rpcReader = RpcReader.create(invocationBytes, abi, TransactionKind.Action);
    var rpcFn = rpcReader.readRpc();
    assertThat(rpcFn.binderInvocation().name()).isEqualTo("onVariablesOpened");
    assertThat(rpcFn.binderArguments().get(0).vecValue().values().get(0).asInt()).isEqualTo(4);
  }

  @Test
  void addBatchesTriple() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var rpc = new byte[] {0x12, 0x01, 0x00, 0x00, 0x00, 0x01};
    var rpcReader = RpcReader.create(rpc, abi, TransactionKind.Action);
    var rpcFn = rpcReader.readRpc();
    assertThat(rpcFn.binderInvocation().name()).isEqualTo("addBatches");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(1);
    assertThat(rpcFn.binderArguments().get(1).asInt()).isEqualTo(1);
  }

  @Test
  void addBatchesInputMask() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var rpc = new byte[] {0x12, 0x00, 0x00, 0x00, 0x00, 0x02};
    var rpcReader = RpcReader.create(rpc, abi, TransactionKind.Action);
    var rpcFn = rpcReader.readRpc();
    assertThat(rpcFn.binderInvocation().name()).isEqualTo("addBatches");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo(0);
    assertThat(rpcFn.binderArguments().get(1).asInt()).isEqualTo(2);
  }

  @Test
  void addExternalEvent() {
    var abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
    var rpc = new byte[] {0x14, 0x00, 0x00, 0x00, 0x04, 0x02, 0x01, 0x00};
    var rpcReader = RpcReader.create(rpc, abi, TransactionKind.Action);
    var rpcFn = rpcReader.readRpc();
    assertThat(rpcFn.binderInvocation().name()).isEqualTo("addExternalEvent");
    assertThat(rpcFn.binderArguments().get(0).asInt()).isEqualTo((byte) 4);
    assertThat(rpcFn.binderArguments().get(1).vecU8Value().length).isEqualTo(3);
  }

  /**
   * Test that trying to use the {@link RealBinderInvocationReader} for an invocation that is
   * abi-friendly fails.
   */
  @Test
  void abiFriendly() {
    var abi = TestingHelper.getContractAbiFromFile("zk_second_price.abi");
    RealBinderInvocationReader reader = new RealBinderInvocationReader(new byte[] {9}, abi);
    assertThatThrownBy(reader::readAbiUnfriendlyBinderInvocation)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Binder invocation '0x09' is not an abi unfriendly invocation");
  }
}
