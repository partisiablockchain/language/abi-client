# Shared Java/Typescript testing framework

## Why
By using the same test to test both ABI-clients we can ensure that both libraries behave 
identically, and it forces us to keep the libraries in sync when changes are made.

## How it works
It works by using sed to automatically convert tests written in Java into Typescript. This is 
done syntactically using the rules written in `scripts/convert-commontests-to-ts.sed`, which means that the Java 
tests must be written in a way that is close to Typescript. Differences between the two
languages that cannot be handled by sed are abstracted away into helper functions that is 
written in both Java an TS using the same signature.

## Running the conversion
Just run the command `scripts/generate-ts-commontests.sh` and any files placed in a subfolder under 
`commontests` will be converted to TS and placed in the abi-client-ts project in the corresponding 
folder.

## Limitations when writing Java tests
- Cannot use types that do not exist in TS. The easiest fix is to use `var` when declaring
variables which is then translated into `const`. Lists can only be used with `List.of(...)`.
- Cannot assign to a declared variable, as all variable declarations are translated into
`const`.
- When accessing list members using `a.get(...)`, this is translated into array indexing 
`a[...]`, but disallows the use of nested parentheses. For functions objects where `.get`
is also defined in TS and you don't want to translate it into array indexing, you can place
`// commontests-ignore-array` in the same line, and it won't translate. Same for `.size()` into 
`.length`.
- Helper functions must be implemented in both languages and placed in its own helper file in
the root of `commontests`. Defining new imports must be added into the header in 
`scripts/convert-commontests-to-ts.sed`.
- There is limited casting support. Casts must be on its own line as the top level expression.
- Cannot make use of @BeforeEach. Constants can be created with private final keywords.
- Option#None is encoded as null in Java and undefined in TS. Thus, we must use a
  comment: `null, // undefined` in Java to translate this to an undefined.
- ...

## Significant differences between the two libraries
- ScValueNumbers: In Java there is a class for each number type (ScValueI8, ScValueI16 etc.),
however as there in Typescript only exists number and BN types, this doesn't make much sense 
there. Therefore, in TS there is only a ScValueNumber class which takes as argument the type
of the number. The method asInt, asLong etc. have also been replaced with the methods asNumber
and asBN.
- FnKind is represented different in the two libraries. In Java, it is an Enum type with
multiple values. As this is not possible in TS, it is instead represented by a type that 
describes the different values of a FnKind, and an object FnKinds that maps the different kinds
into the type FnKind.
- Some records have extra methods in Java. As abi types are represented in Java using record 
classes, it is possible to define methods on these. This is not possible in TS as the types 
are represented using interfaces. Some of these methods include:
    - StructTypeSpec has methods .field and .getFieldByName
    - ContractInvocation has method .shortnameAsString

