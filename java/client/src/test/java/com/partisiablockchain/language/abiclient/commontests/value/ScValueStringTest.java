package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.value.ScValueString;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import org.junit.jupiter.api.Test;

final class ScValueStringTest {

  @Test
  void value() {
    var string = new ScValueString("test");
    assertThat(string.stringValue()).isEqualTo("test");
  }

  @Test
  void getType() {
    var value = new ScValueString("test");
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.String);

    var valueTest = new ScValueString("test");
    assertThat(valueTest.getType()).isEqualTo(TypeSpec.TypeIndex.String);
  }
}
