package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.abiclient.rpc.RpcValue;
import com.partisiablockchain.language.abiclient.value.JsonRpcConverter;
import com.partisiablockchain.language.abiclient.value.JsonValueConverter;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueArray;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueBlsPublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueHash;
import com.partisiablockchain.language.abiclient.value.ScValueI128;
import com.partisiablockchain.language.abiclient.value.ScValueI16;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueI8;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValuePublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abiclient.value.ScValueString;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueU128;
import com.partisiablockchain.language.abiclient.value.ScValueU16;
import com.partisiablockchain.language.abiclient.value.ScValueU32;
import com.partisiablockchain.language.abiclient.value.ScValueU64;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TypedJsonValueTest {

  /** Simple types can be serialized to json. */
  @Test
  void simpleTypes() {
    ScValue value = new ScValueBool(true);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"bool\",\"value\":true}");
    value = new ScValueI8((byte) -40);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"i8\",\"value\":-40}");
    value = new ScValueI16((short) 40);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"i16\",\"value\":40}");
    value = new ScValueI32(Integer.MAX_VALUE);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"i32\",\"value\":2147483647}");
    value = new ScValueI64(Long.MAX_VALUE);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"i64\",\"value\":9223372036854775807}");
    value = new ScValueI128(BigInteger.valueOf(-3));
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"i128\",\"value\":-3}");
    value = new ScValueU8((byte) 8);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"u8\",\"value\":8}");
    value = new ScValueU16((short) 8);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"u16\",\"value\":8}");
    value = new ScValueU32(8);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"u32\",\"value\":8}");
    value = new ScValueU64(8);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"u64\",\"value\":8}");
    value = new ScValueU128(new BigInteger("ffffffffffffffffffffffffffffffff", 16));
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"u128\",\"value\":340282366920938463463374607431768211455}");
    value = new ScValueString("Hello");
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"string\",\"value\":\"Hello\"}");
    byte[] bytes = new byte[21];
    Arrays.fill(bytes, (byte) 255);
    value = new ScValueAddress(bytes);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"address\",\"value\":\"ffffffffffffffffffffffffffffffffffffffffff\"}");
    value = new ScValueHash(new byte[32]);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"hash\",\"value\":"
                + "\"0000000000000000000000000000000000000000000000000000000000000000\"}");
    value = new ScValueSignature(new byte[65]);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"signature\",\"value\":"
                + "\"0000000000000000000000000000000000000000000000000000000000000000"
                + "000000000000000000000000000000000000000000000000000000000000000000\"}");
    value = new ScValuePublicKey(new byte[33]);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"publicKey\",\"value\":"
                + "\"000000000000000000000000000000000000000000000000000000000000000000\"}");
    value = new ScValueBlsSignature(new byte[48]);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"blsSignature\",\"value\":"
                + "\"000000000000000000000000000000000000000000000000"
                + "000000000000000000000000000000000000000000000000\"}");
    value = new ScValueBlsPublicKey(new byte[96]);
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo(
            "{\"@type\":\"blsPublicKey\",\"value\":"
                + "\"0000000000000000000000000000000000000000000000000000000000000000"
                + "0000000000000000000000000000000000000000000000000000000000000000"
                + "0000000000000000000000000000000000000000000000000000000000000000\"}");
  }

  /** Vector types can be serialized to json. */
  @Test
  void vec() {
    ScValue value1 = new ScValueI32(40);
    ScValue value2 = new ScValueI32(41);
    ScValue value3 = new ScValueI32(41);
    ScValue value = new ScValueVector(List.of(value1, value2, value3));
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "vec",
              "value" : [ {
                "@type" : "i32",
                "value" : 40
              }, {
                "@type" : "i32",
                "value" : 41
              }, {
                "@type" : "i32",
                "value" : 41
              } ]
            }""");
  }

  /** Map types can be serialized to json. */
  @Test
  void map() {
    LinkedHashMap<ScValue, ScValue> map = new LinkedHashMap<>();
    ScValue key1 = new ScValueString("3");
    ScValue value1 = new ScValueI32(40);
    map.put(key1, value1);
    ScValue key2 = new ScValueString("2");
    ScValue value2 = new ScValueI32(41);
    map.put(key2, value2);
    ScValue key3 = new ScValueString("1");
    ScValue value3 = new ScValueI32(41);
    map.put(key3, value3);
    ScValue value = new ScValueMap(map);
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "map",
              "value" : [ {
                "key" : {
                  "@type" : "string",
                  "value" : "3"
                },
                "value" : {
                  "@type" : "i32",
                  "value" : 40
                }
              }, {
                "key" : {
                  "@type" : "string",
                  "value" : "2"
                },
                "value" : {
                  "@type" : "i32",
                  "value" : 41
                }
              }, {
                "key" : {
                  "@type" : "string",
                  "value" : "1"
                },
                "value" : {
                  "@type" : "i32",
                  "value" : 41
                }
              } ]
            }""");
  }

  /** Option types can be serialized to json. */
  @Test
  void option() {
    ScValue value1 = new ScValueOption(new ScValueI32(40));
    ScValue value2 = new ScValueOption(null);
    assertThat(JsonValueConverter.toTypedJson(value1).toString())
        .isEqualTo("{\"@type\":\"option\",\"value\":{\"@type\":\"i32\",\"value\":40}}");
    assertThat(JsonValueConverter.toTypedJson(value2).toString())
        .isEqualTo("{\"@type\":\"option\",\"value\":null}");
  }

  /** Struct types can be serialized to json. */
  @Test
  void struct() {
    LinkedHashMap<String, ScValue> map = new LinkedHashMap<>();
    map.put("f1", new ScValueI32(40));
    map.put("f2", new ScValueI32(41));
    map.put("f3", new ScValueI32(42));
    ScValue value = new ScValueStruct("name", map);
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "struct",
              "value" : {
                "f1" : {
                  "@type" : "i32",
                  "value" : 40
                },
                "f2" : {
                  "@type" : "i32",
                  "value" : 41
                },
                "f3" : {
                  "@type" : "i32",
                  "value" : 42
                }
              }
            }""");
  }

  /** Enum types can be serialized to json. */
  @Test
  void enumValue() {
    LinkedHashMap<String, ScValue> map = new LinkedHashMap<>();
    map.put("f1", new ScValueI32(40));
    map.put("f2", new ScValueI32(41));
    map.put("f3", new ScValueI32(42));
    ScValueStruct structValue = new ScValueStruct("name", map);
    ScValue value = new ScValueEnum("enumName", structValue);
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "enum",
              "value" : {
                "@discriminant" : "name",
                "f1" : {
                  "@type" : "i32",
                  "value" : 40
                },
                "f2" : {
                  "@type" : "i32",
                  "value" : 41
                },
                "f3" : {
                  "@type" : "i32",
                  "value" : 42
                }
              }
            }""");
  }

  /** Enum types can be serialized to json. */
  @Test
  void nestedEnumValue() {
    LinkedHashMap<String, ScValue> map = new LinkedHashMap<>();
    map.put(
        "f1",
        new ScValueEnum(
            "innerEnumName", new ScValueStruct("innerVariantName", new LinkedHashMap<>())));
    map.put("f2", new ScValueI32(41));
    ScValueStruct structValue = new ScValueStruct("name", map);
    ScValue value = new ScValueEnum("enumName", structValue);
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "enum",
              "value" : {
                "@discriminant" : "name",
                "f1" : {
                  "@type" : "enum",
                  "value" : {
                    "@discriminant" : "innerVariantName"
                  }
                },
                "f2" : {
                  "@type" : "i32",
                  "value" : 41
                }
              }
            }""");
  }

  /** Avl tree map types can be serialized to json. */
  @Test
  void avlTreeMap() {
    LinkedHashMap<ScValue, ScValue> map = new LinkedHashMap<>();
    ScValue key1 = new ScValueString("3");
    ScValue value1 = new ScValueI32(40);
    map.put(key1, value1);
    ScValue key2 = new ScValueString("2");
    ScValue value2 = new ScValueI32(41);
    map.put(key2, value2);
    ScValue value = new ScValueAvlTreeMap(0, map);
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "avlTreeMap",
              "value" : {
                "treeId" : 0,
                "map" : [ {
                  "key" : {
                    "@type" : "string",
                    "value" : "3"
                  },
                  "value" : {
                    "@type" : "i32",
                    "value" : 40
                  }
                }, {
                  "key" : {
                    "@type" : "string",
                    "value" : "2"
                  },
                  "value" : {
                    "@type" : "i32",
                    "value" : 41
                  }
                } ]
              }
            }""");
    value =
        new ScValueVector(List.of(new ScValueAvlTreeMap(1, null), new ScValueAvlTreeMap(2, null)));
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "vec",
              "value" : [ {
                "@type" : "avlTreeMap",
                "value" : {
                  "treeId" : 1
                }
              }, {
                "@type" : "avlTreeMap",
                "value" : {
                  "treeId" : 2
                }
              } ]
            }""");
  }

  /** Array types can be serialized to json. */
  @Test
  void array() {
    ScValue value1 = new ScValueI32(40);
    ScValue value2 = new ScValueI32(41);
    ScValue value3 = new ScValueI32(42);
    ScValue value = new ScValueArray(List.of(value1, value2, value3));
    assertThat(JsonValueConverter.toTypedJson(value).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "@type" : "array",
              "value" : [ {
                "@type" : "i32",
                "value" : 40
              }, {
                "@type" : "i32",
                "value" : 41
              }, {
                "@type" : "i32",
                "value" : 42
              } ]
            }""");
  }

  /** Vec u8 types can be serialized to json. */
  @Test
  void vecU8() {
    ScValue value = ScValueVector.fromBytes(new byte[] {1, 2, 3});
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"vec\",\"value\":\"010203\"}");
  }

  /** An empty vec u8 is serialized as an empty array. */
  @Test
  void vecU8Empty() {
    ScValue value = ScValueVector.fromBytes(new byte[] {});
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"vec\",\"value\":[]}");
  }

  /** Array u8 types can be serialized to json. */
  @Test
  void arrayU8() {
    ScValue value = ScValueArray.fromBytes(new byte[] {1, 2, 3});
    assertThat(JsonValueConverter.toTypedJson(value).toString())
        .isEqualTo("{\"@type\":\"array\",\"value\":\"010203\"}");
  }

  /** Rpc values can be serialized to json. */
  @Test
  void fnRpcValue() {
    BinderInvocation binderInvocation =
        new BinderInvocation(TransactionKind.Action, "invoke", null, List.of(), 2);
    ContractInvocation fnAbi =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION,
            "action_name",
            new byte[] {1},
            List.of(
                new ArgumentAbi(
                    "arg1", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool))),
                new ArgumentAbi("arg2", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))));
    ScValue arg1 = new ScValueVector(List.of(new ScValueBool(true)));
    ScValue arg2 = new ScValueI32(4);
    RpcValue rpcVal = new RpcValue(binderInvocation, fnAbi, List.of(), List.of(arg1, arg2));

    assertThat(JsonRpcConverter.toTypedJson(rpcVal).toPrettyString())
        .isEqualToIgnoringWhitespace(
            """
            {
              "binderInvocationName" : "invoke",
              "binderArguments" : {},
              "actionName" : "action_name",
              "contractArguments" : {
                "arg1" : {
                  "@type" : "vec",
                  "value" : [ {
                    "@type" : "bool",
                    "value" : true
                  } ]
                },
                "arg2" : {
                  "@type" : "i32",
                  "value" : 4
                }
              }
            }""");
  }

  /** Rpc values, with only binder invocation can be serialized to json. */
  @Test
  void fnRpcValueOnlyBinder() {
    BinderInvocation binderInvocation =
        new BinderInvocation(
            TransactionKind.Action,
            "invoke",
            null,
            List.of(
                new ArgumentAbi(
                    "arg1", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.bool))),
                new ArgumentAbi("arg2", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
            null);
    ScValue arg1 = new ScValueVector(List.of(new ScValueBool(true)));
    ScValue arg2 = new ScValueI32(4);
    RpcValue rpcVal = new RpcValue(binderInvocation, null, List.of(arg1, arg2), List.of());

    assertThat(JsonRpcConverter.toTypedJson(rpcVal).toPrettyString())
        .isEqualToNormalizingNewlines(
            """
            {
              "binderInvocationName" : "invoke",
              "binderArguments" : {
                "arg1" : {
                  "@type" : "vec",
                  "value" : [ {
                    "@type" : "bool",
                    "value" : true
                  } ]
                },
                "arg2" : {
                  "@type" : "i32",
                  "value" : 4
                }
              }
            }""");
  }

  /** Multiple rpc values get serialized to JSOn correctly. */
  @Test
  void multipleRpc() {
    BinderInvocation invocation1 =
        new BinderInvocation(TransactionKind.Action, "name1", (byte) 1, List.of(), null);
    BinderInvocation invocation2 =
        new BinderInvocation(TransactionKind.Action, "name2", (byte) 2, List.of(), null);
    ContractInvocation fn1 =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "name1", new byte[] {1}, List.of());
    ContractInvocation fn2 =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION, "name2", new byte[] {2}, List.of());
    List<RpcValue> rpcValues =
        List.of(
            new RpcValue(invocation1, fn1, List.of(), List.of()),
            new RpcValue(invocation2, fn2, List.of(), List.of()));
    assertThat(new ObjectMapper().valueToTree(rpcValues).toPrettyString())
        .isEqualToIgnoringWhitespace(
            """
            [ {
              "binderInvocationName" : "name1",
              "binderArguments" : {},
              "actionName" : "name1",
              "contractArguments" : {}
            }, {
              "binderInvocationName" : "name2",
              "binderArguments" : {},
              "actionName" : "name2",
              "contractArguments" : {}
            } ]""");
  }
}
