package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.state.RustSyntaxStatePrinter;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.HexFormat;

/** Helper class for state tests. */
public final class StateReaderHelper {

  /**
   * Helper function to wrap a hex string into a StateReader under a given model.
   *
   * @param hex The hex string to wrap
   * @param contractAbi The model
   * @return the StateReader
   */
  public static StateReader wrap(String hex, ContractAbi contractAbi) {
    return StateReader.create(HexFormat.of().parseHex(hex), contractAbi);
  }

  /**
   * Helper function to wrap a hex string under no model.
   *
   * @param hex The hex string to wrap
   * @return The StateReader
   */
  public static StateReader wrap(String hex) {
    return wrap(hex, null);
  }

  /**
   * Helper function read bytes as a state and pretty print the state.
   *
   * @param abiFile The model for which to read the bytes under
   * @param bytes The bytes to read
   * @return A string representation of the read state
   */
  public static String stringifyBytes(String abiFile, byte[] bytes) {
    AbiParser parser = TestingHelper.loadAbiParserFromFile(abiFile);
    ContractAbi model = (ContractAbi) parser.parseAbi().chainComponent();
    StateReader reader = StateReader.create(bytes, model);
    ScValueStruct state = reader.readStateValue(model.stateType()).structValue();

    return RustSyntaxStatePrinter.printState(state);
  }

  /**
   * Helper function read a state file as a state and pretty print the state.
   *
   * @param abiFile The model for which to read the bytes under
   * @param stateFile The file to read
   * @return A string representation of the read state
   */
  public static String stringify(String abiFile, String stateFile) {
    byte[] bytes = readStateFile(stateFile);
    return stringifyBytes(abiFile, bytes);
  }

  /**
   * Helper function to read a state file.
   *
   * @param fileName The name of the state file to read
   * @return The bytes of file
   */
  public static byte[] readStateFile(String fileName) {
    try (InputStream in = StateReaderHelper.class.getResourceAsStream(fileName)) {
      return in.readAllBytes();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
