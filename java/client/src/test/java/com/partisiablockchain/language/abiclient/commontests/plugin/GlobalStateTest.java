package com.partisiablockchain.language.abiclient.commontests.plugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.AccountPluginInvocationReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class GlobalStateTest {

  private AccountPluginInvocationReader reader;

  @BeforeEach
  void setup() {
    reader = new AccountPluginInvocationReader();
  }

  /** - 0x0 - faked set_gas_price_1000_network. */
  @Test
  void setGasPrice1000Network() {
    var invocationBytes = TestingHelper.readPayloadFile("faked_set_gas_price_1000_network.txt");
    var value = reader.deserializeGlobalStateUpdate(invocationBytes);

    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("set_gas_price_1000_network");
  }

  /** - 0x1 - faked set_gas_price_1000_storage. */
  @Test
  void setGasPrice1000Storage() {
    var invocationBytes = TestingHelper.readPayloadFile("faked_set_gas_price_1000_storage.txt");

    var value = reader.deserializeGlobalStateUpdate(invocationBytes);
    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("set_gas_price_1000_storage");
  }

  /** - 0x2 - set_coin - 000f2b50331abb84166d59cf33c5816c762f034aebe04009862e7db8987e880e. */
  @Test
  void setCoin() {
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "set_coin-" + "000f2b50331abb84166d59cf33c5816c762f034aebe04009862e7db8987e880e.txt");

    var value = reader.deserializeGlobalStateUpdate(invocationBytes);
    assertThat(value.contractArguments()).hasSize(3);
    assertThat(requireNonNull(value.contractInvocation()).name()).isEqualTo("set_coin");
  }

  /**
   * - 0x3 - set_gas_price_for_1000_ice_stakes -
   * 50068e17118b4cb6903daa0e83249a85532e94ffc15cd238e40658d902f068f7.
   */
  @Test
  void setGasPriceFor1000IceStakes() {
    var invocationBytes =
        TestingHelper.readPayloadFile(
            "set_gas_price_for_1000_ice_stakes-"
                + "50068e17118b4cb6903daa0e83249a85532e94ffc15cd238e40658d902f068f7.txt");

    var value = reader.deserializeGlobalStateUpdate(invocationBytes);
    assertThat(value.contractArguments()).hasSize(1);
    assertThat(requireNonNull(value.contractInvocation()).name())
        .isEqualTo("set_gas_price_for_1000_ice_stakes");
  }
}
