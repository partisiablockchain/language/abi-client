package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.abiclient.rpc.RpcValue;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.JsonRpcConverter;
import com.partisiablockchain.language.abiclient.value.JsonValueConverter;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;

/** Helper class for testing values. */
public final class ValueHelper {

  /**
   * Helper function to whether a ScValue is parsed to Json correctly.
   *
   * @param scValue The ScValue to parse to Json
   * @param expected The expected Json object as string representation
   */
  public static void assertValueJson(ScValue scValue, String expected) {
    var mapper = new ObjectMapper();
    var actual = JsonValueConverter.toJson(scValue);
    var expectedJson =
        ExceptionConverter.call(
            () -> mapper.readValue(expected, JsonNode.class), "Unable to parse json");
    assertThat(actual).isEqualTo(expectedJson);
    var actualString = JsonValueConverter.toJsonString(scValue);
    var actualJson =
        ExceptionConverter.call(
            () -> mapper.readValue(actualString, JsonNode.class), "Unable to parse json string");
    assertThat(actual).isEqualTo(actualJson);
  }

  /**
   * Helper function to whether a RpcValueFn is parsed to Json correctly.
   *
   * @param rpcValue The RpcValueFn to parse to Json
   * @param expected The expected Json object as string representation
   */
  public static void assertRpcJson(RpcValue rpcValue, String expected) {
    var mapper = new ObjectMapper();
    var actual = JsonRpcConverter.toJson(rpcValue);
    var expectedJson =
        ExceptionConverter.call(
            () -> mapper.readValue(expected, JsonNode.class), "Unable to parse json");
    assertThat(actual).isEqualTo(expectedJson);
    var actualString = JsonRpcConverter.toJsonString(rpcValue);
    var actualJson =
        ExceptionConverter.call(
            () -> mapper.readValue(actualString, JsonNode.class), "Unable to parse json string");
    assertThat(actual).isEqualTo(actualJson);
  }

  /**
   * Helper function to load a state file into a ScValueStruct under a given model.
   *
   * @param abiFile The file name of the model
   * @param stateFile The file name of the state file
   * @return The loaded state struct
   */
  public static ScValueStruct loadState(String abiFile, String stateFile) {
    byte[] bytes = StateReaderHelper.readStateFile(stateFile);
    AbiParser parser = TestingHelper.loadAbiParserFromFile(abiFile);
    ContractAbi model = (ContractAbi) parser.parseAbi().chainComponent();
    StateReader reader = StateReader.create(bytes, model);
    return reader.readStateValue(model.stateType()).structValue();
  }
}
