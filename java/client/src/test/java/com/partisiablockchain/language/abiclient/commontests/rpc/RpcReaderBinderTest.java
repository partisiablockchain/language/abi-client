package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.rpc.RpcReadException;
import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Testing reading rpc for binder invocations. */
@SuppressWarnings("PMD.TooManyStaticImports")
final class RpcReaderBinderTest {

  /** Can read the binder arguments for a payload of an onSecretInputOnChain invocation. */
  @Test
  void zkSecretInputOnChain() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary.abi").parseAbi();
    var rpc = TestingHelper.readPayloadFile("onSecretInputOnChain-v6.txt");
    var binderFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();

    assertThat(binderFn.binderInvocation().name()).isEqualTo("zeroKnowledgeInputOnChain");

    assertThat(binderFn.binderArguments().size()).isEqualTo(3);
    assertThat(binderFn.contractArguments().size()).isEqualTo(0);
    assertThat(binderFn.arguments().size()).isEqualTo(3); // commontests-ignore-arg

    assertThat(binderFn.binderInvocation().arguments().get(0).type())
        .isEqualTo(new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)));
    List<ScValue> values = binderFn.binderArguments().get(0).vecValue().values();
    assertThat(values).hasSize(1);
    assertThat(values.get(0).asInt()).isEqualTo(32);

    assertThat(binderFn.binderInvocation().arguments().get(1).type())
        .isEqualTo(new SimpleTypeSpec(TypeSpec.TypeIndex.PublicKey));
    assertThat(binderFn.binderArguments().get(1).publicKeyValue().asString())
        .isEqualTo("02a71214ac34adc41cc202035d20d2427ec0bfa884d424c29abe228becfe2c3915");

    assertThat(binderFn.binderArguments().get(2).vecValue().values().size()).isEqualTo(4);
    for (var value : binderFn.binderArguments().get(2).vecValue().values()) {
      assertThat(requireNonNull(value.structValue().getFieldValue("bytes")).vecU8Value())
          .hasSize(64);
    }

    assertThat(requireNonNull(binderFn.contractInvocation()).name()).isEqualTo("add_salary");
  }

  /**
   * Can successfully read a binder invocation even if the binder invocation does not have a
   * shortname.
   */
  @Test
  void passThrough() {
    var abi = TestingHelper.loadAbiParserFromFile("voting.abi").parseAbi();
    byte[] rpc = new byte[] {1, 1};

    var binderFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();

    assertThat(binderFn.binderInvocation().name()).isEqualTo("invoke");
    assertThat(requireNonNull(binderFn.contractInvocation()).name()).isEqualTo("vote");
    assertThat(binderFn.size()).isEqualTo(1); // commontests-ignore-array
    assertThat(binderFn.get(0).boolValue()).isEqualTo(true); // commontests-ignore-array
  }

  /** Can read invocations that only communicate with the binder. */
  @Test
  void onlyBinderInvocation() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary.abi").parseAbi();
    byte[] rpc =
        TestingHelper.readPayloadFile(
            "extendZkComputation-"
                + "4c5c658984b097efe2e250bba74d812eab74e10028f72d64ea9306b6f95789dd.txt");

    var binderFn = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();
    assertThat(binderFn.binderInvocation().name()).isEqualTo("extendZkComputationDeadline");
    assertThat(binderFn.binderArguments().get(0).asLong()).isEqualTo(3024L);
    assertThat(binderFn.binderArguments().get(1).asLong()).isEqualTo(1L);
    assertThat(binderFn.binderArguments().get(2).asLong()).isEqualTo(86400000L);
    assertThat(binderFn.binderArguments().get(3).asLong()).isEqualTo(15724800000L);
    assertThat(binderFn.contractInvocation()).isEqualTo(null);
  }

  /** Can also read the initialization bytes as a binder invocation. */
  @Test
  void zkDeploy() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary.abi").parseAbi();
    byte[] rpc = TestingHelper.readPayloadFile("deploy-average-salary.txt");
    var binderFn = RpcReader.create(rpc, abi, TransactionKind.Init).readRpc();
    assertThat(binderFn.binderInvocation().name()).isEqualTo("create");
    assertThat(binderFn.binderArguments().size()).isEqualTo(8);
    assertThat(requireNonNull(binderFn.contractInvocation()).name()).isEqualTo("initialize");
  }

  /** An empty rpc will be read as the 'topUpGas' invocation. */
  @Test
  void emptyRpc() {
    var abi = TestingHelper.loadAbiParserFromFile("voting.abi").parseAbi();
    var rpc = new byte[0];
    var rpcValue = RpcReader.create(rpc, abi, TransactionKind.Action).readRpc();
    assertThat(rpcValue.binderInvocation().name()).isEqualTo("topUpGas");
  }

  /**
   * The RpcReader will throw if there does not exist an invocation with the shortname of the rpc
   * payload.
   */
  @Test
  void unknownInvocation() {
    var abi = TestingHelper.loadAbiParserFromFile("average_salary.abi").parseAbi();
    var rpc = new byte[] {127};
    var reader = RpcReader.create(rpc, abi, TransactionKind.Action);
    assertThatThrownBy(reader::readRpc)
        .isInstanceOf(RpcReadException.class)
        .hasMessage("No binder invocation with kind 'Action' and shortname '127'");
  }

  /** Can read the upgrade binder invocation, with no shortnames. */
  @Test
  void readUpgrade() {
    var abi = TestingHelper.loadAbiParserFromFile("PublicDeployContract.abi").parseAbi();
    var rpc = new byte[21];
    var invocation = RpcReader.create(rpc, abi, TransactionKind.Upgrade).readRpc();
    assertThat(invocation.binderInvocation().name()).isEqualTo("upgrade");
    assertThat(requireNonNull(invocation.contractInvocation()).name()).isEqualTo("upgrade");
    List<ScValue> allArguments = invocation.arguments(); // commontests-ignore-arg
    assertThat(allArguments).hasSize(1);
    assertThat(allArguments.get(0).addressValue().bytes()).isEqualTo(rpc);
  }

  /**
   * If there isn't defined a special EmptyAction binder invocation, then the rpc reader will
   * instead try to read an empty payload as a normal action.
   */
  @Test
  void noDefinedEmptyFunction() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(
                new BinderInvocation(TransactionKind.Action, "notEmpty", null, List.of(), null)),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(11, 1, 0), new AbiVersion(5, 1, 0), null, contractAbi);
    RpcReader reader = RpcReader.create(new byte[0], abi, TransactionKind.Action);
    assertThat(reader.readRpc().binderInvocation().name())
        .isEqualTo("notEmpty"); // commontests-ignore-name
  }

  /** An empty callback invocation will not be read as the special EmptyAction binder invocation. */
  @Test
  void onlyActionCanHitSpecialEmptyAction() {
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(),
            List.of(
                new BinderInvocation(TransactionKind.EmptyAction, "Empty", null, List.of(), null),
                new BinderInvocation(TransactionKind.Callback, "callback", null, List.of(), null)),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(12, 0, 0), new AbiVersion(6, 0, 0), null, contractAbi);
    RpcReader reader = RpcReader.create(new byte[0], abi, TransactionKind.Callback);
    assertThat(reader.readRpc().binderInvocation().name()).isEqualTo("callback");
  }
}
