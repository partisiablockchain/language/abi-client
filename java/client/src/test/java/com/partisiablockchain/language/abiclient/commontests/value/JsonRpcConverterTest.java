package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.commontests.ValueHelper;
import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abiclient.rpc.RpcValue;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class JsonRpcConverterTest {

  @Test
  void rpcTest() {
    var contract =
        new ContractAbi(
            BinderType.Public,
            ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS,
            List.of(),
            ImplicitBinderAbi.defaultPubInvocations(),
            List.of(
                new ContractInvocation(
                    ImplicitBinderAbi.DefaultKinds.ACTION,
                    "jsonTest",
                    HexFormat.of().parseHex("dddddd0d"),
                    List.of(
                        new ArgumentAbi("value1", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                        new ArgumentAbi("value2", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                        new ArgumentAbi(
                            "value3",
                            new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i8)))))),
            new NamedTypeRef(0));
    var abiVersion = new AbiVersion(4, 0, 0);
    var fileAbi = new FileAbi("PBCABI", abiVersion, abiVersion, null, contract);

    var rpcVal =
        RpcReader.create(
                HexFormat.of().parseHex("dddddd0d00000000140000000401020304"),
                fileAbi,
                TransactionKind.Action)
            .readRpc();
    ValueHelper.assertRpcJson(
        rpcVal,
        "{\n"
            + "  \"binderInvocationName\" : \"invoke\",\n"
            + "  \"binderArguments\" : {},\n"
            + "  \"actionName\" : \"jsonTest\",\n"
            + "  \"contractArguments\" : {\n"
            + "    \"value1\" : false,\n"
            + "    \"value2\" : 20,\n"
            + "    \"value3\" : [ 1, 2, 3, 4 ]\n"
            + "  }\n"
            + "}");
  }

  /** The rpc json includes arguments of the binder invocation. */
  @Test
  void rpcBinderArgs() {
    var invo =
        new BinderInvocation(
            TransactionKind.Action,
            "myBinderName",
            (byte) 5,
            List.of(new ArgumentAbi("binderArg", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
            2);

    var func =
        new ContractInvocation(
            ImplicitBinderAbi.DefaultKinds.ACTION,
            "myActionName",
            new byte[1],
            List.of(new ArgumentAbi("contractArg", new SimpleTypeSpec(TypeSpec.TypeIndex.bool))));
    var rpcVal =
        new RpcValue(invo, func, List.of(new ScValueI32(4)), List.of(new ScValueBool(false)));
    ValueHelper.assertRpcJson(
        rpcVal,
        "{\n"
            + "  \"binderInvocationName\" : \"myBinderName\",\n"
            + "  \"binderArguments\" : {\n"
            + "    \"binderArg\" : 4\n"
            + "  },\n"
            + "  \"actionName\" : \"myActionName\",\n"
            + "  \"contractArguments\" : {\n"
            + "    \"contractArg\" : false\n"
            + "  }\n"
            + "}");
  }

  /**
   * If the RPC only hits the binder and not the contract, then the json of it only contains the
   * binder arguments.
   */
  @Test
  void rpcBinderOnly() {
    var invo =
        new BinderInvocation(
            TransactionKind.Action,
            "myBinderName",
            (byte) 5,
            List.of(new ArgumentAbi("binderArg", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))),
            2);
    var rpcVal = new RpcValue(invo, null, List.of(new ScValueI32(4)), List.of());
    ValueHelper.assertRpcJson(
        rpcVal,
        "{\n"
            + "  \"binderInvocationName\" : \"myBinderName\",\n"
            + "  \"binderArguments\" : {\n"
            + "    \"binderArg\" : 4\n"
            + "  }\n"
            + "}");
  }
}
