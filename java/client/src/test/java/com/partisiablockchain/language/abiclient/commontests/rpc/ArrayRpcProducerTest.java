package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import org.junit.jupiter.api.Test;

/** Test of {@link RpcContractBuilder}. */
public final class ArrayRpcProducerTest {

  /** It is possible to {@link RpcContractBuilder#addSizedByteArray}. */
  @Test
  public void contractArraySuccessful() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    final var builder =
        TestingHelper.createBuilderFromFile("contract_byte_arrays_v3.abi", "update_my_array_2");
    builder.addSizedByteArray(new byte[] {1, 2, 3, 4, 5});

    byte[] rpc = builder.getBytes();
    assertThat(rpc).isEqualTo(concatBytes(bytesFromHex("ec9da775"), new byte[] {1, 2, 3, 4, 5}));
  }

  /** It is possible to {@link RpcContractBuilder#addSizedByteArray}. */
  @Test
  public void contractArraySuccessfulManually() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    final var builder =
        TestingHelper.createBuilderFromFile("contract_byte_arrays_v3.abi", "update_my_array_2");
    builder
        .addSizedArray()
        .addU8((byte) 1)
        .addU8((byte) 2)
        .addU8((byte) 3)
        .addU8((byte) 4)
        .addU8((byte) 5);

    byte[] rpc = builder.getBytes();
    assertThat(rpc).isEqualTo(concatBytes(bytesFromHex("ec9da775"), new byte[] {1, 2, 3, 4, 5}));
  }

  /**
   * {@link RpcContractBuilder#addSizedByteArray} must fail when given the wrong number of bytes.
   */
  @Test
  public void failWhenAddingTooMany() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
    final var builder =
        TestingHelper.createBuilderFromFile("contract_byte_arrays_v3.abi", "update_my_array");
    assertThatThrownBy(() -> builder.addSizedByteArray(new byte[100]))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage(
            "Array builder at update_my_array/value/16: Array is already at max size 16, cannot add"
                + " more");
  }

  /** {@link RpcContractBuilder#addSizedByteArray} must fail when expecting a non-array type. */
  @Test
  public void errorIfNoArrayType() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
    final var builder =
        TestingHelper.createBuilderFromFile("contract_booleans_v3.abi", "update_my_bool");
    assertThatThrownBy(() -> builder.addSizedByteArray(new byte[] {1}))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_bool/value, Expected type bool, but got SizedArray");
  }
}
