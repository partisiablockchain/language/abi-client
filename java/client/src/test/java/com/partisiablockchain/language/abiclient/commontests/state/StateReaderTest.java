package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.StateReaderHelper;
import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abiclient.state.StateBytes;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueArray;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test for {@link StateReader}. */
public final class StateReaderTest {

  private final SimpleTypeSpec u8 = new SimpleTypeSpec(TypeSpec.TypeIndex.u8);
  private final VecTypeSpec vecU8 = new VecTypeSpec(u8);
  private final SetTypeSpec setU8 = new SetTypeSpec(u8);
  private final MapTypeSpec mapU8U8 = new MapTypeSpec(u8, u8);
  private final OptionTypeSpec optionU8 = new OptionTypeSpec(u8);
  private final SizedArrayTypeSpec byteArrayOfFour =
      new SizedArrayTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8), 4);

  private final StructTypeSpec structType =
      new StructTypeSpec(
          "struct0", List.of(new FieldAbi("field0", u8), new FieldAbi("field1", u8)));
  private final NamedTypeRef structRef = new NamedTypeRef(0);

  @Test
  void readVec() {
    ScValueVector vec = StateReaderHelper.wrap("0100000010").readStateValue(vecU8).vecValue();
    assertThat(vec.size()).isEqualTo(1); // commontests-ignore-array
    assertThat(vec.get(0).getType()).isEqualTo(TypeSpec.TypeIndex.u8); // commontests-ignore-array
    assertThatThrownBy(vec::boolValue)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot read bool for type: Vec");
  }

  @Test
  void readString() {
    ScValue value =
        StateReaderHelper.wrap("0100000042")
            .readStateValue(new SimpleTypeSpec(TypeSpec.TypeIndex.String));
    assertThat(value.stringValue()).isEqualTo("B");
  }

  @Test
  void readSet() {
    ScValueSet set = StateReaderHelper.wrap("0100000010").readStateValue(setU8).setValue();
    assertThat(set.size()).isEqualTo(1); // commontests-ignore-array
    assertThat(set.get(0).getType()).isEqualTo(TypeSpec.TypeIndex.u8); // commontests-ignore-array
  }

  @Test
  void readMap() {
    ScValueMap map = StateReaderHelper.wrap("010000001020").readStateValue(mapU8U8).mapValue();
    assertThat(map.size()).isEqualTo(1); // commontests-ignore-array
    assertThat(map.isEmpty()).isFalse();

    var key = new ScValueU8((byte) 0x10);
    assertThat(map.get(key)).isEqualTo(new ScValueU8((byte) 0x20)); // commontests-ignore-array
  }

  @Test
  void readOption() {
    ScValueOption none = StateReaderHelper.wrap("00").readStateValue(optionU8).optionValue();
    assertThat(none.isSome()).isFalse();

    ScValueOption some = StateReaderHelper.wrap("0101").readStateValue(optionU8).optionValue();
    assertThat(some.isSome()).isTrue();
    assertThat(some.innerValue()).isEqualTo(new ScValueU8((byte) 1));
  }

  @Test
  void readSizedArray() {
    ScValue value = StateReaderHelper.wrap("01020304").readStateValue(byteArrayOfFour);
    assertThat(value.vecU8Value()).hasSize(4);
    ScValueArray expected = ScValueArray.fromBytes(new byte[] {0x01, 0x02, 0x03, 0x04});
    assertThat(value).isEqualTo(expected);
  }

  @Test
  void readStruct() {
    NamedTypeRef typeRef = new NamedTypeRef(0);
    ContractAbi contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(structType), List.of(), List.of(), typeRef);
    ScValueStruct state =
        StateReaderHelper.wrap("ff92", contract).readStateValue(structRef).structValue();
    assertThat(state.size()).isEqualTo(2); // commontests-ignore-array
    assertThat(state.getFieldValue("field0")).isEqualTo(new ScValueU8((byte) 0xff));
    assertThat(state.getFieldValue("field1")).isEqualTo(new ScValueU8((byte) 0x92));

    ScValueStruct valueTest =
        StateReaderHelper.wrap("ff92", contract).readStateValue(structRef).structValue();
    assertThat(valueTest.size()).isEqualTo(2); // commontests-ignore-array
    assertThat(valueTest.getFieldValue("field0")).isEqualTo(new ScValueU8((byte) 0xff));
    assertThat(valueTest.getFieldValue("field1")).isEqualTo(new ScValueU8((byte) 0x92));
  }

  @Test
  void readSimpleNumber() {
    var value = StateReaderHelper.wrap("ff").readSimpleType(TypeSpec.TypeIndex.u8);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.u8);
    assertThat(value.asInt()).isEqualTo(255);
  }

  @Test
  void readBigIntegerNumber() {
    var value1 =
        StateReaderHelper.wrap("ffffffffffffffffffffffffffffffff")
            .readSimpleType(TypeSpec.TypeIndex.u128);
    assertThat(value1.getType()).isEqualTo(TypeSpec.TypeIndex.u128);
    assertThat(
            value1
                .asBigInteger()
                .compareTo(new BigInteger("340282366920938463463374607431768211455")))
        .isEqualTo(0);

    var value2 =
        StateReaderHelper.wrap("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
            .readSimpleType(TypeSpec.TypeIndex.u256);
    assertThat(value2.getType()).isEqualTo(TypeSpec.TypeIndex.u256);
    assertThat(
            value2
                .asBigInteger()
                .compareTo(
                    new BigInteger(
                        "11579208923731619542357098500868"
                            + "7907853269984665640564039457584007913129639935")))
        .isEqualTo(0);
  }

  @Test
  void readSimpleBool() {
    var value1 = StateReaderHelper.wrap("01").readSimpleType(TypeSpec.TypeIndex.bool).boolValue();
    assertThat(value1).isEqualTo(true);
    var value2 = StateReaderHelper.wrap("ff").readSimpleType(TypeSpec.TypeIndex.bool).boolValue();
    assertThat(value2).isEqualTo(true);
    var value3 = StateReaderHelper.wrap("00").readSimpleType(TypeSpec.TypeIndex.bool).boolValue();
    assertThat(value3).isEqualTo(false);
  }

  @Test
  void readSimpleString() {
    var expected = "hello";

    var value =
        StateReaderHelper.wrap("0500000068656c6c6f").readSimpleType(TypeSpec.TypeIndex.String);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.String);
    assertThat(value.stringValue()).isEqualTo(expected);
  }

  @Test
  void readSimpleAddress() {
    var addressBytes = "020101010101010101010101010101010101010101";
    var value = StateReaderHelper.wrap(addressBytes).readSimpleType(TypeSpec.TypeIndex.Address);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.Address);
    assertThat(value.addressValue().bytes()).isEqualTo(bytesFromHex(addressBytes));
  }

  @Test
  void readHash() {
    var hashBytesAsHexString = ValidTestHexValues.HASH;
    var value =
        StateReaderHelper.wrap(hashBytesAsHexString).readSimpleType(TypeSpec.TypeIndex.Hash);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.Hash);
    assertThat(value.hashValue().bytes()).isEqualTo(bytesFromHex(hashBytesAsHexString));
  }

  @Test
  void readPublicKey() {
    var publicKeyBytesAsHexString = ValidTestHexValues.PUBLIC_KEY;
    var value =
        StateReaderHelper.wrap(publicKeyBytesAsHexString)
            .readSimpleType(TypeSpec.TypeIndex.PublicKey);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.PublicKey);
    assertThat(value.publicKeyValue().bytes()).isEqualTo(bytesFromHex(publicKeyBytesAsHexString));
  }

  @Test
  void readSignature() {
    var signatureBytesAsHexString = ValidTestHexValues.SIGNATURE;
    var value =
        StateReaderHelper.wrap(signatureBytesAsHexString)
            .readSimpleType(TypeSpec.TypeIndex.Signature);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.Signature);
    assertThat(value.signatureValue().bytes()).isEqualTo(bytesFromHex(signatureBytesAsHexString));
  }

  @Test
  void readBlsPublicKey() {
    var blsPublicKeyBytesAsHexString = ValidTestHexValues.BLS_PUBLIC_KEY;
    var value =
        StateReaderHelper.wrap(blsPublicKeyBytesAsHexString)
            .readSimpleType(TypeSpec.TypeIndex.BlsPublicKey);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.BlsPublicKey);
    assertThat(value.blsPublicKeyValue().bytes())
        .isEqualTo(bytesFromHex(blsPublicKeyBytesAsHexString));
  }

  @Test
  void readBlsSignature() {
    var blsSignatureBytesAsHexString = ValidTestHexValues.BLS_SIGNATURE;
    var value =
        StateReaderHelper.wrap(blsSignatureBytesAsHexString)
            .readSimpleType(TypeSpec.TypeIndex.BlsSignature);
    assertThat(value.getType()).isEqualTo(TypeSpec.TypeIndex.BlsSignature);
    assertThat(value.blsSignatureValue().bytes())
        .isEqualTo(bytesFromHex(blsSignatureBytesAsHexString));
  }

  @Test
  void readAvlTreeMap() {
    var type =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.i32), new SimpleTypeSpec(TypeSpec.TypeIndex.u8));

    var stateBuilder = new StateBuilder(null);
    var mapBuilder = stateBuilder.addMap();
    mapBuilder.addOption().addI32(0);
    var innerMapBuilder = mapBuilder.addOption().addOption().addMap();
    innerMapBuilder.addOption().addOption().addVecU8(HexFormat.of().parseHex("01000000"));
    innerMapBuilder.addOption().addVecU8(HexFormat.of().parseHex("01"));
    innerMapBuilder.addOption().addOption().addVecU8(HexFormat.of().parseHex("02000000"));
    innerMapBuilder.addOption().addVecU8(HexFormat.of().parseHex("42"));

    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));
    var stateReader =
        StateReader.create(HexFormat.of().parseHex("00000000"), contract, stateBuilder.getBytes());
    var value = stateReader.readStateValue(type).avlTreeMapValue();
    var key1 = new ScValueI32(1);
    var key2 = new ScValueI32(2);
    assertThat(value.treeId()).isEqualTo(0);
    var map = requireNonNull(value.map());
    assertThat(requireNonNull(map.get(key1)).asInt()).isEqualTo(1); // commontests-ignore-array
    assertThat(requireNonNull(map.get(key2)).asInt()).isEqualTo(0x42); // commontests-ignore-array
  }

  @Test
  void readAvlTreeMapNoMap() {
    var type =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.i32), new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));
    var stateReader = StateReader.create(HexFormat.of().parseHex("00000000"), contract);
    var value = stateReader.readStateValue(type).avlTreeMapValue();
    assertThat(value.treeId()).isEqualTo(0);
    assertThat(value.map()).isNull(); // commontests-expect-null
  }

  @Test
  void readAvlTreeMapError() {
    var type =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.i32), new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));

    var stateReader =
        StateReader.create(
            HexFormat.of().parseHex("00000000"), contract, HexFormat.of().parseHex("00000000"));
    assertThatThrownBy(() -> stateReader.readStateValue(type))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Tried to read Avl Tree Map with tree id 0, but it didn't exist");
  }

  @Test
  void readNestedAvlTree() {
    var type =
        new AvlTreeMapTypeSpec(
            new SimpleTypeSpec(TypeSpec.TypeIndex.i32),
            new AvlTreeMapTypeSpec(
                new SimpleTypeSpec(TypeSpec.TypeIndex.u8),
                new SimpleTypeSpec(TypeSpec.TypeIndex.bool)));

    var stateBuilder = new StateBuilder(null);
    var mapBuilder = stateBuilder.addMap();
    mapBuilder.addOption().addI32(0);
    var innerMapBuilder = mapBuilder.addOption().addOption().addMap();
    innerMapBuilder.addOption().addOption().addVecU8(HexFormat.of().parseHex("42000000"));
    innerMapBuilder.addOption().addVecU8(HexFormat.of().parseHex("01000000"));
    mapBuilder.addOption().addI32(1);
    var innerMapBuilder2 = mapBuilder.addOption().addOption().addMap();
    innerMapBuilder2.addOption().addOption().addVecU8(HexFormat.of().parseHex("00"));
    innerMapBuilder2.addOption().addVecU8(HexFormat.of().parseHex("01"));
    innerMapBuilder2.addOption().addOption().addVecU8(HexFormat.of().parseHex("01"));
    innerMapBuilder2.addOption().addVecU8(HexFormat.of().parseHex("00"));

    var contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));
    var stateReader =
        StateReader.create(HexFormat.of().parseHex("00000000"), contract, stateBuilder.getBytes());
    var value = stateReader.readStateValue(type).avlTreeMapValue();
    assertThat(value.treeId()).isEqualTo(0);

    var map =
        requireNonNull(requireNonNull(value.map()).get(new ScValueI32(0x42))).avlTreeMapValue();
    assertThat(map.treeId()).isEqualTo(1);
    var innerMap = requireNonNull(map.map());
    assertThat(requireNonNull(innerMap.get(new ScValueU8((byte) 0))).boolValue()).isTrue();
    assertThat(requireNonNull(innerMap.get(new ScValueU8((byte) 1))).boolValue()).isFalse();
  }

  /** The state reader is capable of reading avl trees where the keys are structs. */
  @Test
  void readAvlTreeStructKey() {
    var type =
        new AvlTreeMapTypeSpec(new NamedTypeRef(0), new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    var keyType =
        new StructTypeSpec(
            "KeyName",
            List.of(
                new FieldAbi("field1", new SimpleTypeSpec(TypeSpec.TypeIndex.u8)),
                new FieldAbi("field1", new SimpleTypeSpec(TypeSpec.TypeIndex.u8))));

    var stateBuilder = new StateBuilder(null);
    var mapBuilder = stateBuilder.addMap();
    mapBuilder.addOption().addI32(0);
    var innerMapBuilder = mapBuilder.addOption().addOption().addMap();
    innerMapBuilder.addOption().addOption().addVecU8(HexFormat.of().parseHex("0102"));
    innerMapBuilder.addOption().addVecU8(HexFormat.of().parseHex("01"));
    innerMapBuilder.addOption().addOption().addVecU8(HexFormat.of().parseHex("0304"));
    innerMapBuilder.addOption().addVecU8(HexFormat.of().parseHex("00"));

    var contract =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(keyType),
            List.of(),
            List.of(),
            new SimpleTypeSpec(TypeSpec.TypeIndex.u8));

    var stateReader =
        StateReader.create(HexFormat.of().parseHex("00000000"), contract, stateBuilder.getBytes());
    var value = stateReader.readGeneric(type).avlTreeMapValue();
    assertThat(value.treeId()).isEqualTo(0);

    var map = requireNonNull(value.map());
    assertThat(map.size()).isEqualTo(2); // commontests-map-size
  }

  @Test
  void stateBytes() {
    var stateBytes = new StateBytes(new byte[2]);
    assertThat(stateBytes.state()).isEqualTo(new byte[2]);
    assertThat(stateBytes.avlTrees()).isNull();
  }
}
