package com.partisiablockchain.language.abiclient.commontests.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.concatBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.builder.StructProducer;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abiclient.state.StateBuilder;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class StructRpcProducerTest {

  @Test
  void tooFew() {
    StructTypeSpec structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));

    NamedTypeRef structType = new NamedTypeRef(0);
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(structAbi), List.of(), List.of(), structType);
    var producer = new StateBuilder(contractAbi);

    assertThatThrownBy(producer::getBytes)
        .isInstanceOf(MissingElementException.class)
        .hasMessage("In root, Missing argument 'f1'");
  }

  @Test
  void tooManyElements() {
    StructTypeSpec structAbi =
        new StructTypeSpec(
            "name", List.of(new FieldAbi("f1", new SimpleTypeSpec(TypeSpec.TypeIndex.u64))));
    NamedTypeRef structType = new NamedTypeRef(0);
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(structAbi), List.of(), List.of(), structType);
    var producer = new StateBuilder(contractAbi);
    producer.addU64(1);

    assertThatThrownBy(() -> producer.addU64(1))
        .isInstanceOf(IllegalArgumentFormat.class)
        .hasMessage("In root, Cannot add more arguments than the struct has fields.");
  }

  @Test
  void structProducerWithNullTypeSpecHasEmptyFieldName() {
    StructProducer producer = new StructProducer(null, "");
    assertThat(producer.getFieldName()).isEqualTo("");
  }

  @Test
  void testSimpleStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    var builder =
        TestingHelper.createBuilderFromFile("contract_simple_struct_v3.abi", "update_my_struct");
    builder.addStruct().addU64(2).addVec().addU64(1).addU64(1);

    // Encoding the element: 2
    byte[] expectedForU64 = HexFormat.of().parseHex("0000000000000002");
    // 2 arguments (4 bytes) of u64 (8 bytes) each has valueType 01
    byte[] expectedForVec =
        HexFormat.of().parseHex("00000002" + "0000000000000001" + "0000000000000001");

    byte[] rpc = builder.getBytes();
    byte[] expected =
        concatBytes(HexFormat.of().parseHex("f3b68ff40e"), expectedForU64, expectedForVec);
    assertThat(rpc).isEqualTo(expected);
  }

  @Test
  public void assertTypeError() {
    // Contract can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    var builder =
        TestingHelper.createBuilderFromFile("contract_simple_struct_v3.abi", "update_my_struct");
    assertThatThrownBy(() -> builder.addBool(false))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_struct/value, Expected type Named, but got bool");

    var noStructType =
        TestingHelper.createBuilderFromFile("contract_booleans_v3.abi", "update_my_bool");
    assertThatThrownBy(noStructType::addStruct)
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_bool/value, Expected type bool, but got Named");
  }

  @Test
  public void assertTypeErrorInStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
    var builder =
        TestingHelper.createBuilderFromFile("contract_simple_struct_v3.abi", "update_my_struct");
    assertThatThrownBy(() -> builder.addStruct().addString("string"))
        .isInstanceOf(TypeCheckingException.class)
        .hasMessage("In update_my_struct/value/some_value, Expected type u64, but got String");
  }

  @Test
  public void structOfStruct() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_struct_of_struct_v3.abi", "update_my_other_struct");
    builder.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
    byte[] rpc = builder.getBytes();
    byte[] expected =
        concatBytes(
            bytesFromHex("85f292be0b"),
            HexFormat.of().parseHex("0000000000000001"),
            // One element
            HexFormat.of()
                .parseHex(
                    "00000001"
                        + "0000000000000002"
                        // One element
                        + "00000001"
                        + "0000000000000003"));
    assertThat(rpc).isEqualTo(expected);

    // Testing without providing type checking
    var builderNoTypeCheck = new RpcContractBuilder(HexFormat.of().parseHex("85f292be0b"));
    builderNoTypeCheck.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);

    byte[] noTypeCheckRpc = builderNoTypeCheck.getBytes();
    assertThat(noTypeCheckRpc).isEqualTo(expected);
  }

  @Test
  public void moreStructsInVec() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
    var builder =
        TestingHelper.createBuilderFromFile(
            "contract_struct_of_struct_v3.abi", "update_my_other_struct");
    var vecRpcProducer = builder.addStruct().addU64(0xFFFF_FFFF_FFFF_FFFFL).addVec();
    vecRpcProducer.addStruct().addU64(1).addVec().addU64(2);
    vecRpcProducer.addStruct().addU64(3).addVec().addU64(4);

    byte[] rpc = builder.getBytes();
    byte[] expected =
        concatBytes(
            HexFormat.of().parseHex("85f292be0b"),
            HexFormat.of().parseHex("ffffffffffffffff"),
            // Two elements in Vec
            HexFormat.of().parseHex("00000002"),
            // First is struct with 1, Vec<2>
            HexFormat.of().parseHex("0000000000000001" + "00000001" + "0000000000000002"),
            // Second is struct with 3, <Vec<4>
            HexFormat.of().parseHex("0000000000000003" + "00000001" + "0000000000000004"));
    assertThat(rpc).isEqualTo(expected);
  }
}
