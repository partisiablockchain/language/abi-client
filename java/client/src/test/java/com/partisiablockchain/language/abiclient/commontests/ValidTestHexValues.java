package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Example valid values of different types. */
public final class ValidTestHexValues {

  /** Hash must be 32 bytes. */
  public static String HASH = "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb";

  /** Public key must be 33 bytes. */
  public static String PUBLIC_KEY =
      "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaa";

  /** Signature must be 65 bytes. */
  public static String SIGNATURE =
      "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaa";

  /** BLS Public Key must be 96 bytes. */
  public static String BLS_PUBLIC_KEY =
      "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabb";

  /** BLS Signature must be 48 bytes. */
  public static String BLS_SIGNATURE =
      "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"
          + "aabbaabbaabbaabb";
}
