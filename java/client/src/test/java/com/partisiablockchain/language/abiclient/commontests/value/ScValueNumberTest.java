package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueI8;
import com.partisiablockchain.language.abiclient.value.ScValueU128;
import com.partisiablockchain.language.abiclient.value.ScValueU16;
import com.partisiablockchain.language.abiclient.value.ScValueU256;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Tests for all StateValue representation of number values. */
final class ScValueNumberTest {
  @Test
  void testAsInt() {
    var i8Max = new ScValueI8((byte) 127);
    var u16Max = new ScValueU16((short) 65535);
    var i64 = new ScValueI64(64L);
    assertThat(i8Max.asInt()).isEqualTo(127);
    assertThat(u16Max.asInt()).isEqualTo(65535);

    assertThatThrownBy(i64::asInt)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read integer for type: i64");
  }

  @Test
  void testAsLong() {
    var i32Max = new ScValueI32(2147483647);
    var i64Min = new ScValueI64(-9223372036854775808L);
    assertThat(i32Max.asLong()).isEqualTo(2147483647L);
    assertThat(i64Min.asLong()).isEqualTo(-9223372036854775808L);
  }

  @Test
  void testAsBigInteger() {
    var u128Max = new ScValueU128(new BigInteger("340282366920938463463374607431768211455"));
    assertThat(u128Max.asBigInteger())
        .isEqualTo(new BigInteger("340282366920938463463374607431768211455"));

    var u256Max =
        new ScValueU256(
            new BigInteger(
                "115792089237316195423570985008687907853269984665640564039457584007913129639935"));
    assertThat(u256Max.asBigInteger())
        .isEqualTo(
            new BigInteger(
                "115792089237316195423570985008687907853269984665640564039457584007913129639935"));

    var u8Max = new ScValueU8((byte) 255);
    assertThat(u8Max.asBigInteger()).isEqualTo(new BigInteger("255"));
  }
}
