package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

final class ScValueSignatureTest {

  @Test
  void invalidByteLength() {
    assertThatThrownBy(() -> new ScValueSignature(new byte[0]))
        .isInstanceOf(ScReadException.class)
        .hasMessage("Signature expects exactly 65 bytes, but found 0");

    assertThatThrownBy(() -> new ScValueSignature(new byte[66]))
        .isInstanceOf(ScReadException.class)
        .hasMessage("Signature expects exactly 65 bytes, but found 66");
  }

  @Test
  void getType() {
    var signature = new ScValueSignature(new byte[65]);
    assertThat(signature.getType()).isEqualTo(TypeSpec.TypeIndex.Signature);
  }

  @Test
  void value() {
    var rawBytes = HexFormat.of().parseHex(ValidTestHexValues.SIGNATURE);

    var signatureFromRaw = new ScValueSignature(rawBytes);
    assertThat(signatureFromRaw.bytes()).isEqualTo(rawBytes);
    assertThat(signatureFromRaw.signatureValue()).isEqualTo(signatureFromRaw);
  }
}
