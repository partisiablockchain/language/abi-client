package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScReadException;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueString;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test for {@link ScValue} accessors. */
public final class ScValueTest {

  /** {@link ScValue}s must throw an error if accessing a variant that this value is not. */
  @Test
  public void assertErrorsForTypes() {
    var input = HexFormat.of().parseHex("010000000000000000000000000000000000000010" + "00");
    ContractAbi contract =
        new ContractAbi(
            BinderType.Public, List.of(), List.of(), List.of(), List.of(), new NamedTypeRef(0));

    StateReader reader = StateReader.create(input, contract);

    ScValue stringValue = reader.readStateValue(new SimpleTypeSpec(TypeSpec.TypeIndex.Address));

    assertThatThrownBy(stringValue::boolValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read bool for type: Address");
    assertThatThrownBy(stringValue::setValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Set for type: Address");
    assertThatThrownBy(stringValue::vecValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Vector for type: Address");
    assertThatThrownBy(stringValue::optionValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Option for type: Address");
    assertThatThrownBy(stringValue::structValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Struct for type: Address");
    assertThatThrownBy(stringValue::mapValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Map for type: Address");
    assertThatThrownBy(stringValue::enumValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Enum for type: Address");
    assertThatThrownBy(stringValue::hashValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Hash for type: Address");
    assertThatThrownBy(stringValue::publicKeyValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read PublicKey for type: Address");
    assertThatThrownBy(stringValue::signatureValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Signature for type: Address");
    assertThatThrownBy(stringValue::blsPublicKeyValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read BlsPublicKey for type: Address");
    assertThatThrownBy(stringValue::blsSignatureValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read BlsSignature for type: Address");
    assertThatThrownBy(stringValue::vecU8Value)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Vec u8 for type: Address");
    assertThatThrownBy(stringValue::stringValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read String for type: Address");

    assertThatThrownBy(stringValue::asInt)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read integer for type: Address");
    assertThatThrownBy(stringValue::asBigInteger)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read BigInteger for type: Address");

    assertThatThrownBy(stringValue::avlTreeMapValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read AvlTreeMap for type: Address");

    assertThatThrownBy(stringValue::arrayValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read array for type: Address");

    ScValue boolValue = reader.readStateValue(new SimpleTypeSpec(TypeSpec.TypeIndex.bool));
    assertThatThrownBy(boolValue::addressValue)
        .isInstanceOf(ScReadException.class)
        .hasMessage("Cannot read Address for type: bool");
  }

  /** {@link ScValue}s must throw an error if accessing a variant that this value is not. */
  @Test
  public void assertErrorsForIntegerTypes() {
    ScValue stringValue = new ScValueString("hello");
    assertThatThrownBy(stringValue::u8Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::u16Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::u32Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::u64Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::u128Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::u256Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete

    assertThatThrownBy(stringValue::i8Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::i16Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::i32Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::i64Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete
    assertThatThrownBy(stringValue::i128Value) // commontests-delete
        .isInstanceOf(ScReadException.class); // commontests-delete

    assertThatThrownBy(stringValue::asInt).isInstanceOf(ScReadException.class);
    assertThatThrownBy(stringValue::asLong).isInstanceOf(ScReadException.class);
    assertThatThrownBy(stringValue::asBigInteger).isInstanceOf(ScReadException.class);
  }
}
