package com.partisiablockchain.language.abiclient.commontests.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abiclient.commontests.TestingHelper.bytesFromHex;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.abiclient.commontests.StateReaderHelper;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abiclient.state.RustSyntaxStatePrinter;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.math.BigInteger;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Tests for {@link RustSyntaxStatePrinter}. */
public final class RealStateValueTest {

  @Test
  void testTokenContract() {
    assertThat(StateReaderHelper.stringify("token_contract.abi", "token_contract.state"))
        .isEqualTo(
            "#[state]\n"
                + "TokenContractState {\n"
                + "    name: \"Token Name\".to_string(),\n"
                + "    decimals: 18u8,\n"
                + "    symbol: \"SYMB\".to_string(),\n"
                + "    owner: \"020000000000000000000000000000000000000001\",\n"
                + "    total_supply: 123123123u64,\n"
                + "    balances: BTreeMap::from([\n"
                + "        (\"020000000000000000000000000000000000000001\", 123123123u64)\n"
                + "    ]),\n"
                + "    allowed: BTreeMap::from([\n"
                + "    ]),\n"
                + "}");
  }

  @Test
  void testContractWithEnum() {
    // Contract source can be found at:
    // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
    assertThat(StateReaderHelper.stringify("contract_enum.abi", "contract_enum.state"))
        .isEqualTo(
            "#[state]\n"
                + "EnumContractState {\n"
                + "    my_enum: Car {\n"
                + "        engine_size: 1u8,\n"
                + "        supports_trailer: false\n"
                + "    },\n"
                + "}");
  }

  @Test
  void testTokenContractValues() {
    AbiParser parser = TestingHelper.loadAbiParserFromFile("token_contract.abi");
    var contract = parser.parseContractAbi();
    StateReader reader =
        StateReader.create(StateReaderHelper.readStateFile("token_contract.state"), contract);

    ScValueStruct state = reader.readState();

    assertThat(requireNonNull(state.getFieldValue("name")).stringValue()).isEqualTo("Token Name");
    assertThat(requireNonNull(state.getFieldValue("decimals")).asInt()).isEqualTo(18);
    assertThat(requireNonNull(state.getFieldValue("symbol")).stringValue()).isEqualTo("SYMB");

    byte[] expectedAddress = HexFormat.of().parseHex("020000000000000000000000000000000000000001");
    ScValueAddress address = requireNonNull(state.getFieldValue("owner")).addressValue();
    assertThat(address.bytes()).isEqualTo(expectedAddress);

    assertThat(
            requireNonNull(state.getFieldValue("total_supply"))
                .asBigInteger()
                .equals(new BigInteger("123123123")))
        .isTrue();

    ScValueMap balancesMap = requireNonNull(state.getFieldValue("balances")).mapValue();
    assertThat(balancesMap.keySet().get(0).addressValue().bytes()) // commontests-delete
        .isEqualTo(expectedAddress); // commontests-delete
    assertThat(
            requireNonNull(balancesMap.get(address)) // commontests-ignore-array
                .asBigInteger()
                .equals(new BigInteger("123123123")))
        .isTrue();

    assertThat(requireNonNull(state.getFieldValue("allowed")).mapValue().isEmpty()).isTrue();
  }

  @Test
  void testTokenContractAfterAction() {
    assertThat(StateReaderHelper.stringify("token_contract.abi", "token_contract_transfer.state"))
        .isEqualTo(
            "#[state]\n"
                + "TokenContractState {\n"
                + "    name: \"Token Name\".to_string(),\n"
                + "    decimals: 18u8,\n"
                + "    symbol: \"SYMB\".to_string(),\n"
                + "    owner: \"020000000000000000000000000000000000000001\",\n"
                + "    total_supply: 123123123u64,\n"
                + "    balances: BTreeMap::from([\n"
                + "        (\"020000000000000000000000000000000000000001\", 123123081u64),\n"
                + "        (\"020000000000000000000000000000000000000002\", 42u64)\n"
                + "    ]),\n"
                + "    allowed: BTreeMap::from([\n"
                + "    ]),\n"
                + "}");
  }

  @Test
  void initialAuctionState() {
    assertThat(
            StateReaderHelper.stringify(
                "rust_example_auction_contract.abi", "auction_initial.state"))
        .isEqualTo(
            "#[state]\n"
                + "TokenContractState {\n"
                + "    contract_owner: \"010000000000000000000000000000000000000001\",\n"
                + "    start_time: 12346i64,\n"
                + "    end_time: 12423i64,\n"
                + "    token_amount_for_sale: 100u64,\n"
                + "    commodity_token_type: \"02eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\",\n"
                + "    currency_token_type: \"02dddddddddddddddddddddddddddddddddddddddd\",\n"
                + "    highest_bidder: Bid {\n"
                + "        bidder: \"010000000000000000000000000000000000000001\",\n"
                + "        amount: 0u64\n"
                + "    },\n"
                + "    reserve_price: 20u64,\n"
                + "    min_increment: 5u64,\n"
                + "    claim_map: BTreeMap::from([\n"
                + "    ]),\n"
                + "    status: 0u8,\n"
                + "}");
  }

  @Test
  void optionVecState() {
    assertThat(StateReaderHelper.stringify("contract_option_vec_v3.abi", "option_vec.state"))
        .isEqualTo(
            "#[state]\n"
                + "ExampleContractState {\n"
                + "    option_vec: Some(vec![\n"
                + "        1u64,\n"
                + "        2u64\n"
                + "    ]),\n"
                + "    option_option: None,\n"
                + "}");
  }

  @Test
  void contractSetState() {
    // Two elements, BTreeSet::from([1u64, 2u64])
    byte[] bytes = HexFormat.of().parseHex("0200000001000000000000000200000000000000");
    assertThat(StateReaderHelper.stringifyBytes("contract_set.abi", bytes))
        .isEqualTo(
            "#[state]\n"
                + "ExampleContractState {\n"
                + "    my_set: BTreeSet::from([\n"
                + "        1u64,\n"
                + "        2u64\n"
                + "    ]),\n"
                + "}");
  }

  @Test
  void contractArrays() {
    // First array: 0-15, second: 16-20
    byte[] bytes = HexFormat.of().parseHex("000102030405060708090a0b0c0d0e0f" + "101112131415");
    assertThat(StateReaderHelper.stringifyBytes("contract_byte_arrays_v3.abi", bytes))
        .isEqualTo(
            ""
                + "#[state]\n"
                + "ExampleContractState {\n"
                + "    my_array: [\n"
                + "        0u8,\n"
                + "        1u8,\n"
                + "        2u8,\n"
                + "        3u8,\n"
                + "        4u8,\n"
                + "        5u8,\n"
                + "        6u8,\n"
                + "        7u8,\n"
                + "        8u8,\n"
                + "        9u8,\n"
                + "        10u8,\n"
                + "        11u8,\n"
                + "        12u8,\n"
                + "        13u8,\n"
                + "        14u8,\n"
                + "        15u8\n"
                + "    ],\n"
                + "    my_array_2: [\n"
                + "        16u8,\n"
                + "        17u8,\n"
                + "        18u8,\n"
                + "        19u8,\n"
                + "        20u8\n"
                + "    ],\n"
                + "}");
  }

  @Test
  void contractBooleans() {
    byte[] initBytes = HexFormat.of().parseHex("01");
    assertThat(StateReaderHelper.stringifyBytes("contract_booleans_v3.abi", initBytes))
        .isEqualTo("#[state]\n" + "ExampleContractState {\n" + "    my_bool: true,\n" + "}");

    byte[] changedStateByte = HexFormat.of().parseHex("00");
    assertThat(StateReaderHelper.stringifyBytes("contract_booleans_v3.abi", changedStateByte))
        .isEqualTo("#[state]\n" + "ExampleContractState {\n" + "    my_bool: false,\n" + "}");
  }

  @Test
  void test128BitContract() {
    byte[] bytes =
        bytesFromHex(
            // 1339673755198158348465859924523878152
            "08070605040302010807060504030201"
                // -170141183460469231731687303715884105728
                + "00000000000000000000000000000080");

    assertThat(StateReaderHelper.stringifyBytes("contract_u128_and_i128_v3.abi", bytes))
        .isEqualTo(
            "#[state]\n"
                + "ExampleContractState {\n"
                + "    my_u128: 1339673755198158348465859924523878152u128,\n"
                + "    my_i128: -170141183460469231731687303715884105728i128,\n"
                + "}");
  }

  @Test
  void testNumbers() {
    assertThat(
            StateReaderHelper.stringify("contract_numbers_v4.abi", "contract_numbers_simple.state"))
        .isEqualTo(
            "#[state]\n"
                + "ExampleContractState {\n"
                + "    my_u8: 1u8,\n"
                + "    my_u16: 2u16,\n"
                + "    my_u32: 3u32,\n"
                + "    my_u64: 4u64,\n"
                + "    my_i8: -1i8,\n"
                + "    my_i16: -2i16,\n"
                + "    my_i32: -3i32,\n"
                + "    my_i64: -4i64,\n"
                + "}");
  }
}
