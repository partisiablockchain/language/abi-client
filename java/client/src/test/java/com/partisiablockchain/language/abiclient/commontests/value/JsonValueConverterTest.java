package com.partisiablockchain.language.abiclient.commontests.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.commontests.ValidTestHexValues;
import com.partisiablockchain.language.abiclient.commontests.ValueHelper;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueArray;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueBlsPublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueHash;
import com.partisiablockchain.language.abiclient.value.ScValueI128;
import com.partisiablockchain.language.abiclient.value.ScValueI16;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueI8;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValuePublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abiclient.value.ScValueString;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueU128;
import com.partisiablockchain.language.abiclient.value.ScValueU16;
import com.partisiablockchain.language.abiclient.value.ScValueU256;
import com.partisiablockchain.language.abiclient.value.ScValueU32;
import com.partisiablockchain.language.abiclient.value.ScValueU64;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.LinkedHashMap;
import java.util.List;
import org.junit.jupiter.api.Test;

final class JsonValueConverterTest {

  @Test
  void toJsonBool() {
    var boolValue = new ScValueBool(false);
    ValueHelper.assertValueJson(boolValue, "false");
  }

  @Test
  void toJsonNumbers() {
    ValueHelper.assertValueJson(new ScValueU8((byte) 255), "255");
    ValueHelper.assertValueJson(new ScValueI8((byte) -1), "-1");
    ValueHelper.assertValueJson(new ScValueU16((short) 65535), "65535");
    ValueHelper.assertValueJson(new ScValueI16((short) -1), "-1");
    ValueHelper.assertValueJson(new ScValueU32(12345678), "12345678");
    ValueHelper.assertValueJson(new ScValueI32(-1), "-1");
    ValueHelper.assertValueJson(new ScValueU64(12345678987654321L), "\"12345678987654321\"");
    ValueHelper.assertValueJson(new ScValueI64(-1L), "\"-1\"");
    ValueHelper.assertValueJson(
        new ScValueU128(new BigInteger("340282366920938463463374607431768211455")),
        "\"340282366920938463463374607431768211455\"");
    ValueHelper.assertValueJson(
        new ScValueU256(
            new BigInteger(
                "115792089237316195423570985008687907853269984665640564039457584007913129639935")),
        "\"115792089237316195423570985008687907853269984665640564039457584007913129639935\"");
    ValueHelper.assertValueJson(new ScValueI128(new BigInteger("-1")), "\"-1\"");
  }

  @Test
  void toJsonAddress() {
    var addressValue =
        new ScValueAddress(HexFormat.of().parseHex("010000000000000000000000000000000000000020"));
    ValueHelper.assertValueJson(addressValue, "\"010000000000000000000000000000000000000020\"");
  }

  @Test
  void toJsonHash() {
    var hashValue = new ScValueHash(HexFormat.of().parseHex(ValidTestHexValues.HASH));
    ValueHelper.assertValueJson(hashValue, toJsonNodeFormat(ValidTestHexValues.HASH));
  }

  @Test
  void toJsonPublicKey() {
    var publicKeyValue =
        new ScValuePublicKey(HexFormat.of().parseHex(ValidTestHexValues.PUBLIC_KEY));
    ValueHelper.assertValueJson(publicKeyValue, toJsonNodeFormat(ValidTestHexValues.PUBLIC_KEY));
  }

  @Test
  void toJsonSignature() {
    var signatureValue =
        new ScValueSignature(HexFormat.of().parseHex(ValidTestHexValues.SIGNATURE));
    ValueHelper.assertValueJson(signatureValue, toJsonNodeFormat(ValidTestHexValues.SIGNATURE));
  }

  @Test
  void toJsonBlsPublicKey() {
    var blsPublicKeyValue =
        new ScValueBlsPublicKey(HexFormat.of().parseHex(ValidTestHexValues.BLS_PUBLIC_KEY));
    ValueHelper.assertValueJson(
        blsPublicKeyValue, toJsonNodeFormat(ValidTestHexValues.BLS_PUBLIC_KEY));
  }

  @Test
  void toJsonBlsSignature() {
    var blsSignatureValue =
        new ScValueBlsSignature(HexFormat.of().parseHex(ValidTestHexValues.BLS_SIGNATURE));
    ValueHelper.assertValueJson(
        blsSignatureValue, toJsonNodeFormat(ValidTestHexValues.BLS_SIGNATURE));
  }

  @Test
  void toJsonString() {
    var stringValue = new ScValueString("hello");
    ValueHelper.assertValueJson(stringValue, "\"hello\"");
  }

  /** A sized byte array writes as hex string. */
  @Test
  void toJsonSizedByteArray() {
    var arrayValue = ScValueArray.fromBytes(HexFormat.of().parseHex("aa00f0f0a245623657"));
    ValueHelper.assertValueJson(arrayValue, "\"aa00f0f0a245623657\"");
  }

  /** A sized array writes as json array. */
  @Test
  void toJsonArray() {
    List<ScValue> scValues = List.of(new ScValueI32(1), new ScValueI32(2), new ScValueI32(3));
    var arrayValue = new ScValueArray(scValues);
    ValueHelper.assertValueJson(arrayValue, "[ 1, 2, 3 ]");
  }

  /** An empty sized array writes as empty json array. */
  @Test
  void toJsonEmptyArray() {
    List<ScValue> scValues = List.of();
    var arrayValue = new ScValueArray(scValues);
    ValueHelper.assertValueJson(arrayValue, "[]");
  }

  @Test
  void toJsonVec() {
    List<ScValue> scValues = List.of(new ScValueI32(1), new ScValueI32(2), new ScValueI32(3));
    var vecValue = new ScValueVector(scValues);
    ValueHelper.assertValueJson(vecValue, "[ 1, 2, 3 ]");
  }

  @Test
  void toJsonStruct() {
    var map = new LinkedHashMap<String, ScValue>();
    map.put("a", new ScValueBool(true));
    map.put("b", new ScValueBool(false));
    var val = new ScValueStruct("struct", map);
    ValueHelper.assertValueJson(val, "{\n" + "  \"a\" : true,\n" + "  \"b\" : false\n" + "}");
  }

  @Test
  void toJsonEnum() {
    var fieldsForStruct = new LinkedHashMap<String, ScValue>();
    fieldsForStruct.put("a", new ScValueBool(true));
    fieldsForStruct.put("b", new ScValueBool(false));
    var struct = new ScValueStruct("struct", fieldsForStruct);
    var val = new ScValueEnum("enum", struct);
    ValueHelper.assertValueJson(
        val,
        "{\n" + "  \"@type\" : \"struct\",\n" + "  \"a\" : true,\n" + "  \"b\" : false\n" + "}");
  }

  @Test
  void toJsonComplexEnum() {
    var fieldsForCar = new LinkedHashMap<String, ScValue>();
    fieldsForCar.put("EngineSize", new ScValueU32(10));
    fieldsForCar.put("Length", new ScValueU32(20));
    var carStruct = new ScValueStruct("Car", fieldsForCar);
    var fieldsForBicycle = new LinkedHashMap<String, ScValue>();
    fieldsForBicycle.put("WheelDiameter", new ScValueU32(4));
    var bicycleStruct = new ScValueStruct("Bicycle", fieldsForBicycle);
    var fieldsForStruct = new LinkedHashMap<String, ScValue>();
    fieldsForStruct.put("a", new ScValueEnum("Vehicle", carStruct));
    fieldsForStruct.put("b", new ScValueEnum("Vehicle", bicycleStruct));
    var struct = new ScValueStruct("OneVariant", fieldsForStruct);
    ValueHelper.assertValueJson(
        struct,
        "{\n"
            + "  \"a\" : {\n"
            + "    \"@type\" : \"Car\",\n"
            + "    \"EngineSize\" : 10,\n"
            + "    \"Length\" : 20\n"
            + "  },\n"
            + "  \"b\" : {\n"
            + "    \"@type\" : \"Bicycle\",\n"
            + "    \"WheelDiameter\" : 4\n"
            + "  }\n"
            + "}");
  }

  @Test
  void toJsonSet() {
    List<ScValue> values = List.of(new ScValueI32(1), new ScValueI32(2), new ScValueI32(3));
    var setValue = new ScValueSet(values);
    ValueHelper.assertValueJson(setValue, "[ 1, 2, 3 ]");
  }

  @Test
  void toJsonOption() {
    var optionSome = new ScValueOption(new ScValueI32(42));
    ValueHelper.assertValueJson(
        optionSome, "{\n" + "  \"isSome\" : true,\n" + "  \"innerValue\" : 42\n" + "}");

    var optionNone = new ScValueOption(null);
    ValueHelper.assertValueJson(optionNone, "{\n" + "  \"isSome\" : false\n" + "}");
  }

  @Test
  void toJsonMap() {
    var map = new LinkedHashMap<ScValue, ScValue>();
    map.put(new ScValueI32(42), new ScValueBool(false));
    map.put(new ScValueI32(21), new ScValueBool(true));
    map.put(new ScValueI32(84), new ScValueBool(true));
    var mapValue = new ScValueMap(map);
    ValueHelper.assertValueJson(
        mapValue,
        "[ {\n"
            + "  \"key\" : 42,\n"
            + "  \"value\" : false\n"
            + "}, {\n"
            + "  \"key\" : 21,\n"
            + "  \"value\" : true\n"
            + "}, {\n"
            + "  \"key\" : 84,\n"
            + "  \"value\" : true\n"
            + "} ]");
  }

  @Test
  public void testTokenContract() {
    ValueHelper.assertValueJson(
        ValueHelper.loadState("token_contract.abi", "token_contract.state"),
        "{\n"
            + "  \"name\" : \"Token Name\",\n"
            + "  \"decimals\" : 18,\n"
            + "  \"symbol\" : \"SYMB\",\n"
            + "  \"owner\" : \"020000000000000000000000000000000000000001\",\n"
            + "  \"total_supply\" : \"123123123\",\n"
            + "  \"balances\" : [ {\n"
            + "    \"key\" : \"020000000000000000000000000000000000000001\",\n"
            + "    \"value\" : \"123123123\"\n"
            + "  } ],\n"
            + "  \"allowed\" : [ ]\n"
            + "}");
  }

  @Test
  void initialAuctionState() {
    ValueHelper.assertValueJson(
        ValueHelper.loadState("rust_example_auction_contract.abi", "auction_initial.state"),
        "{\n"
            + "  \"contract_owner\" : \"010000000000000000000000000000000000000001\",\n"
            + "  \"start_time\" : \"12346\",\n"
            + "  \"end_time\" : \"12423\",\n"
            + "  \"token_amount_for_sale\" : \"100\",\n"
            + "  \"commodity_token_type\" : \"02eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\",\n"
            + "  \"currency_token_type\" : \"02dddddddddddddddddddddddddddddddddddddddd\",\n"
            + "  \"highest_bidder\" : {\n"
            + "    \"bidder\" : \"010000000000000000000000000000000000000001\",\n"
            + "    \"amount\" : \"0\"\n"
            + "  },\n"
            + "  \"reserve_price\" : \"20\",\n"
            + "  \"min_increment\" : \"5\",\n"
            + "  \"claim_map\" : [ ],\n"
            + "  \"status\" : 0\n"
            + "}");
  }

  @Test
  void stateTest() {
    var stateStruct =
        new StructTypeSpec(
            "State",
            List.of(
                new FieldAbi("active", new SimpleTypeSpec(TypeSpec.TypeIndex.bool)),
                new FieldAbi("id", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                new FieldAbi("list", new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u64))),
                new FieldAbi("point", new NamedTypeRef(1))));
    var pointStruct =
        new StructTypeSpec(
            "Point",
            List.of(
                new FieldAbi("left", new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
                new FieldAbi("right", new SimpleTypeSpec(TypeSpec.TypeIndex.i32))));

    var contract =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(stateStruct, pointStruct),
            List.of(),
            List.of(),
            new NamedTypeRef(0));
    var stateHex =
        "01" // boolean: true
            + "02000000" // i32: 2
            + "02000000" // Vec<u64> length: 2
            + "0300000000000000" // Vec[0]: 3
            + "0400000000000000" // Vec[1]: 4
            + "05000000" // Point.left: 5
            + "06000000"; // Point.right: 6
    var stateVal = StateReader.create(HexFormat.of().parseHex(stateHex), contract).readState();

    ValueHelper.assertValueJson(
        stateVal,
        "{\n"
            + "  \"active\" : true,\n"
            + "  \"id\" : 2,\n"
            + "  \"list\" : [ \"3\", \"4\" ],\n"
            + "  \"point\" : {\n"
            + "    \"left\" : 5,\n"
            + "    \"right\" : 6\n"
            + "  }\n"
            + "}");
  }

  @Test
  void vecU8small() {
    var vec =
        new ScValueVector(
            List.of(new ScValueU8((byte) 1), new ScValueU8((byte) 2), new ScValueU8((byte) 3)));
    ValueHelper.assertValueJson(vec, "\"010203\"");
  }

  @Test
  void vecU8empty() {
    var vec = new ScValueVector(List.of());
    ValueHelper.assertValueJson(vec, "[]");
  }

  @Test
  void vecU8length256() {
    List<ScValue> values = new ArrayList<>();
    for (int i = 0; i < 256; i++) {
      values.add(new ScValueU8((byte) 0));
    }
    var vec = new ScValueVector(values);
    ValueHelper.assertValueJson(
        vec,
        "\"00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "\"");
  }

  @Test
  void vecU8Long() {
    List<ScValue> values = new ArrayList<>();
    for (int i = 0; i < 280; i++) {
      values.add(new ScValueU8((byte) 0));
    }
    var vec = new ScValueVector(values);
    ValueHelper.assertValueJson(
        vec,
        "\"00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "00000000000000000000000000000000"
            + "...\"");
  }

  @Test
  void toJsonAvlTreeMap() {
    var simpleVal = new ScValueAvlTreeMap(0, null);
    ValueHelper.assertValueJson(simpleVal, "{\n" + "  \"treeId\": 0\n" + "}");

    var map = new LinkedHashMap<ScValue, ScValue>();
    map.put(new ScValueI32(42), new ScValueBool(false));
    map.put(new ScValueI32(21), new ScValueBool(true));
    map.put(new ScValueI32(84), new ScValueBool(true));
    var avlTreeMapValue = new ScValueAvlTreeMap(0, map);
    ValueHelper.assertValueJson(
        avlTreeMapValue,
        "{\n"
            + "  \"treeId\": 0,\n"
            + "  \"map\": [\n"
            + "    {\n"
            + "      \"key\" : 42,\n"
            + "      \"value\" : false\n"
            + "    }, {\n"
            + "      \"key\" : 21,\n"
            + "      \"value\" : true\n"
            + "    }, {\n"
            + "      \"key\" : 84,\n"
            + "      \"value\" : true\n"
            + "    }\n"
            + "  ]\n"
            + "}");
  }

  private String toJsonNodeFormat(String string) {
    return String.format("\"%s\"", string);
  }
}
