package com.partisiablockchain.language.abiclient.exceptions;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.Serial;

/** MissingElementException. */
public final class MissingElementException extends RuntimeException {

  @Serial private static final long serialVersionUID = 1L;

  /**
   * Construct a new {@link MissingElementException}.
   *
   * @param message the message (formatted using String.format)
   * @param args the format arguments
   */
  @SuppressWarnings("AnnotateFormatMethod")
  public MissingElementException(String message, Object... args) {
    super(String.format(message, args));
  }
}
