package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** State representation of a u32 value. */
public record ScValueU32(@JsonProperty("value") int u32Value) implements ScValue {

  // Convert signed int representation to unsigned long representation
  private static final long SIGN_MODIFIER = 0x100000000L;

  @Override
  public long asLong() {
    if (u32Value < 0) {
      // Conversion to unsigned long
      return u32Value + SIGN_MODIFIER;
    }
    return u32Value;
  }

  @Override
  public BigInteger asBigInteger() {
    BigInteger mask = new BigInteger("ffffffff", 16);
    return BigInteger.valueOf(u32Value).and(mask);
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.u32;
  }
}
