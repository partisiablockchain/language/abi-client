package com.partisiablockchain.language.abiclient.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Contract state as serialized on the blockchain. */
@SuppressWarnings("ArrayRecordComponent")
public record StateBytes(byte[] state, byte[] avlTrees) {

  /**
   * Default constructor.
   *
   * @param state state bytes
   * @param avlTrees Avl Tree Map bytes
   */
  public StateBytes {}

  /**
   * Constructor.
   *
   * @param state state bytes
   */
  public StateBytes(byte[] state) {
    this(state, null);
  }
}
