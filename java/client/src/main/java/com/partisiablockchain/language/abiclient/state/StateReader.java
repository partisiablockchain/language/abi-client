package com.partisiablockchain.language.abiclient.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.AbstractReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.secata.stream.LittleEndianByteInput;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/** The state reader is a class for reading data serialized with the Rust trait ReadWriteState. */
public final class StateReader extends AbstractReader {

  private final TypeSpec stateType;
  private final Map<Integer, List<Map.Entry<byte[], byte[]>>> avlTrees;

  /**
   * Construct a new {@link StateReader} instance.
   *
   * @param in the input to read from
   * @param namedTypes the named types of the abi
   * @param avlTrees avl trees external to the wasm state
   */
  private StateReader(
      LittleEndianByteInput in,
      List<NamedTypeSpec> namedTypes,
      TypeSpec stateType,
      Map<Integer, List<Map.Entry<byte[], byte[]>>> avlTrees) {
    super(new AbiByteInput(in), namedTypes);
    this.stateType = stateType;
    this.avlTrees = avlTrees;
  }

  /**
   * Construct a new {@link StateReader} instance from a byte array and external avl trees.
   *
   * @param bytes bytes to construct the reader from
   * @param namedTypes the named types of the abi
   * @param avlTrees avl trees external to the wasm state
   */
  private StateReader(
      byte[] bytes,
      List<NamedTypeSpec> namedTypes,
      TypeSpec stateType,
      Map<Integer, List<Map.Entry<byte[], byte[]>>> avlTrees) {
    this(
        new LittleEndianByteInput(new ByteArrayInputStream(bytes)),
        namedTypes,
        stateType,
        avlTrees);
  }

  /**
   * Construct a new {@link StateReader} instance from a byte array and external avl trees.
   *
   * @param in bytes to construct the reader from
   * @param namedTypes the named types of the abi
   * @param avlTrees avl trees external to the wasm state
   */
  private StateReader(
      LittleEndianByteInput in,
      List<NamedTypeSpec> namedTypes,
      TypeSpec stateType,
      byte[] avlTrees) {
    this(in, namedTypes, stateType, readAvlTrees(avlTrees));
  }

  /**
   * Create a reader capable of reading contract state.
   *
   * @param bytes state bytes
   * @param contractAbi abi
   * @param avlTrees serialized avl trees
   * @return contract state reader
   */
  public static StateReader create(byte[] bytes, ContractAbi contractAbi, byte[] avlTrees) {
    LittleEndianByteInput in = new LittleEndianByteInput(new ByteArrayInputStream(bytes));
    if (contractAbi == null) {
      return new StateReader(in, List.of(), null, avlTrees);
    } else {
      List<NamedTypeSpec> namedTypes = contractAbi.namedTypes();
      TypeSpec stateType = contractAbi.stateType();
      return new StateReader(in, namedTypes, stateType, avlTrees);
    }
  }

  /**
   * Create a reader capable of reading contract state.
   *
   * @param bytes state bytes
   * @param contractAbi abi
   * @return contract state reader
   */
  public static StateReader create(byte[] bytes, ContractAbi contractAbi) {
    return create(bytes, contractAbi, null);
  }

  /**
   * Read a map from the stream.
   *
   * @param type the type spec for the map
   * @return the read map
   */
  @Override
  public ScValueMap readMap(MapTypeSpec type) {
    final LinkedHashMap<ScValue, ScValue> map = new LinkedHashMap<>();

    final var length = in.readI32();
    for (int i = 0; i < length; i++) {
      ScValue keyInstance = readGeneric(type.keyType());
      ScValue valueInstance = readGeneric(type.valueType());

      map.put(keyInstance, valueInstance);
    }
    return new ScValueMap(map);
  }

  /**
   * Read a set from the stream.
   *
   * @param set the type spec
   * @return the read value
   */
  @Override
  public ScValueSet readSet(SetTypeSpec set) {
    final int length = in.readI32();
    List<ScValue> list = new ArrayList<>();
    for (int i = 0; i < length; i++) {
      list.add(readGeneric(set.valueType()));
    }
    return new ScValueSet(list);
  }

  @Override
  public ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec avlTreeMap) {
    final int treeId = in.readI32();
    if (avlTrees == null) {
      return new ScValueAvlTreeMap(treeId, null);
    }
    List<Map.Entry<byte[], byte[]>> tree = avlTrees.get(treeId);
    if (tree == null) {
      throw new RuntimeException(
          "Tried to read Avl Tree Map with tree id %d, but it didn't exist".formatted(treeId));
    }
    final LinkedHashMap<ScValue, ScValue> map = new LinkedHashMap<>();
    tree.forEach(
        (entry) -> {
          ScValue key =
              new StateReader(entry.getKey(), namedTypes, stateType, avlTrees)
                  .readGeneric(avlTreeMap.keyType());
          ScValue value =
              new StateReader(entry.getValue(), namedTypes, stateType, avlTrees)
                  .readGeneric(avlTreeMap.valueType());
          map.put(key, value);
        });
    return new ScValueAvlTreeMap(treeId, map);
  }

  /**
   * Returns the state representation of the contract.
   *
   * @return StateValueStruct representation of the state of the contract.
   */
  public ScValueStruct readState() {
    return readStruct((StructTypeSpec) namedTypes.get(((NamedTypeRef) stateType).index()));
  }

  /**
   * Read a type instance based on the {@link TypeSpec}.
   *
   * @param type the type spec to read
   * @return the resulting instance
   */
  public ScValue readStateValue(TypeSpec type) {
    return readGeneric(type);
  }

  private static Map<Integer, List<Map.Entry<byte[], byte[]>>> readAvlTrees(
      byte[] serializedTrees) {
    if (serializedTrees == null) {
      return null;
    }
    LittleEndianByteInput treeStream =
        new LittleEndianByteInput(new ByteArrayInputStream(serializedTrees));
    TypeSpec type =
        new MapTypeSpec(
            new OptionTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32)),
            new OptionTypeSpec(
                new OptionTypeSpec(
                    new MapTypeSpec(
                        new OptionTypeSpec(
                            new OptionTypeSpec(
                                new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))),
                        new OptionTypeSpec(
                            new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8)))))));
    ScValue treeValue = new StateReader(treeStream, null, null, (byte[]) null).readGeneric(type);
    Map<Integer, List<Map.Entry<byte[], byte[]>>> map = new HashMap<>();
    treeValue
        .mapValue()
        .map()
        .forEach(
            (key, value) ->
                map.put(key.optionValue().innerValue().i32Value(), convertSingleTree(value)));
    return map;
  }

  private static List<Map.Entry<byte[], byte[]>> convertSingleTree(ScValue singleTree) {
    return singleTree
        .optionValue()
        .innerValue()
        .optionValue()
        .innerValue()
        .mapValue()
        .map()
        .entrySet()
        .stream()
        .map(
            entry ->
                Map.entry(
                    entry
                        .getKey()
                        .optionValue()
                        .innerValue()
                        .optionValue()
                        .innerValue()
                        .vecU8Value(),
                    entry.getValue().optionValue().innerValue().vecU8Value()))
        .toList();
  }
}
