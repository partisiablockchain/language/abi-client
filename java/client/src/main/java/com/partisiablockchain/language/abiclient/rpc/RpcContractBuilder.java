package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abistreams.AbiByteOutput;

/**
 * Builder for contract invocations. Used to generate serialized bytes representing the built action
 * in accordance with the RPC Binary Format described in the <a
 * href="https://partisiablockchain.gitlab.io/documentation/abiv.html">abi documentation</a>
 */
public final class RpcContractBuilder extends AbstractBuilder {

  /**
   * Creates a builder object which can be used to build the rpc for an action invocation on the
   * smart contract. Requires that a contract is specified, to fetch the ContractInvocation from the
   * actionName. Performs type checking according to the action defined in the contract.
   *
   * @param contractAbi the contract to interact with - required non-null
   * @param actionName name of action as a string
   */
  public RpcContractBuilder(ContractAbi contractAbi, String actionName) {
    super(contractAbi.namedTypes(), "", RpcContractProducer.create(contractAbi, actionName));
  }

  /**
   * Creates a builder object which can be used to build the rpc for an action invocation on the
   * smart contract. Requires that a contract is specified, to fetch the ContractInvocation from the
   * shortName. Performs type checking according to the action defined in the contract.
   *
   * @param contractAbi the contract to interact with - required non-null.
   * @param shortName the shortName of the action to generate rpc for - required non-null.
   * @param kind the kind of the action to generate rpc for.
   */
  public RpcContractBuilder(
      ContractAbi contractAbi, byte[] shortName, ContractInvocationKind kind) {
    super(contractAbi.namedTypes(), "", RpcContractProducer.create(contractAbi, shortName, kind));
  }

  /**
   * Creates a builder object which can be used to build the rpc. Since no type arguments are
   * specified, the type checking is not done.
   *
   * @param shortName the shortname of the function
   */
  public RpcContractBuilder(byte[] shortName) {
    super(null, "", RpcContractProducer.create(shortName));
  }

  /**
   * Creates and returns the bytes representing the given action. Creates bytes in big endian
   * format.
   *
   * @return the bytes of the action
   */
  public byte[] getBytes() {
    return AbiByteOutput.serializeBigEndian((out) -> getAggregateProducer().write(out));
  }
}
