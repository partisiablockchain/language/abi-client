package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/** An AvlTreeMap. */
@JsonSerialize(using = ScValueAvlTreeMap.AvlTreeMapSerializer.class)
public record ScValueAvlTreeMap(int treeId, LinkedHashMap<ScValue, ScValue> map)
    implements ScValue {
  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.AvlTreeMap;
  }

  @Override
  public ScValueAvlTreeMap avlTreeMapValue() {
    return this;
  }

  /**
   * Apply mapping functions to map key and value if present, else return null.
   *
   * @param keyMapper the function to apply to the keys of the map.
   * @param valueMapper the function to apply to the values of the map.
   * @param <K> type of the map's keys.
   * @param <V> type of the map's values.
   * @return the result of the callback or null.
   */
  public <K, V> Map<K, V> mapKeysValues(
      Function<ScValue, K> keyMapper, Function<ScValue, V> valueMapper) {
    if (map != null) {
      return map.entrySet().stream()
          .collect(
              Collectors.toMap(
                  entry -> keyMapper.apply(entry.getKey()),
                  entry -> valueMapper.apply(entry.getValue())));
    } else {
      return null;
    }
  }

  static final class AvlTreeMapSerializer extends JsonSerializer<ScValueAvlTreeMap> {

    @Override
    public void serialize(
        ScValueAvlTreeMap value, JsonGenerator gen, SerializerProvider serializers)
        throws IOException {
      gen.writeObjectFieldStart("value");
      gen.writeNumberField("treeId", value.treeId);
      if (value.map != null) {
        gen.writeFieldName("map");
        new ScValueMap.ScValueMapSerializer().serialize(value.map, gen, serializers);
      }
      gen.writeEndObject();
    }

    @Override
    public void serializeWithType(
        ScValueAvlTreeMap value,
        JsonGenerator gen,
        SerializerProvider serializers,
        TypeSerializer typeSer)
        throws IOException {
      typeSer.writeTypePrefix(gen, typeSer.typeId(value, JsonToken.START_OBJECT));
      serialize(value, gen, serializers);
      typeSer.writeTypeSuffix(gen, typeSer.typeId(value, JsonToken.START_OBJECT));
    }
  }
}
