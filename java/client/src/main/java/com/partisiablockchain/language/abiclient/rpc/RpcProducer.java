package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.abiclient.builder.AggregateProducer;
import com.partisiablockchain.language.abiclient.builder.Producer;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the abstract rpc builder. Allows users to build up the rpc used to make action
 * invocations. Expects that #build is called separately to correctly generate the entire rpc.
 */
final class RpcProducer implements AggregateProducer {

  private final BinderInvocation binderInvocation;
  private final List<Producer> elements = new ArrayList<>();
  private RpcContractProducer contractCall;

  RpcProducer(BinderInvocation binderInvocation) {
    this.binderInvocation = requireNonNull(binderInvocation);
  }

  static RpcProducer create(ContractAbi contractAbi, String invocationName) {
    BinderInvocation function = contractAbi.getBinderInvocationByName(invocationName);
    if (function == null) {
      throw new IllegalArgumentException(
          "Contract does not contain function with name '%s'".formatted(invocationName));
    }
    return new RpcProducer(function);
  }

  static RpcProducer create(ContractAbi contractAbi, Byte binderShortname, TransactionKind kind) {
    BinderInvocation binderInvocation = contractAbi.getBinderInvocation(binderShortname, kind);
    if (binderInvocation == null) {
      String binderErrorText =
          binderShortname != null
              ? "binder shortname '0x%02x'".formatted(binderShortname)
              : "no binder shortname";
      throw new IllegalArgumentException(
          ("Contract does not contain function with %s and " + "kind '%s'")
              .formatted(binderErrorText, kind));
    }
    return new RpcProducer(binderInvocation);
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (elements.size() < binderInvocation.arguments().size()) {
      var nextIndex = elements.size();
      var nextArgument = binderInvocation.arguments().get(nextIndex);
      return nextArgument.type();
    } else {
      return null;
    }
  }

  @Override
  public void addElement(Producer argument) {
    if (elements.size() < binderInvocation.arguments().size()) {
      this.elements.add(argument);
    } else {
      throw new IllegalArgumentException("Cannot add more arguments than the invocation expects");
    }
  }

  @Override
  public void write(AbiOutput out) {
    if (binderInvocation.shortname() != null) {
      out.writeU8(binderInvocation.shortname());
    }
    for (Producer element : elements) {
      element.write(out);
    }
    validate();
    if (contractCall != null) {
      contractCall.write(out);
    }
  }

  private void validate() {
    if (elements.size() < binderInvocation.arguments().size()) {
      var missingArgument = binderInvocation.arguments().get(elements.size());
      throw new MissingElementException("Missing argument '%s'", missingArgument.name());
    }
    if (binderInvocation.contractCallKindId() != null && contractCall == null) {
      throw new MissingElementException("Missing contract call");
    }
  }

  @Override
  public String getFieldName() {
    if (elements.size() < binderInvocation.arguments().size()) {
      var nextIndex = elements.size();
      var nextArgument = binderInvocation.arguments().get(nextIndex);
      return binderInvocation.name() + "/" + nextArgument.name();
    } else {
      return "";
    }
  }

  public void addContractCall(RpcContractProducer contractCall) {
    this.contractCall = contractCall;
  }

  BinderInvocation getBinderInvocation() {
    return binderInvocation;
  }
}
