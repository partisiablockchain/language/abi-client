package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;

/**
 * Builder for generating rpc/state for enum types, which supports type checking of the expected
 * fields in the enum variant's struct.
 */
public final class EnumVariantProducer implements AggregateProducer {

  private final int discriminant;
  private final String variantName;

  private final StructProducer structProducer;

  /**
   * EnumVariantProducer object, capable of generating the rpc.state for an EnumVariant type.
   *
   * @param variantType type of the underlying struct
   * @param discriminant the discriminant of the specific enum variant.
   * @param errorPath error path
   */
  public EnumVariantProducer(StructTypeSpec variantType, int discriminant, String errorPath) {
    if (variantType == null) {
      this.variantName = "";
    } else {
      this.variantName = variantType.name();
    }

    this.structProducer = new StructProducer(variantType, errorPath);
    this.discriminant = discriminant;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeU8(discriminant);
    this.structProducer.write(out);
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    return this.structProducer.getTypeSpecForElement();
  }

  @Override
  public String getFieldName() {
    return "/" + variantName + this.structProducer.getFieldName();
  }

  @Override
  public void addElement(Producer argument) {
    this.structProducer.addElement(argument);
  }
}
