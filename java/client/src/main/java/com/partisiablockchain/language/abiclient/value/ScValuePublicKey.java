package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;

/**
 * A public key is used to send encrypted transactions on the blockchain. Transactions must be
 * encrypted under a public key registered on the blockchain otherwise they will fail.
 *
 * <p>The key consists of a {@value PUBLIC_KEY_LENGTH} long byte array.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValuePublicKey(
    @JsonSerialize(using = ScValueAddress.HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** A public key is 33 bytes long. */
  public static final int PUBLIC_KEY_LENGTH = 33;

  /**
   * Create a new public key. The supplied value must be {@value PUBLIC_KEY_LENGTH} bytes long.
   *
   * @param bytes The byte value of the public key.
   */
  public ScValuePublicKey {
    if (bytes.length != PUBLIC_KEY_LENGTH) {
      throw new ScReadException(
          "PublicKey expects exactly %d bytes, but found %d", PUBLIC_KEY_LENGTH, bytes.length);
    }
  }

  /**
   * Return this public key as a hex encoded string.
   *
   * @return hex encoded string
   */
  public String asString() {
    return HexFormat.of().formatHex(bytes);
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.PublicKey;
  }

  @Override
  public ScValuePublicKey publicKeyValue() {
    return this;
  }

  @Override
  public BlockchainPublicKey asBlockchainPublicKey() {
    return BlockchainPublicKey.fromEncodedEcPoint(bytes);
  }
}
