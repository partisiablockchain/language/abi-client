package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AggregateProducer;
import com.partisiablockchain.language.abiclient.builder.Producer;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;

/**
 * Implementation of the abstract rpc builder. Allows users to build up the rpc used to make
 * contract invocations. Expects that #build is called separately to correctly generate the entire
 * rpc.
 */
final class RpcContractProducer implements AggregateProducer {

  private final List<Producer> elements = new ArrayList<>();
  private final byte[] shortname;
  private final ContractInvocation action;

  RpcContractProducer(byte[] shortname, ContractInvocation action) {
    this.shortname = shortname;
    this.action = action;
  }

  static RpcContractProducer create(byte[] shortname) {
    return new RpcContractProducer(shortname, null);
  }

  static RpcContractProducer create(ContractAbi contractAbi, String actionName) {
    ContractInvocation function = contractAbi.getFunctionByName(actionName);
    if (function == null) {
      throw new IllegalArgumentException(
          "Contract does not contain contract action with name '%s'".formatted(actionName));
    }
    return new RpcContractProducer(null, function);
  }

  static RpcContractProducer create(
      ContractAbi contractAbi, byte[] shortname, ContractInvocationKind kind) {
    ContractInvocation function = contractAbi.getFunction(shortname, kind);
    if (function == null) {
      throw new IllegalArgumentException(
          "Contract does not contain contract action with kind '%s' and shortname '0x%s'"
              .formatted(
                  kind.name().toLowerCase(Locale.ENGLISH), HexFormat.of().formatHex(shortname)));
    }
    return new RpcContractProducer(null, function);
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (action == null || action.arguments().size() == elements.size()) {
      return null;
    } else {
      var nextIndex = elements.size();
      var nextArgument = action.arguments().get(nextIndex);
      return nextArgument.type();
    }
  }

  @Override
  public void addElement(Producer argument) {
    if (action != null && action.arguments().size() == elements.size()) {
      throw new IllegalArgumentException("Cannot add more arguments than the action expects.");
    }

    this.elements.add(argument);
  }

  @Override
  public void write(AbiOutput out) {
    if (action == null) {
      out.writeBytes(shortname);
    } else if (action.kind().hasShortname()) {
      out.writeBytes(action.shortname());
    }

    for (Producer element : elements) {
      element.write(out);
    }
    validate();
  }

  private void validate() {
    if (action != null && elements.size() < action.arguments().size()) {
      var missingArgument = action.arguments().get(elements.size());
      throw new MissingElementException("Missing argument '%s'", missingArgument.name());
    }
  }

  @Override
  public String getFieldName() {
    if (action == null || action.arguments().size() == elements.size()) {
      return "";
    } else {
      var nextIndex = elements.size();
      var nextArgument = action.arguments().get(nextIndex);
      return action.name() + "/" + nextArgument.name();
    }
  }

  public ContractInvocation getFnAbi() {
    return action;
  }
}
