package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** State representation of a u128 value. */
public record ScValueU128(@JsonProperty("value") BigInteger u128Value) implements ScValue {

  private static final BigInteger MAX_VALUE =
      new BigInteger("340282366920938463463374607431768211455");

  /**
   * Constructs a u128 value representation, ensuring that the value is within the allowed range of
   * a u128.
   *
   * @param u128Value the u128 value
   */
  public ScValueU128 {
    if (u128Value.signum() < 0 || u128Value.compareTo(MAX_VALUE) > 0) {
      throw new IllegalArgumentException(
          "Value specified for u128 exceeds allowed range of unsigned 128-bit value");
    }
  }

  @Override
  public BigInteger asBigInteger() {
    return u128Value;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.u128;
  }
}
