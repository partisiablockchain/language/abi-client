package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** State representation of a u8 value. */
public record ScValueU8(@JsonProperty("value") byte u8Value) implements ScValue {

  private static final int SIGN_MODIFIER = 0x100;

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.u8;
  }

  @Override
  public int asInt() {
    // If two's complement representation is negative, convert to unsigned representation.
    if (u8Value < 0) {
      return u8Value + SIGN_MODIFIER;
    }

    return u8Value;
  }

  @Override
  public long asLong() {
    if (u8Value < 0) {
      return (long) u8Value + SIGN_MODIFIER;
    }
    return u8Value;
  }

  @Override
  public BigInteger asBigInteger() {
    BigInteger mask = new BigInteger("ff", 16);
    return BigInteger.valueOf(u8Value).and(mask);
  }
}
