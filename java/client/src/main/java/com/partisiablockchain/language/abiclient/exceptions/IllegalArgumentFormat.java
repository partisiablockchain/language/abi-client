package com.partisiablockchain.language.abiclient.exceptions;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.Serial;

/**
 * Runtime exception for illegal formats of arguments - thrown when the rpc/state encounters an
 * argument which does not follow the expected structure, defined by the specification.
 */
public final class IllegalArgumentFormat extends RuntimeException {

  @Serial private static final long serialVersionUID = 5L;

  /**
   * Constructor for runtime exception for illegal formats of arguments, using {@link String#format}
   * as formatting.
   *
   * @param message error message
   * @param args arguments for formatting
   */
  @SuppressWarnings("AnnotateFormatMethod")
  public IllegalArgumentFormat(String message, Object... args) {
    super(String.format(message, args));
  }
}
