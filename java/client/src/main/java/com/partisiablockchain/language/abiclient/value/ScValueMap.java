package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/** State representation of a map. */
public record ScValueMap(
    @JsonSerialize(using = ScValueMap.ScValueMapSerializer.class) @JsonProperty("value")
        LinkedHashMap<ScValue, ScValue> map)
    implements ScValue {

  /**
   * Returns the value for the given key.
   *
   * @param key the key to obtain the value from
   * @return the value associated with the specified key
   */
  public ScValue get(ScValue key) {
    return map.get(key);
  }

  /**
   * Returns the key set of the map as a StateValueSet.
   *
   * @return key set of the map as a StateValueSet
   */
  public ScValueSet keySet() {
    return new ScValueSet(map.keySet().stream().toList());
  }

  @Override
  public ScValueMap mapValue() {
    return this;
  }

  /**
   * Returns the size of the map.
   *
   * @return size of the map
   */
  public int size() {
    return map.size();
  }

  /**
   * Returns true if the map is empty, false otherwise.
   *
   * @return true if map is empty, false otherwise
   */
  @JsonIgnore
  public boolean isEmpty() {
    return map.size() == 0;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Map;
  }

  static final class ScValueMapSerializer extends JsonSerializer<Map<ScValue, ScValue>> {

    @Override
    public void serialize(
        Map<ScValue, ScValue> scValueMap,
        JsonGenerator jsonGenerator,
        SerializerProvider serializerProvider)
        throws IOException {
      jsonGenerator.writeStartArray();
      for (Map.Entry<ScValue, ScValue> entry : scValueMap.entrySet()) {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("key", entry.getKey());
        jsonGenerator.writeObjectField("value", entry.getValue());
        jsonGenerator.writeEndObject();
      }
      jsonGenerator.writeEndArray();
    }
  }
}
