package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.language.abimodel.model.TypeSpec;

/**
 * A BLS (Boneh-Lynn-Shacham) is a different type of {@link ScValueSignature signature}, that allows
 * for aggregation.
 *
 * <p>It is used to produce a joint (single) signature on e.g. a block between a group of users,
 * rather than one signature per user as is the case for {@link ScValueSignature signature}. A BLS
 * signature consists of a {@value BLS_SIGNATURE_LENGTH} byte array.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValueBlsSignature(
    @JsonSerialize(using = ScValueAddress.HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** A BLS signature is 48 bytes long. */
  public static final int BLS_SIGNATURE_LENGTH = 48;

  /**
   * Create a new BLS signature. The supplied value must be {@value BLS_SIGNATURE_LENGTH} bytes
   * long.
   *
   * @param bytes The byte value of the BLS signature.
   */
  public ScValueBlsSignature {
    if (bytes.length != BLS_SIGNATURE_LENGTH) {
      throw new ScReadException(
          "BlsSignature expects exactly %d bytes, but found %d",
          BLS_SIGNATURE_LENGTH, bytes.length);
    }
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.BlsSignature;
  }

  @Override
  public ScValueBlsSignature blsSignatureValue() {
    return this;
  }

  @Override
  public BlsSignature asBlsSignature() {
    return new BlsSignature(bytes);
  }
}
