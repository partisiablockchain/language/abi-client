package com.partisiablockchain.language.abiclient.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.AbstractReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiBitInput;
import com.secata.stream.BitInput;
import com.secata.stream.CompactBitArray;

/** Deserialize Bit streams into specific values. */
public final class ZkSecretReader extends AbstractReader {

  /**
   * Create a new reader for a given contract, reading from the provided bit stream.
   *
   * @param secretBits the stream of bits to read from.
   * @param contractAbi the abi of the contract, with the type information.
   */
  public ZkSecretReader(BitInput secretBits, ContractAbi contractAbi) {
    super(new AbiBitInput(secretBits), contractAbi.namedTypes());
  }

  /**
   * Create a new reader for a given contract, reading from the provided bit stream.
   *
   * @param secretBits the stream of bits to read from.
   * @param contractAbi the abi of the contract, with the type information.
   */
  public ZkSecretReader(CompactBitArray secretBits, ContractAbi contractAbi) {
    super(new AbiBitInput(BitInput.create(secretBits.data())), contractAbi.namedTypes());
  }

  @Override
  protected ScValueMap readMap(MapTypeSpec map) {
    throw new UnsupportedOperationException("Type Map is not supported in secret variables");
  }

  @Override
  protected ScValueSet readSet(SetTypeSpec set) {
    throw new UnsupportedOperationException("Type Set is not supported in secret variables");
  }

  @Override
  protected ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec map) {
    throw new UnsupportedOperationException("Type AvlTreeMap is not supported in secret variables");
  }

  /**
   * Read a secret type and return the Value type. The type must be public equivalent of the secret
   * type.
   *
   * @param secretType the secret type to deserialize into.
   * @return the value type, that matches the given secret type specification.
   */
  public ScValue readSecret(TypeSpec secretType) {
    return readGeneric(secretType);
  }
}
