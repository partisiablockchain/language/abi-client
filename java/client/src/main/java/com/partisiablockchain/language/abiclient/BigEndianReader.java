package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiByteInput;

/** Simple reader for deserializing a big endian payload. */
public final class BigEndianReader extends AbstractReader {
  /**
   * Constructor a new {@link AbstractReader} instance.
   *
   * @param in the input to read from
   * @param contractAbi the contract abi for specifying named types
   */
  public BigEndianReader(byte[] in, ContractAbi contractAbi) {
    super(AbiByteInput.createBigEndian(in), contractAbi.namedTypes());
  }

  @Override
  protected ScValueMap readMap(MapTypeSpec type) {
    throw new RuntimeException("Type %s is not supported in rpc".formatted(type.typeIndex()));
  }

  @Override
  protected ScValueSet readSet(SetTypeSpec type) {
    throw new RuntimeException("Type %s is not supported in rpc".formatted(type.typeIndex()));
  }

  @Override
  protected ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec type) {
    throw new RuntimeException("Type %s is not supported in rpc".formatted(type.typeIndex()));
  }

  /**
   * Read a given type from the payload.
   *
   * @param type the type to be read
   * @return the value that has been deserialized
   */
  public ScValue read(TypeSpec type) {
    return readGeneric(type);
  }
}
