package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Producer for generating rpc/state for struct types, which supports type checking of the expected
 * fields in the struct.
 */
public final class StructProducer implements AggregateProducer {

  private final StructTypeSpec structTypeSpec;
  private final List<Producer> elements = new ArrayList<>();
  private final String errorPath;

  /**
   * StructProducer object, capable of generating the rpc/state for a Struct type with the given
   * fields.
   *
   * @param structTypeSpec type of the struct
   * @param errorPath error path
   */
  public StructProducer(StructTypeSpec structTypeSpec, String errorPath) {
    this.structTypeSpec = structTypeSpec;
    this.errorPath = errorPath;
  }

  @Override
  public void addElement(Producer argument) {
    this.elements.add(argument);
  }

  @Override
  public void write(AbiOutput out) {
    for (Producer element : elements) {
      element.write(out);
    }
    validate();
  }

  private void validate() {
    if (structTypeSpec != null && elements.size() != structTypeSpec.fields().size()) {
      var missingArgument = structTypeSpec.fields().get(elements.size());
      throw new MissingElementException(
          "In %s, Missing argument '%s'", errorPath, missingArgument.name());
    }
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (structTypeSpec != null) {
      var lastIndex = elements.size();
      if (lastIndex >= structTypeSpec.fields().size()) {
        throw new IllegalArgumentFormat(
            "In %s, Cannot add more arguments than the struct has fields.", errorPath);
      }
      return structTypeSpec.field(lastIndex).type();
    }
    return null;
  }

  @Override
  public String getFieldName() {
    if (structTypeSpec != null) {
      var lastIndex = elements.size();
      return "/" + structTypeSpec.field(lastIndex).name();
    }
    return "";
  }
}
