package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.secata.stream.BigEndianByteOutput;

/**
 * Builder for contract actions. Used to generate serialized bytes representing the built action in
 * accordance with the RPC Binary Format described in the <a
 * href="https://partisiablockchain.gitlab.io/documentation/abiv.html">abi documentation</a>
 */
public final class RpcBuilder extends AbstractBuilder {

  private final ContractAbi contractAbi;

  /**
   * Creates a builder object which can be used to build the rpc for an action invocation on the
   * smart contract. Requires that a contract is specified, to fetch the ContractInvocation from the
   * actionName. Performs type checking according to the action defined in the contract.
   *
   * @param contractAbi the contract to interact with - required non-null
   * @param binderInvocationName name of binder invocation as a string
   */
  public RpcBuilder(ContractAbi contractAbi, String binderInvocationName) {
    super(contractAbi.namedTypes(), "", RpcProducer.create(contractAbi, binderInvocationName));
    this.contractAbi = contractAbi;
  }

  /**
   * Creates a builder object which can be used to build the rpc for an action invocation on the
   * smart contract. Requires that a contract is specified, to fetch the ContractFunction from the
   * shortname. Performs type checking according to the action defined in the contract.
   *
   * @param contractAbi the contract to interact with - required non-null.
   * @param binderShortname the binder shortName of the action to generate rpc for.
   * @param kind the kind of the action to generate rpc for.
   */
  public RpcBuilder(ContractAbi contractAbi, Byte binderShortname, TransactionKind kind) {
    super(contractAbi.namedTypes(), "", RpcProducer.create(contractAbi, binderShortname, kind));
    this.contractAbi = contractAbi;
  }

  /**
   * Creates and returns the bytes representing the given action. Creates bytes in big endian
   * format.
   *
   * @return the bytes of the action
   */
  public byte[] getBytes() {
    return BigEndianByteOutput.serialize(
        (out) -> getAggregateProducer().write(new AbiByteOutput(out)));
  }

  /**
   * Add a contract call to this builder. The binder invocation must be able to call the given
   * contract function.
   *
   * @param actionName name of the contract function to call
   * @return new builder capable of building the contract call
   */
  public AbstractBuilder contractCall(String actionName) {
    RpcProducer producer = (RpcProducer) getAggregateProducer();
    BinderInvocation binderInvocation = producer.getBinderInvocation();
    assertCanCall(binderInvocation);
    RpcContractProducer rpcContractProducer = RpcContractProducer.create(contractAbi, actionName);
    ContractInvocation function = rpcContractProducer.getFnAbi();
    if (binderInvocation.contractCallKindId() != function.kind().kindId()) {
      throw new IllegalArgumentException(
          "Binder invocation '%s' attempts to call kind '%d', but action '%s' has kind '%d'"
              .formatted(
                  binderInvocation.name(),
                  binderInvocation.contractCallKindId(),
                  function.name(),
                  function.kind().kindId()));
    }
    producer.addContractCall(rpcContractProducer);
    return new AbstractBuilder(contractAbi.namedTypes(), "contractCall/", rpcContractProducer) {};
  }

  /**
   * Add a contract call to this builder. The binder invocation must be able to call a contract
   * function with the given shortname.
   *
   * @param shortname shortname of the contract function to call
   * @return new builder capable of building the contract call
   */
  public AbstractBuilder contractCall(byte[] shortname) {
    RpcProducer producer = (RpcProducer) getAggregateProducer();
    BinderInvocation binderInvocation = producer.getBinderInvocation();
    assertCanCall(binderInvocation);
    ContractInvocationKind expectedFunctionKind =
        ContractInvocationKind.getKind(
            contractAbi.invocationKinds(), binderInvocation.contractCallKindId());
    RpcContractProducer rpcContractProducer =
        RpcContractProducer.create(contractAbi, shortname, expectedFunctionKind);
    producer.addContractCall(rpcContractProducer);
    return new AbstractBuilder(contractAbi.namedTypes(), "contractCall", rpcContractProducer) {};
  }

  private void assertCanCall(BinderInvocation binderInvocation) {
    if (binderInvocation.contractCallKindId() == null) {
      throw new IllegalArgumentException(
          "Binder invocation '%s' does not call contract".formatted(binderInvocation.name()));
    }
  }
}
