package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.function.Function;

/**
 * Object representation of the state value of an option type. The innerValue is deemed present if
 * it is non-null.
 */
public record ScValueOption(@JsonProperty("value") ScValue innerValue) implements ScValue {

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Option;
  }

  /**
   * Checks if the option has a value.
   *
   * @return true if the innerValue is non-null, false otherwise
   */
  @JsonIgnore
  public boolean isSome() {
    return innerValue != null;
  }

  @Override
  public ScValueOption optionValue() {
    return this;
  }

  /**
   * Apply callback function to inner value if present, else return null.
   *
   * @param callback the function to apply to the inner value.
   * @param <K> type of the return value.
   * @return the result of the callback or null.
   */
  public <K> K valueOrNull(Function<ScValue, K> callback) {
    if (isSome()) {
      return callback.apply(innerValue);
    } else {
      return null;
    }
  }
}
