package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.InputStream;

/**
 * Used for deserializing the Account plugin invocation from the Rpc of the AccountUpdatePlugin
 * events.
 */
public final class AccountPluginInvocationReader {

  /**
   * ABI is created with the Rust contracts account-invocation, account-contract-invocation and
   * account-state-global which resides <a href=
   * "https://gitlab.com/partisiablockchain/language/contracts/governance">here</a>.
   */
  private static final String ACCOUNT_CONTRACT_INVOCATION = "/account_contract_invocation.abi";

  private static final String ACCOUNT_INVOCATION = "/account_invocation.abi";
  private static final String ACCOUNT_GLOBAL_STATE = "/account_state_global.abi";
  private static final String ACCOUNT_CONTEXT_FREE = "/account_context_free.abi";

  private static final FileAbi accountContractInvocation =
      new AbiParser(readAbiFile(ACCOUNT_CONTRACT_INVOCATION)).parseAbi();
  private static final FileAbi accountInvocation =
      new AbiParser(readAbiFile(ACCOUNT_INVOCATION)).parseAbi();
  private static final FileAbi accountGlobalState =
      new AbiParser(readAbiFile(ACCOUNT_GLOBAL_STATE)).parseAbi();
  private static final FileAbi accountContextFree =
      new AbiParser(readAbiFile(ACCOUNT_CONTEXT_FREE)).parseAbi();

  /** Create a reader for Account Plugin invocations. */
  public AccountPluginInvocationReader() {}

  /**
   * Deserialize a context free invocation.
   *
   * @param rpc The RPC bytes of the invocation to deserialize.
   * @return The RpcValueFn of the invocation.
   */
  public RpcValue deserializeContextFreeInvocation(byte[] rpc) {
    RpcReader reader = RpcReader.create(rpc, accountContextFree, TransactionKind.Action);
    return reader.readRpc();
  }

  /**
   * Deserialize a local state update.
   *
   * @param targetAddress The address used to check if the state update is for an account or a
   *     contract.
   * @param rpc The RPC bytes of the local state update.
   * @return The RpcValueFn of the local state update.
   */
  public RpcValue deserializeLocalStateUpdate(BlockchainAddress targetAddress, byte[] rpc) {
    RpcReader reader;
    if (targetAddress.getType().equals(BlockchainAddress.Type.ACCOUNT)) {
      reader = RpcReader.create(rpc, accountInvocation, TransactionKind.Action);
    } else {
      reader = RpcReader.create(rpc, accountContractInvocation, TransactionKind.Action);
    }
    return reader.readRpc();
  }

  /**
   * Deserialize a global state update.
   *
   * @param rpc The RPC bytes of the global state update.
   * @return The RpcValueFn of the global state update
   */
  public RpcValue deserializeGlobalStateUpdate(byte[] rpc) {
    RpcReader reader = RpcReader.create(rpc, accountGlobalState, TransactionKind.Action);
    return reader.readRpc();
  }

  static byte[] readAbiFile(String fileName) {
    InputStream in =
        ExceptionConverter.call(
            () -> AccountPluginInvocationReader.class.getResourceAsStream(fileName),
            "Failed to read abi file: " + fileName);
    return ExceptionConverter.call(in::readAllBytes);
  }
}
