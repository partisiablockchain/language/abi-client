package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** State representation of an i128 value. */
public record ScValueI128(@JsonProperty("value") BigInteger i128Value) implements ScValue {

  private static final BigInteger MIN_VALUE =
      new BigInteger("-170141183460469231731687303715884105728");
  private static final BigInteger MAX_VALUE =
      new BigInteger("170141183460469231731687303715884105728");

  /**
   * Constructs an i128 value representation, ensuring that the value is within the allowed range of
   * an i128.
   *
   * @param i128Value the i128 value
   */
  public ScValueI128 {
    if (i128Value.compareTo(MIN_VALUE) < 0 || i128Value.compareTo(MAX_VALUE) > 0) {
      throw new IllegalArgumentException(
          "Specified value for i128 exceeds allowed range for signed 128-bit value");
    }
  }

  @Override
  public BigInteger asBigInteger() {
    return i128Value;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.i128;
  }
}
