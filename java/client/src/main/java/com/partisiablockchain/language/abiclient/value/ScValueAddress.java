package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.stream.SafeDataInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HexFormat;

/**
 * State representation of a blockchain address. The address consist of a {@value ADDRESS_LENGTH}
 * long byte array.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValueAddress(
    @JsonSerialize(using = HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** An address is 21 bytes long. */
  public static final int ADDRESS_LENGTH = 21;

  /**
   * Create a new address. The supplied value must be {@value ADDRESS_LENGTH} bytes long.
   *
   * @param bytes the byte value of the address
   */
  public ScValueAddress {
    if (bytes.length != ADDRESS_LENGTH) {
      throw new ScReadException(
          "Address expects exactly %d bytes, but found %d", ADDRESS_LENGTH, bytes.length);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o instanceof ScValueAddress that) {
      return Arrays.equals(bytes, that.bytes);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(bytes);
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Address;
  }

  @Override
  public ScValueAddress addressValue() {
    return this;
  }

  @Override
  public BlockchainAddress asBlockchainAddress() {
    return SafeDataInputStream.readFully(bytes, BlockchainAddress::read);
  }

  static final class HexSerializer extends JsonSerializer<byte[]> {

    @Override
    public void serialize(
        byte[] bytes, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      jsonGenerator.writeString(HexFormat.of().formatHex(bytes));
    }
  }
}
