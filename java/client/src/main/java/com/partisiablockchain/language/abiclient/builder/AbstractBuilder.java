package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;

/**
 * Abstract builder of RPC, state and zk inputs.
 *
 * <p>The builder is invoked to generate the desired arguments for an action/state invocation of a
 * contract. Given a Java model for the contract, the builder expects a ContractInvocation/Struct,
 * which is the action on which the rpc/state generation is desired. The builder allows building the
 * arguments by calling the appropriate method to generate the expected input of the action/state.
 * This is done by storing an object representation of that type. Whenever an argument is added, the
 * builder type checks according to the action, that the provided argument is of the same type as
 * the expected argument. Finally, when ready to generate the entire rpc/state, the outer builder's
 * #build can be invoked, to iterate through all object representations of the arguments and
 * generate the byte array for these. If type checking is not desired, the concrete rpc/state
 * builder can be instantiated with no arguments, meaning no type checking will be performed.
 */
public abstract class AbstractBuilder {

  /** Builder used to handle aggregate data structure. */
  private static final class AggregateBuilder extends AbstractBuilder {
    /**
     * Constructor for an abstract rpc/state builder for a specified contract.
     *
     * @param namedTypes the contract to generate rpc calls for.
     * @param errorPath path to the current element
     * @param aggregateProducer producer responsible for handling aggregate data structures
     */
    private AggregateBuilder(
        List<NamedTypeSpec> namedTypes, String errorPath, AggregateProducer aggregateProducer) {
      super(namedTypes, errorPath, aggregateProducer);
    }
  }

  private final List<NamedTypeSpec> namedTypes;
  private final String errorPath;

  /** Producer responsible for handling aggregate data structures. */
  private final AggregateProducer aggregateProducer;

  /**
   * Constructor for an abstract rpc/state builder for a specified contract.
   *
   * @param namedTypes the contract to generate rpc calls for.
   * @param errorPath path to the current element
   * @param aggregateProducer producer responsible for handling aggregate data structures
   */
  public AbstractBuilder(
      List<NamedTypeSpec> namedTypes, String errorPath, AggregateProducer aggregateProducer) {
    this.namedTypes = namedTypes;
    this.errorPath = errorPath;
    this.aggregateProducer = aggregateProducer;
  }

  /**
   * Adds a boolean argument to the builder.
   *
   * @param value the value of the boolean
   * @return updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBool(boolean value) {
    typeCheckAndAddArg(out -> out.writeBoolean(value), TypeSpec.TypeIndex.bool);
    return this;
  }

  /**
   * Adds a String argument to the builder.
   *
   * @param value the value of the string
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addString(String value) {
    typeCheckAndAddArg(out -> out.writeString(value), TypeSpec.TypeIndex.String);
    return this;
  }

  /**
   * Adds an Address argument to the builder.
   *
   * @param address the value of the address
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addAddress(byte[] address) {
    typeCheckAndAddArg(
        new AddressProducer(address, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.Address);
    return this;
  }

  /**
   * Adds an Address argument to the builder.
   *
   * @param address the value of the address
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addAddress(String address) {
    return addAddress(HexFormat.of().parseHex(address));
  }

  /**
   * Adds an Address argument to the builder.
   *
   * @param address the value of the address
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addAddress(BlockchainAddress address) {
    return addAddress(SafeDataOutputStream.serialize(address::write));
  }

  /**
   * Adds a Hash argument to the builder.
   *
   * @param hash the value of the hash
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addHash(byte[] hash) {
    typeCheckAndAddArg(
        new HashProducer(hash, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.Hash);
    return this;
  }

  /**
   * Adds a Hash argument to the builder.
   *
   * @param hash the value of the hash
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addHash(String hash) {
    return addHash(HexFormat.of().parseHex(hash));
  }

  /**
   * Adds a Hash argument to the builder.
   *
   * @param hash the value of the hash
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addHash(Hash hash) {
    return addHash(hash.getBytes());
  }

  /**
   * Adds a PublicKey argument to the builder.
   *
   * @param publicKey the value of the publicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addPublicKey(byte[] publicKey) {
    typeCheckAndAddArg(
        new PublicKeyProducer(publicKey, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.PublicKey);
    return this;
  }

  /**
   * Adds a PublicKey argument to the builder.
   *
   * @param publicKey the value of the publicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addPublicKey(String publicKey) {
    return addPublicKey(HexFormat.of().parseHex(publicKey));
  }

  /**
   * Adds a PublicKey argument to the builder.
   *
   * @param publicKey the value of the publicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addPublicKey(BlockchainPublicKey publicKey) {
    return addPublicKey(publicKey.asBytes());
  }

  /**
   * Adds a Signature argument to the builder.
   *
   * @param signature the value of the signature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSignature(byte[] signature) {
    typeCheckAndAddArg(
        new SignatureProducer(signature, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.Signature);
    return this;
  }

  /**
   * Adds a Signature argument to the builder.
   *
   * @param signature the value of the signature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSignature(String signature) {
    return addSignature(HexFormat.of().parseHex(signature));
  }

  /**
   * Adds a Signature argument to the builder.
   *
   * @param signature the value of the signature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSignature(Signature signature) {
    return addSignature(SafeDataOutputStream.serialize(signature::write));
  }

  /**
   * Adds a BlsPublicKey argument to the builder.
   *
   * @param blsPublicKey the value of the blsPublicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsPublicKey(byte[] blsPublicKey) {
    typeCheckAndAddArg(
        new BlsPublicKeyProducer(blsPublicKey, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.BlsPublicKey);
    return this;
  }

  /**
   * Adds a BlsPublicKey argument to the builder.
   *
   * @param blsPublicKey the value of the blsPublicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsPublicKey(String blsPublicKey) {
    return addBlsPublicKey(HexFormat.of().parseHex(blsPublicKey));
  }

  /**
   * Adds a BlsPublicKey argument to the builder.
   *
   * @param blsPublicKey the value of the blsPublicKey
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsPublicKey(BlsPublicKey blsPublicKey) {
    return addBlsPublicKey(blsPublicKey.getPublicKeyValue().serialize(true));
  }

  /**
   * Adds a BlsSignature argument to the builder.
   *
   * @param blsSignature the value of the blsSignature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsSignature(byte[] blsSignature) {
    typeCheckAndAddArg(
        new BlsSignatureProducer(blsSignature, errorPath + aggregateProducer.getFieldName()),
        TypeSpec.TypeIndex.BlsSignature);
    return this;
  }

  /**
   * Adds a BlsSignature argument to the builder.
   *
   * @param blsSignature the value of the blsSignature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsSignature(String blsSignature) {
    return addBlsSignature(HexFormat.of().parseHex(blsSignature));
  }

  /**
   * Adds a BlsSignature argument to the builder.
   *
   * @param blsSignature the value of the blsSignature
   * @return the updated builder.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addBlsSignature(BlsSignature blsSignature) {
    return addBlsSignature(blsSignature.getSignatureValue().serialize(true));
  }

  /**
   * Adds a u8 argument to the builder.
   *
   * @param value the value of the u8, represented as a byte
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU8(byte value) {
    typeCheckAndAddArg(out -> out.writeU8(value), TypeSpec.TypeIndex.u8);
    return this;
  }

  /**
   * Adds a u16 argument to the builder.
   *
   * @param value the value of the u16, represented as a short
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU16(short value) {
    typeCheckAndAddArg(out -> out.writeU16(value), TypeSpec.TypeIndex.u16);
    return this;
  }

  /**
   * Adds a u32 argument to the builder.
   *
   * @param value the value of the u32, represented as an int (interpreted as a u32 as per
   *     https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU32(int value) {
    typeCheckAndAddArg(out -> out.writeU32(value), TypeSpec.TypeIndex.u32);
    return this;
  }

  /**
   * Adds a u64 argument to the builder.
   *
   * @param value the value of the u64, represented as a long (interpreted as a u64 long as per
   *     https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU64(long value) {
    typeCheckAndAddArg(out -> out.writeU64(value), TypeSpec.TypeIndex.u64);
    return this;
  }

  /**
   * Adds a u128 argument to the builder.
   *
   * @param value the value of the argument, represented as a BigInteger
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU128(BigInteger value) {
    typeCheckAndAddArg(out -> out.writeUnsignedBigInteger(value, 16), TypeSpec.TypeIndex.u128);
    return this;
  }

  /**
   * Adds a u256 argument to the builder.
   *
   * @param value the value of the argument, represented as a BigInteger
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addU256(BigInteger value) {
    typeCheckAndAddArg(out -> out.writeUnsignedBigInteger(value, 32), TypeSpec.TypeIndex.u256);
    return this;
  }

  /**
   * Adds an i8 argument to the builder.
   *
   * @param value the value of the i8 argument, represented as a byte
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addI8(byte value) {
    typeCheckAndAddArg(out -> out.writeI8(value), TypeSpec.TypeIndex.i8);
    return this;
  }

  /**
   * Adds an i16 argument to the builder.
   *
   * @param value the value of the i16 argument, represented as a short
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addI16(short value) {
    typeCheckAndAddArg(out -> out.writeI16(value), TypeSpec.TypeIndex.i16);
    return this;
  }

  /**
   * Adds an i32 argument to the builder.
   *
   * @param value the value of the i32 argument, represented as an int
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addI32(int value) {
    typeCheckAndAddArg(out -> out.writeI32(value), TypeSpec.TypeIndex.i32);
    return this;
  }

  /**
   * Adds an i64 argument to the builder.
   *
   * @param value the value of the i64 argument, represented as a long
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addI64(long value) {
    typeCheckAndAddArg(out -> out.writeI64(value), TypeSpec.TypeIndex.i64);
    return this;
  }

  /**
   * Adds an i128 argument to the builder.
   *
   * @param value the value of the argument, represented as a BigInteger
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addI128(BigInteger value) {
    typeCheckAndAddArg(out -> out.writeSignedBigInteger(value, 16), TypeSpec.TypeIndex.i128);
    return this;
  }

  /**
   * Adds a struct argument to the builder.
   *
   * @return a new builder, for building the arguments for the struct.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addStruct() {
    TypeSpec struct = aggregateProducer.getTypeSpecForElement();
    typeCheck(TypeSpec.TypeIndex.Named);
    StructTypeSpec structType = null;
    if (namedTypes != null) {
      structType = (StructTypeSpec) namedTypes.get(((NamedTypeRef) struct).index());
    }
    String newErrorPath = errorPath + aggregateProducer.getFieldName();
    StructProducer structProducer = new StructProducer(structType, newErrorPath);
    aggregateProducer.addElement(structProducer);
    return new AggregateBuilder(namedTypes, newErrorPath, structProducer);
  }

  /**
   * Adds an enumVariant argument to the builder.
   *
   * @param discriminant the discriminant of the enum variant.
   * @return a new builder, for building the arguments for the enumVariant.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addEnumVariant(int discriminant) {
    TypeSpec enumTypeRef = aggregateProducer.getTypeSpecForElement();
    typeCheck(TypeSpec.TypeIndex.Named);
    NamedTypeRef structRef = getEnumVariantTypeRef((NamedTypeRef) enumTypeRef, discriminant);
    StructTypeSpec structTypeSpec;
    if (namedTypes == null) {
      structTypeSpec = null;
    } else {
      structTypeSpec = (StructTypeSpec) namedTypes.get(structRef.index());
    }
    String newErrorPath = errorPath + aggregateProducer.getFieldName();
    EnumVariantProducer enumVariantProducer =
        new EnumVariantProducer(structTypeSpec, discriminant, newErrorPath);
    aggregateProducer.addElement(enumVariantProducer);
    return new AggregateBuilder(namedTypes, newErrorPath, enumVariantProducer);
  }

  /**
   * Adds a vector argument to the builder.
   *
   * @return a new builder, for building the arguments for the vector.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addVec() {
    TypeSpec vec = aggregateProducer.getTypeSpecForElement();
    typeCheck(TypeSpec.TypeIndex.Vec);

    VecProducer vecProducer = new VecProducer((VecTypeSpec) vec);
    String fieldName = aggregateProducer.getFieldName();
    aggregateProducer.addElement(vecProducer);
    return new AggregateBuilder(namedTypes, errorPath + fieldName, vecProducer);
  }

  /**
   * Adds a vector of u8 argument to the builder.
   *
   * @param values list of values for the u8 vector
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addVecU8(byte... values) {
    typeCheck(TypeSpec.TypeIndex.Vec);
    VecTypeSpec vecType = (VecTypeSpec) aggregateProducer.getTypeSpecForElement();
    if (vecType != null) {
      TypeSpec.TypeIndex expectedType = vecType.valueType().typeIndex();
      if (expectedType != TypeSpec.TypeIndex.u8) {
        throw new TypeCheckingException(
            "In %s, Expected type Vec<%s>, but got Vec<u8>",
            errorPath + aggregateProducer.getFieldName(), expectedType);
      }
    }
    aggregateProducer.addElement(
        writer -> {
          writer.writeI32(values.length);
          writer.writeBytes(values);
        });
    return this;
  }

  /**
   * Adds a map argument to the builder.
   *
   * @return a new builder, for building the arguments for the map.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addMap() {
    TypeSpec map = aggregateProducer.getTypeSpecForElement();
    typeCheck(TypeSpec.TypeIndex.Map);

    String newErrorPath = errorPath + aggregateProducer.getFieldName();
    MapProducer mapStateProducer = new MapProducer((MapTypeSpec) map, newErrorPath);
    aggregateProducer.addElement(mapStateProducer);
    return new AggregateBuilder(namedTypes, newErrorPath, mapStateProducer);
  }

  /**
   * Adds a set argument to the builder.
   *
   * @return a new builder, for building the arguments for the set.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSet() {
    return addVec();
  }

  /**
   * Adds an option argument to the buildaer.
   *
   * @return a new builder for building the argument for the option type.
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addOption() {
    TypeSpec optionType = aggregateProducer.getTypeSpecForElement();
    typeCheck(TypeSpec.TypeIndex.Option);

    String newErrorPath = errorPath + aggregateProducer.getFieldName();
    OptionProducer optionProducer = new OptionProducer((OptionTypeSpec) optionType, newErrorPath);
    aggregateProducer.addElement(optionProducer);
    return new AggregateBuilder(namedTypes, newErrorPath, optionProducer);
  }

  /**
   * Adds a sized byte array argument to the builder.
   *
   * @param values list of values for the array
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSizedByteArray(byte... values) {
    final AbstractBuilder arrayBuilder = addSizedArray();
    for (final byte v : values) {
      arrayBuilder.addU8(v);
    }
    return this;
  }

  /**
   * Adds a sized array argument to the builder.
   *
   * @return the updated builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addSizedArray() {
    TypeSpec spec = typeCheck(TypeSpec.TypeIndex.SizedArray);

    String newErrorPath = errorPath + aggregateProducer.getFieldName();

    SizedArrayProducer arrayProducer =
        new SizedArrayProducer((SizedArrayTypeSpec) spec, newErrorPath);
    aggregateProducer.addElement(arrayProducer);
    return new AggregateBuilder(namedTypes, newErrorPath, arrayProducer);
  }

  /**
   * Add the tree id of an avl tree map to the builder.
   *
   * @param treeId the tree id of the tree
   * @return the update builder
   */
  @CanIgnoreReturnValue
  public AbstractBuilder addAvlTreeMap(int treeId) {
    typeCheckAndAddArg(out -> out.writeI32(treeId), TypeSpec.TypeIndex.AvlTreeMap);
    return this;
  }

  /**
   * Performs type checking between the type of the argument and the expected type as defined by the
   * contract. If type checking is successful, add the argument to the list of arguments, and
   * increment the argumentIndex counter. Type checking only happens, if action is not null.
   *
   * @param argument the argument to type check and add to the list of arguments
   * @param expected the expected type of the argument, defined by the contract. returns null if no
   *     contract is specified
   */
  void typeCheckAndAddArg(Producer argument, TypeSpec.TypeIndex expected) {
    typeCheck(expected);
    aggregateProducer.addElement(argument);
  }

  TypeSpec typeCheck(TypeSpec.TypeIndex actualType) {
    TypeSpec typeSpecForArgument = aggregateProducer.getTypeSpecForElement();
    if (typeSpecForArgument != null) {
      TypeSpec.TypeIndex expectedType = typeSpecForArgument.typeIndex();
      if (expectedType != actualType) {
        throw new TypeCheckingException(
            "In %s, Expected type %s, but got %s",
            errorPath + aggregateProducer.getFieldName(), expectedType, actualType);
      }
    }
    return typeSpecForArgument;
  }

  /**
   * Write builder to a ByteOutput.
   *
   * @param out the ByteOutput to write to
   */
  public void write(AbiOutput out) {
    aggregateProducer.write(out);
  }

  @CheckReturnValue
  private NamedTypeRef getEnumVariantTypeRef(NamedTypeRef enumTypeRef, int discriminant) {
    if (namedTypes == null) {
      return null;
    } else {
      EnumTypeSpec enumTypeSpec = (EnumTypeSpec) namedTypes.get(enumTypeRef.index());
      EnumVariant variant = enumTypeSpec.variant(discriminant);
      if (variant == null) {
        throw new IllegalArgumentFormat(
            "In %s, Undefined variant discriminant %d for %s",
            errorPath + aggregateProducer.getFieldName(), discriminant, enumTypeSpec.name());
      } else {
        return variant.def();
      }
    }
  }

  /**
   * Getter for the aggregate producer.
   *
   * @return the aggregate producer
   */
  @CheckReturnValue
  protected AggregateProducer getAggregateProducer() {
    return aggregateProducer;
  }
}
