package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Producer for generating state for map types, which supports type checking of elements added to
 * the map.
 */
final class MapProducer implements AggregateProducer {

  private final MapTypeSpec mapTypeSpec;
  private final List<Producer> elements = new ArrayList<>();
  private final String errorPath;

  /**
   * MapProducer object, capable of generating the state for a Map type with a specified value type.
   *
   * @param mapTypeSpec the representation of the MapTypeSpec from the abi model
   * @param errorPath error path
   */
  public MapProducer(MapTypeSpec mapTypeSpec, String errorPath) {
    this.mapTypeSpec = mapTypeSpec;
    this.errorPath = errorPath;
  }

  @Override
  public void write(AbiOutput out) {
    if (elements.size() % 2 == 1) {
      throw new MissingElementException("In %s, Missing value for key", errorPath);
    }
    out.writeI32(size());
    for (Producer element : elements) {
      element.write(out);
    }
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (mapTypeSpec != null) {
      if (elements.size() % 2 == 0) {
        return mapTypeSpec.keyType();
      } else {
        return mapTypeSpec.valueType();
      }
    }
    return null;
  }

  @Override
  public String getFieldName() {
    int index = elements.size() / 2;
    if (elements.size() % 2 == 0) {
      return "/" + index + "/key";
    } else {
      return "/" + index + "/value";
    }
  }

  @Override
  public void addElement(Producer element) {
    this.elements.add(element);
  }

  /**
   * Returns the number of key-value pairs of the map.
   *
   * @return number of key-value pairs
   */
  private int size() {
    return (elements.size() + 1) / 2;
  }
}
