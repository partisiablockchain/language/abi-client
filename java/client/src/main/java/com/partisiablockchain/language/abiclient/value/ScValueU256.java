package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** An unsigned 256-bit integer. */
public record ScValueU256(@JsonProperty("value") BigInteger u256Value) implements ScValue {
  private static final BigInteger MAX_VALUE =
      new BigInteger(
          "115792089237316195423570985008687907853269984665640564039457584007913129639935");

  /**
   * Constructs a u256 value representation, ensuring that the value is within the allowed range of
   * a u256.
   *
   * @param u256Value the u256 value
   */
  public ScValueU256 {
    if (u256Value.signum() < 0 || u256Value.compareTo(MAX_VALUE) > 0) {
      throw new IllegalArgumentException(
          "Value specified for u256 exceeds allowed range of unsigned 256-bit value");
    }
  }

  @Override
  public BigInteger asBigInteger() {
    return u256Value;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.u256;
  }
}
