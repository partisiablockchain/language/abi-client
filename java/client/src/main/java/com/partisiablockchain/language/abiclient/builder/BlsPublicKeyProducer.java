package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.value.ScValueBlsPublicKey;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.HexFormat;

/** Representation object of a bls public key type, which is yet to be serialized. */
final class BlsPublicKeyProducer implements Producer {

  private final byte[] blsPublicKey;

  /**
   * A bls public key object representation, which is yet to be serialized. Performs explicit test,
   * to ensure that the length of the bls public key is {@value
   * ScValueBlsPublicKey#BLS_PUBLIC_KEY_LENGTH} bytes.
   *
   * @param blsPublicKey the bls public key to be serialized.
   * @param errorPath error path
   */
  public BlsPublicKeyProducer(byte[] blsPublicKey, String errorPath) {
    if (blsPublicKey.length != ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH) {
      throw new IllegalArgumentFormat(
          "In %s, Bls public key must have length %d bytes, got length = %d, value = %s",
          errorPath,
          ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH,
          blsPublicKey.length,
          HexFormat.of().formatHex(blsPublicKey));
    }
    this.blsPublicKey = blsPublicKey;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeBytes(blsPublicKey);
  }
}
