package com.partisiablockchain.language.abiclient.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.secata.stream.CompactBitArray;

/**
 * Builder for the secret input of a function. Used to generate the bits representing the built
 * secret input to be turned into encrypted shares.
 */
public final class ZkInputBuilder extends AbstractBuilder {

  private ZkInputBuilder(ContractAbi contractAbi, ZkInputProducer zkInputProducer) {
    super(contractAbi.namedTypes(), "", zkInputProducer);
  }

  /**
   * Creates a new ZkInputBuilder from the name of the function and the ContractAbi.
   *
   * @param name the name of the function.
   * @param contractAbi the abi of the secret input.
   * @return a new ZkInputBuilder.
   */
  public static ZkInputBuilder createZkInputBuilder(String name, ContractAbi contractAbi) {
    ContractInvocation fn = contractAbi.getFunctionByName(name);
    if (fn == null) {
      throw new IllegalArgumentException(
          "Contract does not have function with name %s".formatted(name));
    }
    if (fn.secretArgument() == null) {
      throw new IllegalArgumentException(
          "Function %s is not a secret input function".formatted(name));
    }
    return new ZkInputBuilder(contractAbi, new ZkInputProducer(fn));
  }

  /**
   * Creates and returns the bits of the secret input.
   *
   * @return the bits of the secret input.
   */
  public CompactBitArray getBits() {
    return AbiBitOutput.serialize((out) -> getAggregateProducer().write(out));
  }
}
