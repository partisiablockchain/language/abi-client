package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Representation of a rpc call. */
@JsonSerialize(using = RpcValue.RpcValueSerializer.class)
public record RpcValue(
    BinderInvocation binderInvocation,
    ContractInvocation contractInvocation,
    List<ScValue> binderArguments,
    List<ScValue> contractArguments) {

  /**
   * Get all arguments of this rcp call. Includes both binder and contract arguments.
   *
   * @return arguments of this rpc call.
   */
  public List<ScValue> arguments() {
    ArrayList<ScValue> arguments = new ArrayList<>(binderArguments);
    arguments.addAll(contractArguments);
    return arguments;
  }

  /**
   * Returns the size of the arguments.
   *
   * @return the size of the arguments.
   */
  public int size() {
    return arguments().size();
  }

  /**
   * Returns the argument at position index.
   *
   * @param index the index into the arguments
   * @return the argument at position index
   */
  public ScValue get(int index) {
    return arguments().get(index);
  }

  static final class RpcValueSerializer extends JsonSerializer<RpcValue> {

    @Override
    public void serialize(RpcValue value, JsonGenerator gen, SerializerProvider serializers)
        throws IOException {
      gen.writeStartObject();
      gen.writeFieldName("binderInvocationName");
      gen.writeString(value.binderInvocation().name());
      gen.writeObjectFieldStart("binderArguments");
      var binderArgs = value.binderInvocation().arguments();
      for (int i = 0; i < binderArgs.size(); i++) {
        gen.writeObjectField(binderArgs.get(i).name(), value.binderArguments().get(i));
      }
      gen.writeEndObject();
      ContractInvocation fn = value.contractInvocation();
      if (fn != null) {
        gen.writeStringField("actionName", fn.name());
        gen.writeObjectFieldStart("contractArguments");
        List<ArgumentAbi> abiArgs = fn.arguments();
        for (int i = 0; i < abiArgs.size(); i++) {
          gen.writeObjectField(abiArgs.get(i).name(), value.contractArguments.get(i));
        }
        gen.writeEndObject();
      }
      gen.writeEndObject();
    }
  }
}
