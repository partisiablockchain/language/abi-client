package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HexFormat;
import java.util.Map;

/** Can create JSON objects from ScValues. */
public final class JsonValueConverter {
  private JsonValueConverter() {}

  /**
   * Reads the value and creates a JSON object.
   *
   * @param scValue the value to create a JSON object from
   * @return JSON object of the value
   */
  public static JsonNode toJson(ScValue scValue) {
    return ExceptionConverter.call(
        () -> {
          var out = new ByteArrayOutputStream();

          var factory = new JsonFactory();
          var json = factory.createGenerator(out);
          writeGeneric(scValue, json);

          json.flush();
          var in = new ByteArrayInputStream(out.toByteArray());
          return new ObjectMapper().readTree(in);
        },
        "IO error");
  }

  /**
   * Reads the value and creates a string representation of the corresponding JSON object.
   *
   * @param scValue the value to create a JSON string from
   * @return string representation of the value
   */
  public static String toJsonString(ScValue scValue) {
    var json = toJson(scValue);
    var objectMapper = new ObjectMapper();
    var objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    return ExceptionConverter.call(() -> objectWriter.writeValueAsString(json), "IO error");
  }

  /**
   * Create a typed JSON representation of the given ScValue.
   *
   * @param value the value to create JSON from
   * @return the typed JSON object
   */
  public static JsonNode toTypedJson(ScValue value) {
    var objectMapper = new ObjectMapper();
    return objectMapper.valueToTree(value);
  }

  /**
   * Writes the type and value of the input to Json.
   *
   * @param scValue The generic value to serialize.
   * @param json A generator for json.
   * @throws IOException Throws IOException in case of writing errors.
   */
  static void writeGeneric(ScValue scValue, JsonGenerator json) throws IOException {
    var type = scValue.getType();
    switch (type) {
      case Vec -> writeVec(scValue.vecValue(), json);
      case Option -> writeOption(scValue.optionValue(), json);
      case Map -> writeMap(scValue.mapValue(), json);
      case AvlTreeMap -> writeAvlTreeMap(scValue.avlTreeMapValue(), json);
      case Set -> writeSet(scValue.setValue(), json);
      case Named -> writeNamed(scValue, json);
      case SizedArray -> writeArray(scValue.arrayValue(), json);
      default -> writeSimple(scValue, json); // Simple type
    }
  }

  private static void writeNamed(ScValue scValue, JsonGenerator json) throws IOException {
    if (scValue instanceof ScValueStruct) {
      writeStruct(scValue.structValue(), json);
    } else {
      writeEnum(scValue.enumValue(), json);
    }
  }

  private static void writeSimple(ScValue scValue, JsonGenerator json) throws IOException {
    var type = scValue.getType();
    switch (type) {
      case i8, u8, i16, u16, i32 -> json.writeNumber(scValue.asInt());
      case u32 -> json.writeNumber(scValue.asLong());
      case i64, u64, i128, u128, u256 -> json.writeString(scValue.asBigInteger().toString());
      case bool -> json.writeBoolean(scValue.boolValue());
      case Address -> json.writeString(HexFormat.of().formatHex(scValue.addressValue().bytes()));
      case Hash -> json.writeString(HexFormat.of().formatHex(scValue.hashValue().bytes()));
      case PublicKey ->
          json.writeString(HexFormat.of().formatHex(scValue.publicKeyValue().bytes()));
      case Signature ->
          json.writeString(HexFormat.of().formatHex(scValue.signatureValue().bytes()));
      case BlsPublicKey ->
          json.writeString(HexFormat.of().formatHex(scValue.blsPublicKeyValue().bytes()));
      case BlsSignature ->
          json.writeString(HexFormat.of().formatHex(scValue.blsSignatureValue().bytes()));
      default -> json.writeString(scValue.stringValue()); // String
    }
  }

  private static void writeVec(ScValueVector vecValue, JsonGenerator json) throws IOException {
    if (vecValue.size() != 0 && vecValue.get(0).getType() == TypeSpec.TypeIndex.u8) {
      var values = vecValue.values();
      var dots = "";
      int maxBytesToShow = 256;
      if (vecValue.size() > maxBytesToShow) {
        values = values.subList(0, maxBytesToShow);
        dots = "...";
      }
      var bytes = new byte[values.size()];
      for (int i = 0; i < values.size(); i++) {
        bytes[i] = values.get(i).u8Value();
      }
      json.writeString(HexFormat.of().formatHex(bytes) + dots);
    } else {
      json.writeStartArray();
      for (var v : vecValue.values()) {
        writeGeneric(v, json);
      }
      json.writeEndArray();
    }
  }

  private static void writeSet(ScValueSet setValue, JsonGenerator json) throws IOException {
    json.writeStartArray();
    for (var v : setValue.values()) {
      writeGeneric(v, json);
    }
    json.writeEndArray();
  }

  private static void writeStruct(ScValueStruct structValue, JsonGenerator json)
      throws IOException {
    json.writeStartObject();
    var fieldMap = structValue.fieldsMap();
    for (Map.Entry<String, ScValue> entry : fieldMap.entrySet()) {
      json.writeFieldName(entry.getKey());
      writeGeneric(entry.getValue(), json);
    }
    json.writeEndObject();
  }

  private static void writeEnum(ScValueEnum enumValue, JsonGenerator json) throws IOException {
    json.writeStartObject();
    ScValueStruct scValueStruct = enumValue.item();
    json.writeFieldName("@type");
    json.writeString(scValueStruct.name());
    var fieldMap = scValueStruct.fieldsMap();
    for (Map.Entry<String, ScValue> entry : fieldMap.entrySet()) {
      json.writeFieldName(entry.getKey());
      writeGeneric(entry.getValue(), json);
    }
    json.writeEndObject();
  }

  private static void writeOption(ScValueOption optionValue, JsonGenerator json)
      throws IOException {
    json.writeStartObject();
    boolean isSome = optionValue.isSome();
    json.writeFieldName("isSome");
    json.writeBoolean(isSome);
    if (isSome) {
      json.writeFieldName("innerValue");
      writeGeneric(optionValue.innerValue(), json);
    }
    json.writeEndObject();
  }

  private static void writeMap(ScValueMap mapValue, JsonGenerator json) throws IOException {
    json.writeStartArray();
    var fieldMap = mapValue.map();
    for (Map.Entry<ScValue, ScValue> entry : fieldMap.entrySet()) {
      json.writeStartObject();
      json.writeFieldName("key");
      writeGeneric(entry.getKey(), json);
      json.writeFieldName("value");
      writeGeneric(entry.getValue(), json);
      json.writeEndObject();
    }
    json.writeEndArray();
  }

  private static void writeAvlTreeMap(ScValueAvlTreeMap avlTreeMapValue, JsonGenerator json)
      throws IOException {
    json.writeStartObject();
    json.writeFieldName("treeId");
    json.writeNumber(avlTreeMapValue.treeId());
    if (avlTreeMapValue.map() != null) {
      json.writeFieldName("map");
      writeMap(new ScValueMap(avlTreeMapValue.map()), json);
    }
    json.writeEndObject();
  }

  private static void writeArray(ScValueArray arrayValue, JsonGenerator json) throws IOException {
    if (!arrayValue.isEmpty() && arrayValue.get(0).getType() == TypeSpec.TypeIndex.u8) {
      byte[] bytes = arrayValue.vecU8Value();
      json.writeString(HexFormat.of().formatHex(bytes));
    } else {
      json.writeStartArray();
      for (var v : arrayValue.values()) {
        writeGeneric(v, json);
      }
      json.writeEndArray();
    }
  }
}
