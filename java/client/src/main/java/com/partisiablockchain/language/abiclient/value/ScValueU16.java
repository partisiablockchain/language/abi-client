package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** State representation of a u16 value. */
public record ScValueU16(@JsonProperty("value") short u16Value) implements ScValue {

  private static final int SIGN_MODIFIER = 0x10000;

  @Override
  public int asInt() {
    if (u16Value < 0) {
      // If interpreted as signed, convert to unsigned
      return u16Value + SIGN_MODIFIER;
    }
    return u16Value;
  }

  @Override
  public long asLong() {
    if (u16Value < 0) {
      return (long) u16Value + SIGN_MODIFIER;
    }
    return u16Value;
  }

  @Override
  public BigInteger asBigInteger() {
    BigInteger mask = new BigInteger("ffff", 16);
    return BigInteger.valueOf(u16Value).and(mask);
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.u16;
  }
}
