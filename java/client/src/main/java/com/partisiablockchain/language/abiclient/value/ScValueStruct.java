package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.LinkedHashMap;

/** Object representation of a struct, consisting of a name and n fields. */
public record ScValueStruct(
    @JsonIgnore String name, @JsonProperty("value") LinkedHashMap<String, ScValue> fieldsMap)
    implements ScValue {

  /**
   * Returns the number of fields present in the struct.
   *
   * @return number of fields in struct.
   */
  public int size() {
    return fieldsMap.size();
  }

  /**
   * Get a field value by the name of the field.
   *
   * @param fieldName the name of the field
   * @return a value if the field exists, null otherwise
   */
  public ScValue getFieldValue(String fieldName) {
    return fieldsMap.get(fieldName);
  }

  @Override
  public ScValueStruct structValue() {
    return this;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Named;
  }
}
