package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.Iterator;
import java.util.List;

/** A state value vector. */
public record ScValueVector(
    @JsonSerialize(using = VecU8Serializer.class) @JsonProperty("value") List<ScValue> values)
    implements ScValue {

  /**
   * Returns the size of the vector.
   *
   * @return the size of the vector.
   */
  public int size() {
    return values.size();
  }

  /**
   * Create a ScValueVector containing the giving byte array, where each byte is converted to a
   * ScValueU8.
   *
   * @param bytes the byte array.
   * @return the ScValueVector created.
   */
  public static ScValueVector fromBytes(byte[] bytes) {
    List<ScValue> valueVector = new ArrayList<>();
    for (byte byteVal : bytes) {
      valueVector.add(new ScValueU8(byteVal));
    }
    return new ScValueVector(valueVector);
  }

  /**
   * Returns true if the vector is empty, false otherwise.
   *
   * @return true if vector is empty, false otherwise
   */
  @JsonIgnore
  public boolean isEmpty() {
    return values.size() == 0;
  }

  /**
   * Returns the element at position index.
   *
   * @param index the index into the vector
   * @return the element at position index
   */
  public ScValue get(int index) {
    return values.get(index);
  }

  /**
   * Returns an iterator over the values in the vector.
   *
   * @return iterator over the vector elements
   */
  public Iterator<ScValue> iterator() {
    return values.iterator();
  }

  @Override
  public ScValueVector vecValue() {
    return this;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Vec;
  }

  @Override
  public byte[] vecU8Value() {
    if (size() == 0) {
      return new byte[0];
    } else if (get(0).getType() != TypeSpec.TypeIndex.u8) {
      throw new ScReadException("Cannot read Vec u8 for current type");
    } else {
      byte[] byteArray = new byte[values.size()];
      for (int i = 0; i < values.size(); i++) {
        byteArray[i] = values.get(i).u8Value();
      }
      return byteArray;
    }
  }

  static final class VecU8Serializer extends JsonSerializer<List<ScValue>> {

    @Override
    public void serialize(List<ScValue> values, JsonGenerator gen, SerializerProvider serializers)
        throws IOException {
      if (!values.isEmpty() && values.get(0).getType() == TypeSpec.TypeIndex.u8) {
        byte[] byteArray = new byte[values.size()];
        for (int i = 0; i < values.size(); i++) {
          byteArray[i] = values.get(i).u8Value();
        }
        gen.writeString(HexFormat.of().formatHex(byteArray));
      } else {
        gen.writeStartArray();
        for (ScValue value : values) {
          gen.writeObject(value);
        }
        gen.writeEndArray();
      }
    }
  }
}
