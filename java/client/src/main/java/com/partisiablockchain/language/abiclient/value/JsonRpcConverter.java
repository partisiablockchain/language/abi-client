package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.abiclient.rpc.RpcValue;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/** Can create JSON objects from ScValues. */
public final class JsonRpcConverter {
  private JsonRpcConverter() {}

  /**
   * Reads the rpc and creates a JSON object.
   *
   * @param rpcValue the value to create a JSON object from
   * @return JSON object of the rpc
   */
  public static JsonNode toJson(RpcValue rpcValue) {
    return ExceptionConverter.call(
        () -> {
          var out = new ByteArrayOutputStream();

          var factory = new JsonFactory();
          var json = factory.createGenerator(out);
          writeRpc(rpcValue, json);

          json.flush();
          var in = new ByteArrayInputStream(out.toByteArray());
          return new ObjectMapper().readTree(in);
        },
        "IO error");
  }

  /**
   * Reads the rpc and creates a string representation of the corresponding JSON object.
   *
   * @param rpcValue the value to create a JSON string from
   * @return string representation of the rpc
   */
  public static String toJsonString(RpcValue rpcValue) {
    var json = toJson(rpcValue);
    var objectMapper = new ObjectMapper();
    var objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    return ExceptionConverter.call(() -> objectWriter.writeValueAsString(json), "IO error");
  }

  /**
   * Create a typed JSON representation of the given RpcValue.
   *
   * @param value the value to create JSON from
   * @return the typed JSON object
   */
  public static JsonNode toTypedJson(RpcValue value) {
    var objectMapper = new ObjectMapper();
    return objectMapper.valueToTree(value);
  }

  private static void writeRpc(RpcValue rpcValue, JsonGenerator json) throws IOException {
    json.writeStartObject();
    json.writeFieldName("binderInvocationName");
    json.writeString(rpcValue.binderInvocation().name());
    json.writeFieldName("binderArguments");
    json.writeStartObject();
    var binderArgs = rpcValue.binderInvocation().arguments();
    for (int i = 0; i < binderArgs.size(); i++) {
      json.writeFieldName(binderArgs.get(i).name());
      var arg = rpcValue.binderArguments().get(i);
      JsonValueConverter.writeGeneric(arg, json);
    }
    json.writeEndObject();
    ContractInvocation fn = rpcValue.contractInvocation();
    if (fn != null) {
      json.writeFieldName("actionName");
      json.writeString(fn.name());
      json.writeFieldName("contractArguments");
      json.writeStartObject();
      var contractArgs = fn.arguments();
      for (int i = 0; i < contractArgs.size(); i++) {
        json.writeFieldName(contractArgs.get(i).name());
        var arg = rpcValue.contractArguments().get(i);
        JsonValueConverter.writeGeneric(arg, json);
      }
      json.writeEndObject();
    }
    json.writeEndObject();
  }
}
