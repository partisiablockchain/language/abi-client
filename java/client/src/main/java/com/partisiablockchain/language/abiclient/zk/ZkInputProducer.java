package com.partisiablockchain.language.abiclient.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AggregateProducer;
import com.partisiablockchain.language.abiclient.builder.Producer;
import com.partisiablockchain.language.abiclient.exceptions.MissingElementException;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;

/** Producer for the secret input of a function. */
final class ZkInputProducer implements AggregateProducer {
  private final ContractInvocation fn;
  private Producer element;

  /**
   * Producer for constructing zk secret inputs.
   *
   * @param fn the function for which to produce the secret input
   */
  public ZkInputProducer(ContractInvocation fn) {
    this.fn = fn;
  }

  /**
   * Returns the TypeSpec of the secret argument. If kind is ZkSecretInput a default type of i32 is
   * returned.
   *
   * @return the TypeSpec of the secret argument
   */
  @Override
  public TypeSpec getTypeSpecForElement() {
    return fn.secretArgument().type();
  }

  @Override
  public String getFieldName() {
    return "secret_input";
  }

  @Override
  public void addElement(Producer argument) {
    if (element != null) {
      throw new IllegalArgumentException("Cannot add secret input twice");
    } else {
      element = argument;
    }
  }

  @Override
  public void write(AbiOutput out) {
    if (element == null) {
      throw new MissingElementException("Missing secret input");
    } else {
      element.write(out);
    }
  }
}
