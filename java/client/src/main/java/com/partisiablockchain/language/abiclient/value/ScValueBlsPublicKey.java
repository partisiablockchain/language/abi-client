package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.language.abimodel.model.TypeSpec;

/**
 * A BLS (Boneh-Lynn-Shacham) is a different type of {@link ScValuePublicKey PublicKey}, that allows
 * for aggregation.
 *
 * <p>It is used to authenticate aggregated BLS Signatures, by aggregating the public keys that was
 * used for signing, into a 'single' key. A BLS public key consists of a {@value
 * BLS_PUBLIC_KEY_LENGTH} byte array.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValueBlsPublicKey(
    @JsonSerialize(using = ScValueAddress.HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** A BLS public key is 96 bytes long. */
  public static final int BLS_PUBLIC_KEY_LENGTH = 96;

  /**
   * Create a new BLS public key. The supplied value must be {@value BLS_PUBLIC_KEY_LENGTH} bytes
   * long.
   *
   * @param bytes The byte value of the BLS public key.
   */
  public ScValueBlsPublicKey {
    if (bytes.length != BLS_PUBLIC_KEY_LENGTH) {
      throw new ScReadException(
          "BlsPublicKey expects exactly %d bytes, but found %d",
          BLS_PUBLIC_KEY_LENGTH, bytes.length);
    }
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.BlsPublicKey;
  }

  @Override
  public ScValueBlsPublicKey blsPublicKeyValue() {
    return this;
  }

  @Override
  public BlsPublicKey asBlsPublicKey() {
    return new BlsPublicKey(bytes);
  }
}
