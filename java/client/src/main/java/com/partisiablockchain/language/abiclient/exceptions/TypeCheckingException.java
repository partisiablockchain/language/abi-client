package com.partisiablockchain.language.abiclient.exceptions;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.Serial;

/**
 * Runtime exception of type checking errors - thrown when the rpc/state encounters a type mismatch
 * between the provided type in the builder and the expected type specified by the abi model.
 */
public final class TypeCheckingException extends RuntimeException {

  @Serial private static final long serialVersionUID = 4L;

  /**
   * Constructor for runtime exception of type checking error using {@link String#format} as
   * formatting.
   *
   * @param message error message
   * @param args arguments for formatting
   */
  @SuppressWarnings("AnnotateFormatMethod")
  public TypeCheckingException(String message, Object... args) {
    super(String.format(message, args));
  }
}
