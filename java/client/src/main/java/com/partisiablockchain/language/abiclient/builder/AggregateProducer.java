package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.TypeSpec;

/** Interface for producers handling aggregate data structures. */
public interface AggregateProducer extends Producer {
  /**
   * Returns the TypeSpec for the current element.
   *
   * @return the TypeSpec for the current element
   */
  TypeSpec getTypeSpecForElement();

  /**
   * Returns the field name for the current element.
   *
   * @return the field name for the current element
   */
  String getFieldName();

  /**
   * Adds the argument to the list of arguments.
   *
   * @param argument the argument to add to the list of arguments
   */
  void addElement(Producer argument);
}
