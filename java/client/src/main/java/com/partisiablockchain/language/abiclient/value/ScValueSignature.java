package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.stream.SafeDataInputStream;

/**
 * A signature is used to authenticate the sender of a transaction on the blockchain.
 *
 * <p>It consists of a {@value SIGNATURE_LENGTH} long byte array.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValueSignature(
    @JsonSerialize(using = ScValueAddress.HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** A signature is 65 bytes long. */
  public static final int SIGNATURE_LENGTH = 65;

  /**
   * Create a new signature. The supplied value must be {@value SIGNATURE_LENGTH} bytes long.
   *
   * @param bytes The byte value of the signature.
   */
  public ScValueSignature {
    if (bytes.length != SIGNATURE_LENGTH) {
      throw new ScReadException(
          "Signature expects exactly %d bytes, but found %d", SIGNATURE_LENGTH, bytes.length);
    }
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Signature;
  }

  @Override
  public ScValueSignature signatureValue() {
    return this;
  }

  @Override
  public Signature asSignature() {
    return SafeDataInputStream.readFully(bytes, Signature::read);
  }
}
