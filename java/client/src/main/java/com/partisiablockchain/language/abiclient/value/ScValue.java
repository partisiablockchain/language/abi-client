package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.math.BigInteger;

/** Base interface for all sc value representations. */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@type")
@JsonSubTypes({
  @JsonSubTypes.Type(value = ScValueU8.class, name = "u8"),
  @JsonSubTypes.Type(value = ScValueU16.class, name = "u16"),
  @JsonSubTypes.Type(value = ScValueU32.class, name = "u32"),
  @JsonSubTypes.Type(value = ScValueU64.class, name = "u64"),
  @JsonSubTypes.Type(value = ScValueU128.class, name = "u128"),
  @JsonSubTypes.Type(value = ScValueU256.class, name = "u256"),
  @JsonSubTypes.Type(value = ScValueI8.class, name = "i8"),
  @JsonSubTypes.Type(value = ScValueI16.class, name = "i16"),
  @JsonSubTypes.Type(value = ScValueI32.class, name = "i32"),
  @JsonSubTypes.Type(value = ScValueI64.class, name = "i64"),
  @JsonSubTypes.Type(value = ScValueI128.class, name = "i128"),
  @JsonSubTypes.Type(value = ScValueBool.class, name = "bool"),
  @JsonSubTypes.Type(value = ScValueString.class, name = "string"),
  @JsonSubTypes.Type(value = ScValueAddress.class, name = "address"),
  @JsonSubTypes.Type(value = ScValueHash.class, name = "hash"),
  @JsonSubTypes.Type(value = ScValuePublicKey.class, name = "publicKey"),
  @JsonSubTypes.Type(value = ScValueSignature.class, name = "signature"),
  @JsonSubTypes.Type(value = ScValueBlsPublicKey.class, name = "blsPublicKey"),
  @JsonSubTypes.Type(value = ScValueBlsSignature.class, name = "blsSignature"),
  @JsonSubTypes.Type(value = ScValueVector.class, name = "vec"),
  @JsonSubTypes.Type(value = ScValueSet.class, name = "set"),
  @JsonSubTypes.Type(value = ScValueMap.class, name = "map"),
  @JsonSubTypes.Type(value = ScValueOption.class, name = "option"),
  @JsonSubTypes.Type(value = ScValueArray.class, name = "array"),
  @JsonSubTypes.Type(value = ScValueAvlTreeMap.class, name = "avlTreeMap"),
  @JsonSubTypes.Type(value = ScValueStruct.class, name = "struct"),
  @JsonSubTypes.Type(value = ScValueEnum.class, name = "enum"),
})
public interface ScValue {

  /**
   * Returns the u8 value as a byte - throws error if the type is not a u8.
   *
   * @return byte value of u8.
   */
  default byte u8Value() {
    throw new ScReadException("Cannot read u8 for type: %s", getType());
  }

  /**
   * Returns the i8 value as a byte - throws error if the type is not an i8.
   *
   * @return byte value of i8.
   */
  default byte i8Value() {
    throw new ScReadException("Cannot read i8 for type: %s", getType());
  }

  /**
   * Returns the u16 value as a short - throws error if the type is not a u16.
   *
   * @return short value of u16.
   */
  default short u16Value() {
    throw new ScReadException("Cannot read u16 for type: %s", getType());
  }

  /**
   * Returns the u8 value as a short - throws error if the type is not an i16.
   *
   * @return short value of i16.
   */
  default short i16Value() {
    throw new ScReadException("Cannot read i16 for type: %s", getType());
  }

  /**
   * Returns the u32 value as an int - throws error if the type is not a u32.
   *
   * @return int value of u32.
   */
  default int u32Value() {
    throw new ScReadException("Cannot read u32 for type: %s", getType());
  }

  /**
   * Returns the i32 value as an int - throws error if the type is not an i32.
   *
   * @return int value of i32.
   */
  default int i32Value() {
    throw new ScReadException("Cannot read i32 for type: %s", getType());
  }

  /**
   * Returns the u64 value as a long - throws error if the type is not a u64.
   *
   * @return long value of u64.
   */
  default long u64Value() {
    throw new ScReadException("Cannot read u64 for type: %s", getType());
  }

  /**
   * Returns the i64 value as a long - throws error if the type is not an i64.
   *
   * @return long value of i64.
   */
  default long i64Value() {
    throw new ScReadException("Cannot read i64 for type: %s", getType());
  }

  /**
   * Returns the u128 value as a BigInteger - throws error if the type is not a u128.
   *
   * @return BigInteger value of u128.
   */
  default BigInteger u128Value() {
    throw new ScReadException("Cannot read u128 for type: %s", getType());
  }

  /**
   * Returns the u256 value as a BigInteger - throws error if the type is not a u256.
   *
   * @return BigInteger value of u256.
   */
  default BigInteger u256Value() {
    throw new ScReadException("Cannot read u256 for type: %s", getType());
  }

  /**
   * Returns the i128 value as a BigInteger - throws error if the type is not an i128.
   *
   * @return BigInteger value of i128.
   */
  default BigInteger i128Value() {
    throw new ScReadException("Cannot read i128 for type: %s", getType());
  }

  /**
   * Returns the int value for a given type - throws error if type is not compatible with integer
   * conversion.
   *
   * @return int value of type.
   */
  default int asInt() {
    throw new ScReadException("Cannot read integer for type: %s", getType());
  }

  /**
   * Returns the long value for a given type - throws error if type is not compatible with long
   * conversion.
   *
   * @return long value of type.
   */
  default long asLong() {
    throw new ScReadException("Cannot read long for type: %s", getType());
  }

  /**
   * Returns the BigInteger value for a given type - throws error if the type is not a number type.
   *
   * @return BigInteger value of type
   */
  default BigInteger asBigInteger() {
    throw new ScReadException("Cannot read BigInteger for type: %s", getType());
  }

  /**
   * Returns the value of a String - throws error if type is not a String.
   *
   * @return String value.
   */
  default String stringValue() {
    throw new ScReadException("Cannot read String for type: %s", getType());
  }

  /**
   * Returns the value of an Address as a byte array - throws error if type is not an Address.
   *
   * @return Address value as a byte array.
   */
  default ScValueAddress addressValue() {
    throw new ScReadException("Cannot read Address for type: %s", getType());
  }

  /**
   * Returns the value of a hash as a byte array - throws error if type is not a hash.
   *
   * @return Hash value as a byte array.
   */
  default ScValueHash hashValue() {
    throw new ScReadException("Cannot read Hash for type: %s", getType());
  }

  /**
   * Returns the value of a public key as a byte array - throws error if type is not a public key.
   *
   * @return Public key value as a byte array.
   */
  default ScValuePublicKey publicKeyValue() {
    throw new ScReadException("Cannot read PublicKey for type: %s", getType());
  }

  /**
   * Returns the value of a signature as a byte array - throws error if type is not a signature.
   *
   * @return Signature value as a byte array.
   */
  default ScValueSignature signatureValue() {
    throw new ScReadException("Cannot read Signature for type: %s", getType());
  }

  /**
   * Returns the value of a BLS public key as a byte array - throws error if type is not a BLS
   * public key.
   *
   * @return BLS public key value as a byte array.
   */
  default ScValueBlsPublicKey blsPublicKeyValue() {
    throw new ScReadException("Cannot read BlsPublicKey for type: %s", getType());
  }

  /**
   * Returns the value of a BLS signature as a byte array - throws error if type is not a BLS
   * signature.
   *
   * @return BLS signature value as a byte array.
   */
  default ScValueBlsSignature blsSignatureValue() {
    throw new ScReadException("Cannot read BlsSignature for type: %s", getType());
  }

  /**
   * Returns the value of a bool - throws error if type is not a bool.
   *
   * @return bool value.
   */
  default boolean boolValue() {
    throw new ScReadException("Cannot read bool for type: %s", getType());
  }

  /**
   * Returns the value of a Vec - throws error if type is not a Vec.
   *
   * @return StateValueVector representation of a vector.
   */
  default ScValueVector vecValue() {
    throw new ScReadException("Cannot read Vector for type: %s", getType());
  }

  /**
   * Returns the value of a Set - throws error if type is not a Set.
   *
   * @return StateValueSet representation of a set.
   */
  default ScValueSet setValue() {
    throw new ScReadException("Cannot read Set for type: %s", getType());
  }

  /**
   * Returns the value of a Map - throws error if type is not a Map.
   *
   * @return StateValueMap representation of a map.
   */
  default ScValueMap mapValue() {
    throw new ScReadException("Cannot read Map for type: %s", getType());
  }

  /**
   * Returns the value of an OptionType - throws error if type is not a OptionType.
   *
   * @return OptionType value.
   */
  default ScValueOption optionValue() {
    throw new ScReadException("Cannot read Option for type: %s", getType());
  }

  /**
   * Returns the value of a Struct - throws error if type is not a Struct.
   *
   * @return Struct value.
   */
  default ScValueStruct structValue() {
    throw new ScReadException("Cannot read Struct for type: %s", getType());
  }

  /**
   * Returns the value of an Enum - throws error if type is not an Enum.
   *
   * @return Enum value.
   */
  default ScValueEnum enumValue() {
    throw new ScReadException("Cannot read Enum for type: %s", getType());
  }

  /**
   * Returns a byte array value.
   *
   * <p>This value can be created values with type:
   *
   * <ul>
   *   <li>{@link TypeSpec.TypeIndex#SizedByteArray}
   *   <li>{@link TypeSpec.TypeIndex#Vec} with subtype {@link TypeSpec.TypeIndex#u8}
   *   <li>{@link TypeSpec.TypeIndex#SizedArray} with subtype {@link TypeSpec.TypeIndex#u8}
   * </ul>
   *
   * @return value as byte array.
   * @throws ScReadException if {@link ScValue} does not represent a byte array value.
   */
  default byte[] vecU8Value() {
    throw new ScReadException("Cannot read Vec u8 for type: %s", getType());
  }

  /**
   * Returns the value of an AvlTreeMap - throws error if type is not an AvlTreeMap.
   *
   * @return AvlTreeMap value.
   */
  default ScValueAvlTreeMap avlTreeMapValue() {
    throw new ScReadException("Cannot read AvlTreeMap for type: %s", getType());
  }

  /**
   * Returns the value of an SizedArray - throws error if type is not an SizedArray.
   *
   * @return SizedArray value.
   */
  default ScValueArray arrayValue() {
    throw new ScReadException("Cannot read array for type: %s", getType());
  }

  /**
   * Returns the value of an address as a BlockchainAddress - throws error if type is not an
   * address.
   *
   * @return address value as a BlockchainAddress.
   */
  default BlockchainAddress asBlockchainAddress() {
    throw new ScReadException("Cannot read blockchain address for type: %s", getType());
  }

  /**
   * Returns the value of a signature as a Signature - throws error if type is not a signature.
   *
   * @return signature value as a Signature.
   */
  default Signature asSignature() {
    throw new ScReadException("Cannot read signature for type: %s", getType());
  }

  /**
   * Returns the value of a hash as a Hash - throws error if type is not a hash.
   *
   * @return hash value as a Hash.
   */
  default Hash asHash() {
    throw new ScReadException("Cannot read hash for type: %s", getType());
  }

  /**
   * Returns the value of a public key as a BlockchainPublicKey - throws error if type is not a
   * public key.
   *
   * @return public key value as a BlockchainPublicKey.
   */
  default BlockchainPublicKey asBlockchainPublicKey() {
    throw new ScReadException("Cannot read blockchain public key for type: %s", getType());
  }

  /**
   * Returns the value of a BLS signature as a BlsSignature - throws error if type is not a BLS
   * signature.
   *
   * @return BLS signature value as a BlsSignature.
   */
  default BlsSignature asBlsSignature() {
    throw new ScReadException("Cannot read BLS signature for type: %s", getType());
  }

  /**
   * Returns the value of a BLS public key as a BlsPublicKey - throws error if type is not a BLS
   * public key.
   *
   * @return BLS public key value as a BlsPublicKey.
   */
  default BlsPublicKey asBlsPublicKey() {
    throw new ScReadException("Cannot read BLS public key for type: %s", getType());
  }

  /**
   * Returns the TypeIndex for the given value.
   *
   * @return typeIndex for the given value.
   */
  @JsonIgnore
  TypeSpec.TypeIndex getType();
}
