package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;

/** Builder for generating an option type, which supports type checking of the given element. */
final class OptionProducer implements AggregateProducer {

  private final OptionTypeSpec optionType;
  private Producer element;
  private final String errorPath;

  /**
   * OptionBuilder object, capable of storing whether the option type is Some or None, and the
   * corresponding argument.
   *
   * @param optionType the representation of the OptionTypeSpec from the abi model
   * @param errorPath error path
   */
  public OptionProducer(OptionTypeSpec optionType, String errorPath) {
    this.optionType = optionType;
    this.errorPath = errorPath;
  }

  @Override
  public void write(AbiOutput out) {
    if (element != null) {
      // Only single element in list of elements possible for OptionType
      out.writeU8(1);
      element.write(out);
    } else {
      out.writeU8(0);
    }
  }

  @Override
  public void addElement(Producer element) {
    if (this.element != null) {
      throw new IllegalArgumentFormat("In %s, Cannot set option value twice.", errorPath);
    }
    this.element = element;
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (optionType != null) {
      return optionType.valueType();
    }
    return null;
  }

  @Override
  public String getFieldName() {
    return "";
  }
}
