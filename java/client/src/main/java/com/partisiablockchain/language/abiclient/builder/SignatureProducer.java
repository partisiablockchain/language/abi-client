package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.HexFormat;

/** Representation object of a signature type, which is yet to be serialized. */
final class SignatureProducer implements Producer {

  private final byte[] signature;

  /**
   * A signature object representation, which is yet to be serialized. Performs explicit test, to
   * ensure that the length of the signature is {@value ScValueSignature#SIGNATURE_LENGTH} bytes.
   *
   * @param signature the signature to be serialized.
   * @param errorPath error path
   */
  public SignatureProducer(byte[] signature, String errorPath) {
    if (signature.length != ScValueSignature.SIGNATURE_LENGTH) {
      throw new IllegalArgumentFormat(
          "In %s, Signature must have length %d bytes, got length = %d, value = %s",
          errorPath,
          ScValueSignature.SIGNATURE_LENGTH,
          signature.length,
          HexFormat.of().formatHex(signature));
    }
    this.signature = signature;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeBytes(signature);
  }
}
