package com.partisiablockchain.language.abiclient.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.AbstractReader;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.secata.stream.BigEndianByteInput;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Reader that can deserialize the byte representation of executed transactions and events from the
 * partisia blockchain.
 */
public final class TransactionReader extends AbstractReader {

  private static final String ABI_FILE = "/transaction_and_event_deserialization.abi";

  private static final ContractAbi CONTRACT_ABI =
      (ContractAbi)
          new AbiParser(readFile(ABI_FILE), AbiParser.Strictness.LENIENT)
              .parseAbi()
              .chainComponent();
  private final ContractAbi contract;

  /**
   * Construct a new {@link TransactionReader} instance.
   *
   * @param in the input to read from
   * @param contract the contract to read the state of
   */
  public TransactionReader(BigEndianByteInput in, ContractAbi contract) {
    super(new AbiByteInput(in), contract.namedTypes());
    this.contract = contract;
  }

  /**
   * Construct a new {@link TransactionReader} instance from a byte array.
   *
   * @param bytes bytes to construct the reader from
   * @param contract the contract to read the state of
   */
  public TransactionReader(byte[] bytes, ContractAbi contract) {
    this(new BigEndianByteInput(new ByteArrayInputStream(bytes)), contract);
  }

  @Override
  protected ScValueMap readMap(MapTypeSpec map) {
    throw new TransactionReadException("Type %s is not supported in rpc", map.typeIndex());
  }

  @Override
  protected ScValueSet readSet(SetTypeSpec set) {
    throw new TransactionReadException("Type %s is not supported in rpc", set.typeIndex());
  }

  @Override
  protected ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec type) {
    throw new TransactionReadException(
        "Type %s is not supported in rpc".formatted(type.typeIndex()));
  }

  private void checkAllBytesRead() {
    byte[] remaining = in.readRemaining();
    if (remaining.length != 0) {
      throw new TransactionReadException(
          "The input wasn't fully read, " + remaining.length + " remaining bytes.");
    }
  }

  /**
   * Deserialize the payload using the abi format defined in
   * transaction_and_event_deserialization.abi as an ExecutableEvent. The source for the ABI file
   * can be found at: <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/governance/-/tree/main/transaction-and-event-deserialization">transaction-and-event-deserialization
   * contract</a>
   *
   * @param payload the payload to deserialize.
   * @return The ExecutableEvent represented by an ScValue.
   */
  public static ScValueStruct deserializeEventPayload(byte[] payload) {
    TransactionReader reader = new TransactionReader(payload, CONTRACT_ABI);

    StructTypeSpec executableEvent =
        (StructTypeSpec) reader.contract.getNamedType("ExecutableEvent");
    ScValueStruct res = reader.readStruct(executableEvent);
    reader.checkAllBytesRead();
    return res;
  }

  /**
   * Deserialize the payload using the abi format defined in
   * transaction_and_event_deserialization.abi as a SignedTransaction. The source for the ABI file
   * can be found at: <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/governance/-/tree/main/transaction-and-event-deserialization">transaction-and-event-deserialization
   * contract</a>
   *
   * @param payload the payload to deserialize.
   * @return The SignedTransaction represented by an ScValue.
   */
  public static ScValueStruct deserializeTransactionPayload(byte[] payload) {
    TransactionReader reader = new TransactionReader(payload, CONTRACT_ABI);

    StructTypeSpec signedTransaction =
        (StructTypeSpec) reader.contract.getNamedType("SignedTransaction");
    ScValueStruct res = reader.readStruct(signedTransaction);
    reader.checkAllBytesRead();
    return res;
  }

  static byte[] readFile(String fileName) {
    try (InputStream in = TransactionReader.class.getResourceAsStream(fileName)) {
      return in.readAllBytes();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Get the contract abi of the transaction_and_event_deserialization contract.
   *
   * @return the contract abi
   */
  public static ContractAbi getContractAbi() {
    return CONTRACT_ABI;
  }
}
