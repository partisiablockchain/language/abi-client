package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.secata.stream.SafeDataInputStream;

/**
 * A hash is the result of a hashing process, yielding a unique identifier for the hashed artifact.
 *
 * <p>This identifier always consists of {@value HASH_LENGTH} bytes.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ScValueHash(
    @JsonSerialize(using = ScValueAddress.HexSerializer.class) @JsonProperty("value") byte[] bytes)
    implements ScValue {

  /** A hash is 32 bytes long. */
  public static final int HASH_LENGTH = 32;

  /**
   * Create a new hash. The supplied value must be {@value HASH_LENGTH} bytes long.
   *
   * @param bytes The byte value of the hash.
   */
  public ScValueHash {
    if (bytes.length != HASH_LENGTH) {
      throw new ScReadException(
          "Hash expects exactly %d bytes, but found %d", HASH_LENGTH, bytes.length);
    }
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Hash;
  }

  @Override
  public ScValueHash hashValue() {
    return this;
  }

  @Override
  public Hash asHash() {
    return SafeDataInputStream.readFully(bytes, Hash::read);
  }
}
