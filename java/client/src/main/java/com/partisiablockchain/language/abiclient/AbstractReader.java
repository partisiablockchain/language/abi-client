package com.partisiablockchain.language.abiclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueArray;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueBlsPublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abiclient.value.ScValueBool;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueHash;
import com.partisiablockchain.language.abiclient.value.ScValueI128;
import com.partisiablockchain.language.abiclient.value.ScValueI16;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueI8;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValuePublicKey;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueSignature;
import com.partisiablockchain.language.abiclient.value.ScValueString;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueU128;
import com.partisiablockchain.language.abiclient.value.ScValueU16;
import com.partisiablockchain.language.abiclient.value.ScValueU256;
import com.partisiablockchain.language.abiclient.value.ScValueU32;
import com.partisiablockchain.language.abiclient.value.ScValueU64;
import com.partisiablockchain.language.abiclient.value.ScValueU8;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abistreams.AbiInput;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/** Abstract class for deserializing a payload into a value of the given type. */
public abstract class AbstractReader {

  /** The input to read from. */
  protected final AbiInput in;

  /** The contract abi for specifying named types. */
  protected final List<NamedTypeSpec> namedTypes;

  /**
   * Constructor a new {@link AbstractReader} instance.
   *
   * @param in the input to read from
   * @param namedTypes the contract abi for specifying named types
   */
  protected AbstractReader(AbiInput in, List<NamedTypeSpec> namedTypes) {
    this.in = in;
    this.namedTypes = namedTypes;
  }

  /**
   * Read a generic TypeSpec from the input.
   *
   * @param type the TypeSpec to read.
   * @return the resulting {@link ScValue}
   */
  public ScValue readGeneric(TypeSpec type) {
    if (type instanceof SimpleTypeSpec simple) {
      return readSimpleType(simple.typeIndex());
    } else if (type instanceof VecTypeSpec vec) {
      return readVec(vec);
    } else if (type instanceof SetTypeSpec set) {
      return readSet(set);
    } else if (type instanceof MapTypeSpec map) {
      return readMap(map);
    } else if (type instanceof OptionTypeSpec option) {
      return readOptional(option);
    } else if (type instanceof NamedTypeRef namedRef) {
      return readNamedType(namedRef);
    } else if (type instanceof SizedArrayTypeSpec array) {
      return readSizedArray(array);
    } else if (type instanceof AvlTreeMapTypeSpec avlTreeMap) {
      return readAvlTreeMap(avlTreeMap);
    } else {
      throw new IllegalArgumentException("Cannot read instance of: " + type);
    }
  }

  /**
   * Read a simple type from the input.
   *
   * @param type the typeIndex to be read.
   * @return the resulting {@link ScValue}
   */
  public ScValue readSimpleType(TypeSpec.TypeIndex type) {
    return switch (type) {
      case i8 -> new ScValueI8(in.readI8());
      case u8 -> new ScValueU8(in.readU8());
      case i16 -> new ScValueI16(in.readI16());
      case u16 -> new ScValueU16(in.readU16());
      case i32 -> new ScValueI32(in.readI32());
      case u32 -> new ScValueU32(in.readU32());
      case i64 -> new ScValueI64(in.readI64());
      case u64 -> new ScValueU64(in.readU64());
      case i128 -> new ScValueI128(in.readSignedBigInteger(16));
      case u128 -> new ScValueU128(in.readUnsignedBigInteger(16));
      case u256 -> new ScValueU256(in.readUnsignedBigInteger(32));
      case bool -> new ScValueBool(in.readBoolean());
      case Address -> new ScValueAddress(in.readBytes(ScValueAddress.ADDRESS_LENGTH));
      case Hash -> new ScValueHash(in.readBytes(ScValueHash.HASH_LENGTH));
      case PublicKey -> new ScValuePublicKey(in.readBytes(ScValuePublicKey.PUBLIC_KEY_LENGTH));
      case Signature -> new ScValueSignature(in.readBytes(ScValueSignature.SIGNATURE_LENGTH));
      case BlsPublicKey ->
          new ScValueBlsPublicKey(in.readBytes(ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH));
      case BlsSignature ->
          new ScValueBlsSignature(in.readBytes(ScValueBlsSignature.BLS_SIGNATURE_LENGTH));
      case String -> new ScValueString(in.readString());
      default -> throw new IllegalArgumentException("Unexpected typeIndex: " + type);
    };
  }

  private ScValueOption readOptional(OptionTypeSpec option) {
    final boolean hasElement = in.readU8() != 0;
    if (hasElement) {
      return new ScValueOption(readGeneric(option.valueType()));
    } else {
      return new ScValueOption(null);
    }
  }

  private ScValueVector readVec(VecTypeSpec typeSpec) {
    final int length = in.readI32();
    return new ScValueVector(readElements(typeSpec.valueType(), length));
  }

  private ScValueArray readSizedArray(SizedArrayTypeSpec typeSpec) {
    return new ScValueArray(readElements(typeSpec.valueType(), typeSpec.length()));
  }

  private List<ScValue> readElements(TypeSpec typeSpec, int length) {
    final List<ScValue> elements = new ArrayList<>();
    for (int i = 0; i < length; i++) {
      elements.add(readGeneric(typeSpec));
    }
    return List.copyOf(elements);
  }

  private ScValue readNamedType(NamedTypeRef typeRef) {
    NamedTypeSpec typeSpec = namedTypes.get(typeRef.index());
    if (typeSpec instanceof StructTypeSpec structTypeSpec) {
      return readStruct(structTypeSpec);
    } else {
      return readEnum((EnumTypeSpec) typeSpec);
    }
  }

  /**
   * Read a struct type from the input.
   *
   * @param struct the StructTypeSpec to be read.
   * @return the resulting {@link ScValueStruct}
   */
  protected ScValueStruct readStruct(StructTypeSpec struct) {
    final LinkedHashMap<String, ScValue> fieldsMap = new LinkedHashMap<>();

    for (FieldAbi field : struct.fields()) {
      ScValue value = readGeneric(field.type());
      fieldsMap.put(field.name(), value);
    }
    return new ScValueStruct(struct.name(), fieldsMap);
  }

  private ScValueEnum readEnum(EnumTypeSpec enumTypeSpec) {
    byte discriminant = in.readU8();
    EnumVariant variant = enumTypeSpec.variant(discriminant);
    if (variant == null) {
      throw new IllegalArgumentException(
          "Undefined EnumVariant " + discriminant + " used in " + enumTypeSpec.name());
    } else {
      StructTypeSpec typeSpec = (StructTypeSpec) namedTypes.get(variant.def().index());
      return new ScValueEnum(typeSpec.name(), readStruct(typeSpec));
    }
  }

  /**
   * Read a map type from the input.
   *
   * @param map the MapTypeSpec to be read.
   * @return the resulting {@link ScValueMap}
   */
  protected abstract ScValueMap readMap(MapTypeSpec map);

  /**
   * Read a set type from the input.
   *
   * @param set the SetTypeSpec to be read.
   * @return the resulting {@link ScValueSet}
   */
  protected abstract ScValueSet readSet(SetTypeSpec set);

  /**
   * Read an avl tree map type from the input.
   *
   * @param map the AvlTreeMapTypeSpec to be read.
   * @return the resulting {@link ScValueMap}
   */
  protected abstract ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec map);
}
