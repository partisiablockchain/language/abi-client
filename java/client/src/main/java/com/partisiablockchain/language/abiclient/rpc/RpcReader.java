package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.AbstractReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAvlTreeMap;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abimodel.parser.Configuration;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.secata.stream.BigEndianByteInput;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;

/** Reader capable of reading rpc payloads sent to a contract. */
public final class RpcReader extends AbstractReader {

  private final ContractAbi contract;
  private final TransactionKind kind;
  private final Configuration configuration;
  private final byte[] bytes;

  /**
   * Constructor a new {@link AbstractReader} instance.
   *
   * @param bytes the input to read from
   * @param contract the contract abi for specifying named types
   * @param kind kind of the invocation
   * @param configuration configuration of how to read the rpc
   */
  private RpcReader(
      byte[] bytes, ContractAbi contract, TransactionKind kind, Configuration configuration) {
    super(
        new AbiByteInput(new BigEndianByteInput(new ByteArrayInputStream(bytes))),
        contract.namedTypes());
    this.bytes = bytes;
    this.contract = contract;
    this.kind = kind;
    this.configuration = configuration;
  }

  /**
   * Construct a new {@link RpcReader} instance from a byte array.
   *
   * @param bytes bytes to construct the reader from
   * @param fileAbi the contract to read the rpc of
   * @param kind the kind of the action to read
   * @return the constructed RpcReader
   */
  public static RpcReader create(byte[] bytes, FileAbi fileAbi, TransactionKind kind) {
    return new RpcReader(bytes, (ContractAbi) fileAbi.chainComponent(), kind, fileAbi.format());
  }

  /**
   * Read the rpc of the reader.
   *
   * @return the resulting {@link RpcValue}
   */
  public RpcValue readRpc() {
    if (kind == TransactionKind.Action && bytes.length == 0) {
      BinderInvocation empty = contract.getBinderInvocation(null, TransactionKind.EmptyAction);
      if (empty != null) {
        return new RpcValue(empty, null, List.of(), List.of());
      }
    }

    if (configuration.chainComponentFormat() == Configuration.ChainComponentFormat.ONLY_CONTRACT
        && contract.isZk()) {
      byte invocationShortname = bytes[0];
      if (RealBinderInvocationReader.isAbiUnfriendly(invocationShortname)) {
        return new RealBinderInvocationReader(bytes, contract).readAbiUnfriendlyBinderInvocation();
      }
    }

    Byte binderShortname = null;
    BinderInvocation binderInvocation = contract.getBinderInvocation(null, kind);
    if (binderInvocation == null) {
      binderShortname = in.readU8();
      binderInvocation = contract.getBinderInvocation(binderShortname, kind);
    }

    if (binderInvocation == null) {
      throw new RpcReadException(
          "No binder invocation with kind '%s' and shortname '%d'", kind, binderShortname);
    }

    List<ScValue> binderArguments =
        binderInvocation.arguments().stream().map(arg -> readGeneric(arg.type())).toList();

    if (binderInvocation.contractCallKindId() == null) {
      return new RpcValue(binderInvocation, null, binderArguments, List.of());
    }

    ContractInvocationKind contractKind =
        ContractInvocationKind.getKind(
            contract.invocationKinds(), binderInvocation.contractCallKindId());
    ContractInvocation fn;
    if (contractKind.hasShortname()) {
      var contractShortname = readShortname();
      fn = contract.getFunction(contractShortname, contractKind);
      if (fn == null) {
        throw new RpcReadException(
            "No contract invocation with kind '%s' and shortname '0x%s'",
            contractKind.name().toLowerCase(Locale.ENGLISH),
            HexFormat.of().formatHex(contractShortname));
      }
    } else {
      fn =
          contract.contractInvocations().stream()
              .filter(f -> f.kind().kindId() == contractKind.kindId())
              .findAny()
              .orElse(null);
      if (fn == null) {
        throw new RpcReadException(
            "No contract invocation with kind '%s'",
            contractKind.name().toLowerCase(Locale.ENGLISH));
      }
    }

    List<ScValue> contractArguments =
        fn.arguments().stream().map(arg -> readGeneric(arg.type())).toList();

    return new RpcValue(binderInvocation, fn, binderArguments, contractArguments);
  }

  /**
   * Read a shortname from the bytestream.
   *
   * @return the read shortname.
   */
  public byte[] readShortname() {
    if (configuration.shortnameLength() == null) {
      return parseLeb128();
    } else {
      return in.readBytes(configuration.shortnameLength());
    }
  }

  private byte[] parseLeb128() {
    byte[] buffer = new byte[5];
    for (int i = 0; i < buffer.length; i++) {
      var currentByte = in.readU8();
      buffer[i] = currentByte;

      if ((currentByte & 0x80) == 0) {
        return Arrays.copyOf(buffer, i + 1);
      }
    }

    throw new RpcReadException(
        "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5"
            + " bytes)");
  }

  @Override
  protected ScValueMap readMap(MapTypeSpec type) {
    throw new RpcReadException("Type %s is not supported in rpc", type.typeIndex());
  }

  @Override
  protected ScValueSet readSet(SetTypeSpec type) {
    throw new RpcReadException("Type %s is not supported in rpc", type.typeIndex());
  }

  @Override
  protected ScValueAvlTreeMap readAvlTreeMap(AvlTreeMapTypeSpec type) {
    throw new RpcReadException("Type %s is not supported in rpc", type.typeIndex());
  }
}
