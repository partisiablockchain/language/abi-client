package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Producer for generating rpc/state for vec types, which supports type checking of elements added
 * to the vector.
 */
final class VecProducer implements AggregateProducer {

  private final VecTypeSpec vec;
  private final List<Producer> elements = new ArrayList<>();

  /**
   * VecProducer object, capable of generating the rpc/state for a Vector type with a specified
   * value type.
   *
   * @param vec the representation of the VecTypeSpec from the abi model
   */
  public VecProducer(VecTypeSpec vec) {
    this.vec = vec;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeI32(elements.size());
    for (Producer element : elements) {
      element.write(out);
    }
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (vec != null) {
      return vec.valueType();
    }
    return null;
  }

  @Override
  public String getFieldName() {
    return "/" + elements.size();
  }

  @Override
  public void addElement(Producer argument) {
    this.elements.add(argument);
  }
}
