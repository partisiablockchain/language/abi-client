package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.value.ScValueHash;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.HexFormat;

/** Representation object of a hash type, which is yet to be serialized. */
final class HashProducer implements Producer {

  private final byte[] hash;

  /**
   * A hash object representation, which is yet to be serialized. Performs explicit test, to ensure
   * that the length of the hash is {@value ScValueHash#HASH_LENGTH} bytes.
   *
   * @param hash the hash to be serialized.
   * @param errorPath error path
   */
  public HashProducer(byte[] hash, String errorPath) {
    if (hash.length != ScValueHash.HASH_LENGTH) {
      throw new IllegalArgumentFormat(
          "In %s, Hash must have length %d bytes, got length = %d, value = %s",
          errorPath, ScValueHash.HASH_LENGTH, hash.length, HexFormat.of().formatHex(hash));
    }
    this.hash = hash;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeBytes(hash);
  }
}
