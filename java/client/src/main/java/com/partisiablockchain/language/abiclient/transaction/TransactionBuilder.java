package com.partisiablockchain.language.abiclient.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.builder.AggregateProducer;
import com.partisiablockchain.language.abiclient.builder.StructProducer;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abistreams.AbiByteOutput;

/**
 * Builder for Signed Transactions and Executable Events. Used to generate serialized bytes
 * representing the built Signed Transaction or Executable Event.
 */
public final class TransactionBuilder extends AbstractBuilder {

  private TransactionBuilder(ContractAbi contractAbi, AggregateProducer aggregateProducer) {
    super(contractAbi.namedTypes(), "", aggregateProducer);
  }

  /**
   * Creates a builder capable of building Signed transactions.
   *
   * @return Signed Transaction builder
   */
  public static TransactionBuilder createTransactionBuilder() {
    String abiFile = "/transaction_and_event_deserialization.abi";

    ContractAbi transactionAbi =
        (ContractAbi)
            new AbiParser(TransactionReader.readFile(abiFile), AbiParser.Strictness.LENIENT)
                .parseAbi()
                .chainComponent();
    StructTypeSpec signedTransaction =
        (StructTypeSpec) transactionAbi.getNamedType("SignedTransaction");
    return new TransactionBuilder(transactionAbi, new StructProducer(signedTransaction, "root"));
  }

  /**
   * Creates a builder capable of building Executable Events.
   *
   * @return Executable Event builder
   */
  public static TransactionBuilder createEventBuilder() {
    String abiFile = "/transaction_and_event_deserialization.abi";

    ContractAbi transactionAbi =
        (ContractAbi)
            new AbiParser(TransactionReader.readFile(abiFile), AbiParser.Strictness.LENIENT)
                .parseAbi()
                .chainComponent();
    StructTypeSpec signedTransaction =
        (StructTypeSpec) transactionAbi.getNamedType("ExecutableEvent");
    return new TransactionBuilder(transactionAbi, new StructProducer(signedTransaction, "root"));
  }

  /**
   * Creates and returns the bytes representing the given transaction or event. Creates bytes in big
   * endian format.
   *
   * @return the bytes of the transaction or event
   */
  public byte[] getBytes() {
    return AbiByteOutput.serializeBigEndian((out) -> getAggregateProducer().write(out));
  }
}
