package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.TypeCheckingException;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Builder for generating elements of the {@link SizedArrayType}, which supports type checking of
 * the given element.
 */
final class SizedArrayProducer implements AggregateProducer {

  private final SizedArrayTypeSpec sizedArrayType;
  private List<Producer> elements;
  private final String errorPath;

  /**
   * Constructor for {@link SizedArrayProducer}.
   *
   * @param sizedArrayType type spec from the abi model
   * @param errorPath error path
   */
  public SizedArrayProducer(SizedArrayTypeSpec sizedArrayType, String errorPath) {
    this.sizedArrayType = sizedArrayType;
    this.errorPath = Objects.requireNonNull(errorPath);
    this.elements = new ArrayList<>();
  }

  @Override
  public void write(AbiOutput out) {
    if (sizedArrayType != null && elements.size() != sizedArrayType.length()) {
      throw new TypeCheckingException(
          "Array builder at %s: Array with size %d did not have the expected size %d",
          errorPath, elements.size(), sizedArrayType.length());
    }

    for (final Producer element : elements) {
      element.write(out);
    }
  }

  @Override
  public void addElement(Producer element) {
    if (sizedArrayType != null && elements.size() == sizedArrayType.length()) {
      throw new TypeCheckingException(
          "Array builder at %s%s: Array is already at max size %s, cannot add more",
          errorPath, getFieldName(), sizedArrayType.length());
    }

    this.elements.add(element);
  }

  @Override
  public TypeSpec getTypeSpecForElement() {
    if (sizedArrayType != null) {
      return sizedArrayType.valueType();
    }
    return null;
  }

  @Override
  public String getFieldName() {
    return "/" + elements.size();
  }
}
