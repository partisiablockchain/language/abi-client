package com.partisiablockchain.language.abiclient.rpc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueI32;
import com.partisiablockchain.language.abiclient.value.ScValueI64;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.BinderInvocation;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.TransactionKind;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.secata.stream.BigEndianByteInput;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Rpc reader for deserializing ABI-unfriendly invocations to a Zk contract. Used for binder
 * invocations before the introduction of the binder abi, which were not capable of having a default
 * binder invocation abi.
 */
public final class RealBinderInvocationReader {

  static final byte COMMIT_RESULT_VARIABLE = 0x00;
  static final byte ON_CHAIN_OUTPUT = 0x01;
  static final byte OPEN_MASKED_INPUT = 0x03;
  static final byte ON_COMPUTE_COMPLETE = 0x0c;
  static final byte ON_VARIABLE_INPUTTED = 0x0f;
  static final byte ADD_EXTERNAL_EVENT = 0x14;

  private final AbiInput in;
  private final ContractAbi contractAbi;

  /**
   * Create a reader for the Real binder invocations, with the given payload, the contract abi,
   * which is wrapped by the Real binder, and the rpc reader for the contract invocation.
   *
   * @param bytes the payload.
   * @param contractAbi the wrapped contracts abi.
   */
  public RealBinderInvocationReader(byte[] bytes, ContractAbi contractAbi) {
    this.in = new AbiByteInput(new BigEndianByteInput(new ByteArrayInputStream(bytes)));
    this.contractAbi = contractAbi;
  }

  /**
   * Does this old binder invocation require special deserialization.
   *
   * @param invocationShortname shortname of the invocation to test
   * @return whether the invocation requires special deserialization
   */
  public static boolean isAbiUnfriendly(byte invocationShortname) {
    return List.of(
            COMMIT_RESULT_VARIABLE,
            ON_CHAIN_OUTPUT,
            OPEN_MASKED_INPUT,
            ON_COMPUTE_COMPLETE,
            ON_VARIABLE_INPUTTED,
            ADD_EXTERNAL_EVENT)
        .contains(invocationShortname);
  }

  /**
   * Deserialize the ABI-unfriendly invocation payload into the invoked Real binder invocation.
   *
   * @return the rpc value for the invocation, containing the name and deserialized arguments.
   */
  public RpcValue readAbiUnfriendlyBinderInvocation() {
    byte invocationShortname = in.readU8();
    return switch (invocationShortname) {
      case ON_CHAIN_OUTPUT -> deserializeOnChainOutput();
      case OPEN_MASKED_INPUT -> deserializeOpenMaskedInput();
      case COMMIT_RESULT_VARIABLE -> deserializeCommitResultVariable();
      case ON_COMPUTE_COMPLETE -> deserializeOnComputeComplete();
      case ON_VARIABLE_INPUTTED -> deserializeOnVariableInputted();
      case ADD_EXTERNAL_EVENT -> deserializeAddExternalEvent();
      default ->
          throw new RuntimeException(
              "Binder invocation '0x%02X' is not an abi unfriendly invocation"
                  .formatted(invocationShortname));
    };
  }

  private RpcValue deserializeAddExternalEvent() {
    // parse id as i32
    int id = in.readI32();
    // parse rest as bytes
    ScValueVector remaining = ScValueVector.fromBytes(in.readRemaining());

    var action = contractAbi.getBinderInvocation(ADD_EXTERNAL_EVENT, TransactionKind.Action);

    return new RpcValue(action, null, List.of(new ScValueI32(id), remaining), List.of());
  }

  private RpcValue deserializeCommitResultVariable() {
    long calculationBlocktime = in.readI64();
    ScValueVector remaining = ScValueVector.fromBytes(in.readRemaining());

    BinderInvocation function =
        contractAbi.getBinderInvocation(COMMIT_RESULT_VARIABLE, TransactionKind.Action);
    return new RpcValue(
        function, null, List.of(new ScValueI64(calculationBlocktime), remaining), List.of());
  }

  private RpcValue deserializeOpenMaskedInput() {
    // parse id as i32
    int id = in.readI32();
    // parse rest as bytes
    ScValueVector remaining = ScValueVector.fromBytes(in.readRemaining());

    BinderInvocation function =
        contractAbi.getBinderInvocation(OPEN_MASKED_INPUT, TransactionKind.Action);
    return new RpcValue(function, null, List.of(new ScValueI32(id), remaining), List.of());
  }

  private RpcValue deserializeOnChainOutput() {
    // parse id as i32
    int id = in.readI32();
    // parse rest as bytes
    ScValueVector remaining = ScValueVector.fromBytes(in.readRemaining());

    var action = contractAbi.getBinderInvocation(ON_CHAIN_OUTPUT, TransactionKind.Action);

    return new RpcValue(action, null, List.of(new ScValueI32(id), remaining), List.of());
  }

  private RpcValue deserializeOnComputeComplete() {
    int idLength = in.readI32();
    List<ScValue> ids = new ArrayList<>();
    for (int i = 0; i < idLength; i++) {
      ids.add(new ScValueI32(in.readI32()));
    }
    ScValue idsVector = new ScValueVector(ids);

    var action = contractAbi.getBinderInvocation(ON_COMPUTE_COMPLETE, TransactionKind.Action);

    return new RpcValue(action, null, List.of(idsVector), List.of());
  }

  private RpcValue deserializeOnVariableInputted() {
    ScValueI32 id = new ScValueI32(in.readI32());

    var action = contractAbi.getBinderInvocation(ON_VARIABLE_INPUTTED, TransactionKind.Action);

    return new RpcValue(action, null, List.of(id), List.of());
  }
}
