package com.partisiablockchain.language.abiclient.value;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.io.IOException;
import java.util.Map;

/** Object representation of an enum variant, consisting of a name and a struct. */
public record ScValueEnum(
    @JsonIgnore String name,
    @JsonSerialize(using = EnumSerializer.class) @JsonProperty("value") ScValueStruct item)
    implements ScValue {

  @Override
  public ScValueEnum enumValue() {
    return this;
  }

  @Override
  public TypeSpec.TypeIndex getType() {
    return TypeSpec.TypeIndex.Named;
  }

  static final class EnumSerializer extends JsonSerializer<ScValueStruct> {

    @Override
    public void serialize(
        ScValueStruct scValueStruct,
        JsonGenerator jsonGenerator,
        SerializerProvider serializerProvider)
        throws IOException {
      jsonGenerator.writeStartObject();
      jsonGenerator.writeStringField("@discriminant", scValueStruct.name());
      for (Map.Entry<String, ScValue> entry : scValueStruct.fieldsMap().entrySet()) {
        jsonGenerator.writeObjectField(entry.getKey(), entry.getValue());
      }
      jsonGenerator.writeEndObject();
    }

    @Override
    public void serializeWithType(
        ScValueStruct value,
        JsonGenerator gen,
        SerializerProvider serializers,
        TypeSerializer typeSer)
        throws IOException {
      serialize(value, gen, serializers);
    }
  }
}
