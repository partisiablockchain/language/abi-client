package com.partisiablockchain.language.abiclient.state;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueAddress;
import com.partisiablockchain.language.abiclient.value.ScValueArray;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueOption;
import com.partisiablockchain.language.abiclient.value.ScValueSet;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abiclient.value.ScValueVector;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.HexFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Can pretty printing the state of a contract. Supports writing the state as a string, in a
 * Rust-like syntax.
 */
public interface RustSyntaxStatePrinter {

  /**
   * Reads the state specified by the StateStructValue and returns a String representation of the
   * read state.
   *
   * @param state the StateStructValue representing the state
   * @return string representation of read state
   */
  static String printState(ScValueStruct state) {
    int indentLevel = 0;
    StringBuilder builder = new StringBuilder();
    builder.append("#[state]\n");
    builder.append(state.name()).append(" {\n");

    final LinkedHashMap<String, ScValue> fieldMap = state.fieldsMap();
    indentLevel++;
    for (String fieldName : fieldMap.keySet()) {
      buildField(builder, fieldName, fieldMap.get(fieldName), indentLevel);
      builder.append(",\n");
    }
    builder.append("}");
    return builder.toString();
  }

  private static void buildField(
      StringBuilder builder, String fieldName, ScValue stateValue, int indentLevel) {
    buildIndent(builder, indentLevel);
    builder.append(fieldName).append(": ");
    buildStateValue(builder, stateValue, indentLevel);
  }

  private static void buildIndent(StringBuilder builder, int indentLevel) {
    builder.append("    ".repeat(Math.max(0, indentLevel)));
  }

  private static void buildStateValue(StringBuilder builder, ScValue stateValue, int indentLevel) {
    TypeSpec.TypeIndex type = stateValue.getType();
    switch (type) {
      case u8, i8, u16, i16, u32, i32, i64 -> builder.append(stateValue.asLong()).append(type);
      case u64, u128, i128 -> builder.append(stateValue.asBigInteger()).append(type);
      case Address -> buildAddress(builder, stateValue.addressValue());
      case bool -> builder.append(stateValue.boolValue());
      case String -> builder.append('"').append(stateValue.stringValue()).append("\".to_string()");
      case Vec -> buildVec(builder, stateValue.vecValue(), indentLevel);
      case Set -> buildSet(builder, stateValue.setValue(), indentLevel);
      case Map -> buildMap(builder, stateValue.mapValue(), indentLevel);
      case Option -> buildOption(builder, stateValue.optionValue(), indentLevel);
      case Named -> buildNamed(builder, stateValue, indentLevel);
      default -> buildArray(builder, stateValue.arrayValue(), indentLevel);
    }
  }

  private static void buildAddress(StringBuilder builder, ScValueAddress addressValue) {
    var hex = HexFormat.of().formatHex(addressValue.bytes());
    builder.append('"').append(hex).append('"');
  }

  private static void buildArray(StringBuilder builder, ScValueArray scValue, int indentLevel) {
    builder.append("[");
    buildArrayInternal(builder, scValue.iterator(), indentLevel);
    builder.append("]");
  }

  private static void buildNamed(StringBuilder builder, ScValue value, int indentLevel) {
    if (value instanceof ScValueStruct) {
      buildStruct(builder, value.structValue(), indentLevel);
    } else {
      buildEnum(builder, value.enumValue(), indentLevel);
    }
  }

  private static void buildEnum(StringBuilder builder, ScValueEnum enumValue, int indentLevel) {
    buildStruct(builder, enumValue.item(), indentLevel);
  }

  private static void buildStruct(StringBuilder builder, ScValueStruct struct, int indentLevel) {
    builder.append(struct.name());
    builder.append(" {");
    indentLevel++;
    for (Iterator<Map.Entry<String, ScValue>> iterator = struct.fieldsMap().entrySet().iterator();
        iterator.hasNext(); ) {
      Map.Entry<String, ScValue> entry = iterator.next();
      builder.append("\n");
      buildField(builder, entry.getKey(), entry.getValue(), indentLevel);
      if (iterator.hasNext()) {
        builder.append(",");
      }
    }
    indentLevel--;
    builder.append("\n");
    buildIndent(builder, indentLevel);
    builder.append("}");
  }

  private static void buildOption(StringBuilder builder, ScValueOption option, int indentLevel) {
    if (option.isSome()) {
      builder.append("Some(");
      buildStateValue(builder, option.innerValue(), indentLevel);
      builder.append(")");
    } else {
      builder.append("None");
    }
  }

  private static void buildMap(StringBuilder builder, ScValueMap stateMap, int indentLevel) {
    builder.append("BTreeMap::from([");
    indentLevel++;
    for (Iterator<Map.Entry<ScValue, ScValue>> iterator = stateMap.map().entrySet().iterator();
        iterator.hasNext(); ) {
      builder.append("\n");
      buildIndent(builder, indentLevel);
      builder.append("(");
      Map.Entry<ScValue, ScValue> entry = iterator.next();
      buildStateValue(builder, entry.getKey(), indentLevel);
      builder.append(", ");
      buildStateValue(builder, entry.getValue(), indentLevel);
      builder.append(")");
      if (iterator.hasNext()) {
        builder.append(",");
      }
    }
    indentLevel--;
    builder.append("\n");
    buildIndent(builder, indentLevel);
    builder.append("])");
  }

  private static void buildSet(StringBuilder builder, ScValueSet scValue, int indentLevel) {
    builder.append("BTreeSet::from([");
    buildArrayInternal(builder, scValue.iterator(), indentLevel);
    builder.append("])");
  }

  private static void buildVec(StringBuilder builder, ScValueVector scValue, int indentLevel) {
    builder.append("vec![");
    buildArrayInternal(builder, scValue.iterator(), indentLevel);
    builder.append("]");
  }

  private static void buildArrayInternal(
      StringBuilder builder, Iterator<ScValue> iterator, int indentLevel) {
    indentLevel++;
    while (iterator.hasNext()) {
      final ScValue element = iterator.next();
      builder.append("\n");
      buildIndent(builder, indentLevel);
      buildStateValue(builder, element, indentLevel);

      if (iterator.hasNext()) {
        builder.append(",");
      }
    }
    indentLevel--;
    builder.append("\n");
    buildIndent(builder, indentLevel);
  }
}
