package com.partisiablockchain.language.abiclient.builder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.exceptions.IllegalArgumentFormat;
import com.partisiablockchain.language.abiclient.value.ScValueBlsSignature;
import com.partisiablockchain.language.abistreams.AbiOutput;
import java.util.HexFormat;

/** Representation object of a bls signature type, which is yet to be serialized. */
final class BlsSignatureProducer implements Producer {

  private final byte[] blsSignature;

  /**
   * A bls signature object representation, which is yet to be serialized. Performs explicit test,
   * to ensure that the length of the bls signature is {@value
   * ScValueBlsSignature#BLS_SIGNATURE_LENGTH} bytes.
   *
   * @param blsSignature the bls signature to be serialized.
   * @param errorPath error path
   */
  public BlsSignatureProducer(byte[] blsSignature, String errorPath) {
    if (blsSignature.length != ScValueBlsSignature.BLS_SIGNATURE_LENGTH) {
      throw new IllegalArgumentFormat(
          "In %s, Bls signature must have length %d bytes, got length = %d, value = %s",
          errorPath,
          ScValueBlsSignature.BLS_SIGNATURE_LENGTH,
          blsSignature.length,
          HexFormat.of().formatHex(blsSignature));
    }
    this.blsSignature = blsSignature;
  }

  @Override
  public void write(AbiOutput out) {
    out.writeBytes(blsSignature);
  }
}
