package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SenderAuthenticationKeyPair;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Testing {@link BlockchainClientForDeploymentImpl}. */
public final class BlockchainClientForDeploymentImplTest {
  /**
   * The deployment transaction client impl sign method calls the underlying transaction client's
   * sign method.
   */
  @Test
  void sign() throws ApiException {
    Transaction transaction =
        Transaction.create(
            BlockchainAddress.fromString("000000000000000000000000000000000000000000"),
            new byte[0]);
    SignedTransaction signedTransaction =
        SignedTransaction.create(
            SenderAuthenticationKeyPair.fromString("aa"), 1, 2, 3, "4", transaction);
    BlockchainTransactionClient mock = Mockito.mock(BlockchainTransactionClient.class);
    Mockito.when(mock.sign(Mockito.any(), Mockito.anyLong())).thenReturn(signedTransaction);

    BlockchainClientForDeploymentImpl deploymentTransactionClient =
        new BlockchainClientForDeploymentImpl(mock, null);
    assertThat(deploymentTransactionClient.sign(transaction, 3)).isEqualTo(signedTransaction);
    Mockito.verify(mock, Mockito.times(1)).sign(transaction, 3);
  }

  /**
   * The deployment transaction client impl send method calls the underlying transaction client's
   * send method.
   */
  @Test
  void send() throws ApiException {
    Transaction transaction =
        Transaction.create(
            BlockchainAddress.fromString("000000000000000000000000000000000000000000"),
            new byte[0]);
    SignedTransaction signedTransaction =
        SignedTransaction.create(
            SenderAuthenticationKeyPair.fromString("aa"), 1, 2, 3, "4", transaction);
    SentTransaction sentTransaction =
        new SentTransaction(signedTransaction, new TransactionPointer());
    BlockchainTransactionClient mock = Mockito.mock(BlockchainTransactionClient.class);
    Mockito.when(mock.send(signedTransaction)).thenReturn(sentTransaction);

    BlockchainClientForDeploymentImpl deploymentTransactionClient =
        new BlockchainClientForDeploymentImpl(mock, null);
    assertThat(deploymentTransactionClient.send(signedTransaction)).isEqualTo(sentTransaction);
    Mockito.verify(mock, Mockito.times(1)).send(signedTransaction);
  }

  /**
   * The deployment transaction client impl wait method calls the underlying transaction client's
   * wait method.
   */
  @Test
  void waitForEvents() throws ApiException {
    Transaction transaction =
        Transaction.create(
            BlockchainAddress.fromString("000000000000000000000000000000000000000000"),
            new byte[0]);
    SignedTransaction signedTransaction =
        SignedTransaction.create(
            SenderAuthenticationKeyPair.fromString("aa"), 1, 2, 3, "4", transaction);
    SentTransaction sentTransaction =
        new SentTransaction(signedTransaction, new TransactionPointer());
    TransactionTree transactionTree = new TransactionTree(new ExecutedTransaction(), List.of());
    BlockchainTransactionClient mock = Mockito.mock(BlockchainTransactionClient.class);
    Mockito.when(mock.waitForSpawnedEvents(sentTransaction)).thenReturn(transactionTree);

    BlockchainClientForDeploymentImpl deploymentTransactionClient =
        new BlockchainClientForDeploymentImpl(mock, null);
    assertThat(deploymentTransactionClient.waitForSpawnedEvents(sentTransaction))
        .isEqualTo(transactionTree);
    Mockito.verify(mock, Mockito.times(1)).waitForSpawnedEvents(sentTransaction);
  }

  /** The deployment state client impl calls the wrapped chain controller's get contract method. */
  @Test
  void getContract() throws ApiException {
    ChainControllerApi mock = Mockito.mock(ChainControllerApi.class);
    Mockito.when(mock.getContract(Mockito.anyString(), Mockito.any()))
        .thenReturn(new Contract().abi(HexFormat.of().parseHex("aa")));
    BlockchainClientForDeploymentImpl deploymentStateClient =
        new BlockchainClientForDeploymentImpl(null, mock);
    assertThat(
            deploymentStateClient
                .getContract(
                    BlockchainAddress.fromString("000000000000000000000000000000000000000000"))
                .getAbi())
        .isEqualTo(HexFormat.of().parseHex("aa"));
    Mockito.verify(mock, Mockito.times(1))
        .getContract("000000000000000000000000000000000000000000", null);
  }
}
