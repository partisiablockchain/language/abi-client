package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.SenderAuthenticationKeyPair;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.ExecutionStatus;
import com.partisiablockchain.api.transactionclient.model.Failure;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.Base64;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Testing building deployment transactions using the {@link DeploymentClient}. */
public final class DeploymentClientTest {

  /**
   * Can build the rpc of pub deploy call wth the minimum required arguments of contract bytes, abi
   * and contract initialization bytes.
   */
  @Test
  void buildRpcPublic() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .buildRpc();

    assertDefaultPubDeployRpc(deployRpc);
  }

  /**
   * Can build the rpc of real deploy call wth the minimum required arguments of contract bytes, abi
   * and contract initialization bytes.
   */
  @Test
  void buildRpcReal() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .zkwa(loadBytes("average_salary.zkwa"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .buildRpc();

    assertDefaultRealDeployRpc(deployRpc);
  }

  /** Can build the deploy call with a pbc file instead of abi and wasm file. */
  @Test
  void buildPbcFilePub() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .pbcFile(loadBytes("nickname.pbc"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .buildRpc();

    assertDefaultPubDeployRpc(deployRpc);
  }

  /** Can build the deploy call with a pbc file instead of abi and zkwa file. */
  @Test
  void buildPbcFileReal() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .pbcFile(loadBytes("average_salary.pbc"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .buildRpc();

    assertDefaultRealDeployRpc(deployRpc);
  }

  /** Can build the initialization bytes using the rpc builder pattern and the abi. */
  @Test
  void buildInitWithBuilder() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("token_v2.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(
                builder -> {
                  builder.addString("Token Name");
                  builder.addString("TS");
                  builder.addU8((byte) 4);
                  builder.addU128(BigInteger.valueOf(100));
                })
            .buildRpc();

    byte[] expectedInitBytes =
        HexFormat.of()
            .parseHex(
                "ffffffff0f" // Shortname
                    + "0000000a546f6b656e204e616d65" // Token Name
                    + "000000025453" // TS
                    + "04" // 4
                    + "00000000000000000000000000000064" // 100
                );

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(4); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("nickname.wasm"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("token_v2.abi"));
    assertThat(stream.readDynamicBytes()).isEqualTo(expectedInitBytes);
    assertThat(stream.readInt()).isEqualTo(1); // binder id
  }

  /** Setting the initialization bytes will replace previously set init bytes. */
  @Test
  void settingInitReplacesOldInit() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpcBytesLast =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(builder -> {})
            .initRpc(HexFormat.of().parseHex("00"))
            .buildRpc();

    SafeDataInputStream streamBytesLast = SafeDataInputStream.createFromBytes(deployRpcBytesLast);
    streamBytesLast.readUnsignedByte();
    streamBytesLast.readDynamicBytes();
    streamBytesLast.readDynamicBytes();
    assertThat(streamBytesLast.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("00"));

    byte[] deployRpcBuilderLast =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("00"))
            .initRpc(builder -> {})
            .buildRpc();

    SafeDataInputStream streamBuilderLast =
        SafeDataInputStream.createFromBytes(deployRpcBuilderLast);
    streamBuilderLast.readUnsignedByte();
    streamBuilderLast.readDynamicBytes();
    streamBuilderLast.readDynamicBytes();
    assertThat(streamBuilderLast.readDynamicBytes())
        .isEqualTo(HexFormat.of().parseHex("ffffffff0f"));
  }

  /** Can specify the required stakes for the deploy call. */
  @Test
  void buildRequiredStakes() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .zkwa(loadBytes("average_salary.zkwa"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .requiredStakes(42L)
            .buildRpc();

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(2); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.zkwa"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("ffffffff0f"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.abi"));
    assertThat(stream.readLong()).isEqualTo(42L); // Required stakes
    assertThat(stream.readInt()).isEqualTo(0); // Default allowed jurisdictions size
    assertThat(stream.readInt()).isEqualTo(1); // binder id
  }

  /** Can specify the allowed jurisdictions for the deploy call. */
  @Test
  void buildAllowedJurisdictions() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .zkwa(loadBytes("average_salary.zkwa"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f01"))
            .allowedJurisdictions(List.of(List.of(42)))
            .buildRpc();

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(2); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.zkwa"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("ffffffff0f01"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.abi"));
    assertThat(stream.readLong()).isEqualTo(2000_0000L); // Default required stakes
    assertThat(stream.readInt()).isEqualTo(1); // Allowed jurisdictions size
    assertThat(stream.readInt()).isEqualTo(1); // Inner list size
    assertThat(stream.readInt()).isEqualTo(42); // Allowed jurisdiction
    assertThat(stream.readInt()).isEqualTo(1); // binder id
  }

  /** Can override the binder id used. */
  @Test
  void buildRpcBinderId() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    byte[] deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .binderId(99)
            .buildRpc();

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(4); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("nickname.wasm"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("nickname.abi"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("ffffffff0f"));
    assertThat(stream.readInt()).isEqualTo(99); // binder id
  }

  /** Can't build without an abi file. */
  @Test
  void missingAbi() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeploymentBuilder deployBuilder =
        deploymentClient
            .builder()
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));

    assertThatThrownBy(deployBuilder::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing abi bytes");
  }

  /** Can't build without contract bytes. */
  @Test
  void missingContractBytes() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeploymentBuilder deployBuilder =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));

    assertThatThrownBy(deployBuilder::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing contract bytes");
  }

  /** Can't build without initialization bytes. */
  @Test
  void missingInitRpc() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeploymentBuilder deployBuilder =
        deploymentClient
            .builder()
            .wasm(loadBytes("nickname.wasm"))
            .abi(loadBytes("average_salary.abi"));

    assertThatThrownBy(deployBuilder::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing initialization bytes");
  }

  /** Trying to add required stakes to a public contract throws an exception. */
  @Test
  void illegalRequiredStakes() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeploymentBuilder deployBuilder =
        deploymentClient
            .builder()
            .wasm(loadBytes("nickname.wasm"))
            .abi(loadBytes("average_salary.abi"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .requiredStakes(42L);

    assertThatThrownBy(deployBuilder::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Required stakes is not allowed as argument to pub deploy");
  }

  /** Trying to add allowed jurisdictions to a public contract throws an exception. */
  @Test
  void illegalAllowedJurisdictions() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeploymentBuilder deployBuilder =
        deploymentClient
            .builder()
            .wasm(loadBytes("nickname.wasm"))
            .abi(loadBytes("average_salary.abi"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .allowedJurisdictions(List.of(List.of(42)));

    assertThatThrownBy(deployBuilder::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Allowed jurisdictions is not allowed as argument to pub deploy");
  }

  /** Can build the signed deploy transaction with default gas for public deploy. */
  @Test
  void buildTransactionPub() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    SignedTransaction signedTransaction =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .build();

    assertThat(signedTransaction.getInner().getAddress())
        .isEqualTo(
            BlockchainAddress.fromString(
                "0197a0e238e924025bad144aa0c4913e46308f9a4d")); // Default pub address
    assertThat(signedTransaction.getGasCost()).isEqualTo(800_000L); // Default pub gas cost

    assertDefaultPubDeployRpc(signedTransaction.getInner().getRpc());
  }

  /** Can build the signed deploy transaction with default gas for real deploy. */
  @Test
  void buildTransactionReal() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    SignedTransaction signedTransaction =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .zkwa(loadBytes("average_salary.zkwa"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .build();

    assertThat(signedTransaction.getInner().getAddress())
        .isEqualTo(
            BlockchainAddress.fromString(
                "018bc1ccbb672b87710327713c97d43204905082cb")); // Default real address
    assertThat(signedTransaction.getGasCost()).isEqualTo(2_000_000L); // Default gas cost

    assertDefaultRealDeployRpc(signedTransaction.getInner().getRpc());
  }

  /** Can set the gas cost of the deploy transaction. */
  @Test
  void buildTransactionWithGasCost() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    SignedTransaction signedTransaction =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .gasCost(400)
            .build();

    assertThat(signedTransaction.getGasCost()).isEqualTo(400);

    assertDefaultPubDeployRpc(signedTransaction.getInner().getRpc());
  }

  /** Can override the deploy address of the deploy contract. */
  @Test
  void buildTransactionWithDeployAddress() {
    BlockchainAddress myDeployAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    DeploymentClient deploymentClient =
        new DeploymentClient(
            new BlockchainClientForDeployment() {
              @Override
              public Contract getContract(BlockchainAddress address) throws ApiException {
                assertThat(address).isEqualTo(myDeployAddress);
                byte[] abiBytes = loadBytes("pub_deploy.abi");
                byte[] stateBytes = loadBase64File("pub_deploy_state.bin");
                return new Contract().serializedContract(stateBytes).abi(abiBytes);
              }

              @Override
              public SignedTransaction sign(Transaction transaction, long gasCost) {
                SenderAuthenticationKeyPair aa = SenderAuthenticationKeyPair.fromString("aa");
                return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
              }

              @Override
              public SentTransaction send(SignedTransaction signedTransaction) {
                throw new RuntimeException("Should not be reached");
              }

              @Override
              public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
                throw new RuntimeException("Should not be reached");
              }
            });

    SignedTransaction signedTransaction =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .deployContractAddress(myDeployAddress)
            .build();

    assertThat(signedTransaction.getInner().getAddress()).isEqualTo(myDeployAddress);

    assertDefaultPubDeployRpc(signedTransaction.getInner().getRpc());
  }

  /** Can deploy a public contract. */
  @Test
  void deployPub() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeployResult deployResult =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .deploy();

    assertThat(deployResult.contractAddress())
        .isEqualTo(BlockchainAddress.fromString("02fa71e0569e82a1a1983a4a8455f5bd382a760a0d"));
    assertThat(deployResult.executedTransaction().transaction().getIdentifier())
        .isEqualTo(
            Hash.fromString("0956da1ca66fee9fe8e501fffa71e0569e82a1a1983a4a8455f5bd382a760a0d"));
  }

  /** Can deploy a zk contract. */
  @Test
  void deployReal() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub());
    DeployResult deployResult =
        deploymentClient
            .builder()
            .abi(loadBytes("average_salary.abi"))
            .zkwa(loadBytes("average_salary.zkwa"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"))
            .deploy();

    assertThat(deployResult.contractAddress())
        .isEqualTo(BlockchainAddress.fromString("03366bda00ca8db3cf92f380817db243ee060d8f0f"));
    assertThat(deployResult.executedTransaction().transaction().getIdentifier())
        .isEqualTo(
            Hash.fromString("5fe9792a3ef4a0b95c57b627366bda00ca8db3cf92f380817db243ee060d8f0f"));
  }

  /** If the client cannot get the deploy contract's state, an exception is thrown. */
  @Test
  void unableToGetDeployState() {
    DeploymentClient deploymentClient =
        new DeploymentClient(
            new BlockchainClientForDeployment() {
              @Override
              public Contract getContract(BlockchainAddress address) throws ApiException {
                throw new RuntimeException("Error 404");
              }

              @Override
              public SignedTransaction sign(Transaction transaction, long gasCost) {
                throw new RuntimeException("Should not be reached");
              }

              @Override
              public SentTransaction send(SignedTransaction signedTransaction) {
                throw new RuntimeException("Should not be reached");
              }

              @Override
              public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
                throw new RuntimeException("Should not be reached");
              }
            });

    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));

    assertThatThrownBy(deployRpc::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to get deploy contract state");
  }

  /**
   * If the contract contains a binder version not present in the deploy contract's state, an error
   * is thrown.
   */
  @Test
  void unknownBinderVersion() {
    DeploymentClient deploymentClient =
        new DeploymentClient(
            new BlockchainClientForDeployment() {
              @Override
              public Contract getContract(BlockchainAddress address) throws ApiException {
                byte[] abiBytes = loadBytes("pub_deploy.abi");
                byte[] noBindersState =
                    HexFormat.of()
                        .parseHex("0100000000000000001014c5f00d7c6d70c3d0919fd7f81c7b9bfe16063620");
                return new Contract().serializedContract(noBindersState).abi(abiBytes);
              }

              @Override
              public SignedTransaction sign(Transaction transaction, long gasCost) {
                throw new RuntimeException("Unreached");
              }

              @Override
              public SentTransaction send(SignedTransaction signedTransaction) {
                throw new RuntimeException("Unreached");
              }

              @Override
              public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
                throw new RuntimeException("Unreached");
              }
            });
    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));

    assertThatThrownBy(deployRpc::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No binders exists supporting version 10.3.0");
  }

  /** If the deployment transaction fails an exceptions is thrown. */
  @Test
  void deployFailed() {
    ExecutionStatus executionStatus =
        new ExecutionStatus()
            .success(false)
            .failure(new Failure().errorMessage("Some failure message"));
    DeploymentClient deploymentClient =
        new DeploymentClient(new BlockchainClientForDeploymentStub(executionStatus, List.of()));

    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));
    assertThatThrownBy(deployRpc::deploy)
        .isInstanceOf(TransactionException.class)
        .hasMessage(
            "Transaction '0956da1ca66fee9fe8e501fffa71e0569e82a1a1983a4a8455f5bd382a760a0d' failed"
                + " with cause: Some failure message");
  }

  /** If a spawned event from the deployment transaction fails an exceptions is thrown. */
  @Test
  void deployEventFailed() {
    ExecutionStatus executionStatus = new ExecutionStatus().success(true);
    ExecutedTransaction failingEvent =
        new ExecutedTransaction()
            .executionStatus(
                new ExecutionStatus()
                    .success(false)
                    .failure(new Failure().errorMessage("Some event failure message")));
    DeploymentClient deploymentClient =
        new DeploymentClient(
            new BlockchainClientForDeploymentStub(executionStatus, List.of(failingEvent)));

    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));
    assertThatThrownBy(deployRpc::deploy)
        .isInstanceOf(TransactionException.class)
        .hasMessage(
            "Transaction '0956da1ca66fee9fe8e501fffa71e0569e82a1a1983a4a8455f5bd382a760a0d' failed"
                + " with cause: Some event failure message");
  }

  /** If the client can't send the deployment transaction an exception is thrown. */
  @Test
  void sendDeploymentTransactionFails() {
    BlockchainClientForDeployment blockchainClientForDeployment =
        new BlockchainClientForDeployment() {
          @Override
          public Contract getContract(BlockchainAddress address) throws ApiException {
            byte[] abiBytes = loadBytes("pub_deploy.abi");
            byte[] stateBytes = loadBase64File("pub_deploy_state.bin");
            return new Contract().serializedContract(stateBytes).abi(abiBytes);
          }

          @Override
          public SignedTransaction sign(Transaction transaction, long gasCost) {
            SenderAuthenticationKeyPair aa = SenderAuthenticationKeyPair.fromString("aa");
            return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
          }

          @Override
          public SentTransaction send(SignedTransaction signedTransaction) {
            throw new RuntimeException("Error 400");
          }

          @Override
          public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
            throw new RuntimeException("Should not be reached");
          }
        };
    DeploymentClient deploymentClient = new DeploymentClient(blockchainClientForDeployment);

    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));
    assertThatThrownBy(deployRpc::deploy)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Failed to send deployment transaction");
  }

  /**
   * Can create a deployment client using a base url of a reader node and sender authentication. As
   * there is no reader node at the url it fails to get the deploy state.
   */
  @Test
  void createWithBaseUrl() {
    DeploymentClient deploymentClient =
        DeploymentClient.create(
            "http://locahost:9432", SenderAuthenticationKeyPair.fromString("aa"));

    DeploymentBuilder deployRpc =
        deploymentClient
            .builder()
            .abi(loadBytes("nickname.abi"))
            .wasm(loadBytes("nickname.wasm"))
            .initRpc(HexFormat.of().parseHex("ffffffff0f"));

    assertThatThrownBy(deployRpc::buildRpc)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to get deploy contract state");
  }

  private static void assertDefaultPubDeployRpc(byte[] deployRpc) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(4); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("nickname.wasm"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("nickname.abi"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("ffffffff0f"));
    assertThat(stream.readInt()).isEqualTo(1); // binder id
  }

  private static void assertDefaultRealDeployRpc(byte[] deployRpc) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(deployRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(2); // shortname
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.zkwa"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("ffffffff0f"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("average_salary.abi"));
    assertThat(stream.readLong()).isEqualTo(2000_0000L); // Default required stakes
    assertThat(stream.readInt()).isEqualTo(0); // Default allowed jurisdictions size
    assertThat(stream.readInt()).isEqualTo(1); // binder id
  }

  static final class BlockchainClientForDeploymentStub implements BlockchainClientForDeployment {

    private final ExecutionStatus executionStatus;
    private final List<ExecutedTransaction> events;

    BlockchainClientForDeploymentStub(
        ExecutionStatus executionStatus, List<ExecutedTransaction> events) {
      this.executionStatus = executionStatus;
      this.events = events;
    }

    BlockchainClientForDeploymentStub() {
      this(new ExecutionStatus().success(true), List.of());
    }

    @Override
    public SignedTransaction sign(Transaction transaction, long gasCost) {
      SenderAuthenticationKeyPair aa = SenderAuthenticationKeyPair.fromString("aa");
      return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
    }

    @Override
    public SentTransaction send(SignedTransaction signedTransaction) {
      return new SentTransaction(
          signedTransaction, new TransactionPointer().identifier(signedTransaction.identifier()));
    }

    @Override
    public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
      ExecutedTransaction executedTransaction = new ExecutedTransaction();
      executedTransaction.identifier(sentTransaction.transactionPointer().getIdentifier());
      executedTransaction.executionStatus(executionStatus);
      return new TransactionTree(executedTransaction, events);
    }

    @Override
    public Contract getContract(BlockchainAddress address) {
      if (address.equals(DeploymentBuilder.DEFAULT_PUB_ADDRESS)) {
        byte[] abiBytes = loadBytes("pub_deploy.abi");
        byte[] stateBytes = loadBase64File("pub_deploy_state.bin");
        return new Contract().serializedContract(stateBytes).abi(abiBytes);
      } else {
        byte[] abiBytes = loadBytes("real_deploy.abi");
        byte[] stateBytes = loadBase64File("real_deploy_state.bin");
        return new Contract().serializedContract(stateBytes).abi(abiBytes);
      }
    }
  }

  static byte[] loadBytes(String fileName) {
    return ExceptionConverter.call(
        () -> DeploymentClientTest.class.getResourceAsStream(fileName).readAllBytes());
  }

  static byte[] loadBase64File(String fileName) {
    return ExceptionConverter.call(
        () ->
            Base64.getDecoder()
                .decode(DeploymentClientTest.class.getResourceAsStream(fileName).readAllBytes()));
  }
}
