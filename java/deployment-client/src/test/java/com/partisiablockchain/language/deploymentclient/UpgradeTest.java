package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.deploymentclient.DeploymentClientTest.loadBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.ExecutionStatus;
import com.partisiablockchain.api.transactionclient.model.Failure;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Testing building and sending upgrade transactions through the {@link DeploymentClient}. */
public final class UpgradeTest {

  /** Can build the rpc of an upgrade transaction. */
  @Test
  void buildUpgradeRpc() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    byte[] upgradeRpc =
        deploymentClient
            .upgradeBuilder()
            .abi(loadBytes("upgradable_v2.abi"))
            .wasm(loadBytes("upgradable_v2.wasm"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
            .buildRpc();

    assertDefaultUpgradeRpc(upgradeRpc);
  }

  /** Can build the upgrade rpc with a pbc file. */
  @Test
  void buildUpgradeRpcPbcFile() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    byte[] upgradeRpc =
        deploymentClient
            .upgradeBuilder()
            .pbcFile(loadBytes("upgradable_v2.pbc"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
            .buildRpc();

    assertDefaultUpgradeRpc(upgradeRpc);
  }

  /** Can set the rpc received by the contract when upgrading. */
  @Test
  void buildUpgradeRpcWithContractUpgradeRpc() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    byte[] upgradeRpc =
        deploymentClient
            .upgradeBuilder()
            .abi(loadBytes("upgradable_v2.abi"))
            .wasm(loadBytes("upgradable_v2.wasm"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000001"))
            .upgradeRpc(HexFormat.of().parseHex("010203"))
            .buildRpc();

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(upgradeRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(5); // shortname
    assertThat(BlockchainAddress.read(stream))
        .isEqualTo(BlockchainAddress.fromString("010000000000000000000000000000000000000001"));
    assertThat(stream.readInt()).isEqualTo(2); // binder id
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("upgradable_v2.wasm"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("upgradable_v2.abi"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex("010203"));
  }

  /** Can't build upgrade without providing abi. */
  @Test
  void missingAbi() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    assertThatThrownBy(
            () ->
                deploymentClient
                    .upgradeBuilder()
                    .wasm(loadBytes("upgradable_v2.wasm"))
                    .contractAddress(
                        BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
                    .buildRpc())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing abi bytes");
  }

  /** Can't build upgrade without providing contract bytes. */
  @Test
  void missingContractBytes() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    assertThatThrownBy(
            () ->
                deploymentClient
                    .upgradeBuilder()
                    .abi(loadBytes("upgradable_v2.abi"))
                    .contractAddress(
                        BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
                    .buildRpc())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing contract bytes");
  }

  /** Can't build upgrade without providing contract address. */
  @Test
  void missingContractAddress() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    assertThatThrownBy(
            () ->
                deploymentClient
                    .upgradeBuilder()
                    .abi(loadBytes("upgradable_v2.abi"))
                    .wasm(loadBytes("upgradable_v2.wasm"))
                    .buildRpc())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Missing contract address");
  }

  /** Cannot upgrade a zk contract. */
  @Test
  void cannotUpgradeZk() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    assertThatThrownBy(
            () -> deploymentClient.upgradeBuilder().pbcFile(loadBytes("average_salary.pbc")))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot upgrade a zk contract");
  }

  /** Can build the signed transaction of an upgrade invocation. */
  @Test
  void buildUpgradeTransaction() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    SignedTransaction upgradeTransaction =
        deploymentClient
            .upgradeBuilder()
            .abi(loadBytes("upgradable_v2.abi"))
            .wasm(loadBytes("upgradable_v2.wasm"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
            .build();

    assertThat(upgradeTransaction.getInner().getAddress())
        .isEqualTo(DeploymentBuilder.DEFAULT_PUB_ADDRESS);
    assertThat(upgradeTransaction.getGasCost()).isEqualTo(2_000_000L);
    assertThat(upgradeTransaction.identifier())
        .isEqualTo(
            Hash.fromString("da6d9f849087a0910a9f6cab05c86209670fba87d2d37f45b2fc4a0f6694f91d"));
    assertDefaultUpgradeRpc(upgradeTransaction.getInner().getRpc());
  }

  /** Can set the gas cost of the upgrade transaction. */
  @Test
  void buildUpgradeTransactionWithGasCost() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());

    SignedTransaction upgradeTransaction =
        deploymentClient
            .upgradeBuilder()
            .abi(loadBytes("upgradable_v2.abi"))
            .wasm(loadBytes("upgradable_v2.wasm"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
            .gasCost(400000)
            .build();

    assertThat(upgradeTransaction.getInner().getAddress())
        .isEqualTo(DeploymentBuilder.DEFAULT_PUB_ADDRESS);
    assertThat(upgradeTransaction.getGasCost()).isEqualTo(400000);
    assertDefaultUpgradeRpc(upgradeTransaction.getInner().getRpc());
  }

  /** Can send the upgrade transaction. */
  @Test
  void sendUpgradeTransaction() {
    DeploymentClient deploymentClient =
        new DeploymentClient(new DeploymentClientTest.BlockchainClientForDeploymentStub());
    TransactionTree upgradeTree =
        deploymentClient
            .upgradeBuilder()
            .abi(loadBytes("upgradable_v2.abi"))
            .wasm(loadBytes("upgradable_v2.wasm"))
            .contractAddress(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
            .upgrade();

    assertThat(upgradeTree.transaction().getIdentifier())
        .isEqualTo(
            Hash.fromString("da6d9f849087a0910a9f6cab05c86209670fba87d2d37f45b2fc4a0f6694f91d"));
  }

  /** If the upgrade transaction fails, an exception is thrown with the error message. */
  @Test
  void sendUpgradeTransactionFails() {
    ExecutionStatus executionStatus =
        new ExecutionStatus()
            .success(false)
            .failure(new Failure().errorMessage("Some failure message"));
    DeploymentClient deploymentClient =
        new DeploymentClient(
            new DeploymentClientTest.BlockchainClientForDeploymentStub(executionStatus, List.of()));
    assertThatThrownBy(
            () ->
                deploymentClient
                    .upgradeBuilder()
                    .abi(loadBytes("upgradable_v2.abi"))
                    .wasm(loadBytes("upgradable_v2.wasm"))
                    .contractAddress(
                        BlockchainAddress.fromString("010000000000000000000000000000000000000000"))
                    .upgrade())
        .isInstanceOf(TransactionException.class)
        .hasMessage(
            "Transaction 'da6d9f849087a0910a9f6cab05c86209670fba87d2d37f45b2fc4a0f6694f91d' failed"
                + " with cause: Some failure message");
  }

  private static void assertDefaultUpgradeRpc(byte[] upgradeRpc) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(upgradeRpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(5); // shortname
    assertThat(BlockchainAddress.read(stream))
        .isEqualTo(BlockchainAddress.fromString("010000000000000000000000000000000000000000"));
    assertThat(stream.readInt()).isEqualTo(2); // binder id
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("upgradable_v2.wasm"));
    assertThat(stream.readDynamicBytes()).isEqualTo(loadBytes("upgradable_v2.abi"));
    assertThat(stream.readDynamicBytes()).isEqualTo(HexFormat.of().parseHex(""));
  }
}
