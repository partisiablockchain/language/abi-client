package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import java.util.Map;
import org.junit.jupiter.api.Test;

/**
 * Testing internal behaviour of {@link DeploymentBuilder} not easily tested using the deploy
 * client.
 */
public final class DeploymentBuilderTest {
  private static final Map<Integer, DeploymentBuilder.VersionInterval> binderMap =
      Map.of(
          1,
          new DeploymentBuilder.VersionInterval(new AbiVersion(2, 0, 0), new AbiVersion(2, 0, 0)),
          2,
          new DeploymentBuilder.VersionInterval(new AbiVersion(3, 1, 0), new AbiVersion(3, 4, 0)));

  /**
   * Returns the binder id of a binder if its version interval includes the given binder version.
   */
  @Test
  void findBinderId() {
    int binderId = DeploymentBuilder.findBinderId(binderMap, new AbiVersion(2, 0, 0));
    assertThat(binderId).isEqualTo(1);
  }

  /** Different binder versions can give same binder id if it supports a range of versions. */
  @Test
  void multipleBinderVersionsGiveSameId() {
    int binderId1 = DeploymentBuilder.findBinderId(binderMap, new AbiVersion(3, 1, 0));
    int binderId2 = DeploymentBuilder.findBinderId(binderMap, new AbiVersion(3, 3, 0));
    assertThat(binderId1).isEqualTo(2);
    assertThat(binderId2).isEqualTo(2);
  }

  /** If the binder version is not supported by any binders, it throws. */
  @Test
  void cannotFindBinderId() {
    assertThatThrownBy(() -> DeploymentBuilder.findBinderId(binderMap, new AbiVersion(1, 0, 0)))
        .hasMessage("No binders exists supporting version 1.0.0");
    assertThatThrownBy(() -> DeploymentBuilder.findBinderId(binderMap, new AbiVersion(2, 1, 0)))
        .hasMessage("No binders exists supporting version 2.1.0");
    assertThatThrownBy(() -> DeploymentBuilder.findBinderId(binderMap, new AbiVersion(3, 0, 0)))
        .hasMessage("No binders exists supporting version 3.0.0");
    assertThatThrownBy(() -> DeploymentBuilder.findBinderId(binderMap, new AbiVersion(4, 0, 0)))
        .hasMessage("No binders exists supporting version 4.0.0");
  }
}
