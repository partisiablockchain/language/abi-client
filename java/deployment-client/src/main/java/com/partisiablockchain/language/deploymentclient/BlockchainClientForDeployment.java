package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.api.transactionclient.utils.ApiException;

/** Blockchain client for getting contract state and building and sending transactions. */
public interface BlockchainClientForDeployment {

  /**
   * Get the contract state for a given address.
   *
   * @param address contract address
   * @return contract state
   * @throws ApiException if it cannot get the state
   */
  Contract getContract(BlockchainAddress address) throws ApiException;

  /**
   * Sign a transaction.
   *
   * @param transaction transaction to sign
   * @param gasCost gas cost of the transaction
   * @return the signed transaction
   */
  SignedTransaction sign(Transaction transaction, long gasCost);

  /**
   * Send a signed transaction.
   *
   * @param signedTransaction the signed transaction to send
   * @return the sent transaction
   */
  SentTransaction send(SignedTransaction signedTransaction);

  /**
   * Wait for a transaction and its spawned events.
   *
   * @param sentTransaction the sent transaction to wait for
   * @return the executed transaction tree of transaction and its events
   */
  TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction);
}
