package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.Failure;
import java.io.Serial;
import java.util.Objects;

/** Runtime error of a deployment transaction. */
public final class TransactionException extends RuntimeException {
  @Serial private static final long serialVersionUID = 1L;

  /**
   * Constructor for {@link TransactionException}.
   *
   * @param transactionTree the executed transaction tree containing at least one failing
   *     transaction or event.
   */
  public TransactionException(TransactionTree transactionTree) {
    super(
        "Transaction '%s' failed with cause: %s"
            .formatted(
                transactionTree.transaction().getIdentifier(),
                transactionTreeToErrorMsg(transactionTree)));
  }

  private static String transactionTreeToErrorMsg(TransactionTree transactionTree) {
    String errorMsg = executedToErrorMsg(transactionTree.transaction());
    if (errorMsg != null) {
      return errorMsg;
    }
    return transactionTree.events().stream()
        .map(TransactionException::executedToErrorMsg)
        .filter(Objects::nonNull)
        .findFirst()
        .orElse(null);
  }

  private static String executedToErrorMsg(ExecutedTransaction executedTransaction) {
    if (executedTransaction.getExecutionStatus().getSuccess()) {
      return null;
    }
    Failure failure = executedTransaction.getExecutionStatus().getFailure();
    return failure.getErrorMessage();
  }
}
