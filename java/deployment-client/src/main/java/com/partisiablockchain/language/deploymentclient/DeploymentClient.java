package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SenderAuthentication;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.api.ShardControllerApi;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;

/** Client for deploying contracts. */
public final class DeploymentClient {
  private final BlockchainClientForDeployment blockchainClient;

  /**
   * Constructor for {@link DeploymentClient}.
   *
   * @param blockchainClient client for getting state and building and sending transactions
   */
  public DeploymentClient(BlockchainClientForDeployment blockchainClient) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Create a {@link DeploymentClient} with our default transaction client and state client.
   *
   * @param transactionClient transaction client building and sending transactions
   * @param chainController blockchain client
   * @return a new {@link DeploymentClient}
   */
  public static DeploymentClient create(
      BlockchainTransactionClient transactionClient, ChainControllerApi chainController) {
    return new DeploymentClient(
        new BlockchainClientForDeploymentImpl(transactionClient, chainController));
  }

  /**
   * Create a {@link DeploymentClient} with targeting a specific base url.
   *
   * @param baseUrl base url of the blockchain to target
   * @param senderAuthentication sender authentication for signing deploy transactions
   * @return a new {@link DeploymentClient}
   */
  public static DeploymentClient create(String baseUrl, SenderAuthentication senderAuthentication) {

    ApiClient apiClient = new ApiClient().setBasePath(baseUrl);
    ChainControllerApi chainController = new ChainControllerApi(apiClient);
    ShardControllerApi shardController = new ShardControllerApi(apiClient);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.create(chainController, shardController, senderAuthentication);
    return create(transactionClient, chainController);
  }

  /**
   * Create a builder for deploying a contract.
   *
   * @return a {@link DeploymentBuilder}
   */
  public DeploymentBuilder builder() {
    return new DeploymentBuilder(blockchainClient);
  }

  /**
   * Create a builder for upgrading a contract.
   *
   * @return a {@link UpgradeBuilder}
   */
  public UpgradeBuilder upgradeBuilder() {
    return new UpgradeBuilder(blockchainClient);
  }
}
