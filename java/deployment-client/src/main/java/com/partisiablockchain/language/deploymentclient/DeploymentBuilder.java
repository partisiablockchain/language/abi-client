package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** Builder for deploying a contract through the {@link DeploymentClient}. */
public final class DeploymentBuilder {
  private static final List<List<Integer>> DEFAULT_ALLOWED_JURISDICTION = List.of();
  private static final long DEFAULT_REQUIRED_STAKES = 2000_0000L;
  static final BlockchainAddress DEFAULT_PUB_ADDRESS =
      BlockchainAddress.fromString("0197a0e238e924025bad144aa0c4913e46308f9a4d");
  static final BlockchainAddress DEFAULT_REAL_ADDRESS =
      BlockchainAddress.fromString("018bc1ccbb672b87710327713c97d43204905082cb");
  private static final long DEFAULT_PUB_GAS_COST = 800_000L;
  private static final long DEFAULT_REAL_GAS_COST = 2_000_000L;

  private final BlockchainClientForDeployment blockchainClient;
  private boolean isZk;
  private byte[] contractBytes;
  private byte[] abiBytes;
  private byte[] initRpc;
  private BlockchainAddress deployContractAddress;
  private List<List<Integer>> allowedJurisdictions;
  private Long requiredStakes;
  private Long gasCost;
  private Integer binderId;
  private Consumer<AbstractBuilder> initBuilderConsumer;

  /**
   * Constructor for {@link DeploymentBuilder}.
   *
   * @param blockchainClient client for getting state and building and sending transactions
   */
  DeploymentBuilder(BlockchainClientForDeployment blockchainClient) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Set the abi and contract bytes to deploy through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and either {@link #wasm} or {@link #zkwa}.
   *
   * @param pbcBytes the bytes of the pbc file to deploy
   * @return this builder
   */
  public DeploymentBuilder pbcFile(byte[] pbcBytes) {
    return pbcFile(PbcFile.fromBytes(pbcBytes));
  }

  /**
   * Set the abi and contract bytes to deploy through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and either {@link #wasm} or {@link #zkwa}.
   *
   * @param pbcFile the pbc file to deploy
   * @return this builder
   */
  public DeploymentBuilder pbcFile(PbcFile pbcFile) {
    this.isZk = pbcFile.isZk;
    this.abiBytes = pbcFile.getAbiBytes();
    if (pbcFile.isZk) {
      this.contractBytes = pbcFile.getZkwaBytes();
    } else {
      this.contractBytes = pbcFile.getWasmBytes();
    }
    return this;
  }

  /**
   * Set the abi of the contract to deploy.
   *
   * <p>It is required to provide the abi either through this method or through {@link #pbcFile}.
   *
   * @param abi abi bytes to deploy
   * @return this builder
   */
  public DeploymentBuilder abi(byte[] abi) {
    this.abiBytes = abi;
    return this;
  }

  /**
   * Set the contract bytes of the contract to deploy. Using this function denotes that it is a zk
   * contract you are trying to deploy.
   *
   * <p>It is required to provide the contract bytes either though this method, {@link #wasm}, or
   * {@link #pbcFile}.
   *
   * @param zkwa contract bytes to deploy
   * @return this builder
   */
  public DeploymentBuilder zkwa(byte[] zkwa) {
    this.isZk = true;
    this.contractBytes = zkwa;
    return this;
  }

  /**
   * Set the contract bytes of the contract to deploy. Using this function denotes that it is a
   * public contract you are trying to deploy.
   *
   * <p>It is required to provide the contract bytes either though this method, {@link #zkwa}, or
   * {@link #pbcFile}.
   *
   * @param wasm contract bytes to deploy
   * @return this builder
   */
  public DeploymentBuilder wasm(byte[] wasm) {
    this.isZk = false;
    this.contractBytes = wasm;
    return this;
  }

  /**
   * Set the initialization bytes of the contract to deploy.
   *
   * <p>Setting the initialization bytes is required.
   *
   * @param initRpc the initialization bytes
   * @return this builder
   */
  public DeploymentBuilder initRpc(byte[] initRpc) {
    this.initRpc = initRpc;
    this.initBuilderConsumer = null;
    return this;
  }

  /**
   * Build the initialization bytes of the contract to deploy using a rpc builder and the abi of the
   * contract.
   *
   * <p>Setting the initialization bytes is required.
   *
   * @param builderConsumer consumer of a rpc builder building the initialization bytes
   * @return this builder
   */
  public DeploymentBuilder initRpc(Consumer<AbstractBuilder> builderConsumer) {
    this.initBuilderConsumer = builderConsumer;
    this.initRpc = null;
    return this;
  }

  /**
   * Set the contract address of the deploy contract to target.
   *
   * <p>Not a required parameter. The default deploy contract address used depends on if you are
   * deploying a public or a zk contract.
   *
   * <ul>
   *   <li>Default pub deploy address: 0197a0e238e924025bad144aa0c4913e46308f9a4d
   *   <li>Default zk deploy address: 018bc1ccbb672b87710327713c97d43204905082cb
   * </ul>
   *
   * @param deployContractAddress address of the deploy contract to target
   * @return this builder
   */
  public DeploymentBuilder deployContractAddress(BlockchainAddress deployContractAddress) {
    this.deployContractAddress = deployContractAddress;
    return this;
  }

  /**
   * Set the allowed jurisdictions from which zk nodes can be selected. Follows the ISO 3166-1
   * numeric format, meaning jurisdictions are integers in [0-999].
   *
   * <p>This argument is not allowed if trying to deploy a public contract. It is not required if
   * deploying a zk contract and a default of all jurisdictions allowed is used if it is not set.
   *
   * <p>There are three cases for the jurisdictions:
   *
   * <ul>
   *   <li>An outer list containing nothing: All jurisdictions are allowed for all requested nodes.
   *   <li>An outer list containing one inner list: Only jurisdictions in the inner list are allowed
   *       for all requested nodes.
   *   <li>An outer list containing multiple inner lists: Jurisdictions in the first inner list are
   *       allowed for the first requested node, jurisdictions in the second list for the second
   *       requested node etc.
   * </ul>
   *
   * @param allowedJurisdictions the jurisdictions from which zk nodes can be selected. Follows the
   *     ISO 3166-1 numeric format, meaning jurisdictions are integers in [0-999]
   * @return this builder
   */
  public DeploymentBuilder allowedJurisdictions(List<List<Integer>> allowedJurisdictions) {
    this.allowedJurisdictions = allowedJurisdictions;
    return this;
  }

  /**
   * Set the required stakes from the nodes selected for the deployed zk contract.
   *
   * <p>This argument is not allowed if trying to deploy a public contract.
   *
   * <p>It is not required if deploying a zk contract and a default of the minimum amount of stakes
   * '2_000_0000' is used if it is not set.
   *
   * @param requiredStakes the amount of stakes required for the computation.
   * @return this builder
   */
  public DeploymentBuilder requiredStakes(long requiredStakes) {
    this.requiredStakes = requiredStakes;
    return this;
  }

  /**
   * Set the binder id used to determine which binder to deploy the contract with.
   *
   * <p>This argument is not required. By default it will read the state of the deploy contract to
   * find a binder which supports the given contract.
   *
   * @param binderId binder id
   * @return this builder
   */
  public DeploymentBuilder binderId(int binderId) {
    this.binderId = binderId;
    return this;
  }

  /**
   * Set the gas cost used to deploy the contract.
   *
   * <p>The default gas cost depends on if you are deploying a public or a zk contract
   *
   * <ul>
   *   <li>Default public gas cost: 800_000
   *   <li>Default public zk cost: 2_000_000
   * </ul>
   *
   * @param gasCost gas cost used to deploy the contract
   * @return this builder
   */
  public DeploymentBuilder gasCost(long gasCost) {
    this.gasCost = gasCost;
    return this;
  }

  /**
   * Build the rpc for the transaction to the deploy contract.
   *
   * @return the rpc for the deploy contract
   */
  public byte[] buildRpc() {
    validateRequiredParameters();
    validateOptionalParameters();

    Contract deployContract =
        ExceptionConverter.call(
            () -> blockchainClient.getContract(deployContractAddress),
            "Unable to get deploy contract state");
    byte[] deployAbiBytes = deployContract.getAbi();
    byte[] deployState = deployContract.getSerializedContract();
    ContractAbi deployAbi = new AbiParser(deployAbiBytes).parseAbi().contract();

    FileAbi abiToBeDeployed = new AbiParser(abiBytes).parseAbi();
    int binderId = determineBinderId(abiToBeDeployed, deployAbi, deployState);

    byte[] initializationBytes = getInitializationBytes(abiToBeDeployed);

    if (isZk) {
      return realDeployRpc(deployAbi, initializationBytes, binderId);
    } else {
      return pubDeployRpc(deployAbi, initializationBytes, binderId);
    }
  }

  private byte[] getInitializationBytes(FileAbi abiToBeDeployed) {
    if (initRpc != null) {
      return initRpc;
    }

    RpcContractBuilder fnRpcBuilder =
        new RpcContractBuilder(
            abiToBeDeployed.contract(),
            HexFormat.of().parseHex("ffffffff0f"),
            abiToBeDeployed.contract().init().kind());
    initBuilderConsumer.accept(fnRpcBuilder);
    return fnRpcBuilder.getBytes();
  }

  /**
   * Build the signed transaction of the deploy call.
   *
   * @return the signed transaction of the deploy call.
   */
  public SignedTransaction build() {
    byte[] bytes = buildRpc();
    long gasCost;
    if (this.gasCost != null) {
      gasCost = this.gasCost;
    } else if (isZk) {
      gasCost = DEFAULT_REAL_GAS_COST;
    } else {
      gasCost = DEFAULT_PUB_GAS_COST;
    }
    return ExceptionConverter.call(
        () -> blockchainClient.sign(Transaction.create(deployContractAddress, bytes), gasCost));
  }

  /**
   * Build and send the signed transaction to the deploy contract.
   *
   * @return the contract address of deployed contract, and the executed transaction tree of the
   *     transaction.
   */
  public DeployResult deploy() {
    SignedTransaction signedTransaction = build();
    SentTransaction sentTransaction =
        ExceptionConverter.call(
            () -> blockchainClient.send(signedTransaction),
            "Failed to send deployment transaction");
    TransactionTree transactionTree = blockchainClient.waitForSpawnedEvents(sentTransaction);
    if (transactionTree.hasFailures()) {
      throw new TransactionException(transactionTree);
    }
    BlockchainAddress.Type addressType =
        isZk ? BlockchainAddress.Type.CONTRACT_ZK : BlockchainAddress.Type.CONTRACT_PUBLIC;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(addressType, signedTransaction.identifier());
    return new DeployResult(contractAddress, transactionTree);
  }

  /**
   * Create the deployment rpc for a zk contract using the deploy-contract's abi.
   *
   * @param deployAbi the deploy-contracts abi
   * @param initializationBytes the initialization bytes of the deployed contract
   * @param binderId the binder id to deploy with
   * @return the deployment rpc
   */
  private byte[] realDeployRpc(ContractAbi deployAbi, byte[] initializationBytes, int binderId) {
    RpcContractBuilder fnBuilder = new RpcContractBuilder(deployAbi, "deployContractV3");
    fnBuilder.addVecU8(contractBytes);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addVecU8(abiBytes);
    fnBuilder.addI64(requiredStakes);
    AbstractBuilder vecBuilder = fnBuilder.addVec();
    for (List<Integer> allowedJurisdiction : allowedJurisdictions) {
      AbstractBuilder vecVecBuilder = vecBuilder.addVec();
      for (Integer allowed : allowedJurisdiction) {
        vecVecBuilder.addI32(allowed);
      }
    }
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  /**
   * Create the deployment rpc for a public contract using the deploy-contract's abi.
   *
   * @param deployAbi the deploy-contracts abi
   * @param initializationBytes the initialization bytes of the deployed contract
   * @param binderId the binder id to deploy with
   * @return the deployment rpc
   */
  private byte[] pubDeployRpc(ContractAbi deployAbi, byte[] initializationBytes, int binderId) {
    RpcContractBuilder fnBuilder = new RpcContractBuilder(deployAbi, "deployContractWithBinderId");
    fnBuilder.addVecU8(contractBytes);
    fnBuilder.addVecU8(abiBytes);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  private void validateOptionalParameters() {
    if (deployContractAddress == null) {
      if (isZk) {
        deployContractAddress = DEFAULT_REAL_ADDRESS;
      } else {
        deployContractAddress = DEFAULT_PUB_ADDRESS;
      }
    }
    if (isZk && allowedJurisdictions == null) {
      allowedJurisdictions = DEFAULT_ALLOWED_JURISDICTION;
    }
    if (!isZk && allowedJurisdictions != null) {
      throw new RuntimeException("Allowed jurisdictions is not allowed as argument to pub deploy");
    }
    if (isZk && requiredStakes == null) {
      requiredStakes = DEFAULT_REQUIRED_STAKES;
    }
    if (!isZk && requiredStakes != null) {
      throw new RuntimeException("Required stakes is not allowed as argument to pub deploy");
    }
  }

  private void validateRequiredParameters() {
    if (contractBytes == null) {
      throw new RuntimeException("Missing contract bytes");
    }
    if (abiBytes == null) {
      throw new RuntimeException("Missing abi bytes");
    }
    if (initRpc == null && initBuilderConsumer == null) {
      throw new RuntimeException("Missing initialization bytes");
    }
  }

  private int determineBinderId(
      FileAbi abiToBeDeployed, ContractAbi deployAbi, byte[] deployState) {
    if (this.binderId != null) {
      return this.binderId;
    }

    Map<Integer, VersionInterval> binderVersions =
        getSupportedBinderVersions(deployState, deployAbi);

    AbiVersion binderVersion = abiToBeDeployed.versionBinder();
    return findBinderId(binderVersions, binderVersion);
  }

  /**
   * Find the supported binder version intervals for a deploy contract.
   *
   * @param deployState state bytes of the deploy-contract
   * @param abi abi of the deploy-contract
   * @return a map from binder id to supported binder version interval
   */
  static Map<Integer, VersionInterval> getSupportedBinderVersions(
      byte[] deployState, ContractAbi abi) {
    ScValueStruct state = StateReader.create(deployState, abi).readState();
    ScValueMap binders = state.getFieldValue("binders").optionValue().innerValue().mapValue();
    return binders.map().entrySet().stream()
        .collect(
            Collectors.toMap(
                entry -> entry.getKey().optionValue().innerValue().asInt(),
                entry ->
                    versionIntervalFromScBinderInfo(
                        entry.getValue().optionValue().innerValue().structValue())));
  }

  /**
   * Get the supported version interval from the binder info struct.
   *
   * @param binderInfo binder info as a ScValueStruct
   * @return the version interval as a java type
   */
  private static VersionInterval versionIntervalFromScBinderInfo(ScValueStruct binderInfo) {
    ScValueStruct versionInterval =
        binderInfo.getFieldValue("versionInterval").optionValue().innerValue().structValue();
    ScValueStruct scMin =
        versionInterval
            .getFieldValue("supportedBinderVersionMin")
            .optionValue()
            .innerValue()
            .structValue();
    ScValueStruct scMax =
        versionInterval
            .getFieldValue("supportedBinderVersionMax")
            .optionValue()
            .innerValue()
            .structValue();
    AbiVersion min =
        new AbiVersion(
            scMin.getFieldValue("major").asInt(),
            scMin.getFieldValue("minor").asInt(),
            scMin.getFieldValue("patch").asInt());
    AbiVersion max =
        new AbiVersion(
            scMax.getFieldValue("major").asInt(),
            scMax.getFieldValue("minor").asInt(),
            scMax.getFieldValue("patch").asInt());
    return new VersionInterval(min, max);
  }

  /**
   * Find the binder id of a binder which supports a given binder version.
   *
   * @param binders map from binder ids to supported version interval
   * @param binderVersion the binder version of the contract about to be deployed
   * @return the binder id supporting the given binder version
   */
  static int findBinderId(Map<Integer, VersionInterval> binders, AbiVersion binderVersion) {

    return binders.entrySet().stream()
        .filter(entry -> checkDeployVersion(binderVersion, entry.getValue()))
        .map(Map.Entry::getKey)
        .findFirst()
        .orElseThrow(
            () -> new RuntimeException("No binders exists supporting version " + binderVersion));
  }

  private static boolean checkDeployVersion(
      AbiVersion binderVersion, VersionInterval versionInterval) {
    return checkVersion(
        binderVersion,
        versionInterval.max().major(),
        versionInterval.max().minor(),
        versionInterval.min().major(),
        versionInterval.min().minor());
  }

  private static boolean checkVersion(
      AbiVersion binderVersion, int maxMajor, int maxMinor, int minMajor, int minMinor) {
    return maxMajor >= binderVersion.major()
        && maxMinor >= binderVersion.minor()
        && minMajor <= binderVersion.major()
        && minMinor <= binderVersion.minor();
  }

  /**
   * A version interval.
   *
   * @param min min version
   * @param max max version
   */
  record VersionInterval(AbiVersion min, AbiVersion max) {}
}
