package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Map;

/** Builder for upgrading contracts through the {@link DeploymentClient}. */
public final class UpgradeBuilder {
  static final long DEFAULT_UPGRADE_COST = 2_000_000L;
  private final BlockchainClientForDeployment blockchainClient;
  private byte[] contractBytes;
  private byte[] abiBytes;
  private byte[] upgradeRpc = new byte[0];
  private BlockchainAddress contractAddress;
  private long gasCost = DEFAULT_UPGRADE_COST;

  /**
   * Constructor for {@link UpgradeBuilder}.
   *
   * @param blockchainClient client for getting state and building and sending transactions
   */
  UpgradeBuilder(BlockchainClientForDeployment blockchainClient) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Set the abi and contract bytes to upgrade to through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and {@link #wasm}.
   *
   * @param pbcBytes the bytes of the pbc file to upgrade to
   * @return this builder
   */
  public UpgradeBuilder pbcFile(byte[] pbcBytes) {
    return pbcFile(PbcFile.fromBytes(pbcBytes));
  }

  /**
   * Set the abi and contract bytes to upgrade to through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and {@link #wasm}.
   *
   * @param pbcFile the pbc file to upgrade to
   * @return this builder
   */
  public UpgradeBuilder pbcFile(PbcFile pbcFile) {
    this.abiBytes = pbcFile.getAbiBytes();
    if (pbcFile.isZk) {
      throw new RuntimeException("Cannot upgrade a zk contract");
    } else {
      this.contractBytes = pbcFile.getWasmBytes();
    }
    return this;
  }

  /**
   * Set the abi of the contract to upgrade to.
   *
   * <p>It is required to provide the abi either through this method or through {@link #pbcFile}.
   *
   * @param abi abi bytes to upgrade to
   * @return this builder
   */
  public UpgradeBuilder abi(byte[] abi) {
    this.abiBytes = abi;
    return this;
  }

  /**
   * Set the contract bytes of the contract to upgrade to.
   *
   * <p>It is required to provide the contract bytes either though this method, or {@link #pbcFile}.
   *
   * @param wasm contract bytes to upgrade to
   * @return this builder
   */
  public UpgradeBuilder wasm(byte[] wasm) {
    this.contractBytes = wasm;
    return this;
  }

  /**
   * Set the address of the contract to be upgraded.
   *
   * <p>This is a required parameter
   *
   * @param contractAddress address of the contract to be upgraded
   * @return this builder
   */
  public UpgradeBuilder contractAddress(BlockchainAddress contractAddress) {
    this.contractAddress = contractAddress;
    return this;
  }

  /**
   * Set the rpc given to contract on its upgrade. If not set an empty upgrade rpc is used.
   *
   * @param upgradeRpc the upgrade rpc
   * @return this builder
   */
  public UpgradeBuilder upgradeRpc(byte[] upgradeRpc) {
    this.upgradeRpc = upgradeRpc;
    return this;
  }

  /**
   * Set the gas cost used to upgrade the contract. If not set a default of 2_000_000 gas is used.
   *
   * @param gasCost gas cost used to upgrade the contract
   * @return this builder
   */
  public UpgradeBuilder gasCost(long gasCost) {
    this.gasCost = gasCost;
    return this;
  }

  /**
   * Build the rpc for the upgrade transaction to the deploy-contract.
   *
   * @return the rpc for the upgrade transaction
   */
  public byte[] buildRpc() {
    validateRequiredParameters();

    Contract deployContract =
        ExceptionConverter.call(
            () -> blockchainClient.getContract(DeploymentBuilder.DEFAULT_PUB_ADDRESS),
            "Unable to get deploy contract state");
    byte[] deployAbiBytes = deployContract.getAbi();
    byte[] deployState = deployContract.getSerializedContract();
    ContractAbi deployAbi = new AbiParser(deployAbiBytes).parseAbi().contract();

    FileAbi abiToBeDeployed = new AbiParser(abiBytes).parseAbi();
    int binderId = determineBinderId(abiToBeDeployed, deployAbi, deployState);

    return createUpgradeRpc(deployAbi, binderId);
  }

  private byte[] createUpgradeRpc(ContractAbi deployAbi, int binderId) {
    RpcContractBuilder fnBuilder = new RpcContractBuilder(deployAbi, "upgradeContract");
    fnBuilder.addAddress(contractAddress);
    fnBuilder.addI32(binderId);
    fnBuilder.addVecU8(contractBytes);
    fnBuilder.addVecU8(abiBytes);
    fnBuilder.addVecU8(upgradeRpc);

    return fnBuilder.getBytes();
  }

  /**
   * Build the signed transaction of the upgrade call.
   *
   * @return the signed transaction of the upgrade call.
   */
  public SignedTransaction build() {
    byte[] bytes = buildRpc();
    return ExceptionConverter.call(
        () ->
            blockchainClient.sign(
                Transaction.create(DeploymentBuilder.DEFAULT_PUB_ADDRESS, bytes), gasCost));
  }

  /**
   * Build and send the upgrade signed transaction to the pub-deploy contract.
   *
   * @return the executed transaction tree of the transaction.
   */
  public TransactionTree upgrade() {
    SignedTransaction signedTransaction = build();
    SentTransaction sentTransaction =
        ExceptionConverter.call(
            () -> blockchainClient.send(signedTransaction), "Failed to send upgrade transaction");
    TransactionTree transactionTree = blockchainClient.waitForSpawnedEvents(sentTransaction);
    if (transactionTree.hasFailures()) {
      throw new TransactionException(transactionTree);
    }
    return transactionTree;
  }

  private void validateRequiredParameters() {
    if (contractBytes == null) {
      throw new RuntimeException("Missing contract bytes");
    }
    if (abiBytes == null) {
      throw new RuntimeException("Missing abi bytes");
    }
    if (contractAddress == null) {
      throw new RuntimeException("Missing contract address");
    }
  }

  private int determineBinderId(
      FileAbi abiToBeDeployed, ContractAbi deployAbi, byte[] deployState) {

    Map<Integer, DeploymentBuilder.VersionInterval> binderVersions =
        DeploymentBuilder.getSupportedBinderVersions(deployState, deployAbi);

    AbiVersion binderVersion = abiToBeDeployed.versionBinder();
    return DeploymentBuilder.findBinderId(binderVersions, binderVersion);
  }
}
