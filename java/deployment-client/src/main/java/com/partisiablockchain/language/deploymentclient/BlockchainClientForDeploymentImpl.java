package com.partisiablockchain.language.deploymentclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SentTransaction;
import com.partisiablockchain.api.transactionclient.SignedTransaction;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.TransactionTree;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.secata.tools.coverage.ExceptionConverter;

/**
 * Default implementation of {@link BlockchainClientForDeployment} using {@link
 * BlockchainTransactionClient} and {@link ChainControllerApi}.
 */
public final class BlockchainClientForDeploymentImpl implements BlockchainClientForDeployment {
  private final BlockchainTransactionClient transactionClient;
  private final ChainControllerApi chainController;

  /**
   * Constructor for {@link BlockchainClientForDeploymentImpl}.
   *
   * @param transactionClient the underlying transaction client
   * @param chainController the underlying chainController
   */
  public BlockchainClientForDeploymentImpl(
      BlockchainTransactionClient transactionClient, ChainControllerApi chainController) {
    this.transactionClient = transactionClient;
    this.chainController = chainController;
  }

  @Override
  public SignedTransaction sign(Transaction transaction, long gasCost) {
    return ExceptionConverter.call(() -> transactionClient.sign(transaction, gasCost));
  }

  @Override
  public SentTransaction send(SignedTransaction signedTransaction) {
    return ExceptionConverter.call(() -> transactionClient.send(signedTransaction));
  }

  @Override
  public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
    return transactionClient.waitForSpawnedEvents(sentTransaction);
  }

  @Override
  public Contract getContract(BlockchainAddress address) throws ApiException {
    return chainController.getContract(address.writeAsString(), null);
  }
}
