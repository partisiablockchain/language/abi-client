package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.BinaryAbiWriter;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ExecutableFunction;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The ABI generator used in the cargo partisia-contract tool to generate an ABI from a compiled
 * WASM contract. It is using the protocol defined by the contract SDK when compiling with the
 * <code>abi</code> feature. The protocol itself is based on raw function pointers in the compiled
 * WASM.
 *
 * <p>The protocol to generate the ABI file is described in the contract SDKs README.md file, and
 * each of the functions to use have documented their input and output in the rust documentation.
 */
public final class AbiGenerator {

  /**
   * Full names and prefixes of the functions that are exported in a contract by the contract SDK
   * when compiled with the "abi" feature.
   */
  private interface AbiGenInterface {
    /**
     * Original function to generate ABI and write it to memory. Takes two lists of function
     * pointers to functions that generate ABI for named types and actions in the contract.
     *
     * <p>Deprecated use {@value GENERATE_ABI_FROM_STATE_FN_NAME} instead if present in the wasm
     * file.
     */
    String GENERATE_ABI_FN_NAME = "__abi_generate_to_ptr";

    /**
     * Updated function to generate ABI and write it to memory. Should be used instead of {@value
     * GENERATE_ABI_FN_NAME} if present in wasm file.
     *
     * <p>Takes a single list of function pointers to generate ABI for contract actions.
     */
    String GENERATE_ABI_FROM_STATE_FN_NAME = "__abi_state_generate_to_ptr";

    /**
     * Function to allocate a vector based buffer in memory, used to store input to the abi
     * generating functions.
     */
    String MALLOC_FN_NAME = "__abi_malloc";

    /**
     * Prefix to function that return pointer to a function that can generate the ABI for a named
     * type in the contract.
     *
     * <p>Deprecated and only used if {@value GENERATE_ABI_FN_NAME} is the only ABI generating
     * function present.
     */
    String TYPE_ABI_GEN_FN_PTR_PREFIX = "__abi_type_as_fn_ptr";

    /**
     * Prefix to function that return pointer to a function that can generate the ABI for a function
     * in the contract.
     */
    String FN_ABI_GEN_FN_PTR_PREFIX = "__abi_fn_as_fn_ptr";

    /**
     * Name pattern for the exported functions that returns pointers to fn abi generating functions.
     */
    Pattern FN_ABI_GEN_FN_PTR_PATTERN =
        Pattern.compile("^__abi_fn_as_fn_ptr___abi_fn_(\\d+)_(\\p{XDigit}+)_");
  }

  /** Sort order for abi compiled with the older `__abi_generate_to_ptr`. */
  static final Comparator<ExecutableFunction> SORTED_BY_INDEX =
      Comparator.comparingInt(o -> o.index().asInt());

  private final WasmInstance instance;
  private final AbiGenSpec abiGenerateSpec;

  AbiGenerator(WasmInstance instance, AbiGenSpec generateSpec) {
    this.instance = instance;
    this.abiGenerateSpec = generateSpec;
  }

  /**
   * Load the WASM file, generate and write the ABI to a file.
   *
   * @param wasmFile the path to the input wasm file.
   * @param output the path to the output location of the generated abi.
   */
  static void generate(Path wasmFile, Path output) throws IOException {
    WasmParser parser = new WasmParser(Files.readAllBytes(wasmFile));
    WasmModule module = parser.parse();
    WasmInstance instance = WasmInstance.forModule(module);

    AbiGenSpec spec = determineAbiGenerateSpec(instance);
    AbiGenerator generator = new AbiGenerator(instance, spec);
    generator.readWasmAndWriteAbi(output);
  }

  /**
   * Determine, from the names of the exported functions in the WASM file, how the ABI should be
   * generated for the contract.
   *
   * @param instance executable WASM used to inspect the available functions
   * @return specification for how to generate the ABI file
   */
  private static AbiGenSpec determineAbiGenerateSpec(WasmInstance instance) {
    if (hasExportedFunction(instance, AbiGenInterface.GENERATE_ABI_FROM_STATE_FN_NAME)) {
      // The WASM file contains the exported `__abi_state_generate_to_ptr` function.
      // As stated in the protocol in the contract SDK the function expects a single list of
      // pointers to fn abi generating functions as argument.
      // Further, the functions should be sorted by first the identifier of the function kind, and
      // next by their shortname in ascending order.
      return new AbiGenSpec(
          AbiGenInterface.GENERATE_ABI_FROM_STATE_FN_NAME,
          List.of(AbiGenInterface.FN_ABI_GEN_FN_PTR_PREFIX),
          AbiGenerator::compareFnKindAndShortname);
    } else if (hasExportedFunction(instance, AbiGenInterface.GENERATE_ABI_FN_NAME)) {
      // The WASM file did not contain `__abi_state_generate_to_ptr`, but had the older
      // `__abi_generate_to_ptr` only.
      // In addition to the list of pointers to fn abi generation functions, it also expects a list
      // of pointers to functions that can generate the abi for named types.
      // No specific order is assumed for the functions, so they are sorted by their index in the
      // wasm file.
      return new AbiGenSpec(
          AbiGenInterface.GENERATE_ABI_FN_NAME,
          List.of(
              AbiGenInterface.FN_ABI_GEN_FN_PTR_PREFIX, AbiGenInterface.TYPE_ABI_GEN_FN_PTR_PREFIX),
          SORTED_BY_INDEX);
    } else {
      // The WASM file did not contain either of the expected functions, and where most likely not
      // compiled with the "abi".
      throw new RuntimeException("The given WASM file was not compiled with the ABI feature.");
    }
  }

  private static int compareFnKindAndShortname(ExecutableFunction o1, ExecutableFunction o2) {
    KindIdAndShortname first = KindIdAndShortname.from(o1);
    KindIdAndShortname second = KindIdAndShortname.from(o2);
    return FN_ABI_SORT_ORDER_KIND_ID_AND_SHORTNAME.compare(first, second);
  }

  private static final Comparator<KindIdAndShortname> FN_ABI_SORT_ORDER_KIND_ID_AND_SHORTNAME =
      Comparator.comparingInt(KindIdAndShortname::kindId)
          .thenComparing(KindIdAndShortname::shortname);

  private record KindIdAndShortname(int kindId, String shortname) {
    private static KindIdAndShortname from(ExecutableFunction function) {
      Matcher matcher = AbiGenInterface.FN_ABI_GEN_FN_PTR_PATTERN.matcher(function.name());
      if (!matcher.find()) {
        throw new RuntimeException("Function names does not match expected pattern");
      } else {
        return new KindIdAndShortname(Integer.parseInt(matcher.group(1)), matcher.group(2));
      }
    }
  }

  private static boolean hasExportedFunction(WasmInstance instance, String name) {
    for (ExecutableFunction function : instance.getExportedFunctions()) {
      if (function.name().equals(name)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Generates the ABI bytecode and writes to the specified file.
   *
   * @param output the path to the output file
   * @throws IOException if an I/ O error occurs writing to or creating the file
   */
  private void readWasmAndWriteAbi(Path output) throws IOException {
    byte[] rawAbi = generateAbi();
    FileAbi sortedAbi = new AbiParser(rawAbi, AbiParser.Strictness.SORTING).parseAbi();
    byte[] sortedAbiBytes = BinaryAbiWriter.serialize(sortedAbi);
    Files.write(output, sortedAbiBytes);
  }

  /**
   * Builds an arguments list, and executes the corresponding function in the WASM file to generate
   * the contract ABI file.
   *
   * @return ABI byte code
   */
  private byte[] generateAbi() {
    List<Literal> arguments = new ArrayList<>();
    for (String prefix : abiGenerateSpec.argumentFunctionPrefixes()) {
      List<Int32> functionPointers = createFunctionPointerList(prefix);
      Int32 memoryPointer = mallocInGuest(sizeOfInts(functionPointers));

      putInMemory(memoryPointer.value(), intsToBytesLe(functionPointers));

      arguments.add(new Int32(functionPointers.size()));
      arguments.add(memoryPointer);
    }

    // Call the abi generating method in the WASM file.
    // The method generates an ABI within the WASM memory and returns a pointer+length to said ABI.
    List<Literal> results =
        instance.runFunction(abiGenerateSpec.abiGenerateFunctionName(), Long.MAX_VALUE, arguments);

    long pairOfAddressAndLen = results.get(0).expectInt64().value();

    // Extract the address and length
    long address = pairOfAddressAndLen & 0x7FFFFFFF;
    long length = (pairOfAddressAndLen >> 32) & 0x7FFFFFFF;

    // The whole ABI is at the memory address.
    return instance
        .getMemory(new Uint31(0))
        .readRange(new Uint31((int) address), new Uint31((int) length));
  }

  /**
   * Create a list of function pointers to exported function in the WASM file.
   *
   * @param prefix function prefix to filter on
   * @return list of filtered and sorted functions
   */
  List<Int32> createFunctionPointerList(String prefix) {
    return instance.getExportedFunctions().stream()
        .filter(func -> func.name().startsWith(prefix))
        .sorted(abiGenerateSpec.functionComparator())
        .map(this::callFunctionThatReturnsWasmPointer)
        .toList();
  }

  /**
   * Calls a function <code>() -> i32</code>. The result is assumed to be a valid function pointer.
   * An exception is thrown if the function does not have the correct signature.
   *
   * @param function the function to call.
   * @return the returned value from the function.
   */
  private Int32 callFunctionThatReturnsWasmPointer(ExecutableFunction function) {
    List<Literal> results = instance.runFunction(function.name(), Long.MAX_VALUE, List.of());
    Literal literal = results.get(0);
    return literal.expectInt32();
  }

  /**
   * Allocates a buffer within the WASM interpreter. It utilizes the <code>__abi_malloc</code> Rust
   * method.
   *
   * @param size the size allocated.
   * @return pointer to allocated buffer.
   */
  private Int32 mallocInGuest(int size) {
    List<Literal> result =
        instance.runFunction(
            AbiGenInterface.MALLOC_FN_NAME,
            Long.MAX_VALUE,
            List.of(Int32.fromUnsignedValue(new Uint31(size))));
    return result.get(0).expectInt32();
  }

  /**
   * Write the buffer to an absolute memory location in the WASM interpreter.
   *
   * @param offset the offset in the wasm memory.
   * @param data the data to write.
   */
  private void putInMemory(int offset, byte[] data) {
    WasmMemory memory = instance.getMemory(new Uint31(0));
    memory.writeRange(new Uint31(offset), data);
  }

  /**
   * Convert a list of {@link Int32} to a flattened byte array containing their byte representation
   * in little-endian.
   *
   * @param ints the list of ints to convert.
   * @return flattened byte array containing byte representation in little-endian.
   */
  private byte[] intsToBytesLe(List<Int32> ints) {
    byte[] result = new byte[sizeOfInts(ints)];
    for (int i = 0; i < ints.size(); i++) {
      Int32 anInt = ints.get(i);
      byte[] intBytes = anInt.toBytes();
      System.arraycopy(intBytes, 0, result, i * 4, 4);
    }
    return result;
  }

  static int sizeOfInts(List<Int32> actions) {
    return actions.size() * 4;
  }

  /**
   * Specification for which abi generating function to call, how many arguments is should be called
   * with, and how those arguments should be sorted.
   *
   * @param abiGenerateFunctionName the name of the exported function to use for generating the abi
   * @param argumentFunctionPrefixes prefixes of the exported functions to use as arguments in lists
   *     to the abi generating function
   * @param functionComparator comparator to use when sorting the functions in the argument lists
   */
  private record AbiGenSpec(
      String abiGenerateFunctionName,
      List<String> argumentFunctionPrefixes,
      Comparator<ExecutableFunction> functionComparator) {}
}
