package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import com.partisiablockchain.language.sections.PbcFile;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/** A command line interpreter for running abi code gen. */
public final class CliParser {
  private CliParser() {}

  /**
   * Main method for parsing and executing the parsed command. Uses System.out as output stream to
   * write to terminal.
   *
   * @param args command-line specified arguments.
   */
  public static void main(String[] args) {
    PrintStream out = new PrintStream(System.out);
    PrintStream err = new PrintStream(System.err);
    parseAndRun(out, err, Arrays.asList(args));
  }

  @SuppressWarnings("LeftCurlyNl")
  static void parseAndRun(PrintStream out, PrintStream err, List<String> args) {
    String helpMessage =
        """
        Possible commands are:
            list-of-actions          List actions of an abi.
            from-wasm                Extract ABI from a wasm file.
            wasm-exports             Show exports for a wasm file
        """;

    if (args.size() == 0) {
      err.println("Missing command");
      out.println(helpMessage);
      return;
    }
    var command = args.get(0);
    switch (command) {
      case "from-wasm" -> parseAndRunWasm(out, err, args.subList(1, args.size()));
      case "wasm-exports" -> parseAndPrintExports(out, err, args.subList(1, args.size()));
      case "list-of-actions" -> parseAndRunShow(out, err, args.subList(1, args.size()));
      case "help", "--help", "-h" -> out.println(helpMessage);
      default -> {
        err.println("Unknown command: " + command);
        out.println(helpMessage);
      }
    }
  }

  private static void parseAndRunShow(PrintStream out, PrintStream err, List<String> args) {
    if (args.contains("--help") || args.contains("-h")) {
      out.println(helpMessageShow());
      return;
    }
    Iterator<String> argIter = args.iterator();
    Path abiPath = null;
    var strictness = AbiParser.Strictness.STRICT;
    try {
      while (argIter.hasNext()) {
        var arg = argIter.next();
        if (arg.equals("--lenient")) {
          strictness = AbiParser.Strictness.LENIENT;
        } else {
          if (abiPath != null) {
            throw new CliParseException("Cannot give input twice");
          }
          abiPath = Path.of(arg);
        }
      }
      if (abiPath == null) {
        throw new CliParseException(
            "show must be provided at least 1 argument, the path to either a .abi- or .pbc-file");
      }

      var abi = new AbiParser(readAbiBytes(abiPath), strictness).parseAbi();
      out.println(actionString((ContractAbi) abi.chainComponent()));
    } catch (Exception e) {
      err.println(e);
      if (e instanceof CliParseException) {
        out.println(helpMessageShow());
      }
    }
  }

  private static byte[] readAbiBytes(Path inputFilePath) {
    if (inputFilePath.toString().endsWith(".pbc")) {
      return ExceptionConverter.call(
          () -> PbcFile.fromBytes(Files.readAllBytes(inputFilePath)).getAbiBytes(),
          "unable to read input file: " + inputFilePath);
    }

    if (inputFilePath.toString().endsWith(".abi")) {
      return ExceptionConverter.call(
          () -> Files.readAllBytes(inputFilePath), "unable to read input file: " + inputFilePath);
    }
    throw new RuntimeException("Input file must either be an abi file or a pbc file");
  }

  private static String actionString(ContractAbi contractAbi) {
    List<ContractInvocation> contractCalls = contractAbi.contractInvocations();

    StringBuilder result = new StringBuilder();
    for (ContractInvocation function : contractCalls) {
      ContractInvocationKind kind = function.kind();

      result.append(fnKindToCallString(kind));
      if (kind.hasShortname()) {
        result.append("_").append(function.shortnameAsString());
      }
      result.append(":").append(function.name()).append("\n");
      // Adding init twice to make it work with old contracts
      if (kind.kindId() == ImplicitBinderAbi.DefaultKinds.INIT.kindId()) {
        result.append("init:").append(function.name()).append("\n");
      }
    }
    return result.toString();
  }

  static String fnKindToCallString(ContractInvocationKind fn) {
    return fn.name().toLowerCase(Locale.ENGLISH);
  }

  private static String helpMessageShow() {
    return """
           USAGE:
               java -jar abi-client.jar list-of-actions [options]* <input-path>

           ARGS:
               <input-path>

           OPTIONS:
               --lenient          Allow invalid identifiers
               -h, --help         Print help information""";
  }

  /**
   * Parse arguments and print exports from wasm file.
   *
   * @param out the standard output
   * @param err the error output
   * @param args the arguments
   */
  private static void parseAndPrintExports(PrintStream out, PrintStream err, List<String> args) {
    if (args.contains("--help") || args.contains("-h")) {
      out.println(helpMessageExports());
      return;
    }
    if (args.size() < 1) {
      err.println("Missing arguments. Requires an input argument.");
      return;
    }
    Path wasmFile = Path.of(args.get(0));
    if (Files.notExists(wasmFile)) {
      err.printf("Could not find ABI file at %s", wasmFile);
      return;
    }

    ExceptionConverter.run(
        () -> {
          WasmParser parser = new WasmParser(Files.readAllBytes(wasmFile));
          WasmModule module = parser.parse();
          module
              .exportSection()
              .iterator()
              .forEachRemaining(export -> out.println(export.getName()));
        });
  }

  /**
   * Create help message for exports subcommand.
   *
   * @return help message for exports subcommand
   */
  static String helpMessageExports() {
    return """
           USAGE:
               java -jar abi-client.jar exports [options]* <input-path>

           ARGS:
               <input-path>                  Wasm file

           OPTIONS:
               -h, --help                    Print help information""";
  }

  /**
   * Parse arguments and generate ABI from Wasm.
   *
   * @param out the standard output
   * @param err the error output
   * @param args the arguments
   */
  private static void parseAndRunWasm(PrintStream out, PrintStream err, List<String> args) {
    if (args.contains("--help") || args.contains("-h")) {
      out.println(helpMessageWasm());
      return;
    }

    if (args.size() < 2) {
      err.println("Missing arguments. Requires an input and an output argument.");
      return;
    }

    Path wasmInput = Path.of(args.get(0));
    Path abiOutput = Path.of(args.get(1));
    ExceptionConverter.run(() -> AbiGenerator.generate(wasmInput, abiOutput));
  }

  /**
   * Create help message for wasm subcommand.
   *
   * @return help message for wasm subcommand
   */
  static String helpMessageWasm() {
    return """
           USAGE:
               java -jar abi-client.jar wasm [options]* <input-path> <output-path>

           ARGS:
               <input-path>                  Wasm file
               <output-path>                 Abi output

           OPTIONS:
               -h, --help                    Print help information""";
  }
}
