package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test utility for converting WASM text format to WASM binary format.
 *
 * <p>The converter uses the wat2wasm commandline tool from The WebAssembly Binary Toolkit.
 *
 * <p>The converter uses hash-based file caching for the translated. This ensures that the tests can
 * run without installing the wat2wasm tool. Whenever a developer modifies or adds new tests the
 * generated WASM-files must be added and committed.
 *
 * @see <a href="https://github.com/WebAssembly/wabt">WABT: The WebAssembly Binary Toolkit</a>
 */
public final class Wat2Wasm {

  private static final Logger logger = LoggerFactory.getLogger(Wat2Wasm.class);

  /** The name of the executable 'wasm2wat' program. */
  public static final String WASM2WAT_PROGRAM = "wat2wasm";

  /** Private constructor because class is never instantiated. */
  private Wat2Wasm() {}

  /**
   * Get the program version.
   *
   * @return The version reported by commandline option '--version'
   */
  public static String version() {
    return executeCommandline(new String[] {WASM2WAT_PROGRAM, "--version"}).trim();
  }

  /**
   * Translate from WASM text format to the WASM binary format.
   *
   * @param wasmTextStream Stream with WASM program in text format.
   * @param testClazz The calling test class. Used as a folder-name for storing the hash-based
   *     cache-files.
   * @return Path to WASM program in binary format.
   * @throws RuntimeException if the translation fails.
   */
  public static Path translate(InputStream wasmTextStream, Class<?> testClazz) {
    return ExceptionConverter.call(
        () ->
            translate(new String(wasmTextStream.readAllBytes(), StandardCharsets.UTF_8), testClazz),
        "Error reading WASM file");
  }

  /**
   * Translate from WASM text format to the WASM binary format.
   *
   * @param wasmText WASM program in text format.
   * @param testClazz The calling test class. Used as a folder-name for storing the hash-based
   *     cache-files.
   * @return Path to WASM program in binary format.
   * @throws RuntimeException if the translation fails.
   */
  public static Path translate(CharSequence wasmText, Class<?> testClazz) {
    // Load caches WASM bytes if present
    Path cachedFile = loadCachedWasm(wasmText, testClazz);
    if (cachedFile != null) {
      return cachedFile;
    }
    // Write WAT to tmp-file
    File watFile = writeTempFile(wasmText);
    // Convert to WASM binary by calling command line tool
    final String wasmFileName = getCacheFile(wasmText, testClazz).toString();
    executeCommandline(
        new String[] {
          WASM2WAT_PROGRAM,
          "--no-check",
          "--debug-names",
          watFile.getAbsolutePath(),
          "-o",
          wasmFileName
        });
    logger.info("Saved binary WASM to cache file '{}'.", wasmFileName);
    return Path.of(wasmFileName);
  }

  /**
   * Load cached WASM byte format from a file based on a hash of the WASM text format.
   *
   * @param wasmText The WASM text format.
   * @param testClazz The calling test class. Used as a folder-name for storing the hash-based
   *     cache-files.
   * @return The cached WASM path if found. null otherwise.
   */
  private static Path loadCachedWasm(CharSequence wasmText, Class<?> testClazz) {
    Path cacheFile = getCacheFile(wasmText, testClazz);
    if (Files.exists(cacheFile)) {
      return cacheFile;
    } else {
      logger.info(
          "Failed to load cached binary WASM file '{}'. Will attempt to generate using '{}'.",
          cacheFile,
          WASM2WAT_PROGRAM);
      return null;
    }
  }

  /**
   * Construct the file used for caching WASM bytes.
   *
   * @param wasmText The WASM text format.
   * @param testClazz The calling test class. Used as a folder-name for storing the hash-based
   *     cache-files.
   * @return The file used for caching the WASM bytes.
   */
  private static Path getCacheFile(CharSequence wasmText, Class<?> testClazz) {
    String wasmTextWithoutCr = wasmText.toString().replace("\r", "");
    final String wasmHash = Integer.toHexString(wasmTextWithoutCr.hashCode());
    final Path cacheFolder = nextToTestSources(testClazz);
    return ExceptionConverter.call(
        () -> {
          Path dir = Files.createDirectories(cacheFolder);
          return dir.resolve(wasmHash + ".wasm").toAbsolutePath();
        },
        "Directory structure error");
  }

  /**
   * Place the cache files next to the test source code.
   *
   * @param clazz The test class.
   * @return The path of the folder to place cache files in.
   */
  private static Path nextToTestSources(Class<?> clazz) {
    String[] split = clazz.getPackageName().split("\\.");
    Path referencePathDir = Paths.get("./src/test/java/", split);
    return referencePathDir.resolve(clazz.getSimpleName());
  }

  /**
   * Write string to temporary text file.
   *
   * @param wasmText The text to write
   * @return The new temporary file containing the text.
   * @throws RuntimeException if any error occurs during writing
   */
  private static File writeTempFile(CharSequence wasmText) {
    return ExceptionConverter.call(
        () -> {
          File watFile = File.createTempFile("wasm-", ".wat");
          BufferedWriter writer =
              new BufferedWriter(new FileWriter(watFile, StandardCharsets.UTF_8));
          writer.append(wasmText);
          writer.close();
          return watFile;
        },
        "Error creating temporary file.");
  }

  /**
   * Execute commandline command with arguments.
   *
   * @param cmdarray array containing the command to call and its arguments.
   * @return The output that the program printed to standard output
   * @throws RuntimeException if any error occurs during execution or the exit code is not 0.
   */
  private static String executeCommandline(String[] cmdarray) {
    Process process =
        ExceptionConverter.call(
            () -> Runtime.getRuntime().exec(cmdarray),
            MessageFormat.format(
                "Cannot execute \"{0}\". "
                    + "The WebAssembly Binary Toolkit must be installed and in the PATH.",
                cmdarray[0]));
    // Wait for the process to exit
    try {
      process.waitFor();
      String stdout = new String(process.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
      String stderr = new String(process.getErrorStream().readAllBytes(), StandardCharsets.UTF_8);
      // Check that it succeeded
      if (process.exitValue() != 0) {
        throw new RuntimeException(
            MessageFormat.format(
                "Error from \"{0}\".\nExit code: {1}\n{2}",
                cmdarray[0], process.exitValue(), stderr));
      }
      return stdout;
    } catch (InterruptedException | IOException e) {
      throw new RuntimeException(MessageFormat.format("Error executing \"{0}\"", cmdarray[0]));
    }
  }
}
