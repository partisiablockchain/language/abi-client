package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

final class ConvertAbiCliParserTest {

  final String pbcPath = "src/test/resources/com/partisiablockchain/language/abicli/";
  Path tmpDir;

  @BeforeEach
  void setUp() throws IOException {
    tmpDir = Files.createTempDirectory(Path.of(pbcPath), "tmp");
  }

  @AfterEach
  void tearDown() {
    deleteDirectory(tmpDir.toFile());
  }

  private static void deleteDirectory(File dir) {
    File[] contents = dir.listFiles();
    if (contents != null) {
      for (File file : contents) {
        deleteDirectory(file);
      }
    }
    dir.delete();
  }
}
