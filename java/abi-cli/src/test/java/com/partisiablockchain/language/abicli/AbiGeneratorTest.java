package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.parser.AbiParseException;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.FunctionInvalid;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class AbiGeneratorTest {

  @TempDir Path temp;

  /**
   * Smoke test that the ABI of a contract that only exports the "__abi_generate_to_ptr" abi
   * generating function can be generated and parsed.
   */
  @Test
  void generateAbiFromTypesAndFunctionLists() throws Exception {
    Path wasm = Path.of(getClass().getResource("voting.wasm").toURI());
    Path out = temp.resolve("actual.abi");

    Path abiGeneratedByRust = Path.of(getClass().getResource("voting.abi").toURI());
    byte[] expectedBytes = Files.readAllBytes(abiGeneratedByRust);

    AbiGenerator.generate(wasm, out);

    byte[] actual = Files.readAllBytes(out);
    Assertions.assertThat(actual).containsExactly(expectedBytes);
  }

  /**
   * Smoke test that the ABI of a contract that exports the "__abi_state_generate_to_ptr" generating
   * function, can be generated and parsed.
   */
  @Test
  void generateAbiFromFunctionListOnly() throws Exception {
    Path wasm = loadWasmTestFile("zk_second_price.wat");
    Path out = temp.resolve("actual.abi");
    Path abiGeneratedByRust = Path.of(getClass().getResource("zk_second_price.abi").toURI());
    byte[] expectedBytes = Files.readAllBytes(abiGeneratedByRust);

    AbiGenerator.generate(wasm, out);
    byte[] actual = Files.readAllBytes(out);
    Assertions.assertThat(actual).containsExactly(expectedBytes);
  }

  /**
   * Smoke test that the ABI generation will sort named types when creating an abi with binder
   * information.
   */
  @Test
  void generateAbiIncludingBinderAbi() throws Exception {
    Path wasm = loadWasmTestFile("zk_second_price_v6.wat");
    Path out = temp.resolve("actual.abi");
    Path abiGeneratedByRust = Path.of(getClass().getResource("zk_second_price_v6.abi").toURI());
    byte[] expectedBytes = Files.readAllBytes(abiGeneratedByRust);

    AbiGenerator.generate(wasm, out);
    byte[] actual = Files.readAllBytes(out);
    new AbiParser(actual).parseAbi();
    Assertions.assertThat(actual).containsExactly(expectedBytes);
  }

  /**
   * If the AbiGenerator is unable to pattern match on the exported function names that generate
   * function ABIs an exception is thrown.
   */
  @Test
  void exportedNamesForFnAbiGeneratingFunctionsMustBePatternMatchable() {
    List<String> badWatFiles =
        List.of(
            "zk_second_price_bad_function_kind.wat",
            "zk_second_price_bad_init_fn_shortname.wat",
            "zk_second_price_bad_add_bid_shortname.wat",
            "zk_second_price_bad_compute_complete_shortname.wat",
            "zk_second_price_bad_register_bidder_shortname.wat");
    for (String badWatFile : badWatFiles) {
      Path wasm = loadWasmTestFile(badWatFile);
      Path out = temp.resolve("actual.abi");

      Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Function names does not match expected pattern");
    }
  }

  /**
   * If the exported function name to generate the ABI is not named as expected in the WASM file an
   * exception is thrown.
   */
  @Test
  void generateFailsIfTheAbiGenerateFunctionCannotBeFound() {
    Path wasm = loadWasmTestFile("zk_second_price_bad_abi_generate_function_name.wat");
    Path out = temp.resolve("actual.abi");

    Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("The given WASM file was not compiled with the ABI feature.");
  }

  /**
   * When a contract contains a new function kind identifier the generator can pattern match and
   * sort on the function kind id, but parsing the function kind fails.
   */
  @Test
  void generateCanHandleNewFunctionKindsButParsingTheNewKindFails() {
    Path wasm = loadWasmTestFile("ping_contract_new_function_kind.wat");
    Path out = temp.resolve("actual.abi");

    Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
        .isInstanceOf(AbiParseException.class)
        .hasMessageContaining("Unsupported FnKind type 0x99 specified");
  }

  /**
   * If the wasm code reverses the list of functions in the ContractAbi an exception is thrown when
   * the generated ABI is parsed.
   */
  @Test
  void generateThrowsExceptionIfGeneratedAbiIsBadlyFormatted() {
    Path wasm = loadWasmTestFile("ping_contract_bad_abi_gen.wat");
    Path out = temp.resolve("actual.abi");

    Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
        .isInstanceOf(AbiParseException.class)
        .hasMessageContaining("Functions are unordered");
  }

  /**
   * When the FnKind identifier does not match actual FnKind identifier of the ABI the function is
   * sorted incorrectly and an exception is thrown when trying to parse the generated ABI.
   */
  @Test
  void generateThrowsExceptionIfTheExportedFnKindIdDoesNotMatchKindIdInAbi() {
    Path wasm = loadWasmTestFile("zk_second_price_with_mismatching_fn_kind_id.wat");
    Path out = temp.resolve("actual.abi");

    Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Functions are unordered");
  }

  /**
   * When the inputted WASM contract has not been compiled with the "abi" feature an exception is
   * thrown.
   */
  @Test
  void failToGenerate() throws Exception {
    Path wasm = Path.of(getClass().getResource("voting-no-abi.wasm").toURI());
    Path out = temp.resolve("actual.abi");

    Assertions.assertThatThrownBy(() -> AbiGenerator.generate(wasm, out))
        .hasMessage("The given WASM file was not compiled with the ABI feature.");
  }

  @Test
  void sizeOfInts() {
    int size = AbiGenerator.sizeOfInts(List.of(Int32.fromBoolean(false)));
    Assertions.assertThat(size).isEqualTo(4);
  }

  @DisplayName("ABIs compiled with the older `__abi_generate_to_ptr` are sorted by index.")
  @Test
  void oldSortedByIndex() {
    // Test for fixing flaky pitest
    List<FunctionInvalid> functions =
        List.of(
            new FunctionInvalid(new Uint31(1), "nameA", null, null),
            new FunctionInvalid(new Uint31(0), "nameB", null, null));
    List<FunctionInvalid> sortedList =
        functions.stream().sorted(AbiGenerator.SORTED_BY_INDEX).toList();
    Assertions.assertThat(sortedList.get(0).name()).isEqualTo("nameB");
    Assertions.assertThat(sortedList.get(1).name()).isEqualTo("nameA");
  }

  private static Path loadWasmTestFile(String wasmTextFileName) {
    return ExceptionConverter.call(
        () -> {
          InputStream wasmTextStream = AbiGeneratorTest.class.getResourceAsStream(wasmTextFileName);
          return Wat2Wasm.translate(wasmTextStream, AbiGeneratorTest.class);
        },
        "Error reading WASM file.");
  }
}
