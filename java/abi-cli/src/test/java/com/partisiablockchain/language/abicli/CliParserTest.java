package com.partisiablockchain.language.abicli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abicli.CliParser.fnKindToCallString;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class CliParserTest {

  @TempDir Path temp;

  @Test
  void testMain() {
    String[] args = {"--help"};
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));

    CliParser.main(args);

    assertThat(
            """
            Possible commands are:
                list-of-actions          List actions of an abi.
                from-wasm                Extract ABI from a wasm file.
                wasm-exports             Show exports for a wasm file""")
        .isEqualTo(outputStream.toString(StandardCharsets.UTF_8).trim());

    System.setOut(System.out);
  }

  @Test
  void missingCommand() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of());
    assertThat(err.toString(StandardCharsets.UTF_8)).contains("Missing command");
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(helpMessageCommand());
  }

  @Test
  void unknownCommand() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of("SomeCommand"));
    assertThat(err.toString(StandardCharsets.UTF_8)).contains("Unknown command: SomeCommand");
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(helpMessageCommand());
  }

  @Test
  void helpCommand() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out1), new PrintStream(err), List.of("help"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out2), new PrintStream(err), List.of("--help"));
    ByteArrayOutputStream out3 = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out3), new PrintStream(err), List.of("-h"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8)).contains(helpMessageCommand());
    assertThat(out2.toString(StandardCharsets.UTF_8)).contains(helpMessageCommand());
    assertThat(out3.toString(StandardCharsets.UTF_8)).contains(helpMessageCommand());
  }

  @Test
  void printWrongNoArgs() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of("list-of-actions"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains(
            "com.partisiablockchain.language.abicli.CliParseException: show must be provided"
                + " at least 1 argument, the path to either a .abi- or .pbc-file");
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(helpMessageShow());
  }

  @Test
  void printWrongTooManyArgs() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of("list-of-actions", "arg1", "arg2", "arg3"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains(
            "com.partisiablockchain.language.abicli.CliParseException: Cannot give input"
                + " twice");
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(helpMessageShow());
  }

  @Test
  void printWrongArg() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of("list-of-actions", "does/not/exist.abi"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains(
            "java.lang.RuntimeException: unable to read input file: does%snot%sexist.abi"
                .formatted(File.separator, File.separator));
    assertThat(out.size()).isEqualTo(0);
  }

  @Test
  void printNonExistingFile() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of("wasm-exports", "invalid_file_name.wasm"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains("Could not find ABI file at invalid_file_name.wasm");
  }

  @Test
  void printNonAbiOrPbc() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of("list-of-actions", "does/not/exist.file"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains("Input file must either be an abi file or a pbc file");
    assertThat(out.size()).isEqualTo(0);
  }

  @Test
  void showLenient() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "--lenient",
            "src/test/resources/testdata/invalid_syntax_or_semantics/"
                + "contract_invalid_identifier_action.pbc"));
    System.out.println(err);
    assertThat(err.size()).isEqualTo(0);
  }

  @Test
  void showHelp() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out1), new PrintStream(err), List.of("list-of-actions", "--help"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out2), new PrintStream(err), List.of("list-of-actions", "-h"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8)).contains(helpMessageShow());
    assertThat(out2.toString(StandardCharsets.UTF_8)).contains(helpMessageShow());
  }

  @Test
  void showActions() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out1),
        new PrintStream(err),
        List.of("list-of-actions", "src/test/resources/testdata/binary/token_contract.abi"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out2),
        new PrintStream(err),
        List.of("list-of-actions", "src/test/resources/testdata/binary/token_contract.abi"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualTo(out2.toString(StandardCharsets.UTF_8));
    assertThat(out1.toString(StandardCharsets.UTF_8)).contains(actionsForTokenAbi());
    assertThat(out2.toString(StandardCharsets.UTF_8)).contains(actionsForTokenAbi());
  }

  @Test
  void showActionsPbc() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out1),
        new PrintStream(err),
        List.of("list-of-actions", "src/test/resources/testdata/binary/token_contract.pbc"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out2),
        new PrintStream(err),
        List.of("list-of-actions", "src/test/resources/testdata/binary/token_contract.pbc"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualTo(out2.toString(StandardCharsets.UTF_8));
    assertThat(out1.toString(StandardCharsets.UTF_8)).contains(actionsForTokenAbi());
    assertThat(out2.toString(StandardCharsets.UTF_8)).contains(actionsForTokenAbi());
  }

  @Test
  void showZkActions() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out1),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "src/test/resources/com/partisiablockchain/language/abicli/zk_actions.abi"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out2),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "src/test/resources/com/partisiablockchain/language/abicli/zk_actions.abi"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualTo(out2.toString(StandardCharsets.UTF_8));

    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualToIgnoringNewLines(actionsForZkTestingActions());
    assertThat(out2.toString(StandardCharsets.UTF_8))
        .isEqualToIgnoringNewLines(actionsForZkTestingActions());
  }

  @Test
  void showActionsContractWithBinderAbi() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "src/test/resources/com/partisiablockchain/language/abicli/average_salary.abi"));
    assertThat(out.toString(StandardCharsets.UTF_8))
        .isEqualToNormalizingNewlines(actionsForContractWithBinderAbi());
  }

  @Test
  void showZkOpenStructActions() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out1),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "src/test/resources/com/partisiablockchain/language/abicli/zk_struct_open.abi"));
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out2),
        new PrintStream(err),
        List.of(
            "list-of-actions",
            "src/test/resources/com/partisiablockchain/language/abicli/zk_struct_open.abi"));

    assertThat(err.size()).isEqualTo(0);
    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualTo(out2.toString(StandardCharsets.UTF_8));
    assertThat(out1.toString(StandardCharsets.UTF_8))
        .isEqualToNormalizingNewlines(actionsForZkOpenStructAbi());
    assertThat(out2.toString(StandardCharsets.UTF_8))
        .isEqualToNormalizingNewlines(actionsForZkOpenStructAbi());
  }

  @Test
  void wasmMissingInput() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of("from-wasm"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains("Missing arguments.")
        .contains("Requires an input and an output argument.");
  }

  @Test
  void wasmHelpMessage() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out), new PrintStream(err), List.of("from-wasm", "--help"));
    assertThat(out.toString(StandardCharsets.UTF_8))
        .contains("USAGE")
        .contains(CliParser.helpMessageWasm());
  }

  @Test
  void wasmShortHelp() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of("from-wasm", "-h"));
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(CliParser.helpMessageWasm());
  }

  @Test
  void wasmGenerate() throws IOException, URISyntaxException {
    Path wasmPath = Path.of(getClass().getResource("voting.wasm").toURI());

    Path abiGeneratedByRust = Path.of(getClass().getResource("voting.abi").toURI());

    Path outputPath = temp.resolve("actual.abi");
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of(
            "from-wasm",
            wasmPath.toAbsolutePath().toString(),
            outputPath.toAbsolutePath().toString()));

    byte[] expectedBytes = Files.readAllBytes(abiGeneratedByRust);
    byte[] actual = Files.readAllBytes(outputPath);
    assertThat(actual).containsExactly(expectedBytes);
  }

  @Test
  void exportsMissingInput() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(new PrintStream(out), new PrintStream(err), List.of("wasm-exports"));
    assertThat(err.toString(StandardCharsets.UTF_8))
        .contains("Missing arguments.")
        .contains("Requires an input argument.");
  }

  @Test
  void exportsHelpMessage() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out), new PrintStream(err), List.of("wasm-exports", "--help"));
    assertThat(out.toString(StandardCharsets.UTF_8))
        .contains("USAGE")
        .contains(CliParser.helpMessageExports());
  }

  @Test
  void exportsShortHelp() {
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    CliParser.parseAndRun(
        new PrintStream(out), new PrintStream(err), List.of("wasm-exports", "-h"));
    assertThat(out.toString(StandardCharsets.UTF_8)).contains(CliParser.helpMessageExports());
  }

  @Test
  void exports() throws IOException, URISyntaxException {
    Path wasmPath = Path.of(getClass().getResource("voting.wasm").toURI());

    ByteArrayOutputStream err = new ByteArrayOutputStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    CliParser.parseAndRun(
        new PrintStream(out),
        new PrintStream(err),
        List.of("wasm-exports", wasmPath.toAbsolutePath().toString()));
    assertThat(out.toString(StandardCharsets.UTF_8)).contains("__PBC_VERSION_CLIENT_5_2_0");
  }

  @Test
  void fnKindUserVariableOpened() {
    // Coverage for deprecated action.
    assertThat(fnKindToCallString(ImplicitBinderAbi.DefaultKinds.ZK_ON_USER_VARIABLES_OPENED))
        .isEqualTo("zk_on_user_variables_opened");
  }

  private static String helpMessageCommand() {
    return """
           Possible commands are:
               list-of-actions          List actions of an abi.
               from-wasm                Extract ABI from a wasm file.
               wasm-exports             Show exports for a wasm file
           """;
  }

  private static String helpMessageShow() {
    return """
           USAGE:
               java -jar abi-client.jar list-of-actions [options]* <input-path>

           ARGS:
               <input-path>

           OPTIONS:
               --lenient          Allow invalid identifiers
               -h, --help         Print help information""";
  }

  private static String actionsForTokenAbi() {
    return """
           init_ffffffff0f:initialize
           init:initialize
           action_caedd5bf02:transfer
           action_d799a3b608:transfer_from
           action_80ad88a707:approve""";
  }

  private static String actionsForZkOpenStructAbi() {
    return """
           init_ffffffff0f:initialize
           init:initialize
           action_00:reset_state
           action_10:open_input
           zk_on_variable_inputted_41:output_variables
           zk_on_compute_complete_42:computation_complete
           zk_on_variables_opened:save_opened_variable
           zk_on_secret_input_40:secret_input

           """;
  }

  private static String actionsForZkTestingActions() {
    return """
           init_ffffffff0f:initialize
           init:initialize
           action_00:some_action
           action_01:to_contract_done
           action_02:start_computation_one_output
           action_0b:start_computation_no_outputs
           action_0c:start_computation_two_outputs
           action_03:new_computation
           action_04:output_complete_delete_variable
           action_05:contract_done
           action_06:transfer_variable_one
           action_07:delete_variable_one
           action_0f:delete_pending_input_one
           action_08:open_variable_one
           action_09:delete_variable_three
           action_0a:verify_one_pending_input
           action_10:verify_one_variable
           action_0d:update_state
           action_11:attest_data
           action_12:start_computation_delete_variable_one
           action_13:open_variable_two
           action_14:open_variable_one_and_two
           action_15:delete_variable_one_and_two
           callback_40:on_callback
           zk_on_variable_inputted_71:zk_on_variable_inputted
           zk_on_variable_rejected:zk_on_variable_rejected
           zk_on_compute_complete_70:zk_on_compute_complete
           zk_on_variables_opened:zk_on_variables_opened
           zk_on_attestation_complete:zk_on_attestation_complete
           zk_on_secret_input_60:secret_input_bit_length_10
           zk_on_secret_input_62:secret_input_bit_length_1
           zk_on_secret_input_63:secret_input_bit_length_1001x1
           zk_on_secret_input_65:secret_input_bit_length_32_32
           zk_on_secret_input_69:secret_input_sealed_single_bit_with_info
           zk_on_external_event:zk_on_external_event

           """;
  }

  private static String actionsForContractWithBinderAbi() {
    return """
           init_ffffffff0f:initialize
           init:initialize
           action_01:compute_average_salary
           zk_on_variable_inputted_41:inputted_variable
           zk_on_compute_complete_42:sum_compute_complete
           zk_on_variables_opened:open_sum_variable
           zk_on_secret_input_40:add_salary

           """;
  }
}
