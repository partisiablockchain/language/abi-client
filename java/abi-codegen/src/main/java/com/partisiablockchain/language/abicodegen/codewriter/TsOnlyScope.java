package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** A scope that is only generate for typescript. */
public final class TsOnlyScope extends Scope {
  final String tsCode;

  /**
   * Constructor.
   *
   * @param tsCode the typescript header
   */
  public TsOnlyScope(String tsCode) {
    this.tsCode = tsCode;
  }

  @Override
  String generateJavaHeader() {
    return "";
  }

  @Override
  String generateTypescriptHeader() {
    return tsCode;
  }

  @Override
  boolean isTsBlock() {
    return true;
  }

  @Override
  String endingDeliminator() {
    return "}";
  }

  @Override
  boolean shouldGenerateJava() {
    return false;
  }
}
