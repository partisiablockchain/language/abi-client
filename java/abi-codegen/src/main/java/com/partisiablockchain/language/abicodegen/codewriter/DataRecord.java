package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegenHelper;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import java.util.List;

/** Data record scope. */
public final class DataRecord extends Scope {
  String name;

  List<NameAndType> fields;
  private final ContractAbi contractAbi;

  /**
   * Constructor.
   *
   * @param name name of the record
   * @param fields fields of the record
   * @param contractAbi contract abi
   */
  public DataRecord(String name, List<NameAndType> fields, ContractAbi contractAbi) {
    this.name = name;
    this.fields = fields;
    this.contractAbi = contractAbi;
  }

  @Override
  String generateJavaHeader() {
    return "public record %s(%s) {"
        .formatted(name, AbiCodegenHelper.javaFields(fields, contractAbi));
  }

  @Override
  String generateTypescriptHeader() {
    return """
           export interface %s {
           %s}
           """
        .formatted(name, AbiCodegenHelper.tsInterfaceFields(fields, contractAbi));
  }

  @Override
  boolean isTsBlock() {
    return false;
  }
}
