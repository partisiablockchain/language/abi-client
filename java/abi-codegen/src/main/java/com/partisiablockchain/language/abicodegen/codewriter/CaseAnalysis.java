package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.stream.Collectors;

/** Case analysis consisting of else-ifs. */
public final class CaseAnalysis extends Statement {
  private final String testValue;
  private final List<Case> cases;
  private final boolean useDotEquals;

  /**
   * Constructor.
   *
   * @param testValue the value that is tests
   * @param cases the different cases
   * @param useDotEquals whether equality should be done using .equals or ==
   */
  public CaseAnalysis(String testValue, List<Case> cases, boolean useDotEquals) {
    this.testValue = testValue;
    this.cases = cases;
    this.useDotEquals = useDotEquals;
  }

  @Override
  String generateJava() {
    String template =
        useDotEquals
            ? """
              if (%s.equals(%s)) {
                return %s;
              }"""
            : """
              if (%s == %s) {
                return %s;
              }""";
    return cases.stream()
        .map(testCase -> template.formatted(testValue, testCase.test, testCase.javaResult))
        .collect(Collectors.joining(" else "));
  }

  @Override
  String generateTypescript() {
    return cases.stream()
        .map(
            testCase ->
                """
                if (%s === %s) {
                  return %s;
                }"""
                    .formatted(testValue, testCase.test, testCase.tsResult))
        .collect(Collectors.joining(" else "));
  }

  /**
   * A singular case.
   *
   * @param test the test of the case
   * @param javaResult the java result of the case
   * @param tsResult the typescript result of the case
   */
  public record Case(String test, String javaResult, String tsResult) {}
}
