package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegenHelper;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.ArrayList;
import java.util.List;

/** Scope containing code. Contains a header line and a block of code. */
public abstract class Scope implements Code {

  List<Code> statements = new ArrayList<>();

  abstract String generateJavaHeader();

  abstract String generateTypescriptHeader();

  /**
   * Add statement to this scope.
   *
   * @param statementOrExpression statement to add
   */
  public void addStatement(Code statementOrExpression) {
    statements.add(statementOrExpression);
  }

  /**
   * Add statement to this scope.
   *
   * @param javaCode java code
   * @param tsCode ts code
   */
  public void addSimpleStatement(String javaCode, String tsCode) {
    addStatement(new SimpleStatement(javaCode, tsCode));
  }

  /**
   * Add statement to this scope.
   *
   * @param statement statement to add
   */
  public void addSimpleStatement(String statement) {
    addStatement(new SimpleStatement(statement, statement));
  }

  /**
   * Add a typescript only statement to this scope.
   *
   * @param tsCode typescript statement to add
   */
  public void addTsOnly(String tsCode) {
    addStatement(new SimpleStatement("", tsCode));
  }

  /**
   * Add a java only statement to this scope.
   *
   * @param javaCode java statement to add
   */
  public void addJavaOnly(String javaCode) {
    addStatement(new SimpleStatement(javaCode, ""));
  }

  /**
   * Add a new scope to this scope.
   *
   * @param scope new scope to add
   * @return the new scope
   */
  public Scope addScope(Scope scope) {
    addStatement(scope);
    return scope;
  }

  /**
   * Write the scope as java code.
   *
   * @param indent the indentation of the scope
   * @return the written code
   */
  public String writeAsJava(int indent) {
    var out = new StringBuilder();
    for (Code statement : statements) {
      if (statement instanceof Scope scope) {
        out.append(scope.generateJavaHeader().indent(indent * 2));
        if (scope.shouldGenerateJava()) {
          if (scope.isJavaBlock()) {
            out.append(scope.writeAsJava(indent + 1));
            out.append(scope.endingDeliminator().indent(indent * 2));
          } else {
            out.append(scope.writeAsJava(indent));
            out.append(System.lineSeparator());
          }
        }
      } else {
        Statement expression = (Statement) statement;
        if (expression instanceof NewLine) {
          // string.indent removes newlines
          out.append(expression.generateJava());
        } else {
          out.append(expression.generateJava().indent(indent * 2));
        }
      }
    }
    return out.toString();
  }

  /**
   * Write the scope as typescript code.
   *
   * @param indent the indentation of the scope
   * @return the written code
   */
  public String writeAsTypescript(int indent) {
    var out = new StringBuilder();
    for (Code statement : statements) {
      if (statement instanceof Scope scope) {
        out.append(scope.generateTypescriptHeader().indent(indent * 2));
        if (scope.isTsBlock()) {
          out.append(scope.writeAsTypescript(indent + 1));
          out.append(scope.endingDeliminator().indent(indent * 2));
        } else {
          out.append(scope.writeAsTypescript(indent));
        }
      } else {
        Statement expression = (Statement) statement;
        if (expression instanceof NewLine) {
          // string.indent removes newlines
          out.append(expression.generateTypescript());
        } else {
          out.append(expression.generateTypescript().indent(indent * 2));
        }
      }
    }
    return out.toString();
  }

  boolean isTsBlock() {
    return true;
  }

  boolean isJavaBlock() {
    return true;
  }

  String endingDeliminator() {
    return "}";
  }

  boolean shouldGenerateJava() {
    return true;
  }

  /** Add a line break to this scope. */
  public void addNewLine() {
    addStatement(new NewLine());
  }

  /**
   * Add an array add statement to this scope.
   *
   * @param name the name of the array
   * @param value the value to be added
   */
  public void addArrayAdd(String name, String value) {
    addStatement(
        new SimpleStatement(
            "%s.add(%s);".formatted(name, value), "%s.push(%s);".formatted(name, value)));
  }

  /**
   * Add a map put statement to this scope.
   *
   * @param name the name of the map
   * @param key the key to be added
   * @param value the value to be added
   */
  public void addMapPut(String name, String key, String value) {
    addStatement(
        new SimpleStatement(
            "%s.put(%s, %s);".formatted(name, key, value),
            "%s.set(%s, %s);".formatted(name, key, value)));
  }

  /**
   * Add a write vec size statement to this scope.
   *
   * @param streamName the name of the stream
   * @param name the name of the array
   */
  public void addWriteVecSize(String streamName, String name) {
    addStatement(
        new SimpleStatement(
            "%s.writeI32(%s.size());".formatted(streamName, name),
            "%s.writeI32(%s.length);".formatted(streamName, name)));
  }

  /**
   * Add a call serialize function statement to this scope.
   *
   * @param streamName name of the stream
   * @param arg the name of the object to be serialized
   * @param structName the name of the struct
   */
  public void addCallSerializeFunc(String streamName, String arg, String structName) {
    addStatement(
        new SimpleStatement(
            "%s.serialize(%s);".formatted(arg, streamName),
            "serialize%s(%s, %s);".formatted(structName, streamName, arg)));
  }

  /**
   * Add a throw error statement to this scope.
   *
   * @param errorMsg the error message
   */
  public void addThrowError(String errorMsg) {
    addStatement(
        new SimpleStatement(
            "throw new RuntimeException(%s);".formatted(errorMsg),
            "throw new Error(%s);".formatted(errorMsg)));
  }

  /**
   * Add an enum entry to this scope.
   *
   * @param name the name of the enum variant
   * @param discriminant the discriminant of the enum variant
   */
  public void addEnumEntry(String name, int discriminant) {
    addStatement(
        new SimpleStatement(
            "%s(%d),".formatted(AbiCodegenHelper.toCapitalizedSnakeCase(name), discriminant),
            "%s = %d,".formatted(name, discriminant)));
  }

  /**
   * Add a write option is some statement to this scope.
   *
   * @param name the name of the option value
   */
  public void addWriteOptionIsSome(String name) {
    addStatement(
        new SimpleStatement(
            "_out.writeBoolean(%s != null);".formatted(name),
            "_out.writeBoolean(%s !== undefined);".formatted(name)));
  }

  /**
   * Add a var decl statement to this scope.
   *
   * @param type the type of the var
   * @param name the name of the var
   * @param javaValue the value of the var if java
   * @param tsValue the value of the var if ts
   * @param contract contract abi
   */
  public void addVarDecl(
      TypeSpec type, String name, String javaValue, String tsValue, ContractAbi contract) {
    String javaType = type == null ? "var" : AbiCodegenHelper.typeSpecToJavaType(type, contract);
    String javaCode = "%s %s = %s;".formatted(javaType, name, javaValue);
    String tsType = type == null ? "" : ": " + AbiCodegenHelper.typeSpecToTsType(type, contract);
    String tsCode = "const %s%s = %s;".formatted(name, tsType, tsValue);
    addStatement(new SimpleStatement(javaCode, tsCode));
  }

  /**
   * Add a var decl statement to this scope.
   *
   * @param type the type of the var
   * @param name the name of the var
   * @param commonCode the value of the var
   * @param contract contract abi
   */
  public void addVarDecl(TypeSpec type, String name, String commonCode, ContractAbi contract) {
    addVarDecl(type, name, commonCode, commonCode, contract);
  }

  /**
   * Add a let decl statement to this scope.
   *
   * @param type the type of the var
   * @param name the name of the var
   * @param javaValue the value of the var if java
   * @param tsValue the value of the var if ts
   * @param contract contract abi
   */
  public void addLetDecl(
      TypeSpec type, String name, String javaValue, String tsValue, ContractAbi contract) {
    String javaCode =
        "%s %s = %s;"
            .formatted(AbiCodegenHelper.typeSpecToJavaType(type, contract), name, javaValue);
    String tsCode =
        "let %s: %s = %s;"
            .formatted(name, AbiCodegenHelper.typeSpecToTsType(type, contract), tsValue);
    addStatement(new SimpleStatement(javaCode, tsCode));
  }

  /**
   * Add a write hex string statement to this scope.
   *
   * @param hex the hex to write
   */
  public void addWriteHexString(String hex) {
    addStatement(
        new SimpleStatement(
            "_out.writeBytes(HexFormat.of().parseHex(\"%s\"));".formatted(hex),
            "_out.writeBytes(Buffer.from(\"%s\", \"hex\"));".formatted(hex)));
  }
}
