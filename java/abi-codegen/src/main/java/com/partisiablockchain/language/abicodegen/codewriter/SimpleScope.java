package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegenHelper;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.List;

/** A simple scope. */
public final class SimpleScope extends Scope {

  final String javaCode;
  final String tsCode;

  /**
   * Constructor with common ts and java code.
   *
   * @param commonCode common code
   */
  public SimpleScope(String commonCode) {
    this.javaCode = commonCode;
    this.tsCode = commonCode;
  }

  /**
   * Constructor.
   *
   * @param javaCode java code
   * @param tsCode ts code
   */
  public SimpleScope(String javaCode, String tsCode) {
    this.javaCode = javaCode;
    this.tsCode = tsCode;
  }

  @Override
  String generateJavaHeader() {
    return javaCode;
  }

  @Override
  String generateTypescriptHeader() {
    return tsCode;
  }

  /**
   * For loop to count scope.
   *
   * @param counterName name of the counter
   * @param counterLength name of the length variable
   * @return new scope
   */
  public static SimpleScope forLoopToCount(String counterName, String counterLength) {
    String javaCode =
        "for (int %s = 0; %s < %s; %s++) {"
            .formatted(counterName, counterName, counterLength, counterName);
    String tsCode =
        "for (let %s = 0; %s < %s; %s++) {"
            .formatted(counterName, counterName, counterLength, counterName);
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * For each loop scope.
   *
   * @param type type of variable
   * @param varName name of the variable
   * @param argsName name of argument to iterate over
   * @param contractAbi contract abi
   * @return new scope
   */
  public static SimpleScope forEachLoop(
      TypeSpec type, String varName, String argsName, ContractAbi contractAbi) {
    String javaCode =
        "for (%s %s : %s) {"
            .formatted(AbiCodegenHelper.typeSpecToJavaType(type, contractAbi), varName, argsName);
    String tsCode = "for (const %s of %s) {".formatted(varName, argsName);
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Deserialize function declaration scope.
   *
   * @param returnType the return type
   * @return new scope
   */
  public static SimpleScope deserializeFunctionDecl(String returnType) {
    String javaCode =
        "private %s deserialize%s(AbiInput _input) {".formatted(returnType, returnType);
    String tsCode =
        "public deserialize%s(_input: AbiInput): %s {".formatted(returnType, returnType);
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Serialize function declaration scope.
   *
   * @param name the name of struct to serialize
   * @return new scope
   */
  public static SimpleScope serializeFunctionDecl(String name) {
    String javaCode = "public void serialize(AbiOutput _out) {";
    String tsCode =
        "function serialize%s(_out: AbiOutput, _value: %s): void {".formatted(name, name);
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Deserialize bytes function declaration scope.
   *
   * @param name name of the struct to deserialize to
   * @param nameKind kind of deserialization (eg. State or Action)
   * @return new scope
   */
  public static SimpleScope addDeserializeBytes(String name, String nameKind) {
    String javaCode = "public static %s deserialize%s(byte[] bytes) {".formatted(name, nameKind);
    String tsCode = "export function deserialize%s(bytes: Buffer): %s {".formatted(nameKind, name);
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Serialize action function declaration scope.
   *
   * @param name name of the action
   * @param arguments arguments of the action
   * @param contractAbi contract abi
   * @return new scope
   */
  public static Scope serializeAction(
      String name, List<ArgumentAbi> arguments, ContractAbi contractAbi) {
    List<NameAndType> argumentsCast = arguments.stream().map(i -> (NameAndType) i).toList();
    String javaCode =
        "public static byte[] %s(%s) {"
            .formatted(name, AbiCodegenHelper.javaFields(argumentsCast, contractAbi));
    String tsCode =
        "export function %s(%s): Buffer {"
            .formatted(name, AbiCodegenHelper.tsFunctionArgs(argumentsCast, contractAbi));
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Serialize secret input function declaration scope.
   *
   * @param name name of the action
   * @param arguments arguments of the action
   * @param secretArgument secret argument of the function
   * @param contractAbi contract abi
   * @return new scope
   */
  public static Scope serializeSecretInput(
      String name,
      List<ArgumentAbi> arguments,
      ArgumentAbi secretArgument,
      ContractAbi contractAbi) {
    List<NameAndType> argumentsCast = arguments.stream().map(i -> (NameAndType) i).toList();
    String javaCode =
        "public static SecretInputBuilder<%s> %s(%s) {"
            .formatted(
                AbiCodegenHelper.typeNameNonPrimitive(secretArgument.type(), contractAbi),
                name,
                AbiCodegenHelper.javaFields(argumentsCast, contractAbi));
    String tsCode =
        "export function %s(%s): SecretInputBuilder<%s> {"
            .formatted(
                name,
                AbiCodegenHelper.tsFunctionArgs(argumentsCast, contractAbi),
                AbiCodegenHelper.typeSpecToTsType(secretArgument.type(), contractAbi));
    return new SimpleScope(javaCode, tsCode);
  }

  /**
   * Enum declaration scope.
   *
   * @param name name of the enum
   * @return new scope
   */
  public static Scope addEnum(String name) {
    return new SimpleScope("public enum %s {".formatted(name), "export enum %s {".formatted(name));
  }

  /**
   * Option if scope.
   *
   * @param name name of the option value
   * @return new scope
   */
  public static Scope optionIf(String name) {
    return new SimpleScope(
        "if (%s != null) {".formatted(name), "if (%s !== undefined) {".formatted(name));
  }
}
