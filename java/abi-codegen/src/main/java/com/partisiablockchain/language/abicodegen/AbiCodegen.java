package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.codewriter.CaseAnalysis;
import com.partisiablockchain.language.abicodegen.codewriter.DataRecord;
import com.partisiablockchain.language.abicodegen.codewriter.DataRecordImplements;
import com.partisiablockchain.language.abicodegen.codewriter.InterfaceScope;
import com.partisiablockchain.language.abicodegen.codewriter.JavaClassScope;
import com.partisiablockchain.language.abicodegen.codewriter.Scope;
import com.partisiablockchain.language.abicodegen.codewriter.ScopeWithEndingDelimiter;
import com.partisiablockchain.language.abicodegen.codewriter.SimpleScope;
import com.partisiablockchain.language.abicodegen.codewriter.TsClassScope;
import com.partisiablockchain.language.abicodegen.codewriter.TsOnlyScope;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class for generating java code from abi files. The java code makes it easier work with the
 * contract of the abi. It will generate java records corresponding to each struct in the abi,
 * together with the necessary logic required to be able to serialize/deserialize rpc/state.
 * Furthermore, java functions can be generated corresponding to the actions in the abi, that are
 * able to generate a rpc payload. Lastly it can generate java records corresponding to each
 * function in the abi together with a function to deserialize a rpc payload of the function.
 */
public final class AbiCodegen {
  private final ContractAbi contract;
  private final CodegenOptions options;

  /**
   * Constructor for AbiCodegen.
   *
   * @param contractAbi the abi
   * @param options options, used to control how part of the code is generated. Eg the name of the
   *     contract or whether to generate deserializing code for rpc or state.
   */
  public AbiCodegen(ContractAbi contractAbi, CodegenOptions options) {
    this.contract = contractAbi;
    this.options = options;
  }

  /**
   * Generates the code from an abi.
   *
   * @return the generated code
   */
  public String generateCode() {
    Scope rootScope = new SimpleScope(null);
    rootScope.addSimpleStatement(
        "// This file is auto-generated from an abi-file using AbiCodegen.");
    rootScope.addJavaOnly("package %s;".formatted(options.packageName()));
    rootScope.addSimpleStatement(AbiCodegenHelper.JAVA_HEADER, AbiCodegenHelper.TS_HEADER);
    addAbiGeneratedAnnotation(rootScope);
    Scope staticScope = rootScope.addScope(new JavaClassScope(options.contractName()));
    Scope classScope = staticScope.addScope(new TsClassScope(options.contractName()));
    buildFieldsAndConstructor(classScope);

    buildNamed(staticScope, classScope);
    buildActions(staticScope);
    buildCallbacks(staticScope);
    buildStateDeserialize(staticScope, classScope);
    buildActionsDeserialize(staticScope, classScope, ImplicitBinderAbi.DefaultKinds.ACTION);
    buildActionsDeserialize(staticScope, classScope, ImplicitBinderAbi.DefaultKinds.CALLBACK);
    buildActionsDeserialize(staticScope, classScope, ImplicitBinderAbi.DefaultKinds.INIT);

    if (options.targetLanguage() == CodegenOptions.TargetLanguage.JAVA) {
      return rootScope.writeAsJava(0);
    } else {
      return rootScope.writeAsTypescript(0);
    }
  }

  /**
   * Builds the fields and the constructor of the contract class.
   *
   * @param scope scope to build in
   */
  private void buildFieldsAndConstructor(Scope scope) {
    if (!constructorIsNeeded()) {
      return;
    }
    String javaCode =
        """
        private final BlockchainStateClient _client;
        private final BlockchainAddress _address;

        public %s(
            BlockchainStateClient client,
            BlockchainAddress address) {
          this._address = address;
          this._client = client;
        }"""
            .formatted(options.contractName());
    String tsCode =
        """
        private readonly _client: BlockchainStateClient | undefined;
        private readonly _address: BlockchainAddress | undefined;

        public constructor(
          client: BlockchainStateClient | undefined,
          address: BlockchainAddress | undefined
        ) {
          this._address = address;
          this._client = client;
        }""";
    scope.addSimpleStatement(javaCode, tsCode);
  }

  /**
   * Checks whether either the state, rpc or a named type should be deserialized. In all three
   * cases, the constructor will be used, i.e. should be generated, else it is not necessary.
   *
   * @return true if the constructor will be needed.
   */
  private boolean constructorIsNeeded() {
    return options.isDeserializeState()
        || options.isDeserializeRpc()
        || !options.deserializeNamedTypes().isEmpty();
  }

  /**
   * Builds the code responsible for deserializing contract invocations with specified fn kind. An
   * interface is generated for the FnKind and a data record is generated for each function of that
   * kind implementing the interface.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   * @param fnKind kind of function to deserialize
   */
  private void buildActionsDeserialize(
      Scope staticScope, Scope classScope, ContractInvocationKind fnKind) {
    if (!options.isDeserializeRpc()) {
      return;
    }
    List<ContractInvocation> functions =
        contract.contractInvocations().stream()
            .filter(fnAbi -> fnAbi.kind().kindId() == fnKind.kindId())
            .toList();
    if (functions.isEmpty()) {
      return;
    }
    String kindName = AbiCodegenHelper.toPascalCase(fnKind.name());
    Function<ContractInvocation, String> nameMap =
        fnAbi -> AbiCodegenHelper.toPascalCase(fnAbi.name()) + kindName;
    addAbiGeneratedAnnotation(staticScope);
    staticScope.addScope(new InterfaceScope(kindName, functions.stream().map(nameMap).toList()));
    staticScope.addNewLine();

    for (ContractInvocation contractInvocation : functions) {
      List<NameAndType> argumentsCast =
          contractInvocation.arguments().stream().map(i -> (NameAndType) i).toList();
      String newName = nameMap.apply(contractInvocation);
      addAbiGeneratedAnnotation(staticScope);
      staticScope.addScope(
          new DataRecordImplements(
              newName,
              argumentsCast,
              kindName,
              "\"%s\"".formatted(contractInvocation.name()),
              contract));
      Scope deserializeScope = classScope.addScope(SimpleScope.deserializeFunctionDecl(newName));
      for (ArgumentAbi argumentAbi : contractInvocation.arguments()) {
        readNameAndType(
            deserializeScope,
            argumentAbi.type(),
            "_input",
            AbiCodegenHelper.toCamelCase(argumentAbi.name()));
      }
      String fieldNames =
          contractInvocation.arguments().stream()
              .map(arg -> AbiCodegenHelper.toCamelCase(arg.name()))
              .collect(Collectors.joining(", "));
      String javaCode = "return new %s(%s);".formatted(newName, fieldNames);
      String tsCode =
          "return { discriminant: \"%s\", %s };".formatted(contractInvocation.name(), fieldNames);
      deserializeScope.addSimpleStatement(javaCode, tsCode);
      classScope.addNewLine();
    }

    Scope deserializeActionsScope =
        staticScope.addScope(SimpleScope.addDeserializeBytes(kindName, kindName));
    deserializeActionsScope.addVarDecl(
        null, "input", "AbiByteInput.createBigEndian(bytes)", contract);
    if (fnKind.kindId() == ImplicitBinderAbi.DefaultKinds.ACTION.kindId() && contract.isZk()) {
      deserializeActionsScope.addSimpleStatement("input.readU8();");
    }
    deserializeActionsScope.addVarDecl(null, "shortname", "input.readShortnameString()", contract);
    deserializeActionsScope.addVarDecl(
        null,
        "contract",
        "new %s(null, null)".formatted(options.contractName()),
        "new %s(undefined, undefined)".formatted(options.contractName()),
        contract);

    deserializeActionsScope.addStatement(
        new CaseAnalysis(
            "shortname",
            functions.stream()
                .map(
                    fnAbi ->
                        new CaseAnalysis.Case(
                            "\"%s\"".formatted(fnAbi.shortnameAsString()),
                            "contract.deserialize%s(input)".formatted(nameMap.apply(fnAbi)),
                            "contract.deserialize%s(input)".formatted(nameMap.apply(fnAbi))))
                .toList(),
            true));
    deserializeActionsScope.addThrowError("\"Illegal shortname: \" + shortname");
    staticScope.addNewLine();
  }

  /**
   * Builds the code responsible for serializing actions. Only action and init kinds are generated.
   *
   * @param scope scope to build in
   */
  private void buildActions(Scope scope) {
    if (!options.isSerializeActions()) {
      return;
    }
    List<ContractInvocation> contractCalls = contract.contractInvocations();
    for (var function : contractCalls) {
      if (function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.ACTION.kindId()
          || function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.INIT.kindId()) {
        buildFunction(scope, function);
      }
      if (function.secretArgument() != null) {
        buildSecretFunction(scope, function);
      }
    }
  }

  /**
   * Builds the code responsible for serializing callbacks.
   *
   * @param scope scope to build in
   */
  private void buildCallbacks(Scope scope) {
    if (!options.isSerializeCallbacks()) {
      return;
    }
    for (var function : contract.contractInvocations()) {
      if (function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.CALLBACK.kindId()) {
        buildFunction(scope, function);
      }
    }
  }

  private void buildFunction(Scope scope, ContractInvocation function) {
    List<ArgumentAbi> arguments = function.arguments();
    Scope functionScope =
        scope.addScope(
            SimpleScope.serializeAction(
                AbiCodegenHelper.toCamelCase(function.name()), arguments, contract));
    Scope serializeScope = functionScope.addScope(ScopeWithEndingDelimiter.serializeBigEndian());
    if (function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.ACTION.kindId()
        && contract.isZk()) {
      serializeScope.addSimpleStatement("_out.writeU8(0x09);");
    }
    serializeScope.addWriteHexString(function.shortnameAsString());

    for (ArgumentAbi argumentAbi : arguments) {
      writeNameAndType(
          serializeScope,
          argumentAbi.type(),
          "_out",
          AbiCodegenHelper.toCamelCase(argumentAbi.name()));
    }
    scope.addNewLine();
  }

  private void buildSecretFunction(Scope scope, ContractInvocation function) {
    List<ArgumentAbi> publicArguments = function.arguments();
    ArgumentAbi secretArgument = function.secretArgument();

    Scope functionScope =
        scope.addScope(
            SimpleScope.serializeSecretInput(
                AbiCodegenHelper.toCamelCase(function.name()),
                publicArguments,
                secretArgument,
                contract));
    String publicVarName = "_publicRpc";
    Scope publicSerializeScope =
        functionScope.addScope(ScopeWithEndingDelimiter.serializeBigEndianVar(publicVarName));
    publicSerializeScope.addWriteHexString(function.shortnameAsString());

    for (ArgumentAbi argumentAbi : publicArguments) {
      writeNameAndType(
          publicSerializeScope,
          argumentAbi.type(),
          "_out",
          AbiCodegenHelper.toCamelCase(argumentAbi.name()));
    }

    String secretVarName = "_secretInput";
    String lambdaName = secretArgument.name() + "_lambda";
    Scope secretSerializeScope =
        functionScope.addScope(
            ScopeWithEndingDelimiter.serializeSecretInputVar(
                secretVarName, lambdaName, secretArgument.type(), contract));
    writeNameAndType(secretSerializeScope, secretArgument.type(), "_out", lambdaName);

    functionScope.addSimpleStatement(
        "return new SecretInputBuilder<>(%s, %s);".formatted(publicVarName, secretVarName));

    scope.addNewLine();
  }

  /**
   * Builds the code responsible for deserializing state.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   */
  private void buildStateDeserialize(Scope staticScope, Scope classScope) {
    if (!options.isDeserializeState()) {
      return;
    }
    String stateName = contract.getStateStruct().name();
    String javaCode =
        """
        public static %1$s deserializeState(
            byte[] bytes,
            BlockchainStateClient client,
            BlockchainAddress address) {
          var input = AbiByteInput.createLittleEndian(bytes);
          return new %2$s(client, address).deserialize%1$s(input);
        }

        public static %1$s deserializeState(byte[] bytes) {
          return deserializeState(bytes, null, null);
        }

        public static %1$s deserializeState(StateWithClient state) {
          return deserializeState(state.bytes(), state.client(), state.address());
        }"""
            .formatted(stateName, options.contractName());

    String tsCode =
        """
        export function deserializeState(state: StateWithClient): %1$s;
        export function deserializeState(bytes: Buffer): %1$s;
        export function deserializeState(
          bytes: Buffer,
          client: BlockchainStateClient,
          address: BlockchainAddress
        ): %1$s;
        export function deserializeState(
          state: Buffer | StateWithClient,
          client?: BlockchainStateClient,
          address?: BlockchainAddress
        ): %1$s {
          if (Buffer.isBuffer(state)) {
            const input = AbiByteInput.createLittleEndian(state);
            return new %2$s(client, address).deserialize%1$s(input);
          } else {
            const input = AbiByteInput.createLittleEndian(state.bytes);
            return new %2$s(
              state.client,
              state.address
            ).deserialize%1$s(input);
          }
        }"""
            .formatted(stateName, options.contractName());

    staticScope.addSimpleStatement(javaCode, tsCode);
    staticScope.addNewLine();

    String javaGetState =
        """
        public %1$s getState() {
          byte[] bytes = _client.getContractStateBinary(_address);
          var input = AbiByteInput.createLittleEndian(bytes);
          return deserialize%1$s(input);
        }"""
            .formatted(stateName);

    String tsGetState =
        """
        public async getState(): Promise<%1$s> {
          const bytes = await this._client?.getContractStateBinary(this._address!);
          if (bytes === undefined) {
            throw new Error("Unable to get state bytes");
          }
          const input = AbiByteInput.createLittleEndian(bytes);
          return this.deserialize%1$s(input);
        }"""
            .formatted(stateName);

    classScope.addSimpleStatement(javaGetState, tsGetState);
    classScope.addNewLine();
  }

  /**
   * Build named types. Only named types that are used with the specified options are generated.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   */
  private void buildNamed(Scope staticScope, Scope classScope) {
    var namedTypes = contract.namedTypes();
    var enumStructs = AbiCodegenHelper.findEnumStructs(contract);
    var rpcStructs = AbiCodegenHelper.findRpcStructs(contract.contractInvocations(), contract);
    var stateStructs = AbiCodegenHelper.findStateStructs(contract);
    var avlKeyStructs = AbiCodegenHelper.findAvlKeyStructs(contract);
    var specialDeserializeStructs =
        AbiCodegenHelper.findSpecialDeserializeStructs(contract, options.deserializeNamedTypes());

    for (var namedType : namedTypes) {
      boolean shouldDeserializeState =
          options.isDeserializeState() && stateStructs.contains(namedType);
      boolean shouldSerializeRpc =
          options.isSerializeActions()
              && (rpcStructs.contains(namedType) || avlKeyStructs.contains(namedType));
      boolean shouldDeserializeRpc = options.isDeserializeRpc() && rpcStructs.contains(namedType);
      boolean shouldDeserialize =
          shouldDeserializeState
              || shouldDeserializeRpc
              || specialDeserializeStructs.contains(namedType);
      boolean shouldSpecialDeserialize = options.deserializeNamedTypes().contains(namedType.name());

      if (namedType instanceof StructTypeSpec structType) {
        if (enumStructs.contains(namedType)) {
          continue;
        }
        buildStruct(staticScope, classScope, structType, shouldDeserialize, shouldSerializeRpc);
      } else {
        EnumTypeSpec enumType = (EnumTypeSpec) namedType;
        buildEnum(staticScope, classScope, enumType, shouldDeserialize, shouldSerializeRpc);
      }

      if (shouldSpecialDeserialize) {
        Scope deserializeScope =
            staticScope.addScope(
                SimpleScope.addDeserializeBytes(namedType.name(), "Special" + namedType.name()));
        deserializeScope.addVarDecl(null, "input", "AbiByteInput.createBigEndian(bytes)", contract);
        deserializeScope.addSimpleStatement(
            "return new %s(null, null).deserialize%s(input);"
                .formatted(options.contractName(), namedType.name()),
            "return new %s(undefined, undefined).deserialize%s(input);"
                .formatted(options.contractName(), namedType.name()));
      }
    }
  }

  /**
   * Builds code responsible for an enum named type. An interface is generated for the enum, and a
   * data record is then generated for each enum variant implementing the interface.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   * @param enumType the enum type to build
   * @param shouldDeserialize whether to generate deserialize code for this enum
   * @param shouldSerializeRpc whether to generate serialize code for this enum
   */
  private void buildEnum(
      Scope staticScope,
      Scope classScope,
      EnumTypeSpec enumType,
      boolean shouldDeserialize,
      boolean shouldSerializeRpc) {
    boolean shouldGenerateStruct = shouldDeserialize || shouldSerializeRpc;
    if (!shouldGenerateStruct) {
      return;
    }
    addAbiGeneratedAnnotation(staticScope);
    Scope enumScope = staticScope.addScope(SimpleScope.addEnum(enumType.name() + "D"));
    List<EnumVariant> variants = enumType.variants();
    Function<EnumVariant, String> variantName =
        variant -> contract.getNamedType(variant.def()).name();
    for (EnumVariant variant : variants) {
      enumScope.addEnumEntry(variantName.apply(variant), variant.discriminant());
    }
    enumScope.addJavaOnly(
        """
        ;
        private final int value;
        %sD(int value) {
          this.value = value;
        }"""
            .formatted(enumType.name()));

    addAbiGeneratedAnnotation(staticScope);
    Scope interfaceScope =
        staticScope.addScope(
            new InterfaceScope(
                enumType.name(),
                variants.stream()
                    .map(variant -> enumType.name() + variantName.apply(variant))
                    .toList()));
    interfaceScope.addJavaOnly("%sD discriminant();".formatted(enumType.name()));

    if (shouldSerializeRpc) {
      interfaceScope.addJavaOnly("void serialize(AbiOutput out);");
      Scope tsScope =
          interfaceScope.addScope(
              new TsOnlyScope(
                  "function serialize%s(out: AbiOutput, value: %s): void {"
                      .formatted(enumType.name(), enumType.name())));
      List<CaseAnalysis.Case> cases =
          variants.stream()
              .map(
                  variant ->
                      new CaseAnalysis.Case(
                          "%sD.%s".formatted(enumType.name(), variantName.apply(variant)),
                          "",
                          "serialize%s%s(out, value)"
                              .formatted(enumType.name(), variantName.apply(variant))))
              .toList();
      tsScope.addStatement(new CaseAnalysis("value.discriminant", cases, false));
    }

    if (shouldDeserialize) {
      Scope deserializeScope =
          classScope.addScope(SimpleScope.deserializeFunctionDecl(enumType.name()));
      deserializeScope.addVarDecl(null, "discriminant", "_input.readU8()", contract);
      deserializeScope.addStatement(
          new CaseAnalysis(
              "discriminant",
              variants.stream()
                  .map(
                      variant ->
                          new CaseAnalysis.Case(
                              "" + variant.discriminant(),
                              "deserialize%s%s(_input)"
                                  .formatted(enumType.name(), variantName.apply(variant)),
                              "this.deserialize%s%s(_input)"
                                  .formatted(enumType.name(), variantName.apply(variant))))
                  .toList(),
              false));
      deserializeScope.addThrowError("\"Unknown discriminant: \" + discriminant");
    }
    staticScope.addNewLine();

    for (EnumVariant variant : variants) {
      buildEnumVariant(
          staticScope, classScope, enumType.name(), variant, shouldDeserialize, shouldSerializeRpc);
    }
  }

  /**
   * Builds code responsible for an enum variant.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   * @param enumTypeName name of the enum
   * @param variant the enum variant to build
   * @param shouldDeserialize whether to generate deserialize code for this enum
   * @param shouldSerializeRpc whether to generate serialize code for this enum
   */
  private void buildEnumVariant(
      Scope staticScope,
      Scope classScope,
      String enumTypeName,
      EnumVariant variant,
      boolean shouldDeserialize,
      boolean shouldSerializeRpc) {
    StructTypeSpec structType = (StructTypeSpec) contract.getNamedType(variant.def());
    List<NameAndType> fieldsCast = structType.fields().stream().map(i -> (NameAndType) i).toList();
    String enumValue = "%sD.%s".formatted(enumTypeName, structType.name());
    addAbiGeneratedAnnotation(staticScope);
    Scope dataRecord =
        staticScope.addScope(
            new DataRecordImplements(
                enumTypeName + structType.name(), fieldsCast, enumTypeName, enumValue, contract));

    dataRecord.addJavaOnly(
        """
        public %sD discriminant() {
          return %sD.%s;
        }"""
            .formatted(
                enumTypeName,
                enumTypeName,
                AbiCodegenHelper.toCapitalizedSnakeCase(structType.name())));

    String fieldNames =
        structType.fields().stream()
            .map(fieldAbi -> AbiCodegenHelper.toCamelCase(fieldAbi.name()))
            .collect(Collectors.joining(", "));

    if (shouldSerializeRpc) {
      Scope serializeScope =
          dataRecord.addScope(SimpleScope.serializeFunctionDecl(enumTypeName + structType.name()));
      serializeScope.addTsOnly("const {%s} = _value;".formatted(fieldNames));
      serializeScope.addSimpleStatement(
          "_out.writeU8(discriminant().value);", "_out.writeU8(_value.discriminant);");
      for (FieldAbi fieldAbi : structType.fields()) {
        writeNameAndType(
            serializeScope, fieldAbi.type(), "_out", AbiCodegenHelper.toCamelCase(fieldAbi.name()));
      }
    }
    if (shouldDeserialize) {

      Scope deserializeScope =
          classScope.addScope(
              SimpleScope.deserializeFunctionDecl(
                  "%s%s".formatted(enumTypeName, structType.name())));
      for (FieldAbi fieldAbi : structType.fields()) {
        readNameAndType(
            deserializeScope,
            fieldAbi.type(),
            "_input",
            AbiCodegenHelper.toCamelCase(fieldAbi.name()));
      }

      String javaCode =
          "return new %s%s(%s);".formatted(enumTypeName, structType.name(), fieldNames);
      String tsCode = "return { discriminant: %s, %s };".formatted(enumValue, fieldNames);
      deserializeScope.addSimpleStatement(javaCode, tsCode);
    }
    staticScope.addNewLine();
  }

  /**
   * Build code responsible for a named struct type. Generates a data record for this struct.
   *
   * @param staticScope scope for the static context
   * @param classScope scope for class context
   * @param structType the struct type to build
   * @param shouldDeserialize whether to generate deserialize code for this struct
   * @param shouldSerializeRpc whether to generate serialize code for this struct
   */
  private void buildStruct(
      Scope staticScope,
      Scope classScope,
      StructTypeSpec structType,
      boolean shouldDeserialize,
      boolean shouldSerializeRpc) {
    boolean shouldGenerateStruct = shouldDeserialize || shouldSerializeRpc;
    if (!shouldGenerateStruct) {
      return;
    }

    addAbiGeneratedAnnotation(staticScope);
    Scope structScope =
        staticScope.addScope(
            new DataRecord(
                structType.name(),
                structType.fields().stream().map(i -> (NameAndType) i).toList(),
                contract));
    String fieldNames =
        structType.fields().stream()
            .map(fieldAbi -> AbiCodegenHelper.toCamelCase(fieldAbi.name()))
            .collect(Collectors.joining(", "));

    if (shouldDeserialize) {
      Scope functionScope =
          classScope.addScope(SimpleScope.deserializeFunctionDecl(structType.name()));
      for (FieldAbi fieldAbi : structType.fields()) {
        readNameAndType(
            functionScope,
            fieldAbi.type(),
            "_input",
            AbiCodegenHelper.toCamelCase(fieldAbi.name()));
      }

      String javaCode = "return new %s(%s);".formatted(structType.name(), fieldNames);
      String tsCode = "return { %s };".formatted(fieldNames);
      functionScope.addSimpleStatement(javaCode, tsCode);

      if (AbiCodegenHelper.isStateStruct(structType, contract)) {
        structScope.addJavaOnly(
            """
            public static %s deserialize(byte[] bytes) {
              return %s.deserializeState(bytes);
            }"""
                .formatted(structType.name(), options.contractName()));
      }
    }

    if (shouldSerializeRpc) {
      Scope functionScope =
          structScope.addScope(SimpleScope.serializeFunctionDecl(structType.name()));
      functionScope.addTsOnly("const { %s } = _value;".formatted(fieldNames));
      for (FieldAbi fieldAbi : structType.fields()) {
        writeNameAndType(
            functionScope, fieldAbi.type(), "_out", AbiCodegenHelper.toCamelCase(fieldAbi.name()));
      }
    }
    staticScope.addNewLine();
  }

  /**
   * Build code responsible for serializing a variable of some type.
   *
   * @param scope scope to build the code in
   * @param type type of the variable
   * @param streamName name of the stream writing bytes
   * @param name name of the variable to write
   */
  private void writeNameAndType(Scope scope, TypeSpec type, String streamName, String name) {
    if (type instanceof SimpleTypeSpec simple) {
      writeSimple(scope, simple, streamName, name);
    } else if (type instanceof NamedTypeRef named) {
      String structName = contract.getNamedType(named).name();
      scope.addCallSerializeFunc(streamName, name, structName);
    } else if (AbiCodegenHelper.isVecU8(type)) {
      scope.addSimpleStatement("%s.writeI32(%s.length);".formatted(streamName, name));
      scope.addSimpleStatement("%s.writeBytes(%s);".formatted(streamName, name));
    } else if (type instanceof VecTypeSpec vecTypeSpec) {
      scope.addWriteVecSize(streamName, name);
      TypeSpec innerType = vecTypeSpec.valueType();
      String innerName = name + "_vec";
      Scope vecScope =
          scope.addScope(SimpleScope.forEachLoop(innerType, innerName, name, contract));
      writeNameAndType(vecScope, innerType, streamName, innerName);
    } else if (type instanceof OptionTypeSpec optionTypeSpec) {
      scope.addWriteOptionIsSome(name);
      Scope optionScope = scope.addScope(SimpleScope.optionIf(name));
      writeNameAndType(optionScope, optionTypeSpec.valueType(), streamName, name);

    } else { // Sized array
      SizedArrayTypeSpec arrayTypeSpec = (SizedArrayTypeSpec) type;
      if (arrayTypeSpec.valueType().typeIndex() == TypeSpec.TypeIndex.u8) {
        // Special handling if it is a byte array
        Scope ifScope =
            scope.addScope(
                new SimpleScope("if (%s.length != %d) {".formatted(name, arrayTypeSpec.length())));
        ifScope.addThrowError(
            "\"Length of %s does not match expected %d\"".formatted(name, arrayTypeSpec.length()));
        scope.addSimpleStatement("%s.writeBytes(%s);".formatted(streamName, name));
      } else {
        Scope ifScope =
            scope.addScope(
                new SimpleScope(
                    "if (%s.size() != %d) {".formatted(name, arrayTypeSpec.length()),
                    "if (%s.length !== %d) {".formatted(name, arrayTypeSpec.length())));
        ifScope.addThrowError(
            "\"Length of %s does not match expected %d\"".formatted(name, arrayTypeSpec.length()));

        TypeSpec innerType = arrayTypeSpec.valueType();
        String innerName = name + "_arr";
        Scope arrScope =
            scope.addScope(SimpleScope.forEachLoop(innerType, innerName, name, contract));
        writeNameAndType(arrScope, innerType, streamName, innerName);
      }
    }
  }

  /**
   * Build code responsible for serializing a variable of some simple type.
   *
   * @param scope scope to build the code in
   * @param type the simple type of the variable
   * @param streamName name of the stream writing bytes
   * @param name name of the variable to write
   */
  private void writeSimple(Scope scope, SimpleTypeSpec type, String streamName, String name) {
    switch (type.typeIndex()) {
      case i8 -> scope.addSimpleStatement("%s.writeI8(%s);".formatted(streamName, name));
      case u8 -> scope.addSimpleStatement("%s.writeU8(%s);".formatted(streamName, name));
      case i16 -> scope.addSimpleStatement("%s.writeI16(%s);".formatted(streamName, name));
      case u16 -> scope.addSimpleStatement("%s.writeU16(%s);".formatted(streamName, name));
      case i32 -> scope.addSimpleStatement("%s.writeI32(%s);".formatted(streamName, name));
      case u32 -> scope.addSimpleStatement("%s.writeU32(%s);".formatted(streamName, name));
      case i64 -> scope.addSimpleStatement("%s.writeI64(%s);".formatted(streamName, name));
      case u64 -> scope.addSimpleStatement("%s.writeU64(%s);".formatted(streamName, name));
      case i128 ->
          scope.addSimpleStatement("%s.writeSignedBigInteger(%s, 16);".formatted(streamName, name));
      case u128 ->
          scope.addSimpleStatement(
              "%s.writeUnsignedBigInteger(%s, 16);".formatted(streamName, name));
      case u256 ->
          scope.addSimpleStatement(
              "%s.writeUnsignedBigInteger(%s, 32);".formatted(streamName, name));
      case Address -> scope.addSimpleStatement("%s.writeAddress(%s);".formatted(streamName, name));
      case Hash -> scope.addSimpleStatement("%s.writeHash(%s);".formatted(streamName, name));
      case PublicKey ->
          scope.addSimpleStatement("%s.writePublicKey(%s);".formatted(streamName, name));
      case Signature ->
          scope.addSimpleStatement("%s.writeSignature(%s);".formatted(streamName, name));
      case BlsPublicKey ->
          scope.addSimpleStatement("%s.writeBlsPublicKey(%s);".formatted(streamName, name));
      case BlsSignature ->
          scope.addSimpleStatement("%s.writeBlsSignature(%s);".formatted(streamName, name));
      case bool -> scope.addSimpleStatement("%s.writeBoolean(%s);".formatted(streamName, name));
      default ->
          scope.addSimpleStatement("%s.writeString(%s);".formatted(streamName, name)); // String
    }
  }

  /**
   * Builds code responsible for reading some type into a variable.
   *
   * @param scope scope to build the code in
   * @param type type of the variable
   * @param streamName name of the stream reading bytes
   * @param name name of the variable being read
   */
  private void readNameAndType(Scope scope, TypeSpec type, String streamName, String name) {
    if (type instanceof SimpleTypeSpec simple) {
      readSimple(scope, simple, streamName, name);
    } else if (type instanceof NamedTypeRef namedTypeRef) {
      String namedName = contract.getNamedType(namedTypeRef).name();
      scope.addVarDecl(
          type,
          name,
          "deserialize%s(%s)".formatted(namedName, streamName),
          "this.deserialize%s(%s)".formatted(namedName, streamName),
          contract);
    } else if (AbiCodegenHelper.isVecU8(type)) {
      String vecLength = name + "_vecLength";
      scope.addVarDecl(null, vecLength, "%s.readI32()".formatted(streamName), contract);
      scope.addVarDecl(type, name, "%s.readBytes(%s)".formatted(streamName, vecLength), contract);
    } else if (type instanceof VecTypeSpec vecTypeSpec) {
      String vecLength = name + "_vecLength";
      scope.addVarDecl(null, vecLength, "%s.readI32()".formatted(streamName), contract);
      readList(scope, type, streamName, name, vecTypeSpec.valueType(), vecLength);
    } else if (type instanceof MapTypeSpec mapTypeSpec) {
      String mapLength = name + "_mapLength";
      scope.addVarDecl(null, mapLength, "%s.readI32()".formatted(streamName), contract);
      scope.addVarDecl(type, name, "new HashMap<>()", "new Map()", contract);
      Scope mapScope = scope.addScope(SimpleScope.forLoopToCount(name + "_i", mapLength));
      String nestedKeyName = name + "_key";
      readNameAndType(mapScope, mapTypeSpec.keyType(), streamName, nestedKeyName);
      String nestedValueName = name + "_value";
      readNameAndType(mapScope, mapTypeSpec.valueType(), streamName, nestedValueName);
      mapScope.addMapPut(name, nestedKeyName, nestedValueName);
    } else if (type instanceof OptionTypeSpec optionTypeSpec) {
      scope.addLetDecl(type, name, "null", "undefined", contract);
      scope.addVarDecl(null, name + "_isSome", "%s.readBoolean()".formatted(streamName), contract);
      Scope optionScope = scope.addScope(new SimpleScope("if (%s_isSome) {".formatted(name)));
      String innerName = name + "_option";
      readNameAndType(optionScope, optionTypeSpec.valueType(), streamName, innerName);
      optionScope.addSimpleStatement("%s = %s;".formatted(name, innerName));
    } else if (type instanceof SetTypeSpec setTypeSpec) {
      String setLength = name + "_setLength";
      scope.addVarDecl(null, setLength, "%s.readI32()".formatted(streamName), contract);
      readList(scope, type, streamName, name, setTypeSpec.valueType(), setLength);
    } else if (type instanceof AvlTreeMapTypeSpec avlType) {
      String treeIdName = name + "_treeId";
      scope.addVarDecl(null, treeIdName, "%s.readI32()".formatted(streamName), contract);
      Scope avlScope = scope.addScope(ScopeWithEndingDelimiter.newAvlTreeMap(type, name, contract));
      avlScope.addSimpleStatement("%s,".formatted(treeIdName));
      avlScope.addSimpleStatement("this._client,");
      avlScope.addSimpleStatement("this._address,");

      String keyName = name + "_key";
      String outStreamName = name + "_out";
      Scope keySerialize =
          avlScope.addScope(ScopeWithEndingDelimiter.serializeAvlKey(keyName, outStreamName));
      writeNameAndType(keySerialize, avlType.keyType(), outStreamName, keyName);

      String avlBytesName = name + "_bytes";
      String inStreamName = name + "_input";
      Scope keyDeserialize =
          avlScope.addScope(ScopeWithEndingDelimiter.deserializeAvl(avlBytesName, "},"));
      keyDeserialize.addVarDecl(
          null,
          inStreamName,
          "AbiByteInput.createLittleEndian(%s)".formatted(avlBytesName),
          contract);
      readNameAndType(keyDeserialize, avlType.keyType(), inStreamName, keyName);
      keyDeserialize.addSimpleStatement("return %s;".formatted(keyName));

      String valueName = name + "_value";
      Scope valueDeserialize =
          avlScope.addScope(ScopeWithEndingDelimiter.deserializeAvl(avlBytesName, "}"));
      valueDeserialize.addVarDecl(
          null,
          inStreamName,
          "AbiByteInput.createLittleEndian(%s)".formatted(avlBytesName),
          contract);
      readNameAndType(valueDeserialize, avlType.valueType(), inStreamName, valueName);
      valueDeserialize.addSimpleStatement("return %s;".formatted(valueName));

    } else { // Sized array
      SizedArrayTypeSpec sized = (SizedArrayTypeSpec) type;
      if (sized.valueType().typeIndex() == TypeSpec.TypeIndex.u8) {
        // Special handling for byte arrays
        scope.addVarDecl(
            type, name, "%s.readBytes(%d)".formatted(streamName, sized.length()), contract);
      } else {
        readList(scope, type, streamName, name, sized.valueType(), "%d".formatted(sized.length()));
      }
    }
  }

  private void readList(
      Scope scope,
      TypeSpec type,
      String streamName,
      String name,
      TypeSpec innerType,
      String vecLength) {
    scope.addVarDecl(type, name, "new ArrayList<>()", "[]", contract);
    Scope vecScope = scope.addScope(SimpleScope.forLoopToCount(name + "_i", vecLength));
    String nestedName = name + "_elem";
    readNameAndType(vecScope, innerType, streamName, nestedName);
    vecScope.addArrayAdd(name, nestedName);
  }

  /**
   * Builds code responsible for reading a some simple type into a variable.
   *
   * @param scope scope to build the code in
   * @param type type of the variable
   * @param streamName name of the stream reading bytes
   * @param name name of the variable being read
   */
  private void readSimple(Scope scope, SimpleTypeSpec type, String streamName, String name) {
    switch (type.typeIndex()) {
      case i8 -> scope.addVarDecl(type, name, "%s.readI8()".formatted(streamName), contract);
      case u8 -> scope.addVarDecl(type, name, "%s.readU8()".formatted(streamName), contract);
      case i16 -> scope.addVarDecl(type, name, "%s.readI16()".formatted(streamName), contract);
      case u16 -> scope.addVarDecl(type, name, "%s.readU16()".formatted(streamName), contract);
      case i32 -> scope.addVarDecl(type, name, "%s.readI32()".formatted(streamName), contract);
      case u32 -> scope.addVarDecl(type, name, "%s.readU32()".formatted(streamName), contract);
      case i64 -> scope.addVarDecl(type, name, "%s.readI64()".formatted(streamName), contract);
      case u64 -> scope.addVarDecl(type, name, "%s.readU64()".formatted(streamName), contract);
      case i128 ->
          scope.addVarDecl(
              type, name, "%s.readSignedBigInteger(16)".formatted(streamName), contract);
      case u128 ->
          scope.addVarDecl(
              type, name, "%s.readUnsignedBigInteger(16)".formatted(streamName), contract);
      case u256 ->
          scope.addVarDecl(
              type, name, "%s.readUnsignedBigInteger(32)".formatted(streamName), contract);
      case Address ->
          scope.addVarDecl(type, name, "%s.readAddress()".formatted(streamName), contract);
      case Hash -> scope.addVarDecl(type, name, "%s.readHash()".formatted(streamName), contract);
      case PublicKey ->
          scope.addVarDecl(type, name, "%s.readPublicKey()".formatted(streamName), contract);
      case Signature ->
          scope.addVarDecl(type, name, "%s.readSignature()".formatted(streamName), contract);
      case BlsPublicKey ->
          scope.addVarDecl(type, name, "%s.readBlsPublicKey()".formatted(streamName), contract);
      case BlsSignature ->
          scope.addVarDecl(type, name, "%s.readBlsSignature()".formatted(streamName), contract);
      case bool -> scope.addVarDecl(type, name, "%s.readBoolean()".formatted(streamName), contract);
      default ->
          scope.addVarDecl(type, name, "%s.readString()".formatted(streamName), contract); // String
    }
  }

  /**
   * Adds the @AbiGenerated annotation to the builder, unless disabled by cli options.
   *
   * @param scope the scope
   */
  private void addAbiGeneratedAnnotation(Scope scope) {
    if (options.isGenerateAnnotations()) {
      scope.addJavaOnly("@AbiGenerated");
    }
  }
}
