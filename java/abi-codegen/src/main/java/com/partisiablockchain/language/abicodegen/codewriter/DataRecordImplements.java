package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegenHelper;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import java.util.List;

/** Data record scope which implements an interface. */
public final class DataRecordImplements extends Scope {

  String name;

  List<NameAndType> fields;
  String superType;
  private final String discriminant;
  private final ContractAbi contractAbi;

  /**
   * Constructor.
   *
   * @param name name of the record
   * @param fields fields of the record
   * @param superType the super type
   * @param discriminant discriminant of the record
   * @param contractAbi contract abi
   */
  public DataRecordImplements(
      String name,
      List<NameAndType> fields,
      String superType,
      String discriminant,
      ContractAbi contractAbi) {
    this.name = name;
    this.fields = fields;
    this.superType = superType;
    this.discriminant = discriminant;
    this.contractAbi = contractAbi;
  }

  @Override
  String generateJavaHeader() {
    return "public record %s(%s) implements %s {"
        .formatted(name, AbiCodegenHelper.javaFields(fields, contractAbi), superType);
  }

  @Override
  String generateTypescriptHeader() {
    return """
           export interface %s {
             discriminant: %s;
           %s}
           """
        .formatted(name, discriminant, AbiCodegenHelper.tsInterfaceFields(fields, contractAbi));
  }

  @Override
  boolean isTsBlock() {
    return false;
  }

  @Override
  String endingDeliminator() {
    return "}";
  }
}
