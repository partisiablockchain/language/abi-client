package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegenHelper;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.TypeSpec;

/** A Lambda scope. Is special as its ending delimiter is '});' */
public final class ScopeWithEndingDelimiter extends Scope {

  final String javaCode;
  final String tsCode;
  private final String endingDelimiter;

  /**
   * Constructor.
   *
   * @param javaCode java code
   * @param tsCode typescript code
   * @param endingDelimiter the ending delimiter
   */
  public ScopeWithEndingDelimiter(String javaCode, String tsCode, String endingDelimiter) {
    this.javaCode = javaCode;
    this.tsCode = tsCode;
    this.endingDelimiter = endingDelimiter;
  }

  @Override
  String generateJavaHeader() {
    return javaCode;
  }

  @Override
  String generateTypescriptHeader() {
    return tsCode;
  }

  @Override
  String endingDeliminator() {
    return endingDelimiter;
  }

  /**
   * Create a serialize big endian lambda scope.
   *
   * @return new lambda scope
   */
  public static ScopeWithEndingDelimiter serializeBigEndian() {
    return new ScopeWithEndingDelimiter(
        "return AbiByteOutput.serializeBigEndian(_out -> {",
        "return AbiByteOutput.serializeBigEndian((_out) => {",
        "});");
  }

  /**
   * Create a serialize big endian lambda scope.
   *
   * @param varName name of the variable to put the result into
   * @return new lambda scope
   */
  public static ScopeWithEndingDelimiter serializeBigEndianVar(String varName) {
    return new ScopeWithEndingDelimiter(
        "byte[] %s = AbiByteOutput.serializeBigEndian(_out -> {".formatted(varName),
        "const %s: Buffer = AbiByteOutput.serializeBigEndian((_out) => {".formatted(varName),
        "});");
  }

  /**
   * Create a serialize secret input scope.
   *
   * @param varName name of the variable to put the result into
   * @param lambdaName name of the lambda parameter
   * @param secretType type of the secret input
   * @param contractAbi contract abi
   * @return new lambda scope
   */
  public static ScopeWithEndingDelimiter serializeSecretInputVar(
      String varName, String lambdaName, TypeSpec secretType, ContractAbi contractAbi) {
    return new ScopeWithEndingDelimiter(
        "Function<%s, CompactBitArray> %s = (%s) -> AbiBitOutput.serialize(_out -> {"
            .formatted(
                AbiCodegenHelper.typeNameNonPrimitive(secretType, contractAbi),
                varName,
                lambdaName),
        "const %s = (%s: %s): CompactBitArray => AbiBitOutput.serialize((_out) => {"
            .formatted(
                varName, lambdaName, AbiCodegenHelper.typeSpecToTsType(secretType, contractAbi)),
        "});");
  }

  /**
   * Create a new avl tree map scope.
   *
   * @param type type of the avl tree map
   * @param name name of the avl tree map variable
   * @param contract contract
   * @return the new scope
   */
  public static ScopeWithEndingDelimiter newAvlTreeMap(
      TypeSpec type, String name, ContractAbi contract) {
    String javaTypeName = AbiCodegenHelper.typeSpecToJavaType(type, contract);
    String tsTypeName = AbiCodegenHelper.typeSpecToTsType(type, contract);
    return new ScopeWithEndingDelimiter(
        "%s %s = new AvlTreeMap<>(".formatted(javaTypeName, name),
        "const %s: %s = new AvlTreeMap(".formatted(name, tsTypeName),
        ");");
  }

  /**
   * Create a serialize avl key scope.
   *
   * @param valueName name of the variable
   * @param streamName name of the stream
   * @return the new scope
   */
  public static ScopeWithEndingDelimiter serializeAvlKey(String valueName, String streamName) {
    return new ScopeWithEndingDelimiter(
        "(%s) -> AbiByteOutput.serializeLittleEndian(%s -> {".formatted(valueName, streamName),
        "(%s) => AbiByteOutput.serializeLittleEndian((%s) => {".formatted(valueName, streamName),
        "}),");
  }

  /**
   * Create a deserialize avl key/value scope.
   *
   * @param valueName name of the variable
   * @param endingDelimiter the ending delimiter
   * @return the new scope
   */
  public static ScopeWithEndingDelimiter deserializeAvl(String valueName, String endingDelimiter) {
    return new ScopeWithEndingDelimiter(
        "(%s) -> {".formatted(valueName), "(%s) => {".formatted(valueName), endingDelimiter);
  }
}
