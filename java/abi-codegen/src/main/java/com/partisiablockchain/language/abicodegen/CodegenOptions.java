package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.util.Set;

/** Codegen options. */
public record CodegenOptions(
    TargetLanguage targetLanguage,
    String contractName,
    AbiParser.Strictness strictness,
    String packageName,
    boolean isDeserializeState,
    boolean isSerializeActions,
    boolean isSerializeCallbacks,
    boolean isDeserializeRpc,
    Set<String> deserializeNamedTypes,
    boolean isGenerateAnnotations) {

  /** All the different target languages. */
  public enum TargetLanguage {
    /** Generate java code. */
    JAVA,
    /** Generate typescript code. */
    TS
  }
}
