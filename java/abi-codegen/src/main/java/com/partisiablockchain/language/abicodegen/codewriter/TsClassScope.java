package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class scope for typescript. It's different from java as there is a need to be able to place some
 * elements in inside the java class while outside the typescript class.
 */
public final class TsClassScope extends Scope {
  private final String contractName;

  /**
   * Constructor.
   *
   * @param contractName contract name
   */
  public TsClassScope(String contractName) {
    this.contractName = contractName;
  }

  @Override
  String generateJavaHeader() {
    return "";
  }

  @Override
  String generateTypescriptHeader() {
    return "export class %s {".formatted(contractName);
  }

  @Override
  boolean isJavaBlock() {
    return false;
  }

  @Override
  String endingDeliminator() {
    return "}";
  }
}
