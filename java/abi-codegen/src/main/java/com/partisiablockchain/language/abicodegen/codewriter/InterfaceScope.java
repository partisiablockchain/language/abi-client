package com.partisiablockchain.language.abicodegen.codewriter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/** Interface scope. */
public final class InterfaceScope extends Scope {
  private final String name;

  private final List<String> memberNames;

  /**
   * Constructor.
   *
   * @param name name of the interface
   * @param memberNames names of its members
   */
  public InterfaceScope(String name, List<String> memberNames) {
    this.name = name;
    this.memberNames = memberNames;
  }

  @Override
  String generateJavaHeader() {
    return "public interface %s {".formatted(name);
  }

  @Override
  String generateTypescriptHeader() {
    return "export type %s =\n  | %s;".formatted(name, String.join("\n  | ", memberNames));
  }

  @Override
  boolean isTsBlock() {
    return false;
  }

  @Override
  String endingDeliminator() {
    return "}";
  }
}
