package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NameAndType;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/** Helper class for common functions between languages. */
public final class AbiCodegenHelper {

  static final String JAVA_HEADER =
      """

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;""";

  static final String TS_HEADER =
      """
      /* eslint-disable */
      // @ts-nocheck
      // noinspection ES6UnusedImports
      import {
        AbiBitInput,
        AbiBitOutput,
        AbiByteInput,
        AbiByteOutput,
        AbiInput,
        AbiOutput,
        AvlTreeMap,
        BlockchainAddress,
        BlockchainPublicKey,
        BlockchainStateClient,
        BlsPublicKey,
        BlsSignature,
        BN,
        Hash,
        Signature,
        StateWithClient,
        SecretInputBuilder,
      } from "@partisiablockchain/abi-client";

      type Option<K> = K | undefined;""";

  private AbiCodegenHelper() {}

  /**
   * Turn snake_case into camelCase.
   *
   * @param name snake case
   * @return camel case
   */
  static String toCamelCase(String name) {
    var names = name.split("_");
    var firstName = names[0];
    var lastNames = Arrays.stream(names).toList().subList(1, names.length);
    var upperCaseNames =
        lastNames.stream()
            .map(str -> str.substring(0, 1).toUpperCase(Locale.ENGLISH) + str.substring(1))
            .collect(Collectors.joining());
    return firstName + upperCaseNames;
  }

  /**
   * Turn pascalCase into CAPITALIZED_SNAKE_CASE.
   *
   * @param pascalCase pascal case
   * @return capitalized snake case
   */
  public static String toCapitalizedSnakeCase(String pascalCase) {
    String regex = "([a-z])([A-Z]+)";
    String replacement = "$1_$2";
    return pascalCase.replaceAll(regex, replacement).toUpperCase(Locale.ENGLISH);
  }

  /**
   * Find all named types that used for rpc.
   *
   * @param functions list of contract invocations
   * @param contract contract abi
   * @return set of named rpc types
   */
  static Set<NamedTypeSpec> findRpcStructs(
      List<ContractInvocation> functions, ContractAbi contract) {
    var rpcStructs = new HashSet<NamedTypeSpec>();
    for (var func : functions) {
      for (var arg : func.arguments()) {
        recStructs(rpcStructs, arg.type(), contract);
      }
      if (func.secretArgument() != null) {
        recStructs(rpcStructs, func.secretArgument().type(), contract);
      }
    }
    return rpcStructs;
  }

  private static void recStructs(Set<NamedTypeSpec> structs, TypeSpec type, ContractAbi contract) {
    if (type instanceof NamedTypeRef namedTypeRef) {
      var namedType = contract.getNamedType(namedTypeRef);
      if (structs.add(namedType)) {
        if (namedType instanceof StructTypeSpec structType) {
          for (var field : structType.fields()) {
            recStructs(structs, field.type(), contract);
          }
        } else {
          var enumType = (EnumTypeSpec) namedType;
          for (var enumVariant : enumType.variants()) {
            recStructs(structs, enumVariant.def(), contract);
          }
        }
      }
    } else if (type instanceof VecTypeSpec vec) {
      recStructs(structs, vec.valueType(), contract);
    } else if (type instanceof OptionTypeSpec option) {
      recStructs(structs, option.valueType(), contract);
    } else if (type instanceof MapTypeSpec map) {
      recStructs(structs, map.keyType(), contract);
      recStructs(structs, map.valueType(), contract);
    } else if (type instanceof SetTypeSpec set) {
      recStructs(structs, set.valueType(), contract);
    } else if (type instanceof AvlTreeMapTypeSpec avl) {
      recStructs(structs, avl.keyType(), contract);
      recStructs(structs, avl.valueType(), contract);
    } else if (type instanceof SizedArrayTypeSpec array) {
      recStructs(structs, array.valueType(), contract);
    }
  }

  /**
   * Find all named types that used for state.
   *
   * @param contract contract abi
   * @return set of named state types
   */
  static Set<NamedTypeSpec> findStateStructs(ContractAbi contract) {
    var stateStructs = new HashSet<NamedTypeSpec>();
    recStructs(stateStructs, contract.stateType(), contract);
    return stateStructs;
  }

  /**
   * Find all named struct types that are enum variants.
   *
   * @param contract contract abi
   * @return set of named types
   */
  static Set<NamedTypeSpec> findEnumStructs(ContractAbi contract) {
    var set = new HashSet<NamedTypeSpec>();
    for (var namedType : contract.namedTypes()) {
      if (namedType instanceof EnumTypeSpec enumType) {
        for (var enumVariant : enumType.variants()) {
          set.add(contract.getNamedType(enumVariant.def()));
        }
      }
    }
    return set;
  }

  /**
   * Find all named types that used for special deserializing.
   *
   * @param contract contract abi
   * @param deserializeNamedTypes set of top level named types tp special deserialize
   * @return set of named types
   */
  static Set<NamedTypeSpec> findSpecialDeserializeStructs(
      ContractAbi contract, Set<String> deserializeNamedTypes) {
    var set = new HashSet<NamedTypeSpec>();
    for (NamedTypeSpec namedTypeSpec : contract.namedTypes()) {
      if (deserializeNamedTypes.contains(namedTypeSpec.name())) {
        recStructs(set, new NamedTypeRef(contract.namedTypes().indexOf(namedTypeSpec)), contract);
      }
    }
    return set;
  }

  /**
   * Find all structs that used as keys in avl tree maps.
   *
   * @param contract contract abi
   * @return set of named types
   */
  static Set<NamedTypeSpec> findAvlKeyStructs(ContractAbi contract) {
    var structs = new HashSet<NamedTypeSpec>();
    var avlKeyStructs = new HashSet<NamedTypeSpec>();
    recAvlKeyStructs(structs, avlKeyStructs, contract.stateType(), contract);
    return avlKeyStructs;
  }

  private static void recAvlKeyStructs(
      Set<NamedTypeSpec> structs,
      Set<NamedTypeSpec> avlKeyStructs,
      TypeSpec type,
      ContractAbi contract) {
    if (type instanceof NamedTypeRef namedTypeRef) {
      var namedType = contract.getNamedType(namedTypeRef);
      if (structs.add(namedType)) {
        if (namedType instanceof StructTypeSpec structType) {
          for (var field : structType.fields()) {
            recAvlKeyStructs(structs, avlKeyStructs, field.type(), contract);
          }
        } else {
          var enumType = (EnumTypeSpec) namedType;
          for (var enumVariant : enumType.variants()) {
            recAvlKeyStructs(structs, avlKeyStructs, enumVariant.def(), contract);
          }
        }
      }
    } else if (type instanceof VecTypeSpec vec) {
      recAvlKeyStructs(structs, avlKeyStructs, vec.valueType(), contract);
    } else if (type instanceof OptionTypeSpec option) {
      recAvlKeyStructs(structs, avlKeyStructs, option.valueType(), contract);
    } else if (type instanceof MapTypeSpec map) {
      recAvlKeyStructs(structs, avlKeyStructs, map.keyType(), contract);
      recAvlKeyStructs(structs, avlKeyStructs, map.valueType(), contract);
    } else if (type instanceof SetTypeSpec set) {
      recAvlKeyStructs(structs, avlKeyStructs, set.valueType(), contract);
    } else if (type instanceof AvlTreeMapTypeSpec avl) {
      recStructs(avlKeyStructs, avl.keyType(), contract);
      recAvlKeyStructs(structs, avlKeyStructs, avl.valueType(), contract);
    }
  }

  /**
   * Is given struct the state struct.
   *
   * @param structType struct type to test
   * @param contract contract abi
   * @return true if struct is the state struct
   */
  static boolean isStateStruct(StructTypeSpec structType, ContractAbi contract) {
    var state = contract.getStateStruct();
    return structType.name().equals(state.name());
  }

  /**
   * Is type a vec u8.
   *
   * @param type type to test
   * @return true if type is vec u8
   */
  static boolean isVecU8(TypeSpec type) {
    return type instanceof VecTypeSpec vec && vec.valueType().typeIndex() == TypeSpec.TypeIndex.u8;
  }

  /**
   * Turn type spec type into its java type.
   *
   * @param type type spec
   * @param contractAbi contract abi
   * @return java type
   */
  public static String typeSpecToJavaType(TypeSpec type, ContractAbi contractAbi) {
    return typeName(type, contractAbi);
  }

  /**
   * Turn type spec type into its java type.
   *
   * @param type type spec
   * @param contract contract abi
   * @return java type
   */
  private static String typeName(TypeSpec type, ContractAbi contract) {
    if (type instanceof SimpleTypeSpec simple) {
      return simpleTypeName(simple.typeIndex());
    } else if (isVecU8(type)) {
      return "byte[]";
    } else if (type instanceof VecTypeSpec vec) {
      return "List<" + typeNameNonPrimitive(vec.valueType(), contract) + ">";
    } else if (type instanceof OptionTypeSpec option) {
      return typeNameNonPrimitive(option.valueType(), contract);
    } else if (type instanceof NamedTypeRef namedTypeRef) {
      return contract.getNamedType(namedTypeRef).name();
    } else if (type instanceof MapTypeSpec map) {
      return "Map<"
          + typeNameNonPrimitive(map.keyType(), contract)
          + ", "
          + typeNameNonPrimitive(map.valueType(), contract)
          + ">";
    } else if (type instanceof SetTypeSpec set) {
      return "List<" + typeNameNonPrimitive(set.valueType(), contract) + ">";
    } else if (type instanceof AvlTreeMapTypeSpec map) {
      return "AvlTreeMap<"
          + typeNameNonPrimitive(map.keyType(), contract)
          + ", "
          + typeNameNonPrimitive(map.valueType(), contract)
          + ">";
    } else {
      // SizedArray
      SizedArrayTypeSpec arrayTypeSpec = (SizedArrayTypeSpec) type;
      if (arrayTypeSpec.valueType().typeIndex() == TypeSpec.TypeIndex.u8) {
        return "byte[]";
      } else {
        return "List<" + typeNameNonPrimitive(arrayTypeSpec.valueType(), contract) + ">";
      }
    }
  }

  /**
   * Turn type spec type into its java type. Primitive types are mapped to their boxed version.
   *
   * @param type type spec
   * @param contract contract abi
   * @return java type
   */
  public static String typeNameNonPrimitive(TypeSpec type, ContractAbi contract) {
    if (type instanceof SimpleTypeSpec simple) {
      return simpleTypeNameNonPrimitive(simple.typeIndex());
    } else {
      return typeName(type, contract);
    }
  }

  /**
   * Simple type abi to java type.
   *
   * @param typeIndex type index
   * @return java type name
   */
  private static String simpleTypeName(TypeSpec.TypeIndex typeIndex) {
    return switch (typeIndex) {
      case i8, u8 -> "byte";
      case i16, u16 -> "short";
      case i32, u32 -> "int";
      case i64, u64 -> "long";
      case bool -> "boolean";
      case i128, u128, u256 -> "BigInteger";
      case Address -> "BlockchainAddress";
      case Hash -> "Hash";
      case PublicKey -> "BlockchainPublicKey";
      case Signature -> "Signature";
      case BlsPublicKey -> "BlsPublicKey";
      case BlsSignature -> "BlsSignature";
      default -> "String"; // String
    };
  }

  /**
   * Simple type abi to java type. Primitive types are mapped to their boxed version.
   *
   * @param typeIndex type index
   * @return java type name
   */
  private static String simpleTypeNameNonPrimitive(TypeSpec.TypeIndex typeIndex) {
    return switch (typeIndex) {
      case i8, u8 -> "Byte";
      case i16, u16 -> "Short";
      case i32, u32 -> "Integer";
      case i64, u64 -> "Long";
      case bool -> "Boolean";
      case i128, u128, u256 -> "BigInteger";
      case Address -> "BlockchainAddress";
      case Hash -> "Hash";
      case PublicKey -> "BlockchainPublicKey";
      case Signature -> "Signature";
      case BlsPublicKey -> "BlsPublicKey";
      case BlsSignature -> "BlsSignature";
      default -> "String"; // String
    };
  }

  /**
   * Turn type spec type into its typescript type.
   *
   * @param type type spec
   * @param contract contract abi
   * @return typescript type
   */
  public static String typeSpecToTsType(TypeSpec type, ContractAbi contract) {
    if (type instanceof SimpleTypeSpec simple) {
      return tsSimpleTypeName(simple.typeIndex());
    } else if (isVecU8(type)) {
      return "Buffer";
    } else if (type instanceof VecTypeSpec vec) {
      return tsArrayType(vec.valueType(), contract);
    } else if (type instanceof OptionTypeSpec option) {
      return "Option<" + typeSpecToTsType(option.valueType(), contract) + ">";
    } else if (type instanceof NamedTypeRef namedTypeRef) {
      return contract.getNamedType(namedTypeRef).name();
    } else if (type instanceof MapTypeSpec map) {
      return "Map<"
          + typeSpecToTsType(map.keyType(), contract)
          + ", "
          + typeSpecToTsType(map.valueType(), contract)
          + ">";
    } else if (type instanceof SetTypeSpec set) {
      return tsArrayType(set.valueType(), contract);
    } else if (type instanceof AvlTreeMapTypeSpec map) {
      return "AvlTreeMap<"
          + typeSpecToTsType(map.keyType(), contract)
          + ", "
          + typeSpecToTsType(map.valueType(), contract)
          + ">";
    } else {
      // SizedArray
      SizedArrayTypeSpec arrayTypeSpec = (SizedArrayTypeSpec) type;
      if (arrayTypeSpec.valueType().typeIndex() == TypeSpec.TypeIndex.u8) {
        return "Buffer";
      } else {
        return tsArrayType(arrayTypeSpec.valueType(), contract);
      }
    }
  }

  private static String tsArrayType(TypeSpec inner, ContractAbi contract) {
    var innerType = typeSpecToTsType(inner, contract);
    if (innerType.contains("<")) {
      return "Array<" + innerType + ">";
    } else {
      return innerType + "[]";
    }
  }

  /**
   * Turn simple type spec into its typescript type.
   *
   * @param typeIndex simple type
   * @return typescript type
   */
  private static String tsSimpleTypeName(TypeSpec.TypeIndex typeIndex) {
    return switch (typeIndex) {
      case i8, u8, i16, u16, i32, u32 -> "number";
      case i64, u64, i128, u128, u256 -> "BN";
      case Address -> "BlockchainAddress";
      case Hash -> "Hash";
      case PublicKey -> "BlockchainPublicKey";
      case Signature -> "Signature";
      case BlsPublicKey -> "BlsPublicKey";
      case BlsSignature -> "BlsSignature";
      case bool -> "boolean";
      default -> "string"; // String
    };
  }

  /**
   * Format list of NameAndType into java fields in a record.
   *
   * @param fields fields
   * @param contractAbi contractAbi
   * @return record fields.
   */
  public static String javaFields(List<NameAndType> fields, ContractAbi contractAbi) {
    return fields.stream()
        .map(
            nameAndType ->
                "%s %s"
                    .formatted(
                        typeSpecToJavaType(nameAndType.type(), contractAbi),
                        toCamelCase(nameAndType.name())))
        .collect(Collectors.joining(", "));
  }

  /**
   * Format list of NameAndType into typescript interface member.
   *
   * @param fields fields
   * @param contractAbi contract abi
   * @return typescript interface fields
   */
  public static String tsInterfaceFields(List<NameAndType> fields, ContractAbi contractAbi) {
    return fields.stream()
        .map(
            nameAndType ->
                "  %s: %s;\n"
                    .formatted(
                        toCamelCase(nameAndType.name()),
                        typeSpecToTsType(nameAndType.type(), contractAbi)))
        .collect(Collectors.joining(""));
  }

  /**
   * Format list of NameAndType into typescript function arguments.
   *
   * @param args arguments
   * @param contractAbi contract abi
   * @return typescript function arguments
   */
  public static String tsFunctionArgs(List<NameAndType> args, ContractAbi contractAbi) {
    return args.stream()
        .map(
            nameAndType ->
                "%s: %s"
                    .formatted(
                        toCamelCase(nameAndType.name()),
                        typeSpecToTsType(nameAndType.type(), contractAbi)))
        .collect(Collectors.joining(", "));
  }

  /**
   * Turn snake_case into PascalCase.
   *
   * @param name snake case
   * @return pascal case
   */
  public static String toPascalCase(String name) {
    return Arrays.stream(name.toLowerCase(Locale.ENGLISH).split("_"))
        .map(str -> str.substring(0, 1).toUpperCase(Locale.ENGLISH) + str.substring(1))
        .collect(Collectors.joining());
  }
}
