// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractSizedArrays {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const byteArray: Buffer = _input.readBytes(12);
    const myArray: number[] = [];
    for (let myArray_i = 0; myArray_i < 16; myArray_i++) {
      const myArray_elem: number = _input.readI32();
      myArray.push(myArray_elem);
    }
    const myArray2: number[][] = [];
    for (let myArray2_i = 0; myArray2_i < 4; myArray2_i++) {
      const myArray2_elem: number[] = [];
      for (let myArray2_elem_i = 0; myArray2_elem_i < 4; myArray2_elem_i++) {
        const myArray2_elem_elem: number = _input.readI32();
        myArray2_elem.push(myArray2_elem_elem);
      }
      myArray2.push(myArray2_elem);
    }
    const myArray3: Array<Option<number>> = [];
    for (let myArray3_i = 0; myArray3_i < 5; myArray3_i++) {
      let myArray3_elem: Option<number> = undefined;
      const myArray3_elem_isSome = _input.readBoolean();
      if (myArray3_elem_isSome) {
        const myArray3_elem_option: number = _input.readI32();
        myArray3_elem = myArray3_elem_option;
      }
      myArray3.push(myArray3_elem);
    }
    return { byteArray, myArray, myArray2, myArray3 };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateByteArrayAction(_input: AbiInput): UpdateByteArrayAction {
    const value: Buffer = _input.readBytes(12);
    return { discriminant: "update_byte_array", value };
  }

  public deserializeUpdateMyArrayAction(_input: AbiInput): UpdateMyArrayAction {
    const value: number[] = [];
    for (let value_i = 0; value_i < 16; value_i++) {
      const value_elem: number = _input.readI32();
      value.push(value_elem);
    }
    return { discriminant: "update_my_array", value };
  }

  public deserializeUpdateMyArray2Action(_input: AbiInput): UpdateMyArray2Action {
    const value: number[][] = [];
    for (let value_i = 0; value_i < 4; value_i++) {
      const value_elem: number[] = [];
      for (let value_elem_i = 0; value_elem_i < 4; value_elem_i++) {
        const value_elem_elem: number = _input.readI32();
        value_elem.push(value_elem_elem);
      }
      value.push(value_elem);
    }
    return { discriminant: "update_my_array_2", value };
  }

  public deserializeUpdateMyArray3Action(_input: AbiInput): UpdateMyArray3Action {
    const value: Array<Option<number>> = [];
    for (let value_i = 0; value_i < 5; value_i++) {
      let value_elem: Option<number> = undefined;
      const value_elem_isSome = _input.readBoolean();
      if (value_elem_isSome) {
        const value_elem_option: number = _input.readI32();
        value_elem = value_elem_option;
      }
      value.push(value_elem);
    }
    return { discriminant: "update_my_array_3", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const byteArray: Buffer = _input.readBytes(12);
    const myArray: number[] = [];
    for (let myArray_i = 0; myArray_i < 16; myArray_i++) {
      const myArray_elem: number = _input.readI32();
      myArray.push(myArray_elem);
    }
    const myArray2: number[][] = [];
    for (let myArray2_i = 0; myArray2_i < 4; myArray2_i++) {
      const myArray2_elem: number[] = [];
      for (let myArray2_elem_i = 0; myArray2_elem_i < 4; myArray2_elem_i++) {
        const myArray2_elem_elem: number = _input.readI32();
        myArray2_elem.push(myArray2_elem_elem);
      }
      myArray2.push(myArray2_elem);
    }
    const myArray3: Array<Option<number>> = [];
    for (let myArray3_i = 0; myArray3_i < 5; myArray3_i++) {
      let myArray3_elem: Option<number> = undefined;
      const myArray3_elem_isSome = _input.readBoolean();
      if (myArray3_elem_isSome) {
        const myArray3_elem_option: number = _input.readI32();
        myArray3_elem = myArray3_elem_option;
      }
      myArray3.push(myArray3_elem);
    }
    return { discriminant: "initialize", byteArray, myArray, myArray2, myArray3 };
  }

}
export interface ExampleContractState {
  byteArray: Buffer;
  myArray: number[];
  myArray2: number[][];
  myArray3: Array<Option<number>>;
}

export function initialize(byteArray: Buffer, myArray: number[], myArray2: number[][], myArray3: Array<Option<number>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    if (byteArray.length != 12) {
      throw new Error("Length of byteArray does not match expected 12");
    }
    _out.writeBytes(byteArray);
    if (myArray.length !== 16) {
      throw new Error("Length of myArray does not match expected 16");
    }
    for (const myArray_arr of myArray) {
      _out.writeI32(myArray_arr);
    }
    if (myArray2.length !== 4) {
      throw new Error("Length of myArray2 does not match expected 4");
    }
    for (const myArray2_arr of myArray2) {
      if (myArray2_arr.length !== 4) {
        throw new Error("Length of myArray2_arr does not match expected 4");
      }
      for (const myArray2_arr_arr of myArray2_arr) {
        _out.writeI32(myArray2_arr_arr);
      }
    }
    if (myArray3.length !== 5) {
      throw new Error("Length of myArray3 does not match expected 5");
    }
    for (const myArray3_arr of myArray3) {
      _out.writeBoolean(myArray3_arr !== undefined);
      if (myArray3_arr !== undefined) {
        _out.writeI32(myArray3_arr);
      }
    }
  });
}

export function updateByteArray(value: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("9df1bab60d", "hex"));
    if (value.length != 12) {
      throw new Error("Length of value does not match expected 12");
    }
    _out.writeBytes(value);
  });
}

export function updateMyArray(value: number[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("cbe4cbfd0d", "hex"));
    if (value.length !== 16) {
      throw new Error("Length of value does not match expected 16");
    }
    for (const value_arr of value) {
      _out.writeI32(value_arr);
    }
  });
}

export function updateMyArray2(value: number[][]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ec9da775", "hex"));
    if (value.length !== 4) {
      throw new Error("Length of value does not match expected 4");
    }
    for (const value_arr of value) {
      if (value_arr.length !== 4) {
        throw new Error("Length of value_arr does not match expected 4");
      }
      for (const value_arr_arr of value_arr) {
        _out.writeI32(value_arr_arr);
      }
    }
  });
}

export function updateMyArray3(value: Array<Option<number>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f3d683af06", "hex"));
    if (value.length !== 5) {
      throw new Error("Length of value does not match expected 5");
    }
    for (const value_arr of value) {
      _out.writeBoolean(value_arr !== undefined);
      if (value_arr !== undefined) {
        _out.writeI32(value_arr);
      }
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractSizedArrays(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractSizedArrays(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateByteArrayAction
  | UpdateMyArrayAction
  | UpdateMyArray2Action
  | UpdateMyArray3Action;

export interface UpdateByteArrayAction {
  discriminant: "update_byte_array";
  value: Buffer;
}
export interface UpdateMyArrayAction {
  discriminant: "update_my_array";
  value: number[];
}
export interface UpdateMyArray2Action {
  discriminant: "update_my_array_2";
  value: number[][];
}
export interface UpdateMyArray3Action {
  discriminant: "update_my_array_3";
  value: Array<Option<number>>;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSizedArrays(undefined, undefined);
  if (shortname === "9df1bab60d") {
    return contract.deserializeUpdateByteArrayAction(input);
  } else if (shortname === "cbe4cbfd0d") {
    return contract.deserializeUpdateMyArrayAction(input);
  } else if (shortname === "ec9da775") {
    return contract.deserializeUpdateMyArray2Action(input);
  } else if (shortname === "f3d683af06") {
    return contract.deserializeUpdateMyArray3Action(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  byteArray: Buffer;
  myArray: number[];
  myArray2: number[][];
  myArray3: Array<Option<number>>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSizedArrays(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

