// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class TransactionAndEventDeserialization_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeSignedTransaction(_input: AbiInput): SignedTransaction {
    const signature: Signature = this.deserializeSignature(_input);
    const innerPart: InnerPart = this.deserializeInnerPart(_input);
    return { signature, innerPart };
  }
  public deserializeInnerPart(_input: AbiInput): InnerPart {
    const core: CoreTransactionPart = this.deserializeCoreTransactionPart(_input);
    const transaction: InteractWithContractTransaction = this.deserializeInteractWithContractTransaction(_input);
    return { core, transaction };
  }
  public deserializeCoreTransactionPart(_input: AbiInput): CoreTransactionPart {
    const nonce: BN = _input.readI64();
    const validToTime: BN = _input.readI64();
    const cost: BN = _input.readI64();
    return { nonce, validToTime, cost };
  }
  public deserializeInteractWithContractTransaction(_input: AbiInput): InteractWithContractTransaction {
    const contractId: BlockchainAddress = _input.readAddress();
    const payload_vecLength = _input.readI32();
    const payload: Buffer = _input.readBytes(payload_vecLength);
    return { contractId, payload };
  }
  public deserializeExecutableEvent(_input: AbiInput): ExecutableEvent {
    let originShard: Option<string> = undefined;
    const originShard_isSome = _input.readBoolean();
    if (originShard_isSome) {
      const originShard_option: string = _input.readString();
      originShard = originShard_option;
    }
    const eventTransaction: EventTransaction = this.deserializeEventTransaction(_input);
    return { originShard, eventTransaction };
  }
  public deserializeEventTransaction(_input: AbiInput): EventTransaction {
    const hash: Buffer = _input.readBytes(32);
    const innerEvent: InnerEvent = this.deserializeInnerEvent(_input);
    const shardRoute: ShardRoute = this.deserializeShardRoute(_input);
    const committeeId: BN = _input.readI64();
    const governanceVersion: BN = _input.readI64();
    const height: number = _input.readU8();
    let returnEnvelope: Option<ReturnEnvelope> = undefined;
    const returnEnvelope_isSome = _input.readBoolean();
    if (returnEnvelope_isSome) {
      const returnEnvelope_option: ReturnEnvelope = this.deserializeReturnEnvelope(_input);
      returnEnvelope = returnEnvelope_option;
    }
    return { hash, innerEvent, shardRoute, committeeId, governanceVersion, height, returnEnvelope };
  }
  public deserializeInnerEvent(_input: AbiInput): InnerEvent {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeInnerEventTransaction(_input);
    } else if (discriminant === 1) {
      return this.deserializeInnerEventCallback(_input);
    } else if (discriminant === 2) {
      return this.deserializeInnerEventSystem(_input);
    } else if (discriminant === 3) {
      return this.deserializeInnerEventSync(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeInnerEventTransaction(_input: AbiInput): InnerEventTransaction {
    const innerTransaction: InnerTransaction = this.deserializeInnerTransaction(_input);
    return { discriminant: InnerEventD.Transaction, innerTransaction };
  }
  public deserializeInnerEventCallback(_input: AbiInput): InnerEventCallback {
    const callbackToContract: CallbackToContract = this.deserializeCallbackToContract(_input);
    return { discriminant: InnerEventD.Callback, callbackToContract };
  }
  public deserializeInnerEventSystem(_input: AbiInput): InnerEventSystem {
    const innerSystemEvent: InnerSystemEvent = this.deserializeInnerSystemEvent(_input);
    return { discriminant: InnerEventD.System, innerSystemEvent };
  }
  public deserializeInnerEventSync(_input: AbiInput): InnerEventSync {
    const syncEvent: SyncEvent = this.deserializeSyncEvent(_input);
    return { discriminant: InnerEventD.Sync, syncEvent };
  }
  public deserializeInnerTransaction(_input: AbiInput): InnerTransaction {
    const from: BlockchainAddress = _input.readAddress();
    const cost: BN = _input.readI64();
    const transaction: Transaction = this.deserializeTransaction(_input);
    return { from, cost, transaction };
  }
  public deserializeCallbackToContract(_input: AbiInput): CallbackToContract {
    const address: BlockchainAddress = _input.readAddress();
    const callbackIdentifier: Buffer = _input.readBytes(32);
    const from: BlockchainAddress = _input.readAddress();
    const cost: BN = _input.readI64();
    const callbackRpc_vecLength = _input.readI32();
    const callbackRpc: Buffer = _input.readBytes(callbackRpc_vecLength);
    return { address, callbackIdentifier, from, cost, callbackRpc };
  }
  public deserializeInnerSystemEvent(_input: AbiInput): InnerSystemEvent {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeInnerSystemEventCreateAccount(_input);
    } else if (discriminant === 1) {
      return this.deserializeInnerSystemEventCheckExistence(_input);
    } else if (discriminant === 2) {
      return this.deserializeInnerSystemEventSetFeature(_input);
    } else if (discriminant === 3) {
      return this.deserializeInnerSystemEventUpdateLocalPluginState(_input);
    } else if (discriminant === 4) {
      return this.deserializeInnerSystemEventUpdateGlobalPluginState(_input);
    } else if (discriminant === 5) {
      return this.deserializeInnerSystemEventUpdatePlugin(_input);
    } else if (discriminant === 6) {
      return this.deserializeInnerSystemEventCallback(_input);
    } else if (discriminant === 7) {
      return this.deserializeInnerSystemEventCreateShard(_input);
    } else if (discriminant === 8) {
      return this.deserializeInnerSystemEventRemoveShard(_input);
    } else if (discriminant === 9) {
      return this.deserializeInnerSystemEventUpdateContextFreePluginState(_input);
    } else if (discriminant === 10) {
      return this.deserializeInnerSystemEventUpgradeSystemContract(_input);
    } else if (discriminant === 11) {
      return this.deserializeInnerSystemEventRemoveContract(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeInnerSystemEventCreateAccount(_input: AbiInput): InnerSystemEventCreateAccount {
    const createAccountEvent: CreateAccountEvent = this.deserializeCreateAccountEvent(_input);
    return { discriminant: InnerSystemEventD.CreateAccount, createAccountEvent };
  }
  public deserializeInnerSystemEventCheckExistence(_input: AbiInput): InnerSystemEventCheckExistence {
    const checkExistenceEvent: CheckExistenceEvent = this.deserializeCheckExistenceEvent(_input);
    return { discriminant: InnerSystemEventD.CheckExistence, checkExistenceEvent };
  }
  public deserializeInnerSystemEventSetFeature(_input: AbiInput): InnerSystemEventSetFeature {
    const setFeatureEvent: SetFeatureEvent = this.deserializeSetFeatureEvent(_input);
    return { discriminant: InnerSystemEventD.SetFeature, setFeatureEvent };
  }
  public deserializeInnerSystemEventUpdateLocalPluginState(_input: AbiInput): InnerSystemEventUpdateLocalPluginState {
    const updateLocalPluginStateEvent: UpdateLocalPluginStateEvent = this.deserializeUpdateLocalPluginStateEvent(_input);
    return { discriminant: InnerSystemEventD.UpdateLocalPluginState, updateLocalPluginStateEvent };
  }
  public deserializeInnerSystemEventUpdateGlobalPluginState(_input: AbiInput): InnerSystemEventUpdateGlobalPluginState {
    const updateGlobalPluginStateEvent: UpdateGlobalPluginStateEvent = this.deserializeUpdateGlobalPluginStateEvent(_input);
    return { discriminant: InnerSystemEventD.UpdateGlobalPluginState, updateGlobalPluginStateEvent };
  }
  public deserializeInnerSystemEventUpdatePlugin(_input: AbiInput): InnerSystemEventUpdatePlugin {
    const updatePluginEvent: UpdatePluginEvent = this.deserializeUpdatePluginEvent(_input);
    return { discriminant: InnerSystemEventD.UpdatePlugin, updatePluginEvent };
  }
  public deserializeInnerSystemEventCallback(_input: AbiInput): InnerSystemEventCallback {
    const callbackEvent: CallbackEvent = this.deserializeCallbackEvent(_input);
    return { discriminant: InnerSystemEventD.Callback, callbackEvent };
  }
  public deserializeInnerSystemEventCreateShard(_input: AbiInput): InnerSystemEventCreateShard {
    const createShardEvent: CreateShardEvent = this.deserializeCreateShardEvent(_input);
    return { discriminant: InnerSystemEventD.CreateShard, createShardEvent };
  }
  public deserializeInnerSystemEventRemoveShard(_input: AbiInput): InnerSystemEventRemoveShard {
    const removeShardEvent: RemoveShardEvent = this.deserializeRemoveShardEvent(_input);
    return { discriminant: InnerSystemEventD.RemoveShard, removeShardEvent };
  }
  public deserializeInnerSystemEventUpdateContextFreePluginState(_input: AbiInput): InnerSystemEventUpdateContextFreePluginState {
    const updateContextFreePluginState: UpdateContextFreePluginState = this.deserializeUpdateContextFreePluginState(_input);
    return { discriminant: InnerSystemEventD.UpdateContextFreePluginState, updateContextFreePluginState };
  }
  public deserializeInnerSystemEventUpgradeSystemContract(_input: AbiInput): InnerSystemEventUpgradeSystemContract {
    const upgradeSystemContractEvent: UpgradeSystemContractEvent = this.deserializeUpgradeSystemContractEvent(_input);
    return { discriminant: InnerSystemEventD.UpgradeSystemContract, upgradeSystemContractEvent };
  }
  public deserializeInnerSystemEventRemoveContract(_input: AbiInput): InnerSystemEventRemoveContract {
    const removeContract: RemoveContract = this.deserializeRemoveContract(_input);
    return { discriminant: InnerSystemEventD.RemoveContract, removeContract };
  }
  public deserializeCreateAccountEvent(_input: AbiInput): CreateAccountEvent {
    const toCreate: BlockchainAddress = _input.readAddress();
    return { toCreate };
  }
  public deserializeCheckExistenceEvent(_input: AbiInput): CheckExistenceEvent {
    const contractOrAccountAddress: BlockchainAddress = _input.readAddress();
    return { contractOrAccountAddress };
  }
  public deserializeSetFeatureEvent(_input: AbiInput): SetFeatureEvent {
    const key: string = _input.readString();
    let value: Option<string> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: string = _input.readString();
      value = value_option;
    }
    return { key, value };
  }
  public deserializeUpdateLocalPluginStateEvent(_input: AbiInput): UpdateLocalPluginStateEvent {
    const chainPluginType: ChainPluginType = this.deserializeChainPluginType(_input);
    const update: LocalPluginStateUpdate = this.deserializeLocalPluginStateUpdate(_input);
    return { chainPluginType, update };
  }
  public deserializeLocalPluginStateUpdate(_input: AbiInput): LocalPluginStateUpdate {
    const context: BlockchainAddress = _input.readAddress();
    const rpc_vecLength = _input.readI32();
    const rpc: Buffer = _input.readBytes(rpc_vecLength);
    return { context, rpc };
  }
  public deserializeChainPluginType(_input: AbiInput): ChainPluginType {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeChainPluginTypeAccount(_input);
    } else if (discriminant === 1) {
      return this.deserializeChainPluginTypeConsensus(_input);
    } else if (discriminant === 2) {
      return this.deserializeChainPluginTypeRouting(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeChainPluginTypeAccount(_input: AbiInput): ChainPluginTypeAccount {
    return { discriminant: ChainPluginTypeD.Account,  };
  }
  public deserializeChainPluginTypeConsensus(_input: AbiInput): ChainPluginTypeConsensus {
    return { discriminant: ChainPluginTypeD.Consensus,  };
  }
  public deserializeChainPluginTypeRouting(_input: AbiInput): ChainPluginTypeRouting {
    return { discriminant: ChainPluginTypeD.Routing,  };
  }
  public deserializeUpdateGlobalPluginStateEvent(_input: AbiInput): UpdateGlobalPluginStateEvent {
    const chainPluginType: ChainPluginType = this.deserializeChainPluginType(_input);
    const update: GlobalPluginStateUpdate = this.deserializeGlobalPluginStateUpdate(_input);
    return { chainPluginType, update };
  }
  public deserializeGlobalPluginStateUpdate(_input: AbiInput): GlobalPluginStateUpdate {
    const rpc_vecLength = _input.readI32();
    const rpc: Buffer = _input.readBytes(rpc_vecLength);
    return { rpc };
  }
  public deserializeUpdatePluginEvent(_input: AbiInput): UpdatePluginEvent {
    const chainPluginType: ChainPluginType = this.deserializeChainPluginType(_input);
    let jar: Option<Buffer> = undefined;
    const jar_isSome = _input.readBoolean();
    if (jar_isSome) {
      const jar_option_vecLength = _input.readI32();
      const jar_option: Buffer = _input.readBytes(jar_option_vecLength);
      jar = jar_option;
    }
    const invocation_vecLength = _input.readI32();
    const invocation: Buffer = _input.readBytes(invocation_vecLength);
    return { chainPluginType, jar, invocation };
  }
  public deserializeCallbackEvent(_input: AbiInput): CallbackEvent {
    const returnEnvelope: ReturnEnvelope = this.deserializeReturnEnvelope(_input);
    const completedTransaction: Buffer = _input.readBytes(32);
    const success: boolean = _input.readBoolean();
    const returnValue_vecLength = _input.readI32();
    const returnValue: Buffer = _input.readBytes(returnValue_vecLength);
    return { returnEnvelope, completedTransaction, success, returnValue };
  }
  public deserializeCreateShardEvent(_input: AbiInput): CreateShardEvent {
    const shardId: string = _input.readString();
    return { shardId };
  }
  public deserializeRemoveShardEvent(_input: AbiInput): RemoveShardEvent {
    const shardId: string = _input.readString();
    return { shardId };
  }
  public deserializeUpdateContextFreePluginState(_input: AbiInput): UpdateContextFreePluginState {
    const chainPluginType: ChainPluginType = this.deserializeChainPluginType(_input);
    const rpc_vecLength = _input.readI32();
    const rpc: Buffer = _input.readBytes(rpc_vecLength);
    return { chainPluginType, rpc };
  }
  public deserializeUpgradeSystemContractEvent(_input: AbiInput): UpgradeSystemContractEvent {
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const binderJar_vecLength = _input.readI32();
    const binderJar: Buffer = _input.readBytes(binderJar_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const rpc_vecLength = _input.readI32();
    const rpc: Buffer = _input.readBytes(rpc_vecLength);
    const contractAddress: BlockchainAddress = _input.readAddress();
    return { contractJar, binderJar, abi, rpc, contractAddress };
  }
  public deserializeRemoveContract(_input: AbiInput): RemoveContract {
    const contract: BlockchainAddress = _input.readAddress();
    return { contract };
  }
  public deserializeSyncEvent(_input: AbiInput): SyncEvent {
    const accounts_vecLength = _input.readI32();
    const accounts: AccountTransfer[] = [];
    for (let accounts_i = 0; accounts_i < accounts_vecLength; accounts_i++) {
      const accounts_elem: AccountTransfer = this.deserializeAccountTransfer(_input);
      accounts.push(accounts_elem);
    }
    return { accounts };
  }
  public deserializeAccountTransfer(_input: AbiInput): AccountTransfer {
    const address: BlockchainAddress = _input.readAddress();
    const nonce: BN = _input.readI64();
    return { address, nonce };
  }
  public deserializeShardRoute(_input: AbiInput): ShardRoute {
    let targetShard: Option<string> = undefined;
    const targetShard_isSome = _input.readBoolean();
    if (targetShard_isSome) {
      const targetShard_option: string = _input.readString();
      targetShard = targetShard_option;
    }
    const nonce: BN = _input.readI64();
    return { targetShard, nonce };
  }
  public deserializeReturnEnvelope(_input: AbiInput): ReturnEnvelope {
    const contract: BlockchainAddress = _input.readAddress();
    return { contract };
  }
  public deserializeTransaction(_input: AbiInput): Transaction {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeTransactionDeployContract(_input);
    } else if (discriminant === 1) {
      return this.deserializeTransactionInteractContract(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeTransactionDeployContract(_input: AbiInput): TransactionDeployContract {
    const createContractTransaction: CreateContractTransaction = this.deserializeCreateContractTransaction(_input);
    return { discriminant: TransactionD.DeployContract, createContractTransaction };
  }
  public deserializeTransactionInteractContract(_input: AbiInput): TransactionInteractContract {
    const interactWithContractTransaction: InteractWithContractTransaction = this.deserializeInteractWithContractTransaction(_input);
    return { discriminant: TransactionD.InteractContract, interactWithContractTransaction };
  }
  public deserializeCreateContractTransaction(_input: AbiInput): CreateContractTransaction {
    const address: BlockchainAddress = _input.readAddress();
    const binderJar_vecLength = _input.readI32();
    const binderJar: Buffer = _input.readBytes(binderJar_vecLength);
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const rpc_vecLength = _input.readI32();
    const rpc: Buffer = _input.readBytes(rpc_vecLength);
    return { address, binderJar, contractJar, abi, rpc };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    return {  };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU8();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface SignedTransaction {
  signature: Signature;
  innerPart: InnerPart;
}

export function deserializeSpecialSignedTransaction(bytes: Buffer): SignedTransaction {
  const input = AbiByteInput.createBigEndian(bytes);
  return new TransactionAndEventDeserialization_SDK_9_1_2(undefined, undefined).deserializeSignedTransaction(input);
}
export interface InnerPart {
  core: CoreTransactionPart;
  transaction: InteractWithContractTransaction;
}

export interface CoreTransactionPart {
  nonce: BN;
  validToTime: BN;
  cost: BN;
}

export interface InteractWithContractTransaction {
  contractId: BlockchainAddress;
  payload: Buffer;
}

export interface ExecutableEvent {
  originShard: Option<string>;
  eventTransaction: EventTransaction;
}

export function deserializeSpecialExecutableEvent(bytes: Buffer): ExecutableEvent {
  const input = AbiByteInput.createBigEndian(bytes);
  return new TransactionAndEventDeserialization_SDK_9_1_2(undefined, undefined).deserializeExecutableEvent(input);
}
export interface EventTransaction {
  hash: Buffer;
  innerEvent: InnerEvent;
  shardRoute: ShardRoute;
  committeeId: BN;
  governanceVersion: BN;
  height: number;
  returnEnvelope: Option<ReturnEnvelope>;
}

export enum InnerEventD {
  Transaction = 0,
  Callback = 1,
  System = 2,
  Sync = 3,
}
export type InnerEvent =
  | InnerEventTransaction
  | InnerEventCallback
  | InnerEventSystem
  | InnerEventSync;

export interface InnerEventTransaction {
  discriminant: InnerEventD.Transaction;
  innerTransaction: InnerTransaction;
}

export interface InnerEventCallback {
  discriminant: InnerEventD.Callback;
  callbackToContract: CallbackToContract;
}

export interface InnerEventSystem {
  discriminant: InnerEventD.System;
  innerSystemEvent: InnerSystemEvent;
}

export interface InnerEventSync {
  discriminant: InnerEventD.Sync;
  syncEvent: SyncEvent;
}

export interface InnerTransaction {
  from: BlockchainAddress;
  cost: BN;
  transaction: Transaction;
}

export interface CallbackToContract {
  address: BlockchainAddress;
  callbackIdentifier: Buffer;
  from: BlockchainAddress;
  cost: BN;
  callbackRpc: Buffer;
}

export enum InnerSystemEventD {
  CreateAccount = 0,
  CheckExistence = 1,
  SetFeature = 2,
  UpdateLocalPluginState = 3,
  UpdateGlobalPluginState = 4,
  UpdatePlugin = 5,
  Callback = 6,
  CreateShard = 7,
  RemoveShard = 8,
  UpdateContextFreePluginState = 9,
  UpgradeSystemContract = 10,
  RemoveContract = 11,
}
export type InnerSystemEvent =
  | InnerSystemEventCreateAccount
  | InnerSystemEventCheckExistence
  | InnerSystemEventSetFeature
  | InnerSystemEventUpdateLocalPluginState
  | InnerSystemEventUpdateGlobalPluginState
  | InnerSystemEventUpdatePlugin
  | InnerSystemEventCallback
  | InnerSystemEventCreateShard
  | InnerSystemEventRemoveShard
  | InnerSystemEventUpdateContextFreePluginState
  | InnerSystemEventUpgradeSystemContract
  | InnerSystemEventRemoveContract;

export interface InnerSystemEventCreateAccount {
  discriminant: InnerSystemEventD.CreateAccount;
  createAccountEvent: CreateAccountEvent;
}

export interface InnerSystemEventCheckExistence {
  discriminant: InnerSystemEventD.CheckExistence;
  checkExistenceEvent: CheckExistenceEvent;
}

export interface InnerSystemEventSetFeature {
  discriminant: InnerSystemEventD.SetFeature;
  setFeatureEvent: SetFeatureEvent;
}

export interface InnerSystemEventUpdateLocalPluginState {
  discriminant: InnerSystemEventD.UpdateLocalPluginState;
  updateLocalPluginStateEvent: UpdateLocalPluginStateEvent;
}

export interface InnerSystemEventUpdateGlobalPluginState {
  discriminant: InnerSystemEventD.UpdateGlobalPluginState;
  updateGlobalPluginStateEvent: UpdateGlobalPluginStateEvent;
}

export interface InnerSystemEventUpdatePlugin {
  discriminant: InnerSystemEventD.UpdatePlugin;
  updatePluginEvent: UpdatePluginEvent;
}

export interface InnerSystemEventCallback {
  discriminant: InnerSystemEventD.Callback;
  callbackEvent: CallbackEvent;
}

export interface InnerSystemEventCreateShard {
  discriminant: InnerSystemEventD.CreateShard;
  createShardEvent: CreateShardEvent;
}

export interface InnerSystemEventRemoveShard {
  discriminant: InnerSystemEventD.RemoveShard;
  removeShardEvent: RemoveShardEvent;
}

export interface InnerSystemEventUpdateContextFreePluginState {
  discriminant: InnerSystemEventD.UpdateContextFreePluginState;
  updateContextFreePluginState: UpdateContextFreePluginState;
}

export interface InnerSystemEventUpgradeSystemContract {
  discriminant: InnerSystemEventD.UpgradeSystemContract;
  upgradeSystemContractEvent: UpgradeSystemContractEvent;
}

export interface InnerSystemEventRemoveContract {
  discriminant: InnerSystemEventD.RemoveContract;
  removeContract: RemoveContract;
}

export interface CreateAccountEvent {
  toCreate: BlockchainAddress;
}

export interface CheckExistenceEvent {
  contractOrAccountAddress: BlockchainAddress;
}

export interface SetFeatureEvent {
  key: string;
  value: Option<string>;
}

export interface UpdateLocalPluginStateEvent {
  chainPluginType: ChainPluginType;
  update: LocalPluginStateUpdate;
}

export interface LocalPluginStateUpdate {
  context: BlockchainAddress;
  rpc: Buffer;
}

export enum ChainPluginTypeD {
  Account = 0,
  Consensus = 1,
  Routing = 2,
}
export type ChainPluginType =
  | ChainPluginTypeAccount
  | ChainPluginTypeConsensus
  | ChainPluginTypeRouting;

export interface ChainPluginTypeAccount {
  discriminant: ChainPluginTypeD.Account;
}

export interface ChainPluginTypeConsensus {
  discriminant: ChainPluginTypeD.Consensus;
}

export interface ChainPluginTypeRouting {
  discriminant: ChainPluginTypeD.Routing;
}

export interface UpdateGlobalPluginStateEvent {
  chainPluginType: ChainPluginType;
  update: GlobalPluginStateUpdate;
}

export interface GlobalPluginStateUpdate {
  rpc: Buffer;
}

export interface UpdatePluginEvent {
  chainPluginType: ChainPluginType;
  jar: Option<Buffer>;
  invocation: Buffer;
}

export interface CallbackEvent {
  returnEnvelope: ReturnEnvelope;
  completedTransaction: Buffer;
  success: boolean;
  returnValue: Buffer;
}

export interface CreateShardEvent {
  shardId: string;
}

export interface RemoveShardEvent {
  shardId: string;
}

export interface UpdateContextFreePluginState {
  chainPluginType: ChainPluginType;
  rpc: Buffer;
}

export interface UpgradeSystemContractEvent {
  contractJar: Buffer;
  binderJar: Buffer;
  abi: Buffer;
  rpc: Buffer;
  contractAddress: BlockchainAddress;
}

export interface RemoveContract {
  contract: BlockchainAddress;
}

export interface SyncEvent {
  accounts: AccountTransfer[];
}

export interface AccountTransfer {
  address: BlockchainAddress;
  nonce: BN;
}

export interface ShardRoute {
  targetShard: Option<string>;
  nonce: BN;
}

export interface ReturnEnvelope {
  contract: BlockchainAddress;
}

export enum TransactionD {
  DeployContract = 0,
  InteractContract = 1,
}
export type Transaction =
  | TransactionDeployContract
  | TransactionInteractContract;

export interface TransactionDeployContract {
  discriminant: TransactionD.DeployContract;
  createContractTransaction: CreateContractTransaction;
}

export interface TransactionInteractContract {
  discriminant: TransactionD.InteractContract;
  interactWithContractTransaction: InteractWithContractTransaction;
}

export interface CreateContractTransaction {
  address: BlockchainAddress;
  binderJar: Buffer;
  contractJar: Buffer;
  abi: Buffer;
  rpc: Buffer;
}

export interface ContractState {
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new TransactionAndEventDeserialization_SDK_9_1_2(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new TransactionAndEventDeserialization_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new TransactionAndEventDeserialization_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

