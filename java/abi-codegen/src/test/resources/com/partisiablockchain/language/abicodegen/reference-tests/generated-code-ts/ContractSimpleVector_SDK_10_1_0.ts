// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractSimpleVector_SDK_10_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myVec_vecLength = _input.readI32();
    const myVec: BN[] = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      const myVec_elem: BN = _input.readU64();
      myVec.push(myVec_elem);
    }
    return { myVec };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyVecAction(_input: AbiInput): UpdateMyVecAction {
    const value_vecLength = _input.readI32();
    const value: BN[] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem: BN = _input.readU64();
      value.push(value_elem);
    }
    return { discriminant: "update_my_vec", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myVec_vecLength = _input.readI32();
    const myVec: BN[] = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      const myVec_elem: BN = _input.readU64();
      myVec.push(myVec_elem);
    }
    return { discriminant: "initialize", myVec };
  }

}
export interface ExampleContractState {
  myVec: BN[];
}

export function initialize(myVec: BN[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(myVec.length);
    for (const myVec_vec of myVec) {
      _out.writeU64(myVec_vec);
    }
  });
}

export function updateMyVec(value: BN[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("9d99e6e401", "hex"));
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeU64(value_vec);
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractSimpleVector_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractSimpleVector_SDK_10_1_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyVecAction;

export interface UpdateMyVecAction {
  discriminant: "update_my_vec";
  value: BN[];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSimpleVector_SDK_10_1_0(undefined, undefined);
  if (shortname === "9d99e6e401") {
    return contract.deserializeUpdateMyVecAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myVec: BN[];
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSimpleVector_SDK_10_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

