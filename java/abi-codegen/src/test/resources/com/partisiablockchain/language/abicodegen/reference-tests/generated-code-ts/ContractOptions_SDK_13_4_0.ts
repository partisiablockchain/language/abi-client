// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractOptions_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    let optionU64: Option<BN> = undefined;
    const optionU64_isSome = _input.readBoolean();
    if (optionU64_isSome) {
      const optionU64_option: BN = _input.readU64();
      optionU64 = optionU64_option;
    }
    let optionString: Option<string> = undefined;
    const optionString_isSome = _input.readBoolean();
    if (optionString_isSome) {
      const optionString_option: string = _input.readString();
      optionString = optionString_option;
    }
    let optionAddress: Option<BlockchainAddress> = undefined;
    const optionAddress_isSome = _input.readBoolean();
    if (optionAddress_isSome) {
      const optionAddress_option: BlockchainAddress = _input.readAddress();
      optionAddress = optionAddress_option;
    }
    let optionBoolean: Option<boolean> = undefined;
    const optionBoolean_isSome = _input.readBoolean();
    if (optionBoolean_isSome) {
      const optionBoolean_option: boolean = _input.readBoolean();
      optionBoolean = optionBoolean_option;
    }
    let optionMap: Option<Map<BN, BN>> = undefined;
    const optionMap_isSome = _input.readBoolean();
    if (optionMap_isSome) {
      const optionMap_option_mapLength = _input.readI32();
      const optionMap_option: Map<BN, BN> = new Map();
      for (let optionMap_option_i = 0; optionMap_option_i < optionMap_option_mapLength; optionMap_option_i++) {
        const optionMap_option_key: BN = _input.readU64();
        const optionMap_option_value: BN = _input.readU64();
        optionMap_option.set(optionMap_option_key, optionMap_option_value);
      }
      optionMap = optionMap_option;
    }
    return { optionU64, optionString, optionAddress, optionBoolean, optionMap };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateU64Action(_input: AbiInput): UpdateU64Action {
    let value: Option<BN> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: BN = _input.readU64();
      value = value_option;
    }
    return { discriminant: "update_u64", value };
  }

  public deserializeUpdateStringAction(_input: AbiInput): UpdateStringAction {
    let value: Option<string> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: string = _input.readString();
      value = value_option;
    }
    return { discriminant: "update_string", value };
  }

  public deserializeUpdateAddressAction(_input: AbiInput): UpdateAddressAction {
    let value: Option<BlockchainAddress> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: BlockchainAddress = _input.readAddress();
      value = value_option;
    }
    return { discriminant: "update_address", value };
  }

  public deserializeUpdateBooleanAction(_input: AbiInput): UpdateBooleanAction {
    let value: Option<boolean> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: boolean = _input.readBoolean();
      value = value_option;
    }
    return { discriminant: "update_boolean", value };
  }

  public deserializeAddEntryToMapAction(_input: AbiInput): AddEntryToMapAction {
    const key: BN = _input.readU64();
    const value: BN = _input.readU64();
    return { discriminant: "add_entry_to_map", key, value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    let optionU64: Option<BN> = undefined;
    const optionU64_isSome = _input.readBoolean();
    if (optionU64_isSome) {
      const optionU64_option: BN = _input.readU64();
      optionU64 = optionU64_option;
    }
    let optionString: Option<string> = undefined;
    const optionString_isSome = _input.readBoolean();
    if (optionString_isSome) {
      const optionString_option: string = _input.readString();
      optionString = optionString_option;
    }
    let optionAddress: Option<BlockchainAddress> = undefined;
    const optionAddress_isSome = _input.readBoolean();
    if (optionAddress_isSome) {
      const optionAddress_option: BlockchainAddress = _input.readAddress();
      optionAddress = optionAddress_option;
    }
    let optionBoolean: Option<boolean> = undefined;
    const optionBoolean_isSome = _input.readBoolean();
    if (optionBoolean_isSome) {
      const optionBoolean_option: boolean = _input.readBoolean();
      optionBoolean = optionBoolean_option;
    }
    return { discriminant: "initialize", optionU64, optionString, optionAddress, optionBoolean };
  }

}
export interface ExampleContractState {
  optionU64: Option<BN>;
  optionString: Option<string>;
  optionAddress: Option<BlockchainAddress>;
  optionBoolean: Option<boolean>;
  optionMap: Option<Map<BN, BN>>;
}

export function initialize(optionU64: Option<BN>, optionString: Option<string>, optionAddress: Option<BlockchainAddress>, optionBoolean: Option<boolean>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeBoolean(optionU64 !== undefined);
    if (optionU64 !== undefined) {
      _out.writeU64(optionU64);
    }
    _out.writeBoolean(optionString !== undefined);
    if (optionString !== undefined) {
      _out.writeString(optionString);
    }
    _out.writeBoolean(optionAddress !== undefined);
    if (optionAddress !== undefined) {
      _out.writeAddress(optionAddress);
    }
    _out.writeBoolean(optionBoolean !== undefined);
    if (optionBoolean !== undefined) {
      _out.writeBoolean(optionBoolean);
    }
  });
}

export function updateU64(value: Option<BN>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("cf9cffe90b", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeU64(value);
    }
  });
}

export function updateString(value: Option<string>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f3eae9b808", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeString(value);
    }
  });
}

export function updateAddress(value: Option<BlockchainAddress>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("acabb1e901", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeAddress(value);
    }
  });
}

export function updateBoolean(value: Option<boolean>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("fc8ecfda06", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeBoolean(value);
    }
  });
}

export function addEntryToMap(key: BN, value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("dbcbb3e00c", "hex"));
    _out.writeU64(key);
    _out.writeU64(value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractOptions_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractOptions_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateU64Action
  | UpdateStringAction
  | UpdateAddressAction
  | UpdateBooleanAction
  | AddEntryToMapAction;

export interface UpdateU64Action {
  discriminant: "update_u64";
  value: Option<BN>;
}
export interface UpdateStringAction {
  discriminant: "update_string";
  value: Option<string>;
}
export interface UpdateAddressAction {
  discriminant: "update_address";
  value: Option<BlockchainAddress>;
}
export interface UpdateBooleanAction {
  discriminant: "update_boolean";
  value: Option<boolean>;
}
export interface AddEntryToMapAction {
  discriminant: "add_entry_to_map";
  key: BN;
  value: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptions_SDK_13_4_0(undefined, undefined);
  if (shortname === "cf9cffe90b") {
    return contract.deserializeUpdateU64Action(input);
  } else if (shortname === "f3eae9b808") {
    return contract.deserializeUpdateStringAction(input);
  } else if (shortname === "acabb1e901") {
    return contract.deserializeUpdateAddressAction(input);
  } else if (shortname === "fc8ecfda06") {
    return contract.deserializeUpdateBooleanAction(input);
  } else if (shortname === "dbcbb3e00c") {
    return contract.deserializeAddEntryToMapAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  optionU64: Option<BN>;
  optionString: Option<string>;
  optionAddress: Option<BlockchainAddress>;
  optionBoolean: Option<boolean>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptions_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

