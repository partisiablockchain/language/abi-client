// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractReturnDataCaller_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractReturnDataCaller_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private U8Struct deserializeU8Struct(AbiInput _input) {
    byte u8Value = _input.readU8();
    var u8Vector_vecLength = _input.readI32();
    byte[] u8Vector = _input.readBytes(u8Vector_vecLength);
    var u8Struct_vecLength = _input.readI32();
    List<U8Struct> u8Struct = new ArrayList<>();
    for (int u8Struct_i = 0; u8Struct_i < u8Struct_vecLength; u8Struct_i++) {
      U8Struct u8Struct_elem = deserializeU8Struct(_input);
      u8Struct.add(u8Struct_elem);
    }
    return new U8Struct(u8Value, u8Vector, u8Struct);
  }
  private State deserializeState(AbiInput _input) {
    long returnedU64 = _input.readU64();
    String returnedString = _input.readString();
    boolean returnedBool = _input.readBoolean();
    U8Struct returnedU8Struct = deserializeU8Struct(_input);
    return new State(returnedU64, returnedString, returnedBool, returnedU8Struct);
  }
  public State getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeState(input);
  }

  private AskForBasicValuesAction deserializeAskForBasicValuesAction(AbiInput _input) {
    BlockchainAddress contractAddress = _input.readAddress();
    return new AskForBasicValuesAction(contractAddress);
  }

  private AskForU8StructAction deserializeAskForU8StructAction(AbiInput _input) {
    BlockchainAddress contractAddress = _input.readAddress();
    return new AskForU8StructAction(contractAddress);
  }

  private AskForU64WithCourierAction deserializeAskForU64WithCourierAction(AbiInput _input) {
    BlockchainAddress contractAddress = _input.readAddress();
    return new AskForU64WithCourierAction(contractAddress);
  }

  private AskForBasicValuesCallbackCallback deserializeAskForBasicValuesCallbackCallback(AbiInput _input) {
    return new AskForBasicValuesCallbackCallback();
  }

  private AskForU8StructCallbackCallback deserializeAskForU8StructCallbackCallback(AbiInput _input) {
    return new AskForU8StructCallbackCallback();
  }

  private AskForU64WithCourierCallbackCallback deserializeAskForU64WithCourierCallbackCallback(AbiInput _input) {
    return new AskForU64WithCourierCallbackCallback();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record U8Struct(byte u8Value, byte[] u8Vector, List<U8Struct> u8Struct) {
  }

  @AbiGenerated
  public record State(long returnedU64, String returnedString, boolean returnedBool, U8Struct returnedU8Struct) {
    public static State deserialize(byte[] bytes) {
      return ContractReturnDataCaller_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] askForBasicValues(BlockchainAddress contractAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(contractAddress);
    });
  }

  public static byte[] askForU8Struct(BlockchainAddress contractAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(contractAddress);
    });
  }

  public static byte[] askForU64WithCourier(BlockchainAddress contractAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(contractAddress);
    });
  }

  public static byte[] askForBasicValuesCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("10"));
    });
  }

  public static byte[] askForU8StructCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("20"));
    });
  }

  public static byte[] askForU64WithCourierCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("30"));
    });
  }

  public static State deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractReturnDataCaller_SDK_13_3_0(client, address).deserializeState(input);
  }
  
  public static State deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static State deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record AskForBasicValuesAction(BlockchainAddress contractAddress) implements Action {
  }
  @AbiGenerated
  public record AskForU8StructAction(BlockchainAddress contractAddress) implements Action {
  }
  @AbiGenerated
  public record AskForU64WithCourierAction(BlockchainAddress contractAddress) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataCaller_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeAskForBasicValuesAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeAskForU8StructAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeAskForU64WithCourierAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record AskForBasicValuesCallbackCallback() implements Callback {
  }
  @AbiGenerated
  public record AskForU8StructCallbackCallback() implements Callback {
  }
  @AbiGenerated
  public record AskForU64WithCourierCallbackCallback() implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataCaller_SDK_13_3_0(null, null);
    if (shortname.equals("10")) {
      return contract.deserializeAskForBasicValuesCallbackCallback(input);
    } else if (shortname.equals("20")) {
      return contract.deserializeAskForU8StructCallbackCallback(input);
    } else if (shortname.equals("30")) {
      return contract.deserializeAskForU64WithCourierCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataCaller_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
