// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class Voting_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public Voting_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VoteState deserializeVoteState(AbiInput _input) {
    long proposalId = _input.readU64();
    var voters_vecLength = _input.readI32();
    List<BlockchainAddress> voters = new ArrayList<>();
    for (int voters_i = 0; voters_i < voters_vecLength; voters_i++) {
      BlockchainAddress voters_elem = _input.readAddress();
      voters.add(voters_elem);
    }
    long deadlineUtcMillis = _input.readI64();
    var votes_mapLength = _input.readI32();
    Map<BlockchainAddress, Boolean> votes = new HashMap<>();
    for (int votes_i = 0; votes_i < votes_mapLength; votes_i++) {
      BlockchainAddress votes_key = _input.readAddress();
      boolean votes_value = _input.readBoolean();
      votes.put(votes_key, votes_value);
    }
    Boolean result = null;
    var result_isSome = _input.readBoolean();
    if (result_isSome) {
      boolean result_option = _input.readBoolean();
      result = result_option;
    }
    return new VoteState(proposalId, voters, deadlineUtcMillis, votes, result);
  }
  public VoteState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeVoteState(input);
  }

  private VoteAction deserializeVoteAction(AbiInput _input) {
    boolean vote = _input.readBoolean();
    return new VoteAction(vote);
  }

  private CountAction deserializeCountAction(AbiInput _input) {
    return new CountAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    long proposalId = _input.readU64();
    var voters_vecLength = _input.readI32();
    List<BlockchainAddress> voters = new ArrayList<>();
    for (int voters_i = 0; voters_i < voters_vecLength; voters_i++) {
      BlockchainAddress voters_elem = _input.readAddress();
      voters.add(voters_elem);
    }
    long deadlineUtcMillis = _input.readI64();
    return new InitializeInit(proposalId, voters, deadlineUtcMillis);
  }


  @AbiGenerated
  public record VoteState(long proposalId, List<BlockchainAddress> voters, long deadlineUtcMillis, Map<BlockchainAddress, Boolean> votes, Boolean result) {
    public static VoteState deserialize(byte[] bytes) {
      return Voting_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(long proposalId, List<BlockchainAddress> voters, long deadlineUtcMillis) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU64(proposalId);
      _out.writeI32(voters.size());
      for (BlockchainAddress voters_vec : voters) {
        _out.writeAddress(voters_vec);
      }
      _out.writeI64(deadlineUtcMillis);
    });
  }

  public static byte[] vote(boolean vote) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeBoolean(vote);
    });
  }

  public static byte[] count() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static VoteState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new Voting_SDK_13_4_0(client, address).deserializeVoteState(input);
  }
  
  public static VoteState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static VoteState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record VoteAction(boolean vote) implements Action {
  }
  @AbiGenerated
  public record CountAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new Voting_SDK_13_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeVoteAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeCountAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(long proposalId, List<BlockchainAddress> voters, long deadlineUtcMillis) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new Voting_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
