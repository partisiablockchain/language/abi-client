// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class LargeOracleContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeDispute(_input: AbiInput): Dispute {
    const challenger: BlockchainAddress = _input.readAddress();
    const contractResultInvocation: number = _input.readU32();
    const contractCounterClaimInvocation: number = _input.readU32();
    const votes_vecLength = _input.readI32();
    const votes: number[] = [];
    for (let votes_i = 0; votes_i < votes_vecLength; votes_i++) {
      const votes_elem: number = _input.readU32();
      votes.push(votes_elem);
    }
    const counterClaimChallengers_vecLength = _input.readI32();
    const counterClaimChallengers: BlockchainAddress[] = [];
    for (let counterClaimChallengers_i = 0; counterClaimChallengers_i < counterClaimChallengers_vecLength; counterClaimChallengers_i++) {
      const counterClaimChallengers_elem: BlockchainAddress = _input.readAddress();
      counterClaimChallengers.push(counterClaimChallengers_elem);
    }
    return { challenger, contractResultInvocation, contractCounterClaimInvocation, votes, counterClaimChallengers };
  }
  public deserializeOracleDisputeId(_input: AbiInput): OracleDisputeId {
    const smallOracle: BlockchainAddress = _input.readAddress();
    const oracleId: BN = _input.readU64();
    const disputeId: BN = _input.readU64();
    return { smallOracle, oracleId, disputeId };
  }
  public deserializeOracleMember(_input: AbiInput): OracleMember {
    const address: BlockchainAddress = _input.readAddress();
    const key: Buffer = _input.readBytes(33);
    return { address, key };
  }
  public deserializeSigningRequest(_input: AbiInput): SigningRequest {
    const nonce: number = _input.readU32();
    const messageHash: Buffer = _input.readBytes(32);
    const transactionHash: Buffer = _input.readBytes(32);
    return { nonce, messageHash, transactionHash };
  }
  public deserializeStakedTokens(_input: AbiInput): StakedTokens {
    const freeTokens: BN = _input.readU64();
    const lockedToOracle_mapLength = _input.readI32();
    const lockedToOracle: Map<BlockchainAddress, BN> = new Map();
    for (let lockedToOracle_i = 0; lockedToOracle_i < lockedToOracle_mapLength; lockedToOracle_i++) {
      const lockedToOracle_key: BlockchainAddress = _input.readAddress();
      const lockedToOracle_value: BN = _input.readU64();
      lockedToOracle.set(lockedToOracle_key, lockedToOracle_value);
    }
    const pendingTokens_mapLength = _input.readI32();
    const pendingTokens: Map<BlockchainAddress, BN> = new Map();
    for (let pendingTokens_i = 0; pendingTokens_i < pendingTokens_mapLength; pendingTokens_i++) {
      const pendingTokens_key: BlockchainAddress = _input.readAddress();
      const pendingTokens_value: BN = _input.readU64();
      pendingTokens.set(pendingTokens_key, pendingTokens_value);
    }
    const lockedToDispute_mapLength = _input.readI32();
    const lockedToDispute: Map<OracleDisputeId, BN> = new Map();
    for (let lockedToDispute_i = 0; lockedToDispute_i < lockedToDispute_mapLength; lockedToDispute_i++) {
      const lockedToDispute_key: OracleDisputeId = this.deserializeOracleDisputeId(_input);
      const lockedToDispute_value: BN = _input.readU64();
      lockedToDispute.set(lockedToDispute_key, lockedToDispute_value);
    }
    return { freeTokens, lockedToOracle, pendingTokens, lockedToDispute };
  }
  public deserializeLargeOracleContractState(_input: AbiInput): LargeOracleContractState {
    const oracleMembers_vecLength = _input.readI32();
    const oracleMembers: OracleMember[] = [];
    for (let oracleMembers_i = 0; oracleMembers_i < oracleMembers_vecLength; oracleMembers_i++) {
      const oracleMembers_elem: OracleMember = this.deserializeOracleMember(_input);
      oracleMembers.push(oracleMembers_elem);
    }
    const oracleKey: Buffer = _input.readBytes(33);
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const governanceUpdates: BlockchainAddress = _input.readAddress();
    const stakedTokens_mapLength = _input.readI32();
    const stakedTokens: Map<BlockchainAddress, StakedTokens> = new Map();
    for (let stakedTokens_i = 0; stakedTokens_i < stakedTokens_mapLength; stakedTokens_i++) {
      const stakedTokens_key: BlockchainAddress = _input.readAddress();
      const stakedTokens_value: StakedTokens = this.deserializeStakedTokens(_input);
      stakedTokens.set(stakedTokens_key, stakedTokens_value);
    }
    const activeDisputes_mapLength = _input.readI32();
    const activeDisputes: Map<OracleDisputeId, Dispute> = new Map();
    for (let activeDisputes_i = 0; activeDisputes_i < activeDisputes_mapLength; activeDisputes_i++) {
      const activeDisputes_key: OracleDisputeId = this.deserializeOracleDisputeId(_input);
      const activeDisputes_value: Dispute = this.deserializeDispute(_input);
      activeDisputes.set(activeDisputes_key, activeDisputes_value);
    }
    const pendingMessages_vecLength = _input.readI32();
    const pendingMessages: SigningRequest[] = [];
    for (let pendingMessages_i = 0; pendingMessages_i < pendingMessages_vecLength; pendingMessages_i++) {
      const pendingMessages_elem: SigningRequest = this.deserializeSigningRequest(_input);
      pendingMessages.push(pendingMessages_elem);
    }
    const currentMessageNonce: number = _input.readU32();
    const initialMessageNonce: number = _input.readU32();
    const signedMessages_mapLength = _input.readI32();
    const signedMessages: Map<SigningRequest, Signature> = new Map();
    for (let signedMessages_i = 0; signedMessages_i < signedMessages_mapLength; signedMessages_i++) {
      const signedMessages_key: SigningRequest = this.deserializeSigningRequest(_input);
      const signedMessages_value: Signature = this.deserializeSignature(_input);
      signedMessages.set(signedMessages_key, signedMessages_value);
    }
    return { oracleMembers, oracleKey, bpOrchestrationContract, governanceUpdates, stakedTokens, activeDisputes, pendingMessages, currentMessageNonce, initialMessageNonce, signedMessages };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU8();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public async getState(): Promise<LargeOracleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeLargeOracleContractState(input);
  }

  public deserializeCreateDisputePollAction(_input: AbiInput): CreateDisputePollAction {
    const disputeChallenger: BlockchainAddress = _input.readAddress();
    const disputeTokenCost: BN = _input.readU64();
    const oracleId: BN = _input.readU64();
    const disputeId: BN = _input.readU64();
    const smallOraclesResultInvocation: number = _input.readU32();
    const smallOraclesCounterClaimInvocation: number = _input.readU32();
    return { discriminant: "create_dispute_poll", disputeChallenger, disputeTokenCost, oracleId, disputeId, smallOraclesResultInvocation, smallOraclesCounterClaimInvocation };
  }

  public deserializeCreateDisputePollWithErrorThrowAction(_input: AbiInput): CreateDisputePollWithErrorThrowAction {
    const disputeChallenger: BlockchainAddress = _input.readAddress();
    const disputeCost: BN = _input.readI64();
    const oracleId: BN = _input.readI64();
    const disputeId: BN = _input.readI64();
    const smallOracleResultInvocation: number = _input.readU32();
    const smallOracleCounterClaimInvocation: number = _input.readU32();
    return { discriminant: "create_dispute_poll_with_error_throw", disputeChallenger, disputeCost, oracleId, disputeId, smallOracleResultInvocation, smallOracleCounterClaimInvocation };
  }

  public deserializeAddDisputeCounterClaimAction(_input: AbiInput): AddDisputeCounterClaimAction {
    const smallOracle: BlockchainAddress = _input.readAddress();
    const oracleId: BN = _input.readU64();
    const disputeId: BN = _input.readU64();
    const passThrough_vecLength = _input.readI32();
    const passThrough: Buffer = _input.readBytes(passThrough_vecLength);
    return { discriminant: "add_dispute_counter_claim", smallOracle, oracleId, disputeId, passThrough };
  }

  public deserializeVoteOnDisputeAction(_input: AbiInput): VoteOnDisputeAction {
    const smallOracle: BlockchainAddress = _input.readAddress();
    const oracleId: BN = _input.readU64();
    const disputeId: BN = _input.readU64();
    const vote: number = _input.readU32();
    return { discriminant: "vote_on_dispute", smallOracle, oracleId, disputeId, vote };
  }

  public deserializeBurnStakedTokensAction(_input: AbiInput): BurnStakedTokensAction {
    const oracles_vecLength = _input.readI32();
    const oracles: BlockchainAddress[] = [];
    for (let oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      const oracles_elem: BlockchainAddress = _input.readAddress();
      oracles.push(oracles_elem);
    }
    const amount: BN = _input.readU64();
    return { discriminant: "burn_staked_tokens", oracles, amount };
  }

  public deserializeReplaceLargeOracleAction(_input: AbiInput): ReplaceLargeOracleAction {
    const newKeys_vecLength = _input.readI32();
    const newKeys: Buffer[] = [];
    for (let newKeys_i = 0; newKeys_i < newKeys_vecLength; newKeys_i++) {
      const newKeys_elem: Buffer = _input.readBytes(33);
      newKeys.push(newKeys_elem);
    }
    const newAddresses_vecLength = _input.readI32();
    const newAddresses: BlockchainAddress[] = [];
    for (let newAddresses_i = 0; newAddresses_i < newAddresses_vecLength; newAddresses_i++) {
      const newAddresses_elem: BlockchainAddress = _input.readAddress();
      newAddresses.push(newAddresses_elem);
    }
    const newPublicKey: Buffer = _input.readBytes(33);
    return { discriminant: "replace_large_oracle", newKeys, newAddresses, newPublicKey };
  }

  public deserializeRequestNewSmallOracleAction(_input: AbiInput): RequestNewSmallOracleAction {
    const requiredStake: BN = _input.readU64();
    const callback: number = _input.readU32();
    const additionalData_vecLength = _input.readI32();
    const additionalData: Buffer = _input.readBytes(additionalData_vecLength);
    return { discriminant: "request_new_small_oracle", requiredStake, callback, additionalData };
  }

  public deserializeAddSignatureAction(_input: AbiInput): AddSignatureAction {
    const message: Buffer = _input.readBytes(32);
    const nonce: number = _input.readU32();
    const signature: Signature = this.deserializeSignature(_input);
    return { discriminant: "add_signature", message, nonce, signature };
  }

  public deserializeChangeExchangeRateAction(_input: AbiInput): ChangeExchangeRateAction {
    const numerator: number = _input.readU32();
    const denominator: number = _input.readU32();
    return { discriminant: "change_exchange_rate", numerator, denominator };
  }

  public deserializeRecalibrateByocTwinsAction(_input: AbiInput): RecalibrateByocTwinsAction {
    const byocTwinSymbol: string = _input.readString();
    const byocTwinAmount: Buffer = _input.readBytes(32);
    const oracles_vecLength = _input.readI32();
    const oracles: BlockchainAddress[] = [];
    for (let oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      const oracles_elem: BlockchainAddress = _input.readAddress();
      oracles.push(oracles_elem);
    }
    return { discriminant: "recalibrate_byoc_twins", byocTwinSymbol, byocTwinAmount, oracles };
  }

  public deserializeSellByocTwinsAction(_input: AbiInput): SellByocTwinsAction {
    const byocTwinSymbol: string = _input.readString();
    const byocTwinAmount: Buffer = _input.readBytes(32);
    const expectedMpcToBuy: BN = _input.readU64();
    return { discriminant: "sell_byoc_twins", byocTwinSymbol, byocTwinAmount, expectedMpcToBuy };
  }

  public deserializeAssociateTokensToContractAction(_input: AbiInput): AssociateTokensToContractAction {
    const amount: BN = _input.readU64();
    return { discriminant: "associate_tokens_to_contract", amount };
  }

  public deserializeDisassociateTokensFromContractAction(_input: AbiInput): DisassociateTokensFromContractAction {
    const amount: BN = _input.readU64();
    return { discriminant: "disassociate_tokens_from_contract", amount };
  }

  public deserializeLockTokensToPriceOracleAction(_input: AbiInput): LockTokensToPriceOracleAction {
    const tokensToLock: BN = _input.readU64();
    const priceOracleNode: BlockchainAddress = _input.readAddress();
    return { discriminant: "lock_tokens_to_price_oracle", tokensToLock, priceOracleNode };
  }

  public deserializeUnlockTokensToPriceOracleAction(_input: AbiInput): UnlockTokensToPriceOracleAction {
    const tokensToUnlock: BN = _input.readU64();
    const priceOracleNode: BlockchainAddress = _input.readAddress();
    return { discriminant: "unlock_tokens_to_price_oracle", tokensToUnlock, priceOracleNode };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const keys_vecLength = _input.readI32();
    const keys: Buffer[] = [];
    for (let keys_i = 0; keys_i < keys_vecLength; keys_i++) {
      const keys_elem: Buffer = _input.readBytes(33);
      keys.push(keys_elem);
    }
    const addresses_vecLength = _input.readI32();
    const addresses: BlockchainAddress[] = [];
    for (let addresses_i = 0; addresses_i < addresses_vecLength; addresses_i++) {
      const addresses_elem: BlockchainAddress = _input.readAddress();
      addresses.push(addresses_elem);
    }
    const oracleKey: Buffer = _input.readBytes(33);
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const governanceUpdates: BlockchainAddress = _input.readAddress();
    return { discriminant: "init", keys, addresses, oracleKey, bpOrchestrationContract, governanceUpdates };
  }

}
export interface Dispute {
  challenger: BlockchainAddress;
  contractResultInvocation: number;
  contractCounterClaimInvocation: number;
  votes: number[];
  counterClaimChallengers: BlockchainAddress[];
}

export interface OracleDisputeId {
  smallOracle: BlockchainAddress;
  oracleId: BN;
  disputeId: BN;
}

export interface OracleMember {
  address: BlockchainAddress;
  key: Buffer;
}

export interface SigningRequest {
  nonce: number;
  messageHash: Buffer;
  transactionHash: Buffer;
}

export interface StakedTokens {
  freeTokens: BN;
  lockedToOracle: Map<BlockchainAddress, BN>;
  pendingTokens: Map<BlockchainAddress, BN>;
  lockedToDispute: Map<OracleDisputeId, BN>;
}

export interface LargeOracleContractState {
  oracleMembers: OracleMember[];
  oracleKey: Buffer;
  bpOrchestrationContract: BlockchainAddress;
  governanceUpdates: BlockchainAddress;
  stakedTokens: Map<BlockchainAddress, StakedTokens>;
  activeDisputes: Map<OracleDisputeId, Dispute>;
  pendingMessages: SigningRequest[];
  currentMessageNonce: number;
  initialMessageNonce: number;
  signedMessages: Map<SigningRequest, Signature>;
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}
function serializeSignature(_out: AbiOutput, _value: Signature): void {
  const { recoveryId, valueR, valueS } = _value;
  _out.writeU8(recoveryId);
  if (valueR.length != 32) {
    throw new Error("Length of valueR does not match expected 32");
  }
  _out.writeBytes(valueR);
  if (valueS.length != 32) {
    throw new Error("Length of valueS does not match expected 32");
  }
  _out.writeBytes(valueS);
}

export function init(keys: Buffer[], addresses: BlockchainAddress[], oracleKey: Buffer, bpOrchestrationContract: BlockchainAddress, governanceUpdates: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(keys.length);
    for (const keys_vec of keys) {
      if (keys_vec.length != 33) {
        throw new Error("Length of keys_vec does not match expected 33");
      }
      _out.writeBytes(keys_vec);
    }
    _out.writeI32(addresses.length);
    for (const addresses_vec of addresses) {
      _out.writeAddress(addresses_vec);
    }
    if (oracleKey.length != 33) {
      throw new Error("Length of oracleKey does not match expected 33");
    }
    _out.writeBytes(oracleKey);
    _out.writeAddress(bpOrchestrationContract);
    _out.writeAddress(governanceUpdates);
  });
}

export function createDisputePoll(disputeChallenger: BlockchainAddress, disputeTokenCost: BN, oracleId: BN, disputeId: BN, smallOraclesResultInvocation: number, smallOraclesCounterClaimInvocation: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeAddress(disputeChallenger);
    _out.writeU64(disputeTokenCost);
    _out.writeU64(oracleId);
    _out.writeU64(disputeId);
    _out.writeU32(smallOraclesResultInvocation);
    _out.writeU32(smallOraclesCounterClaimInvocation);
  });
}

export function createDisputePollWithErrorThrow(disputeChallenger: BlockchainAddress, disputeCost: BN, oracleId: BN, disputeId: BN, smallOracleResultInvocation: number, smallOracleCounterClaimInvocation: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0e", "hex"));
    _out.writeAddress(disputeChallenger);
    _out.writeI64(disputeCost);
    _out.writeI64(oracleId);
    _out.writeI64(disputeId);
    _out.writeU32(smallOracleResultInvocation);
    _out.writeU32(smallOracleCounterClaimInvocation);
  });
}

export function addDisputeCounterClaim(smallOracle: BlockchainAddress, oracleId: BN, disputeId: BN, passThrough: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(smallOracle);
    _out.writeU64(oracleId);
    _out.writeU64(disputeId);
    _out.writeI32(passThrough.length);
    _out.writeBytes(passThrough);
  });
}

export function voteOnDispute(smallOracle: BlockchainAddress, oracleId: BN, disputeId: BN, vote: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(smallOracle);
    _out.writeU64(oracleId);
    _out.writeU64(disputeId);
    _out.writeU32(vote);
  });
}

export function burnStakedTokens(oracles: BlockchainAddress[], amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeI32(oracles.length);
    for (const oracles_vec of oracles) {
      _out.writeAddress(oracles_vec);
    }
    _out.writeU64(amount);
  });
}

export function replaceLargeOracle(newKeys: Buffer[], newAddresses: BlockchainAddress[], newPublicKey: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeI32(newKeys.length);
    for (const newKeys_vec of newKeys) {
      if (newKeys_vec.length != 33) {
        throw new Error("Length of newKeys_vec does not match expected 33");
      }
      _out.writeBytes(newKeys_vec);
    }
    _out.writeI32(newAddresses.length);
    for (const newAddresses_vec of newAddresses) {
      _out.writeAddress(newAddresses_vec);
    }
    if (newPublicKey.length != 33) {
      throw new Error("Length of newPublicKey does not match expected 33");
    }
    _out.writeBytes(newPublicKey);
  });
}

export function requestNewSmallOracle(requiredStake: BN, callback: number, additionalData: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeU64(requiredStake);
    _out.writeU32(callback);
    _out.writeI32(additionalData.length);
    _out.writeBytes(additionalData);
  });
}

export function addSignature(message: Buffer, nonce: number, signature: Signature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    if (message.length != 32) {
      throw new Error("Length of message does not match expected 32");
    }
    _out.writeBytes(message);
    _out.writeU32(nonce);
    serializeSignature(_out, signature);
  });
}

export function changeExchangeRate(numerator: number, denominator: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("07", "hex"));
    _out.writeU32(numerator);
    _out.writeU32(denominator);
  });
}

export function recalibrateByocTwins(byocTwinSymbol: string, byocTwinAmount: Buffer, oracles: BlockchainAddress[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("08", "hex"));
    _out.writeString(byocTwinSymbol);
    if (byocTwinAmount.length != 32) {
      throw new Error("Length of byocTwinAmount does not match expected 32");
    }
    _out.writeBytes(byocTwinAmount);
    _out.writeI32(oracles.length);
    for (const oracles_vec of oracles) {
      _out.writeAddress(oracles_vec);
    }
  });
}

export function sellByocTwins(byocTwinSymbol: string, byocTwinAmount: Buffer, expectedMpcToBuy: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("09", "hex"));
    _out.writeString(byocTwinSymbol);
    if (byocTwinAmount.length != 32) {
      throw new Error("Length of byocTwinAmount does not match expected 32");
    }
    _out.writeBytes(byocTwinAmount);
    _out.writeU64(expectedMpcToBuy);
  });
}

export function associateTokensToContract(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0a", "hex"));
    _out.writeU64(amount);
  });
}

export function disassociateTokensFromContract(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0b", "hex"));
    _out.writeU64(amount);
  });
}

export function lockTokensToPriceOracle(tokensToLock: BN, priceOracleNode: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0c", "hex"));
    _out.writeU64(tokensToLock);
    _out.writeAddress(priceOracleNode);
  });
}

export function unlockTokensToPriceOracle(tokensToUnlock: BN, priceOracleNode: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0d", "hex"));
    _out.writeU64(tokensToUnlock);
    _out.writeAddress(priceOracleNode);
  });
}

export function deserializeState(state: StateWithClient): LargeOracleContractState;
export function deserializeState(bytes: Buffer): LargeOracleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): LargeOracleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): LargeOracleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new LargeOracleContract_SDK_13_3_0(client, address).deserializeLargeOracleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new LargeOracleContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeLargeOracleContractState(input);
  }
}

export type Action =
  | CreateDisputePollAction
  | CreateDisputePollWithErrorThrowAction
  | AddDisputeCounterClaimAction
  | VoteOnDisputeAction
  | BurnStakedTokensAction
  | ReplaceLargeOracleAction
  | RequestNewSmallOracleAction
  | AddSignatureAction
  | ChangeExchangeRateAction
  | RecalibrateByocTwinsAction
  | SellByocTwinsAction
  | AssociateTokensToContractAction
  | DisassociateTokensFromContractAction
  | LockTokensToPriceOracleAction
  | UnlockTokensToPriceOracleAction;

export interface CreateDisputePollAction {
  discriminant: "create_dispute_poll";
  disputeChallenger: BlockchainAddress;
  disputeTokenCost: BN;
  oracleId: BN;
  disputeId: BN;
  smallOraclesResultInvocation: number;
  smallOraclesCounterClaimInvocation: number;
}
export interface CreateDisputePollWithErrorThrowAction {
  discriminant: "create_dispute_poll_with_error_throw";
  disputeChallenger: BlockchainAddress;
  disputeCost: BN;
  oracleId: BN;
  disputeId: BN;
  smallOracleResultInvocation: number;
  smallOracleCounterClaimInvocation: number;
}
export interface AddDisputeCounterClaimAction {
  discriminant: "add_dispute_counter_claim";
  smallOracle: BlockchainAddress;
  oracleId: BN;
  disputeId: BN;
  passThrough: Buffer;
}
export interface VoteOnDisputeAction {
  discriminant: "vote_on_dispute";
  smallOracle: BlockchainAddress;
  oracleId: BN;
  disputeId: BN;
  vote: number;
}
export interface BurnStakedTokensAction {
  discriminant: "burn_staked_tokens";
  oracles: BlockchainAddress[];
  amount: BN;
}
export interface ReplaceLargeOracleAction {
  discriminant: "replace_large_oracle";
  newKeys: Buffer[];
  newAddresses: BlockchainAddress[];
  newPublicKey: Buffer;
}
export interface RequestNewSmallOracleAction {
  discriminant: "request_new_small_oracle";
  requiredStake: BN;
  callback: number;
  additionalData: Buffer;
}
export interface AddSignatureAction {
  discriminant: "add_signature";
  message: Buffer;
  nonce: number;
  signature: Signature;
}
export interface ChangeExchangeRateAction {
  discriminant: "change_exchange_rate";
  numerator: number;
  denominator: number;
}
export interface RecalibrateByocTwinsAction {
  discriminant: "recalibrate_byoc_twins";
  byocTwinSymbol: string;
  byocTwinAmount: Buffer;
  oracles: BlockchainAddress[];
}
export interface SellByocTwinsAction {
  discriminant: "sell_byoc_twins";
  byocTwinSymbol: string;
  byocTwinAmount: Buffer;
  expectedMpcToBuy: BN;
}
export interface AssociateTokensToContractAction {
  discriminant: "associate_tokens_to_contract";
  amount: BN;
}
export interface DisassociateTokensFromContractAction {
  discriminant: "disassociate_tokens_from_contract";
  amount: BN;
}
export interface LockTokensToPriceOracleAction {
  discriminant: "lock_tokens_to_price_oracle";
  tokensToLock: BN;
  priceOracleNode: BlockchainAddress;
}
export interface UnlockTokensToPriceOracleAction {
  discriminant: "unlock_tokens_to_price_oracle";
  tokensToUnlock: BN;
  priceOracleNode: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LargeOracleContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeCreateDisputePollAction(input);
  } else if (shortname === "0e") {
    return contract.deserializeCreateDisputePollWithErrorThrowAction(input);
  } else if (shortname === "01") {
    return contract.deserializeAddDisputeCounterClaimAction(input);
  } else if (shortname === "02") {
    return contract.deserializeVoteOnDisputeAction(input);
  } else if (shortname === "03") {
    return contract.deserializeBurnStakedTokensAction(input);
  } else if (shortname === "04") {
    return contract.deserializeReplaceLargeOracleAction(input);
  } else if (shortname === "05") {
    return contract.deserializeRequestNewSmallOracleAction(input);
  } else if (shortname === "06") {
    return contract.deserializeAddSignatureAction(input);
  } else if (shortname === "07") {
    return contract.deserializeChangeExchangeRateAction(input);
  } else if (shortname === "08") {
    return contract.deserializeRecalibrateByocTwinsAction(input);
  } else if (shortname === "09") {
    return contract.deserializeSellByocTwinsAction(input);
  } else if (shortname === "0a") {
    return contract.deserializeAssociateTokensToContractAction(input);
  } else if (shortname === "0b") {
    return contract.deserializeDisassociateTokensFromContractAction(input);
  } else if (shortname === "0c") {
    return contract.deserializeLockTokensToPriceOracleAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeUnlockTokensToPriceOracleAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  keys: Buffer[];
  addresses: BlockchainAddress[];
  oracleKey: Buffer;
  bpOrchestrationContract: BlockchainAddress;
  governanceUpdates: BlockchainAddress;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LargeOracleContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

