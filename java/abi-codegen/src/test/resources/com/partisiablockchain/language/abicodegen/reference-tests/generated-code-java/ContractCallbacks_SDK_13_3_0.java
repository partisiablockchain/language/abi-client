// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractCallbacks_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractCallbacks_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    int val = _input.readU32();
    boolean successfulCallback = _input.readBoolean();
    var callbackResults_vecLength = _input.readI32();
    List<Boolean> callbackResults = new ArrayList<>();
    for (int callbackResults_i = 0; callbackResults_i < callbackResults_vecLength; callbackResults_i++) {
      boolean callbackResults_elem = _input.readBoolean();
      callbackResults.add(callbackResults_elem);
    }
    int callbackValue = _input.readU32();
    return new ExampleContractState(val, successfulCallback, callbackResults, callbackValue);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private SetAndCallbackAction deserializeSetAndCallbackAction(AbiInput _input) {
    int value = _input.readU32();
    return new SetAndCallbackAction(value);
  }

  private PanicAndCallbackAction deserializePanicAndCallbackAction(AbiInput _input) {
    return new PanicAndCallbackAction();
  }

  private PanickingAction deserializePanickingAction(AbiInput _input) {
    return new PanickingAction();
  }

  private SetAction deserializeSetAction(AbiInput _input) {
    int value = _input.readU32();
    return new SetAction(value);
  }

  private MultipleEventsAndCallbackAction deserializeMultipleEventsAndCallbackAction(AbiInput _input) {
    int value = _input.readU32();
    return new MultipleEventsAndCallbackAction(value);
  }

  private CallbackCallback deserializeCallbackCallback(AbiInput _input) {
    int val = _input.readU32();
    return new CallbackCallback(val);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    int val = _input.readU32();
    return new InitializeInit(val);
  }


  @AbiGenerated
  public record ExampleContractState(int val, boolean successfulCallback, List<Boolean> callbackResults, int callbackValue) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractCallbacks_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(int val) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU32(val);
    });
  }

  public static byte[] setAndCallback(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU32(value);
    });
  }

  public static byte[] panicAndCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] panicking() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] set(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeU32(value);
    });
  }

  public static byte[] multipleEventsAndCallback(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeU32(value);
    });
  }

  public static byte[] callback(int val) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeU32(val);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractCallbacks_SDK_13_3_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record SetAndCallbackAction(int value) implements Action {
  }
  @AbiGenerated
  public record PanicAndCallbackAction() implements Action {
  }
  @AbiGenerated
  public record PanickingAction() implements Action {
  }
  @AbiGenerated
  public record SetAction(int value) implements Action {
  }
  @AbiGenerated
  public record MultipleEventsAndCallbackAction(int value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractCallbacks_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeSetAndCallbackAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializePanicAndCallbackAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializePanickingAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeSetAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeMultipleEventsAndCallbackAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record CallbackCallback(int val) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractCallbacks_SDK_13_3_0(null, null);
    if (shortname.equals("05")) {
      return contract.deserializeCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(int val) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractCallbacks_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
