// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class PubDeploy_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public PubDeploy_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private PubDeployContractState deserializePubDeployContractState(AbiInput _input) {
    boolean checkSignature = _input.readBoolean();
    BlockchainAddress owner = _input.readAddress();
    var bindingJar_vecLength = _input.readI32();
    byte[] bindingJar = _input.readBytes(bindingJar_vecLength);
    return new PubDeployContractState(checkSignature, owner, bindingJar);
  }
  public PubDeployContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializePubDeployContractState(input);
  }

  private DeployContractAction deserializeDeployContractAction(AbiInput _input) {
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    var initialization_vecLength = _input.readI32();
    byte[] initialization = _input.readBytes(initialization_vecLength);
    return new DeployContractAction(contractJar, abi, initialization);
  }

  private UpdateBinderAction deserializeUpdateBinderAction(AbiInput _input) {
    var bindingJar_vecLength = _input.readI32();
    byte[] bindingJar = _input.readBytes(bindingJar_vecLength);
    return new UpdateBinderAction(bindingJar);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    boolean checkSignature = _input.readBoolean();
    var bindingJar_vecLength = _input.readI32();
    byte[] bindingJar = _input.readBytes(bindingJar_vecLength);
    return new InitInit(checkSignature, bindingJar);
  }


  @AbiGenerated
  public record PubDeployContractState(boolean checkSignature, BlockchainAddress owner, byte[] bindingJar) {
    public static PubDeployContractState deserialize(byte[] bytes) {
      return PubDeploy_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] init(boolean checkSignature, byte[] bindingJar) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeBoolean(checkSignature);
      _out.writeI32(bindingJar.length);
      _out.writeBytes(bindingJar);
    });
  }

  public static byte[] deployContract(byte[] contractJar, byte[] abi, byte[] initialization) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeI32(contractJar.length);
      _out.writeBytes(contractJar);
      _out.writeI32(abi.length);
      _out.writeBytes(abi);
      _out.writeI32(initialization.length);
      _out.writeBytes(initialization);
    });
  }

  public static byte[] updateBinder(byte[] bindingJar) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeI32(bindingJar.length);
      _out.writeBytes(bindingJar);
    });
  }

  public static PubDeployContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new PubDeploy_SDK_13_3_0(client, address).deserializePubDeployContractState(input);
  }
  
  public static PubDeployContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static PubDeployContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record DeployContractAction(byte[] contractJar, byte[] abi, byte[] initialization) implements Action {
  }
  @AbiGenerated
  public record UpdateBinderAction(byte[] bindingJar) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new PubDeploy_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeDeployContractAction(input);
    } else if (shortname.equals("00")) {
      return contract.deserializeUpdateBinderAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(boolean checkSignature, byte[] bindingJar) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new PubDeploy_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
