// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractSizedArrays {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractSizedArrays(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    byte[] byteArray = _input.readBytes(12);
    List<Integer> myArray = new ArrayList<>();
    for (int myArray_i = 0; myArray_i < 16; myArray_i++) {
      int myArray_elem = _input.readI32();
      myArray.add(myArray_elem);
    }
    List<List<Integer>> myArray2 = new ArrayList<>();
    for (int myArray2_i = 0; myArray2_i < 4; myArray2_i++) {
      List<Integer> myArray2_elem = new ArrayList<>();
      for (int myArray2_elem_i = 0; myArray2_elem_i < 4; myArray2_elem_i++) {
        int myArray2_elem_elem = _input.readI32();
        myArray2_elem.add(myArray2_elem_elem);
      }
      myArray2.add(myArray2_elem);
    }
    List<Integer> myArray3 = new ArrayList<>();
    for (int myArray3_i = 0; myArray3_i < 5; myArray3_i++) {
      Integer myArray3_elem = null;
      var myArray3_elem_isSome = _input.readBoolean();
      if (myArray3_elem_isSome) {
        int myArray3_elem_option = _input.readI32();
        myArray3_elem = myArray3_elem_option;
      }
      myArray3.add(myArray3_elem);
    }
    return new ExampleContractState(byteArray, myArray, myArray2, myArray3);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateByteArrayAction deserializeUpdateByteArrayAction(AbiInput _input) {
    byte[] value = _input.readBytes(12);
    return new UpdateByteArrayAction(value);
  }

  private UpdateMyArrayAction deserializeUpdateMyArrayAction(AbiInput _input) {
    List<Integer> value = new ArrayList<>();
    for (int value_i = 0; value_i < 16; value_i++) {
      int value_elem = _input.readI32();
      value.add(value_elem);
    }
    return new UpdateMyArrayAction(value);
  }

  private UpdateMyArray2Action deserializeUpdateMyArray2Action(AbiInput _input) {
    List<List<Integer>> value = new ArrayList<>();
    for (int value_i = 0; value_i < 4; value_i++) {
      List<Integer> value_elem = new ArrayList<>();
      for (int value_elem_i = 0; value_elem_i < 4; value_elem_i++) {
        int value_elem_elem = _input.readI32();
        value_elem.add(value_elem_elem);
      }
      value.add(value_elem);
    }
    return new UpdateMyArray2Action(value);
  }

  private UpdateMyArray3Action deserializeUpdateMyArray3Action(AbiInput _input) {
    List<Integer> value = new ArrayList<>();
    for (int value_i = 0; value_i < 5; value_i++) {
      Integer value_elem = null;
      var value_elem_isSome = _input.readBoolean();
      if (value_elem_isSome) {
        int value_elem_option = _input.readI32();
        value_elem = value_elem_option;
      }
      value.add(value_elem);
    }
    return new UpdateMyArray3Action(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte[] byteArray = _input.readBytes(12);
    List<Integer> myArray = new ArrayList<>();
    for (int myArray_i = 0; myArray_i < 16; myArray_i++) {
      int myArray_elem = _input.readI32();
      myArray.add(myArray_elem);
    }
    List<List<Integer>> myArray2 = new ArrayList<>();
    for (int myArray2_i = 0; myArray2_i < 4; myArray2_i++) {
      List<Integer> myArray2_elem = new ArrayList<>();
      for (int myArray2_elem_i = 0; myArray2_elem_i < 4; myArray2_elem_i++) {
        int myArray2_elem_elem = _input.readI32();
        myArray2_elem.add(myArray2_elem_elem);
      }
      myArray2.add(myArray2_elem);
    }
    List<Integer> myArray3 = new ArrayList<>();
    for (int myArray3_i = 0; myArray3_i < 5; myArray3_i++) {
      Integer myArray3_elem = null;
      var myArray3_elem_isSome = _input.readBoolean();
      if (myArray3_elem_isSome) {
        int myArray3_elem_option = _input.readI32();
        myArray3_elem = myArray3_elem_option;
      }
      myArray3.add(myArray3_elem);
    }
    return new InitializeInit(byteArray, myArray, myArray2, myArray3);
  }


  @AbiGenerated
  public record ExampleContractState(byte[] byteArray, List<Integer> myArray, List<List<Integer>> myArray2, List<Integer> myArray3) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractSizedArrays.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte[] byteArray, List<Integer> myArray, List<List<Integer>> myArray2, List<Integer> myArray3) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      if (byteArray.length != 12) {
        throw new RuntimeException("Length of byteArray does not match expected 12");
      }
      _out.writeBytes(byteArray);
      if (myArray.size() != 16) {
        throw new RuntimeException("Length of myArray does not match expected 16");
      }
      for (int myArray_arr : myArray) {
        _out.writeI32(myArray_arr);
      }
      if (myArray2.size() != 4) {
        throw new RuntimeException("Length of myArray2 does not match expected 4");
      }
      for (List<Integer> myArray2_arr : myArray2) {
        if (myArray2_arr.size() != 4) {
          throw new RuntimeException("Length of myArray2_arr does not match expected 4");
        }
        for (int myArray2_arr_arr : myArray2_arr) {
          _out.writeI32(myArray2_arr_arr);
        }
      }
      if (myArray3.size() != 5) {
        throw new RuntimeException("Length of myArray3 does not match expected 5");
      }
      for (Integer myArray3_arr : myArray3) {
        _out.writeBoolean(myArray3_arr != null);
        if (myArray3_arr != null) {
          _out.writeI32(myArray3_arr);
        }
      }
    });
  }

  public static byte[] updateByteArray(byte[] value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("9df1bab60d"));
      if (value.length != 12) {
        throw new RuntimeException("Length of value does not match expected 12");
      }
      _out.writeBytes(value);
    });
  }

  public static byte[] updateMyArray(List<Integer> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("cbe4cbfd0d"));
      if (value.size() != 16) {
        throw new RuntimeException("Length of value does not match expected 16");
      }
      for (int value_arr : value) {
        _out.writeI32(value_arr);
      }
    });
  }

  public static byte[] updateMyArray2(List<List<Integer>> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ec9da775"));
      if (value.size() != 4) {
        throw new RuntimeException("Length of value does not match expected 4");
      }
      for (List<Integer> value_arr : value) {
        if (value_arr.size() != 4) {
          throw new RuntimeException("Length of value_arr does not match expected 4");
        }
        for (int value_arr_arr : value_arr) {
          _out.writeI32(value_arr_arr);
        }
      }
    });
  }

  public static byte[] updateMyArray3(List<Integer> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f3d683af06"));
      if (value.size() != 5) {
        throw new RuntimeException("Length of value does not match expected 5");
      }
      for (Integer value_arr : value) {
        _out.writeBoolean(value_arr != null);
        if (value_arr != null) {
          _out.writeI32(value_arr);
        }
      }
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractSizedArrays(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateByteArrayAction(byte[] value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArrayAction(List<Integer> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArray2Action(List<List<Integer>> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArray3Action(List<Integer> value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractSizedArrays(null, null);
    if (shortname.equals("9df1bab60d")) {
      return contract.deserializeUpdateByteArrayAction(input);
    } else if (shortname.equals("cbe4cbfd0d")) {
      return contract.deserializeUpdateMyArrayAction(input);
    } else if (shortname.equals("ec9da775")) {
      return contract.deserializeUpdateMyArray2Action(input);
    } else if (shortname.equals("f3d683af06")) {
      return contract.deserializeUpdateMyArray3Action(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte[] byteArray, List<Integer> myArray, List<List<Integer>> myArray2, List<Integer> myArray3) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractSizedArrays(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
