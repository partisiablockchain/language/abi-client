// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class AuctionContract_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeBid(_input: AbiInput): Bid {
    const bidder: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { bidder, amount };
  }
  public deserializeTokenClaim(_input: AbiInput): TokenClaim {
    const tokensForBidding: BN = _input.readU64();
    const tokensForSale: BN = _input.readU64();
    return { tokensForBidding, tokensForSale };
  }
  public deserializeAuctionContractState(_input: AbiInput): AuctionContractState {
    const contractOwner: BlockchainAddress = _input.readAddress();
    const startTimeMillis: BN = _input.readI64();
    const endTimeMillis: BN = _input.readI64();
    const tokenAmountForSale: BN = _input.readU64();
    const tokenForSale: BlockchainAddress = _input.readAddress();
    const tokenForBidding: BlockchainAddress = _input.readAddress();
    const highestBidder: Bid = this.deserializeBid(_input);
    const reservePrice: BN = _input.readU64();
    const minIncrement: BN = _input.readU64();
    const claimMap_mapLength = _input.readI32();
    const claimMap: Map<BlockchainAddress, TokenClaim> = new Map();
    for (let claimMap_i = 0; claimMap_i < claimMap_mapLength; claimMap_i++) {
      const claimMap_key: BlockchainAddress = _input.readAddress();
      const claimMap_value: TokenClaim = this.deserializeTokenClaim(_input);
      claimMap.set(claimMap_key, claimMap_value);
    }
    const status: number = _input.readU8();
    return { contractOwner, startTimeMillis, endTimeMillis, tokenAmountForSale, tokenForSale, tokenForBidding, highestBidder, reservePrice, minIncrement, claimMap, status };
  }
  public async getState(): Promise<AuctionContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeAuctionContractState(input);
  }

  public deserializeStartAction(_input: AbiInput): StartAction {
    return { discriminant: "start",  };
  }

  public deserializeBidAction(_input: AbiInput): BidAction {
    const bidAmount: BN = _input.readU64();
    return { discriminant: "bid", bidAmount };
  }

  public deserializeClaimAction(_input: AbiInput): ClaimAction {
    return { discriminant: "claim",  };
  }

  public deserializeExecuteAction(_input: AbiInput): ExecuteAction {
    return { discriminant: "execute",  };
  }

  public deserializeCancelAction(_input: AbiInput): CancelAction {
    return { discriminant: "cancel",  };
  }

  public deserializeStartCallbackCallback(_input: AbiInput): StartCallbackCallback {
    return { discriminant: "start_callback",  };
  }

  public deserializeBidCallbackCallback(_input: AbiInput): BidCallbackCallback {
    const bid: Bid = this.deserializeBid(_input);
    return { discriminant: "bid_callback", bid };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const tokenAmountForSale: BN = _input.readU64();
    const tokenForSale: BlockchainAddress = _input.readAddress();
    const tokenForBidding: BlockchainAddress = _input.readAddress();
    const reservePrice: BN = _input.readU64();
    const minIncrement: BN = _input.readU64();
    const auctionDurationHours: number = _input.readU32();
    return { discriminant: "initialize", tokenAmountForSale, tokenForSale, tokenForBidding, reservePrice, minIncrement, auctionDurationHours };
  }

}
export interface Bid {
  bidder: BlockchainAddress;
  amount: BN;
}
function serializeBid(_out: AbiOutput, _value: Bid): void {
  const { bidder, amount } = _value;
  _out.writeAddress(bidder);
  _out.writeU64(amount);
}

export interface TokenClaim {
  tokensForBidding: BN;
  tokensForSale: BN;
}

export interface AuctionContractState {
  contractOwner: BlockchainAddress;
  startTimeMillis: BN;
  endTimeMillis: BN;
  tokenAmountForSale: BN;
  tokenForSale: BlockchainAddress;
  tokenForBidding: BlockchainAddress;
  highestBidder: Bid;
  reservePrice: BN;
  minIncrement: BN;
  claimMap: Map<BlockchainAddress, TokenClaim>;
  status: number;
}

export function initialize(tokenAmountForSale: BN, tokenForSale: BlockchainAddress, tokenForBidding: BlockchainAddress, reservePrice: BN, minIncrement: BN, auctionDurationHours: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU64(tokenAmountForSale);
    _out.writeAddress(tokenForSale);
    _out.writeAddress(tokenForBidding);
    _out.writeU64(reservePrice);
    _out.writeU64(minIncrement);
    _out.writeU32(auctionDurationHours);
  });
}

export function start(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function bid(bidAmount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeU64(bidAmount);
  });
}

export function claim(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function execute(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
  });
}

export function cancel(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("07", "hex"));
  });
}

export function startCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function bidCallback(bid: Bid): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    serializeBid(_out, bid);
  });
}

export function deserializeState(state: StateWithClient): AuctionContractState;
export function deserializeState(bytes: Buffer): AuctionContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): AuctionContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): AuctionContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new AuctionContract_SDK_11_0_0(client, address).deserializeAuctionContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new AuctionContract_SDK_11_0_0(
      state.client,
      state.address
    ).deserializeAuctionContractState(input);
  }
}

export type Action =
  | StartAction
  | BidAction
  | ClaimAction
  | ExecuteAction
  | CancelAction;

export interface StartAction {
  discriminant: "start";
}
export interface BidAction {
  discriminant: "bid";
  bidAmount: BN;
}
export interface ClaimAction {
  discriminant: "claim";
}
export interface ExecuteAction {
  discriminant: "execute";
}
export interface CancelAction {
  discriminant: "cancel";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new AuctionContract_SDK_11_0_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeStartAction(input);
  } else if (shortname === "03") {
    return contract.deserializeBidAction(input);
  } else if (shortname === "05") {
    return contract.deserializeClaimAction(input);
  } else if (shortname === "06") {
    return contract.deserializeExecuteAction(input);
  } else if (shortname === "07") {
    return contract.deserializeCancelAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | StartCallbackCallback
  | BidCallbackCallback;

export interface StartCallbackCallback {
  discriminant: "start_callback";
}
export interface BidCallbackCallback {
  discriminant: "bid_callback";
  bid: Bid;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new AuctionContract_SDK_11_0_0(undefined, undefined);
  if (shortname === "02") {
    return contract.deserializeStartCallbackCallback(input);
  } else if (shortname === "04") {
    return contract.deserializeBidCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  tokenAmountForSale: BN;
  tokenForSale: BlockchainAddress;
  tokenForBidding: BlockchainAddress;
  reservePrice: BN;
  minIncrement: BN;
  auctionDurationHours: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new AuctionContract_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

