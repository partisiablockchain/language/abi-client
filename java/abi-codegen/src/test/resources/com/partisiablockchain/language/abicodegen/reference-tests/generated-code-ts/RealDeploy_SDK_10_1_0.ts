// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class RealDeploy_SDK_10_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeSemanticVersion(_input: AbiInput): SemanticVersion {
    const major: number = _input.readU32();
    const minor: number = _input.readU32();
    const patch: number = _input.readU32();
    return { major, minor, patch };
  }
  public deserializeRealDeployContractState(_input: AbiInput): RealDeployContractState {
    const bindingJar_vecLength = _input.readI32();
    const bindingJar: Buffer = _input.readBytes(bindingJar_vecLength);
    const preProcess: BlockchainAddress = _input.readAddress();
    const zkNodeRegistry: BlockchainAddress = _input.readAddress();
    const supportedProtocolVersion: SemanticVersion = this.deserializeSemanticVersion(_input);
    return { bindingJar, preProcess, zkNodeRegistry, supportedProtocolVersion };
  }
  public async getState(): Promise<RealDeployContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeRealDeployContractState(input);
  }

  public deserializeDeployContractV1Action(_input: AbiInput): DeployContractV1Action {
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const initialization_vecLength = _input.readI32();
    const initialization: Buffer = _input.readBytes(initialization_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const requiredStakes: BN = _input.readI64();
    return { discriminant: "deploy_contract_v1", contractJar, initialization, abi, requiredStakes };
  }

  public deserializeDeployContractV2Action(_input: AbiInput): DeployContractV2Action {
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const initialization_vecLength = _input.readI32();
    const initialization: Buffer = _input.readBytes(initialization_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const requiredStakes: BN = _input.readI64();
    const jurisdictions_vecLength = _input.readI32();
    const jurisdictions: number[][] = [];
    for (let jurisdictions_i = 0; jurisdictions_i < jurisdictions_vecLength; jurisdictions_i++) {
      const jurisdictions_elem_vecLength = _input.readI32();
      const jurisdictions_elem: number[] = [];
      for (let jurisdictions_elem_i = 0; jurisdictions_elem_i < jurisdictions_elem_vecLength; jurisdictions_elem_i++) {
        const jurisdictions_elem_elem: number = _input.readI32();
        jurisdictions_elem.push(jurisdictions_elem_elem);
      }
      jurisdictions.push(jurisdictions_elem);
    }
    return { discriminant: "deploy_contract_v2", contractJar, initialization, abi, requiredStakes, jurisdictions };
  }

  public deserializeNodeRequestCallbackCallback(_input: AbiInput): NodeRequestCallbackCallback {
    const contractAddress: BlockchainAddress = _input.readAddress();
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const initialization_vecLength = _input.readI32();
    const initialization: Buffer = _input.readBytes(initialization_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const tokensLockedToContract: BN = _input.readI64();
    return { discriminant: "node_request_callback", contractAddress, contractJar, initialization, abi, tokensLockedToContract };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const bindingJar_vecLength = _input.readI32();
    const bindingJar: Buffer = _input.readBytes(bindingJar_vecLength);
    const preProcess: BlockchainAddress = _input.readAddress();
    const zkNodeRegistry: BlockchainAddress = _input.readAddress();
    const supportedProtocolVersion: SemanticVersion = this.deserializeSemanticVersion(_input);
    return { discriminant: "init", bindingJar, preProcess, zkNodeRegistry, supportedProtocolVersion };
  }

}
export interface SemanticVersion {
  major: number;
  minor: number;
  patch: number;
}
function serializeSemanticVersion(_out: AbiOutput, _value: SemanticVersion): void {
  const { major, minor, patch } = _value;
  _out.writeU32(major);
  _out.writeU32(minor);
  _out.writeU32(patch);
}

export interface RealDeployContractState {
  bindingJar: Buffer;
  preProcess: BlockchainAddress;
  zkNodeRegistry: BlockchainAddress;
  supportedProtocolVersion: SemanticVersion;
}

export function init(bindingJar: Buffer, preProcess: BlockchainAddress, zkNodeRegistry: BlockchainAddress, supportedProtocolVersion: SemanticVersion): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(bindingJar.length);
    _out.writeBytes(bindingJar);
    _out.writeAddress(preProcess);
    _out.writeAddress(zkNodeRegistry);
    serializeSemanticVersion(_out, supportedProtocolVersion);
  });
}

export function deployContractV1(contractJar: Buffer, initialization: Buffer, abi: Buffer, requiredStakes: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeI32(contractJar.length);
    _out.writeBytes(contractJar);
    _out.writeI32(initialization.length);
    _out.writeBytes(initialization);
    _out.writeI32(abi.length);
    _out.writeBytes(abi);
    _out.writeI64(requiredStakes);
  });
}

export function deployContractV2(contractJar: Buffer, initialization: Buffer, abi: Buffer, requiredStakes: BN, jurisdictions: number[][]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeI32(contractJar.length);
    _out.writeBytes(contractJar);
    _out.writeI32(initialization.length);
    _out.writeBytes(initialization);
    _out.writeI32(abi.length);
    _out.writeBytes(abi);
    _out.writeI64(requiredStakes);
    _out.writeI32(jurisdictions.length);
    for (const jurisdictions_vec of jurisdictions) {
      _out.writeI32(jurisdictions_vec.length);
      for (const jurisdictions_vec_vec of jurisdictions_vec) {
        _out.writeI32(jurisdictions_vec_vec);
      }
    }
  });
}

export function nodeRequestCallback(contractAddress: BlockchainAddress, contractJar: Buffer, initialization: Buffer, abi: Buffer, tokensLockedToContract: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeAddress(contractAddress);
    _out.writeI32(contractJar.length);
    _out.writeBytes(contractJar);
    _out.writeI32(initialization.length);
    _out.writeBytes(initialization);
    _out.writeI32(abi.length);
    _out.writeBytes(abi);
    _out.writeI64(tokensLockedToContract);
  });
}

export function deserializeState(state: StateWithClient): RealDeployContractState;
export function deserializeState(bytes: Buffer): RealDeployContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): RealDeployContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): RealDeployContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new RealDeploy_SDK_10_1_0(client, address).deserializeRealDeployContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new RealDeploy_SDK_10_1_0(
      state.client,
      state.address
    ).deserializeRealDeployContractState(input);
  }
}

export type Action =
  | DeployContractV1Action
  | DeployContractV2Action;

export interface DeployContractV1Action {
  discriminant: "deploy_contract_v1";
  contractJar: Buffer;
  initialization: Buffer;
  abi: Buffer;
  requiredStakes: BN;
}
export interface DeployContractV2Action {
  discriminant: "deploy_contract_v2";
  contractJar: Buffer;
  initialization: Buffer;
  abi: Buffer;
  requiredStakes: BN;
  jurisdictions: number[][];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RealDeploy_SDK_10_1_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeDeployContractV1Action(input);
  } else if (shortname === "01") {
    return contract.deserializeDeployContractV2Action(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | NodeRequestCallbackCallback;

export interface NodeRequestCallbackCallback {
  discriminant: "node_request_callback";
  contractAddress: BlockchainAddress;
  contractJar: Buffer;
  initialization: Buffer;
  abi: Buffer;
  tokensLockedToContract: BN;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RealDeploy_SDK_10_1_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeNodeRequestCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  bindingJar: Buffer;
  preProcess: BlockchainAddress;
  zkNodeRegistry: BlockchainAddress;
  supportedProtocolVersion: SemanticVersion;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RealDeploy_SDK_10_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

