// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class RustExampleZkEvents_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public RustExampleZkEvents_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private OpenState deserializeOpenState(AbiInput _input) {
    return new OpenState();
  }
  public OpenState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeOpenState(input);
  }

  private ReturnInteractionAction deserializeReturnInteractionAction(AbiInput _input) {
    return new ReturnInteractionAction();
  }

  private ReturnCallbackAction deserializeReturnCallbackAction(AbiInput _input) {
    return new ReturnCallbackAction();
  }

  private ReturnValueAction deserializeReturnValueAction(AbiInput _input) {
    return new ReturnValueAction();
  }

  private CallbackCallback deserializeCallbackCallback(AbiInput _input) {
    return new CallbackCallback();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record OpenState() {
    public static OpenState deserialize(byte[] bytes) {
      return RustExampleZkEvents_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] returnInteraction() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static byte[] returnCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] returnValue() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static SecretInputBuilder<Integer> onSecretInput() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static byte[] callback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static OpenState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new RustExampleZkEvents_SDK_13_3_0(client, address).deserializeOpenState(input);
  }
  
  public static OpenState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static OpenState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ReturnInteractionAction() implements Action {
  }
  @AbiGenerated
  public record ReturnCallbackAction() implements Action {
  }
  @AbiGenerated
  public record ReturnValueAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new RustExampleZkEvents_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeReturnInteractionAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeReturnCallbackAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeReturnValueAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record CallbackCallback() implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RustExampleZkEvents_SDK_13_3_0(null, null);
    if (shortname.equals("05")) {
      return contract.deserializeCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RustExampleZkEvents_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
