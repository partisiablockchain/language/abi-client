// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class BpOrchestration_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeBlockProducer(_input: AbiInput): BlockProducer {
    let name: Option<string> = undefined;
    const name_isSome = _input.readBoolean();
    if (name_isSome) {
      const name_option: string = _input.readString();
      name = name_option;
    }
    let address: Option<string> = undefined;
    const address_isSome = _input.readBoolean();
    if (address_isSome) {
      const address_option: string = _input.readString();
      address = address_option;
    }
    let website: Option<string> = undefined;
    const website_isSome = _input.readBoolean();
    if (website_isSome) {
      const website_option: string = _input.readString();
      website = website_option;
    }
    let identity: Option<BlockchainAddress> = undefined;
    const identity_isSome = _input.readBoolean();
    if (identity_isSome) {
      const identity_option: BlockchainAddress = _input.readAddress();
      identity = identity_option;
    }
    const entityJurisdiction: number = _input.readI32();
    const serverJurisdiction: number = _input.readI32();
    let publicKey: Option<BlockchainPublicKey> = undefined;
    const publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      const publicKey_option: BlockchainPublicKey = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    let blsPublicKey: Option<BlsPublicKey> = undefined;
    const blsPublicKey_isSome = _input.readBoolean();
    if (blsPublicKey_isSome) {
      const blsPublicKey_option: BlsPublicKey = _input.readBlsPublicKey();
      blsPublicKey = blsPublicKey_option;
    }
    const numberOfVotes: BN = _input.readI64();
    let status: Option<BlockProducerStatus> = undefined;
    const status_isSome = _input.readBoolean();
    if (status_isSome) {
      const status_option: BlockProducerStatus = this.deserializeBlockProducerStatus(_input);
      status = status_option;
    }
    return { name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status };
  }
  public deserializeBlockProducerInformation(_input: AbiInput): BlockProducerInformation {
    const name: string = _input.readString();
    const website: string = _input.readString();
    const address: string = _input.readString();
    const publicKey: BlockchainPublicKey = _input.readPublicKey();
    const blsPublicKey: BlsPublicKey = _input.readBlsPublicKey();
    const entityJurisdiction: number = _input.readI32();
    const serverJurisdiction: number = _input.readI32();
    return { name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction };
  }
  public deserializeLargeOracleUpdate(_input: AbiInput): LargeOracleUpdate {
    let producers: Option<Array<Option<BlockProducer>>> = undefined;
    const producers_isSome = _input.readBoolean();
    if (producers_isSome) {
      const producers_option_vecLength = _input.readI32();
      const producers_option: Array<Option<BlockProducer>> = [];
      for (let producers_option_i = 0; producers_option_i < producers_option_vecLength; producers_option_i++) {
        let producers_option_elem: Option<BlockProducer> = undefined;
        const producers_option_elem_isSome = _input.readBoolean();
        if (producers_option_elem_isSome) {
          const producers_option_elem_option: BlockProducer = this.deserializeBlockProducer(_input);
          producers_option_elem = producers_option_elem_option;
        }
        producers_option.push(producers_option_elem);
      }
      producers = producers_option;
    }
    let thresholdKey: Option<ThresholdKey> = undefined;
    const thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      const thresholdKey_option: ThresholdKey = this.deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    let candidates: Option<Array<Option<CandidateKey>>> = undefined;
    const candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      const candidates_option_vecLength = _input.readI32();
      const candidates_option: Array<Option<CandidateKey>> = [];
      for (let candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        let candidates_option_elem: Option<CandidateKey> = undefined;
        const candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          const candidates_option_elem_option: CandidateKey = this.deserializeCandidateKey(_input);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.push(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    let honestPartyViews: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>> = undefined;
    const honestPartyViews_isSome = _input.readBoolean();
    if (honestPartyViews_isSome) {
      const honestPartyViews_option_mapLength = _input.readI32();
      const honestPartyViews_option: Map<Option<BlockchainAddress>, Option<Bitmap>> = new Map();
      for (let honestPartyViews_option_i = 0; honestPartyViews_option_i < honestPartyViews_option_mapLength; honestPartyViews_option_i++) {
        let honestPartyViews_option_key: Option<BlockchainAddress> = undefined;
        const honestPartyViews_option_key_isSome = _input.readBoolean();
        if (honestPartyViews_option_key_isSome) {
          const honestPartyViews_option_key_option: BlockchainAddress = _input.readAddress();
          honestPartyViews_option_key = honestPartyViews_option_key_option;
        }
        let honestPartyViews_option_value: Option<Bitmap> = undefined;
        const honestPartyViews_option_value_isSome = _input.readBoolean();
        if (honestPartyViews_option_value_isSome) {
          const honestPartyViews_option_value_option: Bitmap = this.deserializeBitmap(_input);
          honestPartyViews_option_value = honestPartyViews_option_value_option;
        }
        honestPartyViews_option.set(honestPartyViews_option_key, honestPartyViews_option_value);
      }
      honestPartyViews = honestPartyViews_option;
    }
    const retryNonce: number = _input.readI32();
    let voters: Option<Bitmap> = undefined;
    const voters_isSome = _input.readBoolean();
    if (voters_isSome) {
      const voters_option: Bitmap = this.deserializeBitmap(_input);
      voters = voters_option;
    }
    let activeProducers: Option<Bitmap> = undefined;
    const activeProducers_isSome = _input.readBoolean();
    if (activeProducers_isSome) {
      const activeProducers_option: Bitmap = this.deserializeBitmap(_input);
      activeProducers = activeProducers_option;
    }
    let heartbeats: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>> = undefined;
    const heartbeats_isSome = _input.readBoolean();
    if (heartbeats_isSome) {
      const heartbeats_option_mapLength = _input.readI32();
      const heartbeats_option: Map<Option<BlockchainAddress>, Option<Bitmap>> = new Map();
      for (let heartbeats_option_i = 0; heartbeats_option_i < heartbeats_option_mapLength; heartbeats_option_i++) {
        let heartbeats_option_key: Option<BlockchainAddress> = undefined;
        const heartbeats_option_key_isSome = _input.readBoolean();
        if (heartbeats_option_key_isSome) {
          const heartbeats_option_key_option: BlockchainAddress = _input.readAddress();
          heartbeats_option_key = heartbeats_option_key_option;
        }
        let heartbeats_option_value: Option<Bitmap> = undefined;
        const heartbeats_option_value_isSome = _input.readBoolean();
        if (heartbeats_option_value_isSome) {
          const heartbeats_option_value_option: Bitmap = this.deserializeBitmap(_input);
          heartbeats_option_value = heartbeats_option_value_option;
        }
        heartbeats_option.set(heartbeats_option_key, heartbeats_option_value);
      }
      heartbeats = heartbeats_option;
    }
    let broadcasters: Option<Bitmap> = undefined;
    const broadcasters_isSome = _input.readBoolean();
    if (broadcasters_isSome) {
      const broadcasters_option: Bitmap = this.deserializeBitmap(_input);
      broadcasters = broadcasters_option;
    }
    let broadcastMessages: Option<Array<Option<BroadcastTracker>>> = undefined;
    const broadcastMessages_isSome = _input.readBoolean();
    if (broadcastMessages_isSome) {
      const broadcastMessages_option_vecLength = _input.readI32();
      const broadcastMessages_option: Array<Option<BroadcastTracker>> = [];
      for (let broadcastMessages_option_i = 0; broadcastMessages_option_i < broadcastMessages_option_vecLength; broadcastMessages_option_i++) {
        let broadcastMessages_option_elem: Option<BroadcastTracker> = undefined;
        const broadcastMessages_option_elem_isSome = _input.readBoolean();
        if (broadcastMessages_option_elem_isSome) {
          const broadcastMessages_option_elem_option: BroadcastTracker = this.deserializeBroadcastTracker(_input);
          broadcastMessages_option_elem = broadcastMessages_option_elem_option;
        }
        broadcastMessages_option.push(broadcastMessages_option_elem);
      }
      broadcastMessages = broadcastMessages_option;
    }
    let signatureRandomization: Option<Hash> = undefined;
    const signatureRandomization_isSome = _input.readBoolean();
    if (signatureRandomization_isSome) {
      const signatureRandomization_option: Hash = _input.readHash();
      signatureRandomization = signatureRandomization_option;
    }
    return { producers, thresholdKey, candidates, honestPartyViews, retryNonce, voters, activeProducers, heartbeats, broadcasters, broadcastMessages, signatureRandomization };
  }
  public deserializeBroadcastTracker(_input: AbiInput): BroadcastTracker {
    let messages: Option<Map<Option<BlockchainAddress>, Option<Hash>>> = undefined;
    const messages_isSome = _input.readBoolean();
    if (messages_isSome) {
      const messages_option_mapLength = _input.readI32();
      const messages_option: Map<Option<BlockchainAddress>, Option<Hash>> = new Map();
      for (let messages_option_i = 0; messages_option_i < messages_option_mapLength; messages_option_i++) {
        let messages_option_key: Option<BlockchainAddress> = undefined;
        const messages_option_key_isSome = _input.readBoolean();
        if (messages_option_key_isSome) {
          const messages_option_key_option: BlockchainAddress = _input.readAddress();
          messages_option_key = messages_option_key_option;
        }
        let messages_option_value: Option<Hash> = undefined;
        const messages_option_value_isSome = _input.readBoolean();
        if (messages_option_value_isSome) {
          const messages_option_value_option: Hash = _input.readHash();
          messages_option_value = messages_option_value_option;
        }
        messages_option.set(messages_option_key, messages_option_value);
      }
      messages = messages_option;
    }
    let bitmaps: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>> = undefined;
    const bitmaps_isSome = _input.readBoolean();
    if (bitmaps_isSome) {
      const bitmaps_option_mapLength = _input.readI32();
      const bitmaps_option: Map<Option<BlockchainAddress>, Option<Bitmap>> = new Map();
      for (let bitmaps_option_i = 0; bitmaps_option_i < bitmaps_option_mapLength; bitmaps_option_i++) {
        let bitmaps_option_key: Option<BlockchainAddress> = undefined;
        const bitmaps_option_key_isSome = _input.readBoolean();
        if (bitmaps_option_key_isSome) {
          const bitmaps_option_key_option: BlockchainAddress = _input.readAddress();
          bitmaps_option_key = bitmaps_option_key_option;
        }
        let bitmaps_option_value: Option<Bitmap> = undefined;
        const bitmaps_option_value_isSome = _input.readBoolean();
        if (bitmaps_option_value_isSome) {
          const bitmaps_option_value_option: Bitmap = this.deserializeBitmap(_input);
          bitmaps_option_value = bitmaps_option_value_option;
        }
        bitmaps_option.set(bitmaps_option_key, bitmaps_option_value);
      }
      bitmaps = bitmaps_option;
    }
    let echos: Option<Bitmap> = undefined;
    const echos_isSome = _input.readBoolean();
    if (echos_isSome) {
      const echos_option: Bitmap = this.deserializeBitmap(_input);
      echos = echos_option;
    }
    const startTime: BN = _input.readI64();
    return { messages, bitmaps, echos, startTime };
  }
  public deserializeBlockProducerStatus(_input: AbiInput): BlockProducerStatus {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeBlockProducerStatusWaitingForCallback(_input);
    } else if (discriminant === 1) {
      return this.deserializeBlockProducerStatusPending(_input);
    } else if (discriminant === 2) {
      return this.deserializeBlockProducerStatusConfirmed(_input);
    } else if (discriminant === 3) {
      return this.deserializeBlockProducerStatusPendingUpdate(_input);
    } else if (discriminant === 4) {
      return this.deserializeBlockProducerStatusRemoved(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeBlockProducerStatusWaitingForCallback(_input: AbiInput): BlockProducerStatusWaitingForCallback {
    return { discriminant: BlockProducerStatusD.WaitingForCallback,  };
  }
  public deserializeBlockProducerStatusPending(_input: AbiInput): BlockProducerStatusPending {
    return { discriminant: BlockProducerStatusD.Pending,  };
  }
  public deserializeBlockProducerStatusConfirmed(_input: AbiInput): BlockProducerStatusConfirmed {
    return { discriminant: BlockProducerStatusD.Confirmed,  };
  }
  public deserializeBlockProducerStatusPendingUpdate(_input: AbiInput): BlockProducerStatusPendingUpdate {
    return { discriminant: BlockProducerStatusD.PendingUpdate,  };
  }
  public deserializeBlockProducerStatusRemoved(_input: AbiInput): BlockProducerStatusRemoved {
    return { discriminant: BlockProducerStatusD.Removed,  };
  }
  public deserializeCommitteeSignature(_input: AbiInput): CommitteeSignature {
    let committeeMembers: Option<Array<Option<BlockProducer>>> = undefined;
    const committeeMembers_isSome = _input.readBoolean();
    if (committeeMembers_isSome) {
      const committeeMembers_option_vecLength = _input.readI32();
      const committeeMembers_option: Array<Option<BlockProducer>> = [];
      for (let committeeMembers_option_i = 0; committeeMembers_option_i < committeeMembers_option_vecLength; committeeMembers_option_i++) {
        let committeeMembers_option_elem: Option<BlockProducer> = undefined;
        const committeeMembers_option_elem_isSome = _input.readBoolean();
        if (committeeMembers_option_elem_isSome) {
          const committeeMembers_option_elem_option: BlockProducer = this.deserializeBlockProducer(_input);
          committeeMembers_option_elem = committeeMembers_option_elem_option;
        }
        committeeMembers_option.push(committeeMembers_option_elem);
      }
      committeeMembers = committeeMembers_option;
    }
    let signature: Option<Signature> = undefined;
    const signature_isSome = _input.readBoolean();
    if (signature_isSome) {
      const signature_option: Signature = _input.readSignature();
      signature = signature_option;
    }
    let thresholdKey: Option<BlockchainPublicKey> = undefined;
    const thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      const thresholdKey_option: BlockchainPublicKey = _input.readPublicKey();
      thresholdKey = thresholdKey_option;
    }
    return { committeeMembers, signature, thresholdKey };
  }
  public deserializeThresholdKey(_input: AbiInput): ThresholdKey {
    let key: Option<BlockchainPublicKey> = undefined;
    const key_isSome = _input.readBoolean();
    if (key_isSome) {
      const key_option: BlockchainPublicKey = _input.readPublicKey();
      key = key_option;
    }
    let keyId: Option<string> = undefined;
    const keyId_isSome = _input.readBoolean();
    if (keyId_isSome) {
      const keyId_option: string = _input.readString();
      keyId = keyId_option;
    }
    return { key, keyId };
  }
  public deserializeCandidateKey(_input: AbiInput): CandidateKey {
    let thresholdKey: Option<ThresholdKey> = undefined;
    const thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      const thresholdKey_option: ThresholdKey = this.deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    const votes: number = _input.readI32();
    return { thresholdKey, votes };
  }
  public deserializeBitmap(_input: AbiInput): Bitmap {
    let bits: Option<Buffer> = undefined;
    const bits_isSome = _input.readBoolean();
    if (bits_isSome) {
      const bits_option_vecLength = _input.readI32();
      const bits_option: Buffer = _input.readBytes(bits_option_vecLength);
      bits = bits_option;
    }
    return { bits };
  }
  public deserializeBpOrchestrationContractState(_input: AbiInput): BpOrchestrationContractState {
    let thresholdKey: Option<ThresholdKey> = undefined;
    const thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      const thresholdKey_option: ThresholdKey = this.deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    let kycAddresses: Option<Array<Option<BlockchainAddress>>> = undefined;
    const kycAddresses_isSome = _input.readBoolean();
    if (kycAddresses_isSome) {
      const kycAddresses_option_vecLength = _input.readI32();
      const kycAddresses_option: Array<Option<BlockchainAddress>> = [];
      for (let kycAddresses_option_i = 0; kycAddresses_option_i < kycAddresses_option_vecLength; kycAddresses_option_i++) {
        let kycAddresses_option_elem: Option<BlockchainAddress> = undefined;
        const kycAddresses_option_elem_isSome = _input.readBoolean();
        if (kycAddresses_option_elem_isSome) {
          const kycAddresses_option_elem_option: BlockchainAddress = _input.readAddress();
          kycAddresses_option_elem = kycAddresses_option_elem_option;
        }
        kycAddresses_option.push(kycAddresses_option_elem);
      }
      kycAddresses = kycAddresses_option;
    }
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    let oracleUpdate: Option<LargeOracleUpdate> = undefined;
    const oracleUpdate_isSome = _input.readBoolean();
    if (oracleUpdate_isSome) {
      const oracleUpdate_option: LargeOracleUpdate = this.deserializeLargeOracleUpdate(_input);
      oracleUpdate = oracleUpdate_option;
    }
    let blockProducers: Option<Map<Option<BlockchainAddress>, Option<BlockProducer>>> = undefined;
    const blockProducers_isSome = _input.readBoolean();
    if (blockProducers_isSome) {
      const blockProducers_option_mapLength = _input.readI32();
      const blockProducers_option: Map<Option<BlockchainAddress>, Option<BlockProducer>> = new Map();
      for (let blockProducers_option_i = 0; blockProducers_option_i < blockProducers_option_mapLength; blockProducers_option_i++) {
        let blockProducers_option_key: Option<BlockchainAddress> = undefined;
        const blockProducers_option_key_isSome = _input.readBoolean();
        if (blockProducers_option_key_isSome) {
          const blockProducers_option_key_option: BlockchainAddress = _input.readAddress();
          blockProducers_option_key = blockProducers_option_key_option;
        }
        let blockProducers_option_value: Option<BlockProducer> = undefined;
        const blockProducers_option_value_isSome = _input.readBoolean();
        if (blockProducers_option_value_isSome) {
          const blockProducers_option_value_option: BlockProducer = this.deserializeBlockProducer(_input);
          blockProducers_option_value = blockProducers_option_value_option;
        }
        blockProducers_option.set(blockProducers_option_key, blockProducers_option_value);
      }
      blockProducers = blockProducers_option;
    }
    const confirmedSinceLastCommittee: BN = _input.readI64();
    let committee: Option<Array<Option<BlockProducer>>> = undefined;
    const committee_isSome = _input.readBoolean();
    if (committee_isSome) {
      const committee_option_vecLength = _input.readI32();
      const committee_option: Array<Option<BlockProducer>> = [];
      for (let committee_option_i = 0; committee_option_i < committee_option_vecLength; committee_option_i++) {
        let committee_option_elem: Option<BlockProducer> = undefined;
        const committee_option_elem_isSome = _input.readBoolean();
        if (committee_option_elem_isSome) {
          const committee_option_elem_option: BlockProducer = this.deserializeBlockProducer(_input);
          committee_option_elem = committee_option_elem_option;
        }
        committee_option.push(committee_option_elem);
      }
      committee = committee_option;
    }
    let largeOracleContract: Option<BlockchainAddress> = undefined;
    const largeOracleContract_isSome = _input.readBoolean();
    if (largeOracleContract_isSome) {
      const largeOracleContract_option: BlockchainAddress = _input.readAddress();
      largeOracleContract = largeOracleContract_option;
    }
    let systemUpdateContractAddress: Option<BlockchainAddress> = undefined;
    const systemUpdateContractAddress_isSome = _input.readBoolean();
    if (systemUpdateContractAddress_isSome) {
      const systemUpdateContractAddress_option: BlockchainAddress = _input.readAddress();
      systemUpdateContractAddress = systemUpdateContractAddress_option;
    }
    let rewardsContractAddress: Option<BlockchainAddress> = undefined;
    const rewardsContractAddress_isSome = _input.readBoolean();
    if (rewardsContractAddress_isSome) {
      const rewardsContractAddress_option: BlockchainAddress = _input.readAddress();
      rewardsContractAddress = rewardsContractAddress_option;
    }
    let committeeLog: Option<Map<Option<number>, Option<CommitteeSignature>>> = undefined;
    const committeeLog_isSome = _input.readBoolean();
    if (committeeLog_isSome) {
      const committeeLog_option_mapLength = _input.readI32();
      const committeeLog_option: Map<Option<number>, Option<CommitteeSignature>> = new Map();
      for (let committeeLog_option_i = 0; committeeLog_option_i < committeeLog_option_mapLength; committeeLog_option_i++) {
        let committeeLog_option_key: Option<number> = undefined;
        const committeeLog_option_key_isSome = _input.readBoolean();
        if (committeeLog_option_key_isSome) {
          const committeeLog_option_key_option: number = _input.readI32();
          committeeLog_option_key = committeeLog_option_key_option;
        }
        let committeeLog_option_value: Option<CommitteeSignature> = undefined;
        const committeeLog_option_value_isSome = _input.readBoolean();
        if (committeeLog_option_value_isSome) {
          const committeeLog_option_value_option: CommitteeSignature = this.deserializeCommitteeSignature(_input);
          committeeLog_option_value = committeeLog_option_value_option;
        }
        committeeLog_option.set(committeeLog_option_key, committeeLog_option_value);
      }
      committeeLog = committeeLog_option;
    }
    let domainSeparator: Option<Hash> = undefined;
    const domainSeparator_isSome = _input.readBoolean();
    if (domainSeparator_isSome) {
      const domainSeparator_option: Hash = _input.readHash();
      domainSeparator = domainSeparator_option;
    }
    const broadcastRoundDelay: BN = _input.readI64();
    return { thresholdKey, kycAddresses, sessionId, retryNonce, oracleUpdate, blockProducers, confirmedSinceLastCommittee, committee, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, committeeLog, domainSeparator, broadcastRoundDelay };
  }
  public async getState(): Promise<BpOrchestrationContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeBpOrchestrationContractState(input);
  }

  public deserializeRegisterAction(_input: AbiInput): RegisterAction {
    const producer: BlockProducerInformation = this.deserializeBlockProducerInformation(_input);
    const popSignature: BlsSignature = _input.readBlsSignature();
    return { discriminant: "register", producer, popSignature };
  }

  public deserializeConfirmBpAction(_input: AbiInput): ConfirmBpAction {
    const bpAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "confirm_bp", bpAddress };
  }

  public deserializeUpdateInfoAction(_input: AbiInput): UpdateInfoAction {
    const updatedName: string = _input.readString();
    const updatedWebsite: string = _input.readString();
    const updatedAddress: string = _input.readString();
    const updatedEntityJurisdiction: number = _input.readI32();
    const updatedServerJurisdiction: number = _input.readI32();
    return { discriminant: "update_info", updatedName, updatedWebsite, updatedAddress, updatedEntityJurisdiction, updatedServerJurisdiction };
  }

  public deserializeGetProducerInfoAction(_input: AbiInput): GetProducerInfoAction {
    const identity: BlockchainAddress = _input.readAddress();
    return { discriminant: "get_producer_info", identity };
  }

  public deserializeAddConfirmedBpAction(_input: AbiInput): AddConfirmedBpAction {
    const address: BlockchainAddress = _input.readAddress();
    const producer: BlockProducerInformation = this.deserializeBlockProducerInformation(_input);
    const popSignature: BlsSignature = _input.readBlsSignature();
    return { discriminant: "add_confirmed_bp", address, producer, popSignature };
  }

  public deserializeRemoveBpAction(_input: AbiInput): RemoveBpAction {
    return { discriminant: "remove_bp",  };
  }

  public deserializeAddCandidateKeyAction(_input: AbiInput): AddCandidateKeyAction {
    const candidateKey: BlockchainPublicKey = _input.readPublicKey();
    const keyId: string = _input.readString();
    const bitmap: Bitmap = this.deserializeBitmap(_input);
    return { discriminant: "add_candidate_key", candidateKey, keyId, bitmap };
  }

  public deserializeAuthorizeNewOracleAction(_input: AbiInput): AuthorizeNewOracleAction {
    const signature: Signature = _input.readSignature();
    return { discriminant: "authorize_new_oracle", signature };
  }

  public deserializeAddHeartbeatAction(_input: AbiInput): AddHeartbeatAction {
    const bitmap: Bitmap = this.deserializeBitmap(_input);
    return { discriminant: "add_heartbeat", bitmap };
  }

  public deserializeAddBroadcastHashAction(_input: AbiInput): AddBroadcastHashAction {
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    const broadcastRound: number = _input.readI32();
    const broadcast: Hash = _input.readHash();
    return { discriminant: "add_broadcast_hash", sessionId, retryNonce, broadcastRound, broadcast };
  }

  public deserializeAddBroadcastBitsAction(_input: AbiInput): AddBroadcastBitsAction {
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    const broadcastRound: number = _input.readI32();
    const bits_vecLength = _input.readI32();
    const bits: Buffer = _input.readBytes(bits_vecLength);
    return { discriminant: "add_broadcast_bits", sessionId, retryNonce, broadcastRound, bits };
  }

  public deserializePokeOngoingUpdateAction(_input: AbiInput): PokeOngoingUpdateAction {
    return { discriminant: "poke_ongoing_update",  };
  }

  public deserializeTriggerNewCommitteeAction(_input: AbiInput): TriggerNewCommitteeAction {
    return { discriminant: "trigger_new_committee",  };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const kycAddresses_vecLength = _input.readI32();
    const kycAddresses: BlockchainAddress[] = [];
    for (let kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      const kycAddresses_elem: BlockchainAddress = _input.readAddress();
      kycAddresses.push(kycAddresses_elem);
    }
    const initialProducers_vecLength = _input.readI32();
    const initialProducers: BlockProducer[] = [];
    for (let initialProducers_i = 0; initialProducers_i < initialProducers_vecLength; initialProducers_i++) {
      const initialProducers_elem: BlockProducer = this.deserializeBlockProducer(_input);
      initialProducers.push(initialProducers_elem);
    }
    const initialThresholdKey: BlockchainPublicKey = _input.readPublicKey();
    const initialThresholdKeyId: string = _input.readString();
    const largeOracleContract: BlockchainAddress = _input.readAddress();
    const systemUpdateContractAddress: BlockchainAddress = _input.readAddress();
    const rewardsContractAddress: BlockchainAddress = _input.readAddress();
    const domainSeparator: Hash = _input.readHash();
    const broadcastRoundDelay: BN = _input.readI64();
    return { discriminant: "init", kycAddresses, initialProducers, initialThresholdKey, initialThresholdKeyId, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, domainSeparator, broadcastRoundDelay };
  }

}
export interface BlockProducer {
  name: Option<string>;
  address: Option<string>;
  website: Option<string>;
  identity: Option<BlockchainAddress>;
  entityJurisdiction: number;
  serverJurisdiction: number;
  publicKey: Option<BlockchainPublicKey>;
  blsPublicKey: Option<BlsPublicKey>;
  numberOfVotes: BN;
  status: Option<BlockProducerStatus>;
}
function serializeBlockProducer(_out: AbiOutput, _value: BlockProducer): void {
  const { name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status } = _value;
  _out.writeBoolean(name !== undefined);
  if (name !== undefined) {
    _out.writeString(name);
  }
  _out.writeBoolean(address !== undefined);
  if (address !== undefined) {
    _out.writeString(address);
  }
  _out.writeBoolean(website !== undefined);
  if (website !== undefined) {
    _out.writeString(website);
  }
  _out.writeBoolean(identity !== undefined);
  if (identity !== undefined) {
    _out.writeAddress(identity);
  }
  _out.writeI32(entityJurisdiction);
  _out.writeI32(serverJurisdiction);
  _out.writeBoolean(publicKey !== undefined);
  if (publicKey !== undefined) {
    _out.writePublicKey(publicKey);
  }
  _out.writeBoolean(blsPublicKey !== undefined);
  if (blsPublicKey !== undefined) {
    _out.writeBlsPublicKey(blsPublicKey);
  }
  _out.writeI64(numberOfVotes);
  _out.writeBoolean(status !== undefined);
  if (status !== undefined) {
    serializeBlockProducerStatus(_out, status);
  }
}

export interface BlockProducerInformation {
  name: string;
  website: string;
  address: string;
  publicKey: BlockchainPublicKey;
  blsPublicKey: BlsPublicKey;
  entityJurisdiction: number;
  serverJurisdiction: number;
}
function serializeBlockProducerInformation(_out: AbiOutput, _value: BlockProducerInformation): void {
  const { name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction } = _value;
  _out.writeString(name);
  _out.writeString(website);
  _out.writeString(address);
  _out.writePublicKey(publicKey);
  _out.writeBlsPublicKey(blsPublicKey);
  _out.writeI32(entityJurisdiction);
  _out.writeI32(serverJurisdiction);
}

export interface LargeOracleUpdate {
  producers: Option<Array<Option<BlockProducer>>>;
  thresholdKey: Option<ThresholdKey>;
  candidates: Option<Array<Option<CandidateKey>>>;
  honestPartyViews: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>>;
  retryNonce: number;
  voters: Option<Bitmap>;
  activeProducers: Option<Bitmap>;
  heartbeats: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>>;
  broadcasters: Option<Bitmap>;
  broadcastMessages: Option<Array<Option<BroadcastTracker>>>;
  signatureRandomization: Option<Hash>;
}

export interface BroadcastTracker {
  messages: Option<Map<Option<BlockchainAddress>, Option<Hash>>>;
  bitmaps: Option<Map<Option<BlockchainAddress>, Option<Bitmap>>>;
  echos: Option<Bitmap>;
  startTime: BN;
}

export enum BlockProducerStatusD {
  WaitingForCallback = 0,
  Pending = 1,
  Confirmed = 2,
  PendingUpdate = 3,
  Removed = 4,
}
export type BlockProducerStatus =
  | BlockProducerStatusWaitingForCallback
  | BlockProducerStatusPending
  | BlockProducerStatusConfirmed
  | BlockProducerStatusPendingUpdate
  | BlockProducerStatusRemoved;
function serializeBlockProducerStatus(out: AbiOutput, value: BlockProducerStatus): void {
  if (value.discriminant === BlockProducerStatusD.WaitingForCallback) {
    return serializeBlockProducerStatusWaitingForCallback(out, value);
  } else if (value.discriminant === BlockProducerStatusD.Pending) {
    return serializeBlockProducerStatusPending(out, value);
  } else if (value.discriminant === BlockProducerStatusD.Confirmed) {
    return serializeBlockProducerStatusConfirmed(out, value);
  } else if (value.discriminant === BlockProducerStatusD.PendingUpdate) {
    return serializeBlockProducerStatusPendingUpdate(out, value);
  } else if (value.discriminant === BlockProducerStatusD.Removed) {
    return serializeBlockProducerStatusRemoved(out, value);
  }
}

export interface BlockProducerStatusWaitingForCallback {
  discriminant: BlockProducerStatusD.WaitingForCallback;
}
function serializeBlockProducerStatusWaitingForCallback(_out: AbiOutput, _value: BlockProducerStatusWaitingForCallback): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface BlockProducerStatusPending {
  discriminant: BlockProducerStatusD.Pending;
}
function serializeBlockProducerStatusPending(_out: AbiOutput, _value: BlockProducerStatusPending): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface BlockProducerStatusConfirmed {
  discriminant: BlockProducerStatusD.Confirmed;
}
function serializeBlockProducerStatusConfirmed(_out: AbiOutput, _value: BlockProducerStatusConfirmed): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface BlockProducerStatusPendingUpdate {
  discriminant: BlockProducerStatusD.PendingUpdate;
}
function serializeBlockProducerStatusPendingUpdate(_out: AbiOutput, _value: BlockProducerStatusPendingUpdate): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface BlockProducerStatusRemoved {
  discriminant: BlockProducerStatusD.Removed;
}
function serializeBlockProducerStatusRemoved(_out: AbiOutput, _value: BlockProducerStatusRemoved): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface CommitteeSignature {
  committeeMembers: Option<Array<Option<BlockProducer>>>;
  signature: Option<Signature>;
  thresholdKey: Option<BlockchainPublicKey>;
}

export interface ThresholdKey {
  key: Option<BlockchainPublicKey>;
  keyId: Option<string>;
}

export interface CandidateKey {
  thresholdKey: Option<ThresholdKey>;
  votes: number;
}

export interface Bitmap {
  bits: Option<Buffer>;
}
function serializeBitmap(_out: AbiOutput, _value: Bitmap): void {
  const { bits } = _value;
  _out.writeBoolean(bits !== undefined);
  if (bits !== undefined) {
    _out.writeI32(bits.length);
    _out.writeBytes(bits);
  }
}

export interface BpOrchestrationContractState {
  thresholdKey: Option<ThresholdKey>;
  kycAddresses: Option<Array<Option<BlockchainAddress>>>;
  sessionId: number;
  retryNonce: number;
  oracleUpdate: Option<LargeOracleUpdate>;
  blockProducers: Option<Map<Option<BlockchainAddress>, Option<BlockProducer>>>;
  confirmedSinceLastCommittee: BN;
  committee: Option<Array<Option<BlockProducer>>>;
  largeOracleContract: Option<BlockchainAddress>;
  systemUpdateContractAddress: Option<BlockchainAddress>;
  rewardsContractAddress: Option<BlockchainAddress>;
  committeeLog: Option<Map<Option<number>, Option<CommitteeSignature>>>;
  domainSeparator: Option<Hash>;
  broadcastRoundDelay: BN;
}

export function init(kycAddresses: BlockchainAddress[], initialProducers: BlockProducer[], initialThresholdKey: BlockchainPublicKey, initialThresholdKeyId: string, largeOracleContract: BlockchainAddress, systemUpdateContractAddress: BlockchainAddress, rewardsContractAddress: BlockchainAddress, domainSeparator: Hash, broadcastRoundDelay: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(kycAddresses.length);
    for (const kycAddresses_vec of kycAddresses) {
      _out.writeAddress(kycAddresses_vec);
    }
    _out.writeI32(initialProducers.length);
    for (const initialProducers_vec of initialProducers) {
      serializeBlockProducer(_out, initialProducers_vec);
    }
    _out.writePublicKey(initialThresholdKey);
    _out.writeString(initialThresholdKeyId);
    _out.writeAddress(largeOracleContract);
    _out.writeAddress(systemUpdateContractAddress);
    _out.writeAddress(rewardsContractAddress);
    _out.writeHash(domainSeparator);
    _out.writeI64(broadcastRoundDelay);
  });
}

export function register(producer: BlockProducerInformation, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    serializeBlockProducerInformation(_out, producer);
    _out.writeBlsSignature(popSignature);
  });
}

export function confirmBp(bpAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(bpAddress);
  });
}

export function updateInfo(updatedName: string, updatedWebsite: string, updatedAddress: string, updatedEntityJurisdiction: number, updatedServerJurisdiction: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeString(updatedName);
    _out.writeString(updatedWebsite);
    _out.writeString(updatedAddress);
    _out.writeI32(updatedEntityJurisdiction);
    _out.writeI32(updatedServerJurisdiction);
  });
}

export function getProducerInfo(identity: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("09", "hex"));
    _out.writeAddress(identity);
  });
}

export function addConfirmedBp(address: BlockchainAddress, producer: BlockProducerInformation, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0a", "hex"));
    _out.writeAddress(address);
    serializeBlockProducerInformation(_out, producer);
    _out.writeBlsSignature(popSignature);
  });
}

export function removeBp(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0b", "hex"));
  });
}

export function addCandidateKey(candidateKey: BlockchainPublicKey, keyId: string, bitmap: Bitmap): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writePublicKey(candidateKey);
    _out.writeString(keyId);
    serializeBitmap(_out, bitmap);
  });
}

export function authorizeNewOracle(signature: Signature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeSignature(signature);
  });
}

export function addHeartbeat(bitmap: Bitmap): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    serializeBitmap(_out, bitmap);
  });
}

export function addBroadcastHash(sessionId: number, retryNonce: number, broadcastRound: number, broadcast: Hash): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeI32(sessionId);
    _out.writeI32(retryNonce);
    _out.writeI32(broadcastRound);
    _out.writeHash(broadcast);
  });
}

export function addBroadcastBits(sessionId: number, retryNonce: number, broadcastRound: number, bits: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0c", "hex"));
    _out.writeI32(sessionId);
    _out.writeI32(retryNonce);
    _out.writeI32(broadcastRound);
    _out.writeI32(bits.length);
    _out.writeBytes(bits);
  });
}

export function pokeOngoingUpdate(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("08", "hex"));
  });
}

export function triggerNewCommittee(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0d", "hex"));
  });
}

export function deserializeState(state: StateWithClient): BpOrchestrationContractState;
export function deserializeState(bytes: Buffer): BpOrchestrationContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): BpOrchestrationContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): BpOrchestrationContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new BpOrchestration_SDK_13_4_0(client, address).deserializeBpOrchestrationContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new BpOrchestration_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeBpOrchestrationContractState(input);
  }
}

export type Action =
  | RegisterAction
  | ConfirmBpAction
  | UpdateInfoAction
  | GetProducerInfoAction
  | AddConfirmedBpAction
  | RemoveBpAction
  | AddCandidateKeyAction
  | AuthorizeNewOracleAction
  | AddHeartbeatAction
  | AddBroadcastHashAction
  | AddBroadcastBitsAction
  | PokeOngoingUpdateAction
  | TriggerNewCommitteeAction;

export interface RegisterAction {
  discriminant: "register";
  producer: BlockProducerInformation;
  popSignature: BlsSignature;
}
export interface ConfirmBpAction {
  discriminant: "confirm_bp";
  bpAddress: BlockchainAddress;
}
export interface UpdateInfoAction {
  discriminant: "update_info";
  updatedName: string;
  updatedWebsite: string;
  updatedAddress: string;
  updatedEntityJurisdiction: number;
  updatedServerJurisdiction: number;
}
export interface GetProducerInfoAction {
  discriminant: "get_producer_info";
  identity: BlockchainAddress;
}
export interface AddConfirmedBpAction {
  discriminant: "add_confirmed_bp";
  address: BlockchainAddress;
  producer: BlockProducerInformation;
  popSignature: BlsSignature;
}
export interface RemoveBpAction {
  discriminant: "remove_bp";
}
export interface AddCandidateKeyAction {
  discriminant: "add_candidate_key";
  candidateKey: BlockchainPublicKey;
  keyId: string;
  bitmap: Bitmap;
}
export interface AuthorizeNewOracleAction {
  discriminant: "authorize_new_oracle";
  signature: Signature;
}
export interface AddHeartbeatAction {
  discriminant: "add_heartbeat";
  bitmap: Bitmap;
}
export interface AddBroadcastHashAction {
  discriminant: "add_broadcast_hash";
  sessionId: number;
  retryNonce: number;
  broadcastRound: number;
  broadcast: Hash;
}
export interface AddBroadcastBitsAction {
  discriminant: "add_broadcast_bits";
  sessionId: number;
  retryNonce: number;
  broadcastRound: number;
  bits: Buffer;
}
export interface PokeOngoingUpdateAction {
  discriminant: "poke_ongoing_update";
}
export interface TriggerNewCommitteeAction {
  discriminant: "trigger_new_committee";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new BpOrchestration_SDK_13_4_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeRegisterAction(input);
  } else if (shortname === "01") {
    return contract.deserializeConfirmBpAction(input);
  } else if (shortname === "05") {
    return contract.deserializeUpdateInfoAction(input);
  } else if (shortname === "09") {
    return contract.deserializeGetProducerInfoAction(input);
  } else if (shortname === "0a") {
    return contract.deserializeAddConfirmedBpAction(input);
  } else if (shortname === "0b") {
    return contract.deserializeRemoveBpAction(input);
  } else if (shortname === "02") {
    return contract.deserializeAddCandidateKeyAction(input);
  } else if (shortname === "03") {
    return contract.deserializeAuthorizeNewOracleAction(input);
  } else if (shortname === "04") {
    return contract.deserializeAddHeartbeatAction(input);
  } else if (shortname === "06") {
    return contract.deserializeAddBroadcastHashAction(input);
  } else if (shortname === "0c") {
    return contract.deserializeAddBroadcastBitsAction(input);
  } else if (shortname === "08") {
    return contract.deserializePokeOngoingUpdateAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeTriggerNewCommitteeAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  kycAddresses: BlockchainAddress[];
  initialProducers: BlockProducer[];
  initialThresholdKey: BlockchainPublicKey;
  initialThresholdKeyId: string;
  largeOracleContract: BlockchainAddress;
  systemUpdateContractAddress: BlockchainAddress;
  rewardsContractAddress: BlockchainAddress;
  domainSeparator: Hash;
  broadcastRoundDelay: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new BpOrchestration_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

