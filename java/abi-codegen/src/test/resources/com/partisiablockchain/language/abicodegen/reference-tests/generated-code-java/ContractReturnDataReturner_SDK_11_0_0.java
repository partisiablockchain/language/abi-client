// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractReturnDataReturner_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractReturnDataReturner_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private U8Struct deserializeU8Struct(AbiInput _input) {
    byte u8Value = _input.readU8();
    var u8Vector_vecLength = _input.readI32();
    byte[] u8Vector = _input.readBytes(u8Vector_vecLength);
    var u8Struct_vecLength = _input.readI32();
    List<U8Struct> u8Struct = new ArrayList<>();
    for (int u8Struct_i = 0; u8Struct_i < u8Struct_vecLength; u8Struct_i++) {
      U8Struct u8Struct_elem = deserializeU8Struct(_input);
      u8Struct.add(u8Struct_elem);
    }
    return new U8Struct(u8Value, u8Vector, u8Struct);
  }
  private State deserializeState(AbiInput _input) {
    long someU64 = _input.readU64();
    String someString = _input.readString();
    boolean someBool = _input.readBoolean();
    U8Struct someU8Struct = deserializeU8Struct(_input);
    return new State(someU64, someString, someBool, someU8Struct);
  }
  public State getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeState(input);
  }

  private UpdateStateAction deserializeUpdateStateAction(AbiInput _input) {
    long someU64 = _input.readU64();
    String someString = _input.readString();
    boolean someBool = _input.readBoolean();
    U8Struct someU8Struct = deserializeU8Struct(_input);
    return new UpdateStateAction(someU64, someString, someBool, someU8Struct);
  }

  private ProvideU64Action deserializeProvideU64Action(AbiInput _input) {
    return new ProvideU64Action();
  }

  private ProvideStringAction deserializeProvideStringAction(AbiInput _input) {
    return new ProvideStringAction();
  }

  private ProvideBoolAction deserializeProvideBoolAction(AbiInput _input) {
    return new ProvideBoolAction();
  }

  private ProvideU8StructAction deserializeProvideU8StructAction(AbiInput _input) {
    return new ProvideU8StructAction();
  }

  private ProvideU64WithCourierAction deserializeProvideU64WithCourierAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    return new ProvideU64WithCourierAction(address);
  }

  private ProvideU64WithCourierCallbackCallback deserializeProvideU64WithCourierCallbackCallback(AbiInput _input) {
    return new ProvideU64WithCourierCallbackCallback();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record U8Struct(byte u8Value, byte[] u8Vector, List<U8Struct> u8Struct) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(u8Value);
      _out.writeI32(u8Vector.length);
      _out.writeBytes(u8Vector);
      _out.writeI32(u8Struct.size());
      for (U8Struct u8Struct_vec : u8Struct) {
        u8Struct_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record State(long someU64, String someString, boolean someBool, U8Struct someU8Struct) {
    public static State deserialize(byte[] bytes) {
      return ContractReturnDataReturner_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] updateState(long someU64, String someString, boolean someBool, U8Struct someU8Struct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU64(someU64);
      _out.writeString(someString);
      _out.writeBoolean(someBool);
      someU8Struct.serialize(_out);
    });
  }

  public static byte[] provideU64() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] provideString() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] provideBool() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] provideU8Struct() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] provideU64WithCourier(BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeAddress(address);
    });
  }

  public static byte[] provideU64WithCourierCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("60"));
    });
  }

  public static State deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractReturnDataReturner_SDK_11_0_0(client, address).deserializeState(input);
  }
  
  public static State deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static State deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateStateAction(long someU64, String someString, boolean someBool, U8Struct someU8Struct) implements Action {
  }
  @AbiGenerated
  public record ProvideU64Action() implements Action {
  }
  @AbiGenerated
  public record ProvideStringAction() implements Action {
  }
  @AbiGenerated
  public record ProvideBoolAction() implements Action {
  }
  @AbiGenerated
  public record ProvideU8StructAction() implements Action {
  }
  @AbiGenerated
  public record ProvideU64WithCourierAction(BlockchainAddress address) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataReturner_SDK_11_0_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeUpdateStateAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeProvideU64Action(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeProvideStringAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeProvideBoolAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeProvideU8StructAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeProvideU64WithCourierAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record ProvideU64WithCourierCallbackCallback() implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataReturner_SDK_11_0_0(null, null);
    if (shortname.equals("60")) {
      return contract.deserializeProvideU64WithCourierCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractReturnDataReturner_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
