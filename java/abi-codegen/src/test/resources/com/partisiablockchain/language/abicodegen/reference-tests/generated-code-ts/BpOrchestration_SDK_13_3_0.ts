// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class BpOrchestration_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeBlockchainPublicKey(_input: AbiInput): BlockchainPublicKey {
    const ecPoint: Buffer = _input.readBytes(33);
    return { ecPoint };
  }
  public deserializeBlsPublicKey(_input: AbiInput): BlsPublicKey {
    const publicKeyValue: Buffer = _input.readBytes(96);
    return { publicKeyValue };
  }
  public deserializeBlsSignature(_input: AbiInput): BlsSignature {
    const signatureValue: Buffer = _input.readBytes(48);
    return { signatureValue };
  }
  public deserializeBlockProducer(_input: AbiInput): BlockProducer {
    const name: string = _input.readString();
    const address: string = _input.readString();
    const website: string = _input.readString();
    const identity: BlockchainAddress = _input.readAddress();
    const entityJurisdiction: number = _input.readI32();
    const serverJurisdiction: number = _input.readI32();
    const publicKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const blsPublicKey: BlsPublicKey = this.deserializeBlsPublicKey(_input);
    const numberOfVotes: BN = _input.readI64();
    const status: number = _input.readU8();
    return { name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status };
  }
  public deserializeBlockProducerInformation(_input: AbiInput): BlockProducerInformation {
    const name: string = _input.readString();
    const website: string = _input.readString();
    const address: string = _input.readString();
    const publicKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const blsPublicKey: BlsPublicKey = this.deserializeBlsPublicKey(_input);
    const entityJurisdiction: number = _input.readI32();
    const serverJurisdiction: number = _input.readI32();
    return { name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction };
  }
  public deserializeLargeOracleUpdate(_input: AbiInput): LargeOracleUpdate {
    const producers_vecLength = _input.readI32();
    const producers: BlockProducer[] = [];
    for (let producers_i = 0; producers_i < producers_vecLength; producers_i++) {
      const producers_elem: BlockProducer = this.deserializeBlockProducer(_input);
      producers.push(producers_elem);
    }
    const thresholdKey: ThresholdKey = this.deserializeThresholdKey(_input);
    const candidates_vecLength = _input.readI32();
    const candidates: CandidateKey[] = [];
    for (let candidates_i = 0; candidates_i < candidates_vecLength; candidates_i++) {
      const candidates_elem: CandidateKey = this.deserializeCandidateKey(_input);
      candidates.push(candidates_elem);
    }
    const honestPartyViews_mapLength = _input.readI32();
    const honestPartyViews: Map<BlockchainAddress, Bitmap> = new Map();
    for (let honestPartyViews_i = 0; honestPartyViews_i < honestPartyViews_mapLength; honestPartyViews_i++) {
      const honestPartyViews_key: BlockchainAddress = _input.readAddress();
      const honestPartyViews_value: Bitmap = this.deserializeBitmap(_input);
      honestPartyViews.set(honestPartyViews_key, honestPartyViews_value);
    }
    const retryNonce: number = _input.readI32();
    const voters: Bitmap = this.deserializeBitmap(_input);
    const activeProducers: Bitmap = this.deserializeBitmap(_input);
    const heartbeats_mapLength = _input.readI32();
    const heartbeats: Map<BlockchainAddress, Bitmap> = new Map();
    for (let heartbeats_i = 0; heartbeats_i < heartbeats_mapLength; heartbeats_i++) {
      const heartbeats_key: BlockchainAddress = _input.readAddress();
      const heartbeats_value: Bitmap = this.deserializeBitmap(_input);
      heartbeats.set(heartbeats_key, heartbeats_value);
    }
    const broadcasters: Bitmap = this.deserializeBitmap(_input);
    const broadcastMessages_vecLength = _input.readI32();
    const broadcastMessages: BroadcasterTracker[] = [];
    for (let broadcastMessages_i = 0; broadcastMessages_i < broadcastMessages_vecLength; broadcastMessages_i++) {
      const broadcastMessages_elem: BroadcasterTracker = this.deserializeBroadcasterTracker(_input);
      broadcastMessages.push(broadcastMessages_elem);
    }
    const signatureRandomization: Buffer = _input.readBytes(32);
    return { producers, thresholdKey, candidates, honestPartyViews, retryNonce, voters, activeProducers, heartbeats, broadcasters, broadcastMessages, signatureRandomization };
  }
  public deserializeBroadcasterTracker(_input: AbiInput): BroadcasterTracker {
    const messages_mapLength = _input.readI32();
    const messages: Map<BlockchainAddress, Buffer> = new Map();
    for (let messages_i = 0; messages_i < messages_mapLength; messages_i++) {
      const messages_key: BlockchainAddress = _input.readAddress();
      const messages_value: Buffer = _input.readBytes(32);
      messages.set(messages_key, messages_value);
    }
    const bitmaps_mapLength = _input.readI32();
    const bitmaps: Map<BlockchainAddress, Bitmap> = new Map();
    for (let bitmaps_i = 0; bitmaps_i < bitmaps_mapLength; bitmaps_i++) {
      const bitmaps_key: BlockchainAddress = _input.readAddress();
      const bitmaps_value: Bitmap = this.deserializeBitmap(_input);
      bitmaps.set(bitmaps_key, bitmaps_value);
    }
    const echos: Bitmap = this.deserializeBitmap(_input);
    const startTime: BN = _input.readI64();
    return { messages, bitmaps, echos, startTime };
  }
  public deserializeCommitteeSignature(_input: AbiInput): CommitteeSignature {
    const committeeMembers_vecLength = _input.readI32();
    const committeeMembers: BlockProducer[] = [];
    for (let committeeMembers_i = 0; committeeMembers_i < committeeMembers_vecLength; committeeMembers_i++) {
      const committeeMembers_elem: BlockProducer = this.deserializeBlockProducer(_input);
      committeeMembers.push(committeeMembers_elem);
    }
    const signature: Signature = this.deserializeSignature(_input);
    const thresholdKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    return { committeeMembers, signature, thresholdKey };
  }
  public deserializeThresholdKey(_input: AbiInput): ThresholdKey {
    const key: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const keyId: string = _input.readString();
    return { key, keyId };
  }
  public deserializeCandidateKey(_input: AbiInput): CandidateKey {
    const thresholdKey: ThresholdKey = this.deserializeThresholdKey(_input);
    const votes: number = _input.readI32();
    return { thresholdKey, votes };
  }
  public deserializeBitmap(_input: AbiInput): Bitmap {
    const bits_vecLength = _input.readI32();
    const bits: Buffer = _input.readBytes(bits_vecLength);
    return { bits };
  }
  public deserializeBpOrchestrationContractState(_input: AbiInput): BpOrchestrationContractState {
    const thresholdKey: ThresholdKey = this.deserializeThresholdKey(_input);
    const kycAddresses_vecLength = _input.readI32();
    const kycAddresses: BlockchainAddress[] = [];
    for (let kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      const kycAddresses_elem: BlockchainAddress = _input.readAddress();
      kycAddresses.push(kycAddresses_elem);
    }
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    const oracleUpdate: LargeOracleUpdate = this.deserializeLargeOracleUpdate(_input);
    const blockProducers_mapLength = _input.readI32();
    const blockProducers: Map<BlockchainAddress, BlockProducer> = new Map();
    for (let blockProducers_i = 0; blockProducers_i < blockProducers_mapLength; blockProducers_i++) {
      const blockProducers_key: BlockchainAddress = _input.readAddress();
      const blockProducers_value: BlockProducer = this.deserializeBlockProducer(_input);
      blockProducers.set(blockProducers_key, blockProducers_value);
    }
    const confirmedSinceLastCommittee: BN = _input.readI64();
    const committee_vecLength = _input.readI32();
    const committee: BlockProducer[] = [];
    for (let committee_i = 0; committee_i < committee_vecLength; committee_i++) {
      const committee_elem: BlockProducer = this.deserializeBlockProducer(_input);
      committee.push(committee_elem);
    }
    const largeOracleContract: BlockchainAddress = _input.readAddress();
    const systemUpdateContractAddress: BlockchainAddress = _input.readAddress();
    const rewardsContractAddress: BlockchainAddress = _input.readAddress();
    const committeeLog_mapLength = _input.readI32();
    const committeeLog: Map<number, CommitteeSignature> = new Map();
    for (let committeeLog_i = 0; committeeLog_i < committeeLog_mapLength; committeeLog_i++) {
      const committeeLog_key: number = _input.readI32();
      const committeeLog_value: CommitteeSignature = this.deserializeCommitteeSignature(_input);
      committeeLog.set(committeeLog_key, committeeLog_value);
    }
    const domainSeparator: Buffer = _input.readBytes(32);
    const broadcastRoundDelay: BN = _input.readI64();
    return { thresholdKey, kycAddresses, sessionId, retryNonce, oracleUpdate, blockProducers, confirmedSinceLastCommittee, committee, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, committeeLog, domainSeparator, broadcastRoundDelay };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU8();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public async getState(): Promise<BpOrchestrationContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeBpOrchestrationContractState(input);
  }

  public deserializeRegisterAction(_input: AbiInput): RegisterAction {
    const producer: BlockProducerInformation = this.deserializeBlockProducerInformation(_input);
    const popSignature: BlsSignature = this.deserializeBlsSignature(_input);
    return { discriminant: "register", producer, popSignature };
  }

  public deserializeConfirmBpAction(_input: AbiInput): ConfirmBpAction {
    const bpAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "confirm_bp", bpAddress };
  }

  public deserializeUpdateInfoAction(_input: AbiInput): UpdateInfoAction {
    const updatedName: string = _input.readString();
    const updatedWebsite: string = _input.readString();
    const updatedAddress: string = _input.readString();
    const updatedEntityJurisdiction: number = _input.readI32();
    const updatedServerJurisdiction: number = _input.readI32();
    return { discriminant: "update_info", updatedName, updatedWebsite, updatedAddress, updatedEntityJurisdiction, updatedServerJurisdiction };
  }

  public deserializeGetProducerInfoAction(_input: AbiInput): GetProducerInfoAction {
    const identity: BlockchainAddress = _input.readAddress();
    return { discriminant: "get_producer_info", identity };
  }

  public deserializeAddConfirmedBpAction(_input: AbiInput): AddConfirmedBpAction {
    const address: BlockchainAddress = _input.readAddress();
    const producer: BlockProducerInformation = this.deserializeBlockProducerInformation(_input);
    const popSignature: BlsSignature = this.deserializeBlsSignature(_input);
    return { discriminant: "add_confirmed_bp", address, producer, popSignature };
  }

  public deserializeRemoveBpAction(_input: AbiInput): RemoveBpAction {
    return { discriminant: "remove_bp",  };
  }

  public deserializeAddCandidateKeyAction(_input: AbiInput): AddCandidateKeyAction {
    const candidateKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const keyId: string = _input.readString();
    const bitmap: Bitmap = this.deserializeBitmap(_input);
    return { discriminant: "add_candidate_key", candidateKey, keyId, bitmap };
  }

  public deserializeAuthorizeNewOracleAction(_input: AbiInput): AuthorizeNewOracleAction {
    const signature: Signature = this.deserializeSignature(_input);
    return { discriminant: "authorize_new_oracle", signature };
  }

  public deserializeAddHeartbeatAction(_input: AbiInput): AddHeartbeatAction {
    const bitmap: Bitmap = this.deserializeBitmap(_input);
    return { discriminant: "add_heartbeat", bitmap };
  }

  public deserializeAddBroadcastHashAction(_input: AbiInput): AddBroadcastHashAction {
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    const broadcastRound: number = _input.readI32();
    const broadcast: Buffer = _input.readBytes(32);
    return { discriminant: "add_broadcast_hash", sessionId, retryNonce, broadcastRound, broadcast };
  }

  public deserializeAddBroadcastBitsAction(_input: AbiInput): AddBroadcastBitsAction {
    const sessionId: number = _input.readI32();
    const retryNonce: number = _input.readI32();
    const broadcastRound: number = _input.readI32();
    const bits_vecLength = _input.readI32();
    const bits: Buffer = _input.readBytes(bits_vecLength);
    return { discriminant: "add_broadcast_bits", sessionId, retryNonce, broadcastRound, bits };
  }

  public deserializePokeOngoingUpdateAction(_input: AbiInput): PokeOngoingUpdateAction {
    return { discriminant: "poke_ongoing_update",  };
  }

  public deserializeTriggerNewCommitteeAction(_input: AbiInput): TriggerNewCommitteeAction {
    return { discriminant: "trigger_new_committee",  };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const kycAddresses_vecLength = _input.readI32();
    const kycAddresses: BlockchainAddress[] = [];
    for (let kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      const kycAddresses_elem: BlockchainAddress = _input.readAddress();
      kycAddresses.push(kycAddresses_elem);
    }
    const initialProducers_vecLength = _input.readI32();
    const initialProducers: BlockProducer[] = [];
    for (let initialProducers_i = 0; initialProducers_i < initialProducers_vecLength; initialProducers_i++) {
      const initialProducers_elem: BlockProducer = this.deserializeBlockProducer(_input);
      initialProducers.push(initialProducers_elem);
    }
    const initialThresholdKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const initialThresholdKeyId: string = _input.readString();
    const largeOracleContract: BlockchainAddress = _input.readAddress();
    const systemUpdateContractAddress: BlockchainAddress = _input.readAddress();
    const rewardsContractAddress: BlockchainAddress = _input.readAddress();
    const domainSeparator: Buffer = _input.readBytes(32);
    const broadcastRoundDelay: BN = _input.readI64();
    return { discriminant: "init", kycAddresses, initialProducers, initialThresholdKey, initialThresholdKeyId, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, domainSeparator, broadcastRoundDelay };
  }

}
export interface BlockchainPublicKey {
  ecPoint: Buffer;
}
function serializeBlockchainPublicKey(_out: AbiOutput, _value: BlockchainPublicKey): void {
  const { ecPoint } = _value;
  if (ecPoint.length != 33) {
    throw new Error("Length of ecPoint does not match expected 33");
  }
  _out.writeBytes(ecPoint);
}

export interface BlsPublicKey {
  publicKeyValue: Buffer;
}
function serializeBlsPublicKey(_out: AbiOutput, _value: BlsPublicKey): void {
  const { publicKeyValue } = _value;
  if (publicKeyValue.length != 96) {
    throw new Error("Length of publicKeyValue does not match expected 96");
  }
  _out.writeBytes(publicKeyValue);
}

export interface BlsSignature {
  signatureValue: Buffer;
}
function serializeBlsSignature(_out: AbiOutput, _value: BlsSignature): void {
  const { signatureValue } = _value;
  if (signatureValue.length != 48) {
    throw new Error("Length of signatureValue does not match expected 48");
  }
  _out.writeBytes(signatureValue);
}

export interface BlockProducer {
  name: string;
  address: string;
  website: string;
  identity: BlockchainAddress;
  entityJurisdiction: number;
  serverJurisdiction: number;
  publicKey: BlockchainPublicKey;
  blsPublicKey: BlsPublicKey;
  numberOfVotes: BN;
  status: number;
}
function serializeBlockProducer(_out: AbiOutput, _value: BlockProducer): void {
  const { name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status } = _value;
  _out.writeString(name);
  _out.writeString(address);
  _out.writeString(website);
  _out.writeAddress(identity);
  _out.writeI32(entityJurisdiction);
  _out.writeI32(serverJurisdiction);
  serializeBlockchainPublicKey(_out, publicKey);
  serializeBlsPublicKey(_out, blsPublicKey);
  _out.writeI64(numberOfVotes);
  _out.writeU8(status);
}

export interface BlockProducerInformation {
  name: string;
  website: string;
  address: string;
  publicKey: BlockchainPublicKey;
  blsPublicKey: BlsPublicKey;
  entityJurisdiction: number;
  serverJurisdiction: number;
}
function serializeBlockProducerInformation(_out: AbiOutput, _value: BlockProducerInformation): void {
  const { name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction } = _value;
  _out.writeString(name);
  _out.writeString(website);
  _out.writeString(address);
  serializeBlockchainPublicKey(_out, publicKey);
  serializeBlsPublicKey(_out, blsPublicKey);
  _out.writeI32(entityJurisdiction);
  _out.writeI32(serverJurisdiction);
}

export interface LargeOracleUpdate {
  producers: BlockProducer[];
  thresholdKey: ThresholdKey;
  candidates: CandidateKey[];
  honestPartyViews: Map<BlockchainAddress, Bitmap>;
  retryNonce: number;
  voters: Bitmap;
  activeProducers: Bitmap;
  heartbeats: Map<BlockchainAddress, Bitmap>;
  broadcasters: Bitmap;
  broadcastMessages: BroadcasterTracker[];
  signatureRandomization: Buffer;
}

export interface BroadcasterTracker {
  messages: Map<BlockchainAddress, Buffer>;
  bitmaps: Map<BlockchainAddress, Bitmap>;
  echos: Bitmap;
  startTime: BN;
}

export interface CommitteeSignature {
  committeeMembers: BlockProducer[];
  signature: Signature;
  thresholdKey: BlockchainPublicKey;
}

export interface ThresholdKey {
  key: BlockchainPublicKey;
  keyId: string;
}

export interface CandidateKey {
  thresholdKey: ThresholdKey;
  votes: number;
}

export interface Bitmap {
  bits: Buffer;
}
function serializeBitmap(_out: AbiOutput, _value: Bitmap): void {
  const { bits } = _value;
  _out.writeI32(bits.length);
  _out.writeBytes(bits);
}

export interface BpOrchestrationContractState {
  thresholdKey: ThresholdKey;
  kycAddresses: BlockchainAddress[];
  sessionId: number;
  retryNonce: number;
  oracleUpdate: LargeOracleUpdate;
  blockProducers: Map<BlockchainAddress, BlockProducer>;
  confirmedSinceLastCommittee: BN;
  committee: BlockProducer[];
  largeOracleContract: BlockchainAddress;
  systemUpdateContractAddress: BlockchainAddress;
  rewardsContractAddress: BlockchainAddress;
  committeeLog: Map<number, CommitteeSignature>;
  domainSeparator: Buffer;
  broadcastRoundDelay: BN;
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}
function serializeSignature(_out: AbiOutput, _value: Signature): void {
  const { recoveryId, valueR, valueS } = _value;
  _out.writeU8(recoveryId);
  if (valueR.length != 32) {
    throw new Error("Length of valueR does not match expected 32");
  }
  _out.writeBytes(valueR);
  if (valueS.length != 32) {
    throw new Error("Length of valueS does not match expected 32");
  }
  _out.writeBytes(valueS);
}

export function init(kycAddresses: BlockchainAddress[], initialProducers: BlockProducer[], initialThresholdKey: BlockchainPublicKey, initialThresholdKeyId: string, largeOracleContract: BlockchainAddress, systemUpdateContractAddress: BlockchainAddress, rewardsContractAddress: BlockchainAddress, domainSeparator: Buffer, broadcastRoundDelay: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(kycAddresses.length);
    for (const kycAddresses_vec of kycAddresses) {
      _out.writeAddress(kycAddresses_vec);
    }
    _out.writeI32(initialProducers.length);
    for (const initialProducers_vec of initialProducers) {
      serializeBlockProducer(_out, initialProducers_vec);
    }
    serializeBlockchainPublicKey(_out, initialThresholdKey);
    _out.writeString(initialThresholdKeyId);
    _out.writeAddress(largeOracleContract);
    _out.writeAddress(systemUpdateContractAddress);
    _out.writeAddress(rewardsContractAddress);
    if (domainSeparator.length != 32) {
      throw new Error("Length of domainSeparator does not match expected 32");
    }
    _out.writeBytes(domainSeparator);
    _out.writeI64(broadcastRoundDelay);
  });
}

export function register(producer: BlockProducerInformation, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    serializeBlockProducerInformation(_out, producer);
    serializeBlsSignature(_out, popSignature);
  });
}

export function confirmBp(bpAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(bpAddress);
  });
}

export function updateInfo(updatedName: string, updatedWebsite: string, updatedAddress: string, updatedEntityJurisdiction: number, updatedServerJurisdiction: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeString(updatedName);
    _out.writeString(updatedWebsite);
    _out.writeString(updatedAddress);
    _out.writeI32(updatedEntityJurisdiction);
    _out.writeI32(updatedServerJurisdiction);
  });
}

export function getProducerInfo(identity: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("09", "hex"));
    _out.writeAddress(identity);
  });
}

export function addConfirmedBp(address: BlockchainAddress, producer: BlockProducerInformation, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0a", "hex"));
    _out.writeAddress(address);
    serializeBlockProducerInformation(_out, producer);
    serializeBlsSignature(_out, popSignature);
  });
}

export function removeBp(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0b", "hex"));
  });
}

export function addCandidateKey(candidateKey: BlockchainPublicKey, keyId: string, bitmap: Bitmap): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    serializeBlockchainPublicKey(_out, candidateKey);
    _out.writeString(keyId);
    serializeBitmap(_out, bitmap);
  });
}

export function authorizeNewOracle(signature: Signature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    serializeSignature(_out, signature);
  });
}

export function addHeartbeat(bitmap: Bitmap): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    serializeBitmap(_out, bitmap);
  });
}

export function addBroadcastHash(sessionId: number, retryNonce: number, broadcastRound: number, broadcast: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeI32(sessionId);
    _out.writeI32(retryNonce);
    _out.writeI32(broadcastRound);
    if (broadcast.length != 32) {
      throw new Error("Length of broadcast does not match expected 32");
    }
    _out.writeBytes(broadcast);
  });
}

export function addBroadcastBits(sessionId: number, retryNonce: number, broadcastRound: number, bits: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0c", "hex"));
    _out.writeI32(sessionId);
    _out.writeI32(retryNonce);
    _out.writeI32(broadcastRound);
    _out.writeI32(bits.length);
    _out.writeBytes(bits);
  });
}

export function pokeOngoingUpdate(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("08", "hex"));
  });
}

export function triggerNewCommittee(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0d", "hex"));
  });
}

export function deserializeState(state: StateWithClient): BpOrchestrationContractState;
export function deserializeState(bytes: Buffer): BpOrchestrationContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): BpOrchestrationContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): BpOrchestrationContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new BpOrchestration_SDK_13_3_0(client, address).deserializeBpOrchestrationContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new BpOrchestration_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeBpOrchestrationContractState(input);
  }
}

export type Action =
  | RegisterAction
  | ConfirmBpAction
  | UpdateInfoAction
  | GetProducerInfoAction
  | AddConfirmedBpAction
  | RemoveBpAction
  | AddCandidateKeyAction
  | AuthorizeNewOracleAction
  | AddHeartbeatAction
  | AddBroadcastHashAction
  | AddBroadcastBitsAction
  | PokeOngoingUpdateAction
  | TriggerNewCommitteeAction;

export interface RegisterAction {
  discriminant: "register";
  producer: BlockProducerInformation;
  popSignature: BlsSignature;
}
export interface ConfirmBpAction {
  discriminant: "confirm_bp";
  bpAddress: BlockchainAddress;
}
export interface UpdateInfoAction {
  discriminant: "update_info";
  updatedName: string;
  updatedWebsite: string;
  updatedAddress: string;
  updatedEntityJurisdiction: number;
  updatedServerJurisdiction: number;
}
export interface GetProducerInfoAction {
  discriminant: "get_producer_info";
  identity: BlockchainAddress;
}
export interface AddConfirmedBpAction {
  discriminant: "add_confirmed_bp";
  address: BlockchainAddress;
  producer: BlockProducerInformation;
  popSignature: BlsSignature;
}
export interface RemoveBpAction {
  discriminant: "remove_bp";
}
export interface AddCandidateKeyAction {
  discriminant: "add_candidate_key";
  candidateKey: BlockchainPublicKey;
  keyId: string;
  bitmap: Bitmap;
}
export interface AuthorizeNewOracleAction {
  discriminant: "authorize_new_oracle";
  signature: Signature;
}
export interface AddHeartbeatAction {
  discriminant: "add_heartbeat";
  bitmap: Bitmap;
}
export interface AddBroadcastHashAction {
  discriminant: "add_broadcast_hash";
  sessionId: number;
  retryNonce: number;
  broadcastRound: number;
  broadcast: Buffer;
}
export interface AddBroadcastBitsAction {
  discriminant: "add_broadcast_bits";
  sessionId: number;
  retryNonce: number;
  broadcastRound: number;
  bits: Buffer;
}
export interface PokeOngoingUpdateAction {
  discriminant: "poke_ongoing_update";
}
export interface TriggerNewCommitteeAction {
  discriminant: "trigger_new_committee";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new BpOrchestration_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeRegisterAction(input);
  } else if (shortname === "01") {
    return contract.deserializeConfirmBpAction(input);
  } else if (shortname === "05") {
    return contract.deserializeUpdateInfoAction(input);
  } else if (shortname === "09") {
    return contract.deserializeGetProducerInfoAction(input);
  } else if (shortname === "0a") {
    return contract.deserializeAddConfirmedBpAction(input);
  } else if (shortname === "0b") {
    return contract.deserializeRemoveBpAction(input);
  } else if (shortname === "02") {
    return contract.deserializeAddCandidateKeyAction(input);
  } else if (shortname === "03") {
    return contract.deserializeAuthorizeNewOracleAction(input);
  } else if (shortname === "04") {
    return contract.deserializeAddHeartbeatAction(input);
  } else if (shortname === "06") {
    return contract.deserializeAddBroadcastHashAction(input);
  } else if (shortname === "0c") {
    return contract.deserializeAddBroadcastBitsAction(input);
  } else if (shortname === "08") {
    return contract.deserializePokeOngoingUpdateAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeTriggerNewCommitteeAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  kycAddresses: BlockchainAddress[];
  initialProducers: BlockProducer[];
  initialThresholdKey: BlockchainPublicKey;
  initialThresholdKeyId: string;
  largeOracleContract: BlockchainAddress;
  systemUpdateContractAddress: BlockchainAddress;
  rewardsContractAddress: BlockchainAddress;
  domainSeparator: Buffer;
  broadcastRoundDelay: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new BpOrchestration_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

