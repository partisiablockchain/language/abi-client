// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractNestedMap_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractNestedMap_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    var myComplexMap_mapLength = _input.readI32();
    Map<Integer, Map<Long, Long>> myComplexMap = new HashMap<>();
    for (int myComplexMap_i = 0; myComplexMap_i < myComplexMap_mapLength; myComplexMap_i++) {
      int myComplexMap_key = _input.readU32();
      var myComplexMap_value_mapLength = _input.readI32();
      Map<Long, Long> myComplexMap_value = new HashMap<>();
      for (int myComplexMap_value_i = 0; myComplexMap_value_i < myComplexMap_value_mapLength; myComplexMap_value_i++) {
        long myComplexMap_value_key = _input.readU64();
        long myComplexMap_value_value = _input.readU64();
        myComplexMap_value.put(myComplexMap_value_key, myComplexMap_value_value);
      }
      myComplexMap.put(myComplexMap_key, myComplexMap_value);
    }
    return new ExampleContractState(myComplexMap);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private InsertInMyNestedMapAction deserializeInsertInMyNestedMapAction(AbiInput _input) {
    int key1 = _input.readU32();
    long key2 = _input.readU64();
    long value = _input.readU64();
    return new InsertInMyNestedMapAction(key1, key2, value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record ExampleContractState(Map<Integer, Map<Long, Long>> myComplexMap) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractNestedMap_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] insertInMyNestedMap(int key1, long key2, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ccfe82ed09"));
      _out.writeU32(key1);
      _out.writeU64(key2);
      _out.writeU64(value);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractNestedMap_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record InsertInMyNestedMapAction(int key1, long key2, long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNestedMap_SDK_10_1_0(null, null);
    if (shortname.equals("ccfe82ed09")) {
      return contract.deserializeInsertInMyNestedMapAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNestedMap_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
