// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class FeeDistribution_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public FeeDistribution_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Signed256 deserializeSigned256(AbiInput _input) {
    byte[] value = _input.readBytes(32);
    boolean sign = _input.readBoolean();
    return new Signed256(value, sign);
  }
  private ConvertedCoin deserializeConvertedCoin(AbiInput _input) {
    byte[] convertedCoins = _input.readBytes(32);
    byte[] convertedGasSum = _input.readBytes(32);
    return new ConvertedCoin(convertedCoins, convertedGasSum);
  }
  private EpochMetrics deserializeEpochMetrics(AbiInput _input) {
    var signatureFrequencies_vecLength = _input.readI32();
    List<Integer> signatureFrequencies = new ArrayList<>();
    for (int signatureFrequencies_i = 0; signatureFrequencies_i < signatureFrequencies_vecLength; signatureFrequencies_i++) {
      int signatureFrequencies_elem = _input.readI32();
      signatureFrequencies.add(signatureFrequencies_elem);
    }
    return new EpochMetrics(signatureFrequencies);
  }
  private ShardInformation deserializeShardInformation(AbiInput _input) {
    var notifyingBlockProducers_vecLength = _input.readI32();
    List<BlockchainAddress> notifyingBlockProducers = new ArrayList<>();
    for (int notifyingBlockProducers_i = 0; notifyingBlockProducers_i < notifyingBlockProducers_vecLength; notifyingBlockProducers_i++) {
      BlockchainAddress notifyingBlockProducers_elem = _input.readAddress();
      notifyingBlockProducers.add(notifyingBlockProducers_elem);
    }
    var metrics_mapLength = _input.readI32();
    Map<BlockchainAddress, EpochMetrics> metrics = new HashMap<>();
    for (int metrics_i = 0; metrics_i < metrics_mapLength; metrics_i++) {
      BlockchainAddress metrics_key = _input.readAddress();
      EpochMetrics metrics_value = deserializeEpochMetrics(_input);
      metrics.put(metrics_key, metrics_value);
    }
    return new ShardInformation(notifyingBlockProducers, metrics);
  }
  private EpochInformation deserializeEpochInformation(AbiInput _input) {
    var shards_mapLength = _input.readI32();
    Map<String, ShardInformation> shards = new HashMap<>();
    for (int shards_i = 0; shards_i < shards_mapLength; shards_i++) {
      String shards_key = null;
      var shards_key_isSome = _input.readBoolean();
      if (shards_key_isSome) {
        String shards_key_option = _input.readString();
        shards_key = shards_key_option;
      }
      ShardInformation shards_value = deserializeShardInformation(_input);
      shards.put(shards_key, shards_value);
    }
    return new EpochInformation(shards);
  }
  private FeeDistributionContractState deserializeFeeDistributionContractState(AbiInput _input) {
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    var blockProducers_vecLength = _input.readI32();
    List<BlockchainAddress> blockProducers = new ArrayList<>();
    for (int blockProducers_i = 0; blockProducers_i < blockProducers_vecLength; blockProducers_i++) {
      BlockchainAddress blockProducers_elem = _input.readAddress();
      blockProducers.add(blockProducers_elem);
    }
    var externalCoins_vecLength = _input.readI32();
    List<ConvertedCoin> externalCoins = new ArrayList<>();
    for (int externalCoins_i = 0; externalCoins_i < externalCoins_vecLength; externalCoins_i++) {
      ConvertedCoin externalCoins_elem = deserializeConvertedCoin(_input);
      externalCoins.add(externalCoins_elem);
    }
    var feesToDistribute_mapLength = _input.readI32();
    Map<BlockchainAddress, byte[]> feesToDistribute = new HashMap<>();
    for (int feesToDistribute_i = 0; feesToDistribute_i < feesToDistribute_mapLength; feesToDistribute_i++) {
      BlockchainAddress feesToDistribute_key = _input.readAddress();
      byte[] feesToDistribute_value = _input.readBytes(32);
      feesToDistribute.put(feesToDistribute_key, feesToDistribute_value);
    }
    var epochs_mapLength = _input.readI32();
    Map<Long, EpochInformation> epochs = new HashMap<>();
    for (int epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      long epochs_key = _input.readI64();
      EpochInformation epochs_value = deserializeEpochInformation(_input);
      epochs.put(epochs_key, epochs_value);
    }
    long lastEpochCollected = _input.readI64();
    var shardIds_vecLength = _input.readI32();
    List<String> shardIds = new ArrayList<>();
    for (int shardIds_i = 0; shardIds_i < shardIds_vecLength; shardIds_i++) {
      String shardIds_elem = null;
      var shardIds_elem_isSome = _input.readBoolean();
      if (shardIds_elem_isSome) {
        String shardIds_elem_option = _input.readString();
        shardIds_elem = shardIds_elem_option;
      }
      shardIds.add(shardIds_elem);
    }
    Signed256 gasRewardPool = deserializeSigned256(_input);
    return new FeeDistributionContractState(bpOrchestrationContract, blockProducers, externalCoins, feesToDistribute, epochs, lastEpochCollected, shardIds, gasRewardPool);
  }
  public FeeDistributionContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeFeeDistributionContractState(input);
  }

  private NotifyWithMetricsAction deserializeNotifyWithMetricsAction(AbiInput _input) {
    String shard = null;
    var shard_isSome = _input.readBoolean();
    if (shard_isSome) {
      String shard_option = _input.readString();
      shard = shard_option;
    }
    long endedEpoch = _input.readI64();
    var metrics_vecLength = _input.readI32();
    List<Short> metrics = new ArrayList<>();
    for (int metrics_i = 0; metrics_i < metrics_vecLength; metrics_i++) {
      short metrics_elem = _input.readU16();
      metrics.add(metrics_elem);
    }
    return new NotifyWithMetricsAction(shard, endedEpoch, metrics);
  }

  private UpdateCommitteeAction deserializeUpdateCommitteeAction(AbiInput _input) {
    var newBlockProducers_vecLength = _input.readI32();
    List<BlockchainAddress> newBlockProducers = new ArrayList<>();
    for (int newBlockProducers_i = 0; newBlockProducers_i < newBlockProducers_vecLength; newBlockProducers_i++) {
      BlockchainAddress newBlockProducers_elem = _input.readAddress();
      newBlockProducers.add(newBlockProducers_elem);
    }
    return new UpdateCommitteeAction(newBlockProducers);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    var shards_vecLength = _input.readI32();
    List<String> shards = new ArrayList<>();
    for (int shards_i = 0; shards_i < shards_vecLength; shards_i++) {
      String shards_elem = null;
      var shards_elem_isSome = _input.readBoolean();
      if (shards_elem_isSome) {
        String shards_elem_option = _input.readString();
        shards_elem = shards_elem_option;
      }
      shards.add(shards_elem);
    }
    return new InitInit(bpOrchestrationContract, shards);
  }


  @AbiGenerated
  public record Signed256(byte[] value, boolean sign) {
  }

  @AbiGenerated
  public record ConvertedCoin(byte[] convertedCoins, byte[] convertedGasSum) {
  }

  @AbiGenerated
  public record EpochMetrics(List<Integer> signatureFrequencies) {
  }

  @AbiGenerated
  public record ShardInformation(List<BlockchainAddress> notifyingBlockProducers, Map<BlockchainAddress, EpochMetrics> metrics) {
  }

  @AbiGenerated
  public record EpochInformation(Map<String, ShardInformation> shards) {
  }

  @AbiGenerated
  public record FeeDistributionContractState(BlockchainAddress bpOrchestrationContract, List<BlockchainAddress> blockProducers, List<ConvertedCoin> externalCoins, Map<BlockchainAddress, byte[]> feesToDistribute, Map<Long, EpochInformation> epochs, long lastEpochCollected, List<String> shardIds, Signed256 gasRewardPool) {
    public static FeeDistributionContractState deserialize(byte[] bytes) {
      return FeeDistribution_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] init(BlockchainAddress bpOrchestrationContract, List<String> shards) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(bpOrchestrationContract);
      _out.writeI32(shards.size());
      for (String shards_vec : shards) {
        _out.writeBoolean(shards_vec != null);
        if (shards_vec != null) {
          _out.writeString(shards_vec);
        }
      }
    });
  }

  public static byte[] notifyWithMetrics(String shard, long endedEpoch, List<Short> metrics) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeBoolean(shard != null);
      if (shard != null) {
        _out.writeString(shard);
      }
      _out.writeI64(endedEpoch);
      _out.writeI32(metrics.size());
      for (short metrics_vec : metrics) {
        _out.writeU16(metrics_vec);
      }
    });
  }

  public static byte[] updateCommittee(List<BlockchainAddress> newBlockProducers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeI32(newBlockProducers.size());
      for (BlockchainAddress newBlockProducers_vec : newBlockProducers) {
        _out.writeAddress(newBlockProducers_vec);
      }
    });
  }

  public static FeeDistributionContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new FeeDistribution_SDK_13_3_0(client, address).deserializeFeeDistributionContractState(input);
  }
  
  public static FeeDistributionContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static FeeDistributionContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record NotifyWithMetricsAction(String shard, long endedEpoch, List<Short> metrics) implements Action {
  }
  @AbiGenerated
  public record UpdateCommitteeAction(List<BlockchainAddress> newBlockProducers) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new FeeDistribution_SDK_13_3_0(null, null);
    if (shortname.equals("04")) {
      return contract.deserializeNotifyWithMetricsAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeUpdateCommitteeAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(BlockchainAddress bpOrchestrationContract, List<String> shards) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new FeeDistribution_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
