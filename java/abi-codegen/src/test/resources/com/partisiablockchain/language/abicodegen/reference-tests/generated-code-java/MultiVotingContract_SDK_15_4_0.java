// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class MultiVotingContract_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public MultiVotingContract_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MultiVotingState deserializeMultiVotingState(AbiInput _input) {
    BlockchainAddress owner = _input.readAddress();
    var eligibleVoters_vecLength = _input.readI32();
    List<BlockchainAddress> eligibleVoters = new ArrayList<>();
    for (int eligibleVoters_i = 0; eligibleVoters_i < eligibleVoters_vecLength; eligibleVoters_i++) {
      BlockchainAddress eligibleVoters_elem = _input.readAddress();
      eligibleVoters.add(eligibleVoters_elem);
    }
    var votingContracts_mapLength = _input.readI32();
    Map<Long, BlockchainAddress> votingContracts = new HashMap<>();
    for (int votingContracts_i = 0; votingContracts_i < votingContracts_mapLength; votingContracts_i++) {
      long votingContracts_key = _input.readU64();
      BlockchainAddress votingContracts_value = null;
      var votingContracts_value_isSome = _input.readBoolean();
      if (votingContracts_value_isSome) {
        BlockchainAddress votingContracts_value_option = _input.readAddress();
        votingContracts_value = votingContracts_value_option;
      }
      votingContracts.put(votingContracts_key, votingContracts_value);
    }
    var votingContractWasm_vecLength = _input.readI32();
    byte[] votingContractWasm = _input.readBytes(votingContractWasm_vecLength);
    var votingContractAbi_vecLength = _input.readI32();
    byte[] votingContractAbi = _input.readBytes(votingContractAbi_vecLength);
    return new MultiVotingState(owner, eligibleVoters, votingContracts, votingContractWasm, votingContractAbi);
  }
  public MultiVotingState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeMultiVotingState(input);
  }

  private AddVoterAction deserializeAddVoterAction(AbiInput _input) {
    BlockchainAddress voter = _input.readAddress();
    return new AddVoterAction(voter);
  }

  private RemoveVoterAction deserializeRemoveVoterAction(AbiInput _input) {
    BlockchainAddress voter = _input.readAddress();
    return new RemoveVoterAction(voter);
  }

  private AddVotingContractAction deserializeAddVotingContractAction(AbiInput _input) {
    long pId = _input.readU64();
    long deadline = _input.readI64();
    return new AddVotingContractAction(pId, deadline);
  }

  private AddVotingContractCallbackCallback deserializeAddVotingContractCallbackCallback(AbiInput _input) {
    long pId = _input.readU64();
    BlockchainAddress votingAddress = _input.readAddress();
    return new AddVotingContractCallbackCallback(pId, votingAddress);
  }

  private VotingContractExistsCallbackCallback deserializeVotingContractExistsCallbackCallback(AbiInput _input) {
    long pId = _input.readU64();
    BlockchainAddress votingAddress = _input.readAddress();
    return new VotingContractExistsCallbackCallback(pId, votingAddress);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    var votingContractWasm_vecLength = _input.readI32();
    byte[] votingContractWasm = _input.readBytes(votingContractWasm_vecLength);
    var votingContractAbi_vecLength = _input.readI32();
    byte[] votingContractAbi = _input.readBytes(votingContractAbi_vecLength);
    return new InitializeInit(votingContractWasm, votingContractAbi);
  }


  @AbiGenerated
  public record MultiVotingState(BlockchainAddress owner, List<BlockchainAddress> eligibleVoters, Map<Long, BlockchainAddress> votingContracts, byte[] votingContractWasm, byte[] votingContractAbi) {
    public static MultiVotingState deserialize(byte[] bytes) {
      return MultiVotingContract_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte[] votingContractWasm, byte[] votingContractAbi) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(votingContractWasm.length);
      _out.writeBytes(votingContractWasm);
      _out.writeI32(votingContractAbi.length);
      _out.writeBytes(votingContractAbi);
    });
  }

  public static byte[] addVoter(BlockchainAddress voter) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("bcdf8463"));
      _out.writeAddress(voter);
    });
  }

  public static byte[] removeVoter(BlockchainAddress voter) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("defab7fd0d"));
      _out.writeAddress(voter);
    });
  }

  public static byte[] addVotingContract(long pId, long deadline) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("d0b5b9a30f"));
      _out.writeU64(pId);
      _out.writeI64(deadline);
    });
  }

  public static byte[] addVotingContractCallback(long pId, BlockchainAddress votingAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU64(pId);
      _out.writeAddress(votingAddress);
    });
  }

  public static byte[] votingContractExistsCallback(long pId, BlockchainAddress votingAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeU64(pId);
      _out.writeAddress(votingAddress);
    });
  }

  public static MultiVotingState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new MultiVotingContract_SDK_15_4_0(client, address).deserializeMultiVotingState(input);
  }
  
  public static MultiVotingState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static MultiVotingState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record AddVoterAction(BlockchainAddress voter) implements Action {
  }
  @AbiGenerated
  public record RemoveVoterAction(BlockchainAddress voter) implements Action {
  }
  @AbiGenerated
  public record AddVotingContractAction(long pId, long deadline) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new MultiVotingContract_SDK_15_4_0(null, null);
    if (shortname.equals("bcdf8463")) {
      return contract.deserializeAddVoterAction(input);
    } else if (shortname.equals("defab7fd0d")) {
      return contract.deserializeRemoveVoterAction(input);
    } else if (shortname.equals("d0b5b9a30f")) {
      return contract.deserializeAddVotingContractAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record AddVotingContractCallbackCallback(long pId, BlockchainAddress votingAddress) implements Callback {
  }
  @AbiGenerated
  public record VotingContractExistsCallbackCallback(long pId, BlockchainAddress votingAddress) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new MultiVotingContract_SDK_15_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeAddVotingContractCallbackCallback(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeVotingContractExistsCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte[] votingContractWasm, byte[] votingContractAbi) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new MultiVotingContract_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
