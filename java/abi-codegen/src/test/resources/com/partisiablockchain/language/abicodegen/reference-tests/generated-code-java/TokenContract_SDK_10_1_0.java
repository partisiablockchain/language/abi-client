// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class TokenContract_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public TokenContract_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Transfer deserializeTransfer(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    long value = _input.readU64();
    return new Transfer(to, value);
  }
  private TokenContractState deserializeTokenContractState(AbiInput _input) {
    String name = _input.readString();
    byte decimals = _input.readU8();
    String symbol = _input.readString();
    BlockchainAddress owner = _input.readAddress();
    long totalSupply = _input.readU64();
    var balances_mapLength = _input.readI32();
    Map<BlockchainAddress, Long> balances = new HashMap<>();
    for (int balances_i = 0; balances_i < balances_mapLength; balances_i++) {
      BlockchainAddress balances_key = _input.readAddress();
      long balances_value = _input.readU64();
      balances.put(balances_key, balances_value);
    }
    var allowed_mapLength = _input.readI32();
    Map<BlockchainAddress, Map<BlockchainAddress, Long>> allowed = new HashMap<>();
    for (int allowed_i = 0; allowed_i < allowed_mapLength; allowed_i++) {
      BlockchainAddress allowed_key = _input.readAddress();
      var allowed_value_mapLength = _input.readI32();
      Map<BlockchainAddress, Long> allowed_value = new HashMap<>();
      for (int allowed_value_i = 0; allowed_value_i < allowed_value_mapLength; allowed_value_i++) {
        BlockchainAddress allowed_value_key = _input.readAddress();
        long allowed_value_value = _input.readU64();
        allowed_value.put(allowed_value_key, allowed_value_value);
      }
      allowed.put(allowed_key, allowed_value);
    }
    return new TokenContractState(name, decimals, symbol, owner, totalSupply, balances, allowed);
  }
  public TokenContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeTokenContractState(input);
  }

  private TransferAction deserializeTransferAction(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    long value = _input.readU64();
    return new TransferAction(to, value);
  }

  private BulkTransferAction deserializeBulkTransferAction(AbiInput _input) {
    var transfers_vecLength = _input.readI32();
    List<Transfer> transfers = new ArrayList<>();
    for (int transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      Transfer transfers_elem = deserializeTransfer(_input);
      transfers.add(transfers_elem);
    }
    return new BulkTransferAction(transfers);
  }

  private TransferFromAction deserializeTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    BlockchainAddress to = _input.readAddress();
    long value = _input.readU64();
    return new TransferFromAction(from, to, value);
  }

  private BulkTransferFromAction deserializeBulkTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    var transfers_vecLength = _input.readI32();
    List<Transfer> transfers = new ArrayList<>();
    for (int transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      Transfer transfers_elem = deserializeTransfer(_input);
      transfers.add(transfers_elem);
    }
    return new BulkTransferFromAction(from, transfers);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    BlockchainAddress spender = _input.readAddress();
    long value = _input.readU64();
    return new ApproveAction(spender, value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    String name = _input.readString();
    String symbol = _input.readString();
    byte decimals = _input.readU8();
    long totalSupply = _input.readU64();
    return new InitializeInit(name, symbol, decimals, totalSupply);
  }


  @AbiGenerated
  public record Transfer(BlockchainAddress to, long value) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(to);
      _out.writeU64(value);
    }
  }

  @AbiGenerated
  public record TokenContractState(String name, byte decimals, String symbol, BlockchainAddress owner, long totalSupply, Map<BlockchainAddress, Long> balances, Map<BlockchainAddress, Map<BlockchainAddress, Long>> allowed) {
    public static TokenContractState deserialize(byte[] bytes) {
      return TokenContract_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(String name, String symbol, byte decimals, long totalSupply) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(name);
      _out.writeString(symbol);
      _out.writeU8(decimals);
      _out.writeU64(totalSupply);
    });
  }

  public static byte[] transfer(BlockchainAddress to, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(to);
      _out.writeU64(value);
    });
  }

  public static byte[] bulkTransfer(List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] transferFrom(BlockchainAddress from, BlockchainAddress to, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(from);
      _out.writeAddress(to);
      _out.writeU64(value);
    });
  }

  public static byte[] bulkTransferFrom(BlockchainAddress from, List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(from);
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] approve(BlockchainAddress spender, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeAddress(spender);
      _out.writeU64(value);
    });
  }

  public static TokenContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new TokenContract_SDK_10_1_0(client, address).deserializeTokenContractState(input);
  }
  
  public static TokenContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static TokenContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record TransferAction(BlockchainAddress to, long value) implements Action {
  }
  @AbiGenerated
  public record BulkTransferAction(List<Transfer> transfers) implements Action {
  }
  @AbiGenerated
  public record TransferFromAction(BlockchainAddress from, BlockchainAddress to, long value) implements Action {
  }
  @AbiGenerated
  public record BulkTransferFromAction(BlockchainAddress from, List<Transfer> transfers) implements Action {
  }
  @AbiGenerated
  public record ApproveAction(BlockchainAddress spender, long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenContract_SDK_10_1_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeTransferAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeBulkTransferAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeTransferFromAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeBulkTransferFromAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeApproveAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(String name, String symbol, byte decimals, long totalSupply) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenContract_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
