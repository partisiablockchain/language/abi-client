// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkLiquiditySwap_SDK_9_1_2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkLiquiditySwap_SDK_9_1_2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private WorklistEntry deserializeWorklistEntry(AbiInput _input) {
    SecretVarId variableId = deserializeSecretVarId(_input);
    BlockchainAddress sender = _input.readAddress();
    return new WorklistEntry(variableId, sender);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress contractOwner = _input.readAddress();
    BlockchainAddress tokenPoolAddress = _input.readAddress();
    BigInteger swapConstant = _input.readUnsignedBigInteger(16);
    boolean isClosed = _input.readBoolean();
    PairwiseTokenBalances balances = deserializePairwiseTokenBalances(_input);
    var worklist_vecLength = _input.readI32();
    List<WorklistEntry> worklist = new ArrayList<>();
    for (int worklist_i = 0; worklist_i < worklist_vecLength; worklist_i++) {
      WorklistEntry worklist_elem = deserializeWorklistEntry(_input);
      worklist.add(worklist_elem);
    }
    var unusedVariables_vecLength = _input.readI32();
    List<SecretVarId> unusedVariables = new ArrayList<>();
    for (int unusedVariables_i = 0; unusedVariables_i < unusedVariables_vecLength; unusedVariables_i++) {
      SecretVarId unusedVariables_elem = deserializeSecretVarId(_input);
      unusedVariables.add(unusedVariables_elem);
    }
    return new ContractState(contractOwner, tokenPoolAddress, swapConstant, isClosed, balances, worklist, unusedVariables);
  }
  private Token deserializeToken(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeTokenTokenA(_input);
    } else if (discriminant == 1) {
      return deserializeTokenTokenB(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private TokenTokenA deserializeTokenTokenA(AbiInput _input) {
    return new TokenTokenA();
  }
  private TokenTokenB deserializeTokenTokenB(AbiInput _input) {
    return new TokenTokenB();
  }
  private Balance deserializeBalance(AbiInput _input) {
    BigInteger poolABalance = _input.readUnsignedBigInteger(16);
    BigInteger poolBBalance = _input.readUnsignedBigInteger(16);
    return new Balance(poolABalance, poolBBalance);
  }
  private PairwiseTokenBalances deserializePairwiseTokenBalances(AbiInput _input) {
    BlockchainAddress tokenA = _input.readAddress();
    BlockchainAddress tokenB = _input.readAddress();
    var balances_mapLength = _input.readI32();
    Map<BlockchainAddress, Balance> balances = new HashMap<>();
    for (int balances_i = 0; balances_i < balances_mapLength; balances_i++) {
      BlockchainAddress balances_key = _input.readAddress();
      Balance balances_value = deserializeBalance(_input);
      balances.put(balances_key, balances_value);
    }
    return new PairwiseTokenBalances(tokenA, tokenB, balances);
  }
  private SecretVarId deserializeSecretVarId(AbiInput _input) {
    int rawId = _input.readU32();
    return new SecretVarId(rawId);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private ProvideLiquidityAction deserializeProvideLiquidityAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger poolSize = _input.readUnsignedBigInteger(16);
    return new ProvideLiquidityAction(tokenAddress, poolSize);
  }

  private DepositAction deserializeDepositAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositAction(tokenAddress, amount);
  }

  private WithdrawAction deserializeWithdrawAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new WithdrawAction(tokenAddress, amount);
  }

  private ClosePoolsAction deserializeClosePoolsAction(AbiInput _input) {
    return new ClosePoolsAction();
  }

  private DepositCallbackCallback deserializeDepositCallbackCallback(AbiInput _input) {
    Token token = deserializeToken(_input);
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositCallbackCallback(token, amount);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress tokenAAddress = _input.readAddress();
    BlockchainAddress tokenBAddress = _input.readAddress();
    return new InitializeInit(tokenAAddress, tokenBAddress);
  }


  @AbiGenerated
  public record WorklistEntry(SecretVarId variableId, BlockchainAddress sender) {
  }

  @AbiGenerated
  public record ContractState(BlockchainAddress contractOwner, BlockchainAddress tokenPoolAddress, BigInteger swapConstant, boolean isClosed, PairwiseTokenBalances balances, List<WorklistEntry> worklist, List<SecretVarId> unusedVariables) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkLiquiditySwap_SDK_9_1_2.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public enum TokenD {
    TOKEN_A(0),
    TOKEN_B(1),
    ;
    private final int value;
    TokenD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Token {
    TokenD discriminant();
    void serialize(AbiOutput out);
  }

  @AbiGenerated
  public record TokenTokenA() implements Token {
    public TokenD discriminant() {
      return TokenD.TOKEN_A;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record TokenTokenB() implements Token {
    public TokenD discriminant() {
      return TokenD.TOKEN_B;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record Balance(BigInteger poolABalance, BigInteger poolBBalance) {
  }

  @AbiGenerated
  public record PairwiseTokenBalances(BlockchainAddress tokenA, BlockchainAddress tokenB, Map<BlockchainAddress, Balance> balances) {
  }

  @AbiGenerated
  public record SecretVarId(int rawId) {
  }

  public static byte[] initialize(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(tokenAAddress);
      _out.writeAddress(tokenBAddress);
    });
  }

  public static byte[] provideLiquidity(BlockchainAddress tokenAddress, BigInteger poolSize) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("10"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(poolSize, 16);
    });
  }

  public static byte[] deposit(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("11"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] withdraw(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("14"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] closePools() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("15"));
    });
  }

  public static SecretInputBuilder<Integer> swap(boolean onlyIfAtFront) {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("13"));
      _out.writeBoolean(onlyIfAtFront);
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static byte[] depositCallback(Token token, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      token.serialize(_out);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkLiquiditySwap_SDK_9_1_2(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ProvideLiquidityAction(BlockchainAddress tokenAddress, BigInteger poolSize) implements Action {
  }
  @AbiGenerated
  public record DepositAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record WithdrawAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ClosePoolsAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new ZkLiquiditySwap_SDK_9_1_2(null, null);
    if (shortname.equals("10")) {
      return contract.deserializeProvideLiquidityAction(input);
    } else if (shortname.equals("11")) {
      return contract.deserializeDepositAction(input);
    } else if (shortname.equals("14")) {
      return contract.deserializeWithdrawAction(input);
    } else if (shortname.equals("15")) {
      return contract.deserializeClosePoolsAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record DepositCallbackCallback(Token token, BigInteger amount) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkLiquiditySwap_SDK_9_1_2(null, null);
    if (shortname.equals("02")) {
      return contract.deserializeDepositCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkLiquiditySwap_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
