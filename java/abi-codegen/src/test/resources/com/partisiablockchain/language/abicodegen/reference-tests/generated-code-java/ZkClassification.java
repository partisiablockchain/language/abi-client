// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkClassification {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkClassification(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress modelOwner = _input.readAddress();
    return new ContractState(modelOwner);
  }
  private Model deserializeModel(AbiInput _input) {
    List<InternalVertex> internals = new ArrayList<>();
    for (int internals_i = 0; internals_i < 7; internals_i++) {
      InternalVertex internals_elem = deserializeInternalVertex(_input);
      internals.add(internals_elem);
    }
    List<LeafVertex> leaves = new ArrayList<>();
    for (int leaves_i = 0; leaves_i < 8; leaves_i++) {
      LeafVertex leaves_elem = deserializeLeafVertex(_input);
      leaves.add(leaves_elem);
    }
    return new Model(internals, leaves);
  }
  private InternalVertex deserializeInternalVertex(AbiInput _input) {
    byte feature = _input.readU8();
    short threshold = _input.readI16();
    return new InternalVertex(feature, threshold);
  }
  private LeafVertex deserializeLeafVertex(AbiInput _input) {
    boolean classification = _input.readBoolean();
    return new LeafVertex(classification);
  }
  private SecretVarId deserializeSecretVarId(AbiInput _input) {
    int rawId = _input.readU32();
    return new SecretVarId(rawId);
  }
  private Sample deserializeSample(AbiInput _input) {
    List<Short> values = new ArrayList<>();
    for (int values_i = 0; values_i < 10; values_i++) {
      short values_elem = _input.readI16();
      values.add(values_elem);
    }
    return new Sample(values);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record ContractState(BlockchainAddress modelOwner) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkClassification.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Model(List<InternalVertex> internals, List<LeafVertex> leaves) {
    public void serialize(AbiOutput _out) {
      if (internals.size() != 7) {
        throw new RuntimeException("Length of internals does not match expected 7");
      }
      for (InternalVertex internals_arr : internals) {
        internals_arr.serialize(_out);
      }
      if (leaves.size() != 8) {
        throw new RuntimeException("Length of leaves does not match expected 8");
      }
      for (LeafVertex leaves_arr : leaves) {
        leaves_arr.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record InternalVertex(byte feature, short threshold) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(feature);
      _out.writeI16(threshold);
    }
  }

  @AbiGenerated
  public record LeafVertex(boolean classification) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(classification);
    }
  }

  @AbiGenerated
  public record SecretVarId(int rawId) {
    public void serialize(AbiOutput _out) {
      _out.writeU32(rawId);
    }
  }

  @AbiGenerated
  public record Sample(List<Short> values) {
    public void serialize(AbiOutput _out) {
      if (values.size() != 10) {
        throw new RuntimeException("Length of values does not match expected 10");
      }
      for (short values_arr : values) {
        _out.writeI16(values_arr);
      }
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static SecretInputBuilder<Model> addModel(List<Short> scalingConversion) {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
      _out.writeI32(scalingConversion.size());
      for (short scalingConversion_vec : scalingConversion) {
        _out.writeU16(scalingConversion_vec);
      }
    });
    Function<Model, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Sample> addInputSample(SecretVarId modelId, BlockchainAddress resultReceiver) {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("41"));
      modelId.serialize(_out);
      _out.writeAddress(resultReceiver);
    });
    Function<Sample, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkClassification(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkClassification(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
