// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkRandomness_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkRandomness_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private RandomnessContractState deserializeRandomnessContractState(AbiInput _input) {
    byte requiredNoOfInputs = _input.readU8();
    var generatedRandoms_vecLength = _input.readI32();
    List<BigInteger> generatedRandoms = new ArrayList<>();
    for (int generatedRandoms_i = 0; generatedRandoms_i < generatedRandoms_vecLength; generatedRandoms_i++) {
      BigInteger generatedRandoms_elem = _input.readSignedBigInteger(16);
      generatedRandoms.add(generatedRandoms_elem);
    }
    return new RandomnessContractState(requiredNoOfInputs, generatedRandoms);
  }
  public RandomnessContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeRandomnessContractState(input);
  }

  private GenerateRandomBytesAction deserializeGenerateRandomBytesAction(AbiInput _input) {
    return new GenerateRandomBytesAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte requiredNoOfInputs = _input.readU8();
    return new InitializeInit(requiredNoOfInputs);
  }


  @AbiGenerated
  public record RandomnessContractState(byte requiredNoOfInputs, List<BigInteger> generatedRandoms) {
    public static RandomnessContractState deserialize(byte[] bytes) {
      return ZkRandomness_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte requiredNoOfInputs) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(requiredNoOfInputs);
    });
  }

  public static byte[] generateRandomBytes() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<BigInteger> inputEntropy() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<BigInteger, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeSignedBigInteger(secret_input_lambda, 16);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static RandomnessContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkRandomness_SDK_15_4_0(client, address).deserializeRandomnessContractState(input);
  }
  
  public static RandomnessContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static RandomnessContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record GenerateRandomBytesAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new ZkRandomness_SDK_15_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeGenerateRandomBytesAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte requiredNoOfInputs) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkRandomness_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
