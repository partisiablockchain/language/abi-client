// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractVecOption_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myVec_vecLength = _input.readI32();
    const myVec: Array<Option<BN>> = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      let myVec_elem: Option<BN> = undefined;
      const myVec_elem_isSome = _input.readBoolean();
      if (myVec_elem_isSome) {
        const myVec_elem_option: BN = _input.readU64();
        myVec_elem = myVec_elem_option;
      }
      myVec.push(myVec_elem);
    }
    const mySet_setLength = _input.readI32();
    const mySet: Array<Option<BN>> = [];
    for (let mySet_i = 0; mySet_i < mySet_setLength; mySet_i++) {
      let mySet_elem: Option<BN> = undefined;
      const mySet_elem_isSome = _input.readBoolean();
      if (mySet_elem_isSome) {
        const mySet_elem_option: BN = _input.readU64();
        mySet_elem = mySet_elem_option;
      }
      mySet.push(mySet_elem);
    }
    return { myVec, mySet };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyVecAction(_input: AbiInput): UpdateMyVecAction {
    const value_vecLength = _input.readI32();
    const value: Array<Option<BN>> = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      let value_elem: Option<BN> = undefined;
      const value_elem_isSome = _input.readBoolean();
      if (value_elem_isSome) {
        const value_elem_option: BN = _input.readU64();
        value_elem = value_elem_option;
      }
      value.push(value_elem);
    }
    return { discriminant: "update_my_vec", value };
  }

  public deserializeUpdateMySetAction(_input: AbiInput): UpdateMySetAction {
    let value: Option<BN> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: BN = _input.readU64();
      value = value_option;
    }
    return { discriminant: "update_my_set", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myVec_vecLength = _input.readI32();
    const myVec: Array<Option<BN>> = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      let myVec_elem: Option<BN> = undefined;
      const myVec_elem_isSome = _input.readBoolean();
      if (myVec_elem_isSome) {
        const myVec_elem_option: BN = _input.readU64();
        myVec_elem = myVec_elem_option;
      }
      myVec.push(myVec_elem);
    }
    return { discriminant: "initialize", myVec };
  }

}
export interface ExampleContractState {
  myVec: Array<Option<BN>>;
  mySet: Array<Option<BN>>;
}

export function initialize(myVec: Array<Option<BN>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(myVec.length);
    for (const myVec_vec of myVec) {
      _out.writeBoolean(myVec_vec !== undefined);
      if (myVec_vec !== undefined) {
        _out.writeU64(myVec_vec);
      }
    }
  });
}

export function updateMyVec(value: Array<Option<BN>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("9d99e6e401", "hex"));
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeBoolean(value_vec !== undefined);
      if (value_vec !== undefined) {
        _out.writeU64(value_vec);
      }
    }
  });
}

export function updateMySet(value: Option<BN>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("c7e1eca904", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeU64(value);
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractVecOption_SDK_13_3_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractVecOption_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyVecAction
  | UpdateMySetAction;

export interface UpdateMyVecAction {
  discriminant: "update_my_vec";
  value: Array<Option<BN>>;
}
export interface UpdateMySetAction {
  discriminant: "update_my_set";
  value: Option<BN>;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractVecOption_SDK_13_3_0(undefined, undefined);
  if (shortname === "9d99e6e401") {
    return contract.deserializeUpdateMyVecAction(input);
  } else if (shortname === "c7e1eca904") {
    return contract.deserializeUpdateMySetAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myVec: Array<Option<BN>>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractVecOption_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

