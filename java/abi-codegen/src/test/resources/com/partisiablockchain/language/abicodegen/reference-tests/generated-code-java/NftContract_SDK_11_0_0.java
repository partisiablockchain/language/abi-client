// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class NftContract_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public NftContract_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private NFTContractState deserializeNFTContractState(AbiInput _input) {
    String name = _input.readString();
    String symbol = _input.readString();
    BlockchainAddress contractOwner = _input.readAddress();
    var owners_mapLength = _input.readI32();
    Map<BigInteger, BlockchainAddress> owners = new HashMap<>();
    for (int owners_i = 0; owners_i < owners_mapLength; owners_i++) {
      BigInteger owners_key = _input.readUnsignedBigInteger(16);
      BlockchainAddress owners_value = _input.readAddress();
      owners.put(owners_key, owners_value);
    }
    var tokenApprovals_mapLength = _input.readI32();
    Map<BigInteger, BlockchainAddress> tokenApprovals = new HashMap<>();
    for (int tokenApprovals_i = 0; tokenApprovals_i < tokenApprovals_mapLength; tokenApprovals_i++) {
      BigInteger tokenApprovals_key = _input.readUnsignedBigInteger(16);
      BlockchainAddress tokenApprovals_value = _input.readAddress();
      tokenApprovals.put(tokenApprovals_key, tokenApprovals_value);
    }
    var operatorApprovals_mapLength = _input.readI32();
    Map<BlockchainAddress, List<BlockchainAddress>> operatorApprovals = new HashMap<>();
    for (int operatorApprovals_i = 0; operatorApprovals_i < operatorApprovals_mapLength; operatorApprovals_i++) {
      BlockchainAddress operatorApprovals_key = _input.readAddress();
      var operatorApprovals_value_vecLength = _input.readI32();
      List<BlockchainAddress> operatorApprovals_value = new ArrayList<>();
      for (int operatorApprovals_value_i = 0; operatorApprovals_value_i < operatorApprovals_value_vecLength; operatorApprovals_value_i++) {
        BlockchainAddress operatorApprovals_value_elem = _input.readAddress();
        operatorApprovals_value.add(operatorApprovals_value_elem);
      }
      operatorApprovals.put(operatorApprovals_key, operatorApprovals_value);
    }
    var tokenUris_mapLength = _input.readI32();
    Map<BigInteger, String> tokenUris = new HashMap<>();
    for (int tokenUris_i = 0; tokenUris_i < tokenUris_mapLength; tokenUris_i++) {
      BigInteger tokenUris_key = _input.readUnsignedBigInteger(16);
      String tokenUris_value = _input.readString();
      tokenUris.put(tokenUris_key, tokenUris_value);
    }
    return new NFTContractState(name, symbol, contractOwner, owners, tokenApprovals, operatorApprovals, tokenUris);
  }
  public NFTContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeNFTContractState(input);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    BlockchainAddress approved = null;
    var approved_isSome = _input.readBoolean();
    if (approved_isSome) {
      BlockchainAddress approved_option = _input.readAddress();
      approved = approved_option;
    }
    BigInteger tokenId = _input.readUnsignedBigInteger(16);
    return new ApproveAction(approved, tokenId);
  }

  private SetApprovalForAllAction deserializeSetApprovalForAllAction(AbiInput _input) {
    BlockchainAddress operator = _input.readAddress();
    boolean approved = _input.readBoolean();
    return new SetApprovalForAllAction(operator, approved);
  }

  private TransferFromAction deserializeTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    BlockchainAddress to = _input.readAddress();
    BigInteger tokenId = _input.readUnsignedBigInteger(16);
    return new TransferFromAction(from, to, tokenId);
  }

  private MintAction deserializeMintAction(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    BigInteger tokenId = _input.readUnsignedBigInteger(16);
    String tokenUri = _input.readString();
    return new MintAction(to, tokenId, tokenUri);
  }

  private BurnAction deserializeBurnAction(AbiInput _input) {
    BigInteger tokenId = _input.readUnsignedBigInteger(16);
    return new BurnAction(tokenId);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    String name = _input.readString();
    String symbol = _input.readString();
    return new InitializeInit(name, symbol);
  }


  @AbiGenerated
  public record NFTContractState(String name, String symbol, BlockchainAddress contractOwner, Map<BigInteger, BlockchainAddress> owners, Map<BigInteger, BlockchainAddress> tokenApprovals, Map<BlockchainAddress, List<BlockchainAddress>> operatorApprovals, Map<BigInteger, String> tokenUris) {
    public static NFTContractState deserialize(byte[] bytes) {
      return NftContract_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(String name, String symbol) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(name);
      _out.writeString(symbol);
    });
  }

  public static byte[] approve(BlockchainAddress approved, BigInteger tokenId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("80ad88a707"));
      _out.writeBoolean(approved != null);
      if (approved != null) {
        _out.writeAddress(approved);
      }
      _out.writeUnsignedBigInteger(tokenId, 16);
    });
  }

  public static byte[] setApprovalForAll(BlockchainAddress operator, boolean approved) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("96c4908202"));
      _out.writeAddress(operator);
      _out.writeBoolean(approved);
    });
  }

  public static byte[] transferFrom(BlockchainAddress from, BlockchainAddress to, BigInteger tokenId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("d799a3b608"));
      _out.writeAddress(from);
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(tokenId, 16);
    });
  }

  public static byte[] mint(BlockchainAddress to, BigInteger tokenId, String tokenUri) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("bbafbce30d"));
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(tokenId, 16);
      _out.writeString(tokenUri);
    });
  }

  public static byte[] burn(BigInteger tokenId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("c5d9feac08"));
      _out.writeUnsignedBigInteger(tokenId, 16);
    });
  }

  public static NFTContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new NftContract_SDK_11_0_0(client, address).deserializeNFTContractState(input);
  }
  
  public static NFTContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static NFTContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ApproveAction(BlockchainAddress approved, BigInteger tokenId) implements Action {
  }
  @AbiGenerated
  public record SetApprovalForAllAction(BlockchainAddress operator, boolean approved) implements Action {
  }
  @AbiGenerated
  public record TransferFromAction(BlockchainAddress from, BlockchainAddress to, BigInteger tokenId) implements Action {
  }
  @AbiGenerated
  public record MintAction(BlockchainAddress to, BigInteger tokenId, String tokenUri) implements Action {
  }
  @AbiGenerated
  public record BurnAction(BigInteger tokenId) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new NftContract_SDK_11_0_0(null, null);
    if (shortname.equals("80ad88a707")) {
      return contract.deserializeApproveAction(input);
    } else if (shortname.equals("96c4908202")) {
      return contract.deserializeSetApprovalForAllAction(input);
    } else if (shortname.equals("d799a3b608")) {
      return contract.deserializeTransferFromAction(input);
    } else if (shortname.equals("bbafbce30d")) {
      return contract.deserializeMintAction(input);
    } else if (shortname.equals("c5d9feac08")) {
      return contract.deserializeBurnAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(String name, String symbol) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new NftContract_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
