// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractCallbacks_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const val: number = _input.readU32();
    const successfulCallback: boolean = _input.readBoolean();
    const callbackResults_vecLength = _input.readI32();
    const callbackResults: boolean[] = [];
    for (let callbackResults_i = 0; callbackResults_i < callbackResults_vecLength; callbackResults_i++) {
      const callbackResults_elem: boolean = _input.readBoolean();
      callbackResults.push(callbackResults_elem);
    }
    const callbackValue: number = _input.readU32();
    return { val, successfulCallback, callbackResults, callbackValue };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeSetAndCallbackAction(_input: AbiInput): SetAndCallbackAction {
    const value: number = _input.readU32();
    return { discriminant: "set_and_callback", value };
  }

  public deserializePanicAndCallbackAction(_input: AbiInput): PanicAndCallbackAction {
    return { discriminant: "panic_and_callback",  };
  }

  public deserializePanickingAction(_input: AbiInput): PanickingAction {
    return { discriminant: "panicking",  };
  }

  public deserializeSetAction(_input: AbiInput): SetAction {
    const value: number = _input.readU32();
    return { discriminant: "set", value };
  }

  public deserializeMultipleEventsAndCallbackAction(_input: AbiInput): MultipleEventsAndCallbackAction {
    const value: number = _input.readU32();
    return { discriminant: "multiple_events_and_callback", value };
  }

  public deserializeCallbackCallback(_input: AbiInput): CallbackCallback {
    const val: number = _input.readU32();
    return { discriminant: "callback", val };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const val: number = _input.readU32();
    return { discriminant: "initialize", val };
  }

}
export interface ExampleContractState {
  val: number;
  successfulCallback: boolean;
  callbackResults: boolean[];
  callbackValue: number;
}

export function initialize(val: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU32(val);
  });
}

export function setAndCallback(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU32(value);
  });
}

export function panicAndCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function panicking(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
  });
}

export function set(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeU32(value);
  });
}

export function multipleEventsAndCallback(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeU32(value);
  });
}

export function callback(val: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeU32(val);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractCallbacks_SDK_11_0_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractCallbacks_SDK_11_0_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | SetAndCallbackAction
  | PanicAndCallbackAction
  | PanickingAction
  | SetAction
  | MultipleEventsAndCallbackAction;

export interface SetAndCallbackAction {
  discriminant: "set_and_callback";
  value: number;
}
export interface PanicAndCallbackAction {
  discriminant: "panic_and_callback";
}
export interface PanickingAction {
  discriminant: "panicking";
}
export interface SetAction {
  discriminant: "set";
  value: number;
}
export interface MultipleEventsAndCallbackAction {
  discriminant: "multiple_events_and_callback";
  value: number;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractCallbacks_SDK_11_0_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeSetAndCallbackAction(input);
  } else if (shortname === "02") {
    return contract.deserializePanicAndCallbackAction(input);
  } else if (shortname === "03") {
    return contract.deserializePanickingAction(input);
  } else if (shortname === "04") {
    return contract.deserializeSetAction(input);
  } else if (shortname === "06") {
    return contract.deserializeMultipleEventsAndCallbackAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | CallbackCallback;

export interface CallbackCallback {
  discriminant: "callback";
  val: number;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractCallbacks_SDK_11_0_0(undefined, undefined);
  if (shortname === "05") {
    return contract.deserializeCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  val: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractCallbacks_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

