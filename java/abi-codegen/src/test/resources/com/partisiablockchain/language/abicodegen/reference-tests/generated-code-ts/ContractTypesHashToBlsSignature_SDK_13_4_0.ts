// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractTypesHashToBlsSignature_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeInputRecord(_input: AbiInput): InputRecord {
    const u256_vecLength = _input.readI32();
    const u256: BN[] = [];
    for (let u256_i = 0; u256_i < u256_vecLength; u256_i++) {
      const u256_elem: BN = _input.readUnsignedBigInteger(32);
      u256.push(u256_elem);
    }
    const hash_vecLength = _input.readI32();
    const hash: Hash[] = [];
    for (let hash_i = 0; hash_i < hash_vecLength; hash_i++) {
      const hash_elem: Hash = _input.readHash();
      hash.push(hash_elem);
    }
    const publicKey_vecLength = _input.readI32();
    const publicKey: BlockchainPublicKey[] = [];
    for (let publicKey_i = 0; publicKey_i < publicKey_vecLength; publicKey_i++) {
      const publicKey_elem: BlockchainPublicKey = _input.readPublicKey();
      publicKey.push(publicKey_elem);
    }
    const signature_vecLength = _input.readI32();
    const signature: Signature[] = [];
    for (let signature_i = 0; signature_i < signature_vecLength; signature_i++) {
      const signature_elem: Signature = _input.readSignature();
      signature.push(signature_elem);
    }
    const blsPublicKey_vecLength = _input.readI32();
    const blsPublicKey: BlsPublicKey[] = [];
    for (let blsPublicKey_i = 0; blsPublicKey_i < blsPublicKey_vecLength; blsPublicKey_i++) {
      const blsPublicKey_elem: BlsPublicKey = _input.readBlsPublicKey();
      blsPublicKey.push(blsPublicKey_elem);
    }
    const blsSignature_vecLength = _input.readI32();
    const blsSignature: BlsSignature[] = [];
    for (let blsSignature_i = 0; blsSignature_i < blsSignature_vecLength; blsSignature_i++) {
      const blsSignature_elem: BlsSignature = _input.readBlsSignature();
      blsSignature.push(blsSignature_elem);
    }
    return { u256, hash, publicKey, signature, blsPublicKey, blsSignature };
  }
  public deserializeState(_input: AbiInput): State {
    const u256: BN = _input.readUnsignedBigInteger(32);
    const hash: Hash = _input.readHash();
    const publicKey: BlockchainPublicKey = _input.readPublicKey();
    const signature: Signature = _input.readSignature();
    const blsPublicKey: BlsPublicKey = _input.readBlsPublicKey();
    const blsSignature: BlsSignature = _input.readBlsSignature();
    const typeVectors: InputRecord = this.deserializeInputRecord(_input);
    return { u256, hash, publicKey, signature, blsPublicKey, blsSignature, typeVectors };
  }
  public async getState(): Promise<State> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeState(input);
  }

  public deserializeSetU256Action(_input: AbiInput): SetU256Action {
    const u256: BN = _input.readUnsignedBigInteger(32);
    return { discriminant: "set_u256", u256 };
  }

  public deserializeSetHashAction(_input: AbiInput): SetHashAction {
    const hash: Hash = _input.readHash();
    return { discriminant: "set_hash", hash };
  }

  public deserializeSetPublicKeyAction(_input: AbiInput): SetPublicKeyAction {
    const publicKey: BlockchainPublicKey = _input.readPublicKey();
    return { discriminant: "set_public_key", publicKey };
  }

  public deserializeSetSignatureAction(_input: AbiInput): SetSignatureAction {
    const signature: Signature = _input.readSignature();
    return { discriminant: "set_signature", signature };
  }

  public deserializeSetBlsPublicKeyAction(_input: AbiInput): SetBlsPublicKeyAction {
    const blsPublicKey: BlsPublicKey = _input.readBlsPublicKey();
    return { discriminant: "set_bls_public_key", blsPublicKey };
  }

  public deserializeSetBlsSignatureAction(_input: AbiInput): SetBlsSignatureAction {
    const blsSignature: BlsSignature = _input.readBlsSignature();
    return { discriminant: "set_bls_signature", blsSignature };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface InputRecord {
  u256: BN[];
  hash: Hash[];
  publicKey: BlockchainPublicKey[];
  signature: Signature[];
  blsPublicKey: BlsPublicKey[];
  blsSignature: BlsSignature[];
}

export interface State {
  u256: BN;
  hash: Hash;
  publicKey: BlockchainPublicKey;
  signature: Signature;
  blsPublicKey: BlsPublicKey;
  blsSignature: BlsSignature;
  typeVectors: InputRecord;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function setU256(u256: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeUnsignedBigInteger(u256, 32);
  });
}

export function setHash(hash: Hash): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeHash(hash);
  });
}

export function setPublicKey(publicKey: BlockchainPublicKey): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writePublicKey(publicKey);
  });
}

export function setSignature(signature: Signature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeSignature(signature);
  });
}

export function setBlsPublicKey(blsPublicKey: BlsPublicKey): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeBlsPublicKey(blsPublicKey);
  });
}

export function setBlsSignature(blsSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeBlsSignature(blsSignature);
  });
}

export function deserializeState(state: StateWithClient): State;
export function deserializeState(bytes: Buffer): State;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): State;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): State {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractTypesHashToBlsSignature_SDK_13_4_0(client, address).deserializeState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractTypesHashToBlsSignature_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeState(input);
  }
}

export type Action =
  | SetU256Action
  | SetHashAction
  | SetPublicKeyAction
  | SetSignatureAction
  | SetBlsPublicKeyAction
  | SetBlsSignatureAction;

export interface SetU256Action {
  discriminant: "set_u256";
  u256: BN;
}
export interface SetHashAction {
  discriminant: "set_hash";
  hash: Hash;
}
export interface SetPublicKeyAction {
  discriminant: "set_public_key";
  publicKey: BlockchainPublicKey;
}
export interface SetSignatureAction {
  discriminant: "set_signature";
  signature: Signature;
}
export interface SetBlsPublicKeyAction {
  discriminant: "set_bls_public_key";
  blsPublicKey: BlsPublicKey;
}
export interface SetBlsSignatureAction {
  discriminant: "set_bls_signature";
  blsSignature: BlsSignature;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractTypesHashToBlsSignature_SDK_13_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeSetU256Action(input);
  } else if (shortname === "02") {
    return contract.deserializeSetHashAction(input);
  } else if (shortname === "03") {
    return contract.deserializeSetPublicKeyAction(input);
  } else if (shortname === "04") {
    return contract.deserializeSetSignatureAction(input);
  } else if (shortname === "05") {
    return contract.deserializeSetBlsPublicKeyAction(input);
  } else if (shortname === "06") {
    return contract.deserializeSetBlsSignatureAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractTypesHashToBlsSignature_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

