// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class PriceOracle_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeDispute(_input: AbiInput): Dispute {
    const claims_vecLength = _input.readI32();
    const claims: RoundData[] = [];
    for (let claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      const claims_elem: RoundData = this.deserializeRoundData(_input);
      claims.push(claims_elem);
    }
    const challenger: BlockchainAddress = _input.readAddress();
    return { claims, challenger };
  }
  public deserializeRoundData(_input: AbiInput): RoundData {
    const roundId: Buffer = _input.readBytes(10);
    const answer: Buffer = _input.readBytes(32);
    const startedAt: Buffer = _input.readBytes(32);
    const updatedAt: Buffer = _input.readBytes(32);
    const answeredInRound: Buffer = _input.readBytes(32);
    return { roundId, answer, startedAt, updatedAt, answeredInRound };
  }
  public deserializePriceUpdates(_input: AbiInput): PriceUpdates {
    const oraclePriceUpdates_mapLength = _input.readI32();
    const oraclePriceUpdates: Map<BlockchainAddress, RoundData> = new Map();
    for (let oraclePriceUpdates_i = 0; oraclePriceUpdates_i < oraclePriceUpdates_mapLength; oraclePriceUpdates_i++) {
      const oraclePriceUpdates_key: BlockchainAddress = _input.readAddress();
      const oraclePriceUpdates_value: RoundData = this.deserializeRoundData(_input);
      oraclePriceUpdates.set(oraclePriceUpdates_key, oraclePriceUpdates_value);
    }
    const disputePeriodEnd: BN = _input.readU64();
    return { oraclePriceUpdates, disputePeriodEnd };
  }
  public deserializePriceOracleContractState(_input: AbiInput): PriceOracleContractState {
    const largeOracleContractAddress: BlockchainAddress = _input.readAddress();
    const referenceContractAddress: string = _input.readString();
    const oracleNodes_vecLength = _input.readI32();
    const oracleNodes: BlockchainAddress[] = [];
    for (let oracleNodes_i = 0; oracleNodes_i < oracleNodes_vecLength; oracleNodes_i++) {
      const oracleNodes_elem: BlockchainAddress = _input.readAddress();
      oracleNodes.push(oracleNodes_elem);
    }
    const roundPriceUpdates_mapLength = _input.readI32();
    const roundPriceUpdates: Map<Buffer, PriceUpdates> = new Map();
    for (let roundPriceUpdates_i = 0; roundPriceUpdates_i < roundPriceUpdates_mapLength; roundPriceUpdates_i++) {
      const roundPriceUpdates_key: Buffer = _input.readBytes(32);
      const roundPriceUpdates_value: PriceUpdates = this.deserializePriceUpdates(_input);
      roundPriceUpdates.set(roundPriceUpdates_key, roundPriceUpdates_value);
    }
    const latestPublishedRoundId: Buffer = _input.readBytes(32);
    const symbol: string = _input.readString();
    const activeDisputePeriodRoundId: Buffer = _input.readBytes(32);
    const dispute: Dispute = this.deserializeDispute(_input);
    return { largeOracleContractAddress, referenceContractAddress, oracleNodes, roundPriceUpdates, latestPublishedRoundId, symbol, activeDisputePeriodRoundId, dispute };
  }
  public async getState(): Promise<PriceOracleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializePriceOracleContractState(input);
  }

  public deserializeRegisterNodeAction(_input: AbiInput): RegisterNodeAction {
    return { discriminant: "register_node",  };
  }

  public deserializeNotifyPriceUpdateAction(_input: AbiInput): NotifyPriceUpdateAction {
    const roundData: RoundData = this.deserializeRoundData(_input);
    return { discriminant: "notify_price_update", roundData };
  }

  public deserializeDisputeResultAction(_input: AbiInput): DisputeResultAction {
    const oracleId: BN = _input.readU64();
    const disputeId: BN = _input.readU64();
    const result: number = _input.readU32();
    return { discriminant: "dispute_result", oracleId, disputeId, result };
  }

  public deserializeDisputeCounterClaimAction(_input: AbiInput): DisputeCounterClaimAction {
    const counterClaim: RoundData = this.deserializeRoundData(_input);
    return { discriminant: "dispute_counter_claim", counterClaim };
  }

  public deserializeDeregisterNodeAction(_input: AbiInput): DeregisterNodeAction {
    return { discriminant: "deregister_node",  };
  }

  public deserializeStartDisputeAction(_input: AbiInput): StartDisputeAction {
    const roundId: Buffer = _input.readBytes(32);
    return { discriminant: "start_dispute", roundId };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const symbol: string = _input.readString();
    const largeOracleContractAddress: BlockchainAddress = _input.readAddress();
    const referenceContractAddress: string = _input.readString();
    return { discriminant: "init", symbol, largeOracleContractAddress, referenceContractAddress };
  }

}
export interface Dispute {
  claims: RoundData[];
  challenger: BlockchainAddress;
}

export interface RoundData {
  roundId: Buffer;
  answer: Buffer;
  startedAt: Buffer;
  updatedAt: Buffer;
  answeredInRound: Buffer;
}
function serializeRoundData(_out: AbiOutput, _value: RoundData): void {
  const { roundId, answer, startedAt, updatedAt, answeredInRound } = _value;
  if (roundId.length != 10) {
    throw new Error("Length of roundId does not match expected 10");
  }
  _out.writeBytes(roundId);
  if (answer.length != 32) {
    throw new Error("Length of answer does not match expected 32");
  }
  _out.writeBytes(answer);
  if (startedAt.length != 32) {
    throw new Error("Length of startedAt does not match expected 32");
  }
  _out.writeBytes(startedAt);
  if (updatedAt.length != 32) {
    throw new Error("Length of updatedAt does not match expected 32");
  }
  _out.writeBytes(updatedAt);
  if (answeredInRound.length != 32) {
    throw new Error("Length of answeredInRound does not match expected 32");
  }
  _out.writeBytes(answeredInRound);
}

export interface PriceUpdates {
  oraclePriceUpdates: Map<BlockchainAddress, RoundData>;
  disputePeriodEnd: BN;
}

export interface PriceOracleContractState {
  largeOracleContractAddress: BlockchainAddress;
  referenceContractAddress: string;
  oracleNodes: BlockchainAddress[];
  roundPriceUpdates: Map<Buffer, PriceUpdates>;
  latestPublishedRoundId: Buffer;
  symbol: string;
  activeDisputePeriodRoundId: Buffer;
  dispute: Dispute;
}

export function init(symbol: string, largeOracleContractAddress: BlockchainAddress, referenceContractAddress: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeString(symbol);
    _out.writeAddress(largeOracleContractAddress);
    _out.writeString(referenceContractAddress);
  });
}

export function registerNode(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
  });
}

export function notifyPriceUpdate(roundData: RoundData): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    serializeRoundData(_out, roundData);
  });
}

export function disputeResult(oracleId: BN, disputeId: BN, result: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeU64(oracleId);
    _out.writeU64(disputeId);
    _out.writeU32(result);
  });
}

export function disputeCounterClaim(counterClaim: RoundData): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    serializeRoundData(_out, counterClaim);
  });
}

export function deregisterNode(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
  });
}

export function startDispute(roundId: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    if (roundId.length != 32) {
      throw new Error("Length of roundId does not match expected 32");
    }
    _out.writeBytes(roundId);
  });
}

export function deserializeState(state: StateWithClient): PriceOracleContractState;
export function deserializeState(bytes: Buffer): PriceOracleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): PriceOracleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): PriceOracleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new PriceOracle_SDK_13_3_0(client, address).deserializePriceOracleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new PriceOracle_SDK_13_3_0(
      state.client,
      state.address
    ).deserializePriceOracleContractState(input);
  }
}

export type Action =
  | RegisterNodeAction
  | NotifyPriceUpdateAction
  | DisputeResultAction
  | DisputeCounterClaimAction
  | DeregisterNodeAction
  | StartDisputeAction;

export interface RegisterNodeAction {
  discriminant: "register_node";
}
export interface NotifyPriceUpdateAction {
  discriminant: "notify_price_update";
  roundData: RoundData;
}
export interface DisputeResultAction {
  discriminant: "dispute_result";
  oracleId: BN;
  disputeId: BN;
  result: number;
}
export interface DisputeCounterClaimAction {
  discriminant: "dispute_counter_claim";
  counterClaim: RoundData;
}
export interface DeregisterNodeAction {
  discriminant: "deregister_node";
}
export interface StartDisputeAction {
  discriminant: "start_dispute";
  roundId: Buffer;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PriceOracle_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeRegisterNodeAction(input);
  } else if (shortname === "01") {
    return contract.deserializeNotifyPriceUpdateAction(input);
  } else if (shortname === "02") {
    return contract.deserializeDisputeResultAction(input);
  } else if (shortname === "05") {
    return contract.deserializeDisputeCounterClaimAction(input);
  } else if (shortname === "04") {
    return contract.deserializeDeregisterNodeAction(input);
  } else if (shortname === "06") {
    return contract.deserializeStartDisputeAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  symbol: string;
  largeOracleContractAddress: BlockchainAddress;
  referenceContractAddress: string;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PriceOracle_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

