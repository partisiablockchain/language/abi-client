// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class TokenMapping_SDK_16_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public TokenMapping_SDK_16_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    var map_treeId = _input.readI32();
    AvlTreeMap<BlockchainAddress, BigInteger> map = new AvlTreeMap<>(
      map_treeId,
      this._client,
      this._address,
      (map_key) -> AbiByteOutput.serializeLittleEndian(map_out -> {
        map_out.writeAddress(map_key);
      }),
      (map_bytes) -> {
        var map_input = AbiByteInput.createLittleEndian(map_bytes);
        BlockchainAddress map_key = map_input.readAddress();
        return map_key;
      },
      (map_bytes) -> {
        var map_input = AbiByteInput.createLittleEndian(map_bytes);
        BigInteger map_value = map_input.readUnsignedBigInteger(16);
        return map_value;
      }
    );
    BigInteger result = null;
    var result_isSome = _input.readBoolean();
    if (result_isSome) {
      BigInteger result_option = _input.readUnsignedBigInteger(16);
      result = result_option;
    }
    return new ContractState(map, result);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private InsertAction deserializeInsertAction(AbiInput _input) {
    BlockchainAddress key = _input.readAddress();
    BigInteger value = _input.readUnsignedBigInteger(16);
    return new InsertAction(key, value);
  }

  private RemoveAction deserializeRemoveAction(AbiInput _input) {
    BlockchainAddress key = _input.readAddress();
    return new RemoveAction(key);
  }

  private GetAction deserializeGetAction(AbiInput _input) {
    BlockchainAddress key = _input.readAddress();
    return new GetAction(key);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record ContractState(AvlTreeMap<BlockchainAddress, BigInteger> map, BigInteger result) {
    public static ContractState deserialize(byte[] bytes) {
      return TokenMapping_SDK_16_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] insert(BlockchainAddress key, BigInteger value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(key);
      _out.writeUnsignedBigInteger(value, 16);
    });
  }

  public static byte[] remove(BlockchainAddress key) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(key);
    });
  }

  public static byte[] get(BlockchainAddress key) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(key);
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new TokenMapping_SDK_16_1_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record InsertAction(BlockchainAddress key, BigInteger value) implements Action {
  }
  @AbiGenerated
  public record RemoveAction(BlockchainAddress key) implements Action {
  }
  @AbiGenerated
  public record GetAction(BlockchainAddress key) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenMapping_SDK_16_1_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeInsertAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeRemoveAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeGetAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenMapping_SDK_16_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
