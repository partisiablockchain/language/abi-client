// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractReturnDataCaller_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeU8Struct(_input: AbiInput): U8Struct {
    const u8Value: number = _input.readU8();
    const u8Vector_vecLength = _input.readI32();
    const u8Vector: Buffer = _input.readBytes(u8Vector_vecLength);
    const u8Struct_vecLength = _input.readI32();
    const u8Struct: U8Struct[] = [];
    for (let u8Struct_i = 0; u8Struct_i < u8Struct_vecLength; u8Struct_i++) {
      const u8Struct_elem: U8Struct = this.deserializeU8Struct(_input);
      u8Struct.push(u8Struct_elem);
    }
    return { u8Value, u8Vector, u8Struct };
  }
  public deserializeState(_input: AbiInput): State {
    const returnedU64: BN = _input.readU64();
    const returnedString: string = _input.readString();
    const returnedBool: boolean = _input.readBoolean();
    const returnedU8Struct: U8Struct = this.deserializeU8Struct(_input);
    return { returnedU64, returnedString, returnedBool, returnedU8Struct };
  }
  public async getState(): Promise<State> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeState(input);
  }

  public deserializeAskForBasicValuesAction(_input: AbiInput): AskForBasicValuesAction {
    const contractAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "ask_for_basic_values", contractAddress };
  }

  public deserializeAskForU8StructAction(_input: AbiInput): AskForU8StructAction {
    const contractAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "ask_for_u8_struct", contractAddress };
  }

  public deserializeAskForU64WithCourierAction(_input: AbiInput): AskForU64WithCourierAction {
    const contractAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "ask_for_u64_with_courier", contractAddress };
  }

  public deserializeAskForBasicValuesCallbackCallback(_input: AbiInput): AskForBasicValuesCallbackCallback {
    return { discriminant: "ask_for_basic_values_callback",  };
  }

  public deserializeAskForU8StructCallbackCallback(_input: AbiInput): AskForU8StructCallbackCallback {
    return { discriminant: "ask_for_u8_struct_callback",  };
  }

  public deserializeAskForU64WithCourierCallbackCallback(_input: AbiInput): AskForU64WithCourierCallbackCallback {
    return { discriminant: "ask_for_u64_with_courier_callback",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface U8Struct {
  u8Value: number;
  u8Vector: Buffer;
  u8Struct: U8Struct[];
}

export interface State {
  returnedU64: BN;
  returnedString: string;
  returnedBool: boolean;
  returnedU8Struct: U8Struct;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function askForBasicValues(contractAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(contractAddress);
  });
}

export function askForU8Struct(contractAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(contractAddress);
  });
}

export function askForU64WithCourier(contractAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(contractAddress);
  });
}

export function askForBasicValuesCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("10", "hex"));
  });
}

export function askForU8StructCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("20", "hex"));
  });
}

export function askForU64WithCourierCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("30", "hex"));
  });
}

export function deserializeState(state: StateWithClient): State;
export function deserializeState(bytes: Buffer): State;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): State;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): State {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractReturnDataCaller_SDK_9_1_2(client, address).deserializeState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractReturnDataCaller_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeState(input);
  }
}

export type Action =
  | AskForBasicValuesAction
  | AskForU8StructAction
  | AskForU64WithCourierAction;

export interface AskForBasicValuesAction {
  discriminant: "ask_for_basic_values";
  contractAddress: BlockchainAddress;
}
export interface AskForU8StructAction {
  discriminant: "ask_for_u8_struct";
  contractAddress: BlockchainAddress;
}
export interface AskForU64WithCourierAction {
  discriminant: "ask_for_u64_with_courier";
  contractAddress: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataCaller_SDK_9_1_2(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeAskForBasicValuesAction(input);
  } else if (shortname === "02") {
    return contract.deserializeAskForU8StructAction(input);
  } else if (shortname === "03") {
    return contract.deserializeAskForU64WithCourierAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | AskForBasicValuesCallbackCallback
  | AskForU8StructCallbackCallback
  | AskForU64WithCourierCallbackCallback;

export interface AskForBasicValuesCallbackCallback {
  discriminant: "ask_for_basic_values_callback";
}
export interface AskForU8StructCallbackCallback {
  discriminant: "ask_for_u8_struct_callback";
}
export interface AskForU64WithCourierCallbackCallback {
  discriminant: "ask_for_u64_with_courier_callback";
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataCaller_SDK_9_1_2(undefined, undefined);
  if (shortname === "10") {
    return contract.deserializeAskForBasicValuesCallbackCallback(input);
  } else if (shortname === "20") {
    return contract.deserializeAskForU8StructCallbackCallback(input);
  } else if (shortname === "30") {
    return contract.deserializeAskForU64WithCourierCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataCaller_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

