// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class FoundationContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVestingSchedule(_input: AbiInput): VestingSchedule {
    const releaseDuration: BN = _input.readI64();
    const releaseInterval: BN = _input.readI64();
    const tokenGenerationEvent: BN = _input.readI64();
    return { releaseDuration, releaseInterval, tokenGenerationEvent };
  }
  public deserializePendingTransfer(_input: AbiInput): PendingTransfer {
    const receiver: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readI64();
    const vestingSchedule: VestingSchedule = this.deserializeVestingSchedule(_input);
    return { receiver, amount, vestingSchedule };
  }
  public deserializeFoundationContractState(_input: AbiInput): FoundationContractState {
    const nonce: BN = _input.readI64();
    const remainingTokens: BN = _input.readI64();
    const foundationMembers_vecLength = _input.readI32();
    const foundationMembers: Buffer[] = [];
    for (let foundationMembers_i = 0; foundationMembers_i < foundationMembers_vecLength; foundationMembers_i++) {
      const foundationMembers_elem: Buffer = _input.readBytes(33);
      foundationMembers.push(foundationMembers_elem);
    }
    return { nonce, remainingTokens, foundationMembers };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU8();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public async getState(): Promise<FoundationContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeFoundationContractState(input);
  }

  public deserializeSignTransferAction(_input: AbiInput): SignTransferAction {
    const pendingTransfers_vecLength = _input.readI32();
    const pendingTransfers: PendingTransfer[] = [];
    for (let pendingTransfers_i = 0; pendingTransfers_i < pendingTransfers_vecLength; pendingTransfers_i++) {
      const pendingTransfers_elem: PendingTransfer = this.deserializePendingTransfer(_input);
      pendingTransfers.push(pendingTransfers_elem);
    }
    const signatures_vecLength = _input.readI32();
    const signatures: Signature[] = [];
    for (let signatures_i = 0; signatures_i < signatures_vecLength; signatures_i++) {
      const signatures_elem: Signature = this.deserializeSignature(_input);
      signatures.push(signatures_elem);
    }
    return { discriminant: "sign_transfer", pendingTransfers, signatures };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const pooledTokens: BN = _input.readI64();
    const foundationMembers_vecLength = _input.readI32();
    const foundationMembers: Buffer[] = [];
    for (let foundationMembers_i = 0; foundationMembers_i < foundationMembers_vecLength; foundationMembers_i++) {
      const foundationMembers_elem: Buffer = _input.readBytes(33);
      foundationMembers.push(foundationMembers_elem);
    }
    return { discriminant: "init", pooledTokens, foundationMembers };
  }

}
export interface VestingSchedule {
  releaseDuration: BN;
  releaseInterval: BN;
  tokenGenerationEvent: BN;
}
function serializeVestingSchedule(_out: AbiOutput, _value: VestingSchedule): void {
  const { releaseDuration, releaseInterval, tokenGenerationEvent } = _value;
  _out.writeI64(releaseDuration);
  _out.writeI64(releaseInterval);
  _out.writeI64(tokenGenerationEvent);
}

export interface PendingTransfer {
  receiver: BlockchainAddress;
  amount: BN;
  vestingSchedule: VestingSchedule;
}
function serializePendingTransfer(_out: AbiOutput, _value: PendingTransfer): void {
  const { receiver, amount, vestingSchedule } = _value;
  _out.writeAddress(receiver);
  _out.writeI64(amount);
  serializeVestingSchedule(_out, vestingSchedule);
}

export interface FoundationContractState {
  nonce: BN;
  remainingTokens: BN;
  foundationMembers: Buffer[];
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}
function serializeSignature(_out: AbiOutput, _value: Signature): void {
  const { recoveryId, valueR, valueS } = _value;
  _out.writeU8(recoveryId);
  if (valueR.length != 32) {
    throw new Error("Length of valueR does not match expected 32");
  }
  _out.writeBytes(valueR);
  if (valueS.length != 32) {
    throw new Error("Length of valueS does not match expected 32");
  }
  _out.writeBytes(valueS);
}

export function init(pooledTokens: BN, foundationMembers: Buffer[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI64(pooledTokens);
    _out.writeI32(foundationMembers.length);
    for (const foundationMembers_vec of foundationMembers) {
      if (foundationMembers_vec.length != 33) {
        throw new Error("Length of foundationMembers_vec does not match expected 33");
      }
      _out.writeBytes(foundationMembers_vec);
    }
  });
}

export function signTransfer(pendingTransfers: PendingTransfer[], signatures: Signature[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeI32(pendingTransfers.length);
    for (const pendingTransfers_vec of pendingTransfers) {
      serializePendingTransfer(_out, pendingTransfers_vec);
    }
    _out.writeI32(signatures.length);
    for (const signatures_vec of signatures) {
      serializeSignature(_out, signatures_vec);
    }
  });
}

export function deserializeState(state: StateWithClient): FoundationContractState;
export function deserializeState(bytes: Buffer): FoundationContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): FoundationContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): FoundationContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new FoundationContract_SDK_13_3_0(client, address).deserializeFoundationContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new FoundationContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeFoundationContractState(input);
  }
}

export type Action =
  | SignTransferAction;

export interface SignTransferAction {
  discriminant: "sign_transfer";
  pendingTransfers: PendingTransfer[];
  signatures: Signature[];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new FoundationContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeSignTransferAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  pooledTokens: BN;
  foundationMembers: Buffer[];
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new FoundationContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

