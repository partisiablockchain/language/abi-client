// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkStructOpen_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkStructOpen_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private SecretPosition deserializeSecretPosition(AbiInput _input) {
    byte x = _input.readI8();
    byte y = _input.readI8();
    return new SecretPosition(x, y);
  }
  private SecretResponse deserializeSecretResponse(AbiInput _input) {
    byte age = _input.readI8();
    short height = _input.readI16();
    SecretPosition position = deserializeSecretPosition(_input);
    BigInteger wealth = _input.readSignedBigInteger(16);
    return new SecretResponse(age, height, position, wealth);
  }
  private Position deserializePosition(AbiInput _input) {
    byte x = _input.readI8();
    byte y = _input.readI8();
    return new Position(x, y);
  }
  private Response deserializeResponse(AbiInput _input) {
    byte age = _input.readI8();
    short height = _input.readI16();
    Position position = deserializePosition(_input);
    BigInteger wealth = _input.readSignedBigInteger(16);
    return new Response(age, height, position, wealth);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    var responses_vecLength = _input.readI32();
    List<Response> responses = new ArrayList<>();
    for (int responses_i = 0; responses_i < responses_vecLength; responses_i++) {
      Response responses_elem = deserializeResponse(_input);
      responses.add(responses_elem);
    }
    return new ContractState(responses);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private ResetStateAction deserializeResetStateAction(AbiInput _input) {
    return new ResetStateAction();
  }

  private OpenInputAction deserializeOpenInputAction(AbiInput _input) {
    Response response = deserializeResponse(_input);
    return new OpenInputAction(response);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record SecretPosition(byte x, byte y) {
    public void serialize(AbiOutput _out) {
      _out.writeI8(x);
      _out.writeI8(y);
    }
  }

  @AbiGenerated
  public record SecretResponse(byte age, short height, SecretPosition position, BigInteger wealth) {
    public void serialize(AbiOutput _out) {
      _out.writeI8(age);
      _out.writeI16(height);
      position.serialize(_out);
      _out.writeSignedBigInteger(wealth, 16);
    }
  }

  @AbiGenerated
  public record Position(byte x, byte y) {
    public void serialize(AbiOutput _out) {
      _out.writeI8(x);
      _out.writeI8(y);
    }
  }

  @AbiGenerated
  public record Response(byte age, short height, Position position, BigInteger wealth) {
    public void serialize(AbiOutput _out) {
      _out.writeI8(age);
      _out.writeI16(height);
      position.serialize(_out);
      _out.writeSignedBigInteger(wealth, 16);
    }
  }

  @AbiGenerated
  public record ContractState(List<Response> responses) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkStructOpen_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] resetState() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("00"));
    });
  }

  public static byte[] openInput(Response response) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("10"));
      response.serialize(_out);
    });
  }

  public static SecretInputBuilder<SecretResponse> secretInput() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<SecretResponse, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkStructOpen_SDK_15_4_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ResetStateAction() implements Action {
  }
  @AbiGenerated
  public record OpenInputAction(Response response) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new ZkStructOpen_SDK_15_4_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeResetStateAction(input);
    } else if (shortname.equals("10")) {
      return contract.deserializeOpenInputAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkStructOpen_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
