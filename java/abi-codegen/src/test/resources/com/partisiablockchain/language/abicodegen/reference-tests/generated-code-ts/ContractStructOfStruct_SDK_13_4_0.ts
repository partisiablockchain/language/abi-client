// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractStructOfStruct_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeMyStructType(_input: AbiInput): MyStructType {
    const someValue: BN = _input.readU64();
    const someVector_vecLength = _input.readI32();
    const someVector: BN[] = [];
    for (let someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      const someVector_elem: BN = _input.readU64();
      someVector.push(someVector_elem);
    }
    return { someValue, someVector };
  }
  public deserializeMyOtherStructType(_input: AbiInput): MyOtherStructType {
    const value: BN = _input.readU64();
    const vector_vecLength = _input.readI32();
    const vector: MyStructType[] = [];
    for (let vector_i = 0; vector_i < vector_vecLength; vector_i++) {
      const vector_elem: MyStructType = this.deserializeMyStructType(_input);
      vector.push(vector_elem);
    }
    return { value, vector };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myOtherStruct: MyOtherStructType = this.deserializeMyOtherStructType(_input);
    return { myOtherStruct };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyOtherStructAction(_input: AbiInput): UpdateMyOtherStructAction {
    const value: MyOtherStructType = this.deserializeMyOtherStructType(_input);
    return { discriminant: "update_my_other_struct", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myOtherStruct: MyOtherStructType = this.deserializeMyOtherStructType(_input);
    return { discriminant: "initialize", myOtherStruct };
  }

}
export interface MyStructType {
  someValue: BN;
  someVector: BN[];
}
function serializeMyStructType(_out: AbiOutput, _value: MyStructType): void {
  const { someValue, someVector } = _value;
  _out.writeU64(someValue);
  _out.writeI32(someVector.length);
  for (const someVector_vec of someVector) {
    _out.writeU64(someVector_vec);
  }
}

export interface MyOtherStructType {
  value: BN;
  vector: MyStructType[];
}
function serializeMyOtherStructType(_out: AbiOutput, _value: MyOtherStructType): void {
  const { value, vector } = _value;
  _out.writeU64(value);
  _out.writeI32(vector.length);
  for (const vector_vec of vector) {
    serializeMyStructType(_out, vector_vec);
  }
}

export interface ExampleContractState {
  myOtherStruct: MyOtherStructType;
}

export function initialize(myOtherStruct: MyOtherStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeMyOtherStructType(_out, myOtherStruct);
  });
}

export function updateMyOtherStruct(value: MyOtherStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("85f292be0b", "hex"));
    serializeMyOtherStructType(_out, value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractStructOfStruct_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractStructOfStruct_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyOtherStructAction;

export interface UpdateMyOtherStructAction {
  discriminant: "update_my_other_struct";
  value: MyOtherStructType;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractStructOfStruct_SDK_13_4_0(undefined, undefined);
  if (shortname === "85f292be0b") {
    return contract.deserializeUpdateMyOtherStructAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myOtherStruct: MyOtherStructType;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractStructOfStruct_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

