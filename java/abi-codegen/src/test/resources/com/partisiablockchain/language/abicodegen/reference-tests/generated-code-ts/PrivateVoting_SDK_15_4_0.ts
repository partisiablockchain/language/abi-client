// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class PrivateVoting_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVoteResult(_input: AbiInput): VoteResult {
    const voteId: number = _input.readU32();
    const votesFor: number = _input.readU32();
    const votesAgainst: number = _input.readU32();
    let proof: Option<string> = undefined;
    const proof_isSome = _input.readBoolean();
    if (proof_isSome) {
      const proof_option: string = _input.readString();
      proof = proof_option;
    }
    return { voteId, votesFor, votesAgainst, proof };
  }
  public deserializeSecretVote(_input: AbiInput): SecretVote {
    const vote: boolean = _input.readBoolean();
    return { vote };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const currentVoteId: number = _input.readU32();
    const voteResults_vecLength = _input.readI32();
    const voteResults: VoteResult[] = [];
    for (let voteResults_i = 0; voteResults_i < voteResults_vecLength; voteResults_i++) {
      const voteResults_elem: VoteResult = this.deserializeVoteResult(_input);
      voteResults.push(voteResults_elem);
    }
    return { currentVoteId, voteResults };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeStartVoteCountingAction(_input: AbiInput): StartVoteCountingAction {
    return { discriminant: "start_vote_counting",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface VoteResult {
  voteId: number;
  votesFor: number;
  votesAgainst: number;
  proof: Option<string>;
}

export interface SecretVote {
  vote: boolean;
}
function serializeSecretVote(_out: AbiOutput, _value: SecretVote): void {
  const { vote } = _value;
  _out.writeBoolean(vote);
}

export interface ContractState {
  currentVoteId: number;
  voteResults: VoteResult[];
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function startVoteCounting(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function castVote(): SecretInputBuilder<SecretVote> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: SecretVote): CompactBitArray => AbiBitOutput.serialize((_out) => {
    serializeSecretVote(_out, secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new PrivateVoting_SDK_15_4_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new PrivateVoting_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | StartVoteCountingAction;

export interface StartVoteCountingAction {
  discriminant: "start_vote_counting";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new PrivateVoting_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeStartVoteCountingAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PrivateVoting_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

