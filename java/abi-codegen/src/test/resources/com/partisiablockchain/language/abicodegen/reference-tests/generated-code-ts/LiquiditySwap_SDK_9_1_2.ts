// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class LiquiditySwap_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeTokenPool(_input: AbiInput): TokenPool {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const pool: BN = _input.readU64();
    return { tokenAddress, pool };
  }
  public deserializeUserBalance(_input: AbiInput): UserBalance {
    const poolABalance: BN = _input.readU64();
    const poolBBalance: BN = _input.readU64();
    return { poolABalance, poolBBalance };
  }
  public deserializeLiquiditySwapContractState(_input: AbiInput): LiquiditySwapContractState {
    const contractOwner: BlockchainAddress = _input.readAddress();
    const tokenPoolA: TokenPool = this.deserializeTokenPool(_input);
    const tokenPoolB: TokenPool = this.deserializeTokenPool(_input);
    const swapConstant: BN = _input.readU64();
    const userBalances_mapLength = _input.readI32();
    const userBalances: Map<BlockchainAddress, UserBalance> = new Map();
    for (let userBalances_i = 0; userBalances_i < userBalances_mapLength; userBalances_i++) {
      const userBalances_key: BlockchainAddress = _input.readAddress();
      const userBalances_value: UserBalance = this.deserializeUserBalance(_input);
      userBalances.set(userBalances_key, userBalances_value);
    }
    const isClosed: boolean = _input.readBoolean();
    return { contractOwner, tokenPoolA, tokenPoolB, swapConstant, userBalances, isClosed };
  }
  public async getState(): Promise<LiquiditySwapContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeLiquiditySwapContractState(input);
  }

  public deserializeProvideLiquidityAction(_input: AbiInput): ProvideLiquidityAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const poolSize: BN = _input.readU64();
    return { discriminant: "provide_liquidity", tokenAddress, poolSize };
  }

  public deserializeDepositAction(_input: AbiInput): DepositAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "deposit", tokenAddress, amount };
  }

  public deserializeSwapAction(_input: AbiInput): SwapAction {
    const inputTokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "swap", inputTokenAddress, amount };
  }

  public deserializeWithdrawAction(_input: AbiInput): WithdrawAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "withdraw", tokenAddress, amount };
  }

  public deserializeClosePoolsAction(_input: AbiInput): ClosePoolsAction {
    return { discriminant: "close_pools",  };
  }

  public deserializeProvideLiquidityCallbackCallback(_input: AbiInput): ProvideLiquidityCallbackCallback {
    const token: number = _input.readU8();
    const poolSize: BN = _input.readU64();
    return { discriminant: "provide_liquidity_callback", token, poolSize };
  }

  public deserializeDepositCallbackCallback(_input: AbiInput): DepositCallbackCallback {
    const token: number = _input.readU8();
    const amount: BN = _input.readU64();
    return { discriminant: "deposit_callback", token, amount };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const tokenAAddress: BlockchainAddress = _input.readAddress();
    const tokenBAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "initialize", tokenAAddress, tokenBAddress };
  }

}
export interface TokenPool {
  tokenAddress: BlockchainAddress;
  pool: BN;
}

export interface UserBalance {
  poolABalance: BN;
  poolBBalance: BN;
}

export interface LiquiditySwapContractState {
  contractOwner: BlockchainAddress;
  tokenPoolA: TokenPool;
  tokenPoolB: TokenPool;
  swapConstant: BN;
  userBalances: Map<BlockchainAddress, UserBalance>;
  isClosed: boolean;
}

export function initialize(tokenAAddress: BlockchainAddress, tokenBAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(tokenAAddress);
    _out.writeAddress(tokenBAddress);
  });
}

export function provideLiquidity(tokenAddress: BlockchainAddress, poolSize: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeU64(poolSize);
  });
}

export function deposit(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeU64(amount);
  });
}

export function swap(inputTokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(inputTokenAddress);
    _out.writeU64(amount);
  });
}

export function withdraw(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeU64(amount);
  });
}

export function closePools(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function provideLiquidityCallback(token: number, poolSize: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("10", "hex"));
    _out.writeU8(token);
    _out.writeU64(poolSize);
  });
}

export function depositCallback(token: number, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("20", "hex"));
    _out.writeU8(token);
    _out.writeU64(amount);
  });
}

export function deserializeState(state: StateWithClient): LiquiditySwapContractState;
export function deserializeState(bytes: Buffer): LiquiditySwapContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): LiquiditySwapContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): LiquiditySwapContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new LiquiditySwap_SDK_9_1_2(client, address).deserializeLiquiditySwapContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new LiquiditySwap_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeLiquiditySwapContractState(input);
  }
}

export type Action =
  | ProvideLiquidityAction
  | DepositAction
  | SwapAction
  | WithdrawAction
  | ClosePoolsAction;

export interface ProvideLiquidityAction {
  discriminant: "provide_liquidity";
  tokenAddress: BlockchainAddress;
  poolSize: BN;
}
export interface DepositAction {
  discriminant: "deposit";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface SwapAction {
  discriminant: "swap";
  inputTokenAddress: BlockchainAddress;
  amount: BN;
}
export interface WithdrawAction {
  discriminant: "withdraw";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface ClosePoolsAction {
  discriminant: "close_pools";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_9_1_2(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeProvideLiquidityAction(input);
  } else if (shortname === "02") {
    return contract.deserializeDepositAction(input);
  } else if (shortname === "03") {
    return contract.deserializeSwapAction(input);
  } else if (shortname === "04") {
    return contract.deserializeWithdrawAction(input);
  } else if (shortname === "05") {
    return contract.deserializeClosePoolsAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | ProvideLiquidityCallbackCallback
  | DepositCallbackCallback;

export interface ProvideLiquidityCallbackCallback {
  discriminant: "provide_liquidity_callback";
  token: number;
  poolSize: BN;
}
export interface DepositCallbackCallback {
  discriminant: "deposit_callback";
  token: number;
  amount: BN;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_9_1_2(undefined, undefined);
  if (shortname === "10") {
    return contract.deserializeProvideLiquidityCallbackCallback(input);
  } else if (shortname === "20") {
    return contract.deserializeDepositCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  tokenAAddress: BlockchainAddress;
  tokenBAddress: BlockchainAddress;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

