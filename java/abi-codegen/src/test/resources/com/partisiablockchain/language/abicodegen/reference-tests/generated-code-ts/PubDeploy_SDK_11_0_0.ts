// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class PubDeploy_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializePubDeployContractState(_input: AbiInput): PubDeployContractState {
    const checkSignature: boolean = _input.readBoolean();
    const owner: BlockchainAddress = _input.readAddress();
    const bindingJar_vecLength = _input.readI32();
    const bindingJar: Buffer = _input.readBytes(bindingJar_vecLength);
    return { checkSignature, owner, bindingJar };
  }
  public async getState(): Promise<PubDeployContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializePubDeployContractState(input);
  }

  public deserializeDeployContractAction(_input: AbiInput): DeployContractAction {
    const contractJar_vecLength = _input.readI32();
    const contractJar: Buffer = _input.readBytes(contractJar_vecLength);
    const abi_vecLength = _input.readI32();
    const abi: Buffer = _input.readBytes(abi_vecLength);
    const initialization_vecLength = _input.readI32();
    const initialization: Buffer = _input.readBytes(initialization_vecLength);
    return { discriminant: "deploy_contract", contractJar, abi, initialization };
  }

  public deserializeUpdateBinderAction(_input: AbiInput): UpdateBinderAction {
    const bindingJar_vecLength = _input.readI32();
    const bindingJar: Buffer = _input.readBytes(bindingJar_vecLength);
    return { discriminant: "update_binder", bindingJar };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const checkSignature: boolean = _input.readBoolean();
    const bindingJar_vecLength = _input.readI32();
    const bindingJar: Buffer = _input.readBytes(bindingJar_vecLength);
    return { discriminant: "init", checkSignature, bindingJar };
  }

}
export interface PubDeployContractState {
  checkSignature: boolean;
  owner: BlockchainAddress;
  bindingJar: Buffer;
}

export function init(checkSignature: boolean, bindingJar: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeBoolean(checkSignature);
    _out.writeI32(bindingJar.length);
    _out.writeBytes(bindingJar);
  });
}

export function deployContract(contractJar: Buffer, abi: Buffer, initialization: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeI32(contractJar.length);
    _out.writeBytes(contractJar);
    _out.writeI32(abi.length);
    _out.writeBytes(abi);
    _out.writeI32(initialization.length);
    _out.writeBytes(initialization);
  });
}

export function updateBinder(bindingJar: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeI32(bindingJar.length);
    _out.writeBytes(bindingJar);
  });
}

export function deserializeState(state: StateWithClient): PubDeployContractState;
export function deserializeState(bytes: Buffer): PubDeployContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): PubDeployContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): PubDeployContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new PubDeploy_SDK_11_0_0(client, address).deserializePubDeployContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new PubDeploy_SDK_11_0_0(
      state.client,
      state.address
    ).deserializePubDeployContractState(input);
  }
}

export type Action =
  | DeployContractAction
  | UpdateBinderAction;

export interface DeployContractAction {
  discriminant: "deploy_contract";
  contractJar: Buffer;
  abi: Buffer;
  initialization: Buffer;
}
export interface UpdateBinderAction {
  discriminant: "update_binder";
  bindingJar: Buffer;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PubDeploy_SDK_11_0_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeDeployContractAction(input);
  } else if (shortname === "00") {
    return contract.deserializeUpdateBinderAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  checkSignature: boolean;
  bindingJar: Buffer;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PubDeploy_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

