// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class PingPong_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializePingPongState(_input: AbiInput): PingPongState {
    return {  };
  }
  public async getState(): Promise<PingPongState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializePingPongState(input);
  }

  public deserializePingAction(_input: AbiInput): PingAction {
    const to: BlockchainAddress = _input.readAddress();
    const callDepth: number = _input.readU32();
    const callWidth: number = _input.readU32();
    return { discriminant: "ping", to, callDepth, callWidth };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface PingPongState {
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function ping(to: BlockchainAddress, callDepth: number, callWidth: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(to);
    _out.writeU32(callDepth);
    _out.writeU32(callWidth);
  });
}

export function deserializeState(state: StateWithClient): PingPongState;
export function deserializeState(bytes: Buffer): PingPongState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): PingPongState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): PingPongState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new PingPong_SDK_15_4_0(client, address).deserializePingPongState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new PingPong_SDK_15_4_0(
      state.client,
      state.address
    ).deserializePingPongState(input);
  }
}

export type Action =
  | PingAction;

export interface PingAction {
  discriminant: "ping";
  to: BlockchainAddress;
  callDepth: number;
  callWidth: number;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PingPong_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializePingAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new PingPong_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

