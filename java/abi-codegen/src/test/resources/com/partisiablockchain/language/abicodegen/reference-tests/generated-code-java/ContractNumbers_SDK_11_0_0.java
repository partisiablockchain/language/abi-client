// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractNumbers_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractNumbers_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    byte myU8 = _input.readU8();
    short myU16 = _input.readU16();
    int myU32 = _input.readU32();
    long myU64 = _input.readU64();
    byte myI8 = _input.readI8();
    short myI16 = _input.readI16();
    int myI32 = _input.readI32();
    long myI64 = _input.readI64();
    return new ExampleContractState(myU8, myU16, myU32, myU64, myI8, myI16, myI32, myI64);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyU8Action deserializeUpdateMyU8Action(AbiInput _input) {
    byte value = _input.readU8();
    return new UpdateMyU8Action(value);
  }

  private UpdateMyU16Action deserializeUpdateMyU16Action(AbiInput _input) {
    short value = _input.readU16();
    return new UpdateMyU16Action(value);
  }

  private UpdateMyU32Action deserializeUpdateMyU32Action(AbiInput _input) {
    int value = _input.readU32();
    return new UpdateMyU32Action(value);
  }

  private UpdateMyU64Action deserializeUpdateMyU64Action(AbiInput _input) {
    long value = _input.readU64();
    return new UpdateMyU64Action(value);
  }

  private UpdateMyI8Action deserializeUpdateMyI8Action(AbiInput _input) {
    byte value = _input.readI8();
    return new UpdateMyI8Action(value);
  }

  private UpdateMyI16Action deserializeUpdateMyI16Action(AbiInput _input) {
    short value = _input.readI16();
    return new UpdateMyI16Action(value);
  }

  private UpdateMyI32Action deserializeUpdateMyI32Action(AbiInput _input) {
    int value = _input.readI32();
    return new UpdateMyI32Action(value);
  }

  private UpdateMyI64Action deserializeUpdateMyI64Action(AbiInput _input) {
    long value = _input.readI64();
    return new UpdateMyI64Action(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte myU8 = _input.readU8();
    short myU16 = _input.readU16();
    int myU32 = _input.readU32();
    long myU64 = _input.readU64();
    byte myI8 = _input.readI8();
    short myI16 = _input.readI16();
    int myI32 = _input.readI32();
    long myI64 = _input.readI64();
    return new InitializeInit(myU8, myU16, myU32, myU64, myI8, myI16, myI32, myI64);
  }


  @AbiGenerated
  public record ExampleContractState(byte myU8, short myU16, int myU32, long myU64, byte myI8, short myI16, int myI32, long myI64) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractNumbers_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte myU8, short myU16, int myU32, long myU64, byte myI8, short myI16, int myI32, long myI64) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(myU8);
      _out.writeU16(myU16);
      _out.writeU32(myU32);
      _out.writeU64(myU64);
      _out.writeI8(myI8);
      _out.writeI16(myI16);
      _out.writeI32(myI32);
      _out.writeI64(myI64);
    });
  }

  public static byte[] updateMyU8(byte value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("80a1b19e07"));
      _out.writeU8(value);
    });
  }

  public static byte[] updateMyU16(short value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b2f8b59804"));
      _out.writeU16(value);
    });
  }

  public static byte[] updateMyU32(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("efe5ed810e"));
      _out.writeU32(value);
    });
  }

  public static byte[] updateMyU64(long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("e4f68de503"));
      _out.writeU64(value);
    });
  }

  public static byte[] updateMyI8(byte value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f495f18c0a"));
      _out.writeI8(value);
    });
  }

  public static byte[] updateMyI16(short value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b9dadd8e0e"));
      _out.writeI16(value);
    });
  }

  public static byte[] updateMyI32(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b3cc80960a"));
      _out.writeI32(value);
    });
  }

  public static byte[] updateMyI64(long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("adc6ccdc05"));
      _out.writeI64(value);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractNumbers_SDK_11_0_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyU8Action(byte value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU16Action(short value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU32Action(int value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU64Action(long value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI8Action(byte value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI16Action(short value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI32Action(int value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI64Action(long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNumbers_SDK_11_0_0(null, null);
    if (shortname.equals("80a1b19e07")) {
      return contract.deserializeUpdateMyU8Action(input);
    } else if (shortname.equals("b2f8b59804")) {
      return contract.deserializeUpdateMyU16Action(input);
    } else if (shortname.equals("efe5ed810e")) {
      return contract.deserializeUpdateMyU32Action(input);
    } else if (shortname.equals("e4f68de503")) {
      return contract.deserializeUpdateMyU64Action(input);
    } else if (shortname.equals("f495f18c0a")) {
      return contract.deserializeUpdateMyI8Action(input);
    } else if (shortname.equals("b9dadd8e0e")) {
      return contract.deserializeUpdateMyI16Action(input);
    } else if (shortname.equals("b3cc80960a")) {
      return contract.deserializeUpdateMyI32Action(input);
    } else if (shortname.equals("adc6ccdc05")) {
      return contract.deserializeUpdateMyI64Action(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte myU8, short myU16, int myU32, long myU64, byte myI8, short myI16, int myI32, long myI64) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNumbers_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
