// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class SecretVoting_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public SecretVoting_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VoteBasis deserializeVoteBasis(AbiInput _input) {
    Fraction requiredRatio = deserializeFraction(_input);
    boolean absentAsAgainst = _input.readBoolean();
    return new VoteBasis(requiredRatio, absentAsAgainst);
  }
  private VoteResult deserializeVoteResult(AbiInput _input) {
    int votesFor = _input.readU32();
    int votesAgainst = _input.readU32();
    boolean passed = _input.readBoolean();
    return new VoteResult(votesFor, votesAgainst, passed);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress administrator = _input.readAddress();
    long deadlineVotingTime = _input.readI64();
    long deadlineCommitmentTime = _input.readI64();
    var allowedVoters_vecLength = _input.readI32();
    List<BlockchainAddress> allowedVoters = new ArrayList<>();
    for (int allowedVoters_i = 0; allowedVoters_i < allowedVoters_vecLength; allowedVoters_i++) {
      BlockchainAddress allowedVoters_elem = _input.readAddress();
      allowedVoters.add(allowedVoters_elem);
    }
    VoteBasis voteDefinition = deserializeVoteBasis(_input);
    VoteResult voteResult = null;
    var voteResult_isSome = _input.readBoolean();
    if (voteResult_isSome) {
      VoteResult voteResult_option = deserializeVoteResult(_input);
      voteResult = voteResult_option;
    }
    return new ContractState(administrator, deadlineVotingTime, deadlineCommitmentTime, allowedVoters, voteDefinition, voteResult);
  }
  private Fraction deserializeFraction(AbiInput _input) {
    int numerator = _input.readU32();
    int denominator = _input.readU32();
    return new Fraction(numerator, denominator);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private StartVoteCountingAction deserializeStartVoteCountingAction(AbiInput _input) {
    return new StartVoteCountingAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    int votingDurationMs = _input.readU32();
    var allowedVoters_vecLength = _input.readI32();
    List<BlockchainAddress> allowedVoters = new ArrayList<>();
    for (int allowedVoters_i = 0; allowedVoters_i < allowedVoters_vecLength; allowedVoters_i++) {
      BlockchainAddress allowedVoters_elem = _input.readAddress();
      allowedVoters.add(allowedVoters_elem);
    }
    VoteBasis voteDefinition = deserializeVoteBasis(_input);
    return new InitializeInit(votingDurationMs, allowedVoters, voteDefinition);
  }


  @AbiGenerated
  public record VoteBasis(Fraction requiredRatio, boolean absentAsAgainst) {
    public void serialize(AbiOutput _out) {
      requiredRatio.serialize(_out);
      _out.writeBoolean(absentAsAgainst);
    }
  }

  @AbiGenerated
  public record VoteResult(int votesFor, int votesAgainst, boolean passed) {
  }

  @AbiGenerated
  public record ContractState(BlockchainAddress administrator, long deadlineVotingTime, long deadlineCommitmentTime, List<BlockchainAddress> allowedVoters, VoteBasis voteDefinition, VoteResult voteResult) {
    public static ContractState deserialize(byte[] bytes) {
      return SecretVoting_SDK_11_0_0.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Fraction(int numerator, int denominator) {
    public void serialize(AbiOutput _out) {
      _out.writeU32(numerator);
      _out.writeU32(denominator);
    }
  }

  public static byte[] initialize(int votingDurationMs, List<BlockchainAddress> allowedVoters, VoteBasis voteDefinition) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU32(votingDurationMs);
      _out.writeI32(allowedVoters.size());
      for (BlockchainAddress allowedVoters_vec : allowedVoters) {
        _out.writeAddress(allowedVoters_vec);
      }
      voteDefinition.serialize(_out);
    });
  }

  public static byte[] startVoteCounting() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<Integer> addVote() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new SecretVoting_SDK_11_0_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record StartVoteCountingAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new SecretVoting_SDK_11_0_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeStartVoteCountingAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(int votingDurationMs, List<BlockchainAddress> allowedVoters, VoteBasis voteDefinition) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new SecretVoting_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
