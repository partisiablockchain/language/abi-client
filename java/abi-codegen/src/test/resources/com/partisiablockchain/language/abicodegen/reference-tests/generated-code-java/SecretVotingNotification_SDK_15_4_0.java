// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class SecretVotingNotification_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public SecretVotingNotification_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VoteResult deserializeVoteResult(AbiInput _input) {
    int votesFor = _input.readU32();
    int votesAgainst = _input.readU32();
    boolean passed = _input.readBoolean();
    return new VoteResult(votesFor, votesAgainst, passed);
  }
  private Notification deserializeNotification(AbiInput _input) {
    BlockchainAddress contract = _input.readAddress();
    NotificationPayload payload = deserializeNotificationPayload(_input);
    return new Notification(contract, payload);
  }
  private NotificationPayload deserializeNotificationPayload(AbiInput _input) {
    int shortname = _input.readU32();
    var bytes_vecLength = _input.readI32();
    byte[] bytes = _input.readBytes(bytes_vecLength);
    return new NotificationPayload(shortname, bytes);
  }
  private VotingContractState deserializeVotingContractState(AbiInput _input) {
    BlockchainAddress owner = _input.readAddress();
    VoteResult voteResult = null;
    var voteResult_isSome = _input.readBoolean();
    if (voteResult_isSome) {
      VoteResult voteResult_option = deserializeVoteResult(_input);
      voteResult = voteResult_option;
    }
    Notification passNotification = null;
    var passNotification_isSome = _input.readBoolean();
    if (passNotification_isSome) {
      Notification passNotification_option = deserializeNotification(_input);
      passNotification = passNotification_option;
    }
    return new VotingContractState(owner, voteResult, passNotification);
  }
  public VotingContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeVotingContractState(input);
  }

  private StartVoteCountingAction deserializeStartVoteCountingAction(AbiInput _input) {
    return new StartVoteCountingAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    Notification passNotification = null;
    var passNotification_isSome = _input.readBoolean();
    if (passNotification_isSome) {
      Notification passNotification_option = deserializeNotification(_input);
      passNotification = passNotification_option;
    }
    return new InitializeInit(passNotification);
  }


  @AbiGenerated
  public record VoteResult(int votesFor, int votesAgainst, boolean passed) {
  }

  @AbiGenerated
  public record Notification(BlockchainAddress contract, NotificationPayload payload) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(contract);
      payload.serialize(_out);
    }
  }

  @AbiGenerated
  public record NotificationPayload(int shortname, byte[] bytes) {
    public void serialize(AbiOutput _out) {
      _out.writeU32(shortname);
      _out.writeI32(bytes.length);
      _out.writeBytes(bytes);
    }
  }

  @AbiGenerated
  public record VotingContractState(BlockchainAddress owner, VoteResult voteResult, Notification passNotification) {
    public static VotingContractState deserialize(byte[] bytes) {
      return SecretVotingNotification_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(Notification passNotification) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeBoolean(passNotification != null);
      if (passNotification != null) {
        passNotification.serialize(_out);
      }
    });
  }

  public static byte[] startVoteCounting() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<Boolean> addVote() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<Boolean, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeBoolean(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static VotingContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new SecretVotingNotification_SDK_15_4_0(client, address).deserializeVotingContractState(input);
  }
  
  public static VotingContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static VotingContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record StartVoteCountingAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new SecretVotingNotification_SDK_15_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeStartVoteCountingAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(Notification passNotification) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new SecretVotingNotification_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
