// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractAddress_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractAddress_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    BlockchainAddress myAddress = _input.readAddress();
    return new ExampleContractState(myAddress);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyAddressAction deserializeUpdateMyAddressAction(AbiInput _input) {
    BlockchainAddress value = _input.readAddress();
    return new UpdateMyAddressAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress myAddress = _input.readAddress();
    return new InitializeInit(myAddress);
  }


  @AbiGenerated
  public record ExampleContractState(BlockchainAddress myAddress) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractAddress_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BlockchainAddress myAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(myAddress);
    });
  }

  public static byte[] updateMyAddress(BlockchainAddress value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("a7e192a009"));
      _out.writeAddress(value);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractAddress_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyAddressAction(BlockchainAddress value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractAddress_SDK_10_1_0(null, null);
    if (shortname.equals("a7e192a009")) {
      return contract.deserializeUpdateMyAddressAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress myAddress) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractAddress_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
