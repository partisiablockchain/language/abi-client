// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ByocFaucet_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ByocFaucet_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private FaucetContractState deserializeFaucetContractState(AbiInput _input) {
    BlockchainAddress faucetAddress = _input.readAddress();
    return new FaucetContractState(faucetAddress);
  }
  public FaucetContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeFaucetContractState(input);
  }

  private MintGasAction deserializeMintGasAction(AbiInput _input) {
    BlockchainAddress receiver = _input.readAddress();
    return new MintGasAction(receiver);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress faucetAddress = _input.readAddress();
    return new InitializeInit(faucetAddress);
  }


  @AbiGenerated
  public record FaucetContractState(BlockchainAddress faucetAddress) {
    public static FaucetContractState deserialize(byte[] bytes) {
      return ByocFaucet_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BlockchainAddress faucetAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(faucetAddress);
    });
  }

  public static byte[] mintGas(BlockchainAddress receiver) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(receiver);
    });
  }

  public static FaucetContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ByocFaucet_SDK_10_1_0(client, address).deserializeFaucetContractState(input);
  }
  
  public static FaucetContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static FaucetContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record MintGasAction(BlockchainAddress receiver) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocFaucet_SDK_10_1_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeMintGasAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress faucetAddress) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocFaucet_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
