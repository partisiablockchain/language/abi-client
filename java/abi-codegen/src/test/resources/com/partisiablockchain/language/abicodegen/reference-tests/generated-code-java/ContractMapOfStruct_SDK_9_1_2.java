// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractMapOfStruct_SDK_9_1_2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractMapOfStruct_SDK_9_1_2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MyStructType deserializeMyStructType(AbiInput _input) {
    long someValue = _input.readU64();
    var someVector_vecLength = _input.readI32();
    List<Long> someVector = new ArrayList<>();
    for (int someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      long someVector_elem = _input.readU64();
      someVector.add(someVector_elem);
    }
    return new MyStructType(someValue, someVector);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    var myMap_mapLength = _input.readI32();
    Map<String, MyStructType> myMap = new HashMap<>();
    for (int myMap_i = 0; myMap_i < myMap_mapLength; myMap_i++) {
      String myMap_key = _input.readString();
      MyStructType myMap_value = deserializeMyStructType(_input);
      myMap.put(myMap_key, myMap_value);
    }
    return new ExampleContractState(myMap);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private InsertInMyMapAction deserializeInsertInMyMapAction(AbiInput _input) {
    String key = _input.readString();
    MyStructType value = deserializeMyStructType(_input);
    return new InsertInMyMapAction(key, value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record MyStructType(long someValue, List<Long> someVector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(someValue);
      _out.writeI32(someVector.size());
      for (long someVector_vec : someVector) {
        _out.writeU64(someVector_vec);
      }
    }
  }

  @AbiGenerated
  public record ExampleContractState(Map<String, MyStructType> myMap) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractMapOfStruct_SDK_9_1_2.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] insertInMyMap(String key, MyStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("fc91b4c102"));
      _out.writeString(key);
      value.serialize(_out);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractMapOfStruct_SDK_9_1_2(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record InsertInMyMapAction(String key, MyStructType value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractMapOfStruct_SDK_9_1_2(null, null);
    if (shortname.equals("fc91b4c102")) {
      return contract.deserializeInsertInMyMapAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractMapOfStruct_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
