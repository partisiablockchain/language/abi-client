// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkRandomness_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeRandomnessContractState(_input: AbiInput): RandomnessContractState {
    const requiredNoOfInputs: number = _input.readU8();
    const generatedRandoms_vecLength = _input.readI32();
    const generatedRandoms: BN[] = [];
    for (let generatedRandoms_i = 0; generatedRandoms_i < generatedRandoms_vecLength; generatedRandoms_i++) {
      const generatedRandoms_elem: BN = _input.readSignedBigInteger(16);
      generatedRandoms.push(generatedRandoms_elem);
    }
    return { requiredNoOfInputs, generatedRandoms };
  }
  public async getState(): Promise<RandomnessContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeRandomnessContractState(input);
  }

  public deserializeGenerateRandomBytesAction(_input: AbiInput): GenerateRandomBytesAction {
    return { discriminant: "generate_random_bytes",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const requiredNoOfInputs: number = _input.readU8();
    return { discriminant: "initialize", requiredNoOfInputs };
  }

}
export interface RandomnessContractState {
  requiredNoOfInputs: number;
  generatedRandoms: BN[];
}

export function initialize(requiredNoOfInputs: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU8(requiredNoOfInputs);
  });
}

export function generateRandomBytes(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function inputEntropy(): SecretInputBuilder<BN> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: BN): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeSignedBigInteger(secret_input_lambda, 16);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): RandomnessContractState;
export function deserializeState(bytes: Buffer): RandomnessContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): RandomnessContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): RandomnessContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkRandomness_SDK_15_4_0(client, address).deserializeRandomnessContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkRandomness_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeRandomnessContractState(input);
  }
}

export type Action =
  | GenerateRandomBytesAction;

export interface GenerateRandomBytesAction {
  discriminant: "generate_random_bytes";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new ZkRandomness_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeGenerateRandomBytesAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  requiredNoOfInputs: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkRandomness_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

