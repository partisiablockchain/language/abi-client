// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractAllTypesNoDecls_SDK_10_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeMyStructType(_input: AbiInput): MyStructType {
    const someValue: BN = _input.readU64();
    const someVector_vecLength = _input.readI32();
    const someVector: BN[] = [];
    for (let someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      const someVector_elem: BN = _input.readU64();
      someVector.push(someVector_elem);
    }
    return { someValue, someVector };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myU8: number = _input.readU8();
    const myU16: number = _input.readU16();
    const myU32: number = _input.readU32();
    const myU64: BN = _input.readU64();
    const myU128: BN = _input.readUnsignedBigInteger(16);
    const myI8: number = _input.readI8();
    const myI16: number = _input.readI16();
    const myI32: number = _input.readI32();
    const myI64: BN = _input.readI64();
    const myI128: BN = _input.readSignedBigInteger(16);
    const myString: string = _input.readString();
    const myBool: boolean = _input.readBoolean();
    const myAddress: BlockchainAddress = _input.readAddress();
    const myVec_vecLength = _input.readI32();
    const myVec: BN[] = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      const myVec_elem: BN = _input.readU64();
      myVec.push(myVec_elem);
    }
    const myVecVec_vecLength = _input.readI32();
    const myVecVec: BN[][] = [];
    for (let myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      const myVecVec_elem_vecLength = _input.readI32();
      const myVecVec_elem: BN[] = [];
      for (let myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        const myVecVec_elem_elem: BN = _input.readU64();
        myVecVec_elem.push(myVecVec_elem_elem);
      }
      myVecVec.push(myVecVec_elem);
    }
    const myMap_mapLength = _input.readI32();
    const myMap: Map<BlockchainAddress, number> = new Map();
    for (let myMap_i = 0; myMap_i < myMap_mapLength; myMap_i++) {
      const myMap_key: BlockchainAddress = _input.readAddress();
      const myMap_value: number = _input.readU8();
      myMap.set(myMap_key, myMap_value);
    }
    const mySet_setLength = _input.readI32();
    const mySet: BN[] = [];
    for (let mySet_i = 0; mySet_i < mySet_setLength; mySet_i++) {
      const mySet_elem: BN = _input.readU64();
      mySet.push(mySet_elem);
    }
    const myComplexMap_mapLength = _input.readI32();
    const myComplexMap: Map<BN, BN[]> = new Map();
    for (let myComplexMap_i = 0; myComplexMap_i < myComplexMap_mapLength; myComplexMap_i++) {
      const myComplexMap_key: BN = _input.readU64();
      const myComplexMap_value_vecLength = _input.readI32();
      const myComplexMap_value: BN[] = [];
      for (let myComplexMap_value_i = 0; myComplexMap_value_i < myComplexMap_value_vecLength; myComplexMap_value_i++) {
        const myComplexMap_value_elem: BN = _input.readU64();
        myComplexMap_value.push(myComplexMap_value_elem);
      }
      myComplexMap.set(myComplexMap_key, myComplexMap_value);
    }
    const myArray: Buffer = _input.readBytes(16);
    const myArray2: Buffer = _input.readBytes(5);
    const myStruct: MyStructType = this.deserializeMyStructType(_input);
    return { myU8, myU16, myU32, myU64, myU128, myI8, myI16, myI32, myI64, myI128, myString, myBool, myAddress, myVec, myVecVec, myMap, mySet, myComplexMap, myArray, myArray2, myStruct };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeInsertInMySetAction(_input: AbiInput): InsertInMySetAction {
    const value: BN = _input.readU64();
    return { discriminant: "insert_in_my_set", value };
  }

  public deserializeInsertInMyMapAction(_input: AbiInput): InsertInMyMapAction {
    const address: BlockchainAddress = _input.readAddress();
    const value: number = _input.readU8();
    return { discriminant: "insert_in_my_map", address, value };
  }

  public deserializeInsertInMyComplexMapAction(_input: AbiInput): InsertInMyComplexMapAction {
    const key: BN = _input.readU64();
    const value_vecLength = _input.readI32();
    const value: BN[] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem: BN = _input.readU64();
      value.push(value_elem);
    }
    return { discriminant: "insert_in_my_complex_map", key, value };
  }

  public deserializeUpdateMyU8Action(_input: AbiInput): UpdateMyU8Action {
    const value: number = _input.readU8();
    return { discriminant: "update_my_u8", value };
  }

  public deserializeUpdateMyU16Action(_input: AbiInput): UpdateMyU16Action {
    const value: number = _input.readU16();
    return { discriminant: "update_my_u16", value };
  }

  public deserializeUpdateMyU32Action(_input: AbiInput): UpdateMyU32Action {
    const value: number = _input.readU32();
    return { discriminant: "update_my_u32", value };
  }

  public deserializeUpdateMyU64Action(_input: AbiInput): UpdateMyU64Action {
    const value: BN = _input.readU64();
    return { discriminant: "update_my_u64", value };
  }

  public deserializeUpdateMyU128Action(_input: AbiInput): UpdateMyU128Action {
    const value: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "update_my_u128", value };
  }

  public deserializeUpdateMyI8Action(_input: AbiInput): UpdateMyI8Action {
    const value: number = _input.readI8();
    return { discriminant: "update_my_i8", value };
  }

  public deserializeUpdateMyI16Action(_input: AbiInput): UpdateMyI16Action {
    const value: number = _input.readI16();
    return { discriminant: "update_my_i16", value };
  }

  public deserializeUpdateMyI32Action(_input: AbiInput): UpdateMyI32Action {
    const value: number = _input.readI32();
    return { discriminant: "update_my_i32", value };
  }

  public deserializeUpdateMyI64Action(_input: AbiInput): UpdateMyI64Action {
    const value: BN = _input.readI64();
    return { discriminant: "update_my_i64", value };
  }

  public deserializeUpdateMyI128Action(_input: AbiInput): UpdateMyI128Action {
    const value: BN = _input.readSignedBigInteger(16);
    return { discriminant: "update_my_i128", value };
  }

  public deserializeUpdateMyStringAction(_input: AbiInput): UpdateMyStringAction {
    const value: string = _input.readString();
    return { discriminant: "update_my_string", value };
  }

  public deserializeUpdateMyBoolAction(_input: AbiInput): UpdateMyBoolAction {
    const value: boolean = _input.readBoolean();
    return { discriminant: "update_my_bool", value };
  }

  public deserializeUpdateMyAddressAction(_input: AbiInput): UpdateMyAddressAction {
    const value: BlockchainAddress = _input.readAddress();
    return { discriminant: "update_my_address", value };
  }

  public deserializeUpdateMyVecAction(_input: AbiInput): UpdateMyVecAction {
    const value_vecLength = _input.readI32();
    const value: BN[] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem: BN = _input.readU64();
      value.push(value_elem);
    }
    return { discriminant: "update_my_vec", value };
  }

  public deserializeUpdateMyVecVecAction(_input: AbiInput): UpdateMyVecVecAction {
    const value_vecLength = _input.readI32();
    const value: BN[][] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem_vecLength = _input.readI32();
      const value_elem: BN[] = [];
      for (let value_elem_i = 0; value_elem_i < value_elem_vecLength; value_elem_i++) {
        const value_elem_elem: BN = _input.readU64();
        value_elem.push(value_elem_elem);
      }
      value.push(value_elem);
    }
    return { discriminant: "update_my_vec_vec", value };
  }

  public deserializeUpdateMyArrayAction(_input: AbiInput): UpdateMyArrayAction {
    const value: Buffer = _input.readBytes(16);
    return { discriminant: "update_my_array", value };
  }

  public deserializeUpdateMyArray2Action(_input: AbiInput): UpdateMyArray2Action {
    const value: Buffer = _input.readBytes(5);
    return { discriminant: "update_my_array_2", value };
  }

  public deserializeUpdateMyStructAction(_input: AbiInput): UpdateMyStructAction {
    const value: MyStructType = this.deserializeMyStructType(_input);
    return { discriminant: "update_my_struct", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myU8: number = _input.readU8();
    const myU16: number = _input.readU16();
    const myU32: number = _input.readU32();
    const myU64: BN = _input.readU64();
    const myU128: BN = _input.readUnsignedBigInteger(16);
    const myI8: number = _input.readI8();
    const myI16: number = _input.readI16();
    const myI32: number = _input.readI32();
    const myI64: BN = _input.readI64();
    const myI128: BN = _input.readSignedBigInteger(16);
    const myString: string = _input.readString();
    const myBool: boolean = _input.readBoolean();
    const myAddress: BlockchainAddress = _input.readAddress();
    const myVec_vecLength = _input.readI32();
    const myVec: BN[] = [];
    for (let myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      const myVec_elem: BN = _input.readU64();
      myVec.push(myVec_elem);
    }
    const myVecVec_vecLength = _input.readI32();
    const myVecVec: BN[][] = [];
    for (let myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      const myVecVec_elem_vecLength = _input.readI32();
      const myVecVec_elem: BN[] = [];
      for (let myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        const myVecVec_elem_elem: BN = _input.readU64();
        myVecVec_elem.push(myVecVec_elem_elem);
      }
      myVecVec.push(myVecVec_elem);
    }
    const myArray: Buffer = _input.readBytes(16);
    const myArray2: Buffer = _input.readBytes(5);
    const myStruct: MyStructType = this.deserializeMyStructType(_input);
    return { discriminant: "initialize", myU8, myU16, myU32, myU64, myU128, myI8, myI16, myI32, myI64, myI128, myString, myBool, myAddress, myVec, myVecVec, myArray, myArray2, myStruct };
  }

}
export interface MyStructType {
  someValue: BN;
  someVector: BN[];
}
function serializeMyStructType(_out: AbiOutput, _value: MyStructType): void {
  const { someValue, someVector } = _value;
  _out.writeU64(someValue);
  _out.writeI32(someVector.length);
  for (const someVector_vec of someVector) {
    _out.writeU64(someVector_vec);
  }
}

export interface ExampleContractState {
  myU8: number;
  myU16: number;
  myU32: number;
  myU64: BN;
  myU128: BN;
  myI8: number;
  myI16: number;
  myI32: number;
  myI64: BN;
  myI128: BN;
  myString: string;
  myBool: boolean;
  myAddress: BlockchainAddress;
  myVec: BN[];
  myVecVec: BN[][];
  myMap: Map<BlockchainAddress, number>;
  mySet: BN[];
  myComplexMap: Map<BN, BN[]>;
  myArray: Buffer;
  myArray2: Buffer;
  myStruct: MyStructType;
}

export function initialize(myU8: number, myU16: number, myU32: number, myU64: BN, myU128: BN, myI8: number, myI16: number, myI32: number, myI64: BN, myI128: BN, myString: string, myBool: boolean, myAddress: BlockchainAddress, myVec: BN[], myVecVec: BN[][], myArray: Buffer, myArray2: Buffer, myStruct: MyStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU8(myU8);
    _out.writeU16(myU16);
    _out.writeU32(myU32);
    _out.writeU64(myU64);
    _out.writeUnsignedBigInteger(myU128, 16);
    _out.writeI8(myI8);
    _out.writeI16(myI16);
    _out.writeI32(myI32);
    _out.writeI64(myI64);
    _out.writeSignedBigInteger(myI128, 16);
    _out.writeString(myString);
    _out.writeBoolean(myBool);
    _out.writeAddress(myAddress);
    _out.writeI32(myVec.length);
    for (const myVec_vec of myVec) {
      _out.writeU64(myVec_vec);
    }
    _out.writeI32(myVecVec.length);
    for (const myVecVec_vec of myVecVec) {
      _out.writeI32(myVecVec_vec.length);
      for (const myVecVec_vec_vec of myVecVec_vec) {
        _out.writeU64(myVecVec_vec_vec);
      }
    }
    if (myArray.length != 16) {
      throw new Error("Length of myArray does not match expected 16");
    }
    _out.writeBytes(myArray);
    if (myArray2.length != 5) {
      throw new Error("Length of myArray2 does not match expected 5");
    }
    _out.writeBytes(myArray2);
    serializeMyStructType(_out, myStruct);
  });
}

export function insertInMySet(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f5faeadf04", "hex"));
    _out.writeU64(value);
  });
}

export function insertInMyMap(address: BlockchainAddress, value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("fc91b4c102", "hex"));
    _out.writeAddress(address);
    _out.writeU8(value);
  });
}

export function insertInMyComplexMap(key: BN, value: BN[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("90b7e3f10c", "hex"));
    _out.writeU64(key);
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeU64(value_vec);
    }
  });
}

export function updateMyU8(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("80a1b19e07", "hex"));
    _out.writeU8(value);
  });
}

export function updateMyU16(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b2f8b59804", "hex"));
    _out.writeU16(value);
  });
}

export function updateMyU32(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("efe5ed810e", "hex"));
    _out.writeU32(value);
  });
}

export function updateMyU64(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("e4f68de503", "hex"));
    _out.writeU64(value);
  });
}

export function updateMyU128(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("8fc4c2a607", "hex"));
    _out.writeUnsignedBigInteger(value, 16);
  });
}

export function updateMyI8(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f495f18c0a", "hex"));
    _out.writeI8(value);
  });
}

export function updateMyI16(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b9dadd8e0e", "hex"));
    _out.writeI16(value);
  });
}

export function updateMyI32(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b3cc80960a", "hex"));
    _out.writeI32(value);
  });
}

export function updateMyI64(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("adc6ccdc05", "hex"));
    _out.writeI64(value);
  });
}

export function updateMyI128(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("8ce7ebef06", "hex"));
    _out.writeSignedBigInteger(value, 16);
  });
}

export function updateMyString(value: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("97c18ca406", "hex"));
    _out.writeString(value);
  });
}

export function updateMyBool(value: boolean): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b0a1fab30c", "hex"));
    _out.writeBoolean(value);
  });
}

export function updateMyAddress(value: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("a7e192a009", "hex"));
    _out.writeAddress(value);
  });
}

export function updateMyVec(value: BN[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("9d99e6e401", "hex"));
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeU64(value_vec);
    }
  });
}

export function updateMyVecVec(value: BN[][]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ccc0d6db04", "hex"));
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeI32(value_vec.length);
      for (const value_vec_vec of value_vec) {
        _out.writeU64(value_vec_vec);
      }
    }
  });
}

export function updateMyArray(value: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("cbe4cbfd0d", "hex"));
    if (value.length != 16) {
      throw new Error("Length of value does not match expected 16");
    }
    _out.writeBytes(value);
  });
}

export function updateMyArray2(value: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ec9da775", "hex"));
    if (value.length != 5) {
      throw new Error("Length of value does not match expected 5");
    }
    _out.writeBytes(value);
  });
}

export function updateMyStruct(value: MyStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f3b68ff40e", "hex"));
    serializeMyStructType(_out, value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractAllTypesNoDecls_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractAllTypesNoDecls_SDK_10_1_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | InsertInMySetAction
  | InsertInMyMapAction
  | InsertInMyComplexMapAction
  | UpdateMyU8Action
  | UpdateMyU16Action
  | UpdateMyU32Action
  | UpdateMyU64Action
  | UpdateMyU128Action
  | UpdateMyI8Action
  | UpdateMyI16Action
  | UpdateMyI32Action
  | UpdateMyI64Action
  | UpdateMyI128Action
  | UpdateMyStringAction
  | UpdateMyBoolAction
  | UpdateMyAddressAction
  | UpdateMyVecAction
  | UpdateMyVecVecAction
  | UpdateMyArrayAction
  | UpdateMyArray2Action
  | UpdateMyStructAction;

export interface InsertInMySetAction {
  discriminant: "insert_in_my_set";
  value: BN;
}
export interface InsertInMyMapAction {
  discriminant: "insert_in_my_map";
  address: BlockchainAddress;
  value: number;
}
export interface InsertInMyComplexMapAction {
  discriminant: "insert_in_my_complex_map";
  key: BN;
  value: BN[];
}
export interface UpdateMyU8Action {
  discriminant: "update_my_u8";
  value: number;
}
export interface UpdateMyU16Action {
  discriminant: "update_my_u16";
  value: number;
}
export interface UpdateMyU32Action {
  discriminant: "update_my_u32";
  value: number;
}
export interface UpdateMyU64Action {
  discriminant: "update_my_u64";
  value: BN;
}
export interface UpdateMyU128Action {
  discriminant: "update_my_u128";
  value: BN;
}
export interface UpdateMyI8Action {
  discriminant: "update_my_i8";
  value: number;
}
export interface UpdateMyI16Action {
  discriminant: "update_my_i16";
  value: number;
}
export interface UpdateMyI32Action {
  discriminant: "update_my_i32";
  value: number;
}
export interface UpdateMyI64Action {
  discriminant: "update_my_i64";
  value: BN;
}
export interface UpdateMyI128Action {
  discriminant: "update_my_i128";
  value: BN;
}
export interface UpdateMyStringAction {
  discriminant: "update_my_string";
  value: string;
}
export interface UpdateMyBoolAction {
  discriminant: "update_my_bool";
  value: boolean;
}
export interface UpdateMyAddressAction {
  discriminant: "update_my_address";
  value: BlockchainAddress;
}
export interface UpdateMyVecAction {
  discriminant: "update_my_vec";
  value: BN[];
}
export interface UpdateMyVecVecAction {
  discriminant: "update_my_vec_vec";
  value: BN[][];
}
export interface UpdateMyArrayAction {
  discriminant: "update_my_array";
  value: Buffer;
}
export interface UpdateMyArray2Action {
  discriminant: "update_my_array_2";
  value: Buffer;
}
export interface UpdateMyStructAction {
  discriminant: "update_my_struct";
  value: MyStructType;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractAllTypesNoDecls_SDK_10_1_0(undefined, undefined);
  if (shortname === "f5faeadf04") {
    return contract.deserializeInsertInMySetAction(input);
  } else if (shortname === "fc91b4c102") {
    return contract.deserializeInsertInMyMapAction(input);
  } else if (shortname === "90b7e3f10c") {
    return contract.deserializeInsertInMyComplexMapAction(input);
  } else if (shortname === "80a1b19e07") {
    return contract.deserializeUpdateMyU8Action(input);
  } else if (shortname === "b2f8b59804") {
    return contract.deserializeUpdateMyU16Action(input);
  } else if (shortname === "efe5ed810e") {
    return contract.deserializeUpdateMyU32Action(input);
  } else if (shortname === "e4f68de503") {
    return contract.deserializeUpdateMyU64Action(input);
  } else if (shortname === "8fc4c2a607") {
    return contract.deserializeUpdateMyU128Action(input);
  } else if (shortname === "f495f18c0a") {
    return contract.deserializeUpdateMyI8Action(input);
  } else if (shortname === "b9dadd8e0e") {
    return contract.deserializeUpdateMyI16Action(input);
  } else if (shortname === "b3cc80960a") {
    return contract.deserializeUpdateMyI32Action(input);
  } else if (shortname === "adc6ccdc05") {
    return contract.deserializeUpdateMyI64Action(input);
  } else if (shortname === "8ce7ebef06") {
    return contract.deserializeUpdateMyI128Action(input);
  } else if (shortname === "97c18ca406") {
    return contract.deserializeUpdateMyStringAction(input);
  } else if (shortname === "b0a1fab30c") {
    return contract.deserializeUpdateMyBoolAction(input);
  } else if (shortname === "a7e192a009") {
    return contract.deserializeUpdateMyAddressAction(input);
  } else if (shortname === "9d99e6e401") {
    return contract.deserializeUpdateMyVecAction(input);
  } else if (shortname === "ccc0d6db04") {
    return contract.deserializeUpdateMyVecVecAction(input);
  } else if (shortname === "cbe4cbfd0d") {
    return contract.deserializeUpdateMyArrayAction(input);
  } else if (shortname === "ec9da775") {
    return contract.deserializeUpdateMyArray2Action(input);
  } else if (shortname === "f3b68ff40e") {
    return contract.deserializeUpdateMyStructAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myU8: number;
  myU16: number;
  myU32: number;
  myU64: BN;
  myU128: BN;
  myI8: number;
  myI16: number;
  myI32: number;
  myI64: BN;
  myI128: BN;
  myString: string;
  myBool: boolean;
  myAddress: BlockchainAddress;
  myVec: BN[];
  myVecVec: BN[][];
  myArray: Buffer;
  myArray2: Buffer;
  myStruct: MyStructType;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractAllTypesNoDecls_SDK_10_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

