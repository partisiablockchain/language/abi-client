// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class Aes_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeAesKey(_input: AbiInput): AesKey {
    const key: BN = _input.readSignedBigInteger(16);
    return { key };
  }
  public deserializeAesPlainText(_input: AbiInput): AesPlainText {
    const plaintext: BN = _input.readSignedBigInteger(16);
    return { plaintext };
  }
  public deserializeAesContractState(_input: AbiInput): AesContractState {
    let aesKeyVariableId: Option<SecretVarId> = undefined;
    const aesKeyVariableId_isSome = _input.readBoolean();
    if (aesKeyVariableId_isSome) {
      const aesKeyVariableId_option: SecretVarId = this.deserializeSecretVarId(_input);
      aesKeyVariableId = aesKeyVariableId_option;
    }
    const permissionChangeKey: BlockchainAddress = _input.readAddress();
    return { aesKeyVariableId, permissionChangeKey };
  }
  public deserializeSecretVarId(_input: AbiInput): SecretVarId {
    const rawId: number = _input.readU32();
    return { rawId };
  }
  public async getState(): Promise<AesContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeAesContractState(input);
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const permissionChangeKey: BlockchainAddress = _input.readAddress();
    return { discriminant: "initialize", permissionChangeKey };
  }

}
export interface AesKey {
  key: BN;
}
function serializeAesKey(_out: AbiOutput, _value: AesKey): void {
  const { key } = _value;
  _out.writeSignedBigInteger(key, 16);
}

export interface AesPlainText {
  plaintext: BN;
}
function serializeAesPlainText(_out: AbiOutput, _value: AesPlainText): void {
  const { plaintext } = _value;
  _out.writeSignedBigInteger(plaintext, 16);
}

export interface AesContractState {
  aesKeyVariableId: Option<SecretVarId>;
  permissionChangeKey: BlockchainAddress;
}

export interface SecretVarId {
  rawId: number;
}

export function initialize(permissionChangeKey: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(permissionChangeKey);
  });
}

export function setAesKey(): SecretInputBuilder<AesKey> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: AesKey): CompactBitArray => AbiBitOutput.serialize((_out) => {
    serializeAesKey(_out, secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function encryptMessage(): SecretInputBuilder<AesPlainText> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("41", "hex"));
  });
  const _secretInput = (secret_input_lambda: AesPlainText): CompactBitArray => AbiBitOutput.serialize((_out) => {
    serializeAesPlainText(_out, secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): AesContractState;
export function deserializeState(bytes: Buffer): AesContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): AesContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): AesContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new Aes_SDK_15_4_0(client, address).deserializeAesContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new Aes_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeAesContractState(input);
  }
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  permissionChangeKey: BlockchainAddress;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new Aes_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

