// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class MpcToken_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeTransferInformation(_input: AbiInput): TransferInformation {
    const sender: BlockchainAddress = _input.readAddress();
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readI64();
    const symbol: string = _input.readString();
    return { sender, recipient, amount, symbol };
  }
  public deserializeMpcTokenContractState(_input: AbiInput): MpcTokenContractState {
    const transfers_mapLength = _input.readI32();
    const transfers: Map<Buffer, TransferInformation> = new Map();
    for (let transfers_i = 0; transfers_i < transfers_mapLength; transfers_i++) {
      const transfers_key: Buffer = _input.readBytes(32);
      const transfers_value: TransferInformation = this.deserializeTransferInformation(_input);
      transfers.set(transfers_key, transfers_value);
    }
    return { transfers };
  }
  public async getState(): Promise<MpcTokenContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeMpcTokenContractState(input);
  }

  public deserializeStakeTokensAction(_input: AbiInput): StakeTokensAction {
    const amount: BN = _input.readU64();
    return { discriminant: "stake_tokens", amount };
  }

  public deserializeUnstakeTokensAction(_input: AbiInput): UnstakeTokensAction {
    const amount: BN = _input.readU64();
    return { discriminant: "unstake_tokens", amount };
  }

  public deserializeDisassociateTokensAction(_input: AbiInput): DisassociateTokensAction {
    const address: BlockchainAddress = _input.readAddress();
    return { discriminant: "disassociate_tokens", address };
  }

  public deserializeTransferAction(_input: AbiInput): TransferAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "transfer", recipient, amount };
  }

  public deserializeRetryAction(_input: AbiInput): RetryAction {
    const transactionId: Buffer = _input.readBytes(32);
    return { discriminant: "retry", transactionId };
  }

  public deserializeCheckPendingUnstakesAction(_input: AbiInput): CheckPendingUnstakesAction {
    return { discriminant: "check_pending_unstakes",  };
  }

  public deserializeCheckVestedTokensAction(_input: AbiInput): CheckVestedTokensAction {
    return { discriminant: "check_vested_tokens",  };
  }

  public deserializeTransferSmallMemoAction(_input: AbiInput): TransferSmallMemoAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    const unused: BN = _input.readU64();
    return { discriminant: "transfer_small_memo", recipient, amount, unused };
  }

  public deserializeTransferLargeMemoAction(_input: AbiInput): TransferLargeMemoAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    const unused: string = _input.readString();
    return { discriminant: "transfer_large_memo", recipient, amount, unused };
  }

  public deserializeDelegateStakesAction(_input: AbiInput): DelegateStakesAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "delegate_stakes", recipient, amount };
  }

  public deserializeRetractDelegatedStakesAction(_input: AbiInput): RetractDelegatedStakesAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "retract_delegated_stakes", recipient, amount };
  }

  public deserializeRetryDelegateStakesAction(_input: AbiInput): RetryDelegateStakesAction {
    const transactionId: Buffer = _input.readBytes(32);
    return { discriminant: "retry_delegate_stakes", transactionId };
  }

  public deserializeAcceptDelegatedStakesAction(_input: AbiInput): AcceptDelegatedStakesAction {
    const sender: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "accept_delegated_stakes", sender, amount };
  }

  public deserializeReduceDelegatedStakesAction(_input: AbiInput): ReduceDelegatedStakesAction {
    const sender: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    return { discriminant: "reduce_delegated_stakes", sender, amount };
  }

  public deserializeByocTransferAction(_input: AbiInput): ByocTransferAction {
    const recipient: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    const symbol: string = _input.readString();
    return { discriminant: "byoc_transfer", recipient, amount, symbol };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const locked: number = _input.readU8();
    return { discriminant: "init", locked };
  }

}
export interface TransferInformation {
  sender: BlockchainAddress;
  recipient: BlockchainAddress;
  amount: BN;
  symbol: string;
}

export interface MpcTokenContractState {
  transfers: Map<Buffer, TransferInformation>;
}

export function init(locked: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU8(locked);
  });
}

export function stakeTokens(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeU64(amount);
  });
}

export function unstakeTokens(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU64(amount);
  });
}

export function disassociateTokens(address: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(address);
  });
}

export function transfer(recipient: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
  });
}

export function retry(transactionId: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    if (transactionId.length != 32) {
      throw new Error("Length of transactionId does not match expected 32");
    }
    _out.writeBytes(transactionId);
  });
}

export function checkPendingUnstakes(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function checkVestedTokens(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
  });
}

export function transferSmallMemo(recipient: BlockchainAddress, amount: BN, unused: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0d", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
    _out.writeU64(unused);
  });
}

export function transferLargeMemo(recipient: BlockchainAddress, amount: BN, unused: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("17", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
    _out.writeString(unused);
  });
}

export function delegateStakes(recipient: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("18", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
  });
}

export function retractDelegatedStakes(recipient: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("19", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
  });
}

export function retryDelegateStakes(transactionId: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("1a", "hex"));
    if (transactionId.length != 32) {
      throw new Error("Length of transactionId does not match expected 32");
    }
    _out.writeBytes(transactionId);
  });
}

export function acceptDelegatedStakes(sender: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("1b", "hex"));
    _out.writeAddress(sender);
    _out.writeU64(amount);
  });
}

export function reduceDelegatedStakes(sender: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("1c", "hex"));
    _out.writeAddress(sender);
    _out.writeU64(amount);
  });
}

export function byocTransfer(recipient: BlockchainAddress, amount: BN, symbol: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("1d", "hex"));
    _out.writeAddress(recipient);
    _out.writeU64(amount);
    _out.writeString(symbol);
  });
}

export function deserializeState(state: StateWithClient): MpcTokenContractState;
export function deserializeState(bytes: Buffer): MpcTokenContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): MpcTokenContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): MpcTokenContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new MpcToken_SDK_9_1_2(client, address).deserializeMpcTokenContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new MpcToken_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeMpcTokenContractState(input);
  }
}

export type Action =
  | StakeTokensAction
  | UnstakeTokensAction
  | DisassociateTokensAction
  | TransferAction
  | RetryAction
  | CheckPendingUnstakesAction
  | CheckVestedTokensAction
  | TransferSmallMemoAction
  | TransferLargeMemoAction
  | DelegateStakesAction
  | RetractDelegatedStakesAction
  | RetryDelegateStakesAction
  | AcceptDelegatedStakesAction
  | ReduceDelegatedStakesAction
  | ByocTransferAction;

export interface StakeTokensAction {
  discriminant: "stake_tokens";
  amount: BN;
}
export interface UnstakeTokensAction {
  discriminant: "unstake_tokens";
  amount: BN;
}
export interface DisassociateTokensAction {
  discriminant: "disassociate_tokens";
  address: BlockchainAddress;
}
export interface TransferAction {
  discriminant: "transfer";
  recipient: BlockchainAddress;
  amount: BN;
}
export interface RetryAction {
  discriminant: "retry";
  transactionId: Buffer;
}
export interface CheckPendingUnstakesAction {
  discriminant: "check_pending_unstakes";
}
export interface CheckVestedTokensAction {
  discriminant: "check_vested_tokens";
}
export interface TransferSmallMemoAction {
  discriminant: "transfer_small_memo";
  recipient: BlockchainAddress;
  amount: BN;
  unused: BN;
}
export interface TransferLargeMemoAction {
  discriminant: "transfer_large_memo";
  recipient: BlockchainAddress;
  amount: BN;
  unused: string;
}
export interface DelegateStakesAction {
  discriminant: "delegate_stakes";
  recipient: BlockchainAddress;
  amount: BN;
}
export interface RetractDelegatedStakesAction {
  discriminant: "retract_delegated_stakes";
  recipient: BlockchainAddress;
  amount: BN;
}
export interface RetryDelegateStakesAction {
  discriminant: "retry_delegate_stakes";
  transactionId: Buffer;
}
export interface AcceptDelegatedStakesAction {
  discriminant: "accept_delegated_stakes";
  sender: BlockchainAddress;
  amount: BN;
}
export interface ReduceDelegatedStakesAction {
  discriminant: "reduce_delegated_stakes";
  sender: BlockchainAddress;
  amount: BN;
}
export interface ByocTransferAction {
  discriminant: "byoc_transfer";
  recipient: BlockchainAddress;
  amount: BN;
  symbol: string;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new MpcToken_SDK_9_1_2(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeStakeTokensAction(input);
  } else if (shortname === "01") {
    return contract.deserializeUnstakeTokensAction(input);
  } else if (shortname === "02") {
    return contract.deserializeDisassociateTokensAction(input);
  } else if (shortname === "03") {
    return contract.deserializeTransferAction(input);
  } else if (shortname === "04") {
    return contract.deserializeRetryAction(input);
  } else if (shortname === "05") {
    return contract.deserializeCheckPendingUnstakesAction(input);
  } else if (shortname === "06") {
    return contract.deserializeCheckVestedTokensAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeTransferSmallMemoAction(input);
  } else if (shortname === "17") {
    return contract.deserializeTransferLargeMemoAction(input);
  } else if (shortname === "18") {
    return contract.deserializeDelegateStakesAction(input);
  } else if (shortname === "19") {
    return contract.deserializeRetractDelegatedStakesAction(input);
  } else if (shortname === "1a") {
    return contract.deserializeRetryDelegateStakesAction(input);
  } else if (shortname === "1b") {
    return contract.deserializeAcceptDelegatedStakesAction(input);
  } else if (shortname === "1c") {
    return contract.deserializeReduceDelegatedStakesAction(input);
  } else if (shortname === "1d") {
    return contract.deserializeByocTransferAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  locked: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new MpcToken_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

