// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class MpcToken_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public MpcToken_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private TransferInformation deserializeTransferInformation(AbiInput _input) {
    BlockchainAddress sender = _input.readAddress();
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readI64();
    String symbol = _input.readString();
    return new TransferInformation(sender, recipient, amount, symbol);
  }
  private MpcTokenContractState deserializeMpcTokenContractState(AbiInput _input) {
    var transfers_mapLength = _input.readI32();
    Map<byte[], TransferInformation> transfers = new HashMap<>();
    for (int transfers_i = 0; transfers_i < transfers_mapLength; transfers_i++) {
      byte[] transfers_key = _input.readBytes(32);
      TransferInformation transfers_value = deserializeTransferInformation(_input);
      transfers.put(transfers_key, transfers_value);
    }
    return new MpcTokenContractState(transfers);
  }
  public MpcTokenContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeMpcTokenContractState(input);
  }

  private StakeTokensAction deserializeStakeTokensAction(AbiInput _input) {
    long amount = _input.readU64();
    return new StakeTokensAction(amount);
  }

  private UnstakeTokensAction deserializeUnstakeTokensAction(AbiInput _input) {
    long amount = _input.readU64();
    return new UnstakeTokensAction(amount);
  }

  private DisassociateTokensAction deserializeDisassociateTokensAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    return new DisassociateTokensAction(address);
  }

  private TransferAction deserializeTransferAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    return new TransferAction(recipient, amount);
  }

  private AbortAction deserializeAbortAction(AbiInput _input) {
    byte[] transactionId = _input.readBytes(32);
    return new AbortAction(transactionId);
  }

  private CheckPendingUnstakesAction deserializeCheckPendingUnstakesAction(AbiInput _input) {
    return new CheckPendingUnstakesAction();
  }

  private CheckVestedTokensAction deserializeCheckVestedTokensAction(AbiInput _input) {
    return new CheckVestedTokensAction();
  }

  private TransferSmallMemoAction deserializeTransferSmallMemoAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    long unused = _input.readU64();
    return new TransferSmallMemoAction(recipient, amount, unused);
  }

  private TransferLargeMemoAction deserializeTransferLargeMemoAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    String unused = _input.readString();
    return new TransferLargeMemoAction(recipient, amount, unused);
  }

  private DelegateStakesAction deserializeDelegateStakesAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    return new DelegateStakesAction(recipient, amount);
  }

  private RetractDelegatedStakesAction deserializeRetractDelegatedStakesAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    return new RetractDelegatedStakesAction(recipient, amount);
  }

  private AbortDelegateStakesAction deserializeAbortDelegateStakesAction(AbiInput _input) {
    byte[] transactionId = _input.readBytes(32);
    return new AbortDelegateStakesAction(transactionId);
  }

  private AcceptDelegatedStakesAction deserializeAcceptDelegatedStakesAction(AbiInput _input) {
    BlockchainAddress sender = _input.readAddress();
    long amount = _input.readU64();
    return new AcceptDelegatedStakesAction(sender, amount);
  }

  private ReduceDelegatedStakesAction deserializeReduceDelegatedStakesAction(AbiInput _input) {
    BlockchainAddress sender = _input.readAddress();
    long amount = _input.readU64();
    return new ReduceDelegatedStakesAction(sender, amount);
  }

  private ByocTransferAction deserializeByocTransferAction(AbiInput _input) {
    BlockchainAddress recipient = _input.readAddress();
    long amount = _input.readU64();
    String symbol = _input.readString();
    return new ByocTransferAction(recipient, amount, symbol);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    byte locked = _input.readU8();
    return new InitInit(locked);
  }


  @AbiGenerated
  public record TransferInformation(BlockchainAddress sender, BlockchainAddress recipient, long amount, String symbol) {
  }

  @AbiGenerated
  public record MpcTokenContractState(Map<byte[], TransferInformation> transfers) {
    public static MpcTokenContractState deserialize(byte[] bytes) {
      return MpcToken_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] init(byte locked) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(locked);
    });
  }

  public static byte[] stakeTokens(long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeU64(amount);
    });
  }

  public static byte[] unstakeTokens(long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU64(amount);
    });
  }

  public static byte[] disassociateTokens(BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(address);
    });
  }

  public static byte[] transfer(BlockchainAddress recipient, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
    });
  }

  public static byte[] abort(byte[] transactionId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      if (transactionId.length != 32) {
        throw new RuntimeException("Length of transactionId does not match expected 32");
      }
      _out.writeBytes(transactionId);
    });
  }

  public static byte[] checkPendingUnstakes() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] checkVestedTokens() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static byte[] transferSmallMemo(BlockchainAddress recipient, long amount, long unused) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0d"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
      _out.writeU64(unused);
    });
  }

  public static byte[] transferLargeMemo(BlockchainAddress recipient, long amount, String unused) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("17"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
      _out.writeString(unused);
    });
  }

  public static byte[] delegateStakes(BlockchainAddress recipient, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("18"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
    });
  }

  public static byte[] retractDelegatedStakes(BlockchainAddress recipient, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("19"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
    });
  }

  public static byte[] abortDelegateStakes(byte[] transactionId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("1a"));
      if (transactionId.length != 32) {
        throw new RuntimeException("Length of transactionId does not match expected 32");
      }
      _out.writeBytes(transactionId);
    });
  }

  public static byte[] acceptDelegatedStakes(BlockchainAddress sender, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("1b"));
      _out.writeAddress(sender);
      _out.writeU64(amount);
    });
  }

  public static byte[] reduceDelegatedStakes(BlockchainAddress sender, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("1c"));
      _out.writeAddress(sender);
      _out.writeU64(amount);
    });
  }

  public static byte[] byocTransfer(BlockchainAddress recipient, long amount, String symbol) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("1d"));
      _out.writeAddress(recipient);
      _out.writeU64(amount);
      _out.writeString(symbol);
    });
  }

  public static MpcTokenContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new MpcToken_SDK_13_3_0(client, address).deserializeMpcTokenContractState(input);
  }
  
  public static MpcTokenContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static MpcTokenContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record StakeTokensAction(long amount) implements Action {
  }
  @AbiGenerated
  public record UnstakeTokensAction(long amount) implements Action {
  }
  @AbiGenerated
  public record DisassociateTokensAction(BlockchainAddress address) implements Action {
  }
  @AbiGenerated
  public record TransferAction(BlockchainAddress recipient, long amount) implements Action {
  }
  @AbiGenerated
  public record AbortAction(byte[] transactionId) implements Action {
  }
  @AbiGenerated
  public record CheckPendingUnstakesAction() implements Action {
  }
  @AbiGenerated
  public record CheckVestedTokensAction() implements Action {
  }
  @AbiGenerated
  public record TransferSmallMemoAction(BlockchainAddress recipient, long amount, long unused) implements Action {
  }
  @AbiGenerated
  public record TransferLargeMemoAction(BlockchainAddress recipient, long amount, String unused) implements Action {
  }
  @AbiGenerated
  public record DelegateStakesAction(BlockchainAddress recipient, long amount) implements Action {
  }
  @AbiGenerated
  public record RetractDelegatedStakesAction(BlockchainAddress recipient, long amount) implements Action {
  }
  @AbiGenerated
  public record AbortDelegateStakesAction(byte[] transactionId) implements Action {
  }
  @AbiGenerated
  public record AcceptDelegatedStakesAction(BlockchainAddress sender, long amount) implements Action {
  }
  @AbiGenerated
  public record ReduceDelegatedStakesAction(BlockchainAddress sender, long amount) implements Action {
  }
  @AbiGenerated
  public record ByocTransferAction(BlockchainAddress recipient, long amount, String symbol) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new MpcToken_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeStakeTokensAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeUnstakeTokensAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeDisassociateTokensAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeTransferAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeAbortAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeCheckPendingUnstakesAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeCheckVestedTokensAction(input);
    } else if (shortname.equals("0d")) {
      return contract.deserializeTransferSmallMemoAction(input);
    } else if (shortname.equals("17")) {
      return contract.deserializeTransferLargeMemoAction(input);
    } else if (shortname.equals("18")) {
      return contract.deserializeDelegateStakesAction(input);
    } else if (shortname.equals("19")) {
      return contract.deserializeRetractDelegatedStakesAction(input);
    } else if (shortname.equals("1a")) {
      return contract.deserializeAbortDelegateStakesAction(input);
    } else if (shortname.equals("1b")) {
      return contract.deserializeAcceptDelegatedStakesAction(input);
    } else if (shortname.equals("1c")) {
      return contract.deserializeReduceDelegatedStakesAction(input);
    } else if (shortname.equals("1d")) {
      return contract.deserializeByocTransferAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(byte locked) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new MpcToken_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
