// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkInputTesting_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkInputTesting_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    var openedInputs_vecLength = _input.readI32();
    List<Integer> openedInputs = new ArrayList<>();
    for (int openedInputs_i = 0; openedInputs_i < openedInputs_vecLength; openedInputs_i++) {
      int openedInputs_elem = _input.readI32();
      openedInputs.add(openedInputs_elem);
    }
    return new ContractState(openedInputs);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record ContractState(List<Integer> openedInputs) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkInputTesting_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static SecretInputBuilder<Integer> secretInput() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkInputTesting_SDK_13_3_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkInputTesting_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
