// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractEnum_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVehicle(_input: AbiInput): Vehicle {
    const discriminant = _input.readU8();
    if (discriminant === 2) {
      return this.deserializeVehicleBicycle(_input);
    } else if (discriminant === 5) {
      return this.deserializeVehicleCar(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeVehicleBicycle(_input: AbiInput): VehicleBicycle {
    const wheelDiameter: number = _input.readI32();
    return { discriminant: VehicleD.Bicycle, wheelDiameter };
  }
  public deserializeVehicleCar(_input: AbiInput): VehicleCar {
    const engineSize: number = _input.readU8();
    const supportsTrailer: boolean = _input.readBoolean();
    return { discriminant: VehicleD.Car, engineSize, supportsTrailer };
  }
  public deserializeEnumContractState(_input: AbiInput): EnumContractState {
    const myEnum: Vehicle = this.deserializeVehicle(_input);
    return { myEnum };
  }
  public async getState(): Promise<EnumContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeEnumContractState(input);
  }

  public deserializeUpdateEnumAction(_input: AbiInput): UpdateEnumAction {
    const val: Vehicle = this.deserializeVehicle(_input);
    return { discriminant: "update_enum", val };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myEnum: Vehicle = this.deserializeVehicle(_input);
    return { discriminant: "initialize", myEnum };
  }

}
export enum VehicleD {
  Bicycle = 2,
  Car = 5,
}
export type Vehicle =
  | VehicleBicycle
  | VehicleCar;
function serializeVehicle(out: AbiOutput, value: Vehicle): void {
  if (value.discriminant === VehicleD.Bicycle) {
    return serializeVehicleBicycle(out, value);
  } else if (value.discriminant === VehicleD.Car) {
    return serializeVehicleCar(out, value);
  }
}

export interface VehicleBicycle {
  discriminant: VehicleD.Bicycle;
  wheelDiameter: number;
}
function serializeVehicleBicycle(_out: AbiOutput, _value: VehicleBicycle): void {
  const {wheelDiameter} = _value;
  _out.writeU8(_value.discriminant);
  _out.writeI32(wheelDiameter);
}

export interface VehicleCar {
  discriminant: VehicleD.Car;
  engineSize: number;
  supportsTrailer: boolean;
}
function serializeVehicleCar(_out: AbiOutput, _value: VehicleCar): void {
  const {engineSize, supportsTrailer} = _value;
  _out.writeU8(_value.discriminant);
  _out.writeU8(engineSize);
  _out.writeBoolean(supportsTrailer);
}

export interface EnumContractState {
  myEnum: Vehicle;
}

export function initialize(myEnum: Vehicle): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeVehicle(_out, myEnum);
  });
}

export function updateEnum(val: Vehicle): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0d", "hex"));
    serializeVehicle(_out, val);
  });
}

export function deserializeState(state: StateWithClient): EnumContractState;
export function deserializeState(bytes: Buffer): EnumContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): EnumContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): EnumContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractEnum_SDK_13_3_0(client, address).deserializeEnumContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractEnum_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeEnumContractState(input);
  }
}

export type Action =
  | UpdateEnumAction;

export interface UpdateEnumAction {
  discriminant: "update_enum";
  val: Vehicle;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractEnum_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0d") {
    return contract.deserializeUpdateEnumAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myEnum: Vehicle;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractEnum_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

