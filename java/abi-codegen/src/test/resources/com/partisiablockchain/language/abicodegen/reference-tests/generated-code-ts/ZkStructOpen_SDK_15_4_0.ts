// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkStructOpen_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeSecretPosition(_input: AbiInput): SecretPosition {
    const x: number = _input.readI8();
    const y: number = _input.readI8();
    return { x, y };
  }
  public deserializeSecretResponse(_input: AbiInput): SecretResponse {
    const age: number = _input.readI8();
    const height: number = _input.readI16();
    const position: SecretPosition = this.deserializeSecretPosition(_input);
    const wealth: BN = _input.readSignedBigInteger(16);
    return { age, height, position, wealth };
  }
  public deserializePosition(_input: AbiInput): Position {
    const x: number = _input.readI8();
    const y: number = _input.readI8();
    return { x, y };
  }
  public deserializeResponse(_input: AbiInput): Response {
    const age: number = _input.readI8();
    const height: number = _input.readI16();
    const position: Position = this.deserializePosition(_input);
    const wealth: BN = _input.readSignedBigInteger(16);
    return { age, height, position, wealth };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const responses_vecLength = _input.readI32();
    const responses: Response[] = [];
    for (let responses_i = 0; responses_i < responses_vecLength; responses_i++) {
      const responses_elem: Response = this.deserializeResponse(_input);
      responses.push(responses_elem);
    }
    return { responses };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeResetStateAction(_input: AbiInput): ResetStateAction {
    return { discriminant: "reset_state",  };
  }

  public deserializeOpenInputAction(_input: AbiInput): OpenInputAction {
    const response: Response = this.deserializeResponse(_input);
    return { discriminant: "open_input", response };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface SecretPosition {
  x: number;
  y: number;
}
function serializeSecretPosition(_out: AbiOutput, _value: SecretPosition): void {
  const { x, y } = _value;
  _out.writeI8(x);
  _out.writeI8(y);
}

export interface SecretResponse {
  age: number;
  height: number;
  position: SecretPosition;
  wealth: BN;
}
function serializeSecretResponse(_out: AbiOutput, _value: SecretResponse): void {
  const { age, height, position, wealth } = _value;
  _out.writeI8(age);
  _out.writeI16(height);
  serializeSecretPosition(_out, position);
  _out.writeSignedBigInteger(wealth, 16);
}

export interface Position {
  x: number;
  y: number;
}
function serializePosition(_out: AbiOutput, _value: Position): void {
  const { x, y } = _value;
  _out.writeI8(x);
  _out.writeI8(y);
}

export interface Response {
  age: number;
  height: number;
  position: Position;
  wealth: BN;
}
function serializeResponse(_out: AbiOutput, _value: Response): void {
  const { age, height, position, wealth } = _value;
  _out.writeI8(age);
  _out.writeI16(height);
  serializePosition(_out, position);
  _out.writeSignedBigInteger(wealth, 16);
}

export interface ContractState {
  responses: Response[];
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function resetState(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("00", "hex"));
  });
}

export function openInput(response: Response): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("10", "hex"));
    serializeResponse(_out, response);
  });
}

export function secretInput(): SecretInputBuilder<SecretResponse> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: SecretResponse): CompactBitArray => AbiBitOutput.serialize((_out) => {
    serializeSecretResponse(_out, secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkStructOpen_SDK_15_4_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkStructOpen_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | ResetStateAction
  | OpenInputAction;

export interface ResetStateAction {
  discriminant: "reset_state";
}
export interface OpenInputAction {
  discriminant: "open_input";
  response: Response;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new ZkStructOpen_SDK_15_4_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeResetStateAction(input);
  } else if (shortname === "10") {
    return contract.deserializeOpenInputAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkStructOpen_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

