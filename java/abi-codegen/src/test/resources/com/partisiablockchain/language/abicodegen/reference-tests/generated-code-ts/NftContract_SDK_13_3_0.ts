// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class NftContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeNFTContractState(_input: AbiInput): NFTContractState {
    const name: string = _input.readString();
    const symbol: string = _input.readString();
    const contractOwner: BlockchainAddress = _input.readAddress();
    const owners_mapLength = _input.readI32();
    const owners: Map<BN, BlockchainAddress> = new Map();
    for (let owners_i = 0; owners_i < owners_mapLength; owners_i++) {
      const owners_key: BN = _input.readUnsignedBigInteger(16);
      const owners_value: BlockchainAddress = _input.readAddress();
      owners.set(owners_key, owners_value);
    }
    const tokenApprovals_mapLength = _input.readI32();
    const tokenApprovals: Map<BN, BlockchainAddress> = new Map();
    for (let tokenApprovals_i = 0; tokenApprovals_i < tokenApprovals_mapLength; tokenApprovals_i++) {
      const tokenApprovals_key: BN = _input.readUnsignedBigInteger(16);
      const tokenApprovals_value: BlockchainAddress = _input.readAddress();
      tokenApprovals.set(tokenApprovals_key, tokenApprovals_value);
    }
    const operatorApprovals_mapLength = _input.readI32();
    const operatorApprovals: Map<BlockchainAddress, BlockchainAddress[]> = new Map();
    for (let operatorApprovals_i = 0; operatorApprovals_i < operatorApprovals_mapLength; operatorApprovals_i++) {
      const operatorApprovals_key: BlockchainAddress = _input.readAddress();
      const operatorApprovals_value_vecLength = _input.readI32();
      const operatorApprovals_value: BlockchainAddress[] = [];
      for (let operatorApprovals_value_i = 0; operatorApprovals_value_i < operatorApprovals_value_vecLength; operatorApprovals_value_i++) {
        const operatorApprovals_value_elem: BlockchainAddress = _input.readAddress();
        operatorApprovals_value.push(operatorApprovals_value_elem);
      }
      operatorApprovals.set(operatorApprovals_key, operatorApprovals_value);
    }
    const tokenUris_mapLength = _input.readI32();
    const tokenUris: Map<BN, string> = new Map();
    for (let tokenUris_i = 0; tokenUris_i < tokenUris_mapLength; tokenUris_i++) {
      const tokenUris_key: BN = _input.readUnsignedBigInteger(16);
      const tokenUris_value: string = _input.readString();
      tokenUris.set(tokenUris_key, tokenUris_value);
    }
    return { name, symbol, contractOwner, owners, tokenApprovals, operatorApprovals, tokenUris };
  }
  public async getState(): Promise<NFTContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeNFTContractState(input);
  }

  public deserializeApproveAction(_input: AbiInput): ApproveAction {
    let approved: Option<BlockchainAddress> = undefined;
    const approved_isSome = _input.readBoolean();
    if (approved_isSome) {
      const approved_option: BlockchainAddress = _input.readAddress();
      approved = approved_option;
    }
    const tokenId: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "approve", approved, tokenId };
  }

  public deserializeSetApprovalForAllAction(_input: AbiInput): SetApprovalForAllAction {
    const operator: BlockchainAddress = _input.readAddress();
    const approved: boolean = _input.readBoolean();
    return { discriminant: "set_approval_for_all", operator, approved };
  }

  public deserializeTransferFromAction(_input: AbiInput): TransferFromAction {
    const from: BlockchainAddress = _input.readAddress();
    const to: BlockchainAddress = _input.readAddress();
    const tokenId: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "transfer_from", from, to, tokenId };
  }

  public deserializeMintAction(_input: AbiInput): MintAction {
    const to: BlockchainAddress = _input.readAddress();
    const tokenId: BN = _input.readUnsignedBigInteger(16);
    const tokenUri: string = _input.readString();
    return { discriminant: "mint", to, tokenId, tokenUri };
  }

  public deserializeBurnAction(_input: AbiInput): BurnAction {
    const tokenId: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "burn", tokenId };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const name: string = _input.readString();
    const symbol: string = _input.readString();
    return { discriminant: "initialize", name, symbol };
  }

}
export interface NFTContractState {
  name: string;
  symbol: string;
  contractOwner: BlockchainAddress;
  owners: Map<BN, BlockchainAddress>;
  tokenApprovals: Map<BN, BlockchainAddress>;
  operatorApprovals: Map<BlockchainAddress, BlockchainAddress[]>;
  tokenUris: Map<BN, string>;
}

export function initialize(name: string, symbol: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeString(name);
    _out.writeString(symbol);
  });
}

export function approve(approved: Option<BlockchainAddress>, tokenId: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("80ad88a707", "hex"));
    _out.writeBoolean(approved !== undefined);
    if (approved !== undefined) {
      _out.writeAddress(approved);
    }
    _out.writeUnsignedBigInteger(tokenId, 16);
  });
}

export function setApprovalForAll(operator: BlockchainAddress, approved: boolean): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("96c4908202", "hex"));
    _out.writeAddress(operator);
    _out.writeBoolean(approved);
  });
}

export function transferFrom(from: BlockchainAddress, to: BlockchainAddress, tokenId: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("d799a3b608", "hex"));
    _out.writeAddress(from);
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(tokenId, 16);
  });
}

export function mint(to: BlockchainAddress, tokenId: BN, tokenUri: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("bbafbce30d", "hex"));
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(tokenId, 16);
    _out.writeString(tokenUri);
  });
}

export function burn(tokenId: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("c5d9feac08", "hex"));
    _out.writeUnsignedBigInteger(tokenId, 16);
  });
}

export function deserializeState(state: StateWithClient): NFTContractState;
export function deserializeState(bytes: Buffer): NFTContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): NFTContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): NFTContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new NftContract_SDK_13_3_0(client, address).deserializeNFTContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new NftContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeNFTContractState(input);
  }
}

export type Action =
  | ApproveAction
  | SetApprovalForAllAction
  | TransferFromAction
  | MintAction
  | BurnAction;

export interface ApproveAction {
  discriminant: "approve";
  approved: Option<BlockchainAddress>;
  tokenId: BN;
}
export interface SetApprovalForAllAction {
  discriminant: "set_approval_for_all";
  operator: BlockchainAddress;
  approved: boolean;
}
export interface TransferFromAction {
  discriminant: "transfer_from";
  from: BlockchainAddress;
  to: BlockchainAddress;
  tokenId: BN;
}
export interface MintAction {
  discriminant: "mint";
  to: BlockchainAddress;
  tokenId: BN;
  tokenUri: string;
}
export interface BurnAction {
  discriminant: "burn";
  tokenId: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new NftContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "80ad88a707") {
    return contract.deserializeApproveAction(input);
  } else if (shortname === "96c4908202") {
    return contract.deserializeSetApprovalForAllAction(input);
  } else if (shortname === "d799a3b608") {
    return contract.deserializeTransferFromAction(input);
  } else if (shortname === "bbafbce30d") {
    return contract.deserializeMintAction(input);
  } else if (shortname === "c5d9feac08") {
    return contract.deserializeBurnAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  name: string;
  symbol: string;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new NftContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

