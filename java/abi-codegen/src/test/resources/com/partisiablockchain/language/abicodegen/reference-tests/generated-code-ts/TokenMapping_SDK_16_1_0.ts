// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class TokenMapping_SDK_16_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const map_treeId = _input.readI32();
    const map: AvlTreeMap<BlockchainAddress, BN> = new AvlTreeMap(
      map_treeId,
      this._client,
      this._address,
      (map_key) => AbiByteOutput.serializeLittleEndian((map_out) => {
        map_out.writeAddress(map_key);
      }),
      (map_bytes) => {
        const map_input = AbiByteInput.createLittleEndian(map_bytes);
        const map_key: BlockchainAddress = map_input.readAddress();
        return map_key;
      },
      (map_bytes) => {
        const map_input = AbiByteInput.createLittleEndian(map_bytes);
        const map_value: BN = map_input.readUnsignedBigInteger(16);
        return map_value;
      }
    );
    let result: Option<BN> = undefined;
    const result_isSome = _input.readBoolean();
    if (result_isSome) {
      const result_option: BN = _input.readUnsignedBigInteger(16);
      result = result_option;
    }
    return { map, result };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeInsertAction(_input: AbiInput): InsertAction {
    const key: BlockchainAddress = _input.readAddress();
    const value: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "insert", key, value };
  }

  public deserializeRemoveAction(_input: AbiInput): RemoveAction {
    const key: BlockchainAddress = _input.readAddress();
    return { discriminant: "remove", key };
  }

  public deserializeGetAction(_input: AbiInput): GetAction {
    const key: BlockchainAddress = _input.readAddress();
    return { discriminant: "get", key };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface ContractState {
  map: AvlTreeMap<BlockchainAddress, BN>;
  result: Option<BN>;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function insert(key: BlockchainAddress, value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(key);
    _out.writeUnsignedBigInteger(value, 16);
  });
}

export function remove(key: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(key);
  });
}

export function get(key: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(key);
  });
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new TokenMapping_SDK_16_1_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new TokenMapping_SDK_16_1_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | InsertAction
  | RemoveAction
  | GetAction;

export interface InsertAction {
  discriminant: "insert";
  key: BlockchainAddress;
  value: BN;
}
export interface RemoveAction {
  discriminant: "remove";
  key: BlockchainAddress;
}
export interface GetAction {
  discriminant: "get";
  key: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new TokenMapping_SDK_16_1_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeInsertAction(input);
  } else if (shortname === "02") {
    return contract.deserializeRemoveAction(input);
  } else if (shortname === "03") {
    return contract.deserializeGetAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new TokenMapping_SDK_16_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

