// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class TokenContractOtherOtherOptions {

  @AbiGenerated
  public record Transfer(BlockchainAddress to, long value) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(to);
      _out.writeU64(value);
    }
  }

  public static byte[] initialize(String name, String symbol, byte decimals, long totalSupply) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(name);
      _out.writeString(symbol);
      _out.writeU8(decimals);
      _out.writeU64(totalSupply);
    });
  }

  public static byte[] transfer(BlockchainAddress to, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(to);
      _out.writeU64(value);
    });
  }

  public static byte[] bulkTransfer(List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] transferFrom(BlockchainAddress from, BlockchainAddress to, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(from);
      _out.writeAddress(to);
      _out.writeU64(value);
    });
  }

  public static byte[] bulkTransferFrom(BlockchainAddress from, List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(from);
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] approve(BlockchainAddress spender, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeAddress(spender);
      _out.writeU64(value);
    });
  }

}
