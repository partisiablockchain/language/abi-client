// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractOptionStruct_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractOptionStruct_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MyStruct deserializeMyStruct(AbiInput _input) {
    Short option = null;
    var option_isSome = _input.readBoolean();
    if (option_isSome) {
      short option_option = _input.readU16();
      option = option_option;
    }
    return new MyStruct(option);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    MyStruct optionStruct = null;
    var optionStruct_isSome = _input.readBoolean();
    if (optionStruct_isSome) {
      MyStruct optionStruct_option = deserializeMyStruct(_input);
      optionStruct = optionStruct_option;
    }
    return new ExampleContractState(optionStruct);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateStructAction deserializeUpdateStructAction(AbiInput _input) {
    MyStruct value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      MyStruct value_option = deserializeMyStruct(_input);
      value = value_option;
    }
    return new UpdateStructAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    MyStruct optionStruct = null;
    var optionStruct_isSome = _input.readBoolean();
    if (optionStruct_isSome) {
      MyStruct optionStruct_option = deserializeMyStruct(_input);
      optionStruct = optionStruct_option;
    }
    return new InitializeInit(optionStruct);
  }


  @AbiGenerated
  public record MyStruct(Short option) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(option != null);
      if (option != null) {
        _out.writeU16(option);
      }
    }
  }

  @AbiGenerated
  public record ExampleContractState(MyStruct optionStruct) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractOptionStruct_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(MyStruct optionStruct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeBoolean(optionStruct != null);
      if (optionStruct != null) {
        optionStruct.serialize(_out);
      }
    });
  }

  public static byte[] updateStruct(MyStruct value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("c5aceefc06"));
      _out.writeBoolean(value != null);
      if (value != null) {
        value.serialize(_out);
      }
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractOptionStruct_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateStructAction(MyStruct value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptionStruct_SDK_13_4_0(null, null);
    if (shortname.equals("c5aceefc06")) {
      return contract.deserializeUpdateStructAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(MyStruct optionStruct) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptionStruct_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
