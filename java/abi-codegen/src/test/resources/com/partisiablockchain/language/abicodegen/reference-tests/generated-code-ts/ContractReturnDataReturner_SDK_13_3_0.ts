// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractReturnDataReturner_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeU8Struct(_input: AbiInput): U8Struct {
    const u8Value: number = _input.readU8();
    const u8Vector_vecLength = _input.readI32();
    const u8Vector: Buffer = _input.readBytes(u8Vector_vecLength);
    const u8Struct_vecLength = _input.readI32();
    const u8Struct: U8Struct[] = [];
    for (let u8Struct_i = 0; u8Struct_i < u8Struct_vecLength; u8Struct_i++) {
      const u8Struct_elem: U8Struct = this.deserializeU8Struct(_input);
      u8Struct.push(u8Struct_elem);
    }
    return { u8Value, u8Vector, u8Struct };
  }
  public deserializeState(_input: AbiInput): State {
    const someU64: BN = _input.readU64();
    const someString: string = _input.readString();
    const someBool: boolean = _input.readBoolean();
    const someU8Struct: U8Struct = this.deserializeU8Struct(_input);
    return { someU64, someString, someBool, someU8Struct };
  }
  public async getState(): Promise<State> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeState(input);
  }

  public deserializeUpdateStateAction(_input: AbiInput): UpdateStateAction {
    const someU64: BN = _input.readU64();
    const someString: string = _input.readString();
    const someBool: boolean = _input.readBoolean();
    const someU8Struct: U8Struct = this.deserializeU8Struct(_input);
    return { discriminant: "update_state", someU64, someString, someBool, someU8Struct };
  }

  public deserializeProvideU64Action(_input: AbiInput): ProvideU64Action {
    return { discriminant: "provide_u64",  };
  }

  public deserializeProvideStringAction(_input: AbiInput): ProvideStringAction {
    return { discriminant: "provide_string",  };
  }

  public deserializeProvideBoolAction(_input: AbiInput): ProvideBoolAction {
    return { discriminant: "provide_bool",  };
  }

  public deserializeProvideU8StructAction(_input: AbiInput): ProvideU8StructAction {
    return { discriminant: "provide_u8_struct",  };
  }

  public deserializeProvideU64WithCourierAction(_input: AbiInput): ProvideU64WithCourierAction {
    const address: BlockchainAddress = _input.readAddress();
    return { discriminant: "provide_u64_with_courier", address };
  }

  public deserializeProvideU64WithCourierCallbackCallback(_input: AbiInput): ProvideU64WithCourierCallbackCallback {
    return { discriminant: "provide_u64_with_courier_callback",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface U8Struct {
  u8Value: number;
  u8Vector: Buffer;
  u8Struct: U8Struct[];
}
function serializeU8Struct(_out: AbiOutput, _value: U8Struct): void {
  const { u8Value, u8Vector, u8Struct } = _value;
  _out.writeU8(u8Value);
  _out.writeI32(u8Vector.length);
  _out.writeBytes(u8Vector);
  _out.writeI32(u8Struct.length);
  for (const u8Struct_vec of u8Struct) {
    serializeU8Struct(_out, u8Struct_vec);
  }
}

export interface State {
  someU64: BN;
  someString: string;
  someBool: boolean;
  someU8Struct: U8Struct;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function updateState(someU64: BN, someString: string, someBool: boolean, someU8Struct: U8Struct): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU64(someU64);
    _out.writeString(someString);
    _out.writeBoolean(someBool);
    serializeU8Struct(_out, someU8Struct);
  });
}

export function provideU64(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function provideString(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
  });
}

export function provideBool(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
  });
}

export function provideU8Struct(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function provideU64WithCourier(address: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeAddress(address);
  });
}

export function provideU64WithCourierCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("60", "hex"));
  });
}

export function deserializeState(state: StateWithClient): State;
export function deserializeState(bytes: Buffer): State;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): State;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): State {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractReturnDataReturner_SDK_13_3_0(client, address).deserializeState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractReturnDataReturner_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeState(input);
  }
}

export type Action =
  | UpdateStateAction
  | ProvideU64Action
  | ProvideStringAction
  | ProvideBoolAction
  | ProvideU8StructAction
  | ProvideU64WithCourierAction;

export interface UpdateStateAction {
  discriminant: "update_state";
  someU64: BN;
  someString: string;
  someBool: boolean;
  someU8Struct: U8Struct;
}
export interface ProvideU64Action {
  discriminant: "provide_u64";
}
export interface ProvideStringAction {
  discriminant: "provide_string";
}
export interface ProvideBoolAction {
  discriminant: "provide_bool";
}
export interface ProvideU8StructAction {
  discriminant: "provide_u8_struct";
}
export interface ProvideU64WithCourierAction {
  discriminant: "provide_u64_with_courier";
  address: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataReturner_SDK_13_3_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeUpdateStateAction(input);
  } else if (shortname === "02") {
    return contract.deserializeProvideU64Action(input);
  } else if (shortname === "03") {
    return contract.deserializeProvideStringAction(input);
  } else if (shortname === "04") {
    return contract.deserializeProvideBoolAction(input);
  } else if (shortname === "05") {
    return contract.deserializeProvideU8StructAction(input);
  } else if (shortname === "06") {
    return contract.deserializeProvideU64WithCourierAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | ProvideU64WithCourierCallbackCallback;

export interface ProvideU64WithCourierCallbackCallback {
  discriminant: "provide_u64_with_courier_callback";
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataReturner_SDK_13_3_0(undefined, undefined);
  if (shortname === "60") {
    return contract.deserializeProvideU64WithCourierCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractReturnDataReturner_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

