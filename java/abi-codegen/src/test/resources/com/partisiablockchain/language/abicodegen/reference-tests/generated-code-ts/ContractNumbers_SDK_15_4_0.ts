// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractNumbers_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myU8: number = _input.readU8();
    const myU16: number = _input.readU16();
    const myU32: number = _input.readU32();
    const myU64: BN = _input.readU64();
    const myI8: number = _input.readI8();
    const myI16: number = _input.readI16();
    const myI32: number = _input.readI32();
    const myI64: BN = _input.readI64();
    return { myU8, myU16, myU32, myU64, myI8, myI16, myI32, myI64 };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyU8Action(_input: AbiInput): UpdateMyU8Action {
    const value: number = _input.readU8();
    return { discriminant: "update_my_u8", value };
  }

  public deserializeUpdateMyU16Action(_input: AbiInput): UpdateMyU16Action {
    const value: number = _input.readU16();
    return { discriminant: "update_my_u16", value };
  }

  public deserializeUpdateMyU32Action(_input: AbiInput): UpdateMyU32Action {
    const value: number = _input.readU32();
    return { discriminant: "update_my_u32", value };
  }

  public deserializeUpdateMyU64Action(_input: AbiInput): UpdateMyU64Action {
    const value: BN = _input.readU64();
    return { discriminant: "update_my_u64", value };
  }

  public deserializeUpdateMyI8Action(_input: AbiInput): UpdateMyI8Action {
    const value: number = _input.readI8();
    return { discriminant: "update_my_i8", value };
  }

  public deserializeUpdateMyI16Action(_input: AbiInput): UpdateMyI16Action {
    const value: number = _input.readI16();
    return { discriminant: "update_my_i16", value };
  }

  public deserializeUpdateMyI32Action(_input: AbiInput): UpdateMyI32Action {
    const value: number = _input.readI32();
    return { discriminant: "update_my_i32", value };
  }

  public deserializeUpdateMyI64Action(_input: AbiInput): UpdateMyI64Action {
    const value: BN = _input.readI64();
    return { discriminant: "update_my_i64", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myU8: number = _input.readU8();
    const myU16: number = _input.readU16();
    const myU32: number = _input.readU32();
    const myU64: BN = _input.readU64();
    const myI8: number = _input.readI8();
    const myI16: number = _input.readI16();
    const myI32: number = _input.readI32();
    const myI64: BN = _input.readI64();
    return { discriminant: "initialize", myU8, myU16, myU32, myU64, myI8, myI16, myI32, myI64 };
  }

}
export interface ExampleContractState {
  myU8: number;
  myU16: number;
  myU32: number;
  myU64: BN;
  myI8: number;
  myI16: number;
  myI32: number;
  myI64: BN;
}

export function initialize(myU8: number, myU16: number, myU32: number, myU64: BN, myI8: number, myI16: number, myI32: number, myI64: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU8(myU8);
    _out.writeU16(myU16);
    _out.writeU32(myU32);
    _out.writeU64(myU64);
    _out.writeI8(myI8);
    _out.writeI16(myI16);
    _out.writeI32(myI32);
    _out.writeI64(myI64);
  });
}

export function updateMyU8(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("80a1b19e07", "hex"));
    _out.writeU8(value);
  });
}

export function updateMyU16(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b2f8b59804", "hex"));
    _out.writeU16(value);
  });
}

export function updateMyU32(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("efe5ed810e", "hex"));
    _out.writeU32(value);
  });
}

export function updateMyU64(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("e4f68de503", "hex"));
    _out.writeU64(value);
  });
}

export function updateMyI8(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f495f18c0a", "hex"));
    _out.writeI8(value);
  });
}

export function updateMyI16(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b9dadd8e0e", "hex"));
    _out.writeI16(value);
  });
}

export function updateMyI32(value: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b3cc80960a", "hex"));
    _out.writeI32(value);
  });
}

export function updateMyI64(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("adc6ccdc05", "hex"));
    _out.writeI64(value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractNumbers_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractNumbers_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyU8Action
  | UpdateMyU16Action
  | UpdateMyU32Action
  | UpdateMyU64Action
  | UpdateMyI8Action
  | UpdateMyI16Action
  | UpdateMyI32Action
  | UpdateMyI64Action;

export interface UpdateMyU8Action {
  discriminant: "update_my_u8";
  value: number;
}
export interface UpdateMyU16Action {
  discriminant: "update_my_u16";
  value: number;
}
export interface UpdateMyU32Action {
  discriminant: "update_my_u32";
  value: number;
}
export interface UpdateMyU64Action {
  discriminant: "update_my_u64";
  value: BN;
}
export interface UpdateMyI8Action {
  discriminant: "update_my_i8";
  value: number;
}
export interface UpdateMyI16Action {
  discriminant: "update_my_i16";
  value: number;
}
export interface UpdateMyI32Action {
  discriminant: "update_my_i32";
  value: number;
}
export interface UpdateMyI64Action {
  discriminant: "update_my_i64";
  value: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNumbers_SDK_15_4_0(undefined, undefined);
  if (shortname === "80a1b19e07") {
    return contract.deserializeUpdateMyU8Action(input);
  } else if (shortname === "b2f8b59804") {
    return contract.deserializeUpdateMyU16Action(input);
  } else if (shortname === "efe5ed810e") {
    return contract.deserializeUpdateMyU32Action(input);
  } else if (shortname === "e4f68de503") {
    return contract.deserializeUpdateMyU64Action(input);
  } else if (shortname === "f495f18c0a") {
    return contract.deserializeUpdateMyI8Action(input);
  } else if (shortname === "b9dadd8e0e") {
    return contract.deserializeUpdateMyI16Action(input);
  } else if (shortname === "b3cc80960a") {
    return contract.deserializeUpdateMyI32Action(input);
  } else if (shortname === "adc6ccdc05") {
    return contract.deserializeUpdateMyI64Action(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myU8: number;
  myU16: number;
  myU32: number;
  myU64: BN;
  myI8: number;
  myI16: number;
  myI32: number;
  myI64: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNumbers_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

