// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkSecondPrice_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkSecondPrice_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private BidderId deserializeBidderId(AbiInput _input) {
    int id = _input.readI32();
    return new BidderId(id);
  }
  private AuctionResult deserializeAuctionResult(AbiInput _input) {
    BidderId winner = deserializeBidderId(_input);
    int secondHighestBid = _input.readI32();
    return new AuctionResult(winner, secondHighestBid);
  }
  private RegisteredBidder deserializeRegisteredBidder(AbiInput _input) {
    BidderId bidderId = deserializeBidderId(_input);
    BlockchainAddress address = _input.readAddress();
    return new RegisteredBidder(bidderId, address);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress owner = _input.readAddress();
    var registeredBidders_vecLength = _input.readI32();
    List<RegisteredBidder> registeredBidders = new ArrayList<>();
    for (int registeredBidders_i = 0; registeredBidders_i < registeredBidders_vecLength; registeredBidders_i++) {
      RegisteredBidder registeredBidders_elem = deserializeRegisteredBidder(_input);
      registeredBidders.add(registeredBidders_elem);
    }
    AuctionResult auctionResult = null;
    var auctionResult_isSome = _input.readBoolean();
    if (auctionResult_isSome) {
      AuctionResult auctionResult_option = deserializeAuctionResult(_input);
      auctionResult = auctionResult_option;
    }
    return new ContractState(owner, registeredBidders, auctionResult);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private RegisterBidderAction deserializeRegisterBidderAction(AbiInput _input) {
    int bidderId = _input.readI32();
    BlockchainAddress address = _input.readAddress();
    return new RegisterBidderAction(bidderId, address);
  }

  private ComputeWinnerAction deserializeComputeWinnerAction(AbiInput _input) {
    return new ComputeWinnerAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record BidderId(int id) {
  }

  @AbiGenerated
  public record AuctionResult(BidderId winner, int secondHighestBid) {
  }

  @AbiGenerated
  public record RegisteredBidder(BidderId bidderId, BlockchainAddress address) {
  }

  @AbiGenerated
  public record ContractState(BlockchainAddress owner, List<RegisteredBidder> registeredBidders, AuctionResult auctionResult) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkSecondPrice_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] registerBidder(int bidderId, BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("30"));
      _out.writeI32(bidderId);
      _out.writeAddress(address);
    });
  }

  public static byte[] computeWinner() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<Integer> addBid() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkSecondPrice_SDK_11_0_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record RegisterBidderAction(int bidderId, BlockchainAddress address) implements Action {
  }
  @AbiGenerated
  public record ComputeWinnerAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new ZkSecondPrice_SDK_11_0_0(null, null);
    if (shortname.equals("30")) {
      return contract.deserializeRegisterBidderAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeComputeWinnerAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkSecondPrice_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
