// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractNestedVector_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myVecVec_vecLength = _input.readI32();
    const myVecVec: BN[][] = [];
    for (let myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      const myVecVec_elem_vecLength = _input.readI32();
      const myVecVec_elem: BN[] = [];
      for (let myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        const myVecVec_elem_elem: BN = _input.readU64();
        myVecVec_elem.push(myVecVec_elem_elem);
      }
      myVecVec.push(myVecVec_elem);
    }
    return { myVecVec };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyVecVecAction(_input: AbiInput): UpdateMyVecVecAction {
    const value_vecLength = _input.readI32();
    const value: BN[][] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem_vecLength = _input.readI32();
      const value_elem: BN[] = [];
      for (let value_elem_i = 0; value_elem_i < value_elem_vecLength; value_elem_i++) {
        const value_elem_elem: BN = _input.readU64();
        value_elem.push(value_elem_elem);
      }
      value.push(value_elem);
    }
    return { discriminant: "update_my_vec_vec", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myVecVec_vecLength = _input.readI32();
    const myVecVec: BN[][] = [];
    for (let myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      const myVecVec_elem_vecLength = _input.readI32();
      const myVecVec_elem: BN[] = [];
      for (let myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        const myVecVec_elem_elem: BN = _input.readU64();
        myVecVec_elem.push(myVecVec_elem_elem);
      }
      myVecVec.push(myVecVec_elem);
    }
    return { discriminant: "initialize", myVecVec };
  }

}
export interface ExampleContractState {
  myVecVec: BN[][];
}

export function initialize(myVecVec: BN[][]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(myVecVec.length);
    for (const myVecVec_vec of myVecVec) {
      _out.writeI32(myVecVec_vec.length);
      for (const myVecVec_vec_vec of myVecVec_vec) {
        _out.writeU64(myVecVec_vec_vec);
      }
    }
  });
}

export function updateMyVecVec(value: BN[][]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ccc0d6db04", "hex"));
    _out.writeI32(value.length);
    for (const value_vec of value) {
      _out.writeI32(value_vec.length);
      for (const value_vec_vec of value_vec) {
        _out.writeU64(value_vec_vec);
      }
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractNestedVector_SDK_13_3_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractNestedVector_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyVecVecAction;

export interface UpdateMyVecVecAction {
  discriminant: "update_my_vec_vec";
  value: BN[][];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNestedVector_SDK_13_3_0(undefined, undefined);
  if (shortname === "ccc0d6db04") {
    return contract.deserializeUpdateMyVecVecAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myVecVec: BN[][];
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNestedVector_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

