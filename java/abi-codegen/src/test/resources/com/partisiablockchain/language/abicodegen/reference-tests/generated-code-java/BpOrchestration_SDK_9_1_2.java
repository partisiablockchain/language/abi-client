// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class BpOrchestration_SDK_9_1_2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public BpOrchestration_SDK_9_1_2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private BlockchainPublicKey deserializeBlockchainPublicKey(AbiInput _input) {
    byte[] ecPoint = _input.readBytes(33);
    return new BlockchainPublicKey(ecPoint);
  }
  private BlsPublicKey deserializeBlsPublicKey(AbiInput _input) {
    byte[] publicKeyValue = _input.readBytes(96);
    return new BlsPublicKey(publicKeyValue);
  }
  private BlsSignature deserializeBlsSignature(AbiInput _input) {
    byte[] signatureValue = _input.readBytes(48);
    return new BlsSignature(signatureValue);
  }
  private BlockProducer deserializeBlockProducer(AbiInput _input) {
    String name = _input.readString();
    String address = _input.readString();
    String website = _input.readString();
    BlockchainAddress identity = _input.readAddress();
    int entityJurisdiction = _input.readI32();
    int serverJurisdiction = _input.readI32();
    BlockchainPublicKey publicKey = deserializeBlockchainPublicKey(_input);
    BlsPublicKey blsPublicKey = deserializeBlsPublicKey(_input);
    long numberOfVotes = _input.readI64();
    byte status = _input.readU8();
    return new BlockProducer(name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status);
  }
  private BlockProducerInformation deserializeBlockProducerInformation(AbiInput _input) {
    String name = _input.readString();
    String website = _input.readString();
    String address = _input.readString();
    BlockchainPublicKey publicKey = deserializeBlockchainPublicKey(_input);
    BlsPublicKey blsPublicKey = deserializeBlsPublicKey(_input);
    int entityJurisdiction = _input.readI32();
    int serverJurisdiction = _input.readI32();
    return new BlockProducerInformation(name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction);
  }
  private LargeOracleUpdate deserializeLargeOracleUpdate(AbiInput _input) {
    var producers_vecLength = _input.readI32();
    List<BlockProducer> producers = new ArrayList<>();
    for (int producers_i = 0; producers_i < producers_vecLength; producers_i++) {
      BlockProducer producers_elem = deserializeBlockProducer(_input);
      producers.add(producers_elem);
    }
    ThresholdKey thresholdKey = deserializeThresholdKey(_input);
    var candidates_vecLength = _input.readI32();
    List<CandidateKey> candidates = new ArrayList<>();
    for (int candidates_i = 0; candidates_i < candidates_vecLength; candidates_i++) {
      CandidateKey candidates_elem = deserializeCandidateKey(_input);
      candidates.add(candidates_elem);
    }
    var honestPartyViews_mapLength = _input.readI32();
    Map<BlockchainAddress, Bitmap> honestPartyViews = new HashMap<>();
    for (int honestPartyViews_i = 0; honestPartyViews_i < honestPartyViews_mapLength; honestPartyViews_i++) {
      BlockchainAddress honestPartyViews_key = _input.readAddress();
      Bitmap honestPartyViews_value = deserializeBitmap(_input);
      honestPartyViews.put(honestPartyViews_key, honestPartyViews_value);
    }
    int retryNonce = _input.readI32();
    Bitmap voters = deserializeBitmap(_input);
    Bitmap activeProducers = deserializeBitmap(_input);
    var heartbeats_mapLength = _input.readI32();
    Map<BlockchainAddress, Bitmap> heartbeats = new HashMap<>();
    for (int heartbeats_i = 0; heartbeats_i < heartbeats_mapLength; heartbeats_i++) {
      BlockchainAddress heartbeats_key = _input.readAddress();
      Bitmap heartbeats_value = deserializeBitmap(_input);
      heartbeats.put(heartbeats_key, heartbeats_value);
    }
    Bitmap broadcasters = deserializeBitmap(_input);
    var broadcastMessages_vecLength = _input.readI32();
    List<BroadcasterTracker> broadcastMessages = new ArrayList<>();
    for (int broadcastMessages_i = 0; broadcastMessages_i < broadcastMessages_vecLength; broadcastMessages_i++) {
      BroadcasterTracker broadcastMessages_elem = deserializeBroadcasterTracker(_input);
      broadcastMessages.add(broadcastMessages_elem);
    }
    byte[] signatureRandomization = _input.readBytes(32);
    return new LargeOracleUpdate(producers, thresholdKey, candidates, honestPartyViews, retryNonce, voters, activeProducers, heartbeats, broadcasters, broadcastMessages, signatureRandomization);
  }
  private BroadcasterTracker deserializeBroadcasterTracker(AbiInput _input) {
    var messages_mapLength = _input.readI32();
    Map<BlockchainAddress, byte[]> messages = new HashMap<>();
    for (int messages_i = 0; messages_i < messages_mapLength; messages_i++) {
      BlockchainAddress messages_key = _input.readAddress();
      byte[] messages_value = _input.readBytes(32);
      messages.put(messages_key, messages_value);
    }
    var bitmaps_mapLength = _input.readI32();
    Map<BlockchainAddress, Bitmap> bitmaps = new HashMap<>();
    for (int bitmaps_i = 0; bitmaps_i < bitmaps_mapLength; bitmaps_i++) {
      BlockchainAddress bitmaps_key = _input.readAddress();
      Bitmap bitmaps_value = deserializeBitmap(_input);
      bitmaps.put(bitmaps_key, bitmaps_value);
    }
    Bitmap echos = deserializeBitmap(_input);
    long startTime = _input.readI64();
    return new BroadcasterTracker(messages, bitmaps, echos, startTime);
  }
  private CommitteeSignature deserializeCommitteeSignature(AbiInput _input) {
    var committeeMembers_vecLength = _input.readI32();
    List<BlockProducer> committeeMembers = new ArrayList<>();
    for (int committeeMembers_i = 0; committeeMembers_i < committeeMembers_vecLength; committeeMembers_i++) {
      BlockProducer committeeMembers_elem = deserializeBlockProducer(_input);
      committeeMembers.add(committeeMembers_elem);
    }
    Signature signature = deserializeSignature(_input);
    BlockchainPublicKey thresholdKey = deserializeBlockchainPublicKey(_input);
    return new CommitteeSignature(committeeMembers, signature, thresholdKey);
  }
  private ThresholdKey deserializeThresholdKey(AbiInput _input) {
    BlockchainPublicKey key = deserializeBlockchainPublicKey(_input);
    String keyId = _input.readString();
    return new ThresholdKey(key, keyId);
  }
  private CandidateKey deserializeCandidateKey(AbiInput _input) {
    ThresholdKey thresholdKey = deserializeThresholdKey(_input);
    int votes = _input.readI32();
    return new CandidateKey(thresholdKey, votes);
  }
  private Bitmap deserializeBitmap(AbiInput _input) {
    var bits_vecLength = _input.readI32();
    byte[] bits = _input.readBytes(bits_vecLength);
    return new Bitmap(bits);
  }
  private BpOrchestrationContractState deserializeBpOrchestrationContractState(AbiInput _input) {
    ThresholdKey thresholdKey = deserializeThresholdKey(_input);
    var kycAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> kycAddresses = new ArrayList<>();
    for (int kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      BlockchainAddress kycAddresses_elem = _input.readAddress();
      kycAddresses.add(kycAddresses_elem);
    }
    int sessionId = _input.readI32();
    int retryNonce = _input.readI32();
    LargeOracleUpdate oracleUpdate = deserializeLargeOracleUpdate(_input);
    var blockProducers_mapLength = _input.readI32();
    Map<BlockchainAddress, BlockProducer> blockProducers = new HashMap<>();
    for (int blockProducers_i = 0; blockProducers_i < blockProducers_mapLength; blockProducers_i++) {
      BlockchainAddress blockProducers_key = _input.readAddress();
      BlockProducer blockProducers_value = deserializeBlockProducer(_input);
      blockProducers.put(blockProducers_key, blockProducers_value);
    }
    long confirmedSinceLastCommittee = _input.readI64();
    var committee_vecLength = _input.readI32();
    List<BlockProducer> committee = new ArrayList<>();
    for (int committee_i = 0; committee_i < committee_vecLength; committee_i++) {
      BlockProducer committee_elem = deserializeBlockProducer(_input);
      committee.add(committee_elem);
    }
    BlockchainAddress largeOracleContract = _input.readAddress();
    BlockchainAddress systemUpdateContractAddress = _input.readAddress();
    BlockchainAddress rewardsContractAddress = _input.readAddress();
    var committeeLog_mapLength = _input.readI32();
    Map<Integer, CommitteeSignature> committeeLog = new HashMap<>();
    for (int committeeLog_i = 0; committeeLog_i < committeeLog_mapLength; committeeLog_i++) {
      int committeeLog_key = _input.readI32();
      CommitteeSignature committeeLog_value = deserializeCommitteeSignature(_input);
      committeeLog.put(committeeLog_key, committeeLog_value);
    }
    byte[] domainSeparator = _input.readBytes(32);
    long broadcastRoundDelay = _input.readI64();
    return new BpOrchestrationContractState(thresholdKey, kycAddresses, sessionId, retryNonce, oracleUpdate, blockProducers, confirmedSinceLastCommittee, committee, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, committeeLog, domainSeparator, broadcastRoundDelay);
  }
  private Signature deserializeSignature(AbiInput _input) {
    byte recoveryId = _input.readU8();
    byte[] valueR = _input.readBytes(32);
    byte[] valueS = _input.readBytes(32);
    return new Signature(recoveryId, valueR, valueS);
  }
  public BpOrchestrationContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeBpOrchestrationContractState(input);
  }

  private RegisterAction deserializeRegisterAction(AbiInput _input) {
    BlockProducerInformation producer = deserializeBlockProducerInformation(_input);
    BlsSignature popSignature = deserializeBlsSignature(_input);
    return new RegisterAction(producer, popSignature);
  }

  private ConfirmBpAction deserializeConfirmBpAction(AbiInput _input) {
    BlockchainAddress bpAddress = _input.readAddress();
    return new ConfirmBpAction(bpAddress);
  }

  private UpdateInfoAction deserializeUpdateInfoAction(AbiInput _input) {
    String updatedName = _input.readString();
    String updatedWebsite = _input.readString();
    String updatedAddress = _input.readString();
    int updatedEntityJurisdiction = _input.readI32();
    int updatedServerJurisdiction = _input.readI32();
    return new UpdateInfoAction(updatedName, updatedWebsite, updatedAddress, updatedEntityJurisdiction, updatedServerJurisdiction);
  }

  private GetProducerInfoAction deserializeGetProducerInfoAction(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    return new GetProducerInfoAction(identity);
  }

  private AddConfirmedBpAction deserializeAddConfirmedBpAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    BlockProducerInformation producer = deserializeBlockProducerInformation(_input);
    BlsSignature popSignature = deserializeBlsSignature(_input);
    return new AddConfirmedBpAction(address, producer, popSignature);
  }

  private RemoveBpAction deserializeRemoveBpAction(AbiInput _input) {
    return new RemoveBpAction();
  }

  private AddCandidateKeyAction deserializeAddCandidateKeyAction(AbiInput _input) {
    BlockchainPublicKey candidateKey = deserializeBlockchainPublicKey(_input);
    String keyId = _input.readString();
    Bitmap bitmap = deserializeBitmap(_input);
    return new AddCandidateKeyAction(candidateKey, keyId, bitmap);
  }

  private AuthorizeNewOracleAction deserializeAuthorizeNewOracleAction(AbiInput _input) {
    Signature signature = deserializeSignature(_input);
    return new AuthorizeNewOracleAction(signature);
  }

  private AddHeartbeatAction deserializeAddHeartbeatAction(AbiInput _input) {
    Bitmap bitmap = deserializeBitmap(_input);
    return new AddHeartbeatAction(bitmap);
  }

  private AddBroadcastHashAction deserializeAddBroadcastHashAction(AbiInput _input) {
    byte[] broadcast = _input.readBytes(32);
    return new AddBroadcastHashAction(broadcast);
  }

  private AddBroadcastBitsAction deserializeAddBroadcastBitsAction(AbiInput _input) {
    var bits_vecLength = _input.readI32();
    byte[] bits = _input.readBytes(bits_vecLength);
    return new AddBroadcastBitsAction(bits);
  }

  private PokeOngoingUpdateAction deserializePokeOngoingUpdateAction(AbiInput _input) {
    return new PokeOngoingUpdateAction();
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    var kycAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> kycAddresses = new ArrayList<>();
    for (int kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      BlockchainAddress kycAddresses_elem = _input.readAddress();
      kycAddresses.add(kycAddresses_elem);
    }
    var initialProducers_vecLength = _input.readI32();
    List<BlockProducer> initialProducers = new ArrayList<>();
    for (int initialProducers_i = 0; initialProducers_i < initialProducers_vecLength; initialProducers_i++) {
      BlockProducer initialProducers_elem = deserializeBlockProducer(_input);
      initialProducers.add(initialProducers_elem);
    }
    BlockchainPublicKey initialThresholdKey = deserializeBlockchainPublicKey(_input);
    String initialThresholdKeyId = _input.readString();
    BlockchainAddress largeOracleContract = _input.readAddress();
    BlockchainAddress systemUpdateContractAddress = _input.readAddress();
    BlockchainAddress rewardsContractAddress = _input.readAddress();
    byte[] domainSeparator = _input.readBytes(32);
    long broadcastRoundDelay = _input.readI64();
    return new InitInit(kycAddresses, initialProducers, initialThresholdKey, initialThresholdKeyId, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, domainSeparator, broadcastRoundDelay);
  }


  @AbiGenerated
  public record BlockchainPublicKey(byte[] ecPoint) {
    public void serialize(AbiOutput _out) {
      if (ecPoint.length != 33) {
        throw new RuntimeException("Length of ecPoint does not match expected 33");
      }
      _out.writeBytes(ecPoint);
    }
  }

  @AbiGenerated
  public record BlsPublicKey(byte[] publicKeyValue) {
    public void serialize(AbiOutput _out) {
      if (publicKeyValue.length != 96) {
        throw new RuntimeException("Length of publicKeyValue does not match expected 96");
      }
      _out.writeBytes(publicKeyValue);
    }
  }

  @AbiGenerated
  public record BlsSignature(byte[] signatureValue) {
    public void serialize(AbiOutput _out) {
      if (signatureValue.length != 48) {
        throw new RuntimeException("Length of signatureValue does not match expected 48");
      }
      _out.writeBytes(signatureValue);
    }
  }

  @AbiGenerated
  public record BlockProducer(String name, String address, String website, BlockchainAddress identity, int entityJurisdiction, int serverJurisdiction, BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey, long numberOfVotes, byte status) {
    public void serialize(AbiOutput _out) {
      _out.writeString(name);
      _out.writeString(address);
      _out.writeString(website);
      _out.writeAddress(identity);
      _out.writeI32(entityJurisdiction);
      _out.writeI32(serverJurisdiction);
      publicKey.serialize(_out);
      blsPublicKey.serialize(_out);
      _out.writeI64(numberOfVotes);
      _out.writeU8(status);
    }
  }

  @AbiGenerated
  public record BlockProducerInformation(String name, String website, String address, BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey, int entityJurisdiction, int serverJurisdiction) {
    public void serialize(AbiOutput _out) {
      _out.writeString(name);
      _out.writeString(website);
      _out.writeString(address);
      publicKey.serialize(_out);
      blsPublicKey.serialize(_out);
      _out.writeI32(entityJurisdiction);
      _out.writeI32(serverJurisdiction);
    }
  }

  @AbiGenerated
  public record LargeOracleUpdate(List<BlockProducer> producers, ThresholdKey thresholdKey, List<CandidateKey> candidates, Map<BlockchainAddress, Bitmap> honestPartyViews, int retryNonce, Bitmap voters, Bitmap activeProducers, Map<BlockchainAddress, Bitmap> heartbeats, Bitmap broadcasters, List<BroadcasterTracker> broadcastMessages, byte[] signatureRandomization) {
  }

  @AbiGenerated
  public record BroadcasterTracker(Map<BlockchainAddress, byte[]> messages, Map<BlockchainAddress, Bitmap> bitmaps, Bitmap echos, long startTime) {
  }

  @AbiGenerated
  public record CommitteeSignature(List<BlockProducer> committeeMembers, Signature signature, BlockchainPublicKey thresholdKey) {
  }

  @AbiGenerated
  public record ThresholdKey(BlockchainPublicKey key, String keyId) {
  }

  @AbiGenerated
  public record CandidateKey(ThresholdKey thresholdKey, int votes) {
  }

  @AbiGenerated
  public record Bitmap(byte[] bits) {
    public void serialize(AbiOutput _out) {
      _out.writeI32(bits.length);
      _out.writeBytes(bits);
    }
  }

  @AbiGenerated
  public record BpOrchestrationContractState(ThresholdKey thresholdKey, List<BlockchainAddress> kycAddresses, int sessionId, int retryNonce, LargeOracleUpdate oracleUpdate, Map<BlockchainAddress, BlockProducer> blockProducers, long confirmedSinceLastCommittee, List<BlockProducer> committee, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, Map<Integer, CommitteeSignature> committeeLog, byte[] domainSeparator, long broadcastRoundDelay) {
    public static BpOrchestrationContractState deserialize(byte[] bytes) {
      return BpOrchestration_SDK_9_1_2.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Signature(byte recoveryId, byte[] valueR, byte[] valueS) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(recoveryId);
      if (valueR.length != 32) {
        throw new RuntimeException("Length of valueR does not match expected 32");
      }
      _out.writeBytes(valueR);
      if (valueS.length != 32) {
        throw new RuntimeException("Length of valueS does not match expected 32");
      }
      _out.writeBytes(valueS);
    }
  }

  public static byte[] init(List<BlockchainAddress> kycAddresses, List<BlockProducer> initialProducers, BlockchainPublicKey initialThresholdKey, String initialThresholdKeyId, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, byte[] domainSeparator, long broadcastRoundDelay) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(kycAddresses.size());
      for (BlockchainAddress kycAddresses_vec : kycAddresses) {
        _out.writeAddress(kycAddresses_vec);
      }
      _out.writeI32(initialProducers.size());
      for (BlockProducer initialProducers_vec : initialProducers) {
        initialProducers_vec.serialize(_out);
      }
      initialThresholdKey.serialize(_out);
      _out.writeString(initialThresholdKeyId);
      _out.writeAddress(largeOracleContract);
      _out.writeAddress(systemUpdateContractAddress);
      _out.writeAddress(rewardsContractAddress);
      if (domainSeparator.length != 32) {
        throw new RuntimeException("Length of domainSeparator does not match expected 32");
      }
      _out.writeBytes(domainSeparator);
      _out.writeI64(broadcastRoundDelay);
    });
  }

  public static byte[] register(BlockProducerInformation producer, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      producer.serialize(_out);
      popSignature.serialize(_out);
    });
  }

  public static byte[] confirmBp(BlockchainAddress bpAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(bpAddress);
    });
  }

  public static byte[] updateInfo(String updatedName, String updatedWebsite, String updatedAddress, int updatedEntityJurisdiction, int updatedServerJurisdiction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeString(updatedName);
      _out.writeString(updatedWebsite);
      _out.writeString(updatedAddress);
      _out.writeI32(updatedEntityJurisdiction);
      _out.writeI32(updatedServerJurisdiction);
    });
  }

  public static byte[] getProducerInfo(BlockchainAddress identity) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("09"));
      _out.writeAddress(identity);
    });
  }

  public static byte[] addConfirmedBp(BlockchainAddress address, BlockProducerInformation producer, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0a"));
      _out.writeAddress(address);
      producer.serialize(_out);
      popSignature.serialize(_out);
    });
  }

  public static byte[] removeBp() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0b"));
    });
  }

  public static byte[] addCandidateKey(BlockchainPublicKey candidateKey, String keyId, Bitmap bitmap) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      candidateKey.serialize(_out);
      _out.writeString(keyId);
      bitmap.serialize(_out);
    });
  }

  public static byte[] authorizeNewOracle(Signature signature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      signature.serialize(_out);
    });
  }

  public static byte[] addHeartbeat(Bitmap bitmap) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      bitmap.serialize(_out);
    });
  }

  public static byte[] addBroadcastHash(byte[] broadcast) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      if (broadcast.length != 32) {
        throw new RuntimeException("Length of broadcast does not match expected 32");
      }
      _out.writeBytes(broadcast);
    });
  }

  public static byte[] addBroadcastBits(byte[] bits) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
      _out.writeI32(bits.length);
      _out.writeBytes(bits);
    });
  }

  public static byte[] pokeOngoingUpdate() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("08"));
    });
  }

  public static BpOrchestrationContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new BpOrchestration_SDK_9_1_2(client, address).deserializeBpOrchestrationContractState(input);
  }
  
  public static BpOrchestrationContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static BpOrchestrationContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record RegisterAction(BlockProducerInformation producer, BlsSignature popSignature) implements Action {
  }
  @AbiGenerated
  public record ConfirmBpAction(BlockchainAddress bpAddress) implements Action {
  }
  @AbiGenerated
  public record UpdateInfoAction(String updatedName, String updatedWebsite, String updatedAddress, int updatedEntityJurisdiction, int updatedServerJurisdiction) implements Action {
  }
  @AbiGenerated
  public record GetProducerInfoAction(BlockchainAddress identity) implements Action {
  }
  @AbiGenerated
  public record AddConfirmedBpAction(BlockchainAddress address, BlockProducerInformation producer, BlsSignature popSignature) implements Action {
  }
  @AbiGenerated
  public record RemoveBpAction() implements Action {
  }
  @AbiGenerated
  public record AddCandidateKeyAction(BlockchainPublicKey candidateKey, String keyId, Bitmap bitmap) implements Action {
  }
  @AbiGenerated
  public record AuthorizeNewOracleAction(Signature signature) implements Action {
  }
  @AbiGenerated
  public record AddHeartbeatAction(Bitmap bitmap) implements Action {
  }
  @AbiGenerated
  public record AddBroadcastHashAction(byte[] broadcast) implements Action {
  }
  @AbiGenerated
  public record AddBroadcastBitsAction(byte[] bits) implements Action {
  }
  @AbiGenerated
  public record PokeOngoingUpdateAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new BpOrchestration_SDK_9_1_2(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeRegisterAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeConfirmBpAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeUpdateInfoAction(input);
    } else if (shortname.equals("09")) {
      return contract.deserializeGetProducerInfoAction(input);
    } else if (shortname.equals("0a")) {
      return contract.deserializeAddConfirmedBpAction(input);
    } else if (shortname.equals("0b")) {
      return contract.deserializeRemoveBpAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeAddCandidateKeyAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeAuthorizeNewOracleAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeAddHeartbeatAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeAddBroadcastHashAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeAddBroadcastBitsAction(input);
    } else if (shortname.equals("08")) {
      return contract.deserializePokeOngoingUpdateAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(List<BlockchainAddress> kycAddresses, List<BlockProducer> initialProducers, BlockchainPublicKey initialThresholdKey, String initialThresholdKeyId, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, byte[] domainSeparator, long broadcastRoundDelay) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new BpOrchestration_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
