// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractEnumOtherOptions {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVehicle(_input: AbiInput): Vehicle {
    const discriminant = _input.readU8();
    if (discriminant === 2) {
      return this.deserializeVehicleBicycle(_input);
    } else if (discriminant === 5) {
      return this.deserializeVehicleCar(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeVehicleBicycle(_input: AbiInput): VehicleBicycle {
    const wheelDiameter: number = _input.readI32();
    return { discriminant: VehicleD.Bicycle, wheelDiameter };
  }
  public deserializeVehicleCar(_input: AbiInput): VehicleCar {
    const engineSize: number = _input.readU8();
    const supportsTrailer: boolean = _input.readBoolean();
    return { discriminant: VehicleD.Car, engineSize, supportsTrailer };
  }
  public deserializeUpdateEnumAction(_input: AbiInput): UpdateEnumAction {
    const val: Vehicle = this.deserializeVehicle(_input);
    return { discriminant: "update_enum", val };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myEnum: Vehicle = this.deserializeVehicle(_input);
    return { discriminant: "initialize", myEnum };
  }

}
export enum VehicleD {
  Bicycle = 2,
  Car = 5,
}
export type Vehicle =
  | VehicleBicycle
  | VehicleCar;

export interface VehicleBicycle {
  discriminant: VehicleD.Bicycle;
  wheelDiameter: number;
}

export interface VehicleCar {
  discriminant: VehicleD.Car;
  engineSize: number;
  supportsTrailer: boolean;
}

export type Action =
  | UpdateEnumAction;

export interface UpdateEnumAction {
  discriminant: "update_enum";
  val: Vehicle;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractEnumOtherOptions(undefined, undefined);
  if (shortname === "ffffffff0d") {
    return contract.deserializeUpdateEnumAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myEnum: Vehicle;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractEnumOtherOptions(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

