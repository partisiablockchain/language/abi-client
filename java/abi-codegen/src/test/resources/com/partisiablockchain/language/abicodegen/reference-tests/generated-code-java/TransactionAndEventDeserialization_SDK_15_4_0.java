// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class TransactionAndEventDeserialization_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public TransactionAndEventDeserialization_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private SignedTransaction deserializeSignedTransaction(AbiInput _input) {
    Signature signature = _input.readSignature();
    InnerPart innerPart = deserializeInnerPart(_input);
    return new SignedTransaction(signature, innerPart);
  }
  private InnerPart deserializeInnerPart(AbiInput _input) {
    CoreTransactionPart core = deserializeCoreTransactionPart(_input);
    InteractWithContractTransaction transaction = deserializeInteractWithContractTransaction(_input);
    return new InnerPart(core, transaction);
  }
  private CoreTransactionPart deserializeCoreTransactionPart(AbiInput _input) {
    long nonce = _input.readI64();
    long validToTime = _input.readI64();
    long cost = _input.readI64();
    return new CoreTransactionPart(nonce, validToTime, cost);
  }
  private InteractWithContractTransaction deserializeInteractWithContractTransaction(AbiInput _input) {
    BlockchainAddress contractId = _input.readAddress();
    var payload_vecLength = _input.readI32();
    byte[] payload = _input.readBytes(payload_vecLength);
    return new InteractWithContractTransaction(contractId, payload);
  }
  private ExecutableEvent deserializeExecutableEvent(AbiInput _input) {
    String originShard = null;
    var originShard_isSome = _input.readBoolean();
    if (originShard_isSome) {
      String originShard_option = _input.readString();
      originShard = originShard_option;
    }
    EventTransaction eventTransaction = deserializeEventTransaction(_input);
    return new ExecutableEvent(originShard, eventTransaction);
  }
  private EventTransaction deserializeEventTransaction(AbiInput _input) {
    Hash hash = _input.readHash();
    InnerEvent innerEvent = deserializeInnerEvent(_input);
    ShardRoute shardRoute = deserializeShardRoute(_input);
    long committeeId = _input.readI64();
    long governanceVersion = _input.readI64();
    byte height = _input.readU8();
    ReturnEnvelope returnEnvelope = null;
    var returnEnvelope_isSome = _input.readBoolean();
    if (returnEnvelope_isSome) {
      ReturnEnvelope returnEnvelope_option = deserializeReturnEnvelope(_input);
      returnEnvelope = returnEnvelope_option;
    }
    return new EventTransaction(hash, innerEvent, shardRoute, committeeId, governanceVersion, height, returnEnvelope);
  }
  private InnerEvent deserializeInnerEvent(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeInnerEventTransaction(_input);
    } else if (discriminant == 1) {
      return deserializeInnerEventCallback(_input);
    } else if (discriminant == 2) {
      return deserializeInnerEventSystem(_input);
    } else if (discriminant == 3) {
      return deserializeInnerEventSync(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private InnerEventTransaction deserializeInnerEventTransaction(AbiInput _input) {
    InnerTransaction innerTransaction = deserializeInnerTransaction(_input);
    return new InnerEventTransaction(innerTransaction);
  }
  private InnerEventCallback deserializeInnerEventCallback(AbiInput _input) {
    CallbackToContract callbackToContract = deserializeCallbackToContract(_input);
    return new InnerEventCallback(callbackToContract);
  }
  private InnerEventSystem deserializeInnerEventSystem(AbiInput _input) {
    InnerSystemEvent innerSystemEvent = deserializeInnerSystemEvent(_input);
    return new InnerEventSystem(innerSystemEvent);
  }
  private InnerEventSync deserializeInnerEventSync(AbiInput _input) {
    SyncEvent syncEvent = deserializeSyncEvent(_input);
    return new InnerEventSync(syncEvent);
  }
  private InnerTransaction deserializeInnerTransaction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    long cost = _input.readI64();
    Transaction transaction = deserializeTransaction(_input);
    return new InnerTransaction(from, cost, transaction);
  }
  private CallbackToContract deserializeCallbackToContract(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    Hash callbackIdentifier = _input.readHash();
    BlockchainAddress from = _input.readAddress();
    long cost = _input.readI64();
    var callbackRpc_vecLength = _input.readI32();
    byte[] callbackRpc = _input.readBytes(callbackRpc_vecLength);
    return new CallbackToContract(address, callbackIdentifier, from, cost, callbackRpc);
  }
  private InnerSystemEvent deserializeInnerSystemEvent(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeInnerSystemEventCreateAccount(_input);
    } else if (discriminant == 1) {
      return deserializeInnerSystemEventCheckExistence(_input);
    } else if (discriminant == 2) {
      return deserializeInnerSystemEventSetFeature(_input);
    } else if (discriminant == 3) {
      return deserializeInnerSystemEventUpdateLocalPluginState(_input);
    } else if (discriminant == 4) {
      return deserializeInnerSystemEventUpdateGlobalPluginState(_input);
    } else if (discriminant == 5) {
      return deserializeInnerSystemEventUpdatePlugin(_input);
    } else if (discriminant == 6) {
      return deserializeInnerSystemEventCallback(_input);
    } else if (discriminant == 7) {
      return deserializeInnerSystemEventCreateShard(_input);
    } else if (discriminant == 8) {
      return deserializeInnerSystemEventRemoveShard(_input);
    } else if (discriminant == 9) {
      return deserializeInnerSystemEventUpdateContextFreePluginState(_input);
    } else if (discriminant == 10) {
      return deserializeInnerSystemEventUpgradeSystemContract(_input);
    } else if (discriminant == 11) {
      return deserializeInnerSystemEventRemoveContract(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private InnerSystemEventCreateAccount deserializeInnerSystemEventCreateAccount(AbiInput _input) {
    CreateAccountEvent createAccountEvent = deserializeCreateAccountEvent(_input);
    return new InnerSystemEventCreateAccount(createAccountEvent);
  }
  private InnerSystemEventCheckExistence deserializeInnerSystemEventCheckExistence(AbiInput _input) {
    CheckExistenceEvent checkExistenceEvent = deserializeCheckExistenceEvent(_input);
    return new InnerSystemEventCheckExistence(checkExistenceEvent);
  }
  private InnerSystemEventSetFeature deserializeInnerSystemEventSetFeature(AbiInput _input) {
    SetFeatureEvent setFeatureEvent = deserializeSetFeatureEvent(_input);
    return new InnerSystemEventSetFeature(setFeatureEvent);
  }
  private InnerSystemEventUpdateLocalPluginState deserializeInnerSystemEventUpdateLocalPluginState(AbiInput _input) {
    UpdateLocalPluginStateEvent updateLocalPluginStateEvent = deserializeUpdateLocalPluginStateEvent(_input);
    return new InnerSystemEventUpdateLocalPluginState(updateLocalPluginStateEvent);
  }
  private InnerSystemEventUpdateGlobalPluginState deserializeInnerSystemEventUpdateGlobalPluginState(AbiInput _input) {
    UpdateGlobalPluginStateEvent updateGlobalPluginStateEvent = deserializeUpdateGlobalPluginStateEvent(_input);
    return new InnerSystemEventUpdateGlobalPluginState(updateGlobalPluginStateEvent);
  }
  private InnerSystemEventUpdatePlugin deserializeInnerSystemEventUpdatePlugin(AbiInput _input) {
    UpdatePluginEvent updatePluginEvent = deserializeUpdatePluginEvent(_input);
    return new InnerSystemEventUpdatePlugin(updatePluginEvent);
  }
  private InnerSystemEventCallback deserializeInnerSystemEventCallback(AbiInput _input) {
    CallbackEvent callbackEvent = deserializeCallbackEvent(_input);
    return new InnerSystemEventCallback(callbackEvent);
  }
  private InnerSystemEventCreateShard deserializeInnerSystemEventCreateShard(AbiInput _input) {
    CreateShardEvent createShardEvent = deserializeCreateShardEvent(_input);
    return new InnerSystemEventCreateShard(createShardEvent);
  }
  private InnerSystemEventRemoveShard deserializeInnerSystemEventRemoveShard(AbiInput _input) {
    RemoveShardEvent removeShardEvent = deserializeRemoveShardEvent(_input);
    return new InnerSystemEventRemoveShard(removeShardEvent);
  }
  private InnerSystemEventUpdateContextFreePluginState deserializeInnerSystemEventUpdateContextFreePluginState(AbiInput _input) {
    UpdateContextFreePluginState updateContextFreePluginState = deserializeUpdateContextFreePluginState(_input);
    return new InnerSystemEventUpdateContextFreePluginState(updateContextFreePluginState);
  }
  private InnerSystemEventUpgradeSystemContract deserializeInnerSystemEventUpgradeSystemContract(AbiInput _input) {
    UpgradeSystemContractEvent upgradeSystemContractEvent = deserializeUpgradeSystemContractEvent(_input);
    return new InnerSystemEventUpgradeSystemContract(upgradeSystemContractEvent);
  }
  private InnerSystemEventRemoveContract deserializeInnerSystemEventRemoveContract(AbiInput _input) {
    RemoveContract removeContract = deserializeRemoveContract(_input);
    return new InnerSystemEventRemoveContract(removeContract);
  }
  private CreateAccountEvent deserializeCreateAccountEvent(AbiInput _input) {
    BlockchainAddress toCreate = _input.readAddress();
    return new CreateAccountEvent(toCreate);
  }
  private CheckExistenceEvent deserializeCheckExistenceEvent(AbiInput _input) {
    BlockchainAddress contractOrAccountAddress = _input.readAddress();
    return new CheckExistenceEvent(contractOrAccountAddress);
  }
  private SetFeatureEvent deserializeSetFeatureEvent(AbiInput _input) {
    String key = _input.readString();
    String value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      String value_option = _input.readString();
      value = value_option;
    }
    return new SetFeatureEvent(key, value);
  }
  private UpdateLocalPluginStateEvent deserializeUpdateLocalPluginStateEvent(AbiInput _input) {
    ChainPluginType chainPluginType = deserializeChainPluginType(_input);
    LocalPluginStateUpdate update = deserializeLocalPluginStateUpdate(_input);
    return new UpdateLocalPluginStateEvent(chainPluginType, update);
  }
  private LocalPluginStateUpdate deserializeLocalPluginStateUpdate(AbiInput _input) {
    BlockchainAddress context = _input.readAddress();
    var rpc_vecLength = _input.readI32();
    byte[] rpc = _input.readBytes(rpc_vecLength);
    return new LocalPluginStateUpdate(context, rpc);
  }
  private ChainPluginType deserializeChainPluginType(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeChainPluginTypeAccount(_input);
    } else if (discriminant == 1) {
      return deserializeChainPluginTypeConsensus(_input);
    } else if (discriminant == 2) {
      return deserializeChainPluginTypeRouting(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private ChainPluginTypeAccount deserializeChainPluginTypeAccount(AbiInput _input) {
    return new ChainPluginTypeAccount();
  }
  private ChainPluginTypeConsensus deserializeChainPluginTypeConsensus(AbiInput _input) {
    return new ChainPluginTypeConsensus();
  }
  private ChainPluginTypeRouting deserializeChainPluginTypeRouting(AbiInput _input) {
    return new ChainPluginTypeRouting();
  }
  private UpdateGlobalPluginStateEvent deserializeUpdateGlobalPluginStateEvent(AbiInput _input) {
    ChainPluginType chainPluginType = deserializeChainPluginType(_input);
    GlobalPluginStateUpdate update = deserializeGlobalPluginStateUpdate(_input);
    return new UpdateGlobalPluginStateEvent(chainPluginType, update);
  }
  private GlobalPluginStateUpdate deserializeGlobalPluginStateUpdate(AbiInput _input) {
    var rpc_vecLength = _input.readI32();
    byte[] rpc = _input.readBytes(rpc_vecLength);
    return new GlobalPluginStateUpdate(rpc);
  }
  private UpdatePluginEvent deserializeUpdatePluginEvent(AbiInput _input) {
    ChainPluginType chainPluginType = deserializeChainPluginType(_input);
    byte[] jar = null;
    var jar_isSome = _input.readBoolean();
    if (jar_isSome) {
      var jar_option_vecLength = _input.readI32();
      byte[] jar_option = _input.readBytes(jar_option_vecLength);
      jar = jar_option;
    }
    var invocation_vecLength = _input.readI32();
    byte[] invocation = _input.readBytes(invocation_vecLength);
    return new UpdatePluginEvent(chainPluginType, jar, invocation);
  }
  private CallbackEvent deserializeCallbackEvent(AbiInput _input) {
    ReturnEnvelope returnEnvelope = deserializeReturnEnvelope(_input);
    Hash completedTransaction = _input.readHash();
    boolean success = _input.readBoolean();
    var returnValue_vecLength = _input.readI32();
    byte[] returnValue = _input.readBytes(returnValue_vecLength);
    return new CallbackEvent(returnEnvelope, completedTransaction, success, returnValue);
  }
  private CreateShardEvent deserializeCreateShardEvent(AbiInput _input) {
    String shardId = _input.readString();
    return new CreateShardEvent(shardId);
  }
  private RemoveShardEvent deserializeRemoveShardEvent(AbiInput _input) {
    String shardId = _input.readString();
    return new RemoveShardEvent(shardId);
  }
  private UpdateContextFreePluginState deserializeUpdateContextFreePluginState(AbiInput _input) {
    ChainPluginType chainPluginType = deserializeChainPluginType(_input);
    var rpc_vecLength = _input.readI32();
    byte[] rpc = _input.readBytes(rpc_vecLength);
    return new UpdateContextFreePluginState(chainPluginType, rpc);
  }
  private UpgradeSystemContractEvent deserializeUpgradeSystemContractEvent(AbiInput _input) {
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var binderJar_vecLength = _input.readI32();
    byte[] binderJar = _input.readBytes(binderJar_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    var rpc_vecLength = _input.readI32();
    byte[] rpc = _input.readBytes(rpc_vecLength);
    BlockchainAddress contractAddress = _input.readAddress();
    return new UpgradeSystemContractEvent(contractJar, binderJar, abi, rpc, contractAddress);
  }
  private RemoveContract deserializeRemoveContract(AbiInput _input) {
    BlockchainAddress contract = _input.readAddress();
    return new RemoveContract(contract);
  }
  private SyncEvent deserializeSyncEvent(AbiInput _input) {
    var accounts_vecLength = _input.readI32();
    List<AccountTransfer> accounts = new ArrayList<>();
    for (int accounts_i = 0; accounts_i < accounts_vecLength; accounts_i++) {
      AccountTransfer accounts_elem = deserializeAccountTransfer(_input);
      accounts.add(accounts_elem);
    }
    var contracts_vecLength = _input.readI32();
    List<ContractTransfer> contracts = new ArrayList<>();
    for (int contracts_i = 0; contracts_i < contracts_vecLength; contracts_i++) {
      ContractTransfer contracts_elem = deserializeContractTransfer(_input);
      contracts.add(contracts_elem);
    }
    var stateStorage_vecLength = _input.readI32();
    byte[] stateStorage = _input.readBytes(stateStorage_vecLength);
    return new SyncEvent(accounts, contracts, stateStorage);
  }
  private AccountTransfer deserializeAccountTransfer(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    long nonce = _input.readI64();
    return new AccountTransfer(address, nonce);
  }
  private ContractTransfer deserializeContractTransfer(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    Hash contractStateHash = _input.readHash();
    Hash pluginStateHash = _input.readHash();
    return new ContractTransfer(address, contractStateHash, pluginStateHash);
  }
  private ShardRoute deserializeShardRoute(AbiInput _input) {
    String targetShard = null;
    var targetShard_isSome = _input.readBoolean();
    if (targetShard_isSome) {
      String targetShard_option = _input.readString();
      targetShard = targetShard_option;
    }
    long nonce = _input.readI64();
    return new ShardRoute(targetShard, nonce);
  }
  private ReturnEnvelope deserializeReturnEnvelope(AbiInput _input) {
    BlockchainAddress contract = _input.readAddress();
    return new ReturnEnvelope(contract);
  }
  private Transaction deserializeTransaction(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeTransactionDeployContract(_input);
    } else if (discriminant == 1) {
      return deserializeTransactionInteractContract(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private TransactionDeployContract deserializeTransactionDeployContract(AbiInput _input) {
    CreateContractTransaction createContractTransaction = deserializeCreateContractTransaction(_input);
    return new TransactionDeployContract(createContractTransaction);
  }
  private TransactionInteractContract deserializeTransactionInteractContract(AbiInput _input) {
    InteractWithContractTransaction interactWithContractTransaction = deserializeInteractWithContractTransaction(_input);
    return new TransactionInteractContract(interactWithContractTransaction);
  }
  private CreateContractTransaction deserializeCreateContractTransaction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    var binderJar_vecLength = _input.readI32();
    byte[] binderJar = _input.readBytes(binderJar_vecLength);
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    var rpc_vecLength = _input.readI32();
    byte[] rpc = _input.readBytes(rpc_vecLength);
    return new CreateContractTransaction(address, binderJar, contractJar, abi, rpc);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    return new ContractState();
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record SignedTransaction(Signature signature, InnerPart innerPart) {
  }

  public static SignedTransaction deserializeSpecialSignedTransaction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    return new TransactionAndEventDeserialization_SDK_15_4_0(null, null).deserializeSignedTransaction(input);
  }
  @AbiGenerated
  public record InnerPart(CoreTransactionPart core, InteractWithContractTransaction transaction) {
  }

  @AbiGenerated
  public record CoreTransactionPart(long nonce, long validToTime, long cost) {
  }

  @AbiGenerated
  public record InteractWithContractTransaction(BlockchainAddress contractId, byte[] payload) {
  }

  @AbiGenerated
  public record ExecutableEvent(String originShard, EventTransaction eventTransaction) {
  }

  public static ExecutableEvent deserializeSpecialExecutableEvent(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    return new TransactionAndEventDeserialization_SDK_15_4_0(null, null).deserializeExecutableEvent(input);
  }
  @AbiGenerated
  public record EventTransaction(Hash hash, InnerEvent innerEvent, ShardRoute shardRoute, long committeeId, long governanceVersion, byte height, ReturnEnvelope returnEnvelope) {
  }

  @AbiGenerated
  public enum InnerEventD {
    TRANSACTION(0),
    CALLBACK(1),
    SYSTEM(2),
    SYNC(3),
    ;
    private final int value;
    InnerEventD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface InnerEvent {
    InnerEventD discriminant();
  }

  @AbiGenerated
  public record InnerEventTransaction(InnerTransaction innerTransaction) implements InnerEvent {
    public InnerEventD discriminant() {
      return InnerEventD.TRANSACTION;
    }
  }

  @AbiGenerated
  public record InnerEventCallback(CallbackToContract callbackToContract) implements InnerEvent {
    public InnerEventD discriminant() {
      return InnerEventD.CALLBACK;
    }
  }

  @AbiGenerated
  public record InnerEventSystem(InnerSystemEvent innerSystemEvent) implements InnerEvent {
    public InnerEventD discriminant() {
      return InnerEventD.SYSTEM;
    }
  }

  @AbiGenerated
  public record InnerEventSync(SyncEvent syncEvent) implements InnerEvent {
    public InnerEventD discriminant() {
      return InnerEventD.SYNC;
    }
  }

  @AbiGenerated
  public record InnerTransaction(BlockchainAddress from, long cost, Transaction transaction) {
  }

  @AbiGenerated
  public record CallbackToContract(BlockchainAddress address, Hash callbackIdentifier, BlockchainAddress from, long cost, byte[] callbackRpc) {
  }

  @AbiGenerated
  public enum InnerSystemEventD {
    CREATE_ACCOUNT(0),
    CHECK_EXISTENCE(1),
    SET_FEATURE(2),
    UPDATE_LOCAL_PLUGIN_STATE(3),
    UPDATE_GLOBAL_PLUGIN_STATE(4),
    UPDATE_PLUGIN(5),
    CALLBACK(6),
    CREATE_SHARD(7),
    REMOVE_SHARD(8),
    UPDATE_CONTEXT_FREE_PLUGIN_STATE(9),
    UPGRADE_SYSTEM_CONTRACT(10),
    REMOVE_CONTRACT(11),
    ;
    private final int value;
    InnerSystemEventD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface InnerSystemEvent {
    InnerSystemEventD discriminant();
  }

  @AbiGenerated
  public record InnerSystemEventCreateAccount(CreateAccountEvent createAccountEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.CREATE_ACCOUNT;
    }
  }

  @AbiGenerated
  public record InnerSystemEventCheckExistence(CheckExistenceEvent checkExistenceEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.CHECK_EXISTENCE;
    }
  }

  @AbiGenerated
  public record InnerSystemEventSetFeature(SetFeatureEvent setFeatureEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.SET_FEATURE;
    }
  }

  @AbiGenerated
  public record InnerSystemEventUpdateLocalPluginState(UpdateLocalPluginStateEvent updateLocalPluginStateEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.UPDATE_LOCAL_PLUGIN_STATE;
    }
  }

  @AbiGenerated
  public record InnerSystemEventUpdateGlobalPluginState(UpdateGlobalPluginStateEvent updateGlobalPluginStateEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.UPDATE_GLOBAL_PLUGIN_STATE;
    }
  }

  @AbiGenerated
  public record InnerSystemEventUpdatePlugin(UpdatePluginEvent updatePluginEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.UPDATE_PLUGIN;
    }
  }

  @AbiGenerated
  public record InnerSystemEventCallback(CallbackEvent callbackEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.CALLBACK;
    }
  }

  @AbiGenerated
  public record InnerSystemEventCreateShard(CreateShardEvent createShardEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.CREATE_SHARD;
    }
  }

  @AbiGenerated
  public record InnerSystemEventRemoveShard(RemoveShardEvent removeShardEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.REMOVE_SHARD;
    }
  }

  @AbiGenerated
  public record InnerSystemEventUpdateContextFreePluginState(UpdateContextFreePluginState updateContextFreePluginState) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.UPDATE_CONTEXT_FREE_PLUGIN_STATE;
    }
  }

  @AbiGenerated
  public record InnerSystemEventUpgradeSystemContract(UpgradeSystemContractEvent upgradeSystemContractEvent) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.UPGRADE_SYSTEM_CONTRACT;
    }
  }

  @AbiGenerated
  public record InnerSystemEventRemoveContract(RemoveContract removeContract) implements InnerSystemEvent {
    public InnerSystemEventD discriminant() {
      return InnerSystemEventD.REMOVE_CONTRACT;
    }
  }

  @AbiGenerated
  public record CreateAccountEvent(BlockchainAddress toCreate) {
  }

  @AbiGenerated
  public record CheckExistenceEvent(BlockchainAddress contractOrAccountAddress) {
  }

  @AbiGenerated
  public record SetFeatureEvent(String key, String value) {
  }

  @AbiGenerated
  public record UpdateLocalPluginStateEvent(ChainPluginType chainPluginType, LocalPluginStateUpdate update) {
  }

  @AbiGenerated
  public record LocalPluginStateUpdate(BlockchainAddress context, byte[] rpc) {
  }

  @AbiGenerated
  public enum ChainPluginTypeD {
    ACCOUNT(0),
    CONSENSUS(1),
    ROUTING(2),
    ;
    private final int value;
    ChainPluginTypeD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface ChainPluginType {
    ChainPluginTypeD discriminant();
  }

  @AbiGenerated
  public record ChainPluginTypeAccount() implements ChainPluginType {
    public ChainPluginTypeD discriminant() {
      return ChainPluginTypeD.ACCOUNT;
    }
  }

  @AbiGenerated
  public record ChainPluginTypeConsensus() implements ChainPluginType {
    public ChainPluginTypeD discriminant() {
      return ChainPluginTypeD.CONSENSUS;
    }
  }

  @AbiGenerated
  public record ChainPluginTypeRouting() implements ChainPluginType {
    public ChainPluginTypeD discriminant() {
      return ChainPluginTypeD.ROUTING;
    }
  }

  @AbiGenerated
  public record UpdateGlobalPluginStateEvent(ChainPluginType chainPluginType, GlobalPluginStateUpdate update) {
  }

  @AbiGenerated
  public record GlobalPluginStateUpdate(byte[] rpc) {
  }

  @AbiGenerated
  public record UpdatePluginEvent(ChainPluginType chainPluginType, byte[] jar, byte[] invocation) {
  }

  @AbiGenerated
  public record CallbackEvent(ReturnEnvelope returnEnvelope, Hash completedTransaction, boolean success, byte[] returnValue) {
  }

  @AbiGenerated
  public record CreateShardEvent(String shardId) {
  }

  @AbiGenerated
  public record RemoveShardEvent(String shardId) {
  }

  @AbiGenerated
  public record UpdateContextFreePluginState(ChainPluginType chainPluginType, byte[] rpc) {
  }

  @AbiGenerated
  public record UpgradeSystemContractEvent(byte[] contractJar, byte[] binderJar, byte[] abi, byte[] rpc, BlockchainAddress contractAddress) {
  }

  @AbiGenerated
  public record RemoveContract(BlockchainAddress contract) {
  }

  @AbiGenerated
  public record SyncEvent(List<AccountTransfer> accounts, List<ContractTransfer> contracts, byte[] stateStorage) {
  }

  @AbiGenerated
  public record AccountTransfer(BlockchainAddress address, long nonce) {
  }

  @AbiGenerated
  public record ContractTransfer(BlockchainAddress address, Hash contractStateHash, Hash pluginStateHash) {
  }

  @AbiGenerated
  public record ShardRoute(String targetShard, long nonce) {
  }

  @AbiGenerated
  public record ReturnEnvelope(BlockchainAddress contract) {
  }

  @AbiGenerated
  public enum TransactionD {
    DEPLOY_CONTRACT(0),
    INTERACT_CONTRACT(1),
    ;
    private final int value;
    TransactionD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Transaction {
    TransactionD discriminant();
  }

  @AbiGenerated
  public record TransactionDeployContract(CreateContractTransaction createContractTransaction) implements Transaction {
    public TransactionD discriminant() {
      return TransactionD.DEPLOY_CONTRACT;
    }
  }

  @AbiGenerated
  public record TransactionInteractContract(InteractWithContractTransaction interactWithContractTransaction) implements Transaction {
    public TransactionD discriminant() {
      return TransactionD.INTERACT_CONTRACT;
    }
  }

  @AbiGenerated
  public record CreateContractTransaction(BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] abi, byte[] rpc) {
  }

  @AbiGenerated
  public record ContractState() {
    public static ContractState deserialize(byte[] bytes) {
      return TransactionAndEventDeserialization_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new TransactionAndEventDeserialization_SDK_15_4_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TransactionAndEventDeserialization_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
