// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class MultiVotingContract_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVote(_input: AbiInput): Vote {
    const proposalId: BN = _input.readU64();
    const vote: number = _input.readU8();
    return { proposalId, vote };
  }
  public deserializeMultiVotingState(_input: AbiInput): MultiVotingState {
    const owner: BlockchainAddress = _input.readAddress();
    const eligibleVoters_vecLength = _input.readI32();
    const eligibleVoters: BlockchainAddress[] = [];
    for (let eligibleVoters_i = 0; eligibleVoters_i < eligibleVoters_vecLength; eligibleVoters_i++) {
      const eligibleVoters_elem: BlockchainAddress = _input.readAddress();
      eligibleVoters.push(eligibleVoters_elem);
    }
    const votingContracts_mapLength = _input.readI32();
    const votingContracts: Map<BN, Option<BlockchainAddress>> = new Map();
    for (let votingContracts_i = 0; votingContracts_i < votingContracts_mapLength; votingContracts_i++) {
      const votingContracts_key: BN = _input.readU64();
      let votingContracts_value: Option<BlockchainAddress> = undefined;
      const votingContracts_value_isSome = _input.readBoolean();
      if (votingContracts_value_isSome) {
        const votingContracts_value_option: BlockchainAddress = _input.readAddress();
        votingContracts_value = votingContracts_value_option;
      }
      votingContracts.set(votingContracts_key, votingContracts_value);
    }
    const votingContractWasm_vecLength = _input.readI32();
    const votingContractWasm: Buffer = _input.readBytes(votingContractWasm_vecLength);
    const votingContractAbi_vecLength = _input.readI32();
    const votingContractAbi: Buffer = _input.readBytes(votingContractAbi_vecLength);
    return { owner, eligibleVoters, votingContracts, votingContractWasm, votingContractAbi };
  }
  public async getState(): Promise<MultiVotingState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeMultiVotingState(input);
  }

  public deserializeAddVoterAction(_input: AbiInput): AddVoterAction {
    const voter: BlockchainAddress = _input.readAddress();
    return { discriminant: "add_voter", voter };
  }

  public deserializeRemoveVoterAction(_input: AbiInput): RemoveVoterAction {
    const voter: BlockchainAddress = _input.readAddress();
    return { discriminant: "remove_voter", voter };
  }

  public deserializeAddVotingContractAction(_input: AbiInput): AddVotingContractAction {
    const pId: BN = _input.readU64();
    return { discriminant: "add_voting_contract", pId };
  }

  public deserializeVoteAction(_input: AbiInput): VoteAction {
    const proposalId: BN = _input.readU64();
    const vote: number = _input.readU8();
    return { discriminant: "vote", proposalId, vote };
  }

  public deserializeBatchVoteAction(_input: AbiInput): BatchVoteAction {
    const votes_vecLength = _input.readI32();
    const votes: Vote[] = [];
    for (let votes_i = 0; votes_i < votes_vecLength; votes_i++) {
      const votes_elem: Vote = this.deserializeVote(_input);
      votes.push(votes_elem);
    }
    return { discriminant: "batch_vote", votes };
  }

  public deserializeAddVotingContractCallbackCallback(_input: AbiInput): AddVotingContractCallbackCallback {
    const pId: BN = _input.readU64();
    const votingAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "add_voting_contract_callback", pId, votingAddress };
  }

  public deserializeVotingContractExistsCallbackCallback(_input: AbiInput): VotingContractExistsCallbackCallback {
    const pId: BN = _input.readU64();
    const votingAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "voting_contract_exists_callback", pId, votingAddress };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const votingContractWasm_vecLength = _input.readI32();
    const votingContractWasm: Buffer = _input.readBytes(votingContractWasm_vecLength);
    const votingContractAbi_vecLength = _input.readI32();
    const votingContractAbi: Buffer = _input.readBytes(votingContractAbi_vecLength);
    return { discriminant: "initialize", votingContractWasm, votingContractAbi };
  }

}
export interface Vote {
  proposalId: BN;
  vote: number;
}
function serializeVote(_out: AbiOutput, _value: Vote): void {
  const { proposalId, vote } = _value;
  _out.writeU64(proposalId);
  _out.writeU8(vote);
}

export interface MultiVotingState {
  owner: BlockchainAddress;
  eligibleVoters: BlockchainAddress[];
  votingContracts: Map<BN, Option<BlockchainAddress>>;
  votingContractWasm: Buffer;
  votingContractAbi: Buffer;
}

export function initialize(votingContractWasm: Buffer, votingContractAbi: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeI32(votingContractWasm.length);
    _out.writeBytes(votingContractWasm);
    _out.writeI32(votingContractAbi.length);
    _out.writeBytes(votingContractAbi);
  });
}

export function addVoter(voter: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("bcdf8463", "hex"));
    _out.writeAddress(voter);
  });
}

export function removeVoter(voter: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("defab7fd0d", "hex"));
    _out.writeAddress(voter);
  });
}

export function addVotingContract(pId: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("d0b5b9a30f", "hex"));
    _out.writeU64(pId);
  });
}

export function vote(proposalId: BN, vote: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f4889dd90a", "hex"));
    _out.writeU64(proposalId);
    _out.writeU8(vote);
  });
}

export function batchVote(votes: Vote[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("83c2ae44", "hex"));
    _out.writeI32(votes.length);
    for (const votes_vec of votes) {
      serializeVote(_out, votes_vec);
    }
  });
}

export function addVotingContractCallback(pId: BN, votingAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU64(pId);
    _out.writeAddress(votingAddress);
  });
}

export function votingContractExistsCallback(pId: BN, votingAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeU64(pId);
    _out.writeAddress(votingAddress);
  });
}

export function deserializeState(state: StateWithClient): MultiVotingState;
export function deserializeState(bytes: Buffer): MultiVotingState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): MultiVotingState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): MultiVotingState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new MultiVotingContract_SDK_9_1_2(client, address).deserializeMultiVotingState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new MultiVotingContract_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeMultiVotingState(input);
  }
}

export type Action =
  | AddVoterAction
  | RemoveVoterAction
  | AddVotingContractAction
  | VoteAction
  | BatchVoteAction;

export interface AddVoterAction {
  discriminant: "add_voter";
  voter: BlockchainAddress;
}
export interface RemoveVoterAction {
  discriminant: "remove_voter";
  voter: BlockchainAddress;
}
export interface AddVotingContractAction {
  discriminant: "add_voting_contract";
  pId: BN;
}
export interface VoteAction {
  discriminant: "vote";
  proposalId: BN;
  vote: number;
}
export interface BatchVoteAction {
  discriminant: "batch_vote";
  votes: Vote[];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new MultiVotingContract_SDK_9_1_2(undefined, undefined);
  if (shortname === "bcdf8463") {
    return contract.deserializeAddVoterAction(input);
  } else if (shortname === "defab7fd0d") {
    return contract.deserializeRemoveVoterAction(input);
  } else if (shortname === "d0b5b9a30f") {
    return contract.deserializeAddVotingContractAction(input);
  } else if (shortname === "f4889dd90a") {
    return contract.deserializeVoteAction(input);
  } else if (shortname === "83c2ae44") {
    return contract.deserializeBatchVoteAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | AddVotingContractCallbackCallback
  | VotingContractExistsCallbackCallback;

export interface AddVotingContractCallbackCallback {
  discriminant: "add_voting_contract_callback";
  pId: BN;
  votingAddress: BlockchainAddress;
}
export interface VotingContractExistsCallbackCallback {
  discriminant: "voting_contract_exists_callback";
  pId: BN;
  votingAddress: BlockchainAddress;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new MultiVotingContract_SDK_9_1_2(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeAddVotingContractCallbackCallback(input);
  } else if (shortname === "02") {
    return contract.deserializeVotingContractExistsCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  votingContractWasm: Buffer;
  votingContractAbi: Buffer;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new MultiVotingContract_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

