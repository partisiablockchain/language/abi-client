// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class AverageSalary {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public AverageSalary(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ZkStateImmutable deserializeZkStateImmutable(AbiInput _input) {
    Map<Integer, AttestationImpl> attestations = null;
    var attestations_isSome = _input.readBoolean();
    if (attestations_isSome) {
      var attestations_option_mapLength = _input.readI32();
      Map<Integer, AttestationImpl> attestations_option = new HashMap<>();
      for (int attestations_option_i = 0; attestations_option_i < attestations_option_mapLength; attestations_option_i++) {
        Integer attestations_option_key = null;
        var attestations_option_key_isSome = _input.readBoolean();
        if (attestations_option_key_isSome) {
          int attestations_option_key_option = _input.readI32();
          attestations_option_key = attestations_option_key_option;
        }
        AttestationImpl attestations_option_value = null;
        var attestations_option_value_isSome = _input.readBoolean();
        if (attestations_option_value_isSome) {
          AttestationImpl attestations_option_value_option = deserializeAttestationImpl(_input);
          attestations_option_value = attestations_option_value_option;
        }
        attestations_option.put(attestations_option_key, attestations_option_value);
      }
      attestations = attestations_option;
    }
    CalculationStatus calculationStatus = null;
    var calculationStatus_isSome = _input.readBoolean();
    if (calculationStatus_isSome) {
      CalculationStatus calculationStatus_option = deserializeCalculationStatus(_input);
      calculationStatus = calculationStatus_option;
    }
    ComputationStateImpl computationState = null;
    var computationState_isSome = _input.readBoolean();
    if (computationState_isSome) {
      ComputationStateImpl computationState_option = deserializeComputationStateImpl(_input);
      computationState = computationState_option;
    }
    boolean defaultOptimisticMode = _input.readBoolean();
    EngineState engines = null;
    var engines_isSome = _input.readBoolean();
    if (engines_isSome) {
      EngineState engines_option = deserializeEngineState(_input);
      engines = engines_option;
    }
    ExternalEventState externalEvents = null;
    var externalEvents_isSome = _input.readBoolean();
    if (externalEvents_isSome) {
      ExternalEventState externalEvents_option = deserializeExternalEventState(_input);
      externalEvents = externalEvents_option;
    }
    List<FinishedComputationImpl> finishedComputations = null;
    var finishedComputations_isSome = _input.readBoolean();
    if (finishedComputations_isSome) {
      var finishedComputations_option_vecLength = _input.readI32();
      List<FinishedComputationImpl> finishedComputations_option = new ArrayList<>();
      for (int finishedComputations_option_i = 0; finishedComputations_option_i < finishedComputations_option_vecLength; finishedComputations_option_i++) {
        FinishedComputationImpl finishedComputations_option_elem = null;
        var finishedComputations_option_elem_isSome = _input.readBoolean();
        if (finishedComputations_option_elem_isSome) {
          FinishedComputationImpl finishedComputations_option_elem_option = deserializeFinishedComputationImpl(_input);
          finishedComputations_option_elem = finishedComputations_option_elem_option;
        }
        finishedComputations_option.add(finishedComputations_option_elem);
      }
      finishedComputations = finishedComputations_option;
    }
    int nextAttestationId = _input.readI32();
    int nextVariableId = _input.readI32();
    BlockchainAddress nodeRegistryContract = null;
    var nodeRegistryContract_isSome = _input.readBoolean();
    if (nodeRegistryContract_isSome) {
      BlockchainAddress nodeRegistryContract_option = _input.readAddress();
      nodeRegistryContract = nodeRegistryContract_option;
    }
    ContractState openState = deserializeContractState(_input);
    Map<Integer, PendingInputImpl> pendingInput = null;
    var pendingInput_isSome = _input.readBoolean();
    if (pendingInput_isSome) {
      var pendingInput_option_mapLength = _input.readI32();
      Map<Integer, PendingInputImpl> pendingInput_option = new HashMap<>();
      for (int pendingInput_option_i = 0; pendingInput_option_i < pendingInput_option_mapLength; pendingInput_option_i++) {
        Integer pendingInput_option_key = null;
        var pendingInput_option_key_isSome = _input.readBoolean();
        if (pendingInput_option_key_isSome) {
          int pendingInput_option_key_option = _input.readI32();
          pendingInput_option_key = pendingInput_option_key_option;
        }
        PendingInputImpl pendingInput_option_value = null;
        var pendingInput_option_value_isSome = _input.readBoolean();
        if (pendingInput_option_value_isSome) {
          PendingInputImpl pendingInput_option_value_option = deserializePendingInputImpl(_input);
          pendingInput_option_value = pendingInput_option_value_option;
        }
        pendingInput_option.put(pendingInput_option_key, pendingInput_option_value);
      }
      pendingInput = pendingInput_option;
    }
    Map<Integer, PendingOnChainOpenImpl> pendingOnChainOpen = null;
    var pendingOnChainOpen_isSome = _input.readBoolean();
    if (pendingOnChainOpen_isSome) {
      var pendingOnChainOpen_option_mapLength = _input.readI32();
      Map<Integer, PendingOnChainOpenImpl> pendingOnChainOpen_option = new HashMap<>();
      for (int pendingOnChainOpen_option_i = 0; pendingOnChainOpen_option_i < pendingOnChainOpen_option_mapLength; pendingOnChainOpen_option_i++) {
        Integer pendingOnChainOpen_option_key = null;
        var pendingOnChainOpen_option_key_isSome = _input.readBoolean();
        if (pendingOnChainOpen_option_key_isSome) {
          int pendingOnChainOpen_option_key_option = _input.readI32();
          pendingOnChainOpen_option_key = pendingOnChainOpen_option_key_option;
        }
        PendingOnChainOpenImpl pendingOnChainOpen_option_value = null;
        var pendingOnChainOpen_option_value_isSome = _input.readBoolean();
        if (pendingOnChainOpen_option_value_isSome) {
          PendingOnChainOpenImpl pendingOnChainOpen_option_value_option = deserializePendingOnChainOpenImpl(_input);
          pendingOnChainOpen_option_value = pendingOnChainOpen_option_value_option;
        }
        pendingOnChainOpen_option.put(pendingOnChainOpen_option_key, pendingOnChainOpen_option_value);
      }
      pendingOnChainOpen = pendingOnChainOpen_option;
    }
    Map<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials = null;
    var preProcessMaterials_isSome = _input.readBoolean();
    if (preProcessMaterials_isSome) {
      var preProcessMaterials_option_mapLength = _input.readI32();
      Map<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials_option = new HashMap<>();
      for (int preProcessMaterials_option_i = 0; preProcessMaterials_option_i < preProcessMaterials_option_mapLength; preProcessMaterials_option_i++) {
        PreProcessMaterialType preProcessMaterials_option_key = null;
        var preProcessMaterials_option_key_isSome = _input.readBoolean();
        if (preProcessMaterials_option_key_isSome) {
          PreProcessMaterialType preProcessMaterials_option_key_option = deserializePreProcessMaterialType(_input);
          preProcessMaterials_option_key = preProcessMaterials_option_key_option;
        }
        PreProcessMaterialState preProcessMaterials_option_value = null;
        var preProcessMaterials_option_value_isSome = _input.readBoolean();
        if (preProcessMaterials_option_value_isSome) {
          PreProcessMaterialState preProcessMaterials_option_value_option = deserializePreProcessMaterialState(_input);
          preProcessMaterials_option_value = preProcessMaterials_option_value_option;
        }
        preProcessMaterials_option.put(preProcessMaterials_option_key, preProcessMaterials_option_value);
      }
      preProcessMaterials = preProcessMaterials_option;
    }
    BlockchainAddress preprocessContract = null;
    var preprocessContract_isSome = _input.readBoolean();
    if (preprocessContract_isSome) {
      BlockchainAddress preprocessContract_option = _input.readAddress();
      preprocessContract = preprocessContract_option;
    }
    Map<Integer, ZkClosedImpl> variables = null;
    var variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      var variables_option_mapLength = _input.readI32();
      Map<Integer, ZkClosedImpl> variables_option = new HashMap<>();
      for (int variables_option_i = 0; variables_option_i < variables_option_mapLength; variables_option_i++) {
        Integer variables_option_key = null;
        var variables_option_key_isSome = _input.readBoolean();
        if (variables_option_key_isSome) {
          int variables_option_key_option = _input.readI32();
          variables_option_key = variables_option_key_option;
        }
        ZkClosedImpl variables_option_value = null;
        var variables_option_value_isSome = _input.readBoolean();
        if (variables_option_value_isSome) {
          ZkClosedImpl variables_option_value_option = deserializeZkClosedImpl(_input);
          variables_option_value = variables_option_value_option;
        }
        variables_option.put(variables_option_key, variables_option_value);
      }
      variables = variables_option;
    }
    long zkComputationDeadline = _input.readI64();
    return new ZkStateImmutable(attestations, calculationStatus, computationState, defaultOptimisticMode, engines, externalEvents, finishedComputations, nextAttestationId, nextVariableId, nodeRegistryContract, openState, pendingInput, pendingOnChainOpen, preProcessMaterials, preprocessContract, variables, zkComputationDeadline);
  }
  private AttestationImpl deserializeAttestationImpl(AbiInput _input) {
    byte[] data = null;
    var data_isSome = _input.readBoolean();
    if (data_isSome) {
      var data_option_vecLength = _input.readI32();
      byte[] data_option = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    int id = _input.readI32();
    List<Signature> signatures = null;
    var signatures_isSome = _input.readBoolean();
    if (signatures_isSome) {
      var signatures_option_vecLength = _input.readI32();
      List<Signature> signatures_option = new ArrayList<>();
      for (int signatures_option_i = 0; signatures_option_i < signatures_option_vecLength; signatures_option_i++) {
        Signature signatures_option_elem = null;
        var signatures_option_elem_isSome = _input.readBoolean();
        if (signatures_option_elem_isSome) {
          Signature signatures_option_elem_option = _input.readSignature();
          signatures_option_elem = signatures_option_elem_option;
        }
        signatures_option.add(signatures_option_elem);
      }
      signatures = signatures_option;
    }
    return new AttestationImpl(data, id, signatures);
  }
  private CalculationStatus deserializeCalculationStatus(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeCalculationStatusCalculationStatus$WAITING(_input);
    } else if (discriminant == 1) {
      return deserializeCalculationStatusCalculationStatus$CALCULATING(_input);
    } else if (discriminant == 2) {
      return deserializeCalculationStatusCalculationStatus$OUTPUT(_input);
    } else if (discriminant == 3) {
      return deserializeCalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR(_input);
    } else if (discriminant == 4) {
      return deserializeCalculationStatusCalculationStatus$DONE(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private CalculationStatusCalculationStatus$WAITING deserializeCalculationStatusCalculationStatus$WAITING(AbiInput _input) {
    return new CalculationStatusCalculationStatus$WAITING();
  }
  private CalculationStatusCalculationStatus$CALCULATING deserializeCalculationStatusCalculationStatus$CALCULATING(AbiInput _input) {
    return new CalculationStatusCalculationStatus$CALCULATING();
  }
  private CalculationStatusCalculationStatus$OUTPUT deserializeCalculationStatusCalculationStatus$OUTPUT(AbiInput _input) {
    return new CalculationStatusCalculationStatus$OUTPUT();
  }
  private CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR deserializeCalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR(AbiInput _input) {
    return new CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR();
  }
  private CalculationStatusCalculationStatus$DONE deserializeCalculationStatusCalculationStatus$DONE(AbiInput _input) {
    return new CalculationStatusCalculationStatus$DONE();
  }
  private ComputationStateImpl deserializeComputationStateImpl(AbiInput _input) {
    long calculateFor = _input.readI64();
    ComputationOutput computationOutput = null;
    var computationOutput_isSome = _input.readBoolean();
    if (computationOutput_isSome) {
      ComputationOutput computationOutput_option = deserializeComputationOutput(_input);
      computationOutput = computationOutput_option;
    }
    int confirmedInputs = _input.readI32();
    int multiplicationCount = _input.readI32();
    int offsetIntoFirstTripleBatch = _input.readI32();
    byte[] onComputeCompleteShortname = null;
    var onComputeCompleteShortname_isSome = _input.readBoolean();
    if (onComputeCompleteShortname_isSome) {
      var onComputeCompleteShortname_option_vecLength = _input.readI32();
      byte[] onComputeCompleteShortname_option = _input.readBytes(onComputeCompleteShortname_option_vecLength);
      onComputeCompleteShortname = onComputeCompleteShortname_option;
    }
    WasmRealContractState openState = null;
    var openState_isSome = _input.readBoolean();
    if (openState_isSome) {
      WasmRealContractState openState_option = deserializeWasmRealContractState(_input);
      openState = openState_option;
    }
    boolean optimisticMode = _input.readBoolean();
    List<Integer> tripleBatches = null;
    var tripleBatches_isSome = _input.readBoolean();
    if (tripleBatches_isSome) {
      var tripleBatches_option_vecLength = _input.readI32();
      List<Integer> tripleBatches_option = new ArrayList<>();
      for (int tripleBatches_option_i = 0; tripleBatches_option_i < tripleBatches_option_vecLength; tripleBatches_option_i++) {
        Integer tripleBatches_option_elem = null;
        var tripleBatches_option_elem_isSome = _input.readBoolean();
        if (tripleBatches_option_elem_isSome) {
          int tripleBatches_option_elem_option = _input.readI32();
          tripleBatches_option_elem = tripleBatches_option_elem_option;
        }
        tripleBatches_option.add(tripleBatches_option_elem);
      }
      tripleBatches = tripleBatches_option;
    }
    Map<Integer, ZkClosedImpl> variables = null;
    var variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      var variables_option_mapLength = _input.readI32();
      Map<Integer, ZkClosedImpl> variables_option = new HashMap<>();
      for (int variables_option_i = 0; variables_option_i < variables_option_mapLength; variables_option_i++) {
        Integer variables_option_key = null;
        var variables_option_key_isSome = _input.readBoolean();
        if (variables_option_key_isSome) {
          int variables_option_key_option = _input.readI32();
          variables_option_key = variables_option_key_option;
        }
        ZkClosedImpl variables_option_value = null;
        var variables_option_value_isSome = _input.readBoolean();
        if (variables_option_value_isSome) {
          ZkClosedImpl variables_option_value_option = deserializeZkClosedImpl(_input);
          variables_option_value = variables_option_value_option;
        }
        variables_option.put(variables_option_key, variables_option_value);
      }
      variables = variables_option;
    }
    return new ComputationStateImpl(calculateFor, computationOutput, confirmedInputs, multiplicationCount, offsetIntoFirstTripleBatch, onComputeCompleteShortname, openState, optimisticMode, tripleBatches, variables);
  }
  private ComputationOutput deserializeComputationOutput(AbiInput _input) {
    byte engines = _input.readI8();
    List<EngineOutputInformation> outputInformation = null;
    var outputInformation_isSome = _input.readBoolean();
    if (outputInformation_isSome) {
      var outputInformation_option_vecLength = _input.readI32();
      List<EngineOutputInformation> outputInformation_option = new ArrayList<>();
      for (int outputInformation_option_i = 0; outputInformation_option_i < outputInformation_option_vecLength; outputInformation_option_i++) {
        EngineOutputInformation outputInformation_option_elem = null;
        var outputInformation_option_elem_isSome = _input.readBoolean();
        if (outputInformation_option_elem_isSome) {
          EngineOutputInformation outputInformation_option_elem_option = deserializeEngineOutputInformation(_input);
          outputInformation_option_elem = outputInformation_option_elem_option;
        }
        outputInformation_option.add(outputInformation_option_elem);
      }
      outputInformation = outputInformation_option;
    }
    List<PendingOutputVariable> variables = null;
    var variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      var variables_option_vecLength = _input.readI32();
      List<PendingOutputVariable> variables_option = new ArrayList<>();
      for (int variables_option_i = 0; variables_option_i < variables_option_vecLength; variables_option_i++) {
        PendingOutputVariable variables_option_elem = null;
        var variables_option_elem_isSome = _input.readBoolean();
        if (variables_option_elem_isSome) {
          PendingOutputVariable variables_option_elem_option = deserializePendingOutputVariable(_input);
          variables_option_elem = variables_option_elem_option;
        }
        variables_option.add(variables_option_elem);
      }
      variables = variables_option;
    }
    return new ComputationOutput(engines, outputInformation, variables);
  }
  private EngineOutputInformation deserializeEngineOutputInformation(AbiInput _input) {
    byte[] linearCombinationElement = null;
    var linearCombinationElement_isSome = _input.readBoolean();
    if (linearCombinationElement_isSome) {
      var linearCombinationElement_option_vecLength = _input.readI32();
      byte[] linearCombinationElement_option = _input.readBytes(linearCombinationElement_option_vecLength);
      linearCombinationElement = linearCombinationElement_option;
    }
    Hash partialOpenings = null;
    var partialOpenings_isSome = _input.readBoolean();
    if (partialOpenings_isSome) {
      Hash partialOpenings_option = _input.readHash();
      partialOpenings = partialOpenings_option;
    }
    byte[] verificationSeed = null;
    var verificationSeed_isSome = _input.readBoolean();
    if (verificationSeed_isSome) {
      var verificationSeed_option_vecLength = _input.readI32();
      byte[] verificationSeed_option = _input.readBytes(verificationSeed_option_vecLength);
      verificationSeed = verificationSeed_option;
    }
    return new EngineOutputInformation(linearCombinationElement, partialOpenings, verificationSeed);
  }
  private PendingOutputVariable deserializePendingOutputVariable(AbiInput _input) {
    int bitLength = _input.readI32();
    byte[] information = null;
    var information_isSome = _input.readBoolean();
    if (information_isSome) {
      var information_option_vecLength = _input.readI32();
      byte[] information_option = _input.readBytes(information_option_vecLength);
      information = information_option;
    }
    int variableId = _input.readI32();
    return new PendingOutputVariable(bitLength, information, variableId);
  }
  private WasmRealContractState deserializeWasmRealContractState(AbiInput _input) {
    Map<Integer, AvlTreeWrapper> avlTrees = null;
    var avlTrees_isSome = _input.readBoolean();
    if (avlTrees_isSome) {
      var avlTrees_option_mapLength = _input.readI32();
      Map<Integer, AvlTreeWrapper> avlTrees_option = new HashMap<>();
      for (int avlTrees_option_i = 0; avlTrees_option_i < avlTrees_option_mapLength; avlTrees_option_i++) {
        Integer avlTrees_option_key = null;
        var avlTrees_option_key_isSome = _input.readBoolean();
        if (avlTrees_option_key_isSome) {
          int avlTrees_option_key_option = _input.readI32();
          avlTrees_option_key = avlTrees_option_key_option;
        }
        AvlTreeWrapper avlTrees_option_value = null;
        var avlTrees_option_value_isSome = _input.readBoolean();
        if (avlTrees_option_value_isSome) {
          AvlTreeWrapper avlTrees_option_value_option = deserializeAvlTreeWrapper(_input);
          avlTrees_option_value = avlTrees_option_value_option;
        }
        avlTrees_option.put(avlTrees_option_key, avlTrees_option_value);
      }
      avlTrees = avlTrees_option;
    }
    ComputationInput computationInput = null;
    var computationInput_isSome = _input.readBoolean();
    if (computationInput_isSome) {
      ComputationInput computationInput_option = deserializeComputationInput(_input);
      computationInput = computationInput_option;
    }
    byte[] openState = null;
    var openState_isSome = _input.readBoolean();
    if (openState_isSome) {
      var openState_option_vecLength = _input.readI32();
      byte[] openState_option = _input.readBytes(openState_option_vecLength);
      openState = openState_option;
    }
    return new WasmRealContractState(avlTrees, computationInput, openState);
  }
  private AvlTreeWrapper deserializeAvlTreeWrapper(AbiInput _input) {
    Map<ComparableByteArray, byte[]> avlTree = null;
    var avlTree_isSome = _input.readBoolean();
    if (avlTree_isSome) {
      var avlTree_option_mapLength = _input.readI32();
      Map<ComparableByteArray, byte[]> avlTree_option = new HashMap<>();
      for (int avlTree_option_i = 0; avlTree_option_i < avlTree_option_mapLength; avlTree_option_i++) {
        ComparableByteArray avlTree_option_key = null;
        var avlTree_option_key_isSome = _input.readBoolean();
        if (avlTree_option_key_isSome) {
          ComparableByteArray avlTree_option_key_option = deserializeComparableByteArray(_input);
          avlTree_option_key = avlTree_option_key_option;
        }
        byte[] avlTree_option_value = null;
        var avlTree_option_value_isSome = _input.readBoolean();
        if (avlTree_option_value_isSome) {
          var avlTree_option_value_option_vecLength = _input.readI32();
          byte[] avlTree_option_value_option = _input.readBytes(avlTree_option_value_option_vecLength);
          avlTree_option_value = avlTree_option_value_option;
        }
        avlTree_option.put(avlTree_option_key, avlTree_option_value);
      }
      avlTree = avlTree_option;
    }
    return new AvlTreeWrapper(avlTree);
  }
  private ComparableByteArray deserializeComparableByteArray(AbiInput _input) {
    byte[] data = null;
    var data_isSome = _input.readBoolean();
    if (data_isSome) {
      var data_option_vecLength = _input.readI32();
      byte[] data_option = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    return new ComparableByteArray(data);
  }
  private ComputationInput deserializeComputationInput(AbiInput _input) {
    List<byte[]> calledFunctionArguments = null;
    var calledFunctionArguments_isSome = _input.readBoolean();
    if (calledFunctionArguments_isSome) {
      var calledFunctionArguments_option_vecLength = _input.readI32();
      List<byte[]> calledFunctionArguments_option = new ArrayList<>();
      for (int calledFunctionArguments_option_i = 0; calledFunctionArguments_option_i < calledFunctionArguments_option_vecLength; calledFunctionArguments_option_i++) {
        byte[] calledFunctionArguments_option_elem = null;
        var calledFunctionArguments_option_elem_isSome = _input.readBoolean();
        if (calledFunctionArguments_option_elem_isSome) {
          var calledFunctionArguments_option_elem_option_vecLength = _input.readI32();
          byte[] calledFunctionArguments_option_elem_option = _input.readBytes(calledFunctionArguments_option_elem_option_vecLength);
          calledFunctionArguments_option_elem = calledFunctionArguments_option_elem_option;
        }
        calledFunctionArguments_option.add(calledFunctionArguments_option_elem);
      }
      calledFunctionArguments = calledFunctionArguments_option;
    }
    int calledFunctionShortname = _input.readI32();
    return new ComputationInput(calledFunctionArguments, calledFunctionShortname);
  }
  private ZkClosedImpl deserializeZkClosedImpl(AbiInput _input) {
    int id = _input.readI32();
    byte[] information = null;
    var information_isSome = _input.readBoolean();
    if (information_isSome) {
      var information_option_vecLength = _input.readI32();
      byte[] information_option = _input.readBytes(information_option_vecLength);
      information = information_option;
    }
    int inputMaskOffset = _input.readI32();
    byte[] maskedInputShare = null;
    var maskedInputShare_isSome = _input.readBoolean();
    if (maskedInputShare_isSome) {
      var maskedInputShare_option_vecLength = _input.readI32();
      byte[] maskedInputShare_option = _input.readBytes(maskedInputShare_option_vecLength);
      maskedInputShare = maskedInputShare_option;
    }
    byte[] openValue = null;
    var openValue_isSome = _input.readBoolean();
    if (openValue_isSome) {
      var openValue_option_vecLength = _input.readI32();
      byte[] openValue_option = _input.readBytes(openValue_option_vecLength);
      openValue = openValue_option;
    }
    BlockchainAddress owner = null;
    var owner_isSome = _input.readBoolean();
    if (owner_isSome) {
      BlockchainAddress owner_option = _input.readAddress();
      owner = owner_option;
    }
    boolean sealed = _input.readBoolean();
    List<Integer> shareBitLengths = null;
    var shareBitLengths_isSome = _input.readBoolean();
    if (shareBitLengths_isSome) {
      var shareBitLengths_option_vecLength = _input.readI32();
      List<Integer> shareBitLengths_option = new ArrayList<>();
      for (int shareBitLengths_option_i = 0; shareBitLengths_option_i < shareBitLengths_option_vecLength; shareBitLengths_option_i++) {
        Integer shareBitLengths_option_elem = null;
        var shareBitLengths_option_elem_isSome = _input.readBoolean();
        if (shareBitLengths_option_elem_isSome) {
          int shareBitLengths_option_elem_option = _input.readI32();
          shareBitLengths_option_elem = shareBitLengths_option_elem_option;
        }
        shareBitLengths_option.add(shareBitLengths_option_elem);
      }
      shareBitLengths = shareBitLengths_option;
    }
    Hash transaction = null;
    var transaction_isSome = _input.readBoolean();
    if (transaction_isSome) {
      Hash transaction_option = _input.readHash();
      transaction = transaction_option;
    }
    return new ZkClosedImpl(id, information, inputMaskOffset, maskedInputShare, openValue, owner, sealed, shareBitLengths, transaction);
  }
  private EngineState deserializeEngineState(AbiInput _input) {
    List<EngineInformation> engines = null;
    var engines_isSome = _input.readBoolean();
    if (engines_isSome) {
      var engines_option_vecLength = _input.readI32();
      List<EngineInformation> engines_option = new ArrayList<>();
      for (int engines_option_i = 0; engines_option_i < engines_option_vecLength; engines_option_i++) {
        EngineInformation engines_option_elem = null;
        var engines_option_elem_isSome = _input.readBoolean();
        if (engines_option_elem_isSome) {
          EngineInformation engines_option_elem_option = deserializeEngineInformation(_input);
          engines_option_elem = engines_option_elem_option;
        }
        engines_option.add(engines_option_elem);
      }
      engines = engines_option;
    }
    return new EngineState(engines);
  }
  private EngineInformation deserializeEngineInformation(AbiInput _input) {
    BlockchainAddress identity = null;
    var identity_isSome = _input.readBoolean();
    if (identity_isSome) {
      BlockchainAddress identity_option = _input.readAddress();
      identity = identity_option;
    }
    BlockchainPublicKey publicKey = null;
    var publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      BlockchainPublicKey publicKey_option = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    String restInterface = null;
    var restInterface_isSome = _input.readBoolean();
    if (restInterface_isSome) {
      String restInterface_option = _input.readString();
      restInterface = restInterface_option;
    }
    return new EngineInformation(identity, publicKey, restInterface);
  }
  private ExternalEventState deserializeExternalEventState(AbiInput _input) {
    Map<Integer, ConfirmedEventLog> confirmedEvents = null;
    var confirmedEvents_isSome = _input.readBoolean();
    if (confirmedEvents_isSome) {
      var confirmedEvents_option_mapLength = _input.readI32();
      Map<Integer, ConfirmedEventLog> confirmedEvents_option = new HashMap<>();
      for (int confirmedEvents_option_i = 0; confirmedEvents_option_i < confirmedEvents_option_mapLength; confirmedEvents_option_i++) {
        Integer confirmedEvents_option_key = null;
        var confirmedEvents_option_key_isSome = _input.readBoolean();
        if (confirmedEvents_option_key_isSome) {
          int confirmedEvents_option_key_option = _input.readI32();
          confirmedEvents_option_key = confirmedEvents_option_key_option;
        }
        ConfirmedEventLog confirmedEvents_option_value = null;
        var confirmedEvents_option_value_isSome = _input.readBoolean();
        if (confirmedEvents_option_value_isSome) {
          ConfirmedEventLog confirmedEvents_option_value_option = deserializeConfirmedEventLog(_input);
          confirmedEvents_option_value = confirmedEvents_option_value_option;
        }
        confirmedEvents_option.put(confirmedEvents_option_key, confirmedEvents_option_value);
      }
      confirmedEvents = confirmedEvents_option;
    }
    int nextEventId = _input.readI32();
    int nextSubscriptionId = _input.readI32();
    Map<Integer, EvmEventSubscription> subscriptions = null;
    var subscriptions_isSome = _input.readBoolean();
    if (subscriptions_isSome) {
      var subscriptions_option_mapLength = _input.readI32();
      Map<Integer, EvmEventSubscription> subscriptions_option = new HashMap<>();
      for (int subscriptions_option_i = 0; subscriptions_option_i < subscriptions_option_mapLength; subscriptions_option_i++) {
        Integer subscriptions_option_key = null;
        var subscriptions_option_key_isSome = _input.readBoolean();
        if (subscriptions_option_key_isSome) {
          int subscriptions_option_key_option = _input.readI32();
          subscriptions_option_key = subscriptions_option_key_option;
        }
        EvmEventSubscription subscriptions_option_value = null;
        var subscriptions_option_value_isSome = _input.readBoolean();
        if (subscriptions_option_value_isSome) {
          EvmEventSubscription subscriptions_option_value_option = deserializeEvmEventSubscription(_input);
          subscriptions_option_value = subscriptions_option_value_option;
        }
        subscriptions_option.put(subscriptions_option_key, subscriptions_option_value);
      }
      subscriptions = subscriptions_option;
    }
    return new ExternalEventState(confirmedEvents, nextEventId, nextSubscriptionId, subscriptions);
  }
  private ConfirmedEventLog deserializeConfirmedEventLog(AbiInput _input) {
    int eventId = _input.readI32();
    EvmEventLog eventLog = null;
    var eventLog_isSome = _input.readBoolean();
    if (eventLog_isSome) {
      EvmEventLog eventLog_option = deserializeEvmEventLog(_input);
      eventLog = eventLog_option;
    }
    int subscriptionId = _input.readI32();
    return new ConfirmedEventLog(eventId, eventLog, subscriptionId);
  }
  private EvmEventLog deserializeEvmEventLog(AbiInput _input) {
    byte[] data = null;
    var data_isSome = _input.readBoolean();
    if (data_isSome) {
      var data_option_vecLength = _input.readI32();
      byte[] data_option = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    EventMetadata metadata = null;
    var metadata_isSome = _input.readBoolean();
    if (metadata_isSome) {
      EventMetadata metadata_option = deserializeEventMetadata(_input);
      metadata = metadata_option;
    }
    TopicList topics = null;
    var topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      TopicList topics_option = deserializeTopicList(_input);
      topics = topics_option;
    }
    return new EvmEventLog(data, metadata, topics);
  }
  private EventMetadata deserializeEventMetadata(AbiInput _input) {
    BigInteger blockNumber = null;
    var blockNumber_isSome = _input.readBoolean();
    if (blockNumber_isSome) {
      BigInteger blockNumber_option = _input.readUnsignedBigInteger(32);
      blockNumber = blockNumber_option;
    }
    BigInteger logIndex = null;
    var logIndex_isSome = _input.readBoolean();
    if (logIndex_isSome) {
      BigInteger logIndex_option = _input.readUnsignedBigInteger(32);
      logIndex = logIndex_option;
    }
    BigInteger transactionIndex = null;
    var transactionIndex_isSome = _input.readBoolean();
    if (transactionIndex_isSome) {
      BigInteger transactionIndex_option = _input.readUnsignedBigInteger(32);
      transactionIndex = transactionIndex_option;
    }
    return new EventMetadata(blockNumber, logIndex, transactionIndex);
  }
  private TopicList deserializeTopicList(AbiInput _input) {
    List<Topic> topics = null;
    var topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      var topics_option_vecLength = _input.readI32();
      List<Topic> topics_option = new ArrayList<>();
      for (int topics_option_i = 0; topics_option_i < topics_option_vecLength; topics_option_i++) {
        Topic topics_option_elem = null;
        var topics_option_elem_isSome = _input.readBoolean();
        if (topics_option_elem_isSome) {
          Topic topics_option_elem_option = deserializeTopic(_input);
          topics_option_elem = topics_option_elem_option;
        }
        topics_option.add(topics_option_elem);
      }
      topics = topics_option;
    }
    return new TopicList(topics);
  }
  private Topic deserializeTopic(AbiInput _input) {
    String topic = null;
    var topic_isSome = _input.readBoolean();
    if (topic_isSome) {
      String topic_option = _input.readString();
      topic = topic_option;
    }
    return new Topic(topic);
  }
  private EvmEventSubscription deserializeEvmEventSubscription(AbiInput _input) {
    String chainId = null;
    var chainId_isSome = _input.readBoolean();
    if (chainId_isSome) {
      String chainId_option = _input.readString();
      chainId = chainId_option;
    }
    EvmAddress contractAddress = null;
    var contractAddress_isSome = _input.readBoolean();
    if (contractAddress_isSome) {
      EvmAddress contractAddress_option = deserializeEvmAddress(_input);
      contractAddress = contractAddress_option;
    }
    BigInteger fromBlock = null;
    var fromBlock_isSome = _input.readBoolean();
    if (fromBlock_isSome) {
      BigInteger fromBlock_option = _input.readUnsignedBigInteger(32);
      fromBlock = fromBlock_option;
    }
    int id = _input.readI32();
    boolean isCancelled = _input.readBoolean();
    EventMetadata latestConfirmedEvent = null;
    var latestConfirmedEvent_isSome = _input.readBoolean();
    if (latestConfirmedEvent_isSome) {
      EventMetadata latestConfirmedEvent_option = deserializeEventMetadata(_input);
      latestConfirmedEvent = latestConfirmedEvent_option;
    }
    PendingEventLog pendingEvent = null;
    var pendingEvent_isSome = _input.readBoolean();
    if (pendingEvent_isSome) {
      PendingEventLog pendingEvent_option = deserializePendingEventLog(_input);
      pendingEvent = pendingEvent_option;
    }
    PendingHeartbeat pendingHeartbeatBlock = null;
    var pendingHeartbeatBlock_isSome = _input.readBoolean();
    if (pendingHeartbeatBlock_isSome) {
      PendingHeartbeat pendingHeartbeatBlock_option = deserializePendingHeartbeat(_input);
      pendingHeartbeatBlock = pendingHeartbeatBlock_option;
    }
    List<TopicList> topics = null;
    var topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      var topics_option_vecLength = _input.readI32();
      List<TopicList> topics_option = new ArrayList<>();
      for (int topics_option_i = 0; topics_option_i < topics_option_vecLength; topics_option_i++) {
        TopicList topics_option_elem = null;
        var topics_option_elem_isSome = _input.readBoolean();
        if (topics_option_elem_isSome) {
          TopicList topics_option_elem_option = deserializeTopicList(_input);
          topics_option_elem = topics_option_elem_option;
        }
        topics_option.add(topics_option_elem);
      }
      topics = topics_option;
    }
    return new EvmEventSubscription(chainId, contractAddress, fromBlock, id, isCancelled, latestConfirmedEvent, pendingEvent, pendingHeartbeatBlock, topics);
  }
  private EvmAddress deserializeEvmAddress(AbiInput _input) {
    String address = null;
    var address_isSome = _input.readBoolean();
    if (address_isSome) {
      String address_option = _input.readString();
      address = address_option;
    }
    return new EvmAddress(address);
  }
  private PendingEventLog deserializePendingEventLog(AbiInput _input) {
    List<EvmEventLog> candidates = null;
    var candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      var candidates_option_vecLength = _input.readI32();
      List<EvmEventLog> candidates_option = new ArrayList<>();
      for (int candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        EvmEventLog candidates_option_elem = null;
        var candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          EvmEventLog candidates_option_elem_option = deserializeEvmEventLog(_input);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.add(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    int eventId = _input.readI32();
    int subscriptionId = _input.readI32();
    return new PendingEventLog(candidates, eventId, subscriptionId);
  }
  private PendingHeartbeat deserializePendingHeartbeat(AbiInput _input) {
    List<BigInteger> candidates = null;
    var candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      var candidates_option_vecLength = _input.readI32();
      List<BigInteger> candidates_option = new ArrayList<>();
      for (int candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        BigInteger candidates_option_elem = null;
        var candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          BigInteger candidates_option_elem_option = _input.readUnsignedBigInteger(32);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.add(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    return new PendingHeartbeat(candidates);
  }
  private FinishedComputationImpl deserializeFinishedComputationImpl(AbiInput _input) {
    long calculateFor = _input.readI64();
    byte engineState = _input.readI8();
    boolean optimistic = _input.readBoolean();
    Hash partialOpens = null;
    var partialOpens_isSome = _input.readBoolean();
    if (partialOpens_isSome) {
      Hash partialOpens_option = _input.readHash();
      partialOpens = partialOpens_option;
    }
    return new FinishedComputationImpl(calculateFor, engineState, optimistic, partialOpens);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress administrator = _input.readAddress();
    Integer averageSalaryResult = null;
    var averageSalaryResult_isSome = _input.readBoolean();
    if (averageSalaryResult_isSome) {
      int averageSalaryResult_option = _input.readU32();
      averageSalaryResult = averageSalaryResult_option;
    }
    Integer numEmployees = null;
    var numEmployees_isSome = _input.readBoolean();
    if (numEmployees_isSome) {
      int numEmployees_option = _input.readU32();
      numEmployees = numEmployees_option;
    }
    return new ContractState(administrator, averageSalaryResult, numEmployees);
  }
  private PendingInputImpl deserializePendingInputImpl(AbiInput _input) {
    List<Hash> commitments = null;
    var commitments_isSome = _input.readBoolean();
    if (commitments_isSome) {
      var commitments_option_vecLength = _input.readI32();
      List<Hash> commitments_option = new ArrayList<>();
      for (int commitments_option_i = 0; commitments_option_i < commitments_option_vecLength; commitments_option_i++) {
        Hash commitments_option_elem = null;
        var commitments_option_elem_isSome = _input.readBoolean();
        if (commitments_option_elem_isSome) {
          Hash commitments_option_elem_option = _input.readHash();
          commitments_option_elem = commitments_option_elem_option;
        }
        commitments_option.add(commitments_option_elem);
      }
      commitments = commitments_option;
    }
    List<byte[]> encryptedShares = null;
    var encryptedShares_isSome = _input.readBoolean();
    if (encryptedShares_isSome) {
      var encryptedShares_option_vecLength = _input.readI32();
      List<byte[]> encryptedShares_option = new ArrayList<>();
      for (int encryptedShares_option_i = 0; encryptedShares_option_i < encryptedShares_option_vecLength; encryptedShares_option_i++) {
        byte[] encryptedShares_option_elem = null;
        var encryptedShares_option_elem_isSome = _input.readBoolean();
        if (encryptedShares_option_elem_isSome) {
          var encryptedShares_option_elem_option_vecLength = _input.readI32();
          byte[] encryptedShares_option_elem_option = _input.readBytes(encryptedShares_option_elem_option_vecLength);
          encryptedShares_option_elem = encryptedShares_option_elem_option;
        }
        encryptedShares_option.add(encryptedShares_option_elem);
      }
      encryptedShares = encryptedShares_option;
    }
    byte engines = _input.readI8();
    List<byte[]> nodeOpenings = null;
    var nodeOpenings_isSome = _input.readBoolean();
    if (nodeOpenings_isSome) {
      var nodeOpenings_option_vecLength = _input.readI32();
      List<byte[]> nodeOpenings_option = new ArrayList<>();
      for (int nodeOpenings_option_i = 0; nodeOpenings_option_i < nodeOpenings_option_vecLength; nodeOpenings_option_i++) {
        byte[] nodeOpenings_option_elem = null;
        var nodeOpenings_option_elem_isSome = _input.readBoolean();
        if (nodeOpenings_option_elem_isSome) {
          var nodeOpenings_option_elem_option_vecLength = _input.readI32();
          byte[] nodeOpenings_option_elem_option = _input.readBytes(nodeOpenings_option_elem_option_vecLength);
          nodeOpenings_option_elem = nodeOpenings_option_elem_option;
        }
        nodeOpenings_option.add(nodeOpenings_option_elem);
      }
      nodeOpenings = nodeOpenings_option;
    }
    byte[] onInputtedShortname = null;
    var onInputtedShortname_isSome = _input.readBoolean();
    if (onInputtedShortname_isSome) {
      var onInputtedShortname_option_vecLength = _input.readI32();
      byte[] onInputtedShortname_option = _input.readBytes(onInputtedShortname_option_vecLength);
      onInputtedShortname = onInputtedShortname_option;
    }
    BlockchainPublicKey publicKey = null;
    var publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      BlockchainPublicKey publicKey_option = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    boolean rejected = _input.readBoolean();
    ZkClosedImpl variable = null;
    var variable_isSome = _input.readBoolean();
    if (variable_isSome) {
      ZkClosedImpl variable_option = deserializeZkClosedImpl(_input);
      variable = variable_option;
    }
    return new PendingInputImpl(commitments, encryptedShares, engines, nodeOpenings, onInputtedShortname, publicKey, rejected, variable);
  }
  private PendingOnChainOpenImpl deserializePendingOnChainOpenImpl(AbiInput _input) {
    Map<Integer, BitLengths> bitLengths = null;
    var bitLengths_isSome = _input.readBoolean();
    if (bitLengths_isSome) {
      var bitLengths_option_mapLength = _input.readI32();
      Map<Integer, BitLengths> bitLengths_option = new HashMap<>();
      for (int bitLengths_option_i = 0; bitLengths_option_i < bitLengths_option_mapLength; bitLengths_option_i++) {
        Integer bitLengths_option_key = null;
        var bitLengths_option_key_isSome = _input.readBoolean();
        if (bitLengths_option_key_isSome) {
          int bitLengths_option_key_option = _input.readI32();
          bitLengths_option_key = bitLengths_option_key_option;
        }
        BitLengths bitLengths_option_value = null;
        var bitLengths_option_value_isSome = _input.readBoolean();
        if (bitLengths_option_value_isSome) {
          BitLengths bitLengths_option_value_option = deserializeBitLengths(_input);
          bitLengths_option_value = bitLengths_option_value_option;
        }
        bitLengths_option.put(bitLengths_option_key, bitLengths_option_value);
      }
      bitLengths = bitLengths_option;
    }
    OpenId outputId = null;
    var outputId_isSome = _input.readBoolean();
    if (outputId_isSome) {
      OpenId outputId_option = deserializeOpenId(_input);
      outputId = outputId_option;
    }
    Map<Integer, binder_Shares> shares = null;
    var shares_isSome = _input.readBoolean();
    if (shares_isSome) {
      var shares_option_mapLength = _input.readI32();
      Map<Integer, binder_Shares> shares_option = new HashMap<>();
      for (int shares_option_i = 0; shares_option_i < shares_option_mapLength; shares_option_i++) {
        Integer shares_option_key = null;
        var shares_option_key_isSome = _input.readBoolean();
        if (shares_option_key_isSome) {
          int shares_option_key_option = _input.readI32();
          shares_option_key = shares_option_key_option;
        }
        binder_Shares shares_option_value = null;
        var shares_option_value_isSome = _input.readBoolean();
        if (shares_option_value_isSome) {
          binder_Shares shares_option_value_option = deserializebinder_Shares(_input);
          shares_option_value = shares_option_value_option;
        }
        shares_option.put(shares_option_key, shares_option_value);
      }
      shares = shares_option;
    }
    List<Integer> variables = null;
    var variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      var variables_option_vecLength = _input.readI32();
      List<Integer> variables_option = new ArrayList<>();
      for (int variables_option_i = 0; variables_option_i < variables_option_vecLength; variables_option_i++) {
        Integer variables_option_elem = null;
        var variables_option_elem_isSome = _input.readBoolean();
        if (variables_option_elem_isSome) {
          int variables_option_elem_option = _input.readI32();
          variables_option_elem = variables_option_elem_option;
        }
        variables_option.add(variables_option_elem);
      }
      variables = variables_option;
    }
    return new PendingOnChainOpenImpl(bitLengths, outputId, shares, variables);
  }
  private BitLengths deserializeBitLengths(AbiInput _input) {
    List<Integer> bitLengths = null;
    var bitLengths_isSome = _input.readBoolean();
    if (bitLengths_isSome) {
      var bitLengths_option_vecLength = _input.readI32();
      List<Integer> bitLengths_option = new ArrayList<>();
      for (int bitLengths_option_i = 0; bitLengths_option_i < bitLengths_option_vecLength; bitLengths_option_i++) {
        Integer bitLengths_option_elem = null;
        var bitLengths_option_elem_isSome = _input.readBoolean();
        if (bitLengths_option_elem_isSome) {
          int bitLengths_option_elem_option = _input.readI32();
          bitLengths_option_elem = bitLengths_option_elem_option;
        }
        bitLengths_option.add(bitLengths_option_elem);
      }
      bitLengths = bitLengths_option;
    }
    return new BitLengths(bitLengths);
  }
  private OpenId deserializeOpenId(AbiInput _input) {
    int id = _input.readI32();
    byte receivedForEngine = _input.readI8();
    return new OpenId(id, receivedForEngine);
  }
  private binder_Shares deserializebinder_Shares(AbiInput _input) {
    List<byte[]> shares = null;
    var shares_isSome = _input.readBoolean();
    if (shares_isSome) {
      var shares_option_vecLength = _input.readI32();
      List<byte[]> shares_option = new ArrayList<>();
      for (int shares_option_i = 0; shares_option_i < shares_option_vecLength; shares_option_i++) {
        byte[] shares_option_elem = null;
        var shares_option_elem_isSome = _input.readBoolean();
        if (shares_option_elem_isSome) {
          var shares_option_elem_option_vecLength = _input.readI32();
          byte[] shares_option_elem_option = _input.readBytes(shares_option_elem_option_vecLength);
          shares_option_elem = shares_option_elem_option;
        }
        shares_option.add(shares_option_elem);
      }
      shares = shares_option;
    }
    return new binder_Shares(shares);
  }
  private PreProcessMaterialType deserializePreProcessMaterialType(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK(_input);
    } else if (discriminant == 1) {
      return deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK(AbiInput _input) {
    return new PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK();
  }
  private PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE(AbiInput _input) {
    return new PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE();
  }
  private PreProcessMaterialState deserializePreProcessMaterialState(AbiInput _input) {
    List<Integer> batchIds = null;
    var batchIds_isSome = _input.readBoolean();
    if (batchIds_isSome) {
      var batchIds_option_vecLength = _input.readI32();
      List<Integer> batchIds_option = new ArrayList<>();
      for (int batchIds_option_i = 0; batchIds_option_i < batchIds_option_vecLength; batchIds_option_i++) {
        Integer batchIds_option_elem = null;
        var batchIds_option_elem_isSome = _input.readBoolean();
        if (batchIds_option_elem_isSome) {
          int batchIds_option_elem_option = _input.readI32();
          batchIds_option_elem = batchIds_option_elem_option;
        }
        batchIds_option.add(batchIds_option_elem);
      }
      batchIds = batchIds_option;
    }
    int numElementsToPrefetch = _input.readI32();
    int numRequestedBatches = _input.readI32();
    long numUsedElements = _input.readI64();
    return new PreProcessMaterialState(batchIds, numElementsToPrefetch, numRequestedBatches, numUsedElements);
  }
  public ZkStateImmutable getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeZkStateImmutable(input);
  }

  private ComputeAverageSalaryAction deserializeComputeAverageSalaryAction(AbiInput _input) {
    return new ComputeAverageSalaryAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record ZkStateImmutable(Map<Integer, AttestationImpl> attestations, CalculationStatus calculationStatus, ComputationStateImpl computationState, boolean defaultOptimisticMode, EngineState engines, ExternalEventState externalEvents, List<FinishedComputationImpl> finishedComputations, int nextAttestationId, int nextVariableId, BlockchainAddress nodeRegistryContract, ContractState openState, Map<Integer, PendingInputImpl> pendingInput, Map<Integer, PendingOnChainOpenImpl> pendingOnChainOpen, Map<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials, BlockchainAddress preprocessContract, Map<Integer, ZkClosedImpl> variables, long zkComputationDeadline) {
    public static ZkStateImmutable deserialize(byte[] bytes) {
      return AverageSalary.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record AttestationImpl(byte[] data, int id, List<Signature> signatures) {
  }

  @AbiGenerated
  public enum CalculationStatusD {
    CALCULATION_STATUS$WAITING(0),
    CALCULATION_STATUS$CALCULATING(1),
    CALCULATION_STATUS$OUTPUT(2),
    CALCULATION_STATUS$MALICIOUS_BEHAVIOUR(3),
    CALCULATION_STATUS$DONE(4),
    ;
    private final int value;
    CalculationStatusD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface CalculationStatus {
    CalculationStatusD discriminant();
  }

  @AbiGenerated
  public record CalculationStatusCalculationStatus$WAITING() implements CalculationStatus {
    public CalculationStatusD discriminant() {
      return CalculationStatusD.CALCULATION_STATUS$WAITING;
    }
  }

  @AbiGenerated
  public record CalculationStatusCalculationStatus$CALCULATING() implements CalculationStatus {
    public CalculationStatusD discriminant() {
      return CalculationStatusD.CALCULATION_STATUS$CALCULATING;
    }
  }

  @AbiGenerated
  public record CalculationStatusCalculationStatus$OUTPUT() implements CalculationStatus {
    public CalculationStatusD discriminant() {
      return CalculationStatusD.CALCULATION_STATUS$OUTPUT;
    }
  }

  @AbiGenerated
  public record CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR() implements CalculationStatus {
    public CalculationStatusD discriminant() {
      return CalculationStatusD.CALCULATION_STATUS$MALICIOUS_BEHAVIOUR;
    }
  }

  @AbiGenerated
  public record CalculationStatusCalculationStatus$DONE() implements CalculationStatus {
    public CalculationStatusD discriminant() {
      return CalculationStatusD.CALCULATION_STATUS$DONE;
    }
  }

  @AbiGenerated
  public record ComputationStateImpl(long calculateFor, ComputationOutput computationOutput, int confirmedInputs, int multiplicationCount, int offsetIntoFirstTripleBatch, byte[] onComputeCompleteShortname, WasmRealContractState openState, boolean optimisticMode, List<Integer> tripleBatches, Map<Integer, ZkClosedImpl> variables) {
  }

  @AbiGenerated
  public record ComputationOutput(byte engines, List<EngineOutputInformation> outputInformation, List<PendingOutputVariable> variables) {
  }

  @AbiGenerated
  public record EngineOutputInformation(byte[] linearCombinationElement, Hash partialOpenings, byte[] verificationSeed) {
  }

  @AbiGenerated
  public record PendingOutputVariable(int bitLength, byte[] information, int variableId) {
  }

  @AbiGenerated
  public record WasmRealContractState(Map<Integer, AvlTreeWrapper> avlTrees, ComputationInput computationInput, byte[] openState) {
  }

  @AbiGenerated
  public record AvlTreeWrapper(Map<ComparableByteArray, byte[]> avlTree) {
  }

  @AbiGenerated
  public record ComparableByteArray(byte[] data) {
  }

  @AbiGenerated
  public record ComputationInput(List<byte[]> calledFunctionArguments, int calledFunctionShortname) {
  }

  @AbiGenerated
  public record ZkClosedImpl(int id, byte[] information, int inputMaskOffset, byte[] maskedInputShare, byte[] openValue, BlockchainAddress owner, boolean sealed, List<Integer> shareBitLengths, Hash transaction) {
  }

  @AbiGenerated
  public record EngineState(List<EngineInformation> engines) {
  }

  @AbiGenerated
  public record EngineInformation(BlockchainAddress identity, BlockchainPublicKey publicKey, String restInterface) {
  }

  @AbiGenerated
  public record ExternalEventState(Map<Integer, ConfirmedEventLog> confirmedEvents, int nextEventId, int nextSubscriptionId, Map<Integer, EvmEventSubscription> subscriptions) {
  }

  @AbiGenerated
  public record ConfirmedEventLog(int eventId, EvmEventLog eventLog, int subscriptionId) {
  }

  @AbiGenerated
  public record EvmEventLog(byte[] data, EventMetadata metadata, TopicList topics) {
  }

  @AbiGenerated
  public record EventMetadata(BigInteger blockNumber, BigInteger logIndex, BigInteger transactionIndex) {
  }

  @AbiGenerated
  public record TopicList(List<Topic> topics) {
  }

  @AbiGenerated
  public record Topic(String topic) {
  }

  @AbiGenerated
  public record EvmEventSubscription(String chainId, EvmAddress contractAddress, BigInteger fromBlock, int id, boolean isCancelled, EventMetadata latestConfirmedEvent, PendingEventLog pendingEvent, PendingHeartbeat pendingHeartbeatBlock, List<TopicList> topics) {
  }

  @AbiGenerated
  public record EvmAddress(String address) {
  }

  @AbiGenerated
  public record PendingEventLog(List<EvmEventLog> candidates, int eventId, int subscriptionId) {
  }

  @AbiGenerated
  public record PendingHeartbeat(List<BigInteger> candidates) {
  }

  @AbiGenerated
  public record FinishedComputationImpl(long calculateFor, byte engineState, boolean optimistic, Hash partialOpens) {
  }

  @AbiGenerated
  public record ContractState(BlockchainAddress administrator, Integer averageSalaryResult, Integer numEmployees) {
  }

  @AbiGenerated
  public record PendingInputImpl(List<Hash> commitments, List<byte[]> encryptedShares, byte engines, List<byte[]> nodeOpenings, byte[] onInputtedShortname, BlockchainPublicKey publicKey, boolean rejected, ZkClosedImpl variable) {
  }

  @AbiGenerated
  public record PendingOnChainOpenImpl(Map<Integer, BitLengths> bitLengths, OpenId outputId, Map<Integer, binder_Shares> shares, List<Integer> variables) {
  }

  @AbiGenerated
  public record BitLengths(List<Integer> bitLengths) {
  }

  @AbiGenerated
  public record OpenId(int id, byte receivedForEngine) {
  }

  @AbiGenerated
  public record binder_Shares(List<byte[]> shares) {
  }

  @AbiGenerated
  public enum PreProcessMaterialTypeD {
    PRE_PROCESS_MATERIAL_TYPE$BINARY_INPUT_MASK(0),
    PRE_PROCESS_MATERIAL_TYPE$BINARY_TRIPLE(1),
    ;
    private final int value;
    PreProcessMaterialTypeD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface PreProcessMaterialType {
    PreProcessMaterialTypeD discriminant();
  }

  @AbiGenerated
  public record PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK() implements PreProcessMaterialType {
    public PreProcessMaterialTypeD discriminant() {
      return PreProcessMaterialTypeD.PRE_PROCESS_MATERIAL_TYPE$BINARY_INPUT_MASK;
    }
  }

  @AbiGenerated
  public record PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE() implements PreProcessMaterialType {
    public PreProcessMaterialTypeD discriminant() {
      return PreProcessMaterialTypeD.PRE_PROCESS_MATERIAL_TYPE$BINARY_TRIPLE;
    }
  }

  @AbiGenerated
  public record PreProcessMaterialState(List<Integer> batchIds, int numElementsToPrefetch, int numRequestedBatches, long numUsedElements) {
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] computeAverageSalary() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<Integer> addSalary() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ZkStateImmutable deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new AverageSalary(client, address).deserializeZkStateImmutable(input);
  }
  
  public static ZkStateImmutable deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ZkStateImmutable deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ComputeAverageSalaryAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new AverageSalary(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeComputeAverageSalaryAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new AverageSalary(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
