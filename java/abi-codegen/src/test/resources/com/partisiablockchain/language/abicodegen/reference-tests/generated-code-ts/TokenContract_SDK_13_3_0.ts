// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class TokenContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeTransfer(_input: AbiInput): Transfer {
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { to, amount };
  }
  public deserializeTokenState(_input: AbiInput): TokenState {
    const name: string = _input.readString();
    const decimals: number = _input.readU8();
    const symbol: string = _input.readString();
    const owner: BlockchainAddress = _input.readAddress();
    const totalSupply: BN = _input.readUnsignedBigInteger(16);
    const balances_mapLength = _input.readI32();
    const balances: Map<BlockchainAddress, BN> = new Map();
    for (let balances_i = 0; balances_i < balances_mapLength; balances_i++) {
      const balances_key: BlockchainAddress = _input.readAddress();
      const balances_value: BN = _input.readUnsignedBigInteger(16);
      balances.set(balances_key, balances_value);
    }
    const allowed_mapLength = _input.readI32();
    const allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>> = new Map();
    for (let allowed_i = 0; allowed_i < allowed_mapLength; allowed_i++) {
      const allowed_key: BlockchainAddress = _input.readAddress();
      const allowed_value_mapLength = _input.readI32();
      const allowed_value: Map<BlockchainAddress, BN> = new Map();
      for (let allowed_value_i = 0; allowed_value_i < allowed_value_mapLength; allowed_value_i++) {
        const allowed_value_key: BlockchainAddress = _input.readAddress();
        const allowed_value_value: BN = _input.readUnsignedBigInteger(16);
        allowed_value.set(allowed_value_key, allowed_value_value);
      }
      allowed.set(allowed_key, allowed_value);
    }
    return { name, decimals, symbol, owner, totalSupply, balances, allowed };
  }
  public async getState(): Promise<TokenState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeTokenState(input);
  }

  public deserializeTransferAction(_input: AbiInput): TransferAction {
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "transfer", to, amount };
  }

  public deserializeBulkTransferAction(_input: AbiInput): BulkTransferAction {
    const transfers_vecLength = _input.readI32();
    const transfers: Transfer[] = [];
    for (let transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      const transfers_elem: Transfer = this.deserializeTransfer(_input);
      transfers.push(transfers_elem);
    }
    return { discriminant: "bulk_transfer", transfers };
  }

  public deserializeTransferFromAction(_input: AbiInput): TransferFromAction {
    const from: BlockchainAddress = _input.readAddress();
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "transfer_from", from, to, amount };
  }

  public deserializeBulkTransferFromAction(_input: AbiInput): BulkTransferFromAction {
    const from: BlockchainAddress = _input.readAddress();
    const transfers_vecLength = _input.readI32();
    const transfers: Transfer[] = [];
    for (let transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      const transfers_elem: Transfer = this.deserializeTransfer(_input);
      transfers.push(transfers_elem);
    }
    return { discriminant: "bulk_transfer_from", from, transfers };
  }

  public deserializeApproveAction(_input: AbiInput): ApproveAction {
    const spender: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "approve", spender, amount };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface Transfer {
  to: BlockchainAddress;
  amount: BN;
}
function serializeTransfer(_out: AbiOutput, _value: Transfer): void {
  const { to, amount } = _value;
  _out.writeAddress(to);
  _out.writeUnsignedBigInteger(amount, 16);
}

export interface TokenState {
  name: string;
  decimals: number;
  symbol: string;
  owner: BlockchainAddress;
  totalSupply: BN;
  balances: Map<BlockchainAddress, BN>;
  allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>>;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function transfer(to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function bulkTransfer(transfers: Transfer[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeI32(transfers.length);
    for (const transfers_vec of transfers) {
      serializeTransfer(_out, transfers_vec);
    }
  });
}

export function transferFrom(from: BlockchainAddress, to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(from);
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function bulkTransferFrom(from: BlockchainAddress, transfers: Transfer[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(from);
    _out.writeI32(transfers.length);
    for (const transfers_vec of transfers) {
      serializeTransfer(_out, transfers_vec);
    }
  });
}

export function approve(spender: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeAddress(spender);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function deserializeState(state: StateWithClient): TokenState;
export function deserializeState(bytes: Buffer): TokenState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): TokenState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): TokenState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new TokenContract_SDK_13_3_0(client, address).deserializeTokenState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new TokenContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeTokenState(input);
  }
}

export type Action =
  | TransferAction
  | BulkTransferAction
  | TransferFromAction
  | BulkTransferFromAction
  | ApproveAction;

export interface TransferAction {
  discriminant: "transfer";
  to: BlockchainAddress;
  amount: BN;
}
export interface BulkTransferAction {
  discriminant: "bulk_transfer";
  transfers: Transfer[];
}
export interface TransferFromAction {
  discriminant: "transfer_from";
  from: BlockchainAddress;
  to: BlockchainAddress;
  amount: BN;
}
export interface BulkTransferFromAction {
  discriminant: "bulk_transfer_from";
  from: BlockchainAddress;
  transfers: Transfer[];
}
export interface ApproveAction {
  discriminant: "approve";
  spender: BlockchainAddress;
  amount: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new TokenContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeTransferAction(input);
  } else if (shortname === "02") {
    return contract.deserializeBulkTransferAction(input);
  } else if (shortname === "03") {
    return contract.deserializeTransferFromAction(input);
  } else if (shortname === "04") {
    return contract.deserializeBulkTransferFromAction(input);
  } else if (shortname === "05") {
    return contract.deserializeApproveAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new TokenContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

