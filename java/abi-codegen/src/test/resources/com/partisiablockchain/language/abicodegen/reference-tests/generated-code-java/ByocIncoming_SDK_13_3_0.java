// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ByocIncoming_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ByocIncoming_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Deposit deserializeDeposit(AbiInput _input) {
    BlockchainAddress receiver = _input.readAddress();
    long amount = _input.readU64();
    int signaturesRegistered = _input.readU32();
    return new Deposit(receiver, amount, signaturesRegistered);
  }
  private Dispute deserializeDispute(AbiInput _input) {
    BlockchainAddress challenger = _input.readAddress();
    var claims_vecLength = _input.readI32();
    List<Transaction> claims = new ArrayList<>();
    for (int claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      Transaction claims_elem = deserializeTransaction(_input);
      claims.add(claims_elem);
    }
    int votingResult = _input.readU32();
    return new Dispute(challenger, claims, votingResult);
  }
  private Epoch deserializeEpoch(AbiInput _input) {
    long fromDepositNonce = _input.readU64();
    long toDepositNonce = _input.readU64();
    var oracles_vecLength = _input.readI32();
    List<BlockchainAddress> oracles = new ArrayList<>();
    for (int oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      BlockchainAddress oracles_elem = _input.readAddress();
      oracles.add(oracles_elem);
    }
    return new Epoch(fromDepositNonce, toDepositNonce, oracles);
  }
  private Transaction deserializeTransaction(AbiInput _input) {
    long nonce = _input.readU64();
    BlockchainAddress receiver = _input.readAddress();
    byte[] amount = _input.readBytes(32);
    return new Transaction(nonce, receiver, amount);
  }
  private ByocIncomingContractState deserializeByocIncomingContractState(AbiInput _input) {
    BlockchainAddress largeOracleAddress = _input.readAddress();
    long depositNonce = _input.readU64();
    var deposits_mapLength = _input.readI32();
    Map<Long, Deposit> deposits = new HashMap<>();
    for (int deposits_i = 0; deposits_i < deposits_mapLength; deposits_i++) {
      long deposits_key = _input.readU64();
      Deposit deposits_value = deserializeDeposit(_input);
      deposits.put(deposits_key, deposits_value);
    }
    String symbol = _input.readString();
    var oracleNodes_vecLength = _input.readI32();
    List<BlockchainAddress> oracleNodes = new ArrayList<>();
    for (int oracleNodes_i = 0; oracleNodes_i < oracleNodes_vecLength; oracleNodes_i++) {
      BlockchainAddress oracleNodes_elem = _input.readAddress();
      oracleNodes.add(oracleNodes_elem);
    }
    Deposit deposit = deserializeDeposit(_input);
    byte[] depositSum = _input.readBytes(32);
    byte[] maximumDepositPerEpoch = _input.readBytes(32);
    var disputes_mapLength = _input.readI32();
    Map<Long, Dispute> disputes = new HashMap<>();
    for (int disputes_i = 0; disputes_i < disputes_mapLength; disputes_i++) {
      long disputes_key = _input.readU64();
      Dispute disputes_value = deserializeDispute(_input);
      disputes.put(disputes_key, disputes_value);
    }
    long oracleNonce = _input.readU64();
    var epochs_mapLength = _input.readI32();
    Map<Long, Epoch> epochs = new HashMap<>();
    for (int epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      long epochs_key = _input.readU64();
      Epoch epochs_value = deserializeEpoch(_input);
      epochs.put(epochs_key, epochs_value);
    }
    return new ByocIncomingContractState(largeOracleAddress, depositNonce, deposits, symbol, oracleNodes, deposit, depositSum, maximumDepositPerEpoch, disputes, oracleNonce, epochs);
  }
  public ByocIncomingContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeByocIncomingContractState(input);
  }

  private DepositAction deserializeDepositAction(AbiInput _input) {
    long depositNonce = _input.readU64();
    BlockchainAddress receiver = _input.readAddress();
    byte[] amount = _input.readBytes(32);
    return new DepositAction(depositNonce, receiver, amount);
  }

  private DisputeCreateAction deserializeDisputeCreateAction(AbiInput _input) {
    Transaction transaction = deserializeTransaction(_input);
    return new DisputeCreateAction(transaction);
  }

  private DisputeCounterClaimAction deserializeDisputeCounterClaimAction(AbiInput _input) {
    Transaction transaction = deserializeTransaction(_input);
    return new DisputeCounterClaimAction(transaction);
  }

  private DisputeResultAction deserializeDisputeResultAction(AbiInput _input) {
    long disputeNonce = _input.readU64();
    int disputeResult = _input.readU32();
    return new DisputeResultAction(disputeNonce, disputeResult);
  }

  private UpdateOracleAction deserializeUpdateOracleAction(AbiInput _input) {
    var newOracles_vecLength = _input.readI32();
    List<BlockchainAddress> newOracles = new ArrayList<>();
    for (int newOracles_i = 0; newOracles_i < newOracles_vecLength; newOracles_i++) {
      BlockchainAddress newOracles_elem = _input.readAddress();
      newOracles.add(newOracles_elem);
    }
    var publicKeys_vecLength = _input.readI32();
    List<byte[]> publicKeys = new ArrayList<>();
    for (int publicKeys_i = 0; publicKeys_i < publicKeys_vecLength; publicKeys_i++) {
      byte[] publicKeys_elem = _input.readBytes(33);
      publicKeys.add(publicKeys_elem);
    }
    return new UpdateOracleAction(newOracles, publicKeys);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    BlockchainAddress largeOracleAddress = _input.readAddress();
    String symbol = _input.readString();
    byte[] maximumDepositPerEpoch = _input.readBytes(32);
    return new InitInit(largeOracleAddress, symbol, maximumDepositPerEpoch);
  }


  @AbiGenerated
  public record Deposit(BlockchainAddress receiver, long amount, int signaturesRegistered) {
  }

  @AbiGenerated
  public record Dispute(BlockchainAddress challenger, List<Transaction> claims, int votingResult) {
  }

  @AbiGenerated
  public record Epoch(long fromDepositNonce, long toDepositNonce, List<BlockchainAddress> oracles) {
  }

  @AbiGenerated
  public record Transaction(long nonce, BlockchainAddress receiver, byte[] amount) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(nonce);
      _out.writeAddress(receiver);
      if (amount.length != 32) {
        throw new RuntimeException("Length of amount does not match expected 32");
      }
      _out.writeBytes(amount);
    }
  }

  @AbiGenerated
  public record ByocIncomingContractState(BlockchainAddress largeOracleAddress, long depositNonce, Map<Long, Deposit> deposits, String symbol, List<BlockchainAddress> oracleNodes, Deposit deposit, byte[] depositSum, byte[] maximumDepositPerEpoch, Map<Long, Dispute> disputes, long oracleNonce, Map<Long, Epoch> epochs) {
    public static ByocIncomingContractState deserialize(byte[] bytes) {
      return ByocIncoming_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] init(BlockchainAddress largeOracleAddress, String symbol, byte[] maximumDepositPerEpoch) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(largeOracleAddress);
      _out.writeString(symbol);
      if (maximumDepositPerEpoch.length != 32) {
        throw new RuntimeException("Length of maximumDepositPerEpoch does not match expected 32");
      }
      _out.writeBytes(maximumDepositPerEpoch);
    });
  }

  public static byte[] deposit(long depositNonce, BlockchainAddress receiver, byte[] amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeU64(depositNonce);
      _out.writeAddress(receiver);
      if (amount.length != 32) {
        throw new RuntimeException("Length of amount does not match expected 32");
      }
      _out.writeBytes(amount);
    });
  }

  public static byte[] disputeCreate(Transaction transaction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      transaction.serialize(_out);
    });
  }

  public static byte[] disputeCounterClaim(Transaction transaction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      transaction.serialize(_out);
    });
  }

  public static byte[] disputeResult(long disputeNonce, int disputeResult) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeU64(disputeNonce);
      _out.writeU32(disputeResult);
    });
  }

  public static byte[] updateOracle(List<BlockchainAddress> newOracles, List<byte[]> publicKeys) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeI32(newOracles.size());
      for (BlockchainAddress newOracles_vec : newOracles) {
        _out.writeAddress(newOracles_vec);
      }
      _out.writeI32(publicKeys.size());
      for (byte[] publicKeys_vec : publicKeys) {
        if (publicKeys_vec.length != 33) {
          throw new RuntimeException("Length of publicKeys_vec does not match expected 33");
        }
        _out.writeBytes(publicKeys_vec);
      }
    });
  }

  public static ByocIncomingContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ByocIncoming_SDK_13_3_0(client, address).deserializeByocIncomingContractState(input);
  }
  
  public static ByocIncomingContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ByocIncomingContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record DepositAction(long depositNonce, BlockchainAddress receiver, byte[] amount) implements Action {
  }
  @AbiGenerated
  public record DisputeCreateAction(Transaction transaction) implements Action {
  }
  @AbiGenerated
  public record DisputeCounterClaimAction(Transaction transaction) implements Action {
  }
  @AbiGenerated
  public record DisputeResultAction(long disputeNonce, int disputeResult) implements Action {
  }
  @AbiGenerated
  public record UpdateOracleAction(List<BlockchainAddress> newOracles, List<byte[]> publicKeys) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocIncoming_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeDepositAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeDisputeCreateAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeDisputeCounterClaimAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeDisputeResultAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeUpdateOracleAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(BlockchainAddress largeOracleAddress, String symbol, byte[] maximumDepositPerEpoch) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocIncoming_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
