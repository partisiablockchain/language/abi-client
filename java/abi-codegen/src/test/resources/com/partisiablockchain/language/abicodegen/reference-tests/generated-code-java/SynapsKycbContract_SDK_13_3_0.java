// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class SynapsKycbContract_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public SynapsKycbContract_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private KycRegistration deserializeKycRegistration(AbiInput _input) {
    String name = _input.readString();
    String address = _input.readString();
    String city = _input.readString();
    int country = _input.readU32();
    int serverJurisdiction = _input.readU32();
    String website = _input.readString();
    return new KycRegistration(name, address, city, country, serverJurisdiction, website);
  }
  private BlsPublicKey deserializeBlsPublicKey(AbiInput _input) {
    byte[] publicKeyValue = _input.readBytes(96);
    return new BlsPublicKey(publicKeyValue);
  }
  private BlsSignature deserializeBlsSignature(AbiInput _input) {
    byte[] signatureValue = _input.readBytes(48);
    return new BlsSignature(signatureValue);
  }
  private SynapsKycbContractState deserializeSynapsKycbContractState(AbiInput _input) {
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    byte[] verificationKey = _input.readBytes(33);
    var kycRegistrations_mapLength = _input.readI32();
    Map<BlockchainAddress, KycRegistration> kycRegistrations = new HashMap<>();
    for (int kycRegistrations_i = 0; kycRegistrations_i < kycRegistrations_mapLength; kycRegistrations_i++) {
      BlockchainAddress kycRegistrations_key = _input.readAddress();
      KycRegistration kycRegistrations_value = deserializeKycRegistration(_input);
      kycRegistrations.put(kycRegistrations_key, kycRegistrations_value);
    }
    return new SynapsKycbContractState(bpOrchestrationContract, verificationKey, kycRegistrations);
  }
  private Signature deserializeSignature(AbiInput _input) {
    byte recoveryId = _input.readU8();
    byte[] valueR = _input.readBytes(32);
    byte[] valueS = _input.readBytes(32);
    return new Signature(recoveryId, valueR, valueS);
  }
  public SynapsKycbContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeSynapsKycbContractState(input);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    String jsonString = _input.readString();
    Signature synapsSignature = deserializeSignature(_input);
    String website = _input.readString();
    int serverJurisdiction = _input.readU32();
    byte[] producerPublicKey = _input.readBytes(33);
    BlsPublicKey producerBlsKey = deserializeBlsPublicKey(_input);
    BlsSignature popSignature = deserializeBlsSignature(_input);
    return new ApproveAction(jsonString, synapsSignature, website, serverJurisdiction, producerPublicKey, producerBlsKey, popSignature);
  }

  private KycRegisterAction deserializeKycRegisterAction(AbiInput _input) {
    String name = _input.readString();
    String address = _input.readString();
    String city = _input.readString();
    int country = _input.readU32();
    String website = _input.readString();
    int serverJurisdiction = _input.readU32();
    return new KycRegisterAction(name, address, city, country, website, serverJurisdiction);
  }

  private KycSubmitAction deserializeKycSubmitAction(AbiInput _input) {
    String jsonString = _input.readString();
    Signature synapsSignature = deserializeSignature(_input);
    byte[] producerPublicKey = _input.readBytes(33);
    BlsPublicKey producerBlsKey = deserializeBlsPublicKey(_input);
    BlsSignature popSignature = deserializeBlsSignature(_input);
    return new KycSubmitAction(jsonString, synapsSignature, producerPublicKey, producerBlsKey, popSignature);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    byte[] verificationKey = _input.readBytes(33);
    return new InitInit(bpOrchestrationContract, verificationKey);
  }


  @AbiGenerated
  public record KycRegistration(String name, String address, String city, int country, int serverJurisdiction, String website) {
  }

  @AbiGenerated
  public record BlsPublicKey(byte[] publicKeyValue) {
    public void serialize(AbiOutput _out) {
      if (publicKeyValue.length != 96) {
        throw new RuntimeException("Length of publicKeyValue does not match expected 96");
      }
      _out.writeBytes(publicKeyValue);
    }
  }

  @AbiGenerated
  public record BlsSignature(byte[] signatureValue) {
    public void serialize(AbiOutput _out) {
      if (signatureValue.length != 48) {
        throw new RuntimeException("Length of signatureValue does not match expected 48");
      }
      _out.writeBytes(signatureValue);
    }
  }

  @AbiGenerated
  public record SynapsKycbContractState(BlockchainAddress bpOrchestrationContract, byte[] verificationKey, Map<BlockchainAddress, KycRegistration> kycRegistrations) {
    public static SynapsKycbContractState deserialize(byte[] bytes) {
      return SynapsKycbContract_SDK_13_3_0.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Signature(byte recoveryId, byte[] valueR, byte[] valueS) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(recoveryId);
      if (valueR.length != 32) {
        throw new RuntimeException("Length of valueR does not match expected 32");
      }
      _out.writeBytes(valueR);
      if (valueS.length != 32) {
        throw new RuntimeException("Length of valueS does not match expected 32");
      }
      _out.writeBytes(valueS);
    }
  }

  public static byte[] init(BlockchainAddress bpOrchestrationContract, byte[] verificationKey) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(bpOrchestrationContract);
      if (verificationKey.length != 33) {
        throw new RuntimeException("Length of verificationKey does not match expected 33");
      }
      _out.writeBytes(verificationKey);
    });
  }

  public static byte[] approve(String jsonString, Signature synapsSignature, String website, int serverJurisdiction, byte[] producerPublicKey, BlsPublicKey producerBlsKey, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeString(jsonString);
      synapsSignature.serialize(_out);
      _out.writeString(website);
      _out.writeU32(serverJurisdiction);
      if (producerPublicKey.length != 33) {
        throw new RuntimeException("Length of producerPublicKey does not match expected 33");
      }
      _out.writeBytes(producerPublicKey);
      producerBlsKey.serialize(_out);
      popSignature.serialize(_out);
    });
  }

  public static byte[] kycRegister(String name, String address, String city, int country, String website, int serverJurisdiction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeString(name);
      _out.writeString(address);
      _out.writeString(city);
      _out.writeU32(country);
      _out.writeString(website);
      _out.writeU32(serverJurisdiction);
    });
  }

  public static byte[] kycSubmit(String jsonString, Signature synapsSignature, byte[] producerPublicKey, BlsPublicKey producerBlsKey, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeString(jsonString);
      synapsSignature.serialize(_out);
      if (producerPublicKey.length != 33) {
        throw new RuntimeException("Length of producerPublicKey does not match expected 33");
      }
      _out.writeBytes(producerPublicKey);
      producerBlsKey.serialize(_out);
      popSignature.serialize(_out);
    });
  }

  public static SynapsKycbContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new SynapsKycbContract_SDK_13_3_0(client, address).deserializeSynapsKycbContractState(input);
  }
  
  public static SynapsKycbContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static SynapsKycbContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ApproveAction(String jsonString, Signature synapsSignature, String website, int serverJurisdiction, byte[] producerPublicKey, BlsPublicKey producerBlsKey, BlsSignature popSignature) implements Action {
  }
  @AbiGenerated
  public record KycRegisterAction(String name, String address, String city, int country, String website, int serverJurisdiction) implements Action {
  }
  @AbiGenerated
  public record KycSubmitAction(String jsonString, Signature synapsSignature, byte[] producerPublicKey, BlsPublicKey producerBlsKey, BlsSignature popSignature) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new SynapsKycbContract_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeApproveAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeKycRegisterAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeKycSubmitAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(BlockchainAddress bpOrchestrationContract, byte[] verificationKey) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new SynapsKycbContract_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
