// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class AuctionContract_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public AuctionContract_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Bid deserializeBid(AbiInput _input) {
    BlockchainAddress bidder = _input.readAddress();
    long amount = _input.readU64();
    return new Bid(bidder, amount);
  }
  private TokenClaim deserializeTokenClaim(AbiInput _input) {
    long tokensForBidding = _input.readU64();
    long tokensForSale = _input.readU64();
    return new TokenClaim(tokensForBidding, tokensForSale);
  }
  private AuctionContractState deserializeAuctionContractState(AbiInput _input) {
    BlockchainAddress contractOwner = _input.readAddress();
    long startTimeMillis = _input.readI64();
    long endTimeMillis = _input.readI64();
    long tokenAmountForSale = _input.readU64();
    BlockchainAddress tokenForSale = _input.readAddress();
    BlockchainAddress tokenForBidding = _input.readAddress();
    Bid highestBidder = deserializeBid(_input);
    long reservePrice = _input.readU64();
    long minIncrement = _input.readU64();
    var claimMap_mapLength = _input.readI32();
    Map<BlockchainAddress, TokenClaim> claimMap = new HashMap<>();
    for (int claimMap_i = 0; claimMap_i < claimMap_mapLength; claimMap_i++) {
      BlockchainAddress claimMap_key = _input.readAddress();
      TokenClaim claimMap_value = deserializeTokenClaim(_input);
      claimMap.put(claimMap_key, claimMap_value);
    }
    byte status = _input.readU8();
    return new AuctionContractState(contractOwner, startTimeMillis, endTimeMillis, tokenAmountForSale, tokenForSale, tokenForBidding, highestBidder, reservePrice, minIncrement, claimMap, status);
  }
  public AuctionContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeAuctionContractState(input);
  }

  private StartAction deserializeStartAction(AbiInput _input) {
    return new StartAction();
  }

  private BidAction deserializeBidAction(AbiInput _input) {
    long bidAmount = _input.readU64();
    return new BidAction(bidAmount);
  }

  private ClaimAction deserializeClaimAction(AbiInput _input) {
    return new ClaimAction();
  }

  private ExecuteAction deserializeExecuteAction(AbiInput _input) {
    return new ExecuteAction();
  }

  private CancelAction deserializeCancelAction(AbiInput _input) {
    return new CancelAction();
  }

  private StartCallbackCallback deserializeStartCallbackCallback(AbiInput _input) {
    return new StartCallbackCallback();
  }

  private BidCallbackCallback deserializeBidCallbackCallback(AbiInput _input) {
    Bid bid = deserializeBid(_input);
    return new BidCallbackCallback(bid);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    long tokenAmountForSale = _input.readU64();
    BlockchainAddress tokenForSale = _input.readAddress();
    BlockchainAddress tokenForBidding = _input.readAddress();
    long reservePrice = _input.readU64();
    long minIncrement = _input.readU64();
    int auctionDurationHours = _input.readU32();
    return new InitializeInit(tokenAmountForSale, tokenForSale, tokenForBidding, reservePrice, minIncrement, auctionDurationHours);
  }


  @AbiGenerated
  public record Bid(BlockchainAddress bidder, long amount) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(bidder);
      _out.writeU64(amount);
    }
  }

  @AbiGenerated
  public record TokenClaim(long tokensForBidding, long tokensForSale) {
  }

  @AbiGenerated
  public record AuctionContractState(BlockchainAddress contractOwner, long startTimeMillis, long endTimeMillis, long tokenAmountForSale, BlockchainAddress tokenForSale, BlockchainAddress tokenForBidding, Bid highestBidder, long reservePrice, long minIncrement, Map<BlockchainAddress, TokenClaim> claimMap, byte status) {
    public static AuctionContractState deserialize(byte[] bytes) {
      return AuctionContract_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(long tokenAmountForSale, BlockchainAddress tokenForSale, BlockchainAddress tokenForBidding, long reservePrice, long minIncrement, int auctionDurationHours) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU64(tokenAmountForSale);
      _out.writeAddress(tokenForSale);
      _out.writeAddress(tokenForBidding);
      _out.writeU64(reservePrice);
      _out.writeU64(minIncrement);
      _out.writeU32(auctionDurationHours);
    });
  }

  public static byte[] start() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static byte[] bid(long bidAmount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeU64(bidAmount);
    });
  }

  public static byte[] claim() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] execute() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static byte[] cancel() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
    });
  }

  public static byte[] startCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] bidCallback(Bid bid) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      bid.serialize(_out);
    });
  }

  public static AuctionContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new AuctionContract_SDK_10_1_0(client, address).deserializeAuctionContractState(input);
  }
  
  public static AuctionContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static AuctionContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record StartAction() implements Action {
  }
  @AbiGenerated
  public record BidAction(long bidAmount) implements Action {
  }
  @AbiGenerated
  public record ClaimAction() implements Action {
  }
  @AbiGenerated
  public record ExecuteAction() implements Action {
  }
  @AbiGenerated
  public record CancelAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new AuctionContract_SDK_10_1_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeStartAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeBidAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeClaimAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeExecuteAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeCancelAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record StartCallbackCallback() implements Callback {
  }
  @AbiGenerated
  public record BidCallbackCallback(Bid bid) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new AuctionContract_SDK_10_1_0(null, null);
    if (shortname.equals("02")) {
      return contract.deserializeStartCallbackCallback(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeBidCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(long tokenAmountForSale, BlockchainAddress tokenForSale, BlockchainAddress tokenForBidding, long reservePrice, long minIncrement, int auctionDurationHours) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new AuctionContract_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
