// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ByocTokenContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializePendingTransfer(_input: AbiInput): PendingTransfer {
    const spender: BlockchainAddress = _input.readAddress();
    const from: BlockchainAddress = _input.readAddress();
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { spender, from, to, amount };
  }
  public deserializeByocTokenContractState(_input: AbiInput): ByocTokenContractState {
    const byocSymbol: string = _input.readString();
    const allowed_mapLength = _input.readI32();
    const allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>> = new Map();
    for (let allowed_i = 0; allowed_i < allowed_mapLength; allowed_i++) {
      const allowed_key: BlockchainAddress = _input.readAddress();
      const allowed_value_mapLength = _input.readI32();
      const allowed_value: Map<BlockchainAddress, BN> = new Map();
      for (let allowed_value_i = 0; allowed_value_i < allowed_value_mapLength; allowed_value_i++) {
        const allowed_value_key: BlockchainAddress = _input.readAddress();
        const allowed_value_value: BN = _input.readUnsignedBigInteger(16);
        allowed_value.set(allowed_value_key, allowed_value_value);
      }
      allowed.set(allowed_key, allowed_value);
    }
    const pendingTransfers_mapLength = _input.readI32();
    const pendingTransfers: Map<Buffer, PendingTransfer> = new Map();
    for (let pendingTransfers_i = 0; pendingTransfers_i < pendingTransfers_mapLength; pendingTransfers_i++) {
      const pendingTransfers_key: Buffer = _input.readBytes(32);
      const pendingTransfers_value: PendingTransfer = this.deserializePendingTransfer(_input);
      pendingTransfers.set(pendingTransfers_key, pendingTransfers_value);
    }
    return { byocSymbol, allowed, pendingTransfers };
  }
  public async getState(): Promise<ByocTokenContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeByocTokenContractState(input);
  }

  public deserializeTransferAction(_input: AbiInput): TransferAction {
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "transfer", to, amount };
  }

  public deserializeTransferFromAction(_input: AbiInput): TransferFromAction {
    const from: BlockchainAddress = _input.readAddress();
    const to: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "transfer_from", from, to, amount };
  }

  public deserializeApproveAction(_input: AbiInput): ApproveAction {
    const spender: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "approve", spender, amount };
  }

  public deserializeAbortAction(_input: AbiInput): AbortAction {
    const transactionHash: Buffer = _input.readBytes(32);
    return { discriminant: "abort", transactionHash };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const symbol: string = _input.readString();
    return { discriminant: "initialize", symbol };
  }

}
export interface PendingTransfer {
  spender: BlockchainAddress;
  from: BlockchainAddress;
  to: BlockchainAddress;
  amount: BN;
}

export interface ByocTokenContractState {
  byocSymbol: string;
  allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>>;
  pendingTransfers: Map<Buffer, PendingTransfer>;
}

export function initialize(symbol: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeString(symbol);
  });
}

export function transfer(to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function transferFrom(from: BlockchainAddress, to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(from);
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function approve(spender: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeAddress(spender);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function abort(transactionHash: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    if (transactionHash.length != 32) {
      throw new Error("Length of transactionHash does not match expected 32");
    }
    _out.writeBytes(transactionHash);
  });
}

export function deserializeState(state: StateWithClient): ByocTokenContractState;
export function deserializeState(bytes: Buffer): ByocTokenContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ByocTokenContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ByocTokenContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ByocTokenContract_SDK_13_3_0(client, address).deserializeByocTokenContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ByocTokenContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeByocTokenContractState(input);
  }
}

export type Action =
  | TransferAction
  | TransferFromAction
  | ApproveAction
  | AbortAction;

export interface TransferAction {
  discriminant: "transfer";
  to: BlockchainAddress;
  amount: BN;
}
export interface TransferFromAction {
  discriminant: "transfer_from";
  from: BlockchainAddress;
  to: BlockchainAddress;
  amount: BN;
}
export interface ApproveAction {
  discriminant: "approve";
  spender: BlockchainAddress;
  amount: BN;
}
export interface AbortAction {
  discriminant: "abort";
  transactionHash: Buffer;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocTokenContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeTransferAction(input);
  } else if (shortname === "03") {
    return contract.deserializeTransferFromAction(input);
  } else if (shortname === "05") {
    return contract.deserializeApproveAction(input);
  } else if (shortname === "06") {
    return contract.deserializeAbortAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  symbol: string;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocTokenContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

