// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class LargeOracleContract_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public LargeOracleContract_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Dispute deserializeDispute(AbiInput _input) {
    BlockchainAddress challenger = _input.readAddress();
    int contractResultInvocation = _input.readU32();
    int contractCounterClaimInvocation = _input.readU32();
    var votes_vecLength = _input.readI32();
    List<Integer> votes = new ArrayList<>();
    for (int votes_i = 0; votes_i < votes_vecLength; votes_i++) {
      int votes_elem = _input.readU32();
      votes.add(votes_elem);
    }
    var counterClaimChallengers_vecLength = _input.readI32();
    List<BlockchainAddress> counterClaimChallengers = new ArrayList<>();
    for (int counterClaimChallengers_i = 0; counterClaimChallengers_i < counterClaimChallengers_vecLength; counterClaimChallengers_i++) {
      BlockchainAddress counterClaimChallengers_elem = _input.readAddress();
      counterClaimChallengers.add(counterClaimChallengers_elem);
    }
    return new Dispute(challenger, contractResultInvocation, contractCounterClaimInvocation, votes, counterClaimChallengers);
  }
  private OracleDisputeId deserializeOracleDisputeId(AbiInput _input) {
    BlockchainAddress smallOracle = _input.readAddress();
    long oracleId = _input.readU64();
    long disputeId = _input.readU64();
    return new OracleDisputeId(smallOracle, oracleId, disputeId);
  }
  private OracleMember deserializeOracleMember(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    byte[] key = _input.readBytes(33);
    return new OracleMember(address, key);
  }
  private SigningRequest deserializeSigningRequest(AbiInput _input) {
    int nonce = _input.readU32();
    Hash messageHash = _input.readHash();
    Hash transactionHash = _input.readHash();
    return new SigningRequest(nonce, messageHash, transactionHash);
  }
  private StakedTokens deserializeStakedTokens(AbiInput _input) {
    long freeTokens = _input.readU64();
    var lockedToOracle_mapLength = _input.readI32();
    Map<BlockchainAddress, Long> lockedToOracle = new HashMap<>();
    for (int lockedToOracle_i = 0; lockedToOracle_i < lockedToOracle_mapLength; lockedToOracle_i++) {
      BlockchainAddress lockedToOracle_key = _input.readAddress();
      long lockedToOracle_value = _input.readU64();
      lockedToOracle.put(lockedToOracle_key, lockedToOracle_value);
    }
    var pendingTokens_mapLength = _input.readI32();
    Map<BlockchainAddress, Long> pendingTokens = new HashMap<>();
    for (int pendingTokens_i = 0; pendingTokens_i < pendingTokens_mapLength; pendingTokens_i++) {
      BlockchainAddress pendingTokens_key = _input.readAddress();
      long pendingTokens_value = _input.readU64();
      pendingTokens.put(pendingTokens_key, pendingTokens_value);
    }
    var lockedToDispute_mapLength = _input.readI32();
    Map<OracleDisputeId, Long> lockedToDispute = new HashMap<>();
    for (int lockedToDispute_i = 0; lockedToDispute_i < lockedToDispute_mapLength; lockedToDispute_i++) {
      OracleDisputeId lockedToDispute_key = deserializeOracleDisputeId(_input);
      long lockedToDispute_value = _input.readU64();
      lockedToDispute.put(lockedToDispute_key, lockedToDispute_value);
    }
    return new StakedTokens(freeTokens, lockedToOracle, pendingTokens, lockedToDispute);
  }
  private LargeOracleContractState deserializeLargeOracleContractState(AbiInput _input) {
    var oracleMembers_vecLength = _input.readI32();
    List<OracleMember> oracleMembers = new ArrayList<>();
    for (int oracleMembers_i = 0; oracleMembers_i < oracleMembers_vecLength; oracleMembers_i++) {
      OracleMember oracleMembers_elem = deserializeOracleMember(_input);
      oracleMembers.add(oracleMembers_elem);
    }
    byte[] oracleKey = _input.readBytes(33);
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    BlockchainAddress governanceUpdates = _input.readAddress();
    var stakedTokens_mapLength = _input.readI32();
    Map<BlockchainAddress, StakedTokens> stakedTokens = new HashMap<>();
    for (int stakedTokens_i = 0; stakedTokens_i < stakedTokens_mapLength; stakedTokens_i++) {
      BlockchainAddress stakedTokens_key = _input.readAddress();
      StakedTokens stakedTokens_value = deserializeStakedTokens(_input);
      stakedTokens.put(stakedTokens_key, stakedTokens_value);
    }
    var activeDisputes_mapLength = _input.readI32();
    Map<OracleDisputeId, Dispute> activeDisputes = new HashMap<>();
    for (int activeDisputes_i = 0; activeDisputes_i < activeDisputes_mapLength; activeDisputes_i++) {
      OracleDisputeId activeDisputes_key = deserializeOracleDisputeId(_input);
      Dispute activeDisputes_value = deserializeDispute(_input);
      activeDisputes.put(activeDisputes_key, activeDisputes_value);
    }
    var pendingMessages_vecLength = _input.readI32();
    List<SigningRequest> pendingMessages = new ArrayList<>();
    for (int pendingMessages_i = 0; pendingMessages_i < pendingMessages_vecLength; pendingMessages_i++) {
      SigningRequest pendingMessages_elem = deserializeSigningRequest(_input);
      pendingMessages.add(pendingMessages_elem);
    }
    int currentMessageNonce = _input.readU32();
    int initialMessageNonce = _input.readU32();
    var signedMessages_mapLength = _input.readI32();
    Map<SigningRequest, Signature> signedMessages = new HashMap<>();
    for (int signedMessages_i = 0; signedMessages_i < signedMessages_mapLength; signedMessages_i++) {
      SigningRequest signedMessages_key = deserializeSigningRequest(_input);
      Signature signedMessages_value = _input.readSignature();
      signedMessages.put(signedMessages_key, signedMessages_value);
    }
    return new LargeOracleContractState(oracleMembers, oracleKey, bpOrchestrationContract, governanceUpdates, stakedTokens, activeDisputes, pendingMessages, currentMessageNonce, initialMessageNonce, signedMessages);
  }
  public LargeOracleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeLargeOracleContractState(input);
  }

  private CreateDisputePollAction deserializeCreateDisputePollAction(AbiInput _input) {
    BlockchainAddress disputeChallenger = _input.readAddress();
    long disputeTokenCost = _input.readU64();
    long oracleId = _input.readU64();
    long disputeId = _input.readU64();
    int smallOraclesResultInvocation = _input.readU32();
    int smallOraclesCounterClaimInvocation = _input.readU32();
    return new CreateDisputePollAction(disputeChallenger, disputeTokenCost, oracleId, disputeId, smallOraclesResultInvocation, smallOraclesCounterClaimInvocation);
  }

  private CreateDisputePollWithErrorThrowAction deserializeCreateDisputePollWithErrorThrowAction(AbiInput _input) {
    BlockchainAddress disputeChallenger = _input.readAddress();
    long disputeCost = _input.readI64();
    long oracleId = _input.readI64();
    long disputeId = _input.readI64();
    int smallOracleResultInvocation = _input.readU32();
    int smallOracleCounterClaimInvocation = _input.readU32();
    return new CreateDisputePollWithErrorThrowAction(disputeChallenger, disputeCost, oracleId, disputeId, smallOracleResultInvocation, smallOracleCounterClaimInvocation);
  }

  private AddDisputeCounterClaimAction deserializeAddDisputeCounterClaimAction(AbiInput _input) {
    BlockchainAddress smallOracle = _input.readAddress();
    long oracleId = _input.readU64();
    long disputeId = _input.readU64();
    var passThrough_vecLength = _input.readI32();
    byte[] passThrough = _input.readBytes(passThrough_vecLength);
    return new AddDisputeCounterClaimAction(smallOracle, oracleId, disputeId, passThrough);
  }

  private VoteOnDisputeAction deserializeVoteOnDisputeAction(AbiInput _input) {
    BlockchainAddress smallOracle = _input.readAddress();
    long oracleId = _input.readU64();
    long disputeId = _input.readU64();
    int vote = _input.readU32();
    return new VoteOnDisputeAction(smallOracle, oracleId, disputeId, vote);
  }

  private BurnStakedTokensAction deserializeBurnStakedTokensAction(AbiInput _input) {
    var oracles_vecLength = _input.readI32();
    List<BlockchainAddress> oracles = new ArrayList<>();
    for (int oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      BlockchainAddress oracles_elem = _input.readAddress();
      oracles.add(oracles_elem);
    }
    long amount = _input.readU64();
    return new BurnStakedTokensAction(oracles, amount);
  }

  private ReplaceLargeOracleAction deserializeReplaceLargeOracleAction(AbiInput _input) {
    var newKeys_vecLength = _input.readI32();
    List<byte[]> newKeys = new ArrayList<>();
    for (int newKeys_i = 0; newKeys_i < newKeys_vecLength; newKeys_i++) {
      byte[] newKeys_elem = _input.readBytes(33);
      newKeys.add(newKeys_elem);
    }
    var newAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> newAddresses = new ArrayList<>();
    for (int newAddresses_i = 0; newAddresses_i < newAddresses_vecLength; newAddresses_i++) {
      BlockchainAddress newAddresses_elem = _input.readAddress();
      newAddresses.add(newAddresses_elem);
    }
    byte[] newPublicKey = _input.readBytes(33);
    return new ReplaceLargeOracleAction(newKeys, newAddresses, newPublicKey);
  }

  private RequestNewSmallOracleAction deserializeRequestNewSmallOracleAction(AbiInput _input) {
    long requiredStake = _input.readU64();
    int callback = _input.readU32();
    var additionalData_vecLength = _input.readI32();
    byte[] additionalData = _input.readBytes(additionalData_vecLength);
    return new RequestNewSmallOracleAction(requiredStake, callback, additionalData);
  }

  private AddSignatureAction deserializeAddSignatureAction(AbiInput _input) {
    Hash message = _input.readHash();
    int nonce = _input.readU32();
    Signature signature = _input.readSignature();
    return new AddSignatureAction(message, nonce, signature);
  }

  private ChangeExchangeRateAction deserializeChangeExchangeRateAction(AbiInput _input) {
    int numerator = _input.readU32();
    int denominator = _input.readU32();
    return new ChangeExchangeRateAction(numerator, denominator);
  }

  private RecalibrateByocTwinsAction deserializeRecalibrateByocTwinsAction(AbiInput _input) {
    String byocTwinSymbol = _input.readString();
    byte[] byocTwinAmount = _input.readBytes(32);
    var oracles_vecLength = _input.readI32();
    List<BlockchainAddress> oracles = new ArrayList<>();
    for (int oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      BlockchainAddress oracles_elem = _input.readAddress();
      oracles.add(oracles_elem);
    }
    return new RecalibrateByocTwinsAction(byocTwinSymbol, byocTwinAmount, oracles);
  }

  private SellByocTwinsAction deserializeSellByocTwinsAction(AbiInput _input) {
    String byocTwinSymbol = _input.readString();
    byte[] byocTwinAmount = _input.readBytes(32);
    long expectedMpcToBuy = _input.readU64();
    return new SellByocTwinsAction(byocTwinSymbol, byocTwinAmount, expectedMpcToBuy);
  }

  private AssociateTokensToContractAction deserializeAssociateTokensToContractAction(AbiInput _input) {
    long amount = _input.readU64();
    return new AssociateTokensToContractAction(amount);
  }

  private DisassociateTokensFromContractAction deserializeDisassociateTokensFromContractAction(AbiInput _input) {
    long amount = _input.readU64();
    return new DisassociateTokensFromContractAction(amount);
  }

  private LockTokensToPriceOracleAction deserializeLockTokensToPriceOracleAction(AbiInput _input) {
    long tokensToLock = _input.readU64();
    BlockchainAddress priceOracleNode = _input.readAddress();
    return new LockTokensToPriceOracleAction(tokensToLock, priceOracleNode);
  }

  private UnlockTokensToPriceOracleAction deserializeUnlockTokensToPriceOracleAction(AbiInput _input) {
    long tokensToUnlock = _input.readU64();
    BlockchainAddress priceOracleNode = _input.readAddress();
    return new UnlockTokensToPriceOracleAction(tokensToUnlock, priceOracleNode);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    var keys_vecLength = _input.readI32();
    List<byte[]> keys = new ArrayList<>();
    for (int keys_i = 0; keys_i < keys_vecLength; keys_i++) {
      byte[] keys_elem = _input.readBytes(33);
      keys.add(keys_elem);
    }
    var addresses_vecLength = _input.readI32();
    List<BlockchainAddress> addresses = new ArrayList<>();
    for (int addresses_i = 0; addresses_i < addresses_vecLength; addresses_i++) {
      BlockchainAddress addresses_elem = _input.readAddress();
      addresses.add(addresses_elem);
    }
    byte[] oracleKey = _input.readBytes(33);
    BlockchainAddress bpOrchestrationContract = _input.readAddress();
    BlockchainAddress governanceUpdates = _input.readAddress();
    return new InitInit(keys, addresses, oracleKey, bpOrchestrationContract, governanceUpdates);
  }


  @AbiGenerated
  public record Dispute(BlockchainAddress challenger, int contractResultInvocation, int contractCounterClaimInvocation, List<Integer> votes, List<BlockchainAddress> counterClaimChallengers) {
  }

  @AbiGenerated
  public record OracleDisputeId(BlockchainAddress smallOracle, long oracleId, long disputeId) {
  }

  @AbiGenerated
  public record OracleMember(BlockchainAddress address, byte[] key) {
  }

  @AbiGenerated
  public record SigningRequest(int nonce, Hash messageHash, Hash transactionHash) {
  }

  @AbiGenerated
  public record StakedTokens(long freeTokens, Map<BlockchainAddress, Long> lockedToOracle, Map<BlockchainAddress, Long> pendingTokens, Map<OracleDisputeId, Long> lockedToDispute) {
  }

  @AbiGenerated
  public record LargeOracleContractState(List<OracleMember> oracleMembers, byte[] oracleKey, BlockchainAddress bpOrchestrationContract, BlockchainAddress governanceUpdates, Map<BlockchainAddress, StakedTokens> stakedTokens, Map<OracleDisputeId, Dispute> activeDisputes, List<SigningRequest> pendingMessages, int currentMessageNonce, int initialMessageNonce, Map<SigningRequest, Signature> signedMessages) {
    public static LargeOracleContractState deserialize(byte[] bytes) {
      return LargeOracleContract_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] init(List<byte[]> keys, List<BlockchainAddress> addresses, byte[] oracleKey, BlockchainAddress bpOrchestrationContract, BlockchainAddress governanceUpdates) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(keys.size());
      for (byte[] keys_vec : keys) {
        if (keys_vec.length != 33) {
          throw new RuntimeException("Length of keys_vec does not match expected 33");
        }
        _out.writeBytes(keys_vec);
      }
      _out.writeI32(addresses.size());
      for (BlockchainAddress addresses_vec : addresses) {
        _out.writeAddress(addresses_vec);
      }
      if (oracleKey.length != 33) {
        throw new RuntimeException("Length of oracleKey does not match expected 33");
      }
      _out.writeBytes(oracleKey);
      _out.writeAddress(bpOrchestrationContract);
      _out.writeAddress(governanceUpdates);
    });
  }

  public static byte[] createDisputePoll(BlockchainAddress disputeChallenger, long disputeTokenCost, long oracleId, long disputeId, int smallOraclesResultInvocation, int smallOraclesCounterClaimInvocation) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeAddress(disputeChallenger);
      _out.writeU64(disputeTokenCost);
      _out.writeU64(oracleId);
      _out.writeU64(disputeId);
      _out.writeU32(smallOraclesResultInvocation);
      _out.writeU32(smallOraclesCounterClaimInvocation);
    });
  }

  public static byte[] createDisputePollWithErrorThrow(BlockchainAddress disputeChallenger, long disputeCost, long oracleId, long disputeId, int smallOracleResultInvocation, int smallOracleCounterClaimInvocation) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0e"));
      _out.writeAddress(disputeChallenger);
      _out.writeI64(disputeCost);
      _out.writeI64(oracleId);
      _out.writeI64(disputeId);
      _out.writeU32(smallOracleResultInvocation);
      _out.writeU32(smallOracleCounterClaimInvocation);
    });
  }

  public static byte[] addDisputeCounterClaim(BlockchainAddress smallOracle, long oracleId, long disputeId, byte[] passThrough) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(smallOracle);
      _out.writeU64(oracleId);
      _out.writeU64(disputeId);
      _out.writeI32(passThrough.length);
      _out.writeBytes(passThrough);
    });
  }

  public static byte[] voteOnDispute(BlockchainAddress smallOracle, long oracleId, long disputeId, int vote) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(smallOracle);
      _out.writeU64(oracleId);
      _out.writeU64(disputeId);
      _out.writeU32(vote);
    });
  }

  public static byte[] burnStakedTokens(List<BlockchainAddress> oracles, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeI32(oracles.size());
      for (BlockchainAddress oracles_vec : oracles) {
        _out.writeAddress(oracles_vec);
      }
      _out.writeU64(amount);
    });
  }

  public static byte[] replaceLargeOracle(List<byte[]> newKeys, List<BlockchainAddress> newAddresses, byte[] newPublicKey) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeI32(newKeys.size());
      for (byte[] newKeys_vec : newKeys) {
        if (newKeys_vec.length != 33) {
          throw new RuntimeException("Length of newKeys_vec does not match expected 33");
        }
        _out.writeBytes(newKeys_vec);
      }
      _out.writeI32(newAddresses.size());
      for (BlockchainAddress newAddresses_vec : newAddresses) {
        _out.writeAddress(newAddresses_vec);
      }
      if (newPublicKey.length != 33) {
        throw new RuntimeException("Length of newPublicKey does not match expected 33");
      }
      _out.writeBytes(newPublicKey);
    });
  }

  public static byte[] requestNewSmallOracle(long requiredStake, int callback, byte[] additionalData) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeU64(requiredStake);
      _out.writeU32(callback);
      _out.writeI32(additionalData.length);
      _out.writeBytes(additionalData);
    });
  }

  public static byte[] addSignature(Hash message, int nonce, Signature signature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeHash(message);
      _out.writeU32(nonce);
      _out.writeSignature(signature);
    });
  }

  public static byte[] changeExchangeRate(int numerator, int denominator) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
      _out.writeU32(numerator);
      _out.writeU32(denominator);
    });
  }

  public static byte[] recalibrateByocTwins(String byocTwinSymbol, byte[] byocTwinAmount, List<BlockchainAddress> oracles) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("08"));
      _out.writeString(byocTwinSymbol);
      if (byocTwinAmount.length != 32) {
        throw new RuntimeException("Length of byocTwinAmount does not match expected 32");
      }
      _out.writeBytes(byocTwinAmount);
      _out.writeI32(oracles.size());
      for (BlockchainAddress oracles_vec : oracles) {
        _out.writeAddress(oracles_vec);
      }
    });
  }

  public static byte[] sellByocTwins(String byocTwinSymbol, byte[] byocTwinAmount, long expectedMpcToBuy) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("09"));
      _out.writeString(byocTwinSymbol);
      if (byocTwinAmount.length != 32) {
        throw new RuntimeException("Length of byocTwinAmount does not match expected 32");
      }
      _out.writeBytes(byocTwinAmount);
      _out.writeU64(expectedMpcToBuy);
    });
  }

  public static byte[] associateTokensToContract(long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0a"));
      _out.writeU64(amount);
    });
  }

  public static byte[] disassociateTokensFromContract(long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0b"));
      _out.writeU64(amount);
    });
  }

  public static byte[] lockTokensToPriceOracle(long tokensToLock, BlockchainAddress priceOracleNode) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0c"));
      _out.writeU64(tokensToLock);
      _out.writeAddress(priceOracleNode);
    });
  }

  public static byte[] unlockTokensToPriceOracle(long tokensToUnlock, BlockchainAddress priceOracleNode) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0d"));
      _out.writeU64(tokensToUnlock);
      _out.writeAddress(priceOracleNode);
    });
  }

  public static LargeOracleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new LargeOracleContract_SDK_13_4_0(client, address).deserializeLargeOracleContractState(input);
  }
  
  public static LargeOracleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static LargeOracleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record CreateDisputePollAction(BlockchainAddress disputeChallenger, long disputeTokenCost, long oracleId, long disputeId, int smallOraclesResultInvocation, int smallOraclesCounterClaimInvocation) implements Action {
  }
  @AbiGenerated
  public record CreateDisputePollWithErrorThrowAction(BlockchainAddress disputeChallenger, long disputeCost, long oracleId, long disputeId, int smallOracleResultInvocation, int smallOracleCounterClaimInvocation) implements Action {
  }
  @AbiGenerated
  public record AddDisputeCounterClaimAction(BlockchainAddress smallOracle, long oracleId, long disputeId, byte[] passThrough) implements Action {
  }
  @AbiGenerated
  public record VoteOnDisputeAction(BlockchainAddress smallOracle, long oracleId, long disputeId, int vote) implements Action {
  }
  @AbiGenerated
  public record BurnStakedTokensAction(List<BlockchainAddress> oracles, long amount) implements Action {
  }
  @AbiGenerated
  public record ReplaceLargeOracleAction(List<byte[]> newKeys, List<BlockchainAddress> newAddresses, byte[] newPublicKey) implements Action {
  }
  @AbiGenerated
  public record RequestNewSmallOracleAction(long requiredStake, int callback, byte[] additionalData) implements Action {
  }
  @AbiGenerated
  public record AddSignatureAction(Hash message, int nonce, Signature signature) implements Action {
  }
  @AbiGenerated
  public record ChangeExchangeRateAction(int numerator, int denominator) implements Action {
  }
  @AbiGenerated
  public record RecalibrateByocTwinsAction(String byocTwinSymbol, byte[] byocTwinAmount, List<BlockchainAddress> oracles) implements Action {
  }
  @AbiGenerated
  public record SellByocTwinsAction(String byocTwinSymbol, byte[] byocTwinAmount, long expectedMpcToBuy) implements Action {
  }
  @AbiGenerated
  public record AssociateTokensToContractAction(long amount) implements Action {
  }
  @AbiGenerated
  public record DisassociateTokensFromContractAction(long amount) implements Action {
  }
  @AbiGenerated
  public record LockTokensToPriceOracleAction(long tokensToLock, BlockchainAddress priceOracleNode) implements Action {
  }
  @AbiGenerated
  public record UnlockTokensToPriceOracleAction(long tokensToUnlock, BlockchainAddress priceOracleNode) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LargeOracleContract_SDK_13_4_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeCreateDisputePollAction(input);
    } else if (shortname.equals("0e")) {
      return contract.deserializeCreateDisputePollWithErrorThrowAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeAddDisputeCounterClaimAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeVoteOnDisputeAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeBurnStakedTokensAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeReplaceLargeOracleAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeRequestNewSmallOracleAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeAddSignatureAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeChangeExchangeRateAction(input);
    } else if (shortname.equals("08")) {
      return contract.deserializeRecalibrateByocTwinsAction(input);
    } else if (shortname.equals("09")) {
      return contract.deserializeSellByocTwinsAction(input);
    } else if (shortname.equals("0a")) {
      return contract.deserializeAssociateTokensToContractAction(input);
    } else if (shortname.equals("0b")) {
      return contract.deserializeDisassociateTokensFromContractAction(input);
    } else if (shortname.equals("0c")) {
      return contract.deserializeLockTokensToPriceOracleAction(input);
    } else if (shortname.equals("0d")) {
      return contract.deserializeUnlockTokensToPriceOracleAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(List<byte[]> keys, List<BlockchainAddress> addresses, byte[] oracleKey, BlockchainAddress bpOrchestrationContract, BlockchainAddress governanceUpdates) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LargeOracleContract_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
