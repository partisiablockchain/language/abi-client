// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractSimpleStruct_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeMyStructType(_input: AbiInput): MyStructType {
    const someValue: BN = _input.readU64();
    const someVector_vecLength = _input.readI32();
    const someVector: BN[] = [];
    for (let someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      const someVector_elem: BN = _input.readU64();
      someVector.push(someVector_elem);
    }
    return { someValue, someVector };
  }
  public deserializeNonStateStructType(_input: AbiInput): NonStateStructType {
    const value: BN = _input.readU64();
    return { value };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myU64: BN = _input.readU64();
    const myStruct: MyStructType = this.deserializeMyStructType(_input);
    return { myU64, myStruct };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyU64UsingStructAction(_input: AbiInput): UpdateMyU64UsingStructAction {
    const value: NonStateStructType = this.deserializeNonStateStructType(_input);
    return { discriminant: "update_my_u64_using_struct", value };
  }

  public deserializeUpdateMyStructAction(_input: AbiInput): UpdateMyStructAction {
    const value: MyStructType = this.deserializeMyStructType(_input);
    return { discriminant: "update_my_struct", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myStruct: MyStructType = this.deserializeMyStructType(_input);
    return { discriminant: "initialize", myStruct };
  }

}
export interface MyStructType {
  someValue: BN;
  someVector: BN[];
}
function serializeMyStructType(_out: AbiOutput, _value: MyStructType): void {
  const { someValue, someVector } = _value;
  _out.writeU64(someValue);
  _out.writeI32(someVector.length);
  for (const someVector_vec of someVector) {
    _out.writeU64(someVector_vec);
  }
}

export interface NonStateStructType {
  value: BN;
}
function serializeNonStateStructType(_out: AbiOutput, _value: NonStateStructType): void {
  const { value } = _value;
  _out.writeU64(value);
}

export interface ExampleContractState {
  myU64: BN;
  myStruct: MyStructType;
}

export function initialize(myStruct: MyStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeMyStructType(_out, myStruct);
  });
}

export function updateMyU64UsingStruct(value: NonStateStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f8e385b70e", "hex"));
    serializeNonStateStructType(_out, value);
  });
}

export function updateMyStruct(value: MyStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f3b68ff40e", "hex"));
    serializeMyStructType(_out, value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractSimpleStruct_SDK_9_1_2(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractSimpleStruct_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyU64UsingStructAction
  | UpdateMyStructAction;

export interface UpdateMyU64UsingStructAction {
  discriminant: "update_my_u64_using_struct";
  value: NonStateStructType;
}
export interface UpdateMyStructAction {
  discriminant: "update_my_struct";
  value: MyStructType;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSimpleStruct_SDK_9_1_2(undefined, undefined);
  if (shortname === "f8e385b70e") {
    return contract.deserializeUpdateMyU64UsingStructAction(input);
  } else if (shortname === "f3b68ff40e") {
    return contract.deserializeUpdateMyStructAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myStruct: MyStructType;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractSimpleStruct_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

