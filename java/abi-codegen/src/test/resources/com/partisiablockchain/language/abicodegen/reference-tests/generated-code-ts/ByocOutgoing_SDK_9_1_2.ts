// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ByocOutgoing_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeUnsigned256(_input: AbiInput): Unsigned256 {
    const bits: Buffer = _input.readBytes(32);
    return { bits };
  }
  public deserializeDispute(_input: AbiInput): Dispute {
    const challenger: BlockchainAddress = _input.readAddress();
    const claims_vecLength = _input.readI32();
    const claims: DisputeTransaction[] = [];
    for (let claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      const claims_elem: DisputeTransaction = this.deserializeDisputeTransaction(_input);
      claims.push(claims_elem);
    }
    const votingResult: number = _input.readI32();
    return { challenger, claims, votingResult };
  }
  public deserializeDisputeId(_input: AbiInput): DisputeId {
    const withdrawalNonce: BN = _input.readU64();
    const oracleNonce: BN = _input.readU64();
    return { withdrawalNonce, oracleNonce };
  }
  public deserializeDisputeTransaction(_input: AbiInput): DisputeTransaction {
    const withdrawalNonce: BN = _input.readU64();
    const receiver: EthereumAddress = this.deserializeEthereumAddress(_input);
    const amount: Unsigned256 = this.deserializeUnsigned256(_input);
    const oracleNonce: BN = _input.readU64();
    const bitmask: number = _input.readU32();
    return { withdrawalNonce, receiver, amount, oracleNonce, bitmask };
  }
  public deserializeEthereumAddress(_input: AbiInput): EthereumAddress {
    const identifier: Buffer = _input.readBytes(20);
    return { identifier };
  }
  public deserializeEpoch(_input: AbiInput): Epoch {
    const fromWithdrawNonce: BN = _input.readU64();
    const toWithdrawNonce: BN = _input.readU64();
    const oracles_vecLength = _input.readI32();
    const oracles: OracleMember[] = [];
    for (let oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      const oracles_elem: OracleMember = this.deserializeOracleMember(_input);
      oracles.push(oracles_elem);
    }
    const merkleTree: Buffer = _input.readBytes(32);
    return { fromWithdrawNonce, toWithdrawNonce, oracles, merkleTree };
  }
  public deserializeOracleMember(_input: AbiInput): OracleMember {
    const identity: BlockchainAddress = _input.readAddress();
    const key: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    return { identity, key };
  }
  public deserializeOracleUpdateRequest(_input: AbiInput): OracleUpdateRequest {
    const oracleNonce: BN = _input.readU64();
    const withdrawNonce: BN = _input.readU64();
    return { oracleNonce, withdrawNonce };
  }
  public deserializeWithdrawal(_input: AbiInput): Withdrawal {
    const receiver: EthereumAddress = this.deserializeEthereumAddress(_input);
    const amount: Unsigned256 = this.deserializeUnsigned256(_input);
    const signatures_vecLength = _input.readI32();
    const signatures: Signature[] = [];
    for (let signatures_i = 0; signatures_i < signatures_vecLength; signatures_i++) {
      const signatures_elem: Signature = this.deserializeSignature(_input);
      signatures.push(signatures_elem);
    }
    return { receiver, amount, signatures };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU32();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public deserializeBlockchainPublicKey(_input: AbiInput): BlockchainPublicKey {
    const ecPoint: Buffer = _input.readBytes(33);
    return { ecPoint };
  }
  public deserializeByocOutgoingContractState(_input: AbiInput): ByocOutgoingContractState {
    const largeOracleContract: BlockchainAddress = _input.readAddress();
    const byocContract: EthereumAddress = this.deserializeEthereumAddress(_input);
    const symbol: string = _input.readString();
    const oracleMembers_vecLength = _input.readI32();
    const oracleMembers: OracleMember[] = [];
    for (let oracleMembers_i = 0; oracleMembers_i < oracleMembers_vecLength; oracleMembers_i++) {
      const oracleMembers_elem: OracleMember = this.deserializeOracleMember(_input);
      oracleMembers.push(oracleMembers_elem);
    }
    const withdrawNonce: BN = _input.readI64();
    const withdrawals_mapLength = _input.readI32();
    const withdrawals: Map<BN, Withdrawal> = new Map();
    for (let withdrawals_i = 0; withdrawals_i < withdrawals_mapLength; withdrawals_i++) {
      const withdrawals_key: BN = _input.readU64();
      const withdrawals_value: Withdrawal = this.deserializeWithdrawal(_input);
      withdrawals.set(withdrawals_key, withdrawals_value);
    }
    const withdrawMinimum: Unsigned256 = this.deserializeUnsigned256(_input);
    const withdrawalSum: Unsigned256 = this.deserializeUnsigned256(_input);
    const maximumWithdrawalPerEpoch: Unsigned256 = this.deserializeUnsigned256(_input);
    const oracleNonce: BN = _input.readI64();
    const epochs_mapLength = _input.readI32();
    const epochs: Map<BN, Epoch> = new Map();
    for (let epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      const epochs_key: BN = _input.readU64();
      const epochs_value: Epoch = this.deserializeEpoch(_input);
      epochs.set(epochs_key, epochs_value);
    }
    const disputes_mapLength = _input.readI32();
    const disputes: Map<DisputeId, Dispute> = new Map();
    for (let disputes_i = 0; disputes_i < disputes_mapLength; disputes_i++) {
      const disputes_key: DisputeId = this.deserializeDisputeId(_input);
      const disputes_value: Dispute = this.deserializeDispute(_input);
      disputes.set(disputes_key, disputes_value);
    }
    const pendingUpdateRequests_vecLength = _input.readI32();
    const pendingUpdateRequests: OracleUpdateRequest[] = [];
    for (let pendingUpdateRequests_i = 0; pendingUpdateRequests_i < pendingUpdateRequests_vecLength; pendingUpdateRequests_i++) {
      const pendingUpdateRequests_elem: OracleUpdateRequest = this.deserializeOracleUpdateRequest(_input);
      pendingUpdateRequests.push(pendingUpdateRequests_elem);
    }
    return { largeOracleContract, byocContract, symbol, oracleMembers, withdrawNonce, withdrawals, withdrawMinimum, withdrawalSum, maximumWithdrawalPerEpoch, oracleNonce, epochs, disputes, pendingUpdateRequests };
  }
  public async getState(): Promise<ByocOutgoingContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeByocOutgoingContractState(input);
  }

  public deserializeAddPendingWithdrawalAction(_input: AbiInput): AddPendingWithdrawalAction {
    const receiver: EthereumAddress = this.deserializeEthereumAddress(_input);
    const amount: Unsigned256 = this.deserializeUnsigned256(_input);
    return { discriminant: "add_pending_withdrawal", receiver, amount };
  }

  public deserializeSignPendingWithdrawalAction(_input: AbiInput): SignPendingWithdrawalAction {
    const withdrawalNonce: BN = _input.readU64();
    const signature: Signature = this.deserializeSignature(_input);
    return { discriminant: "sign_pending_withdrawal", withdrawalNonce, signature };
  }

  public deserializeDisputeCreateAction(_input: AbiInput): DisputeCreateAction {
    const transaction: DisputeTransaction = this.deserializeDisputeTransaction(_input);
    return { discriminant: "dispute_create", transaction };
  }

  public deserializeDisputeCounterClaimAction(_input: AbiInput): DisputeCounterClaimAction {
    const transaction: DisputeTransaction = this.deserializeDisputeTransaction(_input);
    return { discriminant: "dispute_counter_claim", transaction };
  }

  public deserializeDisputeResultAction(_input: AbiInput): DisputeResultAction {
    const withdrawalNonce: BN = _input.readU64();
    const disputeResult: number = _input.readU32();
    return { discriminant: "dispute_result", withdrawalNonce, disputeResult };
  }

  public deserializeUpdateSmallOracleCallback(_input: AbiInput): UpdateSmallOracleCallback {
    const oracleIdentities_vecLength = _input.readI32();
    const oracleIdentities: BlockchainAddress[] = [];
    for (let oracleIdentities_i = 0; oracleIdentities_i < oracleIdentities_vecLength; oracleIdentities_i++) {
      const oracleIdentities_elem: BlockchainAddress = _input.readAddress();
      oracleIdentities.push(oracleIdentities_elem);
    }
    const oraclePubKeys_vecLength = _input.readI32();
    const oraclePubKeys: BlockchainPublicKey[] = [];
    for (let oraclePubKeys_i = 0; oraclePubKeys_i < oraclePubKeys_vecLength; oraclePubKeys_i++) {
      const oraclePubKeys_elem: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
      oraclePubKeys.push(oraclePubKeys_elem);
    }
    return { discriminant: "update_small_oracle", oracleIdentities, oraclePubKeys };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const largeOracleAddress: BlockchainAddress = _input.readAddress();
    const byocContract: EthereumAddress = this.deserializeEthereumAddress(_input);
    const symbol: string = _input.readString();
    const oracleIdentities_vecLength = _input.readI32();
    const oracleIdentities: BlockchainAddress[] = [];
    for (let oracleIdentities_i = 0; oracleIdentities_i < oracleIdentities_vecLength; oracleIdentities_i++) {
      const oracleIdentities_elem: BlockchainAddress = _input.readAddress();
      oracleIdentities.push(oracleIdentities_elem);
    }
    const oracleKeys_vecLength = _input.readI32();
    const oracleKeys: BlockchainPublicKey[] = [];
    for (let oracleKeys_i = 0; oracleKeys_i < oracleKeys_vecLength; oracleKeys_i++) {
      const oracleKeys_elem: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
      oracleKeys.push(oracleKeys_elem);
    }
    const withdrawMinimum: Unsigned256 = this.deserializeUnsigned256(_input);
    const totalWithdrawalMaximum: Unsigned256 = this.deserializeUnsigned256(_input);
    return { discriminant: "init", largeOracleAddress, byocContract, symbol, oracleIdentities, oracleKeys, withdrawMinimum, totalWithdrawalMaximum };
  }

}
export interface Unsigned256 {
  bits: Buffer;
}
function serializeUnsigned256(_out: AbiOutput, _value: Unsigned256): void {
  const { bits } = _value;
  if (bits.length != 32) {
    throw new Error("Length of bits does not match expected 32");
  }
  _out.writeBytes(bits);
}

export interface Dispute {
  challenger: BlockchainAddress;
  claims: DisputeTransaction[];
  votingResult: number;
}

export interface DisputeId {
  withdrawalNonce: BN;
  oracleNonce: BN;
}

export interface DisputeTransaction {
  withdrawalNonce: BN;
  receiver: EthereumAddress;
  amount: Unsigned256;
  oracleNonce: BN;
  bitmask: number;
}
function serializeDisputeTransaction(_out: AbiOutput, _value: DisputeTransaction): void {
  const { withdrawalNonce, receiver, amount, oracleNonce, bitmask } = _value;
  _out.writeU64(withdrawalNonce);
  serializeEthereumAddress(_out, receiver);
  serializeUnsigned256(_out, amount);
  _out.writeU64(oracleNonce);
  _out.writeU32(bitmask);
}

export interface EthereumAddress {
  identifier: Buffer;
}
function serializeEthereumAddress(_out: AbiOutput, _value: EthereumAddress): void {
  const { identifier } = _value;
  if (identifier.length != 20) {
    throw new Error("Length of identifier does not match expected 20");
  }
  _out.writeBytes(identifier);
}

export interface Epoch {
  fromWithdrawNonce: BN;
  toWithdrawNonce: BN;
  oracles: OracleMember[];
  merkleTree: Buffer;
}

export interface OracleMember {
  identity: BlockchainAddress;
  key: BlockchainPublicKey;
}

export interface OracleUpdateRequest {
  oracleNonce: BN;
  withdrawNonce: BN;
}

export interface Withdrawal {
  receiver: EthereumAddress;
  amount: Unsigned256;
  signatures: Signature[];
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}
function serializeSignature(_out: AbiOutput, _value: Signature): void {
  const { recoveryId, valueR, valueS } = _value;
  _out.writeU32(recoveryId);
  if (valueR.length != 32) {
    throw new Error("Length of valueR does not match expected 32");
  }
  _out.writeBytes(valueR);
  if (valueS.length != 32) {
    throw new Error("Length of valueS does not match expected 32");
  }
  _out.writeBytes(valueS);
}

export interface BlockchainPublicKey {
  ecPoint: Buffer;
}
function serializeBlockchainPublicKey(_out: AbiOutput, _value: BlockchainPublicKey): void {
  const { ecPoint } = _value;
  if (ecPoint.length != 33) {
    throw new Error("Length of ecPoint does not match expected 33");
  }
  _out.writeBytes(ecPoint);
}

export interface ByocOutgoingContractState {
  largeOracleContract: BlockchainAddress;
  byocContract: EthereumAddress;
  symbol: string;
  oracleMembers: OracleMember[];
  withdrawNonce: BN;
  withdrawals: Map<BN, Withdrawal>;
  withdrawMinimum: Unsigned256;
  withdrawalSum: Unsigned256;
  maximumWithdrawalPerEpoch: Unsigned256;
  oracleNonce: BN;
  epochs: Map<BN, Epoch>;
  disputes: Map<DisputeId, Dispute>;
  pendingUpdateRequests: OracleUpdateRequest[];
}

export function init(largeOracleAddress: BlockchainAddress, byocContract: EthereumAddress, symbol: string, oracleIdentities: BlockchainAddress[], oracleKeys: BlockchainPublicKey[], withdrawMinimum: Unsigned256, totalWithdrawalMaximum: Unsigned256): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(largeOracleAddress);
    serializeEthereumAddress(_out, byocContract);
    _out.writeString(symbol);
    _out.writeI32(oracleIdentities.length);
    for (const oracleIdentities_vec of oracleIdentities) {
      _out.writeAddress(oracleIdentities_vec);
    }
    _out.writeI32(oracleKeys.length);
    for (const oracleKeys_vec of oracleKeys) {
      serializeBlockchainPublicKey(_out, oracleKeys_vec);
    }
    serializeUnsigned256(_out, withdrawMinimum);
    serializeUnsigned256(_out, totalWithdrawalMaximum);
  });
}

export function addPendingWithdrawal(receiver: EthereumAddress, amount: Unsigned256): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    serializeEthereumAddress(_out, receiver);
    serializeUnsigned256(_out, amount);
  });
}

export function signPendingWithdrawal(withdrawalNonce: BN, signature: Signature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU64(withdrawalNonce);
    serializeSignature(_out, signature);
  });
}

export function disputeCreate(transaction: DisputeTransaction): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    serializeDisputeTransaction(_out, transaction);
  });
}

export function disputeCounterClaim(transaction: DisputeTransaction): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    serializeDisputeTransaction(_out, transaction);
  });
}

export function disputeResult(withdrawalNonce: BN, disputeResult: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeU64(withdrawalNonce);
    _out.writeU32(disputeResult);
  });
}

export function updateSmallOracle(oracleIdentities: BlockchainAddress[], oraclePubKeys: BlockchainPublicKey[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeI32(oracleIdentities.length);
    for (const oracleIdentities_vec of oracleIdentities) {
      _out.writeAddress(oracleIdentities_vec);
    }
    _out.writeI32(oraclePubKeys.length);
    for (const oraclePubKeys_vec of oraclePubKeys) {
      serializeBlockchainPublicKey(_out, oraclePubKeys_vec);
    }
  });
}

export function deserializeState(state: StateWithClient): ByocOutgoingContractState;
export function deserializeState(bytes: Buffer): ByocOutgoingContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ByocOutgoingContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ByocOutgoingContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ByocOutgoing_SDK_9_1_2(client, address).deserializeByocOutgoingContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ByocOutgoing_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeByocOutgoingContractState(input);
  }
}

export type Action =
  | AddPendingWithdrawalAction
  | SignPendingWithdrawalAction
  | DisputeCreateAction
  | DisputeCounterClaimAction
  | DisputeResultAction;

export interface AddPendingWithdrawalAction {
  discriminant: "add_pending_withdrawal";
  receiver: EthereumAddress;
  amount: Unsigned256;
}
export interface SignPendingWithdrawalAction {
  discriminant: "sign_pending_withdrawal";
  withdrawalNonce: BN;
  signature: Signature;
}
export interface DisputeCreateAction {
  discriminant: "dispute_create";
  transaction: DisputeTransaction;
}
export interface DisputeCounterClaimAction {
  discriminant: "dispute_counter_claim";
  transaction: DisputeTransaction;
}
export interface DisputeResultAction {
  discriminant: "dispute_result";
  withdrawalNonce: BN;
  disputeResult: number;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocOutgoing_SDK_9_1_2(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeAddPendingWithdrawalAction(input);
  } else if (shortname === "01") {
    return contract.deserializeSignPendingWithdrawalAction(input);
  } else if (shortname === "02") {
    return contract.deserializeDisputeCreateAction(input);
  } else if (shortname === "03") {
    return contract.deserializeDisputeCounterClaimAction(input);
  } else if (shortname === "04") {
    return contract.deserializeDisputeResultAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | UpdateSmallOracleCallback;

export interface UpdateSmallOracleCallback {
  discriminant: "update_small_oracle";
  oracleIdentities: BlockchainAddress[];
  oraclePubKeys: BlockchainPublicKey[];
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocOutgoing_SDK_9_1_2(undefined, undefined);
  if (shortname === "05") {
    return contract.deserializeUpdateSmallOracleCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  largeOracleAddress: BlockchainAddress;
  byocContract: EthereumAddress;
  symbol: string;
  oracleIdentities: BlockchainAddress[];
  oracleKeys: BlockchainPublicKey[];
  withdrawMinimum: Unsigned256;
  totalWithdrawalMaximum: Unsigned256;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocOutgoing_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

