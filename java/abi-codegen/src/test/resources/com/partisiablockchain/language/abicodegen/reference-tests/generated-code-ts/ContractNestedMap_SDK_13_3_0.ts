// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractNestedMap_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myComplexMap_mapLength = _input.readI32();
    const myComplexMap: Map<number, Map<BN, BN>> = new Map();
    for (let myComplexMap_i = 0; myComplexMap_i < myComplexMap_mapLength; myComplexMap_i++) {
      const myComplexMap_key: number = _input.readU32();
      const myComplexMap_value_mapLength = _input.readI32();
      const myComplexMap_value: Map<BN, BN> = new Map();
      for (let myComplexMap_value_i = 0; myComplexMap_value_i < myComplexMap_value_mapLength; myComplexMap_value_i++) {
        const myComplexMap_value_key: BN = _input.readU64();
        const myComplexMap_value_value: BN = _input.readU64();
        myComplexMap_value.set(myComplexMap_value_key, myComplexMap_value_value);
      }
      myComplexMap.set(myComplexMap_key, myComplexMap_value);
    }
    return { myComplexMap };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeInsertInMyNestedMapAction(_input: AbiInput): InsertInMyNestedMapAction {
    const key1: number = _input.readU32();
    const key2: BN = _input.readU64();
    const value: BN = _input.readU64();
    return { discriminant: "insert_in_my_nested_map", key1, key2, value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface ExampleContractState {
  myComplexMap: Map<number, Map<BN, BN>>;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function insertInMyNestedMap(key1: number, key2: BN, value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ccfe82ed09", "hex"));
    _out.writeU32(key1);
    _out.writeU64(key2);
    _out.writeU64(value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractNestedMap_SDK_13_3_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractNestedMap_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | InsertInMyNestedMapAction;

export interface InsertInMyNestedMapAction {
  discriminant: "insert_in_my_nested_map";
  key1: number;
  key2: BN;
  value: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNestedMap_SDK_13_3_0(undefined, undefined);
  if (shortname === "ccfe82ed09") {
    return contract.deserializeInsertInMyNestedMapAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractNestedMap_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

