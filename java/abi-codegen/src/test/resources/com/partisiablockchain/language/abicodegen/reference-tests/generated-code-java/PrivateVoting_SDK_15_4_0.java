// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class PrivateVoting_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public PrivateVoting_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VoteResult deserializeVoteResult(AbiInput _input) {
    int voteId = _input.readU32();
    int votesFor = _input.readU32();
    int votesAgainst = _input.readU32();
    String proof = null;
    var proof_isSome = _input.readBoolean();
    if (proof_isSome) {
      String proof_option = _input.readString();
      proof = proof_option;
    }
    return new VoteResult(voteId, votesFor, votesAgainst, proof);
  }
  private SecretVote deserializeSecretVote(AbiInput _input) {
    boolean vote = _input.readBoolean();
    return new SecretVote(vote);
  }
  private ContractState deserializeContractState(AbiInput _input) {
    int currentVoteId = _input.readU32();
    var voteResults_vecLength = _input.readI32();
    List<VoteResult> voteResults = new ArrayList<>();
    for (int voteResults_i = 0; voteResults_i < voteResults_vecLength; voteResults_i++) {
      VoteResult voteResults_elem = deserializeVoteResult(_input);
      voteResults.add(voteResults_elem);
    }
    return new ContractState(currentVoteId, voteResults);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private StartVoteCountingAction deserializeStartVoteCountingAction(AbiInput _input) {
    return new StartVoteCountingAction();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record VoteResult(int voteId, int votesFor, int votesAgainst, String proof) {
  }

  @AbiGenerated
  public record SecretVote(boolean vote) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(vote);
    }
  }

  @AbiGenerated
  public record ContractState(int currentVoteId, List<VoteResult> voteResults) {
    public static ContractState deserialize(byte[] bytes) {
      return PrivateVoting_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] startVoteCounting() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<SecretVote> castVote() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<SecretVote, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new PrivateVoting_SDK_15_4_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record StartVoteCountingAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new PrivateVoting_SDK_15_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeStartVoteCountingAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new PrivateVoting_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
