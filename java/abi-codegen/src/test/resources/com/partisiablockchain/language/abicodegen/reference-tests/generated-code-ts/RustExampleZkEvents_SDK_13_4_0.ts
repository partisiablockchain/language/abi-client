// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class RustExampleZkEvents_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeOpenState(_input: AbiInput): OpenState {
    return {  };
  }
  public async getState(): Promise<OpenState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeOpenState(input);
  }

  public deserializeReturnInteractionAction(_input: AbiInput): ReturnInteractionAction {
    return { discriminant: "return_interaction",  };
  }

  public deserializeReturnCallbackAction(_input: AbiInput): ReturnCallbackAction {
    return { discriminant: "return_callback",  };
  }

  public deserializeReturnValueAction(_input: AbiInput): ReturnValueAction {
    return { discriminant: "return_value",  };
  }

  public deserializeCallbackCallback(_input: AbiInput): CallbackCallback {
    return { discriminant: "callback",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface OpenState {
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function returnInteraction(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function returnCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function returnValue(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("06", "hex"));
  });
}

export function onSecretInput(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function callback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function deserializeState(state: StateWithClient): OpenState;
export function deserializeState(bytes: Buffer): OpenState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): OpenState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): OpenState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new RustExampleZkEvents_SDK_13_4_0(client, address).deserializeOpenState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new RustExampleZkEvents_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeOpenState(input);
  }
}

export type Action =
  | ReturnInteractionAction
  | ReturnCallbackAction
  | ReturnValueAction;

export interface ReturnInteractionAction {
  discriminant: "return_interaction";
}
export interface ReturnCallbackAction {
  discriminant: "return_callback";
}
export interface ReturnValueAction {
  discriminant: "return_value";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new RustExampleZkEvents_SDK_13_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeReturnInteractionAction(input);
  } else if (shortname === "02") {
    return contract.deserializeReturnCallbackAction(input);
  } else if (shortname === "06") {
    return contract.deserializeReturnValueAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | CallbackCallback;

export interface CallbackCallback {
  discriminant: "callback";
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RustExampleZkEvents_SDK_13_4_0(undefined, undefined);
  if (shortname === "05") {
    return contract.deserializeCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RustExampleZkEvents_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

