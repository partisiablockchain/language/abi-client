// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkNodeRegistry_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkNodeRegistry_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Collaborators deserializeCollaborators(AbiInput _input) {
    BlockchainAddress bpOrchestration = _input.readAddress();
    BlockchainAddress zkContractDeploy = _input.readAddress();
    return new Collaborators(bpOrchestration, zkContractDeploy);
  }
  private BlockchainPublicKey deserializeBlockchainPublicKey(AbiInput _input) {
    byte[] ecPoint = _input.readBytes(33);
    return new BlockchainPublicKey(ecPoint);
  }
  private ZkNode deserializeZkNode(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    BlockchainPublicKey publicKey = deserializeBlockchainPublicKey(_input);
    String restEndpoint = _input.readString();
    int serverJurisdiction = _input.readI32();
    return new ZkNode(identity, publicKey, restEndpoint, serverJurisdiction);
  }
  private ZkNodeTokens deserializeZkNodeTokens(AbiInput _input) {
    long total = _input.readI64();
    var allocatedTokens_mapLength = _input.readI32();
    Map<BlockchainAddress, Long> allocatedTokens = new HashMap<>();
    for (int allocatedTokens_i = 0; allocatedTokens_i < allocatedTokens_mapLength; allocatedTokens_i++) {
      BlockchainAddress allocatedTokens_key = _input.readAddress();
      long allocatedTokens_value = _input.readI64();
      allocatedTokens.put(allocatedTokens_key, allocatedTokens_value);
    }
    var pendingUnlock_vecLength = _input.readI32();
    List<Pending> pendingUnlock = new ArrayList<>();
    for (int pendingUnlock_i = 0; pendingUnlock_i < pendingUnlock_vecLength; pendingUnlock_i++) {
      Pending pendingUnlock_elem = deserializePending(_input);
      pendingUnlock.add(pendingUnlock_elem);
    }
    return new ZkNodeTokens(total, allocatedTokens, pendingUnlock);
  }
  private Pending deserializePending(AbiInput _input) {
    long amount = _input.readI64();
    long unlockTime = _input.readI64();
    return new Pending(amount, unlockTime);
  }
  private SemanticVersion deserializeSemanticVersion(AbiInput _input) {
    int major = _input.readI32();
    int minor = _input.readI32();
    int patch = _input.readI32();
    return new SemanticVersion(major, minor, patch);
  }
  private ZkNodeRegistryState deserializeZkNodeRegistryState(AbiInput _input) {
    Collaborators collaborators = deserializeCollaborators(_input);
    var zkNodes_mapLength = _input.readI32();
    Map<BlockchainAddress, ZkNode> zkNodes = new HashMap<>();
    for (int zkNodes_i = 0; zkNodes_i < zkNodes_mapLength; zkNodes_i++) {
      BlockchainAddress zkNodes_key = _input.readAddress();
      ZkNode zkNodes_value = deserializeZkNode(_input);
      zkNodes.put(zkNodes_key, zkNodes_value);
    }
    var stakedTokens_mapLength = _input.readI32();
    Map<BlockchainAddress, ZkNodeTokens> stakedTokens = new HashMap<>();
    for (int stakedTokens_i = 0; stakedTokens_i < stakedTokens_mapLength; stakedTokens_i++) {
      BlockchainAddress stakedTokens_key = _input.readAddress();
      ZkNodeTokens stakedTokens_value = deserializeZkNodeTokens(_input);
      stakedTokens.put(stakedTokens_key, stakedTokens_value);
    }
    return new ZkNodeRegistryState(collaborators, zkNodes, stakedTokens);
  }
  public ZkNodeRegistryState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeZkNodeRegistryState(input);
  }

  private RegisterAsZkNodeAction deserializeRegisterAsZkNodeAction(AbiInput _input) {
    String restEndpoint = _input.readString();
    return new RegisterAsZkNodeAction(restEndpoint);
  }

  private UpdateRestEndpointAction deserializeUpdateRestEndpointAction(AbiInput _input) {
    String restEndpoint = _input.readString();
    return new UpdateRestEndpointAction(restEndpoint);
  }

  private UpdateServerJurisdictionAction deserializeUpdateServerJurisdictionAction(AbiInput _input) {
    int jurisdiction = _input.readI32();
    return new UpdateServerJurisdictionAction(jurisdiction);
  }

  private AssociateTokensAction deserializeAssociateTokensAction(AbiInput _input) {
    long tokenAmount = _input.readI64();
    return new AssociateTokensAction(tokenAmount);
  }

  private DisassociateTokensAction deserializeDisassociateTokensAction(AbiInput _input) {
    long tokenAmount = _input.readI64();
    return new DisassociateTokensAction(tokenAmount);
  }

  private UpdateNodeProtocolVersionAction deserializeUpdateNodeProtocolVersionAction(AbiInput _input) {
    SemanticVersion supportedProtocolVersion = deserializeSemanticVersion(_input);
    return new UpdateNodeProtocolVersionAction(supportedProtocolVersion);
  }

  private RequestZkComputationNodesV1Action deserializeRequestZkComputationNodesV1Action(AbiInput _input) {
    BlockchainAddress zkContractAddress = _input.readAddress();
    int numberOfNodes = _input.readI32();
    long totalStakes = _input.readI64();
    return new RequestZkComputationNodesV1Action(zkContractAddress, numberOfNodes, totalStakes);
  }

  private RequestZkComputationNodesV2Action deserializeRequestZkComputationNodesV2Action(AbiInput _input) {
    BlockchainAddress zkContractAddress = _input.readAddress();
    int numberOfNodes = _input.readI32();
    long totalStakes = _input.readI64();
    var jurisdictions_vecLength = _input.readI32();
    List<List<Integer>> jurisdictions = new ArrayList<>();
    for (int jurisdictions_i = 0; jurisdictions_i < jurisdictions_vecLength; jurisdictions_i++) {
      var jurisdictions_elem_vecLength = _input.readI32();
      List<Integer> jurisdictions_elem = new ArrayList<>();
      for (int jurisdictions_elem_i = 0; jurisdictions_elem_i < jurisdictions_elem_vecLength; jurisdictions_elem_i++) {
        int jurisdictions_elem_elem = _input.readI32();
        jurisdictions_elem.add(jurisdictions_elem_elem);
      }
      jurisdictions.add(jurisdictions_elem);
    }
    SemanticVersion requiredProtocolVersion = deserializeSemanticVersion(_input);
    return new RequestZkComputationNodesV2Action(zkContractAddress, numberOfNodes, totalStakes, jurisdictions, requiredProtocolVersion);
  }

  private NotifyZkComputationDoneAction deserializeNotifyZkComputationDoneAction(AbiInput _input) {
    return new NotifyZkComputationDoneAction();
  }

  private UnlockExpiredStakesAction deserializeUnlockExpiredStakesAction(AbiInput _input) {
    return new UnlockExpiredStakesAction();
  }

  private CheckContractStatusAction deserializeCheckContractStatusAction(AbiInput _input) {
    BlockchainAddress zkContract = _input.readAddress();
    return new CheckContractStatusAction(zkContract);
  }

  private AssociateTokensCallbackCallback deserializeAssociateTokensCallbackCallback(AbiInput _input) {
    BlockchainAddress account = _input.readAddress();
    long amount = _input.readI64();
    return new AssociateTokensCallbackCallback(account, amount);
  }

  private DisassociateTokensCallbackCallback deserializeDisassociateTokensCallbackCallback(AbiInput _input) {
    BlockchainAddress account = _input.readAddress();
    long amount = _input.readI64();
    return new DisassociateTokensCallbackCallback(account, amount);
  }

  private GetProducerInfoCallbackCallback deserializeGetProducerInfoCallbackCallback(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    String restEndpoint = _input.readString();
    return new GetProducerInfoCallbackCallback(identity, restEndpoint);
  }

  private ContractExistsCallbackCallback deserializeContractExistsCallbackCallback(AbiInput _input) {
    BlockchainAddress zkContract = _input.readAddress();
    return new ContractExistsCallbackCallback(zkContract);
  }

  private ContractComputationDeadlineCallbackCallback deserializeContractComputationDeadlineCallbackCallback(AbiInput _input) {
    BlockchainAddress zkContract = _input.readAddress();
    return new ContractComputationDeadlineCallbackCallback(zkContract);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    Collaborators collaborators = deserializeCollaborators(_input);
    return new InitInit(collaborators);
  }


  @AbiGenerated
  public record Collaborators(BlockchainAddress bpOrchestration, BlockchainAddress zkContractDeploy) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(bpOrchestration);
      _out.writeAddress(zkContractDeploy);
    }
  }

  @AbiGenerated
  public record BlockchainPublicKey(byte[] ecPoint) {
  }

  @AbiGenerated
  public record ZkNode(BlockchainAddress identity, BlockchainPublicKey publicKey, String restEndpoint, int serverJurisdiction) {
  }

  @AbiGenerated
  public record ZkNodeTokens(long total, Map<BlockchainAddress, Long> allocatedTokens, List<Pending> pendingUnlock) {
  }

  @AbiGenerated
  public record Pending(long amount, long unlockTime) {
  }

  @AbiGenerated
  public record SemanticVersion(int major, int minor, int patch) {
    public void serialize(AbiOutput _out) {
      _out.writeI32(major);
      _out.writeI32(minor);
      _out.writeI32(patch);
    }
  }

  @AbiGenerated
  public record ZkNodeRegistryState(Collaborators collaborators, Map<BlockchainAddress, ZkNode> zkNodes, Map<BlockchainAddress, ZkNodeTokens> stakedTokens) {
    public static ZkNodeRegistryState deserialize(byte[] bytes) {
      return ZkNodeRegistry_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] init(Collaborators collaborators) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      collaborators.serialize(_out);
    });
  }

  public static byte[] registerAsZkNode(String restEndpoint) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeString(restEndpoint);
    });
  }

  public static byte[] updateRestEndpoint(String restEndpoint) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeString(restEndpoint);
    });
  }

  public static byte[] updateServerJurisdiction(int jurisdiction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0d"));
      _out.writeI32(jurisdiction);
    });
  }

  public static byte[] associateTokens(long tokenAmount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0a"));
      _out.writeI64(tokenAmount);
    });
  }

  public static byte[] disassociateTokens(long tokenAmount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0b"));
      _out.writeI64(tokenAmount);
    });
  }

  public static byte[] updateNodeProtocolVersion(SemanticVersion supportedProtocolVersion) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0c"));
      supportedProtocolVersion.serialize(_out);
    });
  }

  public static byte[] requestZkComputationNodesV1(BlockchainAddress zkContractAddress, int numberOfNodes, long totalStakes) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(zkContractAddress);
      _out.writeI32(numberOfNodes);
      _out.writeI64(totalStakes);
    });
  }

  public static byte[] requestZkComputationNodesV2(BlockchainAddress zkContractAddress, int numberOfNodes, long totalStakes, List<List<Integer>> jurisdictions, SemanticVersion requiredProtocolVersion) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("08"));
      _out.writeAddress(zkContractAddress);
      _out.writeI32(numberOfNodes);
      _out.writeI64(totalStakes);
      _out.writeI32(jurisdictions.size());
      for (List<Integer> jurisdictions_vec : jurisdictions) {
        _out.writeI32(jurisdictions_vec.size());
        for (int jurisdictions_vec_vec : jurisdictions_vec) {
          _out.writeI32(jurisdictions_vec_vec);
        }
      }
      requiredProtocolVersion.serialize(_out);
    });
  }

  public static byte[] notifyZkComputationDone() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] unlockExpiredStakes() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static byte[] checkContractStatus(BlockchainAddress zkContract) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
      _out.writeAddress(zkContract);
    });
  }

  public static byte[] associateTokensCallback(BlockchainAddress account, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeAddress(account);
      _out.writeI64(amount);
    });
  }

  public static byte[] disassociateTokensCallback(BlockchainAddress account, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(account);
      _out.writeI64(amount);
    });
  }

  public static byte[] getProducerInfoCallback(BlockchainAddress identity, String restEndpoint) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(identity);
      _out.writeString(restEndpoint);
    });
  }

  public static byte[] contractExistsCallback(BlockchainAddress zkContract) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(zkContract);
    });
  }

  public static byte[] contractComputationDeadlineCallback(BlockchainAddress zkContract) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(zkContract);
    });
  }

  public static ZkNodeRegistryState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkNodeRegistry_SDK_13_3_0(client, address).deserializeZkNodeRegistryState(input);
  }
  
  public static ZkNodeRegistryState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ZkNodeRegistryState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record RegisterAsZkNodeAction(String restEndpoint) implements Action {
  }
  @AbiGenerated
  public record UpdateRestEndpointAction(String restEndpoint) implements Action {
  }
  @AbiGenerated
  public record UpdateServerJurisdictionAction(int jurisdiction) implements Action {
  }
  @AbiGenerated
  public record AssociateTokensAction(long tokenAmount) implements Action {
  }
  @AbiGenerated
  public record DisassociateTokensAction(long tokenAmount) implements Action {
  }
  @AbiGenerated
  public record UpdateNodeProtocolVersionAction(SemanticVersion supportedProtocolVersion) implements Action {
  }
  @AbiGenerated
  public record RequestZkComputationNodesV1Action(BlockchainAddress zkContractAddress, int numberOfNodes, long totalStakes) implements Action {
  }
  @AbiGenerated
  public record RequestZkComputationNodesV2Action(BlockchainAddress zkContractAddress, int numberOfNodes, long totalStakes, List<List<Integer>> jurisdictions, SemanticVersion requiredProtocolVersion) implements Action {
  }
  @AbiGenerated
  public record NotifyZkComputationDoneAction() implements Action {
  }
  @AbiGenerated
  public record UnlockExpiredStakesAction() implements Action {
  }
  @AbiGenerated
  public record CheckContractStatusAction(BlockchainAddress zkContract) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkNodeRegistry_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeRegisterAsZkNodeAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeUpdateRestEndpointAction(input);
    } else if (shortname.equals("0d")) {
      return contract.deserializeUpdateServerJurisdictionAction(input);
    } else if (shortname.equals("0a")) {
      return contract.deserializeAssociateTokensAction(input);
    } else if (shortname.equals("0b")) {
      return contract.deserializeDisassociateTokensAction(input);
    } else if (shortname.equals("0c")) {
      return contract.deserializeUpdateNodeProtocolVersionAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeRequestZkComputationNodesV1Action(input);
    } else if (shortname.equals("08")) {
      return contract.deserializeRequestZkComputationNodesV2Action(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeNotifyZkComputationDoneAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeUnlockExpiredStakesAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeCheckContractStatusAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record AssociateTokensCallbackCallback(BlockchainAddress account, long amount) implements Callback {
  }
  @AbiGenerated
  public record DisassociateTokensCallbackCallback(BlockchainAddress account, long amount) implements Callback {
  }
  @AbiGenerated
  public record GetProducerInfoCallbackCallback(BlockchainAddress identity, String restEndpoint) implements Callback {
  }
  @AbiGenerated
  public record ContractExistsCallbackCallback(BlockchainAddress zkContract) implements Callback {
  }
  @AbiGenerated
  public record ContractComputationDeadlineCallbackCallback(BlockchainAddress zkContract) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkNodeRegistry_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeAssociateTokensCallbackCallback(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeDisassociateTokensCallbackCallback(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeGetProducerInfoCallbackCallback(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeContractExistsCallbackCallback(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeContractComputationDeadlineCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(Collaborators collaborators) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkNodeRegistry_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
