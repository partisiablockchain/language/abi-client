// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ByocFaucet_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeFaucetContractState(_input: AbiInput): FaucetContractState {
    const faucetAddress: BlockchainAddress = _input.readAddress();
    return { faucetAddress };
  }
  public async getState(): Promise<FaucetContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeFaucetContractState(input);
  }

  public deserializeMintGasAction(_input: AbiInput): MintGasAction {
    const receiver: BlockchainAddress = _input.readAddress();
    return { discriminant: "mint_gas", receiver };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const faucetAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "initialize", faucetAddress };
  }

}
export interface FaucetContractState {
  faucetAddress: BlockchainAddress;
}

export function initialize(faucetAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(faucetAddress);
  });
}

export function mintGas(receiver: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(receiver);
  });
}

export function deserializeState(state: StateWithClient): FaucetContractState;
export function deserializeState(bytes: Buffer): FaucetContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): FaucetContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): FaucetContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ByocFaucet_SDK_11_0_0(client, address).deserializeFaucetContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ByocFaucet_SDK_11_0_0(
      state.client,
      state.address
    ).deserializeFaucetContractState(input);
  }
}

export type Action =
  | MintGasAction;

export interface MintGasAction {
  discriminant: "mint_gas";
  receiver: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocFaucet_SDK_11_0_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeMintGasAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  faucetAddress: BlockchainAddress;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocFaucet_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

