// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class AverageSalary {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeZkStateImmutable(_input: AbiInput): ZkStateImmutable {
    let attestations: Option<Map<Option<number>, Option<AttestationImpl>>> = undefined;
    const attestations_isSome = _input.readBoolean();
    if (attestations_isSome) {
      const attestations_option_mapLength = _input.readI32();
      const attestations_option: Map<Option<number>, Option<AttestationImpl>> = new Map();
      for (let attestations_option_i = 0; attestations_option_i < attestations_option_mapLength; attestations_option_i++) {
        let attestations_option_key: Option<number> = undefined;
        const attestations_option_key_isSome = _input.readBoolean();
        if (attestations_option_key_isSome) {
          const attestations_option_key_option: number = _input.readI32();
          attestations_option_key = attestations_option_key_option;
        }
        let attestations_option_value: Option<AttestationImpl> = undefined;
        const attestations_option_value_isSome = _input.readBoolean();
        if (attestations_option_value_isSome) {
          const attestations_option_value_option: AttestationImpl = this.deserializeAttestationImpl(_input);
          attestations_option_value = attestations_option_value_option;
        }
        attestations_option.set(attestations_option_key, attestations_option_value);
      }
      attestations = attestations_option;
    }
    let calculationStatus: Option<CalculationStatus> = undefined;
    const calculationStatus_isSome = _input.readBoolean();
    if (calculationStatus_isSome) {
      const calculationStatus_option: CalculationStatus = this.deserializeCalculationStatus(_input);
      calculationStatus = calculationStatus_option;
    }
    let computationState: Option<ComputationStateImpl> = undefined;
    const computationState_isSome = _input.readBoolean();
    if (computationState_isSome) {
      const computationState_option: ComputationStateImpl = this.deserializeComputationStateImpl(_input);
      computationState = computationState_option;
    }
    const defaultOptimisticMode: boolean = _input.readBoolean();
    let engines: Option<EngineState> = undefined;
    const engines_isSome = _input.readBoolean();
    if (engines_isSome) {
      const engines_option: EngineState = this.deserializeEngineState(_input);
      engines = engines_option;
    }
    let externalEvents: Option<ExternalEventState> = undefined;
    const externalEvents_isSome = _input.readBoolean();
    if (externalEvents_isSome) {
      const externalEvents_option: ExternalEventState = this.deserializeExternalEventState(_input);
      externalEvents = externalEvents_option;
    }
    let finishedComputations: Option<Array<Option<FinishedComputationImpl>>> = undefined;
    const finishedComputations_isSome = _input.readBoolean();
    if (finishedComputations_isSome) {
      const finishedComputations_option_vecLength = _input.readI32();
      const finishedComputations_option: Array<Option<FinishedComputationImpl>> = [];
      for (let finishedComputations_option_i = 0; finishedComputations_option_i < finishedComputations_option_vecLength; finishedComputations_option_i++) {
        let finishedComputations_option_elem: Option<FinishedComputationImpl> = undefined;
        const finishedComputations_option_elem_isSome = _input.readBoolean();
        if (finishedComputations_option_elem_isSome) {
          const finishedComputations_option_elem_option: FinishedComputationImpl = this.deserializeFinishedComputationImpl(_input);
          finishedComputations_option_elem = finishedComputations_option_elem_option;
        }
        finishedComputations_option.push(finishedComputations_option_elem);
      }
      finishedComputations = finishedComputations_option;
    }
    const nextAttestationId: number = _input.readI32();
    const nextVariableId: number = _input.readI32();
    let nodeRegistryContract: Option<BlockchainAddress> = undefined;
    const nodeRegistryContract_isSome = _input.readBoolean();
    if (nodeRegistryContract_isSome) {
      const nodeRegistryContract_option: BlockchainAddress = _input.readAddress();
      nodeRegistryContract = nodeRegistryContract_option;
    }
    const openState: ContractState = this.deserializeContractState(_input);
    let pendingInput: Option<Map<Option<number>, Option<PendingInputImpl>>> = undefined;
    const pendingInput_isSome = _input.readBoolean();
    if (pendingInput_isSome) {
      const pendingInput_option_mapLength = _input.readI32();
      const pendingInput_option: Map<Option<number>, Option<PendingInputImpl>> = new Map();
      for (let pendingInput_option_i = 0; pendingInput_option_i < pendingInput_option_mapLength; pendingInput_option_i++) {
        let pendingInput_option_key: Option<number> = undefined;
        const pendingInput_option_key_isSome = _input.readBoolean();
        if (pendingInput_option_key_isSome) {
          const pendingInput_option_key_option: number = _input.readI32();
          pendingInput_option_key = pendingInput_option_key_option;
        }
        let pendingInput_option_value: Option<PendingInputImpl> = undefined;
        const pendingInput_option_value_isSome = _input.readBoolean();
        if (pendingInput_option_value_isSome) {
          const pendingInput_option_value_option: PendingInputImpl = this.deserializePendingInputImpl(_input);
          pendingInput_option_value = pendingInput_option_value_option;
        }
        pendingInput_option.set(pendingInput_option_key, pendingInput_option_value);
      }
      pendingInput = pendingInput_option;
    }
    let pendingOnChainOpen: Option<Map<Option<number>, Option<PendingOnChainOpenImpl>>> = undefined;
    const pendingOnChainOpen_isSome = _input.readBoolean();
    if (pendingOnChainOpen_isSome) {
      const pendingOnChainOpen_option_mapLength = _input.readI32();
      const pendingOnChainOpen_option: Map<Option<number>, Option<PendingOnChainOpenImpl>> = new Map();
      for (let pendingOnChainOpen_option_i = 0; pendingOnChainOpen_option_i < pendingOnChainOpen_option_mapLength; pendingOnChainOpen_option_i++) {
        let pendingOnChainOpen_option_key: Option<number> = undefined;
        const pendingOnChainOpen_option_key_isSome = _input.readBoolean();
        if (pendingOnChainOpen_option_key_isSome) {
          const pendingOnChainOpen_option_key_option: number = _input.readI32();
          pendingOnChainOpen_option_key = pendingOnChainOpen_option_key_option;
        }
        let pendingOnChainOpen_option_value: Option<PendingOnChainOpenImpl> = undefined;
        const pendingOnChainOpen_option_value_isSome = _input.readBoolean();
        if (pendingOnChainOpen_option_value_isSome) {
          const pendingOnChainOpen_option_value_option: PendingOnChainOpenImpl = this.deserializePendingOnChainOpenImpl(_input);
          pendingOnChainOpen_option_value = pendingOnChainOpen_option_value_option;
        }
        pendingOnChainOpen_option.set(pendingOnChainOpen_option_key, pendingOnChainOpen_option_value);
      }
      pendingOnChainOpen = pendingOnChainOpen_option;
    }
    let preProcessMaterials: Option<Map<Option<PreProcessMaterialType>, Option<PreProcessMaterialState>>> = undefined;
    const preProcessMaterials_isSome = _input.readBoolean();
    if (preProcessMaterials_isSome) {
      const preProcessMaterials_option_mapLength = _input.readI32();
      const preProcessMaterials_option: Map<Option<PreProcessMaterialType>, Option<PreProcessMaterialState>> = new Map();
      for (let preProcessMaterials_option_i = 0; preProcessMaterials_option_i < preProcessMaterials_option_mapLength; preProcessMaterials_option_i++) {
        let preProcessMaterials_option_key: Option<PreProcessMaterialType> = undefined;
        const preProcessMaterials_option_key_isSome = _input.readBoolean();
        if (preProcessMaterials_option_key_isSome) {
          const preProcessMaterials_option_key_option: PreProcessMaterialType = this.deserializePreProcessMaterialType(_input);
          preProcessMaterials_option_key = preProcessMaterials_option_key_option;
        }
        let preProcessMaterials_option_value: Option<PreProcessMaterialState> = undefined;
        const preProcessMaterials_option_value_isSome = _input.readBoolean();
        if (preProcessMaterials_option_value_isSome) {
          const preProcessMaterials_option_value_option: PreProcessMaterialState = this.deserializePreProcessMaterialState(_input);
          preProcessMaterials_option_value = preProcessMaterials_option_value_option;
        }
        preProcessMaterials_option.set(preProcessMaterials_option_key, preProcessMaterials_option_value);
      }
      preProcessMaterials = preProcessMaterials_option;
    }
    let preprocessContract: Option<BlockchainAddress> = undefined;
    const preprocessContract_isSome = _input.readBoolean();
    if (preprocessContract_isSome) {
      const preprocessContract_option: BlockchainAddress = _input.readAddress();
      preprocessContract = preprocessContract_option;
    }
    let variables: Option<Map<Option<number>, Option<ZkClosedImpl>>> = undefined;
    const variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      const variables_option_mapLength = _input.readI32();
      const variables_option: Map<Option<number>, Option<ZkClosedImpl>> = new Map();
      for (let variables_option_i = 0; variables_option_i < variables_option_mapLength; variables_option_i++) {
        let variables_option_key: Option<number> = undefined;
        const variables_option_key_isSome = _input.readBoolean();
        if (variables_option_key_isSome) {
          const variables_option_key_option: number = _input.readI32();
          variables_option_key = variables_option_key_option;
        }
        let variables_option_value: Option<ZkClosedImpl> = undefined;
        const variables_option_value_isSome = _input.readBoolean();
        if (variables_option_value_isSome) {
          const variables_option_value_option: ZkClosedImpl = this.deserializeZkClosedImpl(_input);
          variables_option_value = variables_option_value_option;
        }
        variables_option.set(variables_option_key, variables_option_value);
      }
      variables = variables_option;
    }
    const zkComputationDeadline: BN = _input.readI64();
    return { attestations, calculationStatus, computationState, defaultOptimisticMode, engines, externalEvents, finishedComputations, nextAttestationId, nextVariableId, nodeRegistryContract, openState, pendingInput, pendingOnChainOpen, preProcessMaterials, preprocessContract, variables, zkComputationDeadline };
  }
  public deserializeAttestationImpl(_input: AbiInput): AttestationImpl {
    let data: Option<Buffer> = undefined;
    const data_isSome = _input.readBoolean();
    if (data_isSome) {
      const data_option_vecLength = _input.readI32();
      const data_option: Buffer = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    const id: number = _input.readI32();
    let signatures: Option<Array<Option<Signature>>> = undefined;
    const signatures_isSome = _input.readBoolean();
    if (signatures_isSome) {
      const signatures_option_vecLength = _input.readI32();
      const signatures_option: Array<Option<Signature>> = [];
      for (let signatures_option_i = 0; signatures_option_i < signatures_option_vecLength; signatures_option_i++) {
        let signatures_option_elem: Option<Signature> = undefined;
        const signatures_option_elem_isSome = _input.readBoolean();
        if (signatures_option_elem_isSome) {
          const signatures_option_elem_option: Signature = _input.readSignature();
          signatures_option_elem = signatures_option_elem_option;
        }
        signatures_option.push(signatures_option_elem);
      }
      signatures = signatures_option;
    }
    return { data, id, signatures };
  }
  public deserializeCalculationStatus(_input: AbiInput): CalculationStatus {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeCalculationStatusCalculationStatus$WAITING(_input);
    } else if (discriminant === 1) {
      return this.deserializeCalculationStatusCalculationStatus$CALCULATING(_input);
    } else if (discriminant === 2) {
      return this.deserializeCalculationStatusCalculationStatus$OUTPUT(_input);
    } else if (discriminant === 3) {
      return this.deserializeCalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR(_input);
    } else if (discriminant === 4) {
      return this.deserializeCalculationStatusCalculationStatus$DONE(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeCalculationStatusCalculationStatus$WAITING(_input: AbiInput): CalculationStatusCalculationStatus$WAITING {
    return { discriminant: CalculationStatusD.CalculationStatus$WAITING,  };
  }
  public deserializeCalculationStatusCalculationStatus$CALCULATING(_input: AbiInput): CalculationStatusCalculationStatus$CALCULATING {
    return { discriminant: CalculationStatusD.CalculationStatus$CALCULATING,  };
  }
  public deserializeCalculationStatusCalculationStatus$OUTPUT(_input: AbiInput): CalculationStatusCalculationStatus$OUTPUT {
    return { discriminant: CalculationStatusD.CalculationStatus$OUTPUT,  };
  }
  public deserializeCalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR(_input: AbiInput): CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR {
    return { discriminant: CalculationStatusD.CalculationStatus$MALICIOUS_BEHAVIOUR,  };
  }
  public deserializeCalculationStatusCalculationStatus$DONE(_input: AbiInput): CalculationStatusCalculationStatus$DONE {
    return { discriminant: CalculationStatusD.CalculationStatus$DONE,  };
  }
  public deserializeComputationStateImpl(_input: AbiInput): ComputationStateImpl {
    const calculateFor: BN = _input.readI64();
    let computationOutput: Option<ComputationOutput> = undefined;
    const computationOutput_isSome = _input.readBoolean();
    if (computationOutput_isSome) {
      const computationOutput_option: ComputationOutput = this.deserializeComputationOutput(_input);
      computationOutput = computationOutput_option;
    }
    const confirmedInputs: number = _input.readI32();
    const multiplicationCount: number = _input.readI32();
    const offsetIntoFirstTripleBatch: number = _input.readI32();
    let onComputeCompleteShortname: Option<Buffer> = undefined;
    const onComputeCompleteShortname_isSome = _input.readBoolean();
    if (onComputeCompleteShortname_isSome) {
      const onComputeCompleteShortname_option_vecLength = _input.readI32();
      const onComputeCompleteShortname_option: Buffer = _input.readBytes(onComputeCompleteShortname_option_vecLength);
      onComputeCompleteShortname = onComputeCompleteShortname_option;
    }
    let openState: Option<WasmRealContractState> = undefined;
    const openState_isSome = _input.readBoolean();
    if (openState_isSome) {
      const openState_option: WasmRealContractState = this.deserializeWasmRealContractState(_input);
      openState = openState_option;
    }
    const optimisticMode: boolean = _input.readBoolean();
    let tripleBatches: Option<Array<Option<number>>> = undefined;
    const tripleBatches_isSome = _input.readBoolean();
    if (tripleBatches_isSome) {
      const tripleBatches_option_vecLength = _input.readI32();
      const tripleBatches_option: Array<Option<number>> = [];
      for (let tripleBatches_option_i = 0; tripleBatches_option_i < tripleBatches_option_vecLength; tripleBatches_option_i++) {
        let tripleBatches_option_elem: Option<number> = undefined;
        const tripleBatches_option_elem_isSome = _input.readBoolean();
        if (tripleBatches_option_elem_isSome) {
          const tripleBatches_option_elem_option: number = _input.readI32();
          tripleBatches_option_elem = tripleBatches_option_elem_option;
        }
        tripleBatches_option.push(tripleBatches_option_elem);
      }
      tripleBatches = tripleBatches_option;
    }
    let variables: Option<Map<Option<number>, Option<ZkClosedImpl>>> = undefined;
    const variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      const variables_option_mapLength = _input.readI32();
      const variables_option: Map<Option<number>, Option<ZkClosedImpl>> = new Map();
      for (let variables_option_i = 0; variables_option_i < variables_option_mapLength; variables_option_i++) {
        let variables_option_key: Option<number> = undefined;
        const variables_option_key_isSome = _input.readBoolean();
        if (variables_option_key_isSome) {
          const variables_option_key_option: number = _input.readI32();
          variables_option_key = variables_option_key_option;
        }
        let variables_option_value: Option<ZkClosedImpl> = undefined;
        const variables_option_value_isSome = _input.readBoolean();
        if (variables_option_value_isSome) {
          const variables_option_value_option: ZkClosedImpl = this.deserializeZkClosedImpl(_input);
          variables_option_value = variables_option_value_option;
        }
        variables_option.set(variables_option_key, variables_option_value);
      }
      variables = variables_option;
    }
    return { calculateFor, computationOutput, confirmedInputs, multiplicationCount, offsetIntoFirstTripleBatch, onComputeCompleteShortname, openState, optimisticMode, tripleBatches, variables };
  }
  public deserializeComputationOutput(_input: AbiInput): ComputationOutput {
    const engines: number = _input.readI8();
    let outputInformation: Option<Array<Option<EngineOutputInformation>>> = undefined;
    const outputInformation_isSome = _input.readBoolean();
    if (outputInformation_isSome) {
      const outputInformation_option_vecLength = _input.readI32();
      const outputInformation_option: Array<Option<EngineOutputInformation>> = [];
      for (let outputInformation_option_i = 0; outputInformation_option_i < outputInformation_option_vecLength; outputInformation_option_i++) {
        let outputInformation_option_elem: Option<EngineOutputInformation> = undefined;
        const outputInformation_option_elem_isSome = _input.readBoolean();
        if (outputInformation_option_elem_isSome) {
          const outputInformation_option_elem_option: EngineOutputInformation = this.deserializeEngineOutputInformation(_input);
          outputInformation_option_elem = outputInformation_option_elem_option;
        }
        outputInformation_option.push(outputInformation_option_elem);
      }
      outputInformation = outputInformation_option;
    }
    let variables: Option<Array<Option<PendingOutputVariable>>> = undefined;
    const variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      const variables_option_vecLength = _input.readI32();
      const variables_option: Array<Option<PendingOutputVariable>> = [];
      for (let variables_option_i = 0; variables_option_i < variables_option_vecLength; variables_option_i++) {
        let variables_option_elem: Option<PendingOutputVariable> = undefined;
        const variables_option_elem_isSome = _input.readBoolean();
        if (variables_option_elem_isSome) {
          const variables_option_elem_option: PendingOutputVariable = this.deserializePendingOutputVariable(_input);
          variables_option_elem = variables_option_elem_option;
        }
        variables_option.push(variables_option_elem);
      }
      variables = variables_option;
    }
    return { engines, outputInformation, variables };
  }
  public deserializeEngineOutputInformation(_input: AbiInput): EngineOutputInformation {
    let linearCombinationElement: Option<Buffer> = undefined;
    const linearCombinationElement_isSome = _input.readBoolean();
    if (linearCombinationElement_isSome) {
      const linearCombinationElement_option_vecLength = _input.readI32();
      const linearCombinationElement_option: Buffer = _input.readBytes(linearCombinationElement_option_vecLength);
      linearCombinationElement = linearCombinationElement_option;
    }
    let partialOpenings: Option<Hash> = undefined;
    const partialOpenings_isSome = _input.readBoolean();
    if (partialOpenings_isSome) {
      const partialOpenings_option: Hash = _input.readHash();
      partialOpenings = partialOpenings_option;
    }
    let verificationSeed: Option<Buffer> = undefined;
    const verificationSeed_isSome = _input.readBoolean();
    if (verificationSeed_isSome) {
      const verificationSeed_option_vecLength = _input.readI32();
      const verificationSeed_option: Buffer = _input.readBytes(verificationSeed_option_vecLength);
      verificationSeed = verificationSeed_option;
    }
    return { linearCombinationElement, partialOpenings, verificationSeed };
  }
  public deserializePendingOutputVariable(_input: AbiInput): PendingOutputVariable {
    const bitLength: number = _input.readI32();
    let information: Option<Buffer> = undefined;
    const information_isSome = _input.readBoolean();
    if (information_isSome) {
      const information_option_vecLength = _input.readI32();
      const information_option: Buffer = _input.readBytes(information_option_vecLength);
      information = information_option;
    }
    const variableId: number = _input.readI32();
    return { bitLength, information, variableId };
  }
  public deserializeWasmRealContractState(_input: AbiInput): WasmRealContractState {
    let avlTrees: Option<Map<Option<number>, Option<AvlTreeWrapper>>> = undefined;
    const avlTrees_isSome = _input.readBoolean();
    if (avlTrees_isSome) {
      const avlTrees_option_mapLength = _input.readI32();
      const avlTrees_option: Map<Option<number>, Option<AvlTreeWrapper>> = new Map();
      for (let avlTrees_option_i = 0; avlTrees_option_i < avlTrees_option_mapLength; avlTrees_option_i++) {
        let avlTrees_option_key: Option<number> = undefined;
        const avlTrees_option_key_isSome = _input.readBoolean();
        if (avlTrees_option_key_isSome) {
          const avlTrees_option_key_option: number = _input.readI32();
          avlTrees_option_key = avlTrees_option_key_option;
        }
        let avlTrees_option_value: Option<AvlTreeWrapper> = undefined;
        const avlTrees_option_value_isSome = _input.readBoolean();
        if (avlTrees_option_value_isSome) {
          const avlTrees_option_value_option: AvlTreeWrapper = this.deserializeAvlTreeWrapper(_input);
          avlTrees_option_value = avlTrees_option_value_option;
        }
        avlTrees_option.set(avlTrees_option_key, avlTrees_option_value);
      }
      avlTrees = avlTrees_option;
    }
    let computationInput: Option<ComputationInput> = undefined;
    const computationInput_isSome = _input.readBoolean();
    if (computationInput_isSome) {
      const computationInput_option: ComputationInput = this.deserializeComputationInput(_input);
      computationInput = computationInput_option;
    }
    let openState: Option<Buffer> = undefined;
    const openState_isSome = _input.readBoolean();
    if (openState_isSome) {
      const openState_option_vecLength = _input.readI32();
      const openState_option: Buffer = _input.readBytes(openState_option_vecLength);
      openState = openState_option;
    }
    return { avlTrees, computationInput, openState };
  }
  public deserializeAvlTreeWrapper(_input: AbiInput): AvlTreeWrapper {
    let avlTree: Option<Map<Option<ComparableByteArray>, Option<Buffer>>> = undefined;
    const avlTree_isSome = _input.readBoolean();
    if (avlTree_isSome) {
      const avlTree_option_mapLength = _input.readI32();
      const avlTree_option: Map<Option<ComparableByteArray>, Option<Buffer>> = new Map();
      for (let avlTree_option_i = 0; avlTree_option_i < avlTree_option_mapLength; avlTree_option_i++) {
        let avlTree_option_key: Option<ComparableByteArray> = undefined;
        const avlTree_option_key_isSome = _input.readBoolean();
        if (avlTree_option_key_isSome) {
          const avlTree_option_key_option: ComparableByteArray = this.deserializeComparableByteArray(_input);
          avlTree_option_key = avlTree_option_key_option;
        }
        let avlTree_option_value: Option<Buffer> = undefined;
        const avlTree_option_value_isSome = _input.readBoolean();
        if (avlTree_option_value_isSome) {
          const avlTree_option_value_option_vecLength = _input.readI32();
          const avlTree_option_value_option: Buffer = _input.readBytes(avlTree_option_value_option_vecLength);
          avlTree_option_value = avlTree_option_value_option;
        }
        avlTree_option.set(avlTree_option_key, avlTree_option_value);
      }
      avlTree = avlTree_option;
    }
    return { avlTree };
  }
  public deserializeComparableByteArray(_input: AbiInput): ComparableByteArray {
    let data: Option<Buffer> = undefined;
    const data_isSome = _input.readBoolean();
    if (data_isSome) {
      const data_option_vecLength = _input.readI32();
      const data_option: Buffer = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    return { data };
  }
  public deserializeComputationInput(_input: AbiInput): ComputationInput {
    let calledFunctionArguments: Option<Array<Option<Buffer>>> = undefined;
    const calledFunctionArguments_isSome = _input.readBoolean();
    if (calledFunctionArguments_isSome) {
      const calledFunctionArguments_option_vecLength = _input.readI32();
      const calledFunctionArguments_option: Array<Option<Buffer>> = [];
      for (let calledFunctionArguments_option_i = 0; calledFunctionArguments_option_i < calledFunctionArguments_option_vecLength; calledFunctionArguments_option_i++) {
        let calledFunctionArguments_option_elem: Option<Buffer> = undefined;
        const calledFunctionArguments_option_elem_isSome = _input.readBoolean();
        if (calledFunctionArguments_option_elem_isSome) {
          const calledFunctionArguments_option_elem_option_vecLength = _input.readI32();
          const calledFunctionArguments_option_elem_option: Buffer = _input.readBytes(calledFunctionArguments_option_elem_option_vecLength);
          calledFunctionArguments_option_elem = calledFunctionArguments_option_elem_option;
        }
        calledFunctionArguments_option.push(calledFunctionArguments_option_elem);
      }
      calledFunctionArguments = calledFunctionArguments_option;
    }
    const calledFunctionShortname: number = _input.readI32();
    return { calledFunctionArguments, calledFunctionShortname };
  }
  public deserializeZkClosedImpl(_input: AbiInput): ZkClosedImpl {
    const id: number = _input.readI32();
    let information: Option<Buffer> = undefined;
    const information_isSome = _input.readBoolean();
    if (information_isSome) {
      const information_option_vecLength = _input.readI32();
      const information_option: Buffer = _input.readBytes(information_option_vecLength);
      information = information_option;
    }
    const inputMaskOffset: number = _input.readI32();
    let maskedInputShare: Option<Buffer> = undefined;
    const maskedInputShare_isSome = _input.readBoolean();
    if (maskedInputShare_isSome) {
      const maskedInputShare_option_vecLength = _input.readI32();
      const maskedInputShare_option: Buffer = _input.readBytes(maskedInputShare_option_vecLength);
      maskedInputShare = maskedInputShare_option;
    }
    let openValue: Option<Buffer> = undefined;
    const openValue_isSome = _input.readBoolean();
    if (openValue_isSome) {
      const openValue_option_vecLength = _input.readI32();
      const openValue_option: Buffer = _input.readBytes(openValue_option_vecLength);
      openValue = openValue_option;
    }
    let owner: Option<BlockchainAddress> = undefined;
    const owner_isSome = _input.readBoolean();
    if (owner_isSome) {
      const owner_option: BlockchainAddress = _input.readAddress();
      owner = owner_option;
    }
    const sealed: boolean = _input.readBoolean();
    let shareBitLengths: Option<Array<Option<number>>> = undefined;
    const shareBitLengths_isSome = _input.readBoolean();
    if (shareBitLengths_isSome) {
      const shareBitLengths_option_vecLength = _input.readI32();
      const shareBitLengths_option: Array<Option<number>> = [];
      for (let shareBitLengths_option_i = 0; shareBitLengths_option_i < shareBitLengths_option_vecLength; shareBitLengths_option_i++) {
        let shareBitLengths_option_elem: Option<number> = undefined;
        const shareBitLengths_option_elem_isSome = _input.readBoolean();
        if (shareBitLengths_option_elem_isSome) {
          const shareBitLengths_option_elem_option: number = _input.readI32();
          shareBitLengths_option_elem = shareBitLengths_option_elem_option;
        }
        shareBitLengths_option.push(shareBitLengths_option_elem);
      }
      shareBitLengths = shareBitLengths_option;
    }
    let transaction: Option<Hash> = undefined;
    const transaction_isSome = _input.readBoolean();
    if (transaction_isSome) {
      const transaction_option: Hash = _input.readHash();
      transaction = transaction_option;
    }
    return { id, information, inputMaskOffset, maskedInputShare, openValue, owner, sealed, shareBitLengths, transaction };
  }
  public deserializeEngineState(_input: AbiInput): EngineState {
    let engines: Option<Array<Option<EngineInformation>>> = undefined;
    const engines_isSome = _input.readBoolean();
    if (engines_isSome) {
      const engines_option_vecLength = _input.readI32();
      const engines_option: Array<Option<EngineInformation>> = [];
      for (let engines_option_i = 0; engines_option_i < engines_option_vecLength; engines_option_i++) {
        let engines_option_elem: Option<EngineInformation> = undefined;
        const engines_option_elem_isSome = _input.readBoolean();
        if (engines_option_elem_isSome) {
          const engines_option_elem_option: EngineInformation = this.deserializeEngineInformation(_input);
          engines_option_elem = engines_option_elem_option;
        }
        engines_option.push(engines_option_elem);
      }
      engines = engines_option;
    }
    return { engines };
  }
  public deserializeEngineInformation(_input: AbiInput): EngineInformation {
    let identity: Option<BlockchainAddress> = undefined;
    const identity_isSome = _input.readBoolean();
    if (identity_isSome) {
      const identity_option: BlockchainAddress = _input.readAddress();
      identity = identity_option;
    }
    let publicKey: Option<BlockchainPublicKey> = undefined;
    const publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      const publicKey_option: BlockchainPublicKey = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    let restInterface: Option<string> = undefined;
    const restInterface_isSome = _input.readBoolean();
    if (restInterface_isSome) {
      const restInterface_option: string = _input.readString();
      restInterface = restInterface_option;
    }
    return { identity, publicKey, restInterface };
  }
  public deserializeExternalEventState(_input: AbiInput): ExternalEventState {
    let confirmedEvents: Option<Map<Option<number>, Option<ConfirmedEventLog>>> = undefined;
    const confirmedEvents_isSome = _input.readBoolean();
    if (confirmedEvents_isSome) {
      const confirmedEvents_option_mapLength = _input.readI32();
      const confirmedEvents_option: Map<Option<number>, Option<ConfirmedEventLog>> = new Map();
      for (let confirmedEvents_option_i = 0; confirmedEvents_option_i < confirmedEvents_option_mapLength; confirmedEvents_option_i++) {
        let confirmedEvents_option_key: Option<number> = undefined;
        const confirmedEvents_option_key_isSome = _input.readBoolean();
        if (confirmedEvents_option_key_isSome) {
          const confirmedEvents_option_key_option: number = _input.readI32();
          confirmedEvents_option_key = confirmedEvents_option_key_option;
        }
        let confirmedEvents_option_value: Option<ConfirmedEventLog> = undefined;
        const confirmedEvents_option_value_isSome = _input.readBoolean();
        if (confirmedEvents_option_value_isSome) {
          const confirmedEvents_option_value_option: ConfirmedEventLog = this.deserializeConfirmedEventLog(_input);
          confirmedEvents_option_value = confirmedEvents_option_value_option;
        }
        confirmedEvents_option.set(confirmedEvents_option_key, confirmedEvents_option_value);
      }
      confirmedEvents = confirmedEvents_option;
    }
    const nextEventId: number = _input.readI32();
    const nextSubscriptionId: number = _input.readI32();
    let subscriptions: Option<Map<Option<number>, Option<EvmEventSubscription>>> = undefined;
    const subscriptions_isSome = _input.readBoolean();
    if (subscriptions_isSome) {
      const subscriptions_option_mapLength = _input.readI32();
      const subscriptions_option: Map<Option<number>, Option<EvmEventSubscription>> = new Map();
      for (let subscriptions_option_i = 0; subscriptions_option_i < subscriptions_option_mapLength; subscriptions_option_i++) {
        let subscriptions_option_key: Option<number> = undefined;
        const subscriptions_option_key_isSome = _input.readBoolean();
        if (subscriptions_option_key_isSome) {
          const subscriptions_option_key_option: number = _input.readI32();
          subscriptions_option_key = subscriptions_option_key_option;
        }
        let subscriptions_option_value: Option<EvmEventSubscription> = undefined;
        const subscriptions_option_value_isSome = _input.readBoolean();
        if (subscriptions_option_value_isSome) {
          const subscriptions_option_value_option: EvmEventSubscription = this.deserializeEvmEventSubscription(_input);
          subscriptions_option_value = subscriptions_option_value_option;
        }
        subscriptions_option.set(subscriptions_option_key, subscriptions_option_value);
      }
      subscriptions = subscriptions_option;
    }
    return { confirmedEvents, nextEventId, nextSubscriptionId, subscriptions };
  }
  public deserializeConfirmedEventLog(_input: AbiInput): ConfirmedEventLog {
    const eventId: number = _input.readI32();
    let eventLog: Option<EvmEventLog> = undefined;
    const eventLog_isSome = _input.readBoolean();
    if (eventLog_isSome) {
      const eventLog_option: EvmEventLog = this.deserializeEvmEventLog(_input);
      eventLog = eventLog_option;
    }
    const subscriptionId: number = _input.readI32();
    return { eventId, eventLog, subscriptionId };
  }
  public deserializeEvmEventLog(_input: AbiInput): EvmEventLog {
    let data: Option<Buffer> = undefined;
    const data_isSome = _input.readBoolean();
    if (data_isSome) {
      const data_option_vecLength = _input.readI32();
      const data_option: Buffer = _input.readBytes(data_option_vecLength);
      data = data_option;
    }
    let metadata: Option<EventMetadata> = undefined;
    const metadata_isSome = _input.readBoolean();
    if (metadata_isSome) {
      const metadata_option: EventMetadata = this.deserializeEventMetadata(_input);
      metadata = metadata_option;
    }
    let topics: Option<TopicList> = undefined;
    const topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      const topics_option: TopicList = this.deserializeTopicList(_input);
      topics = topics_option;
    }
    return { data, metadata, topics };
  }
  public deserializeEventMetadata(_input: AbiInput): EventMetadata {
    let blockNumber: Option<BN> = undefined;
    const blockNumber_isSome = _input.readBoolean();
    if (blockNumber_isSome) {
      const blockNumber_option: BN = _input.readUnsignedBigInteger(32);
      blockNumber = blockNumber_option;
    }
    let logIndex: Option<BN> = undefined;
    const logIndex_isSome = _input.readBoolean();
    if (logIndex_isSome) {
      const logIndex_option: BN = _input.readUnsignedBigInteger(32);
      logIndex = logIndex_option;
    }
    let transactionIndex: Option<BN> = undefined;
    const transactionIndex_isSome = _input.readBoolean();
    if (transactionIndex_isSome) {
      const transactionIndex_option: BN = _input.readUnsignedBigInteger(32);
      transactionIndex = transactionIndex_option;
    }
    return { blockNumber, logIndex, transactionIndex };
  }
  public deserializeTopicList(_input: AbiInput): TopicList {
    let topics: Option<Array<Option<Topic>>> = undefined;
    const topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      const topics_option_vecLength = _input.readI32();
      const topics_option: Array<Option<Topic>> = [];
      for (let topics_option_i = 0; topics_option_i < topics_option_vecLength; topics_option_i++) {
        let topics_option_elem: Option<Topic> = undefined;
        const topics_option_elem_isSome = _input.readBoolean();
        if (topics_option_elem_isSome) {
          const topics_option_elem_option: Topic = this.deserializeTopic(_input);
          topics_option_elem = topics_option_elem_option;
        }
        topics_option.push(topics_option_elem);
      }
      topics = topics_option;
    }
    return { topics };
  }
  public deserializeTopic(_input: AbiInput): Topic {
    let topic: Option<string> = undefined;
    const topic_isSome = _input.readBoolean();
    if (topic_isSome) {
      const topic_option: string = _input.readString();
      topic = topic_option;
    }
    return { topic };
  }
  public deserializeEvmEventSubscription(_input: AbiInput): EvmEventSubscription {
    let chainId: Option<string> = undefined;
    const chainId_isSome = _input.readBoolean();
    if (chainId_isSome) {
      const chainId_option: string = _input.readString();
      chainId = chainId_option;
    }
    let contractAddress: Option<EvmAddress> = undefined;
    const contractAddress_isSome = _input.readBoolean();
    if (contractAddress_isSome) {
      const contractAddress_option: EvmAddress = this.deserializeEvmAddress(_input);
      contractAddress = contractAddress_option;
    }
    let fromBlock: Option<BN> = undefined;
    const fromBlock_isSome = _input.readBoolean();
    if (fromBlock_isSome) {
      const fromBlock_option: BN = _input.readUnsignedBigInteger(32);
      fromBlock = fromBlock_option;
    }
    const id: number = _input.readI32();
    const isCancelled: boolean = _input.readBoolean();
    let latestConfirmedEvent: Option<EventMetadata> = undefined;
    const latestConfirmedEvent_isSome = _input.readBoolean();
    if (latestConfirmedEvent_isSome) {
      const latestConfirmedEvent_option: EventMetadata = this.deserializeEventMetadata(_input);
      latestConfirmedEvent = latestConfirmedEvent_option;
    }
    let pendingEvent: Option<PendingEventLog> = undefined;
    const pendingEvent_isSome = _input.readBoolean();
    if (pendingEvent_isSome) {
      const pendingEvent_option: PendingEventLog = this.deserializePendingEventLog(_input);
      pendingEvent = pendingEvent_option;
    }
    let pendingHeartbeatBlock: Option<PendingHeartbeat> = undefined;
    const pendingHeartbeatBlock_isSome = _input.readBoolean();
    if (pendingHeartbeatBlock_isSome) {
      const pendingHeartbeatBlock_option: PendingHeartbeat = this.deserializePendingHeartbeat(_input);
      pendingHeartbeatBlock = pendingHeartbeatBlock_option;
    }
    let topics: Option<Array<Option<TopicList>>> = undefined;
    const topics_isSome = _input.readBoolean();
    if (topics_isSome) {
      const topics_option_vecLength = _input.readI32();
      const topics_option: Array<Option<TopicList>> = [];
      for (let topics_option_i = 0; topics_option_i < topics_option_vecLength; topics_option_i++) {
        let topics_option_elem: Option<TopicList> = undefined;
        const topics_option_elem_isSome = _input.readBoolean();
        if (topics_option_elem_isSome) {
          const topics_option_elem_option: TopicList = this.deserializeTopicList(_input);
          topics_option_elem = topics_option_elem_option;
        }
        topics_option.push(topics_option_elem);
      }
      topics = topics_option;
    }
    return { chainId, contractAddress, fromBlock, id, isCancelled, latestConfirmedEvent, pendingEvent, pendingHeartbeatBlock, topics };
  }
  public deserializeEvmAddress(_input: AbiInput): EvmAddress {
    let address: Option<string> = undefined;
    const address_isSome = _input.readBoolean();
    if (address_isSome) {
      const address_option: string = _input.readString();
      address = address_option;
    }
    return { address };
  }
  public deserializePendingEventLog(_input: AbiInput): PendingEventLog {
    let candidates: Option<Array<Option<EvmEventLog>>> = undefined;
    const candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      const candidates_option_vecLength = _input.readI32();
      const candidates_option: Array<Option<EvmEventLog>> = [];
      for (let candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        let candidates_option_elem: Option<EvmEventLog> = undefined;
        const candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          const candidates_option_elem_option: EvmEventLog = this.deserializeEvmEventLog(_input);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.push(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    const eventId: number = _input.readI32();
    const subscriptionId: number = _input.readI32();
    return { candidates, eventId, subscriptionId };
  }
  public deserializePendingHeartbeat(_input: AbiInput): PendingHeartbeat {
    let candidates: Option<Array<Option<BN>>> = undefined;
    const candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      const candidates_option_vecLength = _input.readI32();
      const candidates_option: Array<Option<BN>> = [];
      for (let candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        let candidates_option_elem: Option<BN> = undefined;
        const candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          const candidates_option_elem_option: BN = _input.readUnsignedBigInteger(32);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.push(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    return { candidates };
  }
  public deserializeFinishedComputationImpl(_input: AbiInput): FinishedComputationImpl {
    const calculateFor: BN = _input.readI64();
    const engineState: number = _input.readI8();
    const optimistic: boolean = _input.readBoolean();
    let partialOpens: Option<Hash> = undefined;
    const partialOpens_isSome = _input.readBoolean();
    if (partialOpens_isSome) {
      const partialOpens_option: Hash = _input.readHash();
      partialOpens = partialOpens_option;
    }
    return { calculateFor, engineState, optimistic, partialOpens };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const administrator: BlockchainAddress = _input.readAddress();
    let averageSalaryResult: Option<number> = undefined;
    const averageSalaryResult_isSome = _input.readBoolean();
    if (averageSalaryResult_isSome) {
      const averageSalaryResult_option: number = _input.readU32();
      averageSalaryResult = averageSalaryResult_option;
    }
    let numEmployees: Option<number> = undefined;
    const numEmployees_isSome = _input.readBoolean();
    if (numEmployees_isSome) {
      const numEmployees_option: number = _input.readU32();
      numEmployees = numEmployees_option;
    }
    return { administrator, averageSalaryResult, numEmployees };
  }
  public deserializePendingInputImpl(_input: AbiInput): PendingInputImpl {
    let commitments: Option<Array<Option<Hash>>> = undefined;
    const commitments_isSome = _input.readBoolean();
    if (commitments_isSome) {
      const commitments_option_vecLength = _input.readI32();
      const commitments_option: Array<Option<Hash>> = [];
      for (let commitments_option_i = 0; commitments_option_i < commitments_option_vecLength; commitments_option_i++) {
        let commitments_option_elem: Option<Hash> = undefined;
        const commitments_option_elem_isSome = _input.readBoolean();
        if (commitments_option_elem_isSome) {
          const commitments_option_elem_option: Hash = _input.readHash();
          commitments_option_elem = commitments_option_elem_option;
        }
        commitments_option.push(commitments_option_elem);
      }
      commitments = commitments_option;
    }
    let encryptedShares: Option<Array<Option<Buffer>>> = undefined;
    const encryptedShares_isSome = _input.readBoolean();
    if (encryptedShares_isSome) {
      const encryptedShares_option_vecLength = _input.readI32();
      const encryptedShares_option: Array<Option<Buffer>> = [];
      for (let encryptedShares_option_i = 0; encryptedShares_option_i < encryptedShares_option_vecLength; encryptedShares_option_i++) {
        let encryptedShares_option_elem: Option<Buffer> = undefined;
        const encryptedShares_option_elem_isSome = _input.readBoolean();
        if (encryptedShares_option_elem_isSome) {
          const encryptedShares_option_elem_option_vecLength = _input.readI32();
          const encryptedShares_option_elem_option: Buffer = _input.readBytes(encryptedShares_option_elem_option_vecLength);
          encryptedShares_option_elem = encryptedShares_option_elem_option;
        }
        encryptedShares_option.push(encryptedShares_option_elem);
      }
      encryptedShares = encryptedShares_option;
    }
    const engines: number = _input.readI8();
    let nodeOpenings: Option<Array<Option<Buffer>>> = undefined;
    const nodeOpenings_isSome = _input.readBoolean();
    if (nodeOpenings_isSome) {
      const nodeOpenings_option_vecLength = _input.readI32();
      const nodeOpenings_option: Array<Option<Buffer>> = [];
      for (let nodeOpenings_option_i = 0; nodeOpenings_option_i < nodeOpenings_option_vecLength; nodeOpenings_option_i++) {
        let nodeOpenings_option_elem: Option<Buffer> = undefined;
        const nodeOpenings_option_elem_isSome = _input.readBoolean();
        if (nodeOpenings_option_elem_isSome) {
          const nodeOpenings_option_elem_option_vecLength = _input.readI32();
          const nodeOpenings_option_elem_option: Buffer = _input.readBytes(nodeOpenings_option_elem_option_vecLength);
          nodeOpenings_option_elem = nodeOpenings_option_elem_option;
        }
        nodeOpenings_option.push(nodeOpenings_option_elem);
      }
      nodeOpenings = nodeOpenings_option;
    }
    let onInputtedShortname: Option<Buffer> = undefined;
    const onInputtedShortname_isSome = _input.readBoolean();
    if (onInputtedShortname_isSome) {
      const onInputtedShortname_option_vecLength = _input.readI32();
      const onInputtedShortname_option: Buffer = _input.readBytes(onInputtedShortname_option_vecLength);
      onInputtedShortname = onInputtedShortname_option;
    }
    let publicKey: Option<BlockchainPublicKey> = undefined;
    const publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      const publicKey_option: BlockchainPublicKey = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    const rejected: boolean = _input.readBoolean();
    let variable: Option<ZkClosedImpl> = undefined;
    const variable_isSome = _input.readBoolean();
    if (variable_isSome) {
      const variable_option: ZkClosedImpl = this.deserializeZkClosedImpl(_input);
      variable = variable_option;
    }
    return { commitments, encryptedShares, engines, nodeOpenings, onInputtedShortname, publicKey, rejected, variable };
  }
  public deserializePendingOnChainOpenImpl(_input: AbiInput): PendingOnChainOpenImpl {
    let bitLengths: Option<Map<Option<number>, Option<BitLengths>>> = undefined;
    const bitLengths_isSome = _input.readBoolean();
    if (bitLengths_isSome) {
      const bitLengths_option_mapLength = _input.readI32();
      const bitLengths_option: Map<Option<number>, Option<BitLengths>> = new Map();
      for (let bitLengths_option_i = 0; bitLengths_option_i < bitLengths_option_mapLength; bitLengths_option_i++) {
        let bitLengths_option_key: Option<number> = undefined;
        const bitLengths_option_key_isSome = _input.readBoolean();
        if (bitLengths_option_key_isSome) {
          const bitLengths_option_key_option: number = _input.readI32();
          bitLengths_option_key = bitLengths_option_key_option;
        }
        let bitLengths_option_value: Option<BitLengths> = undefined;
        const bitLengths_option_value_isSome = _input.readBoolean();
        if (bitLengths_option_value_isSome) {
          const bitLengths_option_value_option: BitLengths = this.deserializeBitLengths(_input);
          bitLengths_option_value = bitLengths_option_value_option;
        }
        bitLengths_option.set(bitLengths_option_key, bitLengths_option_value);
      }
      bitLengths = bitLengths_option;
    }
    let outputId: Option<OpenId> = undefined;
    const outputId_isSome = _input.readBoolean();
    if (outputId_isSome) {
      const outputId_option: OpenId = this.deserializeOpenId(_input);
      outputId = outputId_option;
    }
    let shares: Option<Map<Option<number>, Option<binder_Shares>>> = undefined;
    const shares_isSome = _input.readBoolean();
    if (shares_isSome) {
      const shares_option_mapLength = _input.readI32();
      const shares_option: Map<Option<number>, Option<binder_Shares>> = new Map();
      for (let shares_option_i = 0; shares_option_i < shares_option_mapLength; shares_option_i++) {
        let shares_option_key: Option<number> = undefined;
        const shares_option_key_isSome = _input.readBoolean();
        if (shares_option_key_isSome) {
          const shares_option_key_option: number = _input.readI32();
          shares_option_key = shares_option_key_option;
        }
        let shares_option_value: Option<binder_Shares> = undefined;
        const shares_option_value_isSome = _input.readBoolean();
        if (shares_option_value_isSome) {
          const shares_option_value_option: binder_Shares = this.deserializebinder_Shares(_input);
          shares_option_value = shares_option_value_option;
        }
        shares_option.set(shares_option_key, shares_option_value);
      }
      shares = shares_option;
    }
    let variables: Option<Array<Option<number>>> = undefined;
    const variables_isSome = _input.readBoolean();
    if (variables_isSome) {
      const variables_option_vecLength = _input.readI32();
      const variables_option: Array<Option<number>> = [];
      for (let variables_option_i = 0; variables_option_i < variables_option_vecLength; variables_option_i++) {
        let variables_option_elem: Option<number> = undefined;
        const variables_option_elem_isSome = _input.readBoolean();
        if (variables_option_elem_isSome) {
          const variables_option_elem_option: number = _input.readI32();
          variables_option_elem = variables_option_elem_option;
        }
        variables_option.push(variables_option_elem);
      }
      variables = variables_option;
    }
    return { bitLengths, outputId, shares, variables };
  }
  public deserializeBitLengths(_input: AbiInput): BitLengths {
    let bitLengths: Option<Array<Option<number>>> = undefined;
    const bitLengths_isSome = _input.readBoolean();
    if (bitLengths_isSome) {
      const bitLengths_option_vecLength = _input.readI32();
      const bitLengths_option: Array<Option<number>> = [];
      for (let bitLengths_option_i = 0; bitLengths_option_i < bitLengths_option_vecLength; bitLengths_option_i++) {
        let bitLengths_option_elem: Option<number> = undefined;
        const bitLengths_option_elem_isSome = _input.readBoolean();
        if (bitLengths_option_elem_isSome) {
          const bitLengths_option_elem_option: number = _input.readI32();
          bitLengths_option_elem = bitLengths_option_elem_option;
        }
        bitLengths_option.push(bitLengths_option_elem);
      }
      bitLengths = bitLengths_option;
    }
    return { bitLengths };
  }
  public deserializeOpenId(_input: AbiInput): OpenId {
    const id: number = _input.readI32();
    const receivedForEngine: number = _input.readI8();
    return { id, receivedForEngine };
  }
  public deserializebinder_Shares(_input: AbiInput): binder_Shares {
    let shares: Option<Array<Option<Buffer>>> = undefined;
    const shares_isSome = _input.readBoolean();
    if (shares_isSome) {
      const shares_option_vecLength = _input.readI32();
      const shares_option: Array<Option<Buffer>> = [];
      for (let shares_option_i = 0; shares_option_i < shares_option_vecLength; shares_option_i++) {
        let shares_option_elem: Option<Buffer> = undefined;
        const shares_option_elem_isSome = _input.readBoolean();
        if (shares_option_elem_isSome) {
          const shares_option_elem_option_vecLength = _input.readI32();
          const shares_option_elem_option: Buffer = _input.readBytes(shares_option_elem_option_vecLength);
          shares_option_elem = shares_option_elem_option;
        }
        shares_option.push(shares_option_elem);
      }
      shares = shares_option;
    }
    return { shares };
  }
  public deserializePreProcessMaterialType(_input: AbiInput): PreProcessMaterialType {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK(_input);
    } else if (discriminant === 1) {
      return this.deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK(_input: AbiInput): PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK {
    return { discriminant: PreProcessMaterialTypeD.PreProcessMaterialType$BINARY_INPUT_MASK,  };
  }
  public deserializePreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE(_input: AbiInput): PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE {
    return { discriminant: PreProcessMaterialTypeD.PreProcessMaterialType$BINARY_TRIPLE,  };
  }
  public deserializePreProcessMaterialState(_input: AbiInput): PreProcessMaterialState {
    let batchIds: Option<Array<Option<number>>> = undefined;
    const batchIds_isSome = _input.readBoolean();
    if (batchIds_isSome) {
      const batchIds_option_vecLength = _input.readI32();
      const batchIds_option: Array<Option<number>> = [];
      for (let batchIds_option_i = 0; batchIds_option_i < batchIds_option_vecLength; batchIds_option_i++) {
        let batchIds_option_elem: Option<number> = undefined;
        const batchIds_option_elem_isSome = _input.readBoolean();
        if (batchIds_option_elem_isSome) {
          const batchIds_option_elem_option: number = _input.readI32();
          batchIds_option_elem = batchIds_option_elem_option;
        }
        batchIds_option.push(batchIds_option_elem);
      }
      batchIds = batchIds_option;
    }
    const numElementsToPrefetch: number = _input.readI32();
    const numRequestedBatches: number = _input.readI32();
    const numUsedElements: BN = _input.readI64();
    return { batchIds, numElementsToPrefetch, numRequestedBatches, numUsedElements };
  }
  public async getState(): Promise<ZkStateImmutable> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeZkStateImmutable(input);
  }

  public deserializeComputeAverageSalaryAction(_input: AbiInput): ComputeAverageSalaryAction {
    return { discriminant: "compute_average_salary",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface ZkStateImmutable {
  attestations: Option<Map<Option<number>, Option<AttestationImpl>>>;
  calculationStatus: Option<CalculationStatus>;
  computationState: Option<ComputationStateImpl>;
  defaultOptimisticMode: boolean;
  engines: Option<EngineState>;
  externalEvents: Option<ExternalEventState>;
  finishedComputations: Option<Array<Option<FinishedComputationImpl>>>;
  nextAttestationId: number;
  nextVariableId: number;
  nodeRegistryContract: Option<BlockchainAddress>;
  openState: ContractState;
  pendingInput: Option<Map<Option<number>, Option<PendingInputImpl>>>;
  pendingOnChainOpen: Option<Map<Option<number>, Option<PendingOnChainOpenImpl>>>;
  preProcessMaterials: Option<Map<Option<PreProcessMaterialType>, Option<PreProcessMaterialState>>>;
  preprocessContract: Option<BlockchainAddress>;
  variables: Option<Map<Option<number>, Option<ZkClosedImpl>>>;
  zkComputationDeadline: BN;
}

export interface AttestationImpl {
  data: Option<Buffer>;
  id: number;
  signatures: Option<Array<Option<Signature>>>;
}

export enum CalculationStatusD {
  CalculationStatus$WAITING = 0,
  CalculationStatus$CALCULATING = 1,
  CalculationStatus$OUTPUT = 2,
  CalculationStatus$MALICIOUS_BEHAVIOUR = 3,
  CalculationStatus$DONE = 4,
}
export type CalculationStatus =
  | CalculationStatusCalculationStatus$WAITING
  | CalculationStatusCalculationStatus$CALCULATING
  | CalculationStatusCalculationStatus$OUTPUT
  | CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR
  | CalculationStatusCalculationStatus$DONE;

export interface CalculationStatusCalculationStatus$WAITING {
  discriminant: CalculationStatusD.CalculationStatus$WAITING;
}

export interface CalculationStatusCalculationStatus$CALCULATING {
  discriminant: CalculationStatusD.CalculationStatus$CALCULATING;
}

export interface CalculationStatusCalculationStatus$OUTPUT {
  discriminant: CalculationStatusD.CalculationStatus$OUTPUT;
}

export interface CalculationStatusCalculationStatus$MALICIOUS_BEHAVIOUR {
  discriminant: CalculationStatusD.CalculationStatus$MALICIOUS_BEHAVIOUR;
}

export interface CalculationStatusCalculationStatus$DONE {
  discriminant: CalculationStatusD.CalculationStatus$DONE;
}

export interface ComputationStateImpl {
  calculateFor: BN;
  computationOutput: Option<ComputationOutput>;
  confirmedInputs: number;
  multiplicationCount: number;
  offsetIntoFirstTripleBatch: number;
  onComputeCompleteShortname: Option<Buffer>;
  openState: Option<WasmRealContractState>;
  optimisticMode: boolean;
  tripleBatches: Option<Array<Option<number>>>;
  variables: Option<Map<Option<number>, Option<ZkClosedImpl>>>;
}

export interface ComputationOutput {
  engines: number;
  outputInformation: Option<Array<Option<EngineOutputInformation>>>;
  variables: Option<Array<Option<PendingOutputVariable>>>;
}

export interface EngineOutputInformation {
  linearCombinationElement: Option<Buffer>;
  partialOpenings: Option<Hash>;
  verificationSeed: Option<Buffer>;
}

export interface PendingOutputVariable {
  bitLength: number;
  information: Option<Buffer>;
  variableId: number;
}

export interface WasmRealContractState {
  avlTrees: Option<Map<Option<number>, Option<AvlTreeWrapper>>>;
  computationInput: Option<ComputationInput>;
  openState: Option<Buffer>;
}

export interface AvlTreeWrapper {
  avlTree: Option<Map<Option<ComparableByteArray>, Option<Buffer>>>;
}

export interface ComparableByteArray {
  data: Option<Buffer>;
}

export interface ComputationInput {
  calledFunctionArguments: Option<Array<Option<Buffer>>>;
  calledFunctionShortname: number;
}

export interface ZkClosedImpl {
  id: number;
  information: Option<Buffer>;
  inputMaskOffset: number;
  maskedInputShare: Option<Buffer>;
  openValue: Option<Buffer>;
  owner: Option<BlockchainAddress>;
  sealed: boolean;
  shareBitLengths: Option<Array<Option<number>>>;
  transaction: Option<Hash>;
}

export interface EngineState {
  engines: Option<Array<Option<EngineInformation>>>;
}

export interface EngineInformation {
  identity: Option<BlockchainAddress>;
  publicKey: Option<BlockchainPublicKey>;
  restInterface: Option<string>;
}

export interface ExternalEventState {
  confirmedEvents: Option<Map<Option<number>, Option<ConfirmedEventLog>>>;
  nextEventId: number;
  nextSubscriptionId: number;
  subscriptions: Option<Map<Option<number>, Option<EvmEventSubscription>>>;
}

export interface ConfirmedEventLog {
  eventId: number;
  eventLog: Option<EvmEventLog>;
  subscriptionId: number;
}

export interface EvmEventLog {
  data: Option<Buffer>;
  metadata: Option<EventMetadata>;
  topics: Option<TopicList>;
}

export interface EventMetadata {
  blockNumber: Option<BN>;
  logIndex: Option<BN>;
  transactionIndex: Option<BN>;
}

export interface TopicList {
  topics: Option<Array<Option<Topic>>>;
}

export interface Topic {
  topic: Option<string>;
}

export interface EvmEventSubscription {
  chainId: Option<string>;
  contractAddress: Option<EvmAddress>;
  fromBlock: Option<BN>;
  id: number;
  isCancelled: boolean;
  latestConfirmedEvent: Option<EventMetadata>;
  pendingEvent: Option<PendingEventLog>;
  pendingHeartbeatBlock: Option<PendingHeartbeat>;
  topics: Option<Array<Option<TopicList>>>;
}

export interface EvmAddress {
  address: Option<string>;
}

export interface PendingEventLog {
  candidates: Option<Array<Option<EvmEventLog>>>;
  eventId: number;
  subscriptionId: number;
}

export interface PendingHeartbeat {
  candidates: Option<Array<Option<BN>>>;
}

export interface FinishedComputationImpl {
  calculateFor: BN;
  engineState: number;
  optimistic: boolean;
  partialOpens: Option<Hash>;
}

export interface ContractState {
  administrator: BlockchainAddress;
  averageSalaryResult: Option<number>;
  numEmployees: Option<number>;
}

export interface PendingInputImpl {
  commitments: Option<Array<Option<Hash>>>;
  encryptedShares: Option<Array<Option<Buffer>>>;
  engines: number;
  nodeOpenings: Option<Array<Option<Buffer>>>;
  onInputtedShortname: Option<Buffer>;
  publicKey: Option<BlockchainPublicKey>;
  rejected: boolean;
  variable: Option<ZkClosedImpl>;
}

export interface PendingOnChainOpenImpl {
  bitLengths: Option<Map<Option<number>, Option<BitLengths>>>;
  outputId: Option<OpenId>;
  shares: Option<Map<Option<number>, Option<binder_Shares>>>;
  variables: Option<Array<Option<number>>>;
}

export interface BitLengths {
  bitLengths: Option<Array<Option<number>>>;
}

export interface OpenId {
  id: number;
  receivedForEngine: number;
}

export interface binder_Shares {
  shares: Option<Array<Option<Buffer>>>;
}

export enum PreProcessMaterialTypeD {
  PreProcessMaterialType$BINARY_INPUT_MASK = 0,
  PreProcessMaterialType$BINARY_TRIPLE = 1,
}
export type PreProcessMaterialType =
  | PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK
  | PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE;

export interface PreProcessMaterialTypePreProcessMaterialType$BINARY_INPUT_MASK {
  discriminant: PreProcessMaterialTypeD.PreProcessMaterialType$BINARY_INPUT_MASK;
}

export interface PreProcessMaterialTypePreProcessMaterialType$BINARY_TRIPLE {
  discriminant: PreProcessMaterialTypeD.PreProcessMaterialType$BINARY_TRIPLE;
}

export interface PreProcessMaterialState {
  batchIds: Option<Array<Option<number>>>;
  numElementsToPrefetch: number;
  numRequestedBatches: number;
  numUsedElements: BN;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function computeAverageSalary(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function addSalary(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): ZkStateImmutable;
export function deserializeState(bytes: Buffer): ZkStateImmutable;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ZkStateImmutable;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ZkStateImmutable {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new AverageSalary(client, address).deserializeZkStateImmutable(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new AverageSalary(
      state.client,
      state.address
    ).deserializeZkStateImmutable(input);
  }
}

export type Action =
  | ComputeAverageSalaryAction;

export interface ComputeAverageSalaryAction {
  discriminant: "compute_average_salary";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new AverageSalary(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeComputeAverageSalaryAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new AverageSalary(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

