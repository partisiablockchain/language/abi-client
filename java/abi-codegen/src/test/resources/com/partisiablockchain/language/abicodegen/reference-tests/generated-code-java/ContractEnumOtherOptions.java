// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractEnumOtherOptions {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractEnumOtherOptions(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Vehicle deserializeVehicle(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 2) {
      return deserializeVehicleBicycle(_input);
    } else if (discriminant == 5) {
      return deserializeVehicleCar(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private VehicleBicycle deserializeVehicleBicycle(AbiInput _input) {
    int wheelDiameter = _input.readI32();
    return new VehicleBicycle(wheelDiameter);
  }
  private VehicleCar deserializeVehicleCar(AbiInput _input) {
    byte engineSize = _input.readU8();
    boolean supportsTrailer = _input.readBoolean();
    return new VehicleCar(engineSize, supportsTrailer);
  }

  @AbiGenerated
  public enum VehicleD {
    BICYCLE(2),
    CAR(5),
    ;
    private final int value;
    VehicleD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Vehicle {
    VehicleD discriminant();
  }

  @AbiGenerated
  public record VehicleBicycle(int wheelDiameter) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.BICYCLE;
    }
  }

  @AbiGenerated
  public record VehicleCar(byte engineSize, boolean supportsTrailer) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.CAR;
    }
  }

  public static Vehicle deserializeSpecialVehicle(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    return new ContractEnumOtherOptions(null, null).deserializeVehicle(input);
  }
}
