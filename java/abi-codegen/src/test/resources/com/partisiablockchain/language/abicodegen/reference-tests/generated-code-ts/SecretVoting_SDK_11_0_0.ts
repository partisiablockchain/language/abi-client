// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class SecretVoting_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVoteBasis(_input: AbiInput): VoteBasis {
    const requiredRatio: Fraction = this.deserializeFraction(_input);
    const absentAsAgainst: boolean = _input.readBoolean();
    return { requiredRatio, absentAsAgainst };
  }
  public deserializeVoteResult(_input: AbiInput): VoteResult {
    const votesFor: number = _input.readU32();
    const votesAgainst: number = _input.readU32();
    const passed: boolean = _input.readBoolean();
    return { votesFor, votesAgainst, passed };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const administrator: BlockchainAddress = _input.readAddress();
    const deadlineVotingTime: BN = _input.readI64();
    const deadlineCommitmentTime: BN = _input.readI64();
    const allowedVoters_vecLength = _input.readI32();
    const allowedVoters: BlockchainAddress[] = [];
    for (let allowedVoters_i = 0; allowedVoters_i < allowedVoters_vecLength; allowedVoters_i++) {
      const allowedVoters_elem: BlockchainAddress = _input.readAddress();
      allowedVoters.push(allowedVoters_elem);
    }
    const voteDefinition: VoteBasis = this.deserializeVoteBasis(_input);
    let voteResult: Option<VoteResult> = undefined;
    const voteResult_isSome = _input.readBoolean();
    if (voteResult_isSome) {
      const voteResult_option: VoteResult = this.deserializeVoteResult(_input);
      voteResult = voteResult_option;
    }
    return { administrator, deadlineVotingTime, deadlineCommitmentTime, allowedVoters, voteDefinition, voteResult };
  }
  public deserializeFraction(_input: AbiInput): Fraction {
    const numerator: number = _input.readU32();
    const denominator: number = _input.readU32();
    return { numerator, denominator };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeStartVoteCountingAction(_input: AbiInput): StartVoteCountingAction {
    return { discriminant: "start_vote_counting",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const votingDurationMs: number = _input.readU32();
    const allowedVoters_vecLength = _input.readI32();
    const allowedVoters: BlockchainAddress[] = [];
    for (let allowedVoters_i = 0; allowedVoters_i < allowedVoters_vecLength; allowedVoters_i++) {
      const allowedVoters_elem: BlockchainAddress = _input.readAddress();
      allowedVoters.push(allowedVoters_elem);
    }
    const voteDefinition: VoteBasis = this.deserializeVoteBasis(_input);
    return { discriminant: "initialize", votingDurationMs, allowedVoters, voteDefinition };
  }

}
export interface VoteBasis {
  requiredRatio: Fraction;
  absentAsAgainst: boolean;
}
function serializeVoteBasis(_out: AbiOutput, _value: VoteBasis): void {
  const { requiredRatio, absentAsAgainst } = _value;
  serializeFraction(_out, requiredRatio);
  _out.writeBoolean(absentAsAgainst);
}

export interface VoteResult {
  votesFor: number;
  votesAgainst: number;
  passed: boolean;
}

export interface ContractState {
  administrator: BlockchainAddress;
  deadlineVotingTime: BN;
  deadlineCommitmentTime: BN;
  allowedVoters: BlockchainAddress[];
  voteDefinition: VoteBasis;
  voteResult: Option<VoteResult>;
}

export interface Fraction {
  numerator: number;
  denominator: number;
}
function serializeFraction(_out: AbiOutput, _value: Fraction): void {
  const { numerator, denominator } = _value;
  _out.writeU32(numerator);
  _out.writeU32(denominator);
}

export function initialize(votingDurationMs: number, allowedVoters: BlockchainAddress[], voteDefinition: VoteBasis): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU32(votingDurationMs);
    _out.writeI32(allowedVoters.length);
    for (const allowedVoters_vec of allowedVoters) {
      _out.writeAddress(allowedVoters_vec);
    }
    serializeVoteBasis(_out, voteDefinition);
  });
}

export function startVoteCounting(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function addVote(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new SecretVoting_SDK_11_0_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new SecretVoting_SDK_11_0_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | StartVoteCountingAction;

export interface StartVoteCountingAction {
  discriminant: "start_vote_counting";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new SecretVoting_SDK_11_0_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeStartVoteCountingAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  votingDurationMs: number;
  allowedVoters: BlockchainAddress[];
  voteDefinition: VoteBasis;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new SecretVoting_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

