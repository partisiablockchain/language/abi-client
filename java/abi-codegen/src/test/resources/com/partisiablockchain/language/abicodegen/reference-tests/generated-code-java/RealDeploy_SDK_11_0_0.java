// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class RealDeploy_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public RealDeploy_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private SemanticVersion deserializeSemanticVersion(AbiInput _input) {
    int major = _input.readU32();
    int minor = _input.readU32();
    int patch = _input.readU32();
    return new SemanticVersion(major, minor, patch);
  }
  private RealDeployContractState deserializeRealDeployContractState(AbiInput _input) {
    var bindingJar_vecLength = _input.readI32();
    byte[] bindingJar = _input.readBytes(bindingJar_vecLength);
    BlockchainAddress preProcess = _input.readAddress();
    BlockchainAddress zkNodeRegistry = _input.readAddress();
    SemanticVersion supportedProtocolVersion = deserializeSemanticVersion(_input);
    return new RealDeployContractState(bindingJar, preProcess, zkNodeRegistry, supportedProtocolVersion);
  }
  public RealDeployContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeRealDeployContractState(input);
  }

  private DeployContractV1Action deserializeDeployContractV1Action(AbiInput _input) {
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var initialization_vecLength = _input.readI32();
    byte[] initialization = _input.readBytes(initialization_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    long requiredStakes = _input.readI64();
    return new DeployContractV1Action(contractJar, initialization, abi, requiredStakes);
  }

  private DeployContractV2Action deserializeDeployContractV2Action(AbiInput _input) {
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var initialization_vecLength = _input.readI32();
    byte[] initialization = _input.readBytes(initialization_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    long requiredStakes = _input.readI64();
    var jurisdictions_vecLength = _input.readI32();
    List<List<Integer>> jurisdictions = new ArrayList<>();
    for (int jurisdictions_i = 0; jurisdictions_i < jurisdictions_vecLength; jurisdictions_i++) {
      var jurisdictions_elem_vecLength = _input.readI32();
      List<Integer> jurisdictions_elem = new ArrayList<>();
      for (int jurisdictions_elem_i = 0; jurisdictions_elem_i < jurisdictions_elem_vecLength; jurisdictions_elem_i++) {
        int jurisdictions_elem_elem = _input.readI32();
        jurisdictions_elem.add(jurisdictions_elem_elem);
      }
      jurisdictions.add(jurisdictions_elem);
    }
    return new DeployContractV2Action(contractJar, initialization, abi, requiredStakes, jurisdictions);
  }

  private NodeRequestCallbackCallback deserializeNodeRequestCallbackCallback(AbiInput _input) {
    BlockchainAddress contractAddress = _input.readAddress();
    var contractJar_vecLength = _input.readI32();
    byte[] contractJar = _input.readBytes(contractJar_vecLength);
    var initialization_vecLength = _input.readI32();
    byte[] initialization = _input.readBytes(initialization_vecLength);
    var abi_vecLength = _input.readI32();
    byte[] abi = _input.readBytes(abi_vecLength);
    long tokensLockedToContract = _input.readI64();
    return new NodeRequestCallbackCallback(contractAddress, contractJar, initialization, abi, tokensLockedToContract);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    var bindingJar_vecLength = _input.readI32();
    byte[] bindingJar = _input.readBytes(bindingJar_vecLength);
    BlockchainAddress preProcess = _input.readAddress();
    BlockchainAddress zkNodeRegistry = _input.readAddress();
    SemanticVersion supportedProtocolVersion = deserializeSemanticVersion(_input);
    return new InitInit(bindingJar, preProcess, zkNodeRegistry, supportedProtocolVersion);
  }


  @AbiGenerated
  public record SemanticVersion(int major, int minor, int patch) {
    public void serialize(AbiOutput _out) {
      _out.writeU32(major);
      _out.writeU32(minor);
      _out.writeU32(patch);
    }
  }

  @AbiGenerated
  public record RealDeployContractState(byte[] bindingJar, BlockchainAddress preProcess, BlockchainAddress zkNodeRegistry, SemanticVersion supportedProtocolVersion) {
    public static RealDeployContractState deserialize(byte[] bytes) {
      return RealDeploy_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] init(byte[] bindingJar, BlockchainAddress preProcess, BlockchainAddress zkNodeRegistry, SemanticVersion supportedProtocolVersion) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(bindingJar.length);
      _out.writeBytes(bindingJar);
      _out.writeAddress(preProcess);
      _out.writeAddress(zkNodeRegistry);
      supportedProtocolVersion.serialize(_out);
    });
  }

  public static byte[] deployContractV1(byte[] contractJar, byte[] initialization, byte[] abi, long requiredStakes) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeI32(contractJar.length);
      _out.writeBytes(contractJar);
      _out.writeI32(initialization.length);
      _out.writeBytes(initialization);
      _out.writeI32(abi.length);
      _out.writeBytes(abi);
      _out.writeI64(requiredStakes);
    });
  }

  public static byte[] deployContractV2(byte[] contractJar, byte[] initialization, byte[] abi, long requiredStakes, List<List<Integer>> jurisdictions) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeI32(contractJar.length);
      _out.writeBytes(contractJar);
      _out.writeI32(initialization.length);
      _out.writeBytes(initialization);
      _out.writeI32(abi.length);
      _out.writeBytes(abi);
      _out.writeI64(requiredStakes);
      _out.writeI32(jurisdictions.size());
      for (List<Integer> jurisdictions_vec : jurisdictions) {
        _out.writeI32(jurisdictions_vec.size());
        for (int jurisdictions_vec_vec : jurisdictions_vec) {
          _out.writeI32(jurisdictions_vec_vec);
        }
      }
    });
  }

  public static byte[] nodeRequestCallback(BlockchainAddress contractAddress, byte[] contractJar, byte[] initialization, byte[] abi, long tokensLockedToContract) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeAddress(contractAddress);
      _out.writeI32(contractJar.length);
      _out.writeBytes(contractJar);
      _out.writeI32(initialization.length);
      _out.writeBytes(initialization);
      _out.writeI32(abi.length);
      _out.writeBytes(abi);
      _out.writeI64(tokensLockedToContract);
    });
  }

  public static RealDeployContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new RealDeploy_SDK_11_0_0(client, address).deserializeRealDeployContractState(input);
  }
  
  public static RealDeployContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static RealDeployContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record DeployContractV1Action(byte[] contractJar, byte[] initialization, byte[] abi, long requiredStakes) implements Action {
  }
  @AbiGenerated
  public record DeployContractV2Action(byte[] contractJar, byte[] initialization, byte[] abi, long requiredStakes, List<List<Integer>> jurisdictions) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RealDeploy_SDK_11_0_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeDeployContractV1Action(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeDeployContractV2Action(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record NodeRequestCallbackCallback(BlockchainAddress contractAddress, byte[] contractJar, byte[] initialization, byte[] abi, long tokensLockedToContract) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RealDeploy_SDK_11_0_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeNodeRequestCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(byte[] bindingJar, BlockchainAddress preProcess, BlockchainAddress zkNodeRegistry, SemanticVersion supportedProtocolVersion) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RealDeploy_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
