// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractOptionVec_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractOptionVec_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    List<Long> optionVec = null;
    var optionVec_isSome = _input.readBoolean();
    if (optionVec_isSome) {
      var optionVec_option_vecLength = _input.readI32();
      List<Long> optionVec_option = new ArrayList<>();
      for (int optionVec_option_i = 0; optionVec_option_i < optionVec_option_vecLength; optionVec_option_i++) {
        long optionVec_option_elem = _input.readU64();
        optionVec_option.add(optionVec_option_elem);
      }
      optionVec = optionVec_option;
    }
    Long optionOption = null;
    var optionOption_isSome = _input.readBoolean();
    if (optionOption_isSome) {
      Long optionOption_option = null;
      var optionOption_option_isSome = _input.readBoolean();
      if (optionOption_option_isSome) {
        long optionOption_option_option = _input.readU64();
        optionOption_option = optionOption_option_option;
      }
      optionOption = optionOption_option;
    }
    return new ExampleContractState(optionVec, optionOption);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateVecAction deserializeUpdateVecAction(AbiInput _input) {
    List<Long> value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      var value_option_vecLength = _input.readI32();
      List<Long> value_option = new ArrayList<>();
      for (int value_option_i = 0; value_option_i < value_option_vecLength; value_option_i++) {
        long value_option_elem = _input.readU64();
        value_option.add(value_option_elem);
      }
      value = value_option;
    }
    return new UpdateVecAction(value);
  }

  private UpdateOptionOptionAction deserializeUpdateOptionOptionAction(AbiInput _input) {
    Long value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      Long value_option = null;
      var value_option_isSome = _input.readBoolean();
      if (value_option_isSome) {
        long value_option_option = _input.readU64();
        value_option = value_option_option;
      }
      value = value_option;
    }
    return new UpdateOptionOptionAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    List<Long> optionVec = null;
    var optionVec_isSome = _input.readBoolean();
    if (optionVec_isSome) {
      var optionVec_option_vecLength = _input.readI32();
      List<Long> optionVec_option = new ArrayList<>();
      for (int optionVec_option_i = 0; optionVec_option_i < optionVec_option_vecLength; optionVec_option_i++) {
        long optionVec_option_elem = _input.readU64();
        optionVec_option.add(optionVec_option_elem);
      }
      optionVec = optionVec_option;
    }
    Long optionOption = null;
    var optionOption_isSome = _input.readBoolean();
    if (optionOption_isSome) {
      Long optionOption_option = null;
      var optionOption_option_isSome = _input.readBoolean();
      if (optionOption_option_isSome) {
        long optionOption_option_option = _input.readU64();
        optionOption_option = optionOption_option_option;
      }
      optionOption = optionOption_option;
    }
    return new InitializeInit(optionVec, optionOption);
  }


  @AbiGenerated
  public record ExampleContractState(List<Long> optionVec, Long optionOption) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractOptionVec_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(List<Long> optionVec, Long optionOption) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeBoolean(optionVec != null);
      if (optionVec != null) {
        _out.writeI32(optionVec.size());
        for (long optionVec_vec : optionVec) {
          _out.writeU64(optionVec_vec);
        }
      }
      _out.writeBoolean(optionOption != null);
      if (optionOption != null) {
        _out.writeBoolean(optionOption != null);
        if (optionOption != null) {
          _out.writeU64(optionOption);
        }
      }
    });
  }

  public static byte[] updateVec(List<Long> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("94c19a8e03"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeI32(value.size());
        for (long value_vec : value) {
          _out.writeU64(value_vec);
        }
      }
    });
  }

  public static byte[] updateOptionOption(Long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b497abf309"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeBoolean(value != null);
        if (value != null) {
          _out.writeU64(value);
        }
      }
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractOptionVec_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateVecAction(List<Long> value) implements Action {
  }
  @AbiGenerated
  public record UpdateOptionOptionAction(Long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptionVec_SDK_15_4_0(null, null);
    if (shortname.equals("94c19a8e03")) {
      return contract.deserializeUpdateVecAction(input);
    } else if (shortname.equals("b497abf309")) {
      return contract.deserializeUpdateOptionOptionAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(List<Long> optionVec, Long optionOption) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptionVec_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
