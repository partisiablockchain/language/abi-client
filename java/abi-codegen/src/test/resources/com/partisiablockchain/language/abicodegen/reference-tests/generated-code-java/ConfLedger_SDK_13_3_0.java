// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ConfLedger_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ConfLedger_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Commitment deserializeCommitment(AbiInput _input) {
    byte[] metaInformation = null;
    var metaInformation_isSome = _input.readBoolean();
    if (metaInformation_isSome) {
      var metaInformation_option_vecLength = _input.readI32();
      byte[] metaInformation_option = _input.readBytes(metaInformation_option_vecLength);
      metaInformation = metaInformation_option;
    }
    var body_vecLength = _input.readI32();
    byte[] body = _input.readBytes(body_vecLength);
    return new Commitment(metaInformation, body);
  }
  private ConfLedger deserializeConfLedger(AbiInput _input) {
    var commitments_mapLength = _input.readI32();
    Map<BlockchainAddress, List<Commitment>> commitments = new HashMap<>();
    for (int commitments_i = 0; commitments_i < commitments_mapLength; commitments_i++) {
      BlockchainAddress commitments_key = _input.readAddress();
      var commitments_value_vecLength = _input.readI32();
      List<Commitment> commitments_value = new ArrayList<>();
      for (int commitments_value_i = 0; commitments_value_i < commitments_value_vecLength; commitments_value_i++) {
        Commitment commitments_value_elem = deserializeCommitment(_input);
        commitments_value.add(commitments_value_elem);
      }
      commitments.put(commitments_key, commitments_value);
    }
    var publicKeys_mapLength = _input.readI32();
    Map<BlockchainAddress, byte[]> publicKeys = new HashMap<>();
    for (int publicKeys_i = 0; publicKeys_i < publicKeys_mapLength; publicKeys_i++) {
      BlockchainAddress publicKeys_key = _input.readAddress();
      var publicKeys_value_vecLength = _input.readI32();
      byte[] publicKeys_value = _input.readBytes(publicKeys_value_vecLength);
      publicKeys.put(publicKeys_key, publicKeys_value);
    }
    long stash = _input.readU64();
    return new ConfLedger(commitments, publicKeys, stash);
  }
  public ConfLedger getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeConfLedger(input);
  }

  private ProvidePublicKeyAction deserializeProvidePublicKeyAction(AbiInput _input) {
    var vk_vecLength = _input.readI32();
    byte[] vk = _input.readBytes(vk_vecLength);
    return new ProvidePublicKeyAction(vk);
  }

  private MintAction deserializeMintAction(AbiInput _input) {
    Commitment c = deserializeCommitment(_input);
    var v_vecLength = _input.readI32();
    byte[] v = _input.readBytes(v_vecLength);
    long amount = _input.readU64();
    return new MintAction(c, v, amount);
  }

  private SplitAction deserializeSplitAction(AbiInput _input) {
    Commitment cSource = deserializeCommitment(_input);
    var destinationCommitments_vecLength = _input.readI32();
    List<Commitment> destinationCommitments = new ArrayList<>();
    for (int destinationCommitments_i = 0; destinationCommitments_i < destinationCommitments_vecLength; destinationCommitments_i++) {
      Commitment destinationCommitments_elem = deserializeCommitment(_input);
      destinationCommitments.add(destinationCommitments_elem);
    }
    var sumProof_vecLength = _input.readI32();
    byte[] sumProof = _input.readBytes(sumProof_vecLength);
    var overflowProof_vecLength = _input.readI32();
    byte[] overflowProof = _input.readBytes(overflowProof_vecLength);
    return new SplitAction(cSource, destinationCommitments, sumProof, overflowProof);
  }

  private CombineAction deserializeCombineAction(AbiInput _input) {
    Commitment cTarget = deserializeCommitment(_input);
    var sourceCommitments_vecLength = _input.readI32();
    List<Commitment> sourceCommitments = new ArrayList<>();
    for (int sourceCommitments_i = 0; sourceCommitments_i < sourceCommitments_vecLength; sourceCommitments_i++) {
      Commitment sourceCommitments_elem = deserializeCommitment(_input);
      sourceCommitments.add(sourceCommitments_elem);
    }
    var sumProof_vecLength = _input.readI32();
    byte[] sumProof = _input.readBytes(sumProof_vecLength);
    var overflowProof_vecLength = _input.readI32();
    byte[] overflowProof = _input.readBytes(overflowProof_vecLength);
    return new CombineAction(cTarget, sourceCommitments, sumProof, overflowProof);
  }

  private TransferAction deserializeTransferAction(AbiInput _input) {
    BlockchainAddress targetAddress = _input.readAddress();
    Commitment cTarget = deserializeCommitment(_input);
    Commitment cSource = deserializeCommitment(_input);
    var sameValueProof_vecLength = _input.readI32();
    byte[] sameValueProof = _input.readBytes(sameValueProof_vecLength);
    return new TransferAction(targetAddress, cTarget, cSource, sameValueProof);
  }

  private WithdrawAction deserializeWithdrawAction(AbiInput _input) {
    Commitment cSource = deserializeCommitment(_input);
    var cSourceOpeningInformation_vecLength = _input.readI32();
    byte[] cSourceOpeningInformation = _input.readBytes(cSourceOpeningInformation_vecLength);
    var tokenProof_vecLength = _input.readI32();
    byte[] tokenProof = _input.readBytes(tokenProof_vecLength);
    long amount = _input.readU64();
    BlockchainAddress address = _input.readAddress();
    return new WithdrawAction(cSource, cSourceOpeningInformation, tokenProof, amount, address);
  }

  private ReadAction deserializeReadAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    return new ReadAction(address);
  }

  private TemplateCallbackCallback deserializeTemplateCallbackCallback(AbiInput _input) {
    return new TemplateCallbackCallback();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record Commitment(byte[] metaInformation, byte[] body) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(metaInformation != null);
      if (metaInformation != null) {
        _out.writeI32(metaInformation.length);
        _out.writeBytes(metaInformation);
      }
      _out.writeI32(body.length);
      _out.writeBytes(body);
    }
  }

  @AbiGenerated
  public record ConfLedger(Map<BlockchainAddress, List<Commitment>> commitments, Map<BlockchainAddress, byte[]> publicKeys, long stash) {
    public static ConfLedger deserialize(byte[] bytes) {
      return ConfLedger_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] providePublicKey(byte[] vk) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeI32(vk.length);
      _out.writeBytes(vk);
    });
  }

  public static byte[] mint(Commitment c, byte[] v, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      c.serialize(_out);
      _out.writeI32(v.length);
      _out.writeBytes(v);
      _out.writeU64(amount);
    });
  }

  public static byte[] split(Commitment cSource, List<Commitment> destinationCommitments, byte[] sumProof, byte[] overflowProof) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      cSource.serialize(_out);
      _out.writeI32(destinationCommitments.size());
      for (Commitment destinationCommitments_vec : destinationCommitments) {
        destinationCommitments_vec.serialize(_out);
      }
      _out.writeI32(sumProof.length);
      _out.writeBytes(sumProof);
      _out.writeI32(overflowProof.length);
      _out.writeBytes(overflowProof);
    });
  }

  public static byte[] combine(Commitment cTarget, List<Commitment> sourceCommitments, byte[] sumProof, byte[] overflowProof) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      cTarget.serialize(_out);
      _out.writeI32(sourceCommitments.size());
      for (Commitment sourceCommitments_vec : sourceCommitments) {
        sourceCommitments_vec.serialize(_out);
      }
      _out.writeI32(sumProof.length);
      _out.writeBytes(sumProof);
      _out.writeI32(overflowProof.length);
      _out.writeBytes(overflowProof);
    });
  }

  public static byte[] transfer(BlockchainAddress targetAddress, Commitment cTarget, Commitment cSource, byte[] sameValueProof) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeAddress(targetAddress);
      cTarget.serialize(_out);
      cSource.serialize(_out);
      _out.writeI32(sameValueProof.length);
      _out.writeBytes(sameValueProof);
    });
  }

  public static byte[] withdraw(Commitment cSource, byte[] cSourceOpeningInformation, byte[] tokenProof, long amount, BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      cSource.serialize(_out);
      _out.writeI32(cSourceOpeningInformation.length);
      _out.writeBytes(cSourceOpeningInformation);
      _out.writeI32(tokenProof.length);
      _out.writeBytes(tokenProof);
      _out.writeU64(amount);
      _out.writeAddress(address);
    });
  }

  public static byte[] read(BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
      _out.writeAddress(address);
    });
  }

  public static byte[] templateCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("10"));
    });
  }

  public static ConfLedger deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ConfLedger_SDK_13_3_0(client, address).deserializeConfLedger(input);
  }
  
  public static ConfLedger deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ConfLedger deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ProvidePublicKeyAction(byte[] vk) implements Action {
  }
  @AbiGenerated
  public record MintAction(Commitment c, byte[] v, long amount) implements Action {
  }
  @AbiGenerated
  public record SplitAction(Commitment cSource, List<Commitment> destinationCommitments, byte[] sumProof, byte[] overflowProof) implements Action {
  }
  @AbiGenerated
  public record CombineAction(Commitment cTarget, List<Commitment> sourceCommitments, byte[] sumProof, byte[] overflowProof) implements Action {
  }
  @AbiGenerated
  public record TransferAction(BlockchainAddress targetAddress, Commitment cTarget, Commitment cSource, byte[] sameValueProof) implements Action {
  }
  @AbiGenerated
  public record WithdrawAction(Commitment cSource, byte[] cSourceOpeningInformation, byte[] tokenProof, long amount, BlockchainAddress address) implements Action {
  }
  @AbiGenerated
  public record ReadAction(BlockchainAddress address) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConfLedger_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeProvidePublicKeyAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeMintAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeSplitAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeCombineAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeTransferAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeWithdrawAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeReadAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record TemplateCallbackCallback() implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConfLedger_SDK_13_3_0(null, null);
    if (shortname.equals("10")) {
      return contract.deserializeTemplateCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConfLedger_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
