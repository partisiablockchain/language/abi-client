// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class Aes_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public Aes_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private AesKey deserializeAesKey(AbiInput _input) {
    BigInteger key = _input.readSignedBigInteger(16);
    return new AesKey(key);
  }
  private AesPlainText deserializeAesPlainText(AbiInput _input) {
    BigInteger plaintext = _input.readSignedBigInteger(16);
    return new AesPlainText(plaintext);
  }
  private AesContractState deserializeAesContractState(AbiInput _input) {
    SecretVarId aesKeyVariableId = null;
    var aesKeyVariableId_isSome = _input.readBoolean();
    if (aesKeyVariableId_isSome) {
      SecretVarId aesKeyVariableId_option = deserializeSecretVarId(_input);
      aesKeyVariableId = aesKeyVariableId_option;
    }
    BlockchainAddress permissionChangeKey = _input.readAddress();
    return new AesContractState(aesKeyVariableId, permissionChangeKey);
  }
  private SecretVarId deserializeSecretVarId(AbiInput _input) {
    int rawId = _input.readU32();
    return new SecretVarId(rawId);
  }
  public AesContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeAesContractState(input);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress permissionChangeKey = _input.readAddress();
    return new InitializeInit(permissionChangeKey);
  }


  @AbiGenerated
  public record AesKey(BigInteger key) {
    public void serialize(AbiOutput _out) {
      _out.writeSignedBigInteger(key, 16);
    }
  }

  @AbiGenerated
  public record AesPlainText(BigInteger plaintext) {
    public void serialize(AbiOutput _out) {
      _out.writeSignedBigInteger(plaintext, 16);
    }
  }

  @AbiGenerated
  public record AesContractState(SecretVarId aesKeyVariableId, BlockchainAddress permissionChangeKey) {
    public static AesContractState deserialize(byte[] bytes) {
      return Aes_SDK_15_4_0.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record SecretVarId(int rawId) {
  }

  public static byte[] initialize(BlockchainAddress permissionChangeKey) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(permissionChangeKey);
    });
  }

  public static SecretInputBuilder<AesKey> setAesKey() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<AesKey, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<AesPlainText> encryptMessage() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("41"));
    });
    Function<AesPlainText, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static AesContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new Aes_SDK_15_4_0(client, address).deserializeAesContractState(input);
  }
  
  public static AesContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static AesContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress permissionChangeKey) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new Aes_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
