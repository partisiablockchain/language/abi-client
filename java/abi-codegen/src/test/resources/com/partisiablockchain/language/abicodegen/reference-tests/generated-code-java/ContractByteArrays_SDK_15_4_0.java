// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractByteArrays_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractByteArrays_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    byte[] myArray = _input.readBytes(16);
    byte[] myArray2 = _input.readBytes(5);
    return new ExampleContractState(myArray, myArray2);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyArrayAction deserializeUpdateMyArrayAction(AbiInput _input) {
    byte[] value = _input.readBytes(16);
    return new UpdateMyArrayAction(value);
  }

  private UpdateMyArray2Action deserializeUpdateMyArray2Action(AbiInput _input) {
    byte[] value = _input.readBytes(5);
    return new UpdateMyArray2Action(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte[] myArray = _input.readBytes(16);
    byte[] myArray2 = _input.readBytes(5);
    return new InitializeInit(myArray, myArray2);
  }


  @AbiGenerated
  public record ExampleContractState(byte[] myArray, byte[] myArray2) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractByteArrays_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte[] myArray, byte[] myArray2) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      if (myArray.length != 16) {
        throw new RuntimeException("Length of myArray does not match expected 16");
      }
      _out.writeBytes(myArray);
      if (myArray2.length != 5) {
        throw new RuntimeException("Length of myArray2 does not match expected 5");
      }
      _out.writeBytes(myArray2);
    });
  }

  public static byte[] updateMyArray(byte[] value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("cbe4cbfd0d"));
      if (value.length != 16) {
        throw new RuntimeException("Length of value does not match expected 16");
      }
      _out.writeBytes(value);
    });
  }

  public static byte[] updateMyArray2(byte[] value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ec9da775"));
      if (value.length != 5) {
        throw new RuntimeException("Length of value does not match expected 5");
      }
      _out.writeBytes(value);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractByteArrays_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyArrayAction(byte[] value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArray2Action(byte[] value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractByteArrays_SDK_15_4_0(null, null);
    if (shortname.equals("cbe4cbfd0d")) {
      return contract.deserializeUpdateMyArrayAction(input);
    } else if (shortname.equals("ec9da775")) {
      return contract.deserializeUpdateMyArray2Action(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte[] myArray, byte[] myArray2) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractByteArrays_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
