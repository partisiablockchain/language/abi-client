// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ConditionalEscrowTransfer_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const sender: BlockchainAddress = _input.readAddress();
    const receiver: BlockchainAddress = _input.readAddress();
    const approver: BlockchainAddress = _input.readAddress();
    const tokenType: BlockchainAddress = _input.readAddress();
    const balance: BN = _input.readU64();
    const startTimeMillis: BN = _input.readI64();
    const endTimeMillis: BN = _input.readI64();
    const status: number = _input.readU8();
    return { sender, receiver, approver, tokenType, balance, startTimeMillis, endTimeMillis, status };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeDepositAction(_input: AbiInput): DepositAction {
    const amount: BN = _input.readU64();
    return { discriminant: "deposit", amount };
  }

  public deserializeApproveAction(_input: AbiInput): ApproveAction {
    return { discriminant: "approve",  };
  }

  public deserializeClaimAction(_input: AbiInput): ClaimAction {
    return { discriminant: "claim",  };
  }

  public deserializeDepositCallbackCallback(_input: AbiInput): DepositCallbackCallback {
    const amount: BN = _input.readU64();
    return { discriminant: "deposit_callback", amount };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const sender: BlockchainAddress = _input.readAddress();
    const receiver: BlockchainAddress = _input.readAddress();
    const approver: BlockchainAddress = _input.readAddress();
    const tokenType: BlockchainAddress = _input.readAddress();
    const hoursUntilDeadline: number = _input.readU32();
    return { discriminant: "initialize", sender, receiver, approver, tokenType, hoursUntilDeadline };
  }

}
export interface ContractState {
  sender: BlockchainAddress;
  receiver: BlockchainAddress;
  approver: BlockchainAddress;
  tokenType: BlockchainAddress;
  balance: BN;
  startTimeMillis: BN;
  endTimeMillis: BN;
  status: number;
}

export function initialize(sender: BlockchainAddress, receiver: BlockchainAddress, approver: BlockchainAddress, tokenType: BlockchainAddress, hoursUntilDeadline: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(sender);
    _out.writeAddress(receiver);
    _out.writeAddress(approver);
    _out.writeAddress(tokenType);
    _out.writeU32(hoursUntilDeadline);
  });
}

export function deposit(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeU64(amount);
  });
}

export function approve(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
  });
}

export function claim(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
  });
}

export function depositCallback(amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeU64(amount);
  });
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ConditionalEscrowTransfer_SDK_9_1_2(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ConditionalEscrowTransfer_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | DepositAction
  | ApproveAction
  | ClaimAction;

export interface DepositAction {
  discriminant: "deposit";
  amount: BN;
}
export interface ApproveAction {
  discriminant: "approve";
}
export interface ClaimAction {
  discriminant: "claim";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConditionalEscrowTransfer_SDK_9_1_2(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeDepositAction(input);
  } else if (shortname === "03") {
    return contract.deserializeApproveAction(input);
  } else if (shortname === "04") {
    return contract.deserializeClaimAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | DepositCallbackCallback;

export interface DepositCallbackCallback {
  discriminant: "deposit_callback";
  amount: BN;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConditionalEscrowTransfer_SDK_9_1_2(undefined, undefined);
  if (shortname === "02") {
    return contract.deserializeDepositCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  sender: BlockchainAddress;
  receiver: BlockchainAddress;
  approver: BlockchainAddress;
  tokenType: BlockchainAddress;
  hoursUntilDeadline: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConditionalEscrowTransfer_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

