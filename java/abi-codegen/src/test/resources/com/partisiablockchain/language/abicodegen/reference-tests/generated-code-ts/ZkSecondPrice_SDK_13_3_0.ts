// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkSecondPrice_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeBidderId(_input: AbiInput): BidderId {
    const id: number = _input.readI32();
    return { id };
  }
  public deserializeAuctionResult(_input: AbiInput): AuctionResult {
    const winner: BidderId = this.deserializeBidderId(_input);
    const secondHighestBid: number = _input.readI32();
    return { winner, secondHighestBid };
  }
  public deserializeRegisteredBidder(_input: AbiInput): RegisteredBidder {
    const bidderId: BidderId = this.deserializeBidderId(_input);
    const address: BlockchainAddress = _input.readAddress();
    return { bidderId, address };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const owner: BlockchainAddress = _input.readAddress();
    const registeredBidders_vecLength = _input.readI32();
    const registeredBidders: RegisteredBidder[] = [];
    for (let registeredBidders_i = 0; registeredBidders_i < registeredBidders_vecLength; registeredBidders_i++) {
      const registeredBidders_elem: RegisteredBidder = this.deserializeRegisteredBidder(_input);
      registeredBidders.push(registeredBidders_elem);
    }
    let auctionResult: Option<AuctionResult> = undefined;
    const auctionResult_isSome = _input.readBoolean();
    if (auctionResult_isSome) {
      const auctionResult_option: AuctionResult = this.deserializeAuctionResult(_input);
      auctionResult = auctionResult_option;
    }
    return { owner, registeredBidders, auctionResult };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeRegisterBidderAction(_input: AbiInput): RegisterBidderAction {
    const bidderId: number = _input.readI32();
    const address: BlockchainAddress = _input.readAddress();
    return { discriminant: "register_bidder", bidderId, address };
  }

  public deserializeComputeWinnerAction(_input: AbiInput): ComputeWinnerAction {
    return { discriminant: "compute_winner",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface BidderId {
  id: number;
}

export interface AuctionResult {
  winner: BidderId;
  secondHighestBid: number;
}

export interface RegisteredBidder {
  bidderId: BidderId;
  address: BlockchainAddress;
}

export interface ContractState {
  owner: BlockchainAddress;
  registeredBidders: RegisteredBidder[];
  auctionResult: Option<AuctionResult>;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function registerBidder(bidderId: number, address: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("30", "hex"));
    _out.writeI32(bidderId);
    _out.writeAddress(address);
  });
}

export function computeWinner(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function addBid(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkSecondPrice_SDK_13_3_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkSecondPrice_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | RegisterBidderAction
  | ComputeWinnerAction;

export interface RegisterBidderAction {
  discriminant: "register_bidder";
  bidderId: number;
  address: BlockchainAddress;
}
export interface ComputeWinnerAction {
  discriminant: "compute_winner";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new ZkSecondPrice_SDK_13_3_0(undefined, undefined);
  if (shortname === "30") {
    return contract.deserializeRegisterBidderAction(input);
  } else if (shortname === "01") {
    return contract.deserializeComputeWinnerAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkSecondPrice_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

