// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractMapOfStruct_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeMyStructType(_input: AbiInput): MyStructType {
    const someValue: BN = _input.readU64();
    const someVector_vecLength = _input.readI32();
    const someVector: BN[] = [];
    for (let someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      const someVector_elem: BN = _input.readU64();
      someVector.push(someVector_elem);
    }
    return { someValue, someVector };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myMap_mapLength = _input.readI32();
    const myMap: Map<string, MyStructType> = new Map();
    for (let myMap_i = 0; myMap_i < myMap_mapLength; myMap_i++) {
      const myMap_key: string = _input.readString();
      const myMap_value: MyStructType = this.deserializeMyStructType(_input);
      myMap.set(myMap_key, myMap_value);
    }
    return { myMap };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeInsertInMyMapAction(_input: AbiInput): InsertInMyMapAction {
    const key: string = _input.readString();
    const value: MyStructType = this.deserializeMyStructType(_input);
    return { discriminant: "insert_in_my_map", key, value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface MyStructType {
  someValue: BN;
  someVector: BN[];
}
function serializeMyStructType(_out: AbiOutput, _value: MyStructType): void {
  const { someValue, someVector } = _value;
  _out.writeU64(someValue);
  _out.writeI32(someVector.length);
  for (const someVector_vec of someVector) {
    _out.writeU64(someVector_vec);
  }
}

export interface ExampleContractState {
  myMap: Map<string, MyStructType>;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function insertInMyMap(key: string, value: MyStructType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("fc91b4c102", "hex"));
    _out.writeString(key);
    serializeMyStructType(_out, value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractMapOfStruct_SDK_9_1_2(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractMapOfStruct_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | InsertInMyMapAction;

export interface InsertInMyMapAction {
  discriminant: "insert_in_my_map";
  key: string;
  value: MyStructType;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractMapOfStruct_SDK_9_1_2(undefined, undefined);
  if (shortname === "fc91b4c102") {
    return contract.deserializeInsertInMyMapAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractMapOfStruct_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

