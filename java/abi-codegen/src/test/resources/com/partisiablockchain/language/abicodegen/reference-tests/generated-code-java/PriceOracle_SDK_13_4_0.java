// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class PriceOracle_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public PriceOracle_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Dispute deserializeDispute(AbiInput _input) {
    var claims_vecLength = _input.readI32();
    List<RoundData> claims = new ArrayList<>();
    for (int claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      RoundData claims_elem = deserializeRoundData(_input);
      claims.add(claims_elem);
    }
    BlockchainAddress challenger = _input.readAddress();
    return new Dispute(claims, challenger);
  }
  private RoundData deserializeRoundData(AbiInput _input) {
    byte[] roundId = _input.readBytes(10);
    byte[] answer = _input.readBytes(32);
    byte[] startedAt = _input.readBytes(32);
    byte[] updatedAt = _input.readBytes(32);
    byte[] answeredInRound = _input.readBytes(32);
    return new RoundData(roundId, answer, startedAt, updatedAt, answeredInRound);
  }
  private PriceUpdates deserializePriceUpdates(AbiInput _input) {
    var oraclePriceUpdates_mapLength = _input.readI32();
    Map<BlockchainAddress, RoundData> oraclePriceUpdates = new HashMap<>();
    for (int oraclePriceUpdates_i = 0; oraclePriceUpdates_i < oraclePriceUpdates_mapLength; oraclePriceUpdates_i++) {
      BlockchainAddress oraclePriceUpdates_key = _input.readAddress();
      RoundData oraclePriceUpdates_value = deserializeRoundData(_input);
      oraclePriceUpdates.put(oraclePriceUpdates_key, oraclePriceUpdates_value);
    }
    long disputePeriodEnd = _input.readU64();
    return new PriceUpdates(oraclePriceUpdates, disputePeriodEnd);
  }
  private PriceOracleContractState deserializePriceOracleContractState(AbiInput _input) {
    BlockchainAddress largeOracleContractAddress = _input.readAddress();
    String referenceContractAddress = _input.readString();
    var oracleNodes_vecLength = _input.readI32();
    List<BlockchainAddress> oracleNodes = new ArrayList<>();
    for (int oracleNodes_i = 0; oracleNodes_i < oracleNodes_vecLength; oracleNodes_i++) {
      BlockchainAddress oracleNodes_elem = _input.readAddress();
      oracleNodes.add(oracleNodes_elem);
    }
    var roundPriceUpdates_mapLength = _input.readI32();
    Map<byte[], PriceUpdates> roundPriceUpdates = new HashMap<>();
    for (int roundPriceUpdates_i = 0; roundPriceUpdates_i < roundPriceUpdates_mapLength; roundPriceUpdates_i++) {
      byte[] roundPriceUpdates_key = _input.readBytes(32);
      PriceUpdates roundPriceUpdates_value = deserializePriceUpdates(_input);
      roundPriceUpdates.put(roundPriceUpdates_key, roundPriceUpdates_value);
    }
    byte[] latestPublishedRoundId = _input.readBytes(32);
    String symbol = _input.readString();
    byte[] activeDisputePeriodRoundId = _input.readBytes(32);
    Dispute dispute = deserializeDispute(_input);
    return new PriceOracleContractState(largeOracleContractAddress, referenceContractAddress, oracleNodes, roundPriceUpdates, latestPublishedRoundId, symbol, activeDisputePeriodRoundId, dispute);
  }
  public PriceOracleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializePriceOracleContractState(input);
  }

  private RegisterNodeAction deserializeRegisterNodeAction(AbiInput _input) {
    return new RegisterNodeAction();
  }

  private NotifyPriceUpdateAction deserializeNotifyPriceUpdateAction(AbiInput _input) {
    RoundData roundData = deserializeRoundData(_input);
    return new NotifyPriceUpdateAction(roundData);
  }

  private DisputeResultAction deserializeDisputeResultAction(AbiInput _input) {
    long oracleId = _input.readU64();
    long disputeId = _input.readU64();
    int result = _input.readU32();
    return new DisputeResultAction(oracleId, disputeId, result);
  }

  private DisputeCounterClaimAction deserializeDisputeCounterClaimAction(AbiInput _input) {
    RoundData counterClaim = deserializeRoundData(_input);
    return new DisputeCounterClaimAction(counterClaim);
  }

  private DeregisterNodeAction deserializeDeregisterNodeAction(AbiInput _input) {
    return new DeregisterNodeAction();
  }

  private StartDisputeAction deserializeStartDisputeAction(AbiInput _input) {
    byte[] roundId = _input.readBytes(32);
    return new StartDisputeAction(roundId);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    String symbol = _input.readString();
    BlockchainAddress largeOracleContractAddress = _input.readAddress();
    String referenceContractAddress = _input.readString();
    return new InitInit(symbol, largeOracleContractAddress, referenceContractAddress);
  }


  @AbiGenerated
  public record Dispute(List<RoundData> claims, BlockchainAddress challenger) {
  }

  @AbiGenerated
  public record RoundData(byte[] roundId, byte[] answer, byte[] startedAt, byte[] updatedAt, byte[] answeredInRound) {
    public void serialize(AbiOutput _out) {
      if (roundId.length != 10) {
        throw new RuntimeException("Length of roundId does not match expected 10");
      }
      _out.writeBytes(roundId);
      if (answer.length != 32) {
        throw new RuntimeException("Length of answer does not match expected 32");
      }
      _out.writeBytes(answer);
      if (startedAt.length != 32) {
        throw new RuntimeException("Length of startedAt does not match expected 32");
      }
      _out.writeBytes(startedAt);
      if (updatedAt.length != 32) {
        throw new RuntimeException("Length of updatedAt does not match expected 32");
      }
      _out.writeBytes(updatedAt);
      if (answeredInRound.length != 32) {
        throw new RuntimeException("Length of answeredInRound does not match expected 32");
      }
      _out.writeBytes(answeredInRound);
    }
  }

  @AbiGenerated
  public record PriceUpdates(Map<BlockchainAddress, RoundData> oraclePriceUpdates, long disputePeriodEnd) {
  }

  @AbiGenerated
  public record PriceOracleContractState(BlockchainAddress largeOracleContractAddress, String referenceContractAddress, List<BlockchainAddress> oracleNodes, Map<byte[], PriceUpdates> roundPriceUpdates, byte[] latestPublishedRoundId, String symbol, byte[] activeDisputePeriodRoundId, Dispute dispute) {
    public static PriceOracleContractState deserialize(byte[] bytes) {
      return PriceOracle_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] init(String symbol, BlockchainAddress largeOracleContractAddress, String referenceContractAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(symbol);
      _out.writeAddress(largeOracleContractAddress);
      _out.writeString(referenceContractAddress);
    });
  }

  public static byte[] registerNode() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
    });
  }

  public static byte[] notifyPriceUpdate(RoundData roundData) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      roundData.serialize(_out);
    });
  }

  public static byte[] disputeResult(long oracleId, long disputeId, int result) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeU64(oracleId);
      _out.writeU64(disputeId);
      _out.writeU32(result);
    });
  }

  public static byte[] disputeCounterClaim(RoundData counterClaim) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      counterClaim.serialize(_out);
    });
  }

  public static byte[] deregisterNode() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] startDispute(byte[] roundId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      if (roundId.length != 32) {
        throw new RuntimeException("Length of roundId does not match expected 32");
      }
      _out.writeBytes(roundId);
    });
  }

  public static PriceOracleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new PriceOracle_SDK_13_4_0(client, address).deserializePriceOracleContractState(input);
  }
  
  public static PriceOracleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static PriceOracleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record RegisterNodeAction() implements Action {
  }
  @AbiGenerated
  public record NotifyPriceUpdateAction(RoundData roundData) implements Action {
  }
  @AbiGenerated
  public record DisputeResultAction(long oracleId, long disputeId, int result) implements Action {
  }
  @AbiGenerated
  public record DisputeCounterClaimAction(RoundData counterClaim) implements Action {
  }
  @AbiGenerated
  public record DeregisterNodeAction() implements Action {
  }
  @AbiGenerated
  public record StartDisputeAction(byte[] roundId) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new PriceOracle_SDK_13_4_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeRegisterNodeAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeNotifyPriceUpdateAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeDisputeResultAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeDisputeCounterClaimAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeDeregisterNodeAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeStartDisputeAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(String symbol, BlockchainAddress largeOracleContractAddress, String referenceContractAddress) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new PriceOracle_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
