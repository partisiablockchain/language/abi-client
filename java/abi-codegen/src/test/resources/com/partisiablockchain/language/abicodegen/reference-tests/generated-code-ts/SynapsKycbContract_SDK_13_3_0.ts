// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class SynapsKycbContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeKycRegistration(_input: AbiInput): KycRegistration {
    const name: string = _input.readString();
    const address: string = _input.readString();
    const city: string = _input.readString();
    const country: number = _input.readU32();
    const serverJurisdiction: number = _input.readU32();
    const website: string = _input.readString();
    return { name, address, city, country, serverJurisdiction, website };
  }
  public deserializeBlsPublicKey(_input: AbiInput): BlsPublicKey {
    const publicKeyValue: Buffer = _input.readBytes(96);
    return { publicKeyValue };
  }
  public deserializeBlsSignature(_input: AbiInput): BlsSignature {
    const signatureValue: Buffer = _input.readBytes(48);
    return { signatureValue };
  }
  public deserializeSynapsKycbContractState(_input: AbiInput): SynapsKycbContractState {
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const verificationKey: Buffer = _input.readBytes(33);
    const kycRegistrations_mapLength = _input.readI32();
    const kycRegistrations: Map<BlockchainAddress, KycRegistration> = new Map();
    for (let kycRegistrations_i = 0; kycRegistrations_i < kycRegistrations_mapLength; kycRegistrations_i++) {
      const kycRegistrations_key: BlockchainAddress = _input.readAddress();
      const kycRegistrations_value: KycRegistration = this.deserializeKycRegistration(_input);
      kycRegistrations.set(kycRegistrations_key, kycRegistrations_value);
    }
    return { bpOrchestrationContract, verificationKey, kycRegistrations };
  }
  public deserializeSignature(_input: AbiInput): Signature {
    const recoveryId: number = _input.readU8();
    const valueR: Buffer = _input.readBytes(32);
    const valueS: Buffer = _input.readBytes(32);
    return { recoveryId, valueR, valueS };
  }
  public async getState(): Promise<SynapsKycbContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeSynapsKycbContractState(input);
  }

  public deserializeApproveAction(_input: AbiInput): ApproveAction {
    const jsonString: string = _input.readString();
    const synapsSignature: Signature = this.deserializeSignature(_input);
    const website: string = _input.readString();
    const serverJurisdiction: number = _input.readU32();
    const producerPublicKey: Buffer = _input.readBytes(33);
    const producerBlsKey: BlsPublicKey = this.deserializeBlsPublicKey(_input);
    const popSignature: BlsSignature = this.deserializeBlsSignature(_input);
    return { discriminant: "approve", jsonString, synapsSignature, website, serverJurisdiction, producerPublicKey, producerBlsKey, popSignature };
  }

  public deserializeKycRegisterAction(_input: AbiInput): KycRegisterAction {
    const name: string = _input.readString();
    const address: string = _input.readString();
    const city: string = _input.readString();
    const country: number = _input.readU32();
    const website: string = _input.readString();
    const serverJurisdiction: number = _input.readU32();
    return { discriminant: "kyc_register", name, address, city, country, website, serverJurisdiction };
  }

  public deserializeKycSubmitAction(_input: AbiInput): KycSubmitAction {
    const jsonString: string = _input.readString();
    const synapsSignature: Signature = this.deserializeSignature(_input);
    const producerPublicKey: Buffer = _input.readBytes(33);
    const producerBlsKey: BlsPublicKey = this.deserializeBlsPublicKey(_input);
    const popSignature: BlsSignature = this.deserializeBlsSignature(_input);
    return { discriminant: "kyc_submit", jsonString, synapsSignature, producerPublicKey, producerBlsKey, popSignature };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const verificationKey: Buffer = _input.readBytes(33);
    return { discriminant: "init", bpOrchestrationContract, verificationKey };
  }

}
export interface KycRegistration {
  name: string;
  address: string;
  city: string;
  country: number;
  serverJurisdiction: number;
  website: string;
}

export interface BlsPublicKey {
  publicKeyValue: Buffer;
}
function serializeBlsPublicKey(_out: AbiOutput, _value: BlsPublicKey): void {
  const { publicKeyValue } = _value;
  if (publicKeyValue.length != 96) {
    throw new Error("Length of publicKeyValue does not match expected 96");
  }
  _out.writeBytes(publicKeyValue);
}

export interface BlsSignature {
  signatureValue: Buffer;
}
function serializeBlsSignature(_out: AbiOutput, _value: BlsSignature): void {
  const { signatureValue } = _value;
  if (signatureValue.length != 48) {
    throw new Error("Length of signatureValue does not match expected 48");
  }
  _out.writeBytes(signatureValue);
}

export interface SynapsKycbContractState {
  bpOrchestrationContract: BlockchainAddress;
  verificationKey: Buffer;
  kycRegistrations: Map<BlockchainAddress, KycRegistration>;
}

export interface Signature {
  recoveryId: number;
  valueR: Buffer;
  valueS: Buffer;
}
function serializeSignature(_out: AbiOutput, _value: Signature): void {
  const { recoveryId, valueR, valueS } = _value;
  _out.writeU8(recoveryId);
  if (valueR.length != 32) {
    throw new Error("Length of valueR does not match expected 32");
  }
  _out.writeBytes(valueR);
  if (valueS.length != 32) {
    throw new Error("Length of valueS does not match expected 32");
  }
  _out.writeBytes(valueS);
}

export function init(bpOrchestrationContract: BlockchainAddress, verificationKey: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(bpOrchestrationContract);
    if (verificationKey.length != 33) {
      throw new Error("Length of verificationKey does not match expected 33");
    }
    _out.writeBytes(verificationKey);
  });
}

export function approve(jsonString: string, synapsSignature: Signature, website: string, serverJurisdiction: number, producerPublicKey: Buffer, producerBlsKey: BlsPublicKey, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeString(jsonString);
    serializeSignature(_out, synapsSignature);
    _out.writeString(website);
    _out.writeU32(serverJurisdiction);
    if (producerPublicKey.length != 33) {
      throw new Error("Length of producerPublicKey does not match expected 33");
    }
    _out.writeBytes(producerPublicKey);
    serializeBlsPublicKey(_out, producerBlsKey);
    serializeBlsSignature(_out, popSignature);
  });
}

export function kycRegister(name: string, address: string, city: string, country: number, website: string, serverJurisdiction: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeString(name);
    _out.writeString(address);
    _out.writeString(city);
    _out.writeU32(country);
    _out.writeString(website);
    _out.writeU32(serverJurisdiction);
  });
}

export function kycSubmit(jsonString: string, synapsSignature: Signature, producerPublicKey: Buffer, producerBlsKey: BlsPublicKey, popSignature: BlsSignature): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeString(jsonString);
    serializeSignature(_out, synapsSignature);
    if (producerPublicKey.length != 33) {
      throw new Error("Length of producerPublicKey does not match expected 33");
    }
    _out.writeBytes(producerPublicKey);
    serializeBlsPublicKey(_out, producerBlsKey);
    serializeBlsSignature(_out, popSignature);
  });
}

export function deserializeState(state: StateWithClient): SynapsKycbContractState;
export function deserializeState(bytes: Buffer): SynapsKycbContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): SynapsKycbContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): SynapsKycbContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new SynapsKycbContract_SDK_13_3_0(client, address).deserializeSynapsKycbContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new SynapsKycbContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeSynapsKycbContractState(input);
  }
}

export type Action =
  | ApproveAction
  | KycRegisterAction
  | KycSubmitAction;

export interface ApproveAction {
  discriminant: "approve";
  jsonString: string;
  synapsSignature: Signature;
  website: string;
  serverJurisdiction: number;
  producerPublicKey: Buffer;
  producerBlsKey: BlsPublicKey;
  popSignature: BlsSignature;
}
export interface KycRegisterAction {
  discriminant: "kyc_register";
  name: string;
  address: string;
  city: string;
  country: number;
  website: string;
  serverJurisdiction: number;
}
export interface KycSubmitAction {
  discriminant: "kyc_submit";
  jsonString: string;
  synapsSignature: Signature;
  producerPublicKey: Buffer;
  producerBlsKey: BlsPublicKey;
  popSignature: BlsSignature;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new SynapsKycbContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeApproveAction(input);
  } else if (shortname === "02") {
    return contract.deserializeKycRegisterAction(input);
  } else if (shortname === "03") {
    return contract.deserializeKycSubmitAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  bpOrchestrationContract: BlockchainAddress;
  verificationKey: Buffer;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new SynapsKycbContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

