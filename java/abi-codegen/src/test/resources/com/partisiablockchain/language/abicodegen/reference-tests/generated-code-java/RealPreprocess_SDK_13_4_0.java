// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class RealPreprocess_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public RealPreprocess_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private PendingBatch deserializePendingBatch(AbiInput _input) {
    byte batchType = _input.readU8();
    BlockchainAddress receiver = _input.readAddress();
    var computationEngines_vecLength = _input.readI32();
    List<EngineInformation> computationEngines = new ArrayList<>();
    for (int computationEngines_i = 0; computationEngines_i < computationEngines_vecLength; computationEngines_i++) {
      EngineInformation computationEngines_elem = deserializeEngineInformation(_input);
      computationEngines.add(computationEngines_elem);
    }
    var preProcessEngines_vecLength = _input.readI32();
    List<EngineInformation> preProcessEngines = new ArrayList<>();
    for (int preProcessEngines_i = 0; preProcessEngines_i < preProcessEngines_vecLength; preProcessEngines_i++) {
      EngineInformation preProcessEngines_elem = deserializeEngineInformation(_input);
      preProcessEngines.add(preProcessEngines_elem);
    }
    byte finished = _input.readU8();
    byte accepted = _input.readU8();
    return new PendingBatch(batchType, receiver, computationEngines, preProcessEngines, finished, accepted);
  }
  private EngineInformation deserializeEngineInformation(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    BlockchainPublicKey publicKey = deserializeBlockchainPublicKey(_input);
    return new EngineInformation(identity, publicKey);
  }
  private BlockchainPublicKey deserializeBlockchainPublicKey(AbiInput _input) {
    byte[] ecPoint = _input.readBytes(33);
    return new BlockchainPublicKey(ecPoint);
  }
  private PreProcessState deserializePreProcessState(AbiInput _input) {
    int nextBatchId = _input.readI32();
    var batches_mapLength = _input.readI32();
    Map<Integer, PendingBatch> batches = new HashMap<>();
    for (int batches_i = 0; batches_i < batches_mapLength; batches_i++) {
      int batches_key = _input.readI32();
      PendingBatch batches_value = deserializePendingBatch(_input);
      batches.put(batches_key, batches_value);
    }
    return new PreProcessState(nextBatchId, batches);
  }
  public PreProcessState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializePreProcessState(input);
  }

  private GenerateBatchV2Action deserializeGenerateBatchV2Action(AbiInput _input) {
    byte batchType = _input.readU8();
    byte count = _input.readU8();
    EngineInformation computationEngine0 = deserializeEngineInformation(_input);
    EngineInformation computationEngine1 = deserializeEngineInformation(_input);
    EngineInformation computationEngine2 = deserializeEngineInformation(_input);
    EngineInformation computationEngine3 = deserializeEngineInformation(_input);
    return new GenerateBatchV2Action(batchType, count, computationEngine0, computationEngine1, computationEngine2, computationEngine3);
  }

  private GenerateBatchV3Action deserializeGenerateBatchV3Action(AbiInput _input) {
    byte batchType = _input.readU8();
    byte count = _input.readU8();
    EngineInformation computationEngine0 = deserializeEngineInformation(_input);
    EngineInformation computationEngine1 = deserializeEngineInformation(_input);
    EngineInformation computationEngine2 = deserializeEngineInformation(_input);
    EngineInformation computationEngine3 = deserializeEngineInformation(_input);
    return new GenerateBatchV3Action(batchType, count, computationEngine0, computationEngine1, computationEngine2, computationEngine3);
  }

  private BatchFailedAction deserializeBatchFailedAction(AbiInput _input) {
    int batchId = _input.readI32();
    return new BatchFailedAction(batchId);
  }

  private BatchFinishedAction deserializeBatchFinishedAction(AbiInput _input) {
    int batchId = _input.readI32();
    return new BatchFinishedAction(batchId);
  }

  private RejectBatchAction deserializeRejectBatchAction(AbiInput _input) {
    int batchId = _input.readI32();
    return new RejectBatchAction(batchId);
  }

  private AcceptBatchAction deserializeAcceptBatchAction(AbiInput _input) {
    int batchId = _input.readI32();
    return new AcceptBatchAction(batchId);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    return new InitInit();
  }


  @AbiGenerated
  public record PendingBatch(byte batchType, BlockchainAddress receiver, List<EngineInformation> computationEngines, List<EngineInformation> preProcessEngines, byte finished, byte accepted) {
  }

  @AbiGenerated
  public record EngineInformation(BlockchainAddress identity, BlockchainPublicKey publicKey) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(identity);
      publicKey.serialize(_out);
    }
  }

  @AbiGenerated
  public record BlockchainPublicKey(byte[] ecPoint) {
    public void serialize(AbiOutput _out) {
      if (ecPoint.length != 33) {
        throw new RuntimeException("Length of ecPoint does not match expected 33");
      }
      _out.writeBytes(ecPoint);
    }
  }

  @AbiGenerated
  public record PreProcessState(int nextBatchId, Map<Integer, PendingBatch> batches) {
    public static PreProcessState deserialize(byte[] bytes) {
      return RealPreprocess_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] init() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] generateBatchV2(byte batchType, byte count, EngineInformation computationEngine0, EngineInformation computationEngine1, EngineInformation computationEngine2, EngineInformation computationEngine3) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeU8(batchType);
      _out.writeU8(count);
      computationEngine0.serialize(_out);
      computationEngine1.serialize(_out);
      computationEngine2.serialize(_out);
      computationEngine3.serialize(_out);
    });
  }

  public static byte[] generateBatchV3(byte batchType, byte count, EngineInformation computationEngine0, EngineInformation computationEngine1, EngineInformation computationEngine2, EngineInformation computationEngine3) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeU8(batchType);
      _out.writeU8(count);
      computationEngine0.serialize(_out);
      computationEngine1.serialize(_out);
      computationEngine2.serialize(_out);
      computationEngine3.serialize(_out);
    });
  }

  public static byte[] batchFailed(int batchId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeI32(batchId);
    });
  }

  public static byte[] batchFinished(int batchId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeI32(batchId);
    });
  }

  public static byte[] rejectBatch(int batchId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeI32(batchId);
    });
  }

  public static byte[] acceptBatch(int batchId) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeI32(batchId);
    });
  }

  public static PreProcessState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new RealPreprocess_SDK_13_4_0(client, address).deserializePreProcessState(input);
  }
  
  public static PreProcessState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static PreProcessState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record GenerateBatchV2Action(byte batchType, byte count, EngineInformation computationEngine0, EngineInformation computationEngine1, EngineInformation computationEngine2, EngineInformation computationEngine3) implements Action {
  }
  @AbiGenerated
  public record GenerateBatchV3Action(byte batchType, byte count, EngineInformation computationEngine0, EngineInformation computationEngine1, EngineInformation computationEngine2, EngineInformation computationEngine3) implements Action {
  }
  @AbiGenerated
  public record BatchFailedAction(int batchId) implements Action {
  }
  @AbiGenerated
  public record BatchFinishedAction(int batchId) implements Action {
  }
  @AbiGenerated
  public record RejectBatchAction(int batchId) implements Action {
  }
  @AbiGenerated
  public record AcceptBatchAction(int batchId) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RealPreprocess_SDK_13_4_0(null, null);
    if (shortname.equals("05")) {
      return contract.deserializeGenerateBatchV2Action(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeGenerateBatchV3Action(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeBatchFailedAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeBatchFinishedAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeRejectBatchAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeAcceptBatchAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new RealPreprocess_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
