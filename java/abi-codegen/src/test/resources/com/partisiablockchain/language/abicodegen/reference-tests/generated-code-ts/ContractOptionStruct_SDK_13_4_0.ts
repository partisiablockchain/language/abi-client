// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractOptionStruct_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeMyStruct(_input: AbiInput): MyStruct {
    let option: Option<number> = undefined;
    const option_isSome = _input.readBoolean();
    if (option_isSome) {
      const option_option: number = _input.readU16();
      option = option_option;
    }
    return { option };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    let optionStruct: Option<MyStruct> = undefined;
    const optionStruct_isSome = _input.readBoolean();
    if (optionStruct_isSome) {
      const optionStruct_option: MyStruct = this.deserializeMyStruct(_input);
      optionStruct = optionStruct_option;
    }
    return { optionStruct };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateStructAction(_input: AbiInput): UpdateStructAction {
    let value: Option<MyStruct> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option: MyStruct = this.deserializeMyStruct(_input);
      value = value_option;
    }
    return { discriminant: "update_struct", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    let optionStruct: Option<MyStruct> = undefined;
    const optionStruct_isSome = _input.readBoolean();
    if (optionStruct_isSome) {
      const optionStruct_option: MyStruct = this.deserializeMyStruct(_input);
      optionStruct = optionStruct_option;
    }
    return { discriminant: "initialize", optionStruct };
  }

}
export interface MyStruct {
  option: Option<number>;
}
function serializeMyStruct(_out: AbiOutput, _value: MyStruct): void {
  const { option } = _value;
  _out.writeBoolean(option !== undefined);
  if (option !== undefined) {
    _out.writeU16(option);
  }
}

export interface ExampleContractState {
  optionStruct: Option<MyStruct>;
}

export function initialize(optionStruct: Option<MyStruct>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeBoolean(optionStruct !== undefined);
    if (optionStruct !== undefined) {
      serializeMyStruct(_out, optionStruct);
    }
  });
}

export function updateStruct(value: Option<MyStruct>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("c5aceefc06", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      serializeMyStruct(_out, value);
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractOptionStruct_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractOptionStruct_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateStructAction;

export interface UpdateStructAction {
  discriminant: "update_struct";
  value: Option<MyStruct>;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptionStruct_SDK_13_4_0(undefined, undefined);
  if (shortname === "c5aceefc06") {
    return contract.deserializeUpdateStructAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  optionStruct: Option<MyStruct>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptionStruct_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

