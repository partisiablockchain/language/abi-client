// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class SecretVotingNotification_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVoteResult(_input: AbiInput): VoteResult {
    const votesFor: number = _input.readU32();
    const votesAgainst: number = _input.readU32();
    const passed: boolean = _input.readBoolean();
    return { votesFor, votesAgainst, passed };
  }
  public deserializeNotification(_input: AbiInput): Notification {
    const contract: BlockchainAddress = _input.readAddress();
    const payload: NotificationPayload = this.deserializeNotificationPayload(_input);
    return { contract, payload };
  }
  public deserializeNotificationPayload(_input: AbiInput): NotificationPayload {
    const shortname: number = _input.readU32();
    const bytes_vecLength = _input.readI32();
    const bytes: Buffer = _input.readBytes(bytes_vecLength);
    return { shortname, bytes };
  }
  public deserializeVotingContractState(_input: AbiInput): VotingContractState {
    const owner: BlockchainAddress = _input.readAddress();
    let voteResult: Option<VoteResult> = undefined;
    const voteResult_isSome = _input.readBoolean();
    if (voteResult_isSome) {
      const voteResult_option: VoteResult = this.deserializeVoteResult(_input);
      voteResult = voteResult_option;
    }
    let passNotification: Option<Notification> = undefined;
    const passNotification_isSome = _input.readBoolean();
    if (passNotification_isSome) {
      const passNotification_option: Notification = this.deserializeNotification(_input);
      passNotification = passNotification_option;
    }
    return { owner, voteResult, passNotification };
  }
  public async getState(): Promise<VotingContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeVotingContractState(input);
  }

  public deserializeStartVoteCountingAction(_input: AbiInput): StartVoteCountingAction {
    return { discriminant: "start_vote_counting",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    let passNotification: Option<Notification> = undefined;
    const passNotification_isSome = _input.readBoolean();
    if (passNotification_isSome) {
      const passNotification_option: Notification = this.deserializeNotification(_input);
      passNotification = passNotification_option;
    }
    return { discriminant: "initialize", passNotification };
  }

}
export interface VoteResult {
  votesFor: number;
  votesAgainst: number;
  passed: boolean;
}

export interface Notification {
  contract: BlockchainAddress;
  payload: NotificationPayload;
}
function serializeNotification(_out: AbiOutput, _value: Notification): void {
  const { contract, payload } = _value;
  _out.writeAddress(contract);
  serializeNotificationPayload(_out, payload);
}

export interface NotificationPayload {
  shortname: number;
  bytes: Buffer;
}
function serializeNotificationPayload(_out: AbiOutput, _value: NotificationPayload): void {
  const { shortname, bytes } = _value;
  _out.writeU32(shortname);
  _out.writeI32(bytes.length);
  _out.writeBytes(bytes);
}

export interface VotingContractState {
  owner: BlockchainAddress;
  voteResult: Option<VoteResult>;
  passNotification: Option<Notification>;
}

export function initialize(passNotification: Option<Notification>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeBoolean(passNotification !== undefined);
    if (passNotification !== undefined) {
      serializeNotification(_out, passNotification);
    }
  });
}

export function startVoteCounting(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function addVote(): SecretInputBuilder<boolean> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: boolean): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeBoolean(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): VotingContractState;
export function deserializeState(bytes: Buffer): VotingContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): VotingContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): VotingContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new SecretVotingNotification_SDK_15_4_0(client, address).deserializeVotingContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new SecretVotingNotification_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeVotingContractState(input);
  }
}

export type Action =
  | StartVoteCountingAction;

export interface StartVoteCountingAction {
  discriminant: "start_vote_counting";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new SecretVotingNotification_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeStartVoteCountingAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  passNotification: Option<Notification>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new SecretVotingNotification_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

