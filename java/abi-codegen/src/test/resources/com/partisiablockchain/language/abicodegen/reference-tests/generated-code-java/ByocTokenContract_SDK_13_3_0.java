// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ByocTokenContract_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ByocTokenContract_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private PendingTransfer deserializePendingTransfer(AbiInput _input) {
    BlockchainAddress spender = _input.readAddress();
    BlockchainAddress from = _input.readAddress();
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new PendingTransfer(spender, from, to, amount);
  }
  private ByocTokenContractState deserializeByocTokenContractState(AbiInput _input) {
    String byocSymbol = _input.readString();
    var allowed_mapLength = _input.readI32();
    Map<BlockchainAddress, Map<BlockchainAddress, BigInteger>> allowed = new HashMap<>();
    for (int allowed_i = 0; allowed_i < allowed_mapLength; allowed_i++) {
      BlockchainAddress allowed_key = _input.readAddress();
      var allowed_value_mapLength = _input.readI32();
      Map<BlockchainAddress, BigInteger> allowed_value = new HashMap<>();
      for (int allowed_value_i = 0; allowed_value_i < allowed_value_mapLength; allowed_value_i++) {
        BlockchainAddress allowed_value_key = _input.readAddress();
        BigInteger allowed_value_value = _input.readUnsignedBigInteger(16);
        allowed_value.put(allowed_value_key, allowed_value_value);
      }
      allowed.put(allowed_key, allowed_value);
    }
    var pendingTransfers_mapLength = _input.readI32();
    Map<byte[], PendingTransfer> pendingTransfers = new HashMap<>();
    for (int pendingTransfers_i = 0; pendingTransfers_i < pendingTransfers_mapLength; pendingTransfers_i++) {
      byte[] pendingTransfers_key = _input.readBytes(32);
      PendingTransfer pendingTransfers_value = deserializePendingTransfer(_input);
      pendingTransfers.put(pendingTransfers_key, pendingTransfers_value);
    }
    return new ByocTokenContractState(byocSymbol, allowed, pendingTransfers);
  }
  public ByocTokenContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeByocTokenContractState(input);
  }

  private TransferAction deserializeTransferAction(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new TransferAction(to, amount);
  }

  private TransferFromAction deserializeTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new TransferFromAction(from, to, amount);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    BlockchainAddress spender = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new ApproveAction(spender, amount);
  }

  private AbortAction deserializeAbortAction(AbiInput _input) {
    byte[] transactionHash = _input.readBytes(32);
    return new AbortAction(transactionHash);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    String symbol = _input.readString();
    return new InitializeInit(symbol);
  }


  @AbiGenerated
  public record PendingTransfer(BlockchainAddress spender, BlockchainAddress from, BlockchainAddress to, BigInteger amount) {
  }

  @AbiGenerated
  public record ByocTokenContractState(String byocSymbol, Map<BlockchainAddress, Map<BlockchainAddress, BigInteger>> allowed, Map<byte[], PendingTransfer> pendingTransfers) {
    public static ByocTokenContractState deserialize(byte[] bytes) {
      return ByocTokenContract_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(String symbol) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(symbol);
    });
  }

  public static byte[] transfer(BlockchainAddress to, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] transferFrom(BlockchainAddress from, BlockchainAddress to, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(from);
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] approve(BlockchainAddress spender, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeAddress(spender);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] abort(byte[] transactionHash) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      if (transactionHash.length != 32) {
        throw new RuntimeException("Length of transactionHash does not match expected 32");
      }
      _out.writeBytes(transactionHash);
    });
  }

  public static ByocTokenContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ByocTokenContract_SDK_13_3_0(client, address).deserializeByocTokenContractState(input);
  }
  
  public static ByocTokenContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ByocTokenContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record TransferAction(BlockchainAddress to, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record TransferFromAction(BlockchainAddress from, BlockchainAddress to, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ApproveAction(BlockchainAddress spender, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record AbortAction(byte[] transactionHash) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocTokenContract_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeTransferAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeTransferFromAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeApproveAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeAbortAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(String symbol) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocTokenContract_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
