// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ConfLedger_SDK_13_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeCommitment(_input: AbiInput): Commitment {
    let metaInformation: Option<Buffer> = undefined;
    const metaInformation_isSome = _input.readBoolean();
    if (metaInformation_isSome) {
      const metaInformation_option_vecLength = _input.readI32();
      const metaInformation_option: Buffer = _input.readBytes(metaInformation_option_vecLength);
      metaInformation = metaInformation_option;
    }
    const body_vecLength = _input.readI32();
    const body: Buffer = _input.readBytes(body_vecLength);
    return { metaInformation, body };
  }
  public deserializeConfLedger(_input: AbiInput): ConfLedger {
    const commitments_mapLength = _input.readI32();
    const commitments: Map<BlockchainAddress, Commitment[]> = new Map();
    for (let commitments_i = 0; commitments_i < commitments_mapLength; commitments_i++) {
      const commitments_key: BlockchainAddress = _input.readAddress();
      const commitments_value_vecLength = _input.readI32();
      const commitments_value: Commitment[] = [];
      for (let commitments_value_i = 0; commitments_value_i < commitments_value_vecLength; commitments_value_i++) {
        const commitments_value_elem: Commitment = this.deserializeCommitment(_input);
        commitments_value.push(commitments_value_elem);
      }
      commitments.set(commitments_key, commitments_value);
    }
    const publicKeys_mapLength = _input.readI32();
    const publicKeys: Map<BlockchainAddress, Buffer> = new Map();
    for (let publicKeys_i = 0; publicKeys_i < publicKeys_mapLength; publicKeys_i++) {
      const publicKeys_key: BlockchainAddress = _input.readAddress();
      const publicKeys_value_vecLength = _input.readI32();
      const publicKeys_value: Buffer = _input.readBytes(publicKeys_value_vecLength);
      publicKeys.set(publicKeys_key, publicKeys_value);
    }
    const stash: BN = _input.readU64();
    return { commitments, publicKeys, stash };
  }
  public async getState(): Promise<ConfLedger> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeConfLedger(input);
  }

  public deserializeProvidePublicKeyAction(_input: AbiInput): ProvidePublicKeyAction {
    const vk_vecLength = _input.readI32();
    const vk: Buffer = _input.readBytes(vk_vecLength);
    return { discriminant: "provide_public_key", vk };
  }

  public deserializeMintAction(_input: AbiInput): MintAction {
    const c: Commitment = this.deserializeCommitment(_input);
    const v_vecLength = _input.readI32();
    const v: Buffer = _input.readBytes(v_vecLength);
    const amount: BN = _input.readU64();
    return { discriminant: "mint", c, v, amount };
  }

  public deserializeSplitAction(_input: AbiInput): SplitAction {
    const cSource: Commitment = this.deserializeCommitment(_input);
    const destinationCommitments_vecLength = _input.readI32();
    const destinationCommitments: Commitment[] = [];
    for (let destinationCommitments_i = 0; destinationCommitments_i < destinationCommitments_vecLength; destinationCommitments_i++) {
      const destinationCommitments_elem: Commitment = this.deserializeCommitment(_input);
      destinationCommitments.push(destinationCommitments_elem);
    }
    const sumProof_vecLength = _input.readI32();
    const sumProof: Buffer = _input.readBytes(sumProof_vecLength);
    const overflowProof_vecLength = _input.readI32();
    const overflowProof: Buffer = _input.readBytes(overflowProof_vecLength);
    return { discriminant: "split", cSource, destinationCommitments, sumProof, overflowProof };
  }

  public deserializeCombineAction(_input: AbiInput): CombineAction {
    const cTarget: Commitment = this.deserializeCommitment(_input);
    const sourceCommitments_vecLength = _input.readI32();
    const sourceCommitments: Commitment[] = [];
    for (let sourceCommitments_i = 0; sourceCommitments_i < sourceCommitments_vecLength; sourceCommitments_i++) {
      const sourceCommitments_elem: Commitment = this.deserializeCommitment(_input);
      sourceCommitments.push(sourceCommitments_elem);
    }
    const sumProof_vecLength = _input.readI32();
    const sumProof: Buffer = _input.readBytes(sumProof_vecLength);
    const overflowProof_vecLength = _input.readI32();
    const overflowProof: Buffer = _input.readBytes(overflowProof_vecLength);
    return { discriminant: "combine", cTarget, sourceCommitments, sumProof, overflowProof };
  }

  public deserializeTransferAction(_input: AbiInput): TransferAction {
    const targetAddress: BlockchainAddress = _input.readAddress();
    const cTarget: Commitment = this.deserializeCommitment(_input);
    const cSource: Commitment = this.deserializeCommitment(_input);
    const sameValueProof_vecLength = _input.readI32();
    const sameValueProof: Buffer = _input.readBytes(sameValueProof_vecLength);
    return { discriminant: "transfer", targetAddress, cTarget, cSource, sameValueProof };
  }

  public deserializeWithdrawAction(_input: AbiInput): WithdrawAction {
    const cSource: Commitment = this.deserializeCommitment(_input);
    const cSourceOpeningInformation_vecLength = _input.readI32();
    const cSourceOpeningInformation: Buffer = _input.readBytes(cSourceOpeningInformation_vecLength);
    const tokenProof_vecLength = _input.readI32();
    const tokenProof: Buffer = _input.readBytes(tokenProof_vecLength);
    const amount: BN = _input.readU64();
    const address: BlockchainAddress = _input.readAddress();
    return { discriminant: "withdraw", cSource, cSourceOpeningInformation, tokenProof, amount, address };
  }

  public deserializeReadAction(_input: AbiInput): ReadAction {
    const address: BlockchainAddress = _input.readAddress();
    return { discriminant: "read", address };
  }

  public deserializeTemplateCallbackCallback(_input: AbiInput): TemplateCallbackCallback {
    return { discriminant: "template_callback",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    return { discriminant: "initialize",  };
  }

}
export interface Commitment {
  metaInformation: Option<Buffer>;
  body: Buffer;
}
function serializeCommitment(_out: AbiOutput, _value: Commitment): void {
  const { metaInformation, body } = _value;
  _out.writeBoolean(metaInformation !== undefined);
  if (metaInformation !== undefined) {
    _out.writeI32(metaInformation.length);
    _out.writeBytes(metaInformation);
  }
  _out.writeI32(body.length);
  _out.writeBytes(body);
}

export interface ConfLedger {
  commitments: Map<BlockchainAddress, Commitment[]>;
  publicKeys: Map<BlockchainAddress, Buffer>;
  stash: BN;
}

export function initialize(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function providePublicKey(vk: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeI32(vk.length);
    _out.writeBytes(vk);
  });
}

export function mint(c: Commitment, v: Buffer, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    serializeCommitment(_out, c);
    _out.writeI32(v.length);
    _out.writeBytes(v);
    _out.writeU64(amount);
  });
}

export function split(cSource: Commitment, destinationCommitments: Commitment[], sumProof: Buffer, overflowProof: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    serializeCommitment(_out, cSource);
    _out.writeI32(destinationCommitments.length);
    for (const destinationCommitments_vec of destinationCommitments) {
      serializeCommitment(_out, destinationCommitments_vec);
    }
    _out.writeI32(sumProof.length);
    _out.writeBytes(sumProof);
    _out.writeI32(overflowProof.length);
    _out.writeBytes(overflowProof);
  });
}

export function combine(cTarget: Commitment, sourceCommitments: Commitment[], sumProof: Buffer, overflowProof: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    serializeCommitment(_out, cTarget);
    _out.writeI32(sourceCommitments.length);
    for (const sourceCommitments_vec of sourceCommitments) {
      serializeCommitment(_out, sourceCommitments_vec);
    }
    _out.writeI32(sumProof.length);
    _out.writeBytes(sumProof);
    _out.writeI32(overflowProof.length);
    _out.writeBytes(overflowProof);
  });
}

export function transfer(targetAddress: BlockchainAddress, cTarget: Commitment, cSource: Commitment, sameValueProof: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeAddress(targetAddress);
    serializeCommitment(_out, cTarget);
    serializeCommitment(_out, cSource);
    _out.writeI32(sameValueProof.length);
    _out.writeBytes(sameValueProof);
  });
}

export function withdraw(cSource: Commitment, cSourceOpeningInformation: Buffer, tokenProof: Buffer, amount: BN, address: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    serializeCommitment(_out, cSource);
    _out.writeI32(cSourceOpeningInformation.length);
    _out.writeBytes(cSourceOpeningInformation);
    _out.writeI32(tokenProof.length);
    _out.writeBytes(tokenProof);
    _out.writeU64(amount);
    _out.writeAddress(address);
  });
}

export function read(address: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("07", "hex"));
    _out.writeAddress(address);
  });
}

export function templateCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("10", "hex"));
  });
}

export function deserializeState(state: StateWithClient): ConfLedger;
export function deserializeState(bytes: Buffer): ConfLedger;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ConfLedger;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ConfLedger {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ConfLedger_SDK_13_4_0(client, address).deserializeConfLedger(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ConfLedger_SDK_13_4_0(
      state.client,
      state.address
    ).deserializeConfLedger(input);
  }
}

export type Action =
  | ProvidePublicKeyAction
  | MintAction
  | SplitAction
  | CombineAction
  | TransferAction
  | WithdrawAction
  | ReadAction;

export interface ProvidePublicKeyAction {
  discriminant: "provide_public_key";
  vk: Buffer;
}
export interface MintAction {
  discriminant: "mint";
  c: Commitment;
  v: Buffer;
  amount: BN;
}
export interface SplitAction {
  discriminant: "split";
  cSource: Commitment;
  destinationCommitments: Commitment[];
  sumProof: Buffer;
  overflowProof: Buffer;
}
export interface CombineAction {
  discriminant: "combine";
  cTarget: Commitment;
  sourceCommitments: Commitment[];
  sumProof: Buffer;
  overflowProof: Buffer;
}
export interface TransferAction {
  discriminant: "transfer";
  targetAddress: BlockchainAddress;
  cTarget: Commitment;
  cSource: Commitment;
  sameValueProof: Buffer;
}
export interface WithdrawAction {
  discriminant: "withdraw";
  cSource: Commitment;
  cSourceOpeningInformation: Buffer;
  tokenProof: Buffer;
  amount: BN;
  address: BlockchainAddress;
}
export interface ReadAction {
  discriminant: "read";
  address: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConfLedger_SDK_13_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeProvidePublicKeyAction(input);
  } else if (shortname === "02") {
    return contract.deserializeMintAction(input);
  } else if (shortname === "03") {
    return contract.deserializeSplitAction(input);
  } else if (shortname === "04") {
    return contract.deserializeCombineAction(input);
  } else if (shortname === "05") {
    return contract.deserializeTransferAction(input);
  } else if (shortname === "06") {
    return contract.deserializeWithdrawAction(input);
  } else if (shortname === "07") {
    return contract.deserializeReadAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | TemplateCallbackCallback;

export interface TemplateCallbackCallback {
  discriminant: "template_callback";
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConfLedger_SDK_13_4_0(undefined, undefined);
  if (shortname === "10") {
    return contract.deserializeTemplateCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ConfLedger_SDK_13_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

