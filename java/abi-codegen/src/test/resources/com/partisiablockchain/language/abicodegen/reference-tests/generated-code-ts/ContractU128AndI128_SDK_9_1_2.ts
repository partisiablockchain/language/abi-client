// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractU128AndI128_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myU128: BN = _input.readUnsignedBigInteger(16);
    const myI128: BN = _input.readSignedBigInteger(16);
    return { myU128, myI128 };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyU128Action(_input: AbiInput): UpdateMyU128Action {
    const value: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "update_my_u128", value };
  }

  public deserializeUpdateMyI128Action(_input: AbiInput): UpdateMyI128Action {
    const value: BN = _input.readSignedBigInteger(16);
    return { discriminant: "update_my_i128", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myU128: BN = _input.readUnsignedBigInteger(16);
    const myI128: BN = _input.readSignedBigInteger(16);
    return { discriminant: "initialize", myU128, myI128 };
  }

}
export interface ExampleContractState {
  myU128: BN;
  myI128: BN;
}

export function initialize(myU128: BN, myI128: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeUnsignedBigInteger(myU128, 16);
    _out.writeSignedBigInteger(myI128, 16);
  });
}

export function updateMyU128(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("8fc4c2a607", "hex"));
    _out.writeUnsignedBigInteger(value, 16);
  });
}

export function updateMyI128(value: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("8ce7ebef06", "hex"));
    _out.writeSignedBigInteger(value, 16);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractU128AndI128_SDK_9_1_2(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractU128AndI128_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyU128Action
  | UpdateMyI128Action;

export interface UpdateMyU128Action {
  discriminant: "update_my_u128";
  value: BN;
}
export interface UpdateMyI128Action {
  discriminant: "update_my_i128";
  value: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractU128AndI128_SDK_9_1_2(undefined, undefined);
  if (shortname === "8fc4c2a607") {
    return contract.deserializeUpdateMyU128Action(input);
  } else if (shortname === "8ce7ebef06") {
    return contract.deserializeUpdateMyI128Action(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myU128: BN;
  myI128: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractU128AndI128_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

