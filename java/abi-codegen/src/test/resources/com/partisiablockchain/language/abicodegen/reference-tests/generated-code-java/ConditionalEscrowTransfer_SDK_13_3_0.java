// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ConditionalEscrowTransfer_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ConditionalEscrowTransfer_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    BlockchainAddress sender = _input.readAddress();
    BlockchainAddress receiver = _input.readAddress();
    BlockchainAddress approver = _input.readAddress();
    BlockchainAddress tokenType = _input.readAddress();
    BigInteger balance = _input.readUnsignedBigInteger(16);
    long startTimeMillis = _input.readI64();
    long endTimeMillis = _input.readI64();
    byte status = _input.readU8();
    return new ContractState(sender, receiver, approver, tokenType, balance, startTimeMillis, endTimeMillis, status);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private DepositAction deserializeDepositAction(AbiInput _input) {
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositAction(amount);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    return new ApproveAction();
  }

  private ClaimAction deserializeClaimAction(AbiInput _input) {
    return new ClaimAction();
  }

  private DepositCallbackCallback deserializeDepositCallbackCallback(AbiInput _input) {
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositCallbackCallback(amount);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress sender = _input.readAddress();
    BlockchainAddress receiver = _input.readAddress();
    BlockchainAddress approver = _input.readAddress();
    BlockchainAddress tokenType = _input.readAddress();
    int hoursUntilDeadline = _input.readU32();
    return new InitializeInit(sender, receiver, approver, tokenType, hoursUntilDeadline);
  }


  @AbiGenerated
  public record ContractState(BlockchainAddress sender, BlockchainAddress receiver, BlockchainAddress approver, BlockchainAddress tokenType, BigInteger balance, long startTimeMillis, long endTimeMillis, byte status) {
    public static ContractState deserialize(byte[] bytes) {
      return ConditionalEscrowTransfer_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BlockchainAddress sender, BlockchainAddress receiver, BlockchainAddress approver, BlockchainAddress tokenType, int hoursUntilDeadline) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(sender);
      _out.writeAddress(receiver);
      _out.writeAddress(approver);
      _out.writeAddress(tokenType);
      _out.writeU32(hoursUntilDeadline);
    });
  }

  public static byte[] deposit(BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] approve() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] claim() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] depositCallback(BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ConditionalEscrowTransfer_SDK_13_3_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record DepositAction(BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ApproveAction() implements Action {
  }
  @AbiGenerated
  public record ClaimAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConditionalEscrowTransfer_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeDepositAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeApproveAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeClaimAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record DepositCallbackCallback(BigInteger amount) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConditionalEscrowTransfer_SDK_13_3_0(null, null);
    if (shortname.equals("02")) {
      return contract.deserializeDepositCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress sender, BlockchainAddress receiver, BlockchainAddress approver, BlockchainAddress tokenType, int hoursUntilDeadline) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ConditionalEscrowTransfer_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
