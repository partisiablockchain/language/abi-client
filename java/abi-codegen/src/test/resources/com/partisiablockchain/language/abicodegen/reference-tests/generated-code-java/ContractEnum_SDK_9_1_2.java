// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractEnum_SDK_9_1_2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractEnum_SDK_9_1_2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Vehicle deserializeVehicle(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 2) {
      return deserializeVehicleBicycle(_input);
    } else if (discriminant == 5) {
      return deserializeVehicleCar(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private VehicleBicycle deserializeVehicleBicycle(AbiInput _input) {
    int wheelDiameter = _input.readI32();
    return new VehicleBicycle(wheelDiameter);
  }
  private VehicleCar deserializeVehicleCar(AbiInput _input) {
    byte engineSize = _input.readU8();
    boolean supportsTrailer = _input.readBoolean();
    return new VehicleCar(engineSize, supportsTrailer);
  }
  private EnumContractState deserializeEnumContractState(AbiInput _input) {
    Vehicle myEnum = deserializeVehicle(_input);
    return new EnumContractState(myEnum);
  }
  public EnumContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeEnumContractState(input);
  }

  private UpdateEnumAction deserializeUpdateEnumAction(AbiInput _input) {
    Vehicle val = deserializeVehicle(_input);
    return new UpdateEnumAction(val);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    Vehicle myEnum = deserializeVehicle(_input);
    return new InitializeInit(myEnum);
  }


  @AbiGenerated
  public enum VehicleD {
    BICYCLE(2),
    CAR(5),
    ;
    private final int value;
    VehicleD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Vehicle {
    VehicleD discriminant();
    void serialize(AbiOutput out);
  }

  @AbiGenerated
  public record VehicleBicycle(int wheelDiameter) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.BICYCLE;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
      _out.writeI32(wheelDiameter);
    }
  }

  @AbiGenerated
  public record VehicleCar(byte engineSize, boolean supportsTrailer) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.CAR;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
      _out.writeU8(engineSize);
      _out.writeBoolean(supportsTrailer);
    }
  }

  @AbiGenerated
  public record EnumContractState(Vehicle myEnum) {
    public static EnumContractState deserialize(byte[] bytes) {
      return ContractEnum_SDK_9_1_2.deserializeState(bytes);
    }
  }

  public static byte[] initialize(Vehicle myEnum) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      myEnum.serialize(_out);
    });
  }

  public static byte[] updateEnum(Vehicle val) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0d"));
      val.serialize(_out);
    });
  }

  public static EnumContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractEnum_SDK_9_1_2(client, address).deserializeEnumContractState(input);
  }
  
  public static EnumContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static EnumContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateEnumAction(Vehicle val) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractEnum_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0d")) {
      return contract.deserializeUpdateEnumAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(Vehicle myEnum) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractEnum_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
