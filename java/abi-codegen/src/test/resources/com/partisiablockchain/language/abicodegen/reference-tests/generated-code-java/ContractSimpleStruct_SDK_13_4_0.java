// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractSimpleStruct_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractSimpleStruct_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MyStructType deserializeMyStructType(AbiInput _input) {
    long someValue = _input.readU64();
    var someVector_vecLength = _input.readI32();
    List<Long> someVector = new ArrayList<>();
    for (int someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      long someVector_elem = _input.readU64();
      someVector.add(someVector_elem);
    }
    return new MyStructType(someValue, someVector);
  }
  private NonStateStructType deserializeNonStateStructType(AbiInput _input) {
    long value = _input.readU64();
    return new NonStateStructType(value);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    long myU64 = _input.readU64();
    MyStructType myStruct = deserializeMyStructType(_input);
    return new ExampleContractState(myU64, myStruct);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyU64UsingStructAction deserializeUpdateMyU64UsingStructAction(AbiInput _input) {
    NonStateStructType value = deserializeNonStateStructType(_input);
    return new UpdateMyU64UsingStructAction(value);
  }

  private UpdateMyStructAction deserializeUpdateMyStructAction(AbiInput _input) {
    MyStructType value = deserializeMyStructType(_input);
    return new UpdateMyStructAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    MyStructType myStruct = deserializeMyStructType(_input);
    return new InitializeInit(myStruct);
  }


  @AbiGenerated
  public record MyStructType(long someValue, List<Long> someVector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(someValue);
      _out.writeI32(someVector.size());
      for (long someVector_vec : someVector) {
        _out.writeU64(someVector_vec);
      }
    }
  }

  @AbiGenerated
  public record NonStateStructType(long value) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(value);
    }
  }

  @AbiGenerated
  public record ExampleContractState(long myU64, MyStructType myStruct) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractSimpleStruct_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(MyStructType myStruct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      myStruct.serialize(_out);
    });
  }

  public static byte[] updateMyU64UsingStruct(NonStateStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f8e385b70e"));
      value.serialize(_out);
    });
  }

  public static byte[] updateMyStruct(MyStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f3b68ff40e"));
      value.serialize(_out);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractSimpleStruct_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyU64UsingStructAction(NonStateStructType value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyStructAction(MyStructType value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractSimpleStruct_SDK_13_4_0(null, null);
    if (shortname.equals("f8e385b70e")) {
      return contract.deserializeUpdateMyU64UsingStructAction(input);
    } else if (shortname.equals("f3b68ff40e")) {
      return contract.deserializeUpdateMyStructAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(MyStructType myStruct) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractSimpleStruct_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
