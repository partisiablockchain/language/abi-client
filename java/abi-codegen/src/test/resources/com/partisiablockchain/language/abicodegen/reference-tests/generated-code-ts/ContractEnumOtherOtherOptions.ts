// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractEnumOtherOtherOptions {
}
export enum VehicleD {
  Bicycle = 2,
  Car = 5,
}
export type Vehicle =
  | VehicleBicycle
  | VehicleCar;
function serializeVehicle(out: AbiOutput, value: Vehicle): void {
  if (value.discriminant === VehicleD.Bicycle) {
    return serializeVehicleBicycle(out, value);
  } else if (value.discriminant === VehicleD.Car) {
    return serializeVehicleCar(out, value);
  }
}

export interface VehicleBicycle {
  discriminant: VehicleD.Bicycle;
  wheelDiameter: number;
}
function serializeVehicleBicycle(_out: AbiOutput, _value: VehicleBicycle): void {
  const {wheelDiameter} = _value;
  _out.writeU8(_value.discriminant);
  _out.writeI32(wheelDiameter);
}

export interface VehicleCar {
  discriminant: VehicleD.Car;
  engineSize: number;
  supportsTrailer: boolean;
}
function serializeVehicleCar(_out: AbiOutput, _value: VehicleCar): void {
  const {engineSize, supportsTrailer} = _value;
  _out.writeU8(_value.discriminant);
  _out.writeU8(engineSize);
  _out.writeBoolean(supportsTrailer);
}

export function initialize(myEnum: Vehicle): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeVehicle(_out, myEnum);
  });
}

export function updateEnum(val: Vehicle): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0d", "hex"));
    serializeVehicle(_out, val);
  });
}

