// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractU128AndI128_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractU128AndI128_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    BigInteger myU128 = _input.readUnsignedBigInteger(16);
    BigInteger myI128 = _input.readSignedBigInteger(16);
    return new ExampleContractState(myU128, myI128);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyU128Action deserializeUpdateMyU128Action(AbiInput _input) {
    BigInteger value = _input.readUnsignedBigInteger(16);
    return new UpdateMyU128Action(value);
  }

  private UpdateMyI128Action deserializeUpdateMyI128Action(AbiInput _input) {
    BigInteger value = _input.readSignedBigInteger(16);
    return new UpdateMyI128Action(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BigInteger myU128 = _input.readUnsignedBigInteger(16);
    BigInteger myI128 = _input.readSignedBigInteger(16);
    return new InitializeInit(myU128, myI128);
  }


  @AbiGenerated
  public record ExampleContractState(BigInteger myU128, BigInteger myI128) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractU128AndI128_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BigInteger myU128, BigInteger myI128) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeUnsignedBigInteger(myU128, 16);
      _out.writeSignedBigInteger(myI128, 16);
    });
  }

  public static byte[] updateMyU128(BigInteger value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("8fc4c2a607"));
      _out.writeUnsignedBigInteger(value, 16);
    });
  }

  public static byte[] updateMyI128(BigInteger value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("8ce7ebef06"));
      _out.writeSignedBigInteger(value, 16);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractU128AndI128_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyU128Action(BigInteger value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI128Action(BigInteger value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractU128AndI128_SDK_15_4_0(null, null);
    if (shortname.equals("8fc4c2a607")) {
      return contract.deserializeUpdateMyU128Action(input);
    } else if (shortname.equals("8ce7ebef06")) {
      return contract.deserializeUpdateMyI128Action(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BigInteger myU128, BigInteger myI128) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractU128AndI128_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
