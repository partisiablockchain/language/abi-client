// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ByocOutgoing_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ByocOutgoing_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Dispute deserializeDispute(AbiInput _input) {
    BlockchainAddress challenger = _input.readAddress();
    var claims_vecLength = _input.readI32();
    List<DisputeTransaction> claims = new ArrayList<>();
    for (int claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      DisputeTransaction claims_elem = deserializeDisputeTransaction(_input);
      claims.add(claims_elem);
    }
    int votingResult = _input.readI32();
    return new Dispute(challenger, claims, votingResult);
  }
  private DisputeId deserializeDisputeId(AbiInput _input) {
    long withdrawalNonce = _input.readU64();
    long oracleNonce = _input.readU64();
    return new DisputeId(withdrawalNonce, oracleNonce);
  }
  private WithdrawId deserializeWithdrawId(AbiInput _input) {
    long oracleNonce = _input.readU64();
    long withdrawalNonce = _input.readU64();
    return new WithdrawId(oracleNonce, withdrawalNonce);
  }
  private DisputeTransaction deserializeDisputeTransaction(AbiInput _input) {
    byte[] receiver = _input.readBytes(20);
    byte[] amount = _input.readBytes(32);
    int bitmask = _input.readU32();
    return new DisputeTransaction(receiver, amount, bitmask);
  }
  private Epoch deserializeEpoch(AbiInput _input) {
    long fromWithdrawNonce = _input.readU64();
    long toWithdrawNonce = _input.readU64();
    var oracles_vecLength = _input.readI32();
    List<OracleMember> oracles = new ArrayList<>();
    for (int oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      OracleMember oracles_elem = deserializeOracleMember(_input);
      oracles.add(oracles_elem);
    }
    byte[] merkleTree = _input.readBytes(32);
    return new Epoch(fromWithdrawNonce, toWithdrawNonce, oracles, merkleTree);
  }
  private OracleMember deserializeOracleMember(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    byte[] key = _input.readBytes(33);
    return new OracleMember(identity, key);
  }
  private OracleUpdateRequest deserializeOracleUpdateRequest(AbiInput _input) {
    long oracleNonce = _input.readU64();
    long withdrawNonce = _input.readU64();
    return new OracleUpdateRequest(oracleNonce, withdrawNonce);
  }
  private Withdrawal deserializeWithdrawal(AbiInput _input) {
    byte[] receiver = _input.readBytes(20);
    byte[] amount = _input.readBytes(32);
    var signatures_vecLength = _input.readI32();
    List<Signature> signatures = new ArrayList<>();
    for (int signatures_i = 0; signatures_i < signatures_vecLength; signatures_i++) {
      Signature signatures_elem = deserializeSignature(_input);
      signatures.add(signatures_elem);
    }
    return new Withdrawal(receiver, amount, signatures);
  }
  private ByocOutgoingContractState deserializeByocOutgoingContractState(AbiInput _input) {
    BlockchainAddress largeOracleContract = _input.readAddress();
    byte[] byocContract = _input.readBytes(20);
    String symbol = _input.readString();
    var oracleMembers_vecLength = _input.readI32();
    List<OracleMember> oracleMembers = new ArrayList<>();
    for (int oracleMembers_i = 0; oracleMembers_i < oracleMembers_vecLength; oracleMembers_i++) {
      OracleMember oracleMembers_elem = deserializeOracleMember(_input);
      oracleMembers.add(oracleMembers_elem);
    }
    long withdrawNonce = _input.readI64();
    var withdrawals_mapLength = _input.readI32();
    Map<Long, Withdrawal> withdrawals = new HashMap<>();
    for (int withdrawals_i = 0; withdrawals_i < withdrawals_mapLength; withdrawals_i++) {
      long withdrawals_key = _input.readU64();
      Withdrawal withdrawals_value = deserializeWithdrawal(_input);
      withdrawals.put(withdrawals_key, withdrawals_value);
    }
    byte[] withdrawMinimum = _input.readBytes(32);
    byte[] withdrawalSum = _input.readBytes(32);
    byte[] maximumWithdrawalPerEpoch = _input.readBytes(32);
    long oracleNonce = _input.readI64();
    var epochs_mapLength = _input.readI32();
    Map<Long, Epoch> epochs = new HashMap<>();
    for (int epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      long epochs_key = _input.readU64();
      Epoch epochs_value = deserializeEpoch(_input);
      epochs.put(epochs_key, epochs_value);
    }
    var disputes_mapLength = _input.readI32();
    Map<DisputeId, Dispute> disputes = new HashMap<>();
    for (int disputes_i = 0; disputes_i < disputes_mapLength; disputes_i++) {
      DisputeId disputes_key = deserializeDisputeId(_input);
      Dispute disputes_value = deserializeDispute(_input);
      disputes.put(disputes_key, disputes_value);
    }
    var pendingUpdateRequests_vecLength = _input.readI32();
    List<OracleUpdateRequest> pendingUpdateRequests = new ArrayList<>();
    for (int pendingUpdateRequests_i = 0; pendingUpdateRequests_i < pendingUpdateRequests_vecLength; pendingUpdateRequests_i++) {
      OracleUpdateRequest pendingUpdateRequests_elem = deserializeOracleUpdateRequest(_input);
      pendingUpdateRequests.add(pendingUpdateRequests_elem);
    }
    return new ByocOutgoingContractState(largeOracleContract, byocContract, symbol, oracleMembers, withdrawNonce, withdrawals, withdrawMinimum, withdrawalSum, maximumWithdrawalPerEpoch, oracleNonce, epochs, disputes, pendingUpdateRequests);
  }
  private Signature deserializeSignature(AbiInput _input) {
    byte recoveryId = _input.readU8();
    byte[] valueR = _input.readBytes(32);
    byte[] valueS = _input.readBytes(32);
    return new Signature(recoveryId, valueR, valueS);
  }
  public ByocOutgoingContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeByocOutgoingContractState(input);
  }

  private AddPendingWithdrawalAction deserializeAddPendingWithdrawalAction(AbiInput _input) {
    byte[] receiver = _input.readBytes(20);
    byte[] amount = _input.readBytes(32);
    return new AddPendingWithdrawalAction(receiver, amount);
  }

  private SignPendingWithdrawalAction deserializeSignPendingWithdrawalAction(AbiInput _input) {
    long oracleNonce = _input.readU64();
    long withdrawalNonce = _input.readU64();
    Signature signature = deserializeSignature(_input);
    return new SignPendingWithdrawalAction(oracleNonce, withdrawalNonce, signature);
  }

  private DisputeCreateAction deserializeDisputeCreateAction(AbiInput _input) {
    WithdrawId withdrawId = deserializeWithdrawId(_input);
    DisputeTransaction transaction = deserializeDisputeTransaction(_input);
    return new DisputeCreateAction(withdrawId, transaction);
  }

  private DisputeCounterClaimAction deserializeDisputeCounterClaimAction(AbiInput _input) {
    DisputeTransaction transaction = deserializeDisputeTransaction(_input);
    return new DisputeCounterClaimAction(transaction);
  }

  private DisputeResultAction deserializeDisputeResultAction(AbiInput _input) {
    long withdrawalNonce = _input.readU64();
    int disputeResult = _input.readU32();
    return new DisputeResultAction(withdrawalNonce, disputeResult);
  }

  private UpdateSmallOracleCallback deserializeUpdateSmallOracleCallback(AbiInput _input) {
    var oracleIdentities_vecLength = _input.readI32();
    List<BlockchainAddress> oracleIdentities = new ArrayList<>();
    for (int oracleIdentities_i = 0; oracleIdentities_i < oracleIdentities_vecLength; oracleIdentities_i++) {
      BlockchainAddress oracleIdentities_elem = _input.readAddress();
      oracleIdentities.add(oracleIdentities_elem);
    }
    var oraclePubKeys_vecLength = _input.readI32();
    List<byte[]> oraclePubKeys = new ArrayList<>();
    for (int oraclePubKeys_i = 0; oraclePubKeys_i < oraclePubKeys_vecLength; oraclePubKeys_i++) {
      byte[] oraclePubKeys_elem = _input.readBytes(33);
      oraclePubKeys.add(oraclePubKeys_elem);
    }
    return new UpdateSmallOracleCallback(oracleIdentities, oraclePubKeys);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    BlockchainAddress largeOracleAddress = _input.readAddress();
    byte[] byocContract = _input.readBytes(20);
    String symbol = _input.readString();
    var oracleIdentities_vecLength = _input.readI32();
    List<BlockchainAddress> oracleIdentities = new ArrayList<>();
    for (int oracleIdentities_i = 0; oracleIdentities_i < oracleIdentities_vecLength; oracleIdentities_i++) {
      BlockchainAddress oracleIdentities_elem = _input.readAddress();
      oracleIdentities.add(oracleIdentities_elem);
    }
    var oracleKeys_vecLength = _input.readI32();
    List<byte[]> oracleKeys = new ArrayList<>();
    for (int oracleKeys_i = 0; oracleKeys_i < oracleKeys_vecLength; oracleKeys_i++) {
      byte[] oracleKeys_elem = _input.readBytes(33);
      oracleKeys.add(oracleKeys_elem);
    }
    byte[] withdrawMinimum = _input.readBytes(32);
    byte[] totalWithdrawalMaximum = _input.readBytes(32);
    return new InitInit(largeOracleAddress, byocContract, symbol, oracleIdentities, oracleKeys, withdrawMinimum, totalWithdrawalMaximum);
  }


  @AbiGenerated
  public record Dispute(BlockchainAddress challenger, List<DisputeTransaction> claims, int votingResult) {
  }

  @AbiGenerated
  public record DisputeId(long withdrawalNonce, long oracleNonce) {
  }

  @AbiGenerated
  public record WithdrawId(long oracleNonce, long withdrawalNonce) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(oracleNonce);
      _out.writeU64(withdrawalNonce);
    }
  }

  @AbiGenerated
  public record DisputeTransaction(byte[] receiver, byte[] amount, int bitmask) {
    public void serialize(AbiOutput _out) {
      if (receiver.length != 20) {
        throw new RuntimeException("Length of receiver does not match expected 20");
      }
      _out.writeBytes(receiver);
      if (amount.length != 32) {
        throw new RuntimeException("Length of amount does not match expected 32");
      }
      _out.writeBytes(amount);
      _out.writeU32(bitmask);
    }
  }

  @AbiGenerated
  public record Epoch(long fromWithdrawNonce, long toWithdrawNonce, List<OracleMember> oracles, byte[] merkleTree) {
  }

  @AbiGenerated
  public record OracleMember(BlockchainAddress identity, byte[] key) {
  }

  @AbiGenerated
  public record OracleUpdateRequest(long oracleNonce, long withdrawNonce) {
  }

  @AbiGenerated
  public record Withdrawal(byte[] receiver, byte[] amount, List<Signature> signatures) {
  }

  @AbiGenerated
  public record ByocOutgoingContractState(BlockchainAddress largeOracleContract, byte[] byocContract, String symbol, List<OracleMember> oracleMembers, long withdrawNonce, Map<Long, Withdrawal> withdrawals, byte[] withdrawMinimum, byte[] withdrawalSum, byte[] maximumWithdrawalPerEpoch, long oracleNonce, Map<Long, Epoch> epochs, Map<DisputeId, Dispute> disputes, List<OracleUpdateRequest> pendingUpdateRequests) {
    public static ByocOutgoingContractState deserialize(byte[] bytes) {
      return ByocOutgoing_SDK_13_3_0.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Signature(byte recoveryId, byte[] valueR, byte[] valueS) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(recoveryId);
      if (valueR.length != 32) {
        throw new RuntimeException("Length of valueR does not match expected 32");
      }
      _out.writeBytes(valueR);
      if (valueS.length != 32) {
        throw new RuntimeException("Length of valueS does not match expected 32");
      }
      _out.writeBytes(valueS);
    }
  }

  public static byte[] init(BlockchainAddress largeOracleAddress, byte[] byocContract, String symbol, List<BlockchainAddress> oracleIdentities, List<byte[]> oracleKeys, byte[] withdrawMinimum, byte[] totalWithdrawalMaximum) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(largeOracleAddress);
      if (byocContract.length != 20) {
        throw new RuntimeException("Length of byocContract does not match expected 20");
      }
      _out.writeBytes(byocContract);
      _out.writeString(symbol);
      _out.writeI32(oracleIdentities.size());
      for (BlockchainAddress oracleIdentities_vec : oracleIdentities) {
        _out.writeAddress(oracleIdentities_vec);
      }
      _out.writeI32(oracleKeys.size());
      for (byte[] oracleKeys_vec : oracleKeys) {
        if (oracleKeys_vec.length != 33) {
          throw new RuntimeException("Length of oracleKeys_vec does not match expected 33");
        }
        _out.writeBytes(oracleKeys_vec);
      }
      if (withdrawMinimum.length != 32) {
        throw new RuntimeException("Length of withdrawMinimum does not match expected 32");
      }
      _out.writeBytes(withdrawMinimum);
      if (totalWithdrawalMaximum.length != 32) {
        throw new RuntimeException("Length of totalWithdrawalMaximum does not match expected 32");
      }
      _out.writeBytes(totalWithdrawalMaximum);
    });
  }

  public static byte[] addPendingWithdrawal(byte[] receiver, byte[] amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      if (receiver.length != 20) {
        throw new RuntimeException("Length of receiver does not match expected 20");
      }
      _out.writeBytes(receiver);
      if (amount.length != 32) {
        throw new RuntimeException("Length of amount does not match expected 32");
      }
      _out.writeBytes(amount);
    });
  }

  public static byte[] signPendingWithdrawal(long oracleNonce, long withdrawalNonce, Signature signature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU64(oracleNonce);
      _out.writeU64(withdrawalNonce);
      signature.serialize(_out);
    });
  }

  public static byte[] disputeCreate(WithdrawId withdrawId, DisputeTransaction transaction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      withdrawId.serialize(_out);
      transaction.serialize(_out);
    });
  }

  public static byte[] disputeCounterClaim(DisputeTransaction transaction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      transaction.serialize(_out);
    });
  }

  public static byte[] disputeResult(long withdrawalNonce, int disputeResult) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeU64(withdrawalNonce);
      _out.writeU32(disputeResult);
    });
  }

  public static byte[] updateSmallOracle(List<BlockchainAddress> oracleIdentities, List<byte[]> oraclePubKeys) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeI32(oracleIdentities.size());
      for (BlockchainAddress oracleIdentities_vec : oracleIdentities) {
        _out.writeAddress(oracleIdentities_vec);
      }
      _out.writeI32(oraclePubKeys.size());
      for (byte[] oraclePubKeys_vec : oraclePubKeys) {
        if (oraclePubKeys_vec.length != 33) {
          throw new RuntimeException("Length of oraclePubKeys_vec does not match expected 33");
        }
        _out.writeBytes(oraclePubKeys_vec);
      }
    });
  }

  public static ByocOutgoingContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ByocOutgoing_SDK_13_3_0(client, address).deserializeByocOutgoingContractState(input);
  }
  
  public static ByocOutgoingContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ByocOutgoingContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record AddPendingWithdrawalAction(byte[] receiver, byte[] amount) implements Action {
  }
  @AbiGenerated
  public record SignPendingWithdrawalAction(long oracleNonce, long withdrawalNonce, Signature signature) implements Action {
  }
  @AbiGenerated
  public record DisputeCreateAction(WithdrawId withdrawId, DisputeTransaction transaction) implements Action {
  }
  @AbiGenerated
  public record DisputeCounterClaimAction(DisputeTransaction transaction) implements Action {
  }
  @AbiGenerated
  public record DisputeResultAction(long withdrawalNonce, int disputeResult) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocOutgoing_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeAddPendingWithdrawalAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeSignPendingWithdrawalAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeDisputeCreateAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeDisputeCounterClaimAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeDisputeResultAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record UpdateSmallOracleCallback(List<BlockchainAddress> oracleIdentities, List<byte[]> oraclePubKeys) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocOutgoing_SDK_13_3_0(null, null);
    if (shortname.equals("05")) {
      return contract.deserializeUpdateSmallOracleCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(BlockchainAddress largeOracleAddress, byte[] byocContract, String symbol, List<BlockchainAddress> oracleIdentities, List<byte[]> oracleKeys, byte[] withdrawMinimum, byte[] totalWithdrawalMaximum) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ByocOutgoing_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
