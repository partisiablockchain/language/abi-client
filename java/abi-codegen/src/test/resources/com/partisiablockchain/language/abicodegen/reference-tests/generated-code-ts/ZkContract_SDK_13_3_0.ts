// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkContract_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const ownVariableId: BN = _input.readU64();
    return { ownVariableId };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeSomeActionAction(_input: AbiInput): SomeActionAction {
    return { discriminant: "some_action",  };
  }

  public deserializeToContractDoneAction(_input: AbiInput): ToContractDoneAction {
    return { discriminant: "to_contract_done",  };
  }

  public deserializeStartComputationOneOutputAction(_input: AbiInput): StartComputationOneOutputAction {
    return { discriminant: "start_computation_one_output",  };
  }

  public deserializeStartComputationNoOutputsAction(_input: AbiInput): StartComputationNoOutputsAction {
    return { discriminant: "start_computation_no_outputs",  };
  }

  public deserializeStartComputationTwoOutputsAction(_input: AbiInput): StartComputationTwoOutputsAction {
    return { discriminant: "start_computation_two_outputs",  };
  }

  public deserializeNewComputationAction(_input: AbiInput): NewComputationAction {
    return { discriminant: "new_computation",  };
  }

  public deserializeOutputCompleteDeleteVariableAction(_input: AbiInput): OutputCompleteDeleteVariableAction {
    return { discriminant: "output_complete_delete_variable",  };
  }

  public deserializeContractDoneAction(_input: AbiInput): ContractDoneAction {
    return { discriminant: "contract_done",  };
  }

  public deserializeTransferVariableOneAction(_input: AbiInput): TransferVariableOneAction {
    return { discriminant: "transfer_variable_one",  };
  }

  public deserializeDeleteVariableOneAction(_input: AbiInput): DeleteVariableOneAction {
    return { discriminant: "delete_variable_one",  };
  }

  public deserializeDeletePendingInputOneAction(_input: AbiInput): DeletePendingInputOneAction {
    return { discriminant: "delete_pending_input_one",  };
  }

  public deserializeOpenVariableOneAction(_input: AbiInput): OpenVariableOneAction {
    return { discriminant: "open_variable_one",  };
  }

  public deserializeDeleteVariableThreeAction(_input: AbiInput): DeleteVariableThreeAction {
    return { discriminant: "delete_variable_three",  };
  }

  public deserializeVerifyOnePendingInputAction(_input: AbiInput): VerifyOnePendingInputAction {
    return { discriminant: "verify_one_pending_input",  };
  }

  public deserializeVerifyOneVariableAction(_input: AbiInput): VerifyOneVariableAction {
    return { discriminant: "verify_one_variable",  };
  }

  public deserializeUpdateStateAction(_input: AbiInput): UpdateStateAction {
    const currentState: BN = _input.readU64();
    const nextState: BN = _input.readU64();
    return { discriminant: "update_state", currentState, nextState };
  }

  public deserializeAttestDataAction(_input: AbiInput): AttestDataAction {
    const dataToAttest_vecLength = _input.readI32();
    const dataToAttest: Buffer = _input.readBytes(dataToAttest_vecLength);
    return { discriminant: "attest_data", dataToAttest };
  }

  public deserializeOnCallbackCallback(_input: AbiInput): OnCallbackCallback {
    return { discriminant: "on_callback",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const mode: number = _input.readU8();
    return { discriminant: "initialize", mode };
  }

}
export interface ContractState {
  ownVariableId: BN;
}

export function initialize(mode: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU8(mode);
  });
}

export function someAction(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("00", "hex"));
  });
}

export function toContractDone(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function startComputationOneOutput(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function startComputationNoOutputs(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("0b", "hex"));
  });
}

export function startComputationTwoOutputs(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("0c", "hex"));
  });
}

export function newComputation(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("03", "hex"));
  });
}

export function outputCompleteDeleteVariable(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("04", "hex"));
  });
}

export function contractDone(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function transferVariableOne(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("06", "hex"));
  });
}

export function deleteVariableOne(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("07", "hex"));
  });
}

export function deletePendingInputOne(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("0f", "hex"));
  });
}

export function openVariableOne(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("08", "hex"));
  });
}

export function deleteVariableThree(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("09", "hex"));
  });
}

export function verifyOnePendingInput(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("0a", "hex"));
  });
}

export function verifyOneVariable(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("10", "hex"));
  });
}

export function updateState(currentState: BN, nextState: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("0d", "hex"));
    _out.writeU64(currentState);
    _out.writeU64(nextState);
  });
}

export function attestData(dataToAttest: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("11", "hex"));
    _out.writeI32(dataToAttest.length);
    _out.writeBytes(dataToAttest);
  });
}

export function secretInputBitLength10(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("60", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength1010(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("61", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength1(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("62", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength1001x1(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("63", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength321010(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("64", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength3232(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("65", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength12(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("66", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLength3211(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("67", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputBitLengthOneVariadic(bitlength: number): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("68", "hex"));
    _out.writeU32(bitlength);
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function secretInputSealedSingleBitWithInfo(): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("69", "hex"));
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function onCallback(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkContract_SDK_13_3_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkContract_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | SomeActionAction
  | ToContractDoneAction
  | StartComputationOneOutputAction
  | StartComputationNoOutputsAction
  | StartComputationTwoOutputsAction
  | NewComputationAction
  | OutputCompleteDeleteVariableAction
  | ContractDoneAction
  | TransferVariableOneAction
  | DeleteVariableOneAction
  | DeletePendingInputOneAction
  | OpenVariableOneAction
  | DeleteVariableThreeAction
  | VerifyOnePendingInputAction
  | VerifyOneVariableAction
  | UpdateStateAction
  | AttestDataAction;

export interface SomeActionAction {
  discriminant: "some_action";
}
export interface ToContractDoneAction {
  discriminant: "to_contract_done";
}
export interface StartComputationOneOutputAction {
  discriminant: "start_computation_one_output";
}
export interface StartComputationNoOutputsAction {
  discriminant: "start_computation_no_outputs";
}
export interface StartComputationTwoOutputsAction {
  discriminant: "start_computation_two_outputs";
}
export interface NewComputationAction {
  discriminant: "new_computation";
}
export interface OutputCompleteDeleteVariableAction {
  discriminant: "output_complete_delete_variable";
}
export interface ContractDoneAction {
  discriminant: "contract_done";
}
export interface TransferVariableOneAction {
  discriminant: "transfer_variable_one";
}
export interface DeleteVariableOneAction {
  discriminant: "delete_variable_one";
}
export interface DeletePendingInputOneAction {
  discriminant: "delete_pending_input_one";
}
export interface OpenVariableOneAction {
  discriminant: "open_variable_one";
}
export interface DeleteVariableThreeAction {
  discriminant: "delete_variable_three";
}
export interface VerifyOnePendingInputAction {
  discriminant: "verify_one_pending_input";
}
export interface VerifyOneVariableAction {
  discriminant: "verify_one_variable";
}
export interface UpdateStateAction {
  discriminant: "update_state";
  currentState: BN;
  nextState: BN;
}
export interface AttestDataAction {
  discriminant: "attest_data";
  dataToAttest: Buffer;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new ZkContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeSomeActionAction(input);
  } else if (shortname === "01") {
    return contract.deserializeToContractDoneAction(input);
  } else if (shortname === "02") {
    return contract.deserializeStartComputationOneOutputAction(input);
  } else if (shortname === "0b") {
    return contract.deserializeStartComputationNoOutputsAction(input);
  } else if (shortname === "0c") {
    return contract.deserializeStartComputationTwoOutputsAction(input);
  } else if (shortname === "03") {
    return contract.deserializeNewComputationAction(input);
  } else if (shortname === "04") {
    return contract.deserializeOutputCompleteDeleteVariableAction(input);
  } else if (shortname === "05") {
    return contract.deserializeContractDoneAction(input);
  } else if (shortname === "06") {
    return contract.deserializeTransferVariableOneAction(input);
  } else if (shortname === "07") {
    return contract.deserializeDeleteVariableOneAction(input);
  } else if (shortname === "0f") {
    return contract.deserializeDeletePendingInputOneAction(input);
  } else if (shortname === "08") {
    return contract.deserializeOpenVariableOneAction(input);
  } else if (shortname === "09") {
    return contract.deserializeDeleteVariableThreeAction(input);
  } else if (shortname === "0a") {
    return contract.deserializeVerifyOnePendingInputAction(input);
  } else if (shortname === "10") {
    return contract.deserializeVerifyOneVariableAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeUpdateStateAction(input);
  } else if (shortname === "11") {
    return contract.deserializeAttestDataAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | OnCallbackCallback;

export interface OnCallbackCallback {
  discriminant: "on_callback";
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "40") {
    return contract.deserializeOnCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  mode: number;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkContract_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

