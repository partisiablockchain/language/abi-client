// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class LiquiditySwap_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public LiquiditySwap_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private TokenPool deserializeTokenPool(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    long pool = _input.readU64();
    return new TokenPool(tokenAddress, pool);
  }
  private UserBalance deserializeUserBalance(AbiInput _input) {
    long poolABalance = _input.readU64();
    long poolBBalance = _input.readU64();
    return new UserBalance(poolABalance, poolBBalance);
  }
  private LiquiditySwapContractState deserializeLiquiditySwapContractState(AbiInput _input) {
    BlockchainAddress contractOwner = _input.readAddress();
    TokenPool tokenPoolA = deserializeTokenPool(_input);
    TokenPool tokenPoolB = deserializeTokenPool(_input);
    long swapConstant = _input.readU64();
    var userBalances_mapLength = _input.readI32();
    Map<BlockchainAddress, UserBalance> userBalances = new HashMap<>();
    for (int userBalances_i = 0; userBalances_i < userBalances_mapLength; userBalances_i++) {
      BlockchainAddress userBalances_key = _input.readAddress();
      UserBalance userBalances_value = deserializeUserBalance(_input);
      userBalances.put(userBalances_key, userBalances_value);
    }
    boolean isClosed = _input.readBoolean();
    return new LiquiditySwapContractState(contractOwner, tokenPoolA, tokenPoolB, swapConstant, userBalances, isClosed);
  }
  public LiquiditySwapContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeLiquiditySwapContractState(input);
  }

  private ProvideLiquidityAction deserializeProvideLiquidityAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    long poolSize = _input.readU64();
    return new ProvideLiquidityAction(tokenAddress, poolSize);
  }

  private DepositAction deserializeDepositAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    long amount = _input.readU64();
    return new DepositAction(tokenAddress, amount);
  }

  private SwapAction deserializeSwapAction(AbiInput _input) {
    BlockchainAddress inputTokenAddress = _input.readAddress();
    long amount = _input.readU64();
    return new SwapAction(inputTokenAddress, amount);
  }

  private WithdrawAction deserializeWithdrawAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    long amount = _input.readU64();
    return new WithdrawAction(tokenAddress, amount);
  }

  private ClosePoolsAction deserializeClosePoolsAction(AbiInput _input) {
    return new ClosePoolsAction();
  }

  private ProvideLiquidityCallbackCallback deserializeProvideLiquidityCallbackCallback(AbiInput _input) {
    byte token = _input.readU8();
    long poolSize = _input.readU64();
    return new ProvideLiquidityCallbackCallback(token, poolSize);
  }

  private DepositCallbackCallback deserializeDepositCallbackCallback(AbiInput _input) {
    byte token = _input.readU8();
    long amount = _input.readU64();
    return new DepositCallbackCallback(token, amount);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress tokenAAddress = _input.readAddress();
    BlockchainAddress tokenBAddress = _input.readAddress();
    return new InitializeInit(tokenAAddress, tokenBAddress);
  }


  @AbiGenerated
  public record TokenPool(BlockchainAddress tokenAddress, long pool) {
  }

  @AbiGenerated
  public record UserBalance(long poolABalance, long poolBBalance) {
  }

  @AbiGenerated
  public record LiquiditySwapContractState(BlockchainAddress contractOwner, TokenPool tokenPoolA, TokenPool tokenPoolB, long swapConstant, Map<BlockchainAddress, UserBalance> userBalances, boolean isClosed) {
    public static LiquiditySwapContractState deserialize(byte[] bytes) {
      return LiquiditySwap_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(tokenAAddress);
      _out.writeAddress(tokenBAddress);
    });
  }

  public static byte[] provideLiquidity(BlockchainAddress tokenAddress, long poolSize) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(tokenAddress);
      _out.writeU64(poolSize);
    });
  }

  public static byte[] deposit(BlockchainAddress tokenAddress, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(tokenAddress);
      _out.writeU64(amount);
    });
  }

  public static byte[] swap(BlockchainAddress inputTokenAddress, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(inputTokenAddress);
      _out.writeU64(amount);
    });
  }

  public static byte[] withdraw(BlockchainAddress tokenAddress, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(tokenAddress);
      _out.writeU64(amount);
    });
  }

  public static byte[] closePools() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] provideLiquidityCallback(byte token, long poolSize) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("10"));
      _out.writeU8(token);
      _out.writeU64(poolSize);
    });
  }

  public static byte[] depositCallback(byte token, long amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("20"));
      _out.writeU8(token);
      _out.writeU64(amount);
    });
  }

  public static LiquiditySwapContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new LiquiditySwap_SDK_11_0_0(client, address).deserializeLiquiditySwapContractState(input);
  }
  
  public static LiquiditySwapContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static LiquiditySwapContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record ProvideLiquidityAction(BlockchainAddress tokenAddress, long poolSize) implements Action {
  }
  @AbiGenerated
  public record DepositAction(BlockchainAddress tokenAddress, long amount) implements Action {
  }
  @AbiGenerated
  public record SwapAction(BlockchainAddress inputTokenAddress, long amount) implements Action {
  }
  @AbiGenerated
  public record WithdrawAction(BlockchainAddress tokenAddress, long amount) implements Action {
  }
  @AbiGenerated
  public record ClosePoolsAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_11_0_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeProvideLiquidityAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeDepositAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeSwapAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeWithdrawAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeClosePoolsAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record ProvideLiquidityCallbackCallback(byte token, long poolSize) implements Callback {
  }
  @AbiGenerated
  public record DepositCallbackCallback(byte token, long amount) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_11_0_0(null, null);
    if (shortname.equals("10")) {
      return contract.deserializeProvideLiquidityCallbackCallback(input);
    } else if (shortname.equals("20")) {
      return contract.deserializeDepositCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
