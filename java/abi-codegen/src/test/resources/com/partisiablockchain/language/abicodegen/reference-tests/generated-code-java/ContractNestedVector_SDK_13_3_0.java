// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractNestedVector_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractNestedVector_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    var myVecVec_vecLength = _input.readI32();
    List<List<Long>> myVecVec = new ArrayList<>();
    for (int myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      var myVecVec_elem_vecLength = _input.readI32();
      List<Long> myVecVec_elem = new ArrayList<>();
      for (int myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        long myVecVec_elem_elem = _input.readU64();
        myVecVec_elem.add(myVecVec_elem_elem);
      }
      myVecVec.add(myVecVec_elem);
    }
    return new ExampleContractState(myVecVec);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyVecVecAction deserializeUpdateMyVecVecAction(AbiInput _input) {
    var value_vecLength = _input.readI32();
    List<List<Long>> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      var value_elem_vecLength = _input.readI32();
      List<Long> value_elem = new ArrayList<>();
      for (int value_elem_i = 0; value_elem_i < value_elem_vecLength; value_elem_i++) {
        long value_elem_elem = _input.readU64();
        value_elem.add(value_elem_elem);
      }
      value.add(value_elem);
    }
    return new UpdateMyVecVecAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    var myVecVec_vecLength = _input.readI32();
    List<List<Long>> myVecVec = new ArrayList<>();
    for (int myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      var myVecVec_elem_vecLength = _input.readI32();
      List<Long> myVecVec_elem = new ArrayList<>();
      for (int myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        long myVecVec_elem_elem = _input.readU64();
        myVecVec_elem.add(myVecVec_elem_elem);
      }
      myVecVec.add(myVecVec_elem);
    }
    return new InitializeInit(myVecVec);
  }


  @AbiGenerated
  public record ExampleContractState(List<List<Long>> myVecVec) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractNestedVector_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(List<List<Long>> myVecVec) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(myVecVec.size());
      for (List<Long> myVecVec_vec : myVecVec) {
        _out.writeI32(myVecVec_vec.size());
        for (long myVecVec_vec_vec : myVecVec_vec) {
          _out.writeU64(myVecVec_vec_vec);
        }
      }
    });
  }

  public static byte[] updateMyVecVec(List<List<Long>> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ccc0d6db04"));
      _out.writeI32(value.size());
      for (List<Long> value_vec : value) {
        _out.writeI32(value_vec.size());
        for (long value_vec_vec : value_vec) {
          _out.writeU64(value_vec_vec);
        }
      }
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractNestedVector_SDK_13_3_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyVecVecAction(List<List<Long>> value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNestedVector_SDK_13_3_0(null, null);
    if (shortname.equals("ccc0d6db04")) {
      return contract.deserializeUpdateMyVecVecAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(List<List<Long>> myVecVec) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractNestedVector_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
