// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractRecursiveTypes_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractRecursiveTypes_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private SelfRecursiveType deserializeSelfRecursiveType(AbiInput _input) {
    var child_vecLength = _input.readI32();
    List<SelfRecursiveType> child = new ArrayList<>();
    for (int child_i = 0; child_i < child_vecLength; child_i++) {
      SelfRecursiveType child_elem = deserializeSelfRecursiveType(_input);
      child.add(child_elem);
    }
    return new SelfRecursiveType(child);
  }
  private MutualOne deserializeMutualOne(AbiInput _input) {
    MutualTwo value = deserializeMutualTwo(_input);
    return new MutualOne(value);
  }
  private MutualTwo deserializeMutualTwo(AbiInput _input) {
    var value_vecLength = _input.readI32();
    List<MutualOne> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      MutualOne value_elem = deserializeMutualOne(_input);
      value.add(value_elem);
    }
    return new MutualTwo(value);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    SelfRecursiveType selfRecursiveStruct = deserializeSelfRecursiveType(_input);
    MutualOne mutuallyRecursiveStruct = deserializeMutualOne(_input);
    return new ExampleContractState(selfRecursiveStruct, mutuallyRecursiveStruct);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateSelfRecursiveAction deserializeUpdateSelfRecursiveAction(AbiInput _input) {
    SelfRecursiveType value = deserializeSelfRecursiveType(_input);
    return new UpdateSelfRecursiveAction(value);
  }

  private UpdateMutuallyRecursiveAction deserializeUpdateMutuallyRecursiveAction(AbiInput _input) {
    MutualOne value = deserializeMutualOne(_input);
    return new UpdateMutuallyRecursiveAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    SelfRecursiveType selfRecursiveStruct = deserializeSelfRecursiveType(_input);
    MutualOne mutuallyRecursiveStruct = deserializeMutualOne(_input);
    return new InitializeInit(selfRecursiveStruct, mutuallyRecursiveStruct);
  }


  @AbiGenerated
  public record SelfRecursiveType(List<SelfRecursiveType> child) {
    public void serialize(AbiOutput _out) {
      _out.writeI32(child.size());
      for (SelfRecursiveType child_vec : child) {
        child_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record MutualOne(MutualTwo value) {
    public void serialize(AbiOutput _out) {
      value.serialize(_out);
    }
  }

  @AbiGenerated
  public record MutualTwo(List<MutualOne> value) {
    public void serialize(AbiOutput _out) {
      _out.writeI32(value.size());
      for (MutualOne value_vec : value) {
        value_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record ExampleContractState(SelfRecursiveType selfRecursiveStruct, MutualOne mutuallyRecursiveStruct) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractRecursiveTypes_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(SelfRecursiveType selfRecursiveStruct, MutualOne mutuallyRecursiveStruct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      selfRecursiveStruct.serialize(_out);
      mutuallyRecursiveStruct.serialize(_out);
    });
  }

  public static byte[] updateSelfRecursive(SelfRecursiveType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("dcc3fe0f"));
      value.serialize(_out);
    });
  }

  public static byte[] updateMutuallyRecursive(MutualOne value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("cae4f5e601"));
      value.serialize(_out);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractRecursiveTypes_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateSelfRecursiveAction(SelfRecursiveType value) implements Action {
  }
  @AbiGenerated
  public record UpdateMutuallyRecursiveAction(MutualOne value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractRecursiveTypes_SDK_13_4_0(null, null);
    if (shortname.equals("dcc3fe0f")) {
      return contract.deserializeUpdateSelfRecursiveAction(input);
    } else if (shortname.equals("cae4f5e601")) {
      return contract.deserializeUpdateMutuallyRecursiveAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(SelfRecursiveType selfRecursiveStruct, MutualOne mutuallyRecursiveStruct) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractRecursiveTypes_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
