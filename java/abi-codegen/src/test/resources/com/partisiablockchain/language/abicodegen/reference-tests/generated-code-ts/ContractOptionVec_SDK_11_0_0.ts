// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractOptionVec_SDK_11_0_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    let optionVec: Option<BN[]> = undefined;
    const optionVec_isSome = _input.readBoolean();
    if (optionVec_isSome) {
      const optionVec_option_vecLength = _input.readI32();
      const optionVec_option: BN[] = [];
      for (let optionVec_option_i = 0; optionVec_option_i < optionVec_option_vecLength; optionVec_option_i++) {
        const optionVec_option_elem: BN = _input.readU64();
        optionVec_option.push(optionVec_option_elem);
      }
      optionVec = optionVec_option;
    }
    let optionOption: Option<Option<BN>> = undefined;
    const optionOption_isSome = _input.readBoolean();
    if (optionOption_isSome) {
      let optionOption_option: Option<BN> = undefined;
      const optionOption_option_isSome = _input.readBoolean();
      if (optionOption_option_isSome) {
        const optionOption_option_option: BN = _input.readU64();
        optionOption_option = optionOption_option_option;
      }
      optionOption = optionOption_option;
    }
    return { optionVec, optionOption };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateVecAction(_input: AbiInput): UpdateVecAction {
    let value: Option<BN[]> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      const value_option_vecLength = _input.readI32();
      const value_option: BN[] = [];
      for (let value_option_i = 0; value_option_i < value_option_vecLength; value_option_i++) {
        const value_option_elem: BN = _input.readU64();
        value_option.push(value_option_elem);
      }
      value = value_option;
    }
    return { discriminant: "update_vec", value };
  }

  public deserializeUpdateOptionOptionAction(_input: AbiInput): UpdateOptionOptionAction {
    let value: Option<Option<BN>> = undefined;
    const value_isSome = _input.readBoolean();
    if (value_isSome) {
      let value_option: Option<BN> = undefined;
      const value_option_isSome = _input.readBoolean();
      if (value_option_isSome) {
        const value_option_option: BN = _input.readU64();
        value_option = value_option_option;
      }
      value = value_option;
    }
    return { discriminant: "update_option_option", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    let optionVec: Option<BN[]> = undefined;
    const optionVec_isSome = _input.readBoolean();
    if (optionVec_isSome) {
      const optionVec_option_vecLength = _input.readI32();
      const optionVec_option: BN[] = [];
      for (let optionVec_option_i = 0; optionVec_option_i < optionVec_option_vecLength; optionVec_option_i++) {
        const optionVec_option_elem: BN = _input.readU64();
        optionVec_option.push(optionVec_option_elem);
      }
      optionVec = optionVec_option;
    }
    let optionOption: Option<Option<BN>> = undefined;
    const optionOption_isSome = _input.readBoolean();
    if (optionOption_isSome) {
      let optionOption_option: Option<BN> = undefined;
      const optionOption_option_isSome = _input.readBoolean();
      if (optionOption_option_isSome) {
        const optionOption_option_option: BN = _input.readU64();
        optionOption_option = optionOption_option_option;
      }
      optionOption = optionOption_option;
    }
    return { discriminant: "initialize", optionVec, optionOption };
  }

}
export interface ExampleContractState {
  optionVec: Option<BN[]>;
  optionOption: Option<Option<BN>>;
}

export function initialize(optionVec: Option<BN[]>, optionOption: Option<Option<BN>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeBoolean(optionVec !== undefined);
    if (optionVec !== undefined) {
      _out.writeI32(optionVec.length);
      for (const optionVec_vec of optionVec) {
        _out.writeU64(optionVec_vec);
      }
    }
    _out.writeBoolean(optionOption !== undefined);
    if (optionOption !== undefined) {
      _out.writeBoolean(optionOption !== undefined);
      if (optionOption !== undefined) {
        _out.writeU64(optionOption);
      }
    }
  });
}

export function updateVec(value: Option<BN[]>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("94c19a8e03", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeI32(value.length);
      for (const value_vec of value) {
        _out.writeU64(value_vec);
      }
    }
  });
}

export function updateOptionOption(value: Option<Option<BN>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("b497abf309", "hex"));
    _out.writeBoolean(value !== undefined);
    if (value !== undefined) {
      _out.writeBoolean(value !== undefined);
      if (value !== undefined) {
        _out.writeU64(value);
      }
    }
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractOptionVec_SDK_11_0_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractOptionVec_SDK_11_0_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateVecAction
  | UpdateOptionOptionAction;

export interface UpdateVecAction {
  discriminant: "update_vec";
  value: Option<BN[]>;
}
export interface UpdateOptionOptionAction {
  discriminant: "update_option_option";
  value: Option<Option<BN>>;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptionVec_SDK_11_0_0(undefined, undefined);
  if (shortname === "94c19a8e03") {
    return contract.deserializeUpdateVecAction(input);
  } else if (shortname === "b497abf309") {
    return contract.deserializeUpdateOptionOptionAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  optionVec: Option<BN[]>;
  optionOption: Option<Option<BN>>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractOptionVec_SDK_11_0_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

