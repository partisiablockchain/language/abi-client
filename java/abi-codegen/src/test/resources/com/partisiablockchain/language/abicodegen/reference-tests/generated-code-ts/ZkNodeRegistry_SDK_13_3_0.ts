// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkNodeRegistry_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeCollaborators(_input: AbiInput): Collaborators {
    const bpOrchestration: BlockchainAddress = _input.readAddress();
    const zkContractDeploy: BlockchainAddress = _input.readAddress();
    return { bpOrchestration, zkContractDeploy };
  }
  public deserializeBlockchainPublicKey(_input: AbiInput): BlockchainPublicKey {
    const ecPoint: Buffer = _input.readBytes(33);
    return { ecPoint };
  }
  public deserializeZkNode(_input: AbiInput): ZkNode {
    const identity: BlockchainAddress = _input.readAddress();
    const publicKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    const restEndpoint: string = _input.readString();
    const serverJurisdiction: number = _input.readI32();
    return { identity, publicKey, restEndpoint, serverJurisdiction };
  }
  public deserializeZkNodeTokens(_input: AbiInput): ZkNodeTokens {
    const total: BN = _input.readI64();
    const allocatedTokens_mapLength = _input.readI32();
    const allocatedTokens: Map<BlockchainAddress, BN> = new Map();
    for (let allocatedTokens_i = 0; allocatedTokens_i < allocatedTokens_mapLength; allocatedTokens_i++) {
      const allocatedTokens_key: BlockchainAddress = _input.readAddress();
      const allocatedTokens_value: BN = _input.readI64();
      allocatedTokens.set(allocatedTokens_key, allocatedTokens_value);
    }
    const pendingUnlock_vecLength = _input.readI32();
    const pendingUnlock: Pending[] = [];
    for (let pendingUnlock_i = 0; pendingUnlock_i < pendingUnlock_vecLength; pendingUnlock_i++) {
      const pendingUnlock_elem: Pending = this.deserializePending(_input);
      pendingUnlock.push(pendingUnlock_elem);
    }
    return { total, allocatedTokens, pendingUnlock };
  }
  public deserializePending(_input: AbiInput): Pending {
    const amount: BN = _input.readI64();
    const unlockTime: BN = _input.readI64();
    return { amount, unlockTime };
  }
  public deserializeSemanticVersion(_input: AbiInput): SemanticVersion {
    const major: number = _input.readI32();
    const minor: number = _input.readI32();
    const patch: number = _input.readI32();
    return { major, minor, patch };
  }
  public deserializeZkNodeRegistryState(_input: AbiInput): ZkNodeRegistryState {
    const collaborators: Collaborators = this.deserializeCollaborators(_input);
    const zkNodes_mapLength = _input.readI32();
    const zkNodes: Map<BlockchainAddress, ZkNode> = new Map();
    for (let zkNodes_i = 0; zkNodes_i < zkNodes_mapLength; zkNodes_i++) {
      const zkNodes_key: BlockchainAddress = _input.readAddress();
      const zkNodes_value: ZkNode = this.deserializeZkNode(_input);
      zkNodes.set(zkNodes_key, zkNodes_value);
    }
    const stakedTokens_mapLength = _input.readI32();
    const stakedTokens: Map<BlockchainAddress, ZkNodeTokens> = new Map();
    for (let stakedTokens_i = 0; stakedTokens_i < stakedTokens_mapLength; stakedTokens_i++) {
      const stakedTokens_key: BlockchainAddress = _input.readAddress();
      const stakedTokens_value: ZkNodeTokens = this.deserializeZkNodeTokens(_input);
      stakedTokens.set(stakedTokens_key, stakedTokens_value);
    }
    return { collaborators, zkNodes, stakedTokens };
  }
  public async getState(): Promise<ZkNodeRegistryState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeZkNodeRegistryState(input);
  }

  public deserializeRegisterAsZkNodeAction(_input: AbiInput): RegisterAsZkNodeAction {
    const restEndpoint: string = _input.readString();
    return { discriminant: "register_as_zk_node", restEndpoint };
  }

  public deserializeUpdateRestEndpointAction(_input: AbiInput): UpdateRestEndpointAction {
    const restEndpoint: string = _input.readString();
    return { discriminant: "update_rest_endpoint", restEndpoint };
  }

  public deserializeUpdateServerJurisdictionAction(_input: AbiInput): UpdateServerJurisdictionAction {
    const jurisdiction: number = _input.readI32();
    return { discriminant: "update_server_jurisdiction", jurisdiction };
  }

  public deserializeAssociateTokensAction(_input: AbiInput): AssociateTokensAction {
    const tokenAmount: BN = _input.readI64();
    return { discriminant: "associate_tokens", tokenAmount };
  }

  public deserializeDisassociateTokensAction(_input: AbiInput): DisassociateTokensAction {
    const tokenAmount: BN = _input.readI64();
    return { discriminant: "disassociate_tokens", tokenAmount };
  }

  public deserializeUpdateNodeProtocolVersionAction(_input: AbiInput): UpdateNodeProtocolVersionAction {
    const supportedProtocolVersion: SemanticVersion = this.deserializeSemanticVersion(_input);
    return { discriminant: "update_node_protocol_version", supportedProtocolVersion };
  }

  public deserializeRequestZkComputationNodesV1Action(_input: AbiInput): RequestZkComputationNodesV1Action {
    const zkContractAddress: BlockchainAddress = _input.readAddress();
    const numberOfNodes: number = _input.readI32();
    const totalStakes: BN = _input.readI64();
    return { discriminant: "request_zk_computation_nodes_v1", zkContractAddress, numberOfNodes, totalStakes };
  }

  public deserializeRequestZkComputationNodesV2Action(_input: AbiInput): RequestZkComputationNodesV2Action {
    const zkContractAddress: BlockchainAddress = _input.readAddress();
    const numberOfNodes: number = _input.readI32();
    const totalStakes: BN = _input.readI64();
    const jurisdictions_vecLength = _input.readI32();
    const jurisdictions: number[][] = [];
    for (let jurisdictions_i = 0; jurisdictions_i < jurisdictions_vecLength; jurisdictions_i++) {
      const jurisdictions_elem_vecLength = _input.readI32();
      const jurisdictions_elem: number[] = [];
      for (let jurisdictions_elem_i = 0; jurisdictions_elem_i < jurisdictions_elem_vecLength; jurisdictions_elem_i++) {
        const jurisdictions_elem_elem: number = _input.readI32();
        jurisdictions_elem.push(jurisdictions_elem_elem);
      }
      jurisdictions.push(jurisdictions_elem);
    }
    const requiredProtocolVersion: SemanticVersion = this.deserializeSemanticVersion(_input);
    return { discriminant: "request_zk_computation_nodes_v2", zkContractAddress, numberOfNodes, totalStakes, jurisdictions, requiredProtocolVersion };
  }

  public deserializeNotifyZkComputationDoneAction(_input: AbiInput): NotifyZkComputationDoneAction {
    return { discriminant: "notify_zk_computation_done",  };
  }

  public deserializeUnlockExpiredStakesAction(_input: AbiInput): UnlockExpiredStakesAction {
    return { discriminant: "unlock_expired_stakes",  };
  }

  public deserializeCheckContractStatusAction(_input: AbiInput): CheckContractStatusAction {
    const zkContract: BlockchainAddress = _input.readAddress();
    return { discriminant: "check_contract_status", zkContract };
  }

  public deserializeAssociateTokensCallbackCallback(_input: AbiInput): AssociateTokensCallbackCallback {
    const account: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readI64();
    return { discriminant: "associate_tokens_callback", account, amount };
  }

  public deserializeDisassociateTokensCallbackCallback(_input: AbiInput): DisassociateTokensCallbackCallback {
    const account: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readI64();
    return { discriminant: "disassociate_tokens_callback", account, amount };
  }

  public deserializeGetProducerInfoCallbackCallback(_input: AbiInput): GetProducerInfoCallbackCallback {
    const identity: BlockchainAddress = _input.readAddress();
    const restEndpoint: string = _input.readString();
    return { discriminant: "get_producer_info_callback", identity, restEndpoint };
  }

  public deserializeContractExistsCallbackCallback(_input: AbiInput): ContractExistsCallbackCallback {
    const zkContract: BlockchainAddress = _input.readAddress();
    return { discriminant: "contract_exists_callback", zkContract };
  }

  public deserializeContractComputationDeadlineCallbackCallback(_input: AbiInput): ContractComputationDeadlineCallbackCallback {
    const zkContract: BlockchainAddress = _input.readAddress();
    return { discriminant: "contract_computation_deadline_callback", zkContract };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const collaborators: Collaborators = this.deserializeCollaborators(_input);
    return { discriminant: "init", collaborators };
  }

}
export interface Collaborators {
  bpOrchestration: BlockchainAddress;
  zkContractDeploy: BlockchainAddress;
}
function serializeCollaborators(_out: AbiOutput, _value: Collaborators): void {
  const { bpOrchestration, zkContractDeploy } = _value;
  _out.writeAddress(bpOrchestration);
  _out.writeAddress(zkContractDeploy);
}

export interface BlockchainPublicKey {
  ecPoint: Buffer;
}

export interface ZkNode {
  identity: BlockchainAddress;
  publicKey: BlockchainPublicKey;
  restEndpoint: string;
  serverJurisdiction: number;
}

export interface ZkNodeTokens {
  total: BN;
  allocatedTokens: Map<BlockchainAddress, BN>;
  pendingUnlock: Pending[];
}

export interface Pending {
  amount: BN;
  unlockTime: BN;
}

export interface SemanticVersion {
  major: number;
  minor: number;
  patch: number;
}
function serializeSemanticVersion(_out: AbiOutput, _value: SemanticVersion): void {
  const { major, minor, patch } = _value;
  _out.writeI32(major);
  _out.writeI32(minor);
  _out.writeI32(patch);
}

export interface ZkNodeRegistryState {
  collaborators: Collaborators;
  zkNodes: Map<BlockchainAddress, ZkNode>;
  stakedTokens: Map<BlockchainAddress, ZkNodeTokens>;
}

export function init(collaborators: Collaborators): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeCollaborators(_out, collaborators);
  });
}

export function registerAsZkNode(restEndpoint: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeString(restEndpoint);
  });
}

export function updateRestEndpoint(restEndpoint: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeString(restEndpoint);
  });
}

export function updateServerJurisdiction(jurisdiction: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0d", "hex"));
    _out.writeI32(jurisdiction);
  });
}

export function associateTokens(tokenAmount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0a", "hex"));
    _out.writeI64(tokenAmount);
  });
}

export function disassociateTokens(tokenAmount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0b", "hex"));
    _out.writeI64(tokenAmount);
  });
}

export function updateNodeProtocolVersion(supportedProtocolVersion: SemanticVersion): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("0c", "hex"));
    serializeSemanticVersion(_out, supportedProtocolVersion);
  });
}

export function requestZkComputationNodesV1(zkContractAddress: BlockchainAddress, numberOfNodes: number, totalStakes: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(zkContractAddress);
    _out.writeI32(numberOfNodes);
    _out.writeI64(totalStakes);
  });
}

export function requestZkComputationNodesV2(zkContractAddress: BlockchainAddress, numberOfNodes: number, totalStakes: BN, jurisdictions: number[][], requiredProtocolVersion: SemanticVersion): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("08", "hex"));
    _out.writeAddress(zkContractAddress);
    _out.writeI32(numberOfNodes);
    _out.writeI64(totalStakes);
    _out.writeI32(jurisdictions.length);
    for (const jurisdictions_vec of jurisdictions) {
      _out.writeI32(jurisdictions_vec.length);
      for (const jurisdictions_vec_vec of jurisdictions_vec) {
        _out.writeI32(jurisdictions_vec_vec);
      }
    }
    serializeSemanticVersion(_out, requiredProtocolVersion);
  });
}

export function notifyZkComputationDone(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
  });
}

export function unlockExpiredStakes(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
  });
}

export function checkContractStatus(zkContract: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("07", "hex"));
    _out.writeAddress(zkContract);
  });
}

export function associateTokensCallback(account: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeAddress(account);
    _out.writeI64(amount);
  });
}

export function disassociateTokensCallback(account: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(account);
    _out.writeI64(amount);
  });
}

export function getProducerInfoCallback(identity: BlockchainAddress, restEndpoint: string): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(identity);
    _out.writeString(restEndpoint);
  });
}

export function contractExistsCallback(zkContract: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(zkContract);
  });
}

export function contractComputationDeadlineCallback(zkContract: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(zkContract);
  });
}

export function deserializeState(state: StateWithClient): ZkNodeRegistryState;
export function deserializeState(bytes: Buffer): ZkNodeRegistryState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ZkNodeRegistryState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ZkNodeRegistryState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkNodeRegistry_SDK_13_3_0(client, address).deserializeZkNodeRegistryState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkNodeRegistry_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeZkNodeRegistryState(input);
  }
}

export type Action =
  | RegisterAsZkNodeAction
  | UpdateRestEndpointAction
  | UpdateServerJurisdictionAction
  | AssociateTokensAction
  | DisassociateTokensAction
  | UpdateNodeProtocolVersionAction
  | RequestZkComputationNodesV1Action
  | RequestZkComputationNodesV2Action
  | NotifyZkComputationDoneAction
  | UnlockExpiredStakesAction
  | CheckContractStatusAction;

export interface RegisterAsZkNodeAction {
  discriminant: "register_as_zk_node";
  restEndpoint: string;
}
export interface UpdateRestEndpointAction {
  discriminant: "update_rest_endpoint";
  restEndpoint: string;
}
export interface UpdateServerJurisdictionAction {
  discriminant: "update_server_jurisdiction";
  jurisdiction: number;
}
export interface AssociateTokensAction {
  discriminant: "associate_tokens";
  tokenAmount: BN;
}
export interface DisassociateTokensAction {
  discriminant: "disassociate_tokens";
  tokenAmount: BN;
}
export interface UpdateNodeProtocolVersionAction {
  discriminant: "update_node_protocol_version";
  supportedProtocolVersion: SemanticVersion;
}
export interface RequestZkComputationNodesV1Action {
  discriminant: "request_zk_computation_nodes_v1";
  zkContractAddress: BlockchainAddress;
  numberOfNodes: number;
  totalStakes: BN;
}
export interface RequestZkComputationNodesV2Action {
  discriminant: "request_zk_computation_nodes_v2";
  zkContractAddress: BlockchainAddress;
  numberOfNodes: number;
  totalStakes: BN;
  jurisdictions: number[][];
  requiredProtocolVersion: SemanticVersion;
}
export interface NotifyZkComputationDoneAction {
  discriminant: "notify_zk_computation_done";
}
export interface UnlockExpiredStakesAction {
  discriminant: "unlock_expired_stakes";
}
export interface CheckContractStatusAction {
  discriminant: "check_contract_status";
  zkContract: BlockchainAddress;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkNodeRegistry_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeRegisterAsZkNodeAction(input);
  } else if (shortname === "01") {
    return contract.deserializeUpdateRestEndpointAction(input);
  } else if (shortname === "0d") {
    return contract.deserializeUpdateServerJurisdictionAction(input);
  } else if (shortname === "0a") {
    return contract.deserializeAssociateTokensAction(input);
  } else if (shortname === "0b") {
    return contract.deserializeDisassociateTokensAction(input);
  } else if (shortname === "0c") {
    return contract.deserializeUpdateNodeProtocolVersionAction(input);
  } else if (shortname === "04") {
    return contract.deserializeRequestZkComputationNodesV1Action(input);
  } else if (shortname === "08") {
    return contract.deserializeRequestZkComputationNodesV2Action(input);
  } else if (shortname === "05") {
    return contract.deserializeNotifyZkComputationDoneAction(input);
  } else if (shortname === "06") {
    return contract.deserializeUnlockExpiredStakesAction(input);
  } else if (shortname === "07") {
    return contract.deserializeCheckContractStatusAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | AssociateTokensCallbackCallback
  | DisassociateTokensCallbackCallback
  | GetProducerInfoCallbackCallback
  | ContractExistsCallbackCallback
  | ContractComputationDeadlineCallbackCallback;

export interface AssociateTokensCallbackCallback {
  discriminant: "associate_tokens_callback";
  account: BlockchainAddress;
  amount: BN;
}
export interface DisassociateTokensCallbackCallback {
  discriminant: "disassociate_tokens_callback";
  account: BlockchainAddress;
  amount: BN;
}
export interface GetProducerInfoCallbackCallback {
  discriminant: "get_producer_info_callback";
  identity: BlockchainAddress;
  restEndpoint: string;
}
export interface ContractExistsCallbackCallback {
  discriminant: "contract_exists_callback";
  zkContract: BlockchainAddress;
}
export interface ContractComputationDeadlineCallbackCallback {
  discriminant: "contract_computation_deadline_callback";
  zkContract: BlockchainAddress;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkNodeRegistry_SDK_13_3_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeAssociateTokensCallbackCallback(input);
  } else if (shortname === "01") {
    return contract.deserializeDisassociateTokensCallbackCallback(input);
  } else if (shortname === "02") {
    return contract.deserializeGetProducerInfoCallbackCallback(input);
  } else if (shortname === "03") {
    return contract.deserializeContractExistsCallbackCallback(input);
  } else if (shortname === "04") {
    return contract.deserializeContractComputationDeadlineCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  collaborators: Collaborators;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkNodeRegistry_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

