// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class VotingContract_SDK_9_1_2 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVotingContractState(_input: AbiInput): VotingContractState {
    const proposalId: BN = _input.readU64();
    const mpAddresses_vecLength = _input.readI32();
    const mpAddresses: BlockchainAddress[] = [];
    for (let mpAddresses_i = 0; mpAddresses_i < mpAddresses_vecLength; mpAddresses_i++) {
      const mpAddresses_elem: BlockchainAddress = _input.readAddress();
      mpAddresses.push(mpAddresses_elem);
    }
    const votes_mapLength = _input.readI32();
    const votes: Map<BlockchainAddress, number> = new Map();
    for (let votes_i = 0; votes_i < votes_mapLength; votes_i++) {
      const votes_key: BlockchainAddress = _input.readAddress();
      const votes_value: number = _input.readU8();
      votes.set(votes_key, votes_value);
    }
    const closed: number = _input.readU8();
    return { proposalId, mpAddresses, votes, closed };
  }
  public async getState(): Promise<VotingContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeVotingContractState(input);
  }

  public deserializeVoteAction(_input: AbiInput): VoteAction {
    const vote: number = _input.readU8();
    return { discriminant: "vote", vote };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const proposalId: BN = _input.readU64();
    const mpAddresses_vecLength = _input.readI32();
    const mpAddresses: BlockchainAddress[] = [];
    for (let mpAddresses_i = 0; mpAddresses_i < mpAddresses_vecLength; mpAddresses_i++) {
      const mpAddresses_elem: BlockchainAddress = _input.readAddress();
      mpAddresses.push(mpAddresses_elem);
    }
    return { discriminant: "initialize", proposalId, mpAddresses };
  }

}
export interface VotingContractState {
  proposalId: BN;
  mpAddresses: BlockchainAddress[];
  votes: Map<BlockchainAddress, number>;
  closed: number;
}

export function initialize(proposalId: BN, mpAddresses: BlockchainAddress[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU64(proposalId);
    _out.writeI32(mpAddresses.length);
    for (const mpAddresses_vec of mpAddresses) {
      _out.writeAddress(mpAddresses_vec);
    }
  });
}

export function vote(vote: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("f4889dd90a", "hex"));
    _out.writeU8(vote);
  });
}

export function deserializeState(state: StateWithClient): VotingContractState;
export function deserializeState(bytes: Buffer): VotingContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): VotingContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): VotingContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new VotingContract_SDK_9_1_2(client, address).deserializeVotingContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new VotingContract_SDK_9_1_2(
      state.client,
      state.address
    ).deserializeVotingContractState(input);
  }
}

export type Action =
  | VoteAction;

export interface VoteAction {
  discriminant: "vote";
  vote: number;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new VotingContract_SDK_9_1_2(undefined, undefined);
  if (shortname === "f4889dd90a") {
    return contract.deserializeVoteAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  proposalId: BN;
  mpAddresses: BlockchainAddress[];
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new VotingContract_SDK_9_1_2(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

