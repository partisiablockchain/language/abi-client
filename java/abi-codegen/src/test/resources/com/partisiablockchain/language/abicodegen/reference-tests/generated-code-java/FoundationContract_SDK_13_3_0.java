// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class FoundationContract_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public FoundationContract_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VestingSchedule deserializeVestingSchedule(AbiInput _input) {
    long releaseDuration = _input.readI64();
    long releaseInterval = _input.readI64();
    long tokenGenerationEvent = _input.readI64();
    return new VestingSchedule(releaseDuration, releaseInterval, tokenGenerationEvent);
  }
  private PendingTransfer deserializePendingTransfer(AbiInput _input) {
    BlockchainAddress receiver = _input.readAddress();
    long amount = _input.readI64();
    VestingSchedule vestingSchedule = deserializeVestingSchedule(_input);
    return new PendingTransfer(receiver, amount, vestingSchedule);
  }
  private FoundationContractState deserializeFoundationContractState(AbiInput _input) {
    long nonce = _input.readI64();
    long remainingTokens = _input.readI64();
    var foundationMembers_vecLength = _input.readI32();
    List<byte[]> foundationMembers = new ArrayList<>();
    for (int foundationMembers_i = 0; foundationMembers_i < foundationMembers_vecLength; foundationMembers_i++) {
      byte[] foundationMembers_elem = _input.readBytes(33);
      foundationMembers.add(foundationMembers_elem);
    }
    return new FoundationContractState(nonce, remainingTokens, foundationMembers);
  }
  private Signature deserializeSignature(AbiInput _input) {
    byte recoveryId = _input.readU8();
    byte[] valueR = _input.readBytes(32);
    byte[] valueS = _input.readBytes(32);
    return new Signature(recoveryId, valueR, valueS);
  }
  public FoundationContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeFoundationContractState(input);
  }

  private SignTransferAction deserializeSignTransferAction(AbiInput _input) {
    var pendingTransfers_vecLength = _input.readI32();
    List<PendingTransfer> pendingTransfers = new ArrayList<>();
    for (int pendingTransfers_i = 0; pendingTransfers_i < pendingTransfers_vecLength; pendingTransfers_i++) {
      PendingTransfer pendingTransfers_elem = deserializePendingTransfer(_input);
      pendingTransfers.add(pendingTransfers_elem);
    }
    var signatures_vecLength = _input.readI32();
    List<Signature> signatures = new ArrayList<>();
    for (int signatures_i = 0; signatures_i < signatures_vecLength; signatures_i++) {
      Signature signatures_elem = deserializeSignature(_input);
      signatures.add(signatures_elem);
    }
    return new SignTransferAction(pendingTransfers, signatures);
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    long pooledTokens = _input.readI64();
    var foundationMembers_vecLength = _input.readI32();
    List<byte[]> foundationMembers = new ArrayList<>();
    for (int foundationMembers_i = 0; foundationMembers_i < foundationMembers_vecLength; foundationMembers_i++) {
      byte[] foundationMembers_elem = _input.readBytes(33);
      foundationMembers.add(foundationMembers_elem);
    }
    return new InitInit(pooledTokens, foundationMembers);
  }


  @AbiGenerated
  public record VestingSchedule(long releaseDuration, long releaseInterval, long tokenGenerationEvent) {
    public void serialize(AbiOutput _out) {
      _out.writeI64(releaseDuration);
      _out.writeI64(releaseInterval);
      _out.writeI64(tokenGenerationEvent);
    }
  }

  @AbiGenerated
  public record PendingTransfer(BlockchainAddress receiver, long amount, VestingSchedule vestingSchedule) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(receiver);
      _out.writeI64(amount);
      vestingSchedule.serialize(_out);
    }
  }

  @AbiGenerated
  public record FoundationContractState(long nonce, long remainingTokens, List<byte[]> foundationMembers) {
    public static FoundationContractState deserialize(byte[] bytes) {
      return FoundationContract_SDK_13_3_0.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record Signature(byte recoveryId, byte[] valueR, byte[] valueS) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(recoveryId);
      if (valueR.length != 32) {
        throw new RuntimeException("Length of valueR does not match expected 32");
      }
      _out.writeBytes(valueR);
      if (valueS.length != 32) {
        throw new RuntimeException("Length of valueS does not match expected 32");
      }
      _out.writeBytes(valueS);
    }
  }

  public static byte[] init(long pooledTokens, List<byte[]> foundationMembers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI64(pooledTokens);
      _out.writeI32(foundationMembers.size());
      for (byte[] foundationMembers_vec : foundationMembers) {
        if (foundationMembers_vec.length != 33) {
          throw new RuntimeException("Length of foundationMembers_vec does not match expected 33");
        }
        _out.writeBytes(foundationMembers_vec);
      }
    });
  }

  public static byte[] signTransfer(List<PendingTransfer> pendingTransfers, List<Signature> signatures) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      _out.writeI32(pendingTransfers.size());
      for (PendingTransfer pendingTransfers_vec : pendingTransfers) {
        pendingTransfers_vec.serialize(_out);
      }
      _out.writeI32(signatures.size());
      for (Signature signatures_vec : signatures) {
        signatures_vec.serialize(_out);
      }
    });
  }

  public static FoundationContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new FoundationContract_SDK_13_3_0(client, address).deserializeFoundationContractState(input);
  }
  
  public static FoundationContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static FoundationContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record SignTransferAction(List<PendingTransfer> pendingTransfers, List<Signature> signatures) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new FoundationContract_SDK_13_3_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeSignTransferAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(long pooledTokens, List<byte[]> foundationMembers) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new FoundationContract_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
