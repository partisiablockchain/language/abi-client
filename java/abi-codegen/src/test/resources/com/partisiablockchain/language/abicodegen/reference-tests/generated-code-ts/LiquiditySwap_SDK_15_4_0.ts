// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class LiquiditySwap_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeToken(_input: AbiInput): Token {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeTokenTokenA(_input);
    } else if (discriminant === 1) {
      return this.deserializeTokenTokenB(_input);
    } else if (discriminant === 2) {
      return this.deserializeTokenLiquidityToken(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeTokenTokenA(_input: AbiInput): TokenTokenA {
    return { discriminant: TokenD.TokenA,  };
  }
  public deserializeTokenTokenB(_input: AbiInput): TokenTokenB {
    return { discriminant: TokenD.TokenB,  };
  }
  public deserializeTokenLiquidityToken(_input: AbiInput): TokenLiquidityToken {
    return { discriminant: TokenD.LiquidityToken,  };
  }
  public deserializeTokenBalance(_input: AbiInput): TokenBalance {
    const aTokens: BN = _input.readUnsignedBigInteger(16);
    const bTokens: BN = _input.readUnsignedBigInteger(16);
    const liquidityTokens: BN = _input.readUnsignedBigInteger(16);
    return { aTokens, bTokens, liquidityTokens };
  }
  public deserializeLiquiditySwapContractState(_input: AbiInput): LiquiditySwapContractState {
    const contract: BlockchainAddress = _input.readAddress();
    const tokenAAddress: BlockchainAddress = _input.readAddress();
    const tokenBAddress: BlockchainAddress = _input.readAddress();
    const swapFeePerMille: BN = _input.readUnsignedBigInteger(16);
    const tokenBalances_mapLength = _input.readI32();
    const tokenBalances: Map<BlockchainAddress, TokenBalance> = new Map();
    for (let tokenBalances_i = 0; tokenBalances_i < tokenBalances_mapLength; tokenBalances_i++) {
      const tokenBalances_key: BlockchainAddress = _input.readAddress();
      const tokenBalances_value: TokenBalance = this.deserializeTokenBalance(_input);
      tokenBalances.set(tokenBalances_key, tokenBalances_value);
    }
    return { contract, tokenAAddress, tokenBAddress, swapFeePerMille, tokenBalances };
  }
  public async getState(): Promise<LiquiditySwapContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeLiquiditySwapContractState(input);
  }

  public deserializeDepositAction(_input: AbiInput): DepositAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "deposit", tokenAddress, amount };
  }

  public deserializeSwapAction(_input: AbiInput): SwapAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "swap", tokenAddress, amount };
  }

  public deserializeWithdrawAction(_input: AbiInput): WithdrawAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "withdraw", tokenAddress, amount };
  }

  public deserializeProvideLiquidityAction(_input: AbiInput): ProvideLiquidityAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "provide_liquidity", tokenAddress, amount };
  }

  public deserializeReclaimLiquidityAction(_input: AbiInput): ReclaimLiquidityAction {
    const liquidityTokenAmount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "reclaim_liquidity", liquidityTokenAmount };
  }

  public deserializeProvideInitialLiquidityAction(_input: AbiInput): ProvideInitialLiquidityAction {
    const tokenAAmount: BN = _input.readUnsignedBigInteger(16);
    const tokenBAmount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "provide_initial_liquidity", tokenAAmount, tokenBAmount };
  }

  public deserializeDepositCallbackCallback(_input: AbiInput): DepositCallbackCallback {
    const token: Token = this.deserializeToken(_input);
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "deposit_callback", token, amount };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const tokenAAddress: BlockchainAddress = _input.readAddress();
    const tokenBAddress: BlockchainAddress = _input.readAddress();
    const swapFeePerMille: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "initialize", tokenAAddress, tokenBAddress, swapFeePerMille };
  }

}
export enum TokenD {
  TokenA = 0,
  TokenB = 1,
  LiquidityToken = 2,
}
export type Token =
  | TokenTokenA
  | TokenTokenB
  | TokenLiquidityToken;
function serializeToken(out: AbiOutput, value: Token): void {
  if (value.discriminant === TokenD.TokenA) {
    return serializeTokenTokenA(out, value);
  } else if (value.discriminant === TokenD.TokenB) {
    return serializeTokenTokenB(out, value);
  } else if (value.discriminant === TokenD.LiquidityToken) {
    return serializeTokenLiquidityToken(out, value);
  }
}

export interface TokenTokenA {
  discriminant: TokenD.TokenA;
}
function serializeTokenTokenA(_out: AbiOutput, _value: TokenTokenA): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface TokenTokenB {
  discriminant: TokenD.TokenB;
}
function serializeTokenTokenB(_out: AbiOutput, _value: TokenTokenB): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface TokenLiquidityToken {
  discriminant: TokenD.LiquidityToken;
}
function serializeTokenLiquidityToken(_out: AbiOutput, _value: TokenLiquidityToken): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface TokenBalance {
  aTokens: BN;
  bTokens: BN;
  liquidityTokens: BN;
}

export interface LiquiditySwapContractState {
  contract: BlockchainAddress;
  tokenAAddress: BlockchainAddress;
  tokenBAddress: BlockchainAddress;
  swapFeePerMille: BN;
  tokenBalances: Map<BlockchainAddress, TokenBalance>;
}

export function initialize(tokenAAddress: BlockchainAddress, tokenBAddress: BlockchainAddress, swapFeePerMille: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(tokenAAddress);
    _out.writeAddress(tokenBAddress);
    _out.writeUnsignedBigInteger(swapFeePerMille, 16);
  });
}

export function deposit(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function swap(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function withdraw(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function provideLiquidity(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function reclaimLiquidity(liquidityTokenAmount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeUnsignedBigInteger(liquidityTokenAmount, 16);
  });
}

export function provideInitialLiquidity(tokenAAmount: BN, tokenBAmount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeUnsignedBigInteger(tokenAAmount, 16);
    _out.writeUnsignedBigInteger(tokenBAmount, 16);
  });
}

export function depositCallback(token: Token, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("10", "hex"));
    serializeToken(_out, token);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function deserializeState(state: StateWithClient): LiquiditySwapContractState;
export function deserializeState(bytes: Buffer): LiquiditySwapContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): LiquiditySwapContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): LiquiditySwapContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new LiquiditySwap_SDK_15_4_0(client, address).deserializeLiquiditySwapContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new LiquiditySwap_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeLiquiditySwapContractState(input);
  }
}

export type Action =
  | DepositAction
  | SwapAction
  | WithdrawAction
  | ProvideLiquidityAction
  | ReclaimLiquidityAction
  | ProvideInitialLiquidityAction;

export interface DepositAction {
  discriminant: "deposit";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface SwapAction {
  discriminant: "swap";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface WithdrawAction {
  discriminant: "withdraw";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface ProvideLiquidityAction {
  discriminant: "provide_liquidity";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface ReclaimLiquidityAction {
  discriminant: "reclaim_liquidity";
  liquidityTokenAmount: BN;
}
export interface ProvideInitialLiquidityAction {
  discriminant: "provide_initial_liquidity";
  tokenAAmount: BN;
  tokenBAmount: BN;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeDepositAction(input);
  } else if (shortname === "02") {
    return contract.deserializeSwapAction(input);
  } else if (shortname === "03") {
    return contract.deserializeWithdrawAction(input);
  } else if (shortname === "04") {
    return contract.deserializeProvideLiquidityAction(input);
  } else if (shortname === "05") {
    return contract.deserializeReclaimLiquidityAction(input);
  } else if (shortname === "06") {
    return contract.deserializeProvideInitialLiquidityAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | DepositCallbackCallback;

export interface DepositCallbackCallback {
  discriminant: "deposit_callback";
  token: Token;
  amount: BN;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "10") {
    return contract.deserializeDepositCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  tokenAAddress: BlockchainAddress;
  tokenBAddress: BlockchainAddress;
  swapFeePerMille: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new LiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

