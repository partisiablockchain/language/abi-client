// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractStructOfStruct_SDK_10_1_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractStructOfStruct_SDK_10_1_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MyStructType deserializeMyStructType(AbiInput _input) {
    long someValue = _input.readU64();
    var someVector_vecLength = _input.readI32();
    List<Long> someVector = new ArrayList<>();
    for (int someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      long someVector_elem = _input.readU64();
      someVector.add(someVector_elem);
    }
    return new MyStructType(someValue, someVector);
  }
  private MyOtherStructType deserializeMyOtherStructType(AbiInput _input) {
    long value = _input.readU64();
    var vector_vecLength = _input.readI32();
    List<MyStructType> vector = new ArrayList<>();
    for (int vector_i = 0; vector_i < vector_vecLength; vector_i++) {
      MyStructType vector_elem = deserializeMyStructType(_input);
      vector.add(vector_elem);
    }
    return new MyOtherStructType(value, vector);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    MyOtherStructType myOtherStruct = deserializeMyOtherStructType(_input);
    return new ExampleContractState(myOtherStruct);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyOtherStructAction deserializeUpdateMyOtherStructAction(AbiInput _input) {
    MyOtherStructType value = deserializeMyOtherStructType(_input);
    return new UpdateMyOtherStructAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    MyOtherStructType myOtherStruct = deserializeMyOtherStructType(_input);
    return new InitializeInit(myOtherStruct);
  }


  @AbiGenerated
  public record MyStructType(long someValue, List<Long> someVector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(someValue);
      _out.writeI32(someVector.size());
      for (long someVector_vec : someVector) {
        _out.writeU64(someVector_vec);
      }
    }
  }

  @AbiGenerated
  public record MyOtherStructType(long value, List<MyStructType> vector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(value);
      _out.writeI32(vector.size());
      for (MyStructType vector_vec : vector) {
        vector_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record ExampleContractState(MyOtherStructType myOtherStruct) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractStructOfStruct_SDK_10_1_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(MyOtherStructType myOtherStruct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      myOtherStruct.serialize(_out);
    });
  }

  public static byte[] updateMyOtherStruct(MyOtherStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("85f292be0b"));
      value.serialize(_out);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractStructOfStruct_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyOtherStructAction(MyOtherStructType value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractStructOfStruct_SDK_10_1_0(null, null);
    if (shortname.equals("85f292be0b")) {
      return contract.deserializeUpdateMyOtherStructAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(MyOtherStructType myOtherStruct) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractStructOfStruct_SDK_10_1_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
