// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class TokenV2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public TokenV2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private AllowedAddress deserializeAllowedAddress(AbiInput _input) {
    BlockchainAddress owner = _input.readAddress();
    BlockchainAddress spender = _input.readAddress();
    return new AllowedAddress(owner, spender);
  }
  private Transfer deserializeTransfer(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new Transfer(to, amount);
  }
  private TokenState deserializeTokenState(AbiInput _input) {
    String name = _input.readString();
    byte decimals = _input.readU8();
    String symbol = _input.readString();
    BlockchainAddress owner = _input.readAddress();
    BigInteger totalSupply = _input.readUnsignedBigInteger(16);
    var balances_treeId = _input.readI32();
    AvlTreeMap<BlockchainAddress, BigInteger> balances = new AvlTreeMap<>(
      balances_treeId,
      this._client,
      this._address,
      (balances_key) -> AbiByteOutput.serializeLittleEndian(balances_out -> {
        balances_out.writeAddress(balances_key);
      }),
      (balances_bytes) -> {
        var balances_input = AbiByteInput.createLittleEndian(balances_bytes);
        BlockchainAddress balances_key = balances_input.readAddress();
        return balances_key;
      },
      (balances_bytes) -> {
        var balances_input = AbiByteInput.createLittleEndian(balances_bytes);
        BigInteger balances_value = balances_input.readUnsignedBigInteger(16);
        return balances_value;
      }
    );
    var allowed_treeId = _input.readI32();
    AvlTreeMap<AllowedAddress, BigInteger> allowed = new AvlTreeMap<>(
      allowed_treeId,
      this._client,
      this._address,
      (allowed_key) -> AbiByteOutput.serializeLittleEndian(allowed_out -> {
        allowed_key.serialize(allowed_out);
      }),
      (allowed_bytes) -> {
        var allowed_input = AbiByteInput.createLittleEndian(allowed_bytes);
        AllowedAddress allowed_key = deserializeAllowedAddress(allowed_input);
        return allowed_key;
      },
      (allowed_bytes) -> {
        var allowed_input = AbiByteInput.createLittleEndian(allowed_bytes);
        BigInteger allowed_value = allowed_input.readUnsignedBigInteger(16);
        return allowed_value;
      }
    );
    return new TokenState(name, decimals, symbol, owner, totalSupply, balances, allowed);
  }
  public TokenState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeTokenState(input);
  }

  private TransferAction deserializeTransferAction(AbiInput _input) {
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new TransferAction(to, amount);
  }

  private BulkTransferAction deserializeBulkTransferAction(AbiInput _input) {
    var transfers_vecLength = _input.readI32();
    List<Transfer> transfers = new ArrayList<>();
    for (int transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      Transfer transfers_elem = deserializeTransfer(_input);
      transfers.add(transfers_elem);
    }
    return new BulkTransferAction(transfers);
  }

  private TransferFromAction deserializeTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    BlockchainAddress to = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new TransferFromAction(from, to, amount);
  }

  private BulkTransferFromAction deserializeBulkTransferFromAction(AbiInput _input) {
    BlockchainAddress from = _input.readAddress();
    var transfers_vecLength = _input.readI32();
    List<Transfer> transfers = new ArrayList<>();
    for (int transfers_i = 0; transfers_i < transfers_vecLength; transfers_i++) {
      Transfer transfers_elem = deserializeTransfer(_input);
      transfers.add(transfers_elem);
    }
    return new BulkTransferFromAction(from, transfers);
  }

  private ApproveAction deserializeApproveAction(AbiInput _input) {
    BlockchainAddress spender = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new ApproveAction(spender, amount);
  }

  private ApproveRelativeAction deserializeApproveRelativeAction(AbiInput _input) {
    BlockchainAddress spender = _input.readAddress();
    BigInteger delta = _input.readSignedBigInteger(16);
    return new ApproveRelativeAction(spender, delta);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    String name = _input.readString();
    String symbol = _input.readString();
    byte decimals = _input.readU8();
    BigInteger totalSupply = _input.readUnsignedBigInteger(16);
    return new InitializeInit(name, symbol, decimals, totalSupply);
  }


  @AbiGenerated
  public record AllowedAddress(BlockchainAddress owner, BlockchainAddress spender) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(owner);
      _out.writeAddress(spender);
    }
  }

  @AbiGenerated
  public record Transfer(BlockchainAddress to, BigInteger amount) {
    public void serialize(AbiOutput _out) {
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(amount, 16);
    }
  }

  @AbiGenerated
  public record TokenState(String name, byte decimals, String symbol, BlockchainAddress owner, BigInteger totalSupply, AvlTreeMap<BlockchainAddress, BigInteger> balances, AvlTreeMap<AllowedAddress, BigInteger> allowed) {
    public static TokenState deserialize(byte[] bytes) {
      return TokenV2.deserializeState(bytes);
    }
  }

  public static byte[] initialize(String name, String symbol, byte decimals, BigInteger totalSupply) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeString(name);
      _out.writeString(symbol);
      _out.writeU8(decimals);
      _out.writeUnsignedBigInteger(totalSupply, 16);
    });
  }

  public static byte[] transfer(BlockchainAddress to, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] bulkTransfer(List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] transferFrom(BlockchainAddress from, BlockchainAddress to, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(from);
      _out.writeAddress(to);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] bulkTransferFrom(BlockchainAddress from, List<Transfer> transfers) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(from);
      _out.writeI32(transfers.size());
      for (Transfer transfers_vec : transfers) {
        transfers_vec.serialize(_out);
      }
    });
  }

  public static byte[] approve(BlockchainAddress spender, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeAddress(spender);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] approveRelative(BlockchainAddress spender, BigInteger delta) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("07"));
      _out.writeAddress(spender);
      _out.writeSignedBigInteger(delta, 16);
    });
  }

  public static TokenState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new TokenV2(client, address).deserializeTokenState(input);
  }
  
  public static TokenState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static TokenState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record TransferAction(BlockchainAddress to, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record BulkTransferAction(List<Transfer> transfers) implements Action {
  }
  @AbiGenerated
  public record TransferFromAction(BlockchainAddress from, BlockchainAddress to, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record BulkTransferFromAction(BlockchainAddress from, List<Transfer> transfers) implements Action {
  }
  @AbiGenerated
  public record ApproveAction(BlockchainAddress spender, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ApproveRelativeAction(BlockchainAddress spender, BigInteger delta) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenV2(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeTransferAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeBulkTransferAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeTransferFromAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeBulkTransferFromAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeApproveAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeApproveRelativeAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(String name, String symbol, byte decimals, BigInteger totalSupply) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new TokenV2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
