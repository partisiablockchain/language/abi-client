// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractAllTypes_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractAllTypes_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private MyStructType deserializeMyStructType(AbiInput _input) {
    long someValue = _input.readU64();
    var someVector_vecLength = _input.readI32();
    List<Long> someVector = new ArrayList<>();
    for (int someVector_i = 0; someVector_i < someVector_vecLength; someVector_i++) {
      long someVector_elem = _input.readU64();
      someVector.add(someVector_elem);
    }
    return new MyStructType(someValue, someVector);
  }
  private MyOtherStructType deserializeMyOtherStructType(AbiInput _input) {
    long value = _input.readU64();
    var vector_vecLength = _input.readI32();
    List<MyStructType> vector = new ArrayList<>();
    for (int vector_i = 0; vector_i < vector_vecLength; vector_i++) {
      MyStructType vector_elem = deserializeMyStructType(_input);
      vector.add(vector_elem);
    }
    return new MyOtherStructType(value, vector);
  }
  private NonStateStructType deserializeNonStateStructType(AbiInput _input) {
    long value = _input.readU64();
    return new NonStateStructType(value);
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    byte myU8 = _input.readU8();
    short myU16 = _input.readU16();
    int myU32 = _input.readU32();
    long myU64 = _input.readU64();
    BigInteger myU128 = _input.readUnsignedBigInteger(16);
    byte myI8 = _input.readI8();
    short myI16 = _input.readI16();
    int myI32 = _input.readI32();
    long myI64 = _input.readI64();
    BigInteger myI128 = _input.readSignedBigInteger(16);
    String myString = _input.readString();
    boolean myBool = _input.readBoolean();
    BlockchainAddress myAddress = _input.readAddress();
    var myVec_vecLength = _input.readI32();
    List<Long> myVec = new ArrayList<>();
    for (int myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      long myVec_elem = _input.readU64();
      myVec.add(myVec_elem);
    }
    var myVecVec_vecLength = _input.readI32();
    List<List<Long>> myVecVec = new ArrayList<>();
    for (int myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      var myVecVec_elem_vecLength = _input.readI32();
      List<Long> myVecVec_elem = new ArrayList<>();
      for (int myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        long myVecVec_elem_elem = _input.readU64();
        myVecVec_elem.add(myVecVec_elem_elem);
      }
      myVecVec.add(myVecVec_elem);
    }
    var myMap_mapLength = _input.readI32();
    Map<BlockchainAddress, Byte> myMap = new HashMap<>();
    for (int myMap_i = 0; myMap_i < myMap_mapLength; myMap_i++) {
      BlockchainAddress myMap_key = _input.readAddress();
      byte myMap_value = _input.readU8();
      myMap.put(myMap_key, myMap_value);
    }
    var mySet_setLength = _input.readI32();
    List<Long> mySet = new ArrayList<>();
    for (int mySet_i = 0; mySet_i < mySet_setLength; mySet_i++) {
      long mySet_elem = _input.readU64();
      mySet.add(mySet_elem);
    }
    var myComplexMap_mapLength = _input.readI32();
    Map<Long, List<Long>> myComplexMap = new HashMap<>();
    for (int myComplexMap_i = 0; myComplexMap_i < myComplexMap_mapLength; myComplexMap_i++) {
      long myComplexMap_key = _input.readU64();
      var myComplexMap_value_vecLength = _input.readI32();
      List<Long> myComplexMap_value = new ArrayList<>();
      for (int myComplexMap_value_i = 0; myComplexMap_value_i < myComplexMap_value_vecLength; myComplexMap_value_i++) {
        long myComplexMap_value_elem = _input.readU64();
        myComplexMap_value.add(myComplexMap_value_elem);
      }
      myComplexMap.put(myComplexMap_key, myComplexMap_value);
    }
    byte[] myArray = _input.readBytes(16);
    byte[] myArray2 = _input.readBytes(5);
    MyStructType myStruct = deserializeMyStructType(_input);
    MyOtherStructType myOtherStruct = deserializeMyOtherStructType(_input);
    return new ExampleContractState(myU8, myU16, myU32, myU64, myU128, myI8, myI16, myI32, myI64, myI128, myString, myBool, myAddress, myVec, myVecVec, myMap, mySet, myComplexMap, myArray, myArray2, myStruct, myOtherStruct);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private InsertInMySetAction deserializeInsertInMySetAction(AbiInput _input) {
    long value = _input.readU64();
    return new InsertInMySetAction(value);
  }

  private InsertInMyMapAction deserializeInsertInMyMapAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    byte value = _input.readU8();
    return new InsertInMyMapAction(address, value);
  }

  private InsertInMyComplexMapAction deserializeInsertInMyComplexMapAction(AbiInput _input) {
    long key = _input.readU64();
    var value_vecLength = _input.readI32();
    List<Long> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      long value_elem = _input.readU64();
      value.add(value_elem);
    }
    return new InsertInMyComplexMapAction(key, value);
  }

  private UpdateMyU8Action deserializeUpdateMyU8Action(AbiInput _input) {
    byte value = _input.readU8();
    return new UpdateMyU8Action(value);
  }

  private UpdateMyU16Action deserializeUpdateMyU16Action(AbiInput _input) {
    short value = _input.readU16();
    return new UpdateMyU16Action(value);
  }

  private UpdateMyU32Action deserializeUpdateMyU32Action(AbiInput _input) {
    int value = _input.readU32();
    return new UpdateMyU32Action(value);
  }

  private UpdateMyU64Action deserializeUpdateMyU64Action(AbiInput _input) {
    long value = _input.readU64();
    return new UpdateMyU64Action(value);
  }

  private UpdateMyU64UsingStructAction deserializeUpdateMyU64UsingStructAction(AbiInput _input) {
    NonStateStructType value = deserializeNonStateStructType(_input);
    return new UpdateMyU64UsingStructAction(value);
  }

  private UpdateMyU128Action deserializeUpdateMyU128Action(AbiInput _input) {
    BigInteger value = _input.readUnsignedBigInteger(16);
    return new UpdateMyU128Action(value);
  }

  private UpdateMyI8Action deserializeUpdateMyI8Action(AbiInput _input) {
    byte value = _input.readI8();
    return new UpdateMyI8Action(value);
  }

  private UpdateMyI16Action deserializeUpdateMyI16Action(AbiInput _input) {
    short value = _input.readI16();
    return new UpdateMyI16Action(value);
  }

  private UpdateMyI32Action deserializeUpdateMyI32Action(AbiInput _input) {
    int value = _input.readI32();
    return new UpdateMyI32Action(value);
  }

  private UpdateMyI64Action deserializeUpdateMyI64Action(AbiInput _input) {
    long value = _input.readI64();
    return new UpdateMyI64Action(value);
  }

  private UpdateMyI128Action deserializeUpdateMyI128Action(AbiInput _input) {
    BigInteger value = _input.readSignedBigInteger(16);
    return new UpdateMyI128Action(value);
  }

  private UpdateMyStringAction deserializeUpdateMyStringAction(AbiInput _input) {
    String value = _input.readString();
    return new UpdateMyStringAction(value);
  }

  private UpdateMyBoolAction deserializeUpdateMyBoolAction(AbiInput _input) {
    boolean value = _input.readBoolean();
    return new UpdateMyBoolAction(value);
  }

  private UpdateMyAddressAction deserializeUpdateMyAddressAction(AbiInput _input) {
    BlockchainAddress value = _input.readAddress();
    return new UpdateMyAddressAction(value);
  }

  private UpdateMyVecAction deserializeUpdateMyVecAction(AbiInput _input) {
    var value_vecLength = _input.readI32();
    List<Long> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      long value_elem = _input.readU64();
      value.add(value_elem);
    }
    return new UpdateMyVecAction(value);
  }

  private UpdateMyVecVecAction deserializeUpdateMyVecVecAction(AbiInput _input) {
    var value_vecLength = _input.readI32();
    List<List<Long>> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      var value_elem_vecLength = _input.readI32();
      List<Long> value_elem = new ArrayList<>();
      for (int value_elem_i = 0; value_elem_i < value_elem_vecLength; value_elem_i++) {
        long value_elem_elem = _input.readU64();
        value_elem.add(value_elem_elem);
      }
      value.add(value_elem);
    }
    return new UpdateMyVecVecAction(value);
  }

  private UpdateMyArrayAction deserializeUpdateMyArrayAction(AbiInput _input) {
    byte[] value = _input.readBytes(16);
    return new UpdateMyArrayAction(value);
  }

  private UpdateMyArray2Action deserializeUpdateMyArray2Action(AbiInput _input) {
    byte[] value = _input.readBytes(5);
    return new UpdateMyArray2Action(value);
  }

  private UpdateMyStructAction deserializeUpdateMyStructAction(AbiInput _input) {
    MyStructType value = deserializeMyStructType(_input);
    return new UpdateMyStructAction(value);
  }

  private UpdateMyOtherStructAction deserializeUpdateMyOtherStructAction(AbiInput _input) {
    MyOtherStructType value = deserializeMyOtherStructType(_input);
    return new UpdateMyOtherStructAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte myU8 = _input.readU8();
    short myU16 = _input.readU16();
    int myU32 = _input.readU32();
    long myU64 = _input.readU64();
    BigInteger myU128 = _input.readUnsignedBigInteger(16);
    byte myI8 = _input.readI8();
    short myI16 = _input.readI16();
    int myI32 = _input.readI32();
    long myI64 = _input.readI64();
    BigInteger myI128 = _input.readSignedBigInteger(16);
    String myString = _input.readString();
    boolean myBool = _input.readBoolean();
    BlockchainAddress myAddress = _input.readAddress();
    var myVec_vecLength = _input.readI32();
    List<Long> myVec = new ArrayList<>();
    for (int myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      long myVec_elem = _input.readU64();
      myVec.add(myVec_elem);
    }
    var myVecVec_vecLength = _input.readI32();
    List<List<Long>> myVecVec = new ArrayList<>();
    for (int myVecVec_i = 0; myVecVec_i < myVecVec_vecLength; myVecVec_i++) {
      var myVecVec_elem_vecLength = _input.readI32();
      List<Long> myVecVec_elem = new ArrayList<>();
      for (int myVecVec_elem_i = 0; myVecVec_elem_i < myVecVec_elem_vecLength; myVecVec_elem_i++) {
        long myVecVec_elem_elem = _input.readU64();
        myVecVec_elem.add(myVecVec_elem_elem);
      }
      myVecVec.add(myVecVec_elem);
    }
    byte[] myArray = _input.readBytes(16);
    byte[] myArray2 = _input.readBytes(5);
    MyStructType myStruct = deserializeMyStructType(_input);
    MyOtherStructType myOtherStruct = deserializeMyOtherStructType(_input);
    return new InitializeInit(myU8, myU16, myU32, myU64, myU128, myI8, myI16, myI32, myI64, myI128, myString, myBool, myAddress, myVec, myVecVec, myArray, myArray2, myStruct, myOtherStruct);
  }


  @AbiGenerated
  public record MyStructType(long someValue, List<Long> someVector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(someValue);
      _out.writeI32(someVector.size());
      for (long someVector_vec : someVector) {
        _out.writeU64(someVector_vec);
      }
    }
  }

  @AbiGenerated
  public record MyOtherStructType(long value, List<MyStructType> vector) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(value);
      _out.writeI32(vector.size());
      for (MyStructType vector_vec : vector) {
        vector_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record NonStateStructType(long value) {
    public void serialize(AbiOutput _out) {
      _out.writeU64(value);
    }
  }

  @AbiGenerated
  public record ExampleContractState(byte myU8, short myU16, int myU32, long myU64, BigInteger myU128, byte myI8, short myI16, int myI32, long myI64, BigInteger myI128, String myString, boolean myBool, BlockchainAddress myAddress, List<Long> myVec, List<List<Long>> myVecVec, Map<BlockchainAddress, Byte> myMap, List<Long> mySet, Map<Long, List<Long>> myComplexMap, byte[] myArray, byte[] myArray2, MyStructType myStruct, MyOtherStructType myOtherStruct) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractAllTypes_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte myU8, short myU16, int myU32, long myU64, BigInteger myU128, byte myI8, short myI16, int myI32, long myI64, BigInteger myI128, String myString, boolean myBool, BlockchainAddress myAddress, List<Long> myVec, List<List<Long>> myVecVec, byte[] myArray, byte[] myArray2, MyStructType myStruct, MyOtherStructType myOtherStruct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(myU8);
      _out.writeU16(myU16);
      _out.writeU32(myU32);
      _out.writeU64(myU64);
      _out.writeUnsignedBigInteger(myU128, 16);
      _out.writeI8(myI8);
      _out.writeI16(myI16);
      _out.writeI32(myI32);
      _out.writeI64(myI64);
      _out.writeSignedBigInteger(myI128, 16);
      _out.writeString(myString);
      _out.writeBoolean(myBool);
      _out.writeAddress(myAddress);
      _out.writeI32(myVec.size());
      for (long myVec_vec : myVec) {
        _out.writeU64(myVec_vec);
      }
      _out.writeI32(myVecVec.size());
      for (List<Long> myVecVec_vec : myVecVec) {
        _out.writeI32(myVecVec_vec.size());
        for (long myVecVec_vec_vec : myVecVec_vec) {
          _out.writeU64(myVecVec_vec_vec);
        }
      }
      if (myArray.length != 16) {
        throw new RuntimeException("Length of myArray does not match expected 16");
      }
      _out.writeBytes(myArray);
      if (myArray2.length != 5) {
        throw new RuntimeException("Length of myArray2 does not match expected 5");
      }
      _out.writeBytes(myArray2);
      myStruct.serialize(_out);
      myOtherStruct.serialize(_out);
    });
  }

  public static byte[] insertInMySet(long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f5faeadf04"));
      _out.writeU64(value);
    });
  }

  public static byte[] insertInMyMap(BlockchainAddress address, byte value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("fc91b4c102"));
      _out.writeAddress(address);
      _out.writeU8(value);
    });
  }

  public static byte[] insertInMyComplexMap(long key, List<Long> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("90b7e3f10c"));
      _out.writeU64(key);
      _out.writeI32(value.size());
      for (long value_vec : value) {
        _out.writeU64(value_vec);
      }
    });
  }

  public static byte[] updateMyU8(byte value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("80a1b19e07"));
      _out.writeU8(value);
    });
  }

  public static byte[] updateMyU16(short value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b2f8b59804"));
      _out.writeU16(value);
    });
  }

  public static byte[] updateMyU32(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("efe5ed810e"));
      _out.writeU32(value);
    });
  }

  public static byte[] updateMyU64(long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("e4f68de503"));
      _out.writeU64(value);
    });
  }

  public static byte[] updateMyU64UsingStruct(NonStateStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f8e385b70e"));
      value.serialize(_out);
    });
  }

  public static byte[] updateMyU128(BigInteger value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("8fc4c2a607"));
      _out.writeUnsignedBigInteger(value, 16);
    });
  }

  public static byte[] updateMyI8(byte value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f495f18c0a"));
      _out.writeI8(value);
    });
  }

  public static byte[] updateMyI16(short value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b9dadd8e0e"));
      _out.writeI16(value);
    });
  }

  public static byte[] updateMyI32(int value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b3cc80960a"));
      _out.writeI32(value);
    });
  }

  public static byte[] updateMyI64(long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("adc6ccdc05"));
      _out.writeI64(value);
    });
  }

  public static byte[] updateMyI128(BigInteger value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("8ce7ebef06"));
      _out.writeSignedBigInteger(value, 16);
    });
  }

  public static byte[] updateMyString(String value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("97c18ca406"));
      _out.writeString(value);
    });
  }

  public static byte[] updateMyBool(boolean value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("b0a1fab30c"));
      _out.writeBoolean(value);
    });
  }

  public static byte[] updateMyAddress(BlockchainAddress value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("a7e192a009"));
      _out.writeAddress(value);
    });
  }

  public static byte[] updateMyVec(List<Long> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("9d99e6e401"));
      _out.writeI32(value.size());
      for (long value_vec : value) {
        _out.writeU64(value_vec);
      }
    });
  }

  public static byte[] updateMyVecVec(List<List<Long>> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ccc0d6db04"));
      _out.writeI32(value.size());
      for (List<Long> value_vec : value) {
        _out.writeI32(value_vec.size());
        for (long value_vec_vec : value_vec) {
          _out.writeU64(value_vec_vec);
        }
      }
    });
  }

  public static byte[] updateMyArray(byte[] value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("cbe4cbfd0d"));
      if (value.length != 16) {
        throw new RuntimeException("Length of value does not match expected 16");
      }
      _out.writeBytes(value);
    });
  }

  public static byte[] updateMyArray2(byte[] value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ec9da775"));
      if (value.length != 5) {
        throw new RuntimeException("Length of value does not match expected 5");
      }
      _out.writeBytes(value);
    });
  }

  public static byte[] updateMyStruct(MyStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f3b68ff40e"));
      value.serialize(_out);
    });
  }

  public static byte[] updateMyOtherStruct(MyOtherStructType value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("85f292be0b"));
      value.serialize(_out);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractAllTypes_SDK_13_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record InsertInMySetAction(long value) implements Action {
  }
  @AbiGenerated
  public record InsertInMyMapAction(BlockchainAddress address, byte value) implements Action {
  }
  @AbiGenerated
  public record InsertInMyComplexMapAction(long key, List<Long> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU8Action(byte value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU16Action(short value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU32Action(int value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU64Action(long value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU64UsingStructAction(NonStateStructType value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyU128Action(BigInteger value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI8Action(byte value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI16Action(short value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI32Action(int value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI64Action(long value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyI128Action(BigInteger value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyStringAction(String value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyBoolAction(boolean value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyAddressAction(BlockchainAddress value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyVecAction(List<Long> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyVecVecAction(List<List<Long>> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArrayAction(byte[] value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyArray2Action(byte[] value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyStructAction(MyStructType value) implements Action {
  }
  @AbiGenerated
  public record UpdateMyOtherStructAction(MyOtherStructType value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractAllTypes_SDK_13_4_0(null, null);
    if (shortname.equals("f5faeadf04")) {
      return contract.deserializeInsertInMySetAction(input);
    } else if (shortname.equals("fc91b4c102")) {
      return contract.deserializeInsertInMyMapAction(input);
    } else if (shortname.equals("90b7e3f10c")) {
      return contract.deserializeInsertInMyComplexMapAction(input);
    } else if (shortname.equals("80a1b19e07")) {
      return contract.deserializeUpdateMyU8Action(input);
    } else if (shortname.equals("b2f8b59804")) {
      return contract.deserializeUpdateMyU16Action(input);
    } else if (shortname.equals("efe5ed810e")) {
      return contract.deserializeUpdateMyU32Action(input);
    } else if (shortname.equals("e4f68de503")) {
      return contract.deserializeUpdateMyU64Action(input);
    } else if (shortname.equals("f8e385b70e")) {
      return contract.deserializeUpdateMyU64UsingStructAction(input);
    } else if (shortname.equals("8fc4c2a607")) {
      return contract.deserializeUpdateMyU128Action(input);
    } else if (shortname.equals("f495f18c0a")) {
      return contract.deserializeUpdateMyI8Action(input);
    } else if (shortname.equals("b9dadd8e0e")) {
      return contract.deserializeUpdateMyI16Action(input);
    } else if (shortname.equals("b3cc80960a")) {
      return contract.deserializeUpdateMyI32Action(input);
    } else if (shortname.equals("adc6ccdc05")) {
      return contract.deserializeUpdateMyI64Action(input);
    } else if (shortname.equals("8ce7ebef06")) {
      return contract.deserializeUpdateMyI128Action(input);
    } else if (shortname.equals("97c18ca406")) {
      return contract.deserializeUpdateMyStringAction(input);
    } else if (shortname.equals("b0a1fab30c")) {
      return contract.deserializeUpdateMyBoolAction(input);
    } else if (shortname.equals("a7e192a009")) {
      return contract.deserializeUpdateMyAddressAction(input);
    } else if (shortname.equals("9d99e6e401")) {
      return contract.deserializeUpdateMyVecAction(input);
    } else if (shortname.equals("ccc0d6db04")) {
      return contract.deserializeUpdateMyVecVecAction(input);
    } else if (shortname.equals("cbe4cbfd0d")) {
      return contract.deserializeUpdateMyArrayAction(input);
    } else if (shortname.equals("ec9da775")) {
      return contract.deserializeUpdateMyArray2Action(input);
    } else if (shortname.equals("f3b68ff40e")) {
      return contract.deserializeUpdateMyStructAction(input);
    } else if (shortname.equals("85f292be0b")) {
      return contract.deserializeUpdateMyOtherStructAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte myU8, short myU16, int myU32, long myU64, BigInteger myU128, byte myI8, short myI16, int myI32, long myI64, BigInteger myI128, String myString, boolean myBool, BlockchainAddress myAddress, List<Long> myVec, List<List<Long>> myVecVec, byte[] myArray, byte[] myArray2, MyStructType myStruct, MyOtherStructType myOtherStruct) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractAllTypes_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
