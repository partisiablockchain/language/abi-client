// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractByteArrays_SDK_10_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const myArray: Buffer = _input.readBytes(16);
    const myArray2: Buffer = _input.readBytes(5);
    return { myArray, myArray2 };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateMyArrayAction(_input: AbiInput): UpdateMyArrayAction {
    const value: Buffer = _input.readBytes(16);
    return { discriminant: "update_my_array", value };
  }

  public deserializeUpdateMyArray2Action(_input: AbiInput): UpdateMyArray2Action {
    const value: Buffer = _input.readBytes(5);
    return { discriminant: "update_my_array_2", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const myArray: Buffer = _input.readBytes(16);
    const myArray2: Buffer = _input.readBytes(5);
    return { discriminant: "initialize", myArray, myArray2 };
  }

}
export interface ExampleContractState {
  myArray: Buffer;
  myArray2: Buffer;
}

export function initialize(myArray: Buffer, myArray2: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    if (myArray.length != 16) {
      throw new Error("Length of myArray does not match expected 16");
    }
    _out.writeBytes(myArray);
    if (myArray2.length != 5) {
      throw new Error("Length of myArray2 does not match expected 5");
    }
    _out.writeBytes(myArray2);
  });
}

export function updateMyArray(value: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("cbe4cbfd0d", "hex"));
    if (value.length != 16) {
      throw new Error("Length of value does not match expected 16");
    }
    _out.writeBytes(value);
  });
}

export function updateMyArray2(value: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ec9da775", "hex"));
    if (value.length != 5) {
      throw new Error("Length of value does not match expected 5");
    }
    _out.writeBytes(value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractByteArrays_SDK_10_1_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractByteArrays_SDK_10_1_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateMyArrayAction
  | UpdateMyArray2Action;

export interface UpdateMyArrayAction {
  discriminant: "update_my_array";
  value: Buffer;
}
export interface UpdateMyArray2Action {
  discriminant: "update_my_array_2";
  value: Buffer;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractByteArrays_SDK_10_1_0(undefined, undefined);
  if (shortname === "cbe4cbfd0d") {
    return contract.deserializeUpdateMyArrayAction(input);
  } else if (shortname === "ec9da775") {
    return contract.deserializeUpdateMyArray2Action(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  myArray: Buffer;
  myArray2: Buffer;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractByteArrays_SDK_10_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

