// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ZkLiquiditySwap_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeToken(_input: AbiInput): Token {
    const discriminant = _input.readU8();
    if (discriminant === 0) {
      return this.deserializeTokenTokenA(_input);
    } else if (discriminant === 1) {
      return this.deserializeTokenTokenB(_input);
    }
    throw new Error("Unknown discriminant: " + discriminant);
  }
  public deserializeTokenTokenA(_input: AbiInput): TokenTokenA {
    return { discriminant: TokenD.TokenA,  };
  }
  public deserializeTokenTokenB(_input: AbiInput): TokenTokenB {
    return { discriminant: TokenD.TokenB,  };
  }
  public deserializeBalance(_input: AbiInput): Balance {
    const poolABalance: BN = _input.readUnsignedBigInteger(16);
    const poolBBalance: BN = _input.readUnsignedBigInteger(16);
    return { poolABalance, poolBBalance };
  }
  public deserializePairwiseTokenBalances(_input: AbiInput): PairwiseTokenBalances {
    const tokenA: BlockchainAddress = _input.readAddress();
    const tokenB: BlockchainAddress = _input.readAddress();
    const balances_mapLength = _input.readI32();
    const balances: Map<BlockchainAddress, Balance> = new Map();
    for (let balances_i = 0; balances_i < balances_mapLength; balances_i++) {
      const balances_key: BlockchainAddress = _input.readAddress();
      const balances_value: Balance = this.deserializeBalance(_input);
      balances.set(balances_key, balances_value);
    }
    return { tokenA, tokenB, balances };
  }
  public deserializeWorklistEntry(_input: AbiInput): WorklistEntry {
    const variableId: SecretVarId = this.deserializeSecretVarId(_input);
    const sender: BlockchainAddress = _input.readAddress();
    return { variableId, sender };
  }
  public deserializeContractState(_input: AbiInput): ContractState {
    const contractOwner: BlockchainAddress = _input.readAddress();
    const tokenPoolAddress: BlockchainAddress = _input.readAddress();
    const swapConstant: BN = _input.readUnsignedBigInteger(16);
    const isClosed: boolean = _input.readBoolean();
    const balances: PairwiseTokenBalances = this.deserializePairwiseTokenBalances(_input);
    const worklist_vecLength = _input.readI32();
    const worklist: WorklistEntry[] = [];
    for (let worklist_i = 0; worklist_i < worklist_vecLength; worklist_i++) {
      const worklist_elem: WorklistEntry = this.deserializeWorklistEntry(_input);
      worklist.push(worklist_elem);
    }
    const unusedVariables_vecLength = _input.readI32();
    const unusedVariables: SecretVarId[] = [];
    for (let unusedVariables_i = 0; unusedVariables_i < unusedVariables_vecLength; unusedVariables_i++) {
      const unusedVariables_elem: SecretVarId = this.deserializeSecretVarId(_input);
      unusedVariables.push(unusedVariables_elem);
    }
    return { contractOwner, tokenPoolAddress, swapConstant, isClosed, balances, worklist, unusedVariables };
  }
  public deserializeSecretVarId(_input: AbiInput): SecretVarId {
    const rawId: number = _input.readU32();
    return { rawId };
  }
  public async getState(): Promise<ContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeContractState(input);
  }

  public deserializeProvideLiquidityAction(_input: AbiInput): ProvideLiquidityAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const poolSize: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "provide_liquidity", tokenAddress, poolSize };
  }

  public deserializeDepositAction(_input: AbiInput): DepositAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "deposit", tokenAddress, amount };
  }

  public deserializeWithdrawAction(_input: AbiInput): WithdrawAction {
    const tokenAddress: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "withdraw", tokenAddress, amount };
  }

  public deserializeClosePoolsAction(_input: AbiInput): ClosePoolsAction {
    return { discriminant: "close_pools",  };
  }

  public deserializeDepositCallbackCallback(_input: AbiInput): DepositCallbackCallback {
    const token: Token = this.deserializeToken(_input);
    const amount: BN = _input.readUnsignedBigInteger(16);
    return { discriminant: "deposit_callback", token, amount };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const tokenAAddress: BlockchainAddress = _input.readAddress();
    const tokenBAddress: BlockchainAddress = _input.readAddress();
    return { discriminant: "initialize", tokenAAddress, tokenBAddress };
  }

}
export enum TokenD {
  TokenA = 0,
  TokenB = 1,
}
export type Token =
  | TokenTokenA
  | TokenTokenB;
function serializeToken(out: AbiOutput, value: Token): void {
  if (value.discriminant === TokenD.TokenA) {
    return serializeTokenTokenA(out, value);
  } else if (value.discriminant === TokenD.TokenB) {
    return serializeTokenTokenB(out, value);
  }
}

export interface TokenTokenA {
  discriminant: TokenD.TokenA;
}
function serializeTokenTokenA(_out: AbiOutput, _value: TokenTokenA): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface TokenTokenB {
  discriminant: TokenD.TokenB;
}
function serializeTokenTokenB(_out: AbiOutput, _value: TokenTokenB): void {
  const {} = _value;
  _out.writeU8(_value.discriminant);
}

export interface Balance {
  poolABalance: BN;
  poolBBalance: BN;
}

export interface PairwiseTokenBalances {
  tokenA: BlockchainAddress;
  tokenB: BlockchainAddress;
  balances: Map<BlockchainAddress, Balance>;
}

export interface WorklistEntry {
  variableId: SecretVarId;
  sender: BlockchainAddress;
}

export interface ContractState {
  contractOwner: BlockchainAddress;
  tokenPoolAddress: BlockchainAddress;
  swapConstant: BN;
  isClosed: boolean;
  balances: PairwiseTokenBalances;
  worklist: WorklistEntry[];
  unusedVariables: SecretVarId[];
}

export interface SecretVarId {
  rawId: number;
}

export function initialize(tokenAAddress: BlockchainAddress, tokenBAddress: BlockchainAddress): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(tokenAAddress);
    _out.writeAddress(tokenBAddress);
  });
}

export function provideLiquidity(tokenAddress: BlockchainAddress, poolSize: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("10", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(poolSize, 16);
  });
}

export function deposit(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("11", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function withdraw(tokenAddress: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("14", "hex"));
    _out.writeAddress(tokenAddress);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function closePools(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("15", "hex"));
  });
}

export function swap(onlyIfAtFront: boolean): SecretInputBuilder<number> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("13", "hex"));
    _out.writeBoolean(onlyIfAtFront);
  });
  const _secretInput = (secret_input_lambda: number): CompactBitArray => AbiBitOutput.serialize((_out) => {
    _out.writeI32(secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function depositCallback(token: Token, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    serializeToken(_out, token);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function deserializeState(state: StateWithClient): ContractState;
export function deserializeState(bytes: Buffer): ContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ZkLiquiditySwap_SDK_15_4_0(client, address).deserializeContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ZkLiquiditySwap_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeContractState(input);
  }
}

export type Action =
  | ProvideLiquidityAction
  | DepositAction
  | WithdrawAction
  | ClosePoolsAction;

export interface ProvideLiquidityAction {
  discriminant: "provide_liquidity";
  tokenAddress: BlockchainAddress;
  poolSize: BN;
}
export interface DepositAction {
  discriminant: "deposit";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface WithdrawAction {
  discriminant: "withdraw";
  tokenAddress: BlockchainAddress;
  amount: BN;
}
export interface ClosePoolsAction {
  discriminant: "close_pools";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new ZkLiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "10") {
    return contract.deserializeProvideLiquidityAction(input);
  } else if (shortname === "11") {
    return contract.deserializeDepositAction(input);
  } else if (shortname === "14") {
    return contract.deserializeWithdrawAction(input);
  } else if (shortname === "15") {
    return contract.deserializeClosePoolsAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Callback =
  | DepositCallbackCallback;

export interface DepositCallbackCallback {
  discriminant: "deposit_callback";
  token: Token;
  amount: BN;
}
export function deserializeCallback(bytes: Buffer): Callback {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkLiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "02") {
    return contract.deserializeDepositCallbackCallback(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  tokenAAddress: BlockchainAddress;
  tokenBAddress: BlockchainAddress;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ZkLiquiditySwap_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

