// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ContractRecursiveTypes_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeSelfRecursiveType(_input: AbiInput): SelfRecursiveType {
    const child_vecLength = _input.readI32();
    const child: SelfRecursiveType[] = [];
    for (let child_i = 0; child_i < child_vecLength; child_i++) {
      const child_elem: SelfRecursiveType = this.deserializeSelfRecursiveType(_input);
      child.push(child_elem);
    }
    return { child };
  }
  public deserializeMutualOne(_input: AbiInput): MutualOne {
    const value: MutualTwo = this.deserializeMutualTwo(_input);
    return { value };
  }
  public deserializeMutualTwo(_input: AbiInput): MutualTwo {
    const value_vecLength = _input.readI32();
    const value: MutualOne[] = [];
    for (let value_i = 0; value_i < value_vecLength; value_i++) {
      const value_elem: MutualOne = this.deserializeMutualOne(_input);
      value.push(value_elem);
    }
    return { value };
  }
  public deserializeExampleContractState(_input: AbiInput): ExampleContractState {
    const selfRecursiveStruct: SelfRecursiveType = this.deserializeSelfRecursiveType(_input);
    const mutuallyRecursiveStruct: MutualOne = this.deserializeMutualOne(_input);
    return { selfRecursiveStruct, mutuallyRecursiveStruct };
  }
  public async getState(): Promise<ExampleContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeExampleContractState(input);
  }

  public deserializeUpdateSelfRecursiveAction(_input: AbiInput): UpdateSelfRecursiveAction {
    const value: SelfRecursiveType = this.deserializeSelfRecursiveType(_input);
    return { discriminant: "update_self_recursive", value };
  }

  public deserializeUpdateMutuallyRecursiveAction(_input: AbiInput): UpdateMutuallyRecursiveAction {
    const value: MutualOne = this.deserializeMutualOne(_input);
    return { discriminant: "update_mutually_recursive", value };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const selfRecursiveStruct: SelfRecursiveType = this.deserializeSelfRecursiveType(_input);
    const mutuallyRecursiveStruct: MutualOne = this.deserializeMutualOne(_input);
    return { discriminant: "initialize", selfRecursiveStruct, mutuallyRecursiveStruct };
  }

}
export interface SelfRecursiveType {
  child: SelfRecursiveType[];
}
function serializeSelfRecursiveType(_out: AbiOutput, _value: SelfRecursiveType): void {
  const { child } = _value;
  _out.writeI32(child.length);
  for (const child_vec of child) {
    serializeSelfRecursiveType(_out, child_vec);
  }
}

export interface MutualOne {
  value: MutualTwo;
}
function serializeMutualOne(_out: AbiOutput, _value: MutualOne): void {
  const { value } = _value;
  serializeMutualTwo(_out, value);
}

export interface MutualTwo {
  value: MutualOne[];
}
function serializeMutualTwo(_out: AbiOutput, _value: MutualTwo): void {
  const { value } = _value;
  _out.writeI32(value.length);
  for (const value_vec of value) {
    serializeMutualOne(_out, value_vec);
  }
}

export interface ExampleContractState {
  selfRecursiveStruct: SelfRecursiveType;
  mutuallyRecursiveStruct: MutualOne;
}

export function initialize(selfRecursiveStruct: SelfRecursiveType, mutuallyRecursiveStruct: MutualOne): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    serializeSelfRecursiveType(_out, selfRecursiveStruct);
    serializeMutualOne(_out, mutuallyRecursiveStruct);
  });
}

export function updateSelfRecursive(value: SelfRecursiveType): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("dcc3fe0f", "hex"));
    serializeSelfRecursiveType(_out, value);
  });
}

export function updateMutuallyRecursive(value: MutualOne): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("cae4f5e601", "hex"));
    serializeMutualOne(_out, value);
  });
}

export function deserializeState(state: StateWithClient): ExampleContractState;
export function deserializeState(bytes: Buffer): ExampleContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ExampleContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ExampleContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ContractRecursiveTypes_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ContractRecursiveTypes_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeExampleContractState(input);
  }
}

export type Action =
  | UpdateSelfRecursiveAction
  | UpdateMutuallyRecursiveAction;

export interface UpdateSelfRecursiveAction {
  discriminant: "update_self_recursive";
  value: SelfRecursiveType;
}
export interface UpdateMutuallyRecursiveAction {
  discriminant: "update_mutually_recursive";
  value: MutualOne;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractRecursiveTypes_SDK_15_4_0(undefined, undefined);
  if (shortname === "dcc3fe0f") {
    return contract.deserializeUpdateSelfRecursiveAction(input);
  } else if (shortname === "cae4f5e601") {
    return contract.deserializeUpdateMutuallyRecursiveAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  selfRecursiveStruct: SelfRecursiveType;
  mutuallyRecursiveStruct: MutualOne;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ContractRecursiveTypes_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

