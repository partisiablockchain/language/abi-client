// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class BpOrchestration_SDK_13_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public BpOrchestration_SDK_13_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private BlockProducer deserializeBlockProducer(AbiInput _input) {
    String name = null;
    var name_isSome = _input.readBoolean();
    if (name_isSome) {
      String name_option = _input.readString();
      name = name_option;
    }
    String address = null;
    var address_isSome = _input.readBoolean();
    if (address_isSome) {
      String address_option = _input.readString();
      address = address_option;
    }
    String website = null;
    var website_isSome = _input.readBoolean();
    if (website_isSome) {
      String website_option = _input.readString();
      website = website_option;
    }
    BlockchainAddress identity = null;
    var identity_isSome = _input.readBoolean();
    if (identity_isSome) {
      BlockchainAddress identity_option = _input.readAddress();
      identity = identity_option;
    }
    int entityJurisdiction = _input.readI32();
    int serverJurisdiction = _input.readI32();
    BlockchainPublicKey publicKey = null;
    var publicKey_isSome = _input.readBoolean();
    if (publicKey_isSome) {
      BlockchainPublicKey publicKey_option = _input.readPublicKey();
      publicKey = publicKey_option;
    }
    BlsPublicKey blsPublicKey = null;
    var blsPublicKey_isSome = _input.readBoolean();
    if (blsPublicKey_isSome) {
      BlsPublicKey blsPublicKey_option = _input.readBlsPublicKey();
      blsPublicKey = blsPublicKey_option;
    }
    long numberOfVotes = _input.readI64();
    BlockProducerStatus status = null;
    var status_isSome = _input.readBoolean();
    if (status_isSome) {
      BlockProducerStatus status_option = deserializeBlockProducerStatus(_input);
      status = status_option;
    }
    return new BlockProducer(name, address, website, identity, entityJurisdiction, serverJurisdiction, publicKey, blsPublicKey, numberOfVotes, status);
  }
  private BlockProducerInformation deserializeBlockProducerInformation(AbiInput _input) {
    String name = _input.readString();
    String website = _input.readString();
    String address = _input.readString();
    BlockchainPublicKey publicKey = _input.readPublicKey();
    BlsPublicKey blsPublicKey = _input.readBlsPublicKey();
    int entityJurisdiction = _input.readI32();
    int serverJurisdiction = _input.readI32();
    return new BlockProducerInformation(name, website, address, publicKey, blsPublicKey, entityJurisdiction, serverJurisdiction);
  }
  private LargeOracleUpdate deserializeLargeOracleUpdate(AbiInput _input) {
    List<BlockProducer> producers = null;
    var producers_isSome = _input.readBoolean();
    if (producers_isSome) {
      var producers_option_vecLength = _input.readI32();
      List<BlockProducer> producers_option = new ArrayList<>();
      for (int producers_option_i = 0; producers_option_i < producers_option_vecLength; producers_option_i++) {
        BlockProducer producers_option_elem = null;
        var producers_option_elem_isSome = _input.readBoolean();
        if (producers_option_elem_isSome) {
          BlockProducer producers_option_elem_option = deserializeBlockProducer(_input);
          producers_option_elem = producers_option_elem_option;
        }
        producers_option.add(producers_option_elem);
      }
      producers = producers_option;
    }
    ThresholdKey thresholdKey = null;
    var thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      ThresholdKey thresholdKey_option = deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    List<CandidateKey> candidates = null;
    var candidates_isSome = _input.readBoolean();
    if (candidates_isSome) {
      var candidates_option_vecLength = _input.readI32();
      List<CandidateKey> candidates_option = new ArrayList<>();
      for (int candidates_option_i = 0; candidates_option_i < candidates_option_vecLength; candidates_option_i++) {
        CandidateKey candidates_option_elem = null;
        var candidates_option_elem_isSome = _input.readBoolean();
        if (candidates_option_elem_isSome) {
          CandidateKey candidates_option_elem_option = deserializeCandidateKey(_input);
          candidates_option_elem = candidates_option_elem_option;
        }
        candidates_option.add(candidates_option_elem);
      }
      candidates = candidates_option;
    }
    Map<BlockchainAddress, Bitmap> honestPartyViews = null;
    var honestPartyViews_isSome = _input.readBoolean();
    if (honestPartyViews_isSome) {
      var honestPartyViews_option_mapLength = _input.readI32();
      Map<BlockchainAddress, Bitmap> honestPartyViews_option = new HashMap<>();
      for (int honestPartyViews_option_i = 0; honestPartyViews_option_i < honestPartyViews_option_mapLength; honestPartyViews_option_i++) {
        BlockchainAddress honestPartyViews_option_key = null;
        var honestPartyViews_option_key_isSome = _input.readBoolean();
        if (honestPartyViews_option_key_isSome) {
          BlockchainAddress honestPartyViews_option_key_option = _input.readAddress();
          honestPartyViews_option_key = honestPartyViews_option_key_option;
        }
        Bitmap honestPartyViews_option_value = null;
        var honestPartyViews_option_value_isSome = _input.readBoolean();
        if (honestPartyViews_option_value_isSome) {
          Bitmap honestPartyViews_option_value_option = deserializeBitmap(_input);
          honestPartyViews_option_value = honestPartyViews_option_value_option;
        }
        honestPartyViews_option.put(honestPartyViews_option_key, honestPartyViews_option_value);
      }
      honestPartyViews = honestPartyViews_option;
    }
    int retryNonce = _input.readI32();
    Bitmap voters = null;
    var voters_isSome = _input.readBoolean();
    if (voters_isSome) {
      Bitmap voters_option = deserializeBitmap(_input);
      voters = voters_option;
    }
    Bitmap activeProducers = null;
    var activeProducers_isSome = _input.readBoolean();
    if (activeProducers_isSome) {
      Bitmap activeProducers_option = deserializeBitmap(_input);
      activeProducers = activeProducers_option;
    }
    Map<BlockchainAddress, Bitmap> heartbeats = null;
    var heartbeats_isSome = _input.readBoolean();
    if (heartbeats_isSome) {
      var heartbeats_option_mapLength = _input.readI32();
      Map<BlockchainAddress, Bitmap> heartbeats_option = new HashMap<>();
      for (int heartbeats_option_i = 0; heartbeats_option_i < heartbeats_option_mapLength; heartbeats_option_i++) {
        BlockchainAddress heartbeats_option_key = null;
        var heartbeats_option_key_isSome = _input.readBoolean();
        if (heartbeats_option_key_isSome) {
          BlockchainAddress heartbeats_option_key_option = _input.readAddress();
          heartbeats_option_key = heartbeats_option_key_option;
        }
        Bitmap heartbeats_option_value = null;
        var heartbeats_option_value_isSome = _input.readBoolean();
        if (heartbeats_option_value_isSome) {
          Bitmap heartbeats_option_value_option = deserializeBitmap(_input);
          heartbeats_option_value = heartbeats_option_value_option;
        }
        heartbeats_option.put(heartbeats_option_key, heartbeats_option_value);
      }
      heartbeats = heartbeats_option;
    }
    Bitmap broadcasters = null;
    var broadcasters_isSome = _input.readBoolean();
    if (broadcasters_isSome) {
      Bitmap broadcasters_option = deserializeBitmap(_input);
      broadcasters = broadcasters_option;
    }
    List<BroadcastTracker> broadcastMessages = null;
    var broadcastMessages_isSome = _input.readBoolean();
    if (broadcastMessages_isSome) {
      var broadcastMessages_option_vecLength = _input.readI32();
      List<BroadcastTracker> broadcastMessages_option = new ArrayList<>();
      for (int broadcastMessages_option_i = 0; broadcastMessages_option_i < broadcastMessages_option_vecLength; broadcastMessages_option_i++) {
        BroadcastTracker broadcastMessages_option_elem = null;
        var broadcastMessages_option_elem_isSome = _input.readBoolean();
        if (broadcastMessages_option_elem_isSome) {
          BroadcastTracker broadcastMessages_option_elem_option = deserializeBroadcastTracker(_input);
          broadcastMessages_option_elem = broadcastMessages_option_elem_option;
        }
        broadcastMessages_option.add(broadcastMessages_option_elem);
      }
      broadcastMessages = broadcastMessages_option;
    }
    Hash signatureRandomization = null;
    var signatureRandomization_isSome = _input.readBoolean();
    if (signatureRandomization_isSome) {
      Hash signatureRandomization_option = _input.readHash();
      signatureRandomization = signatureRandomization_option;
    }
    return new LargeOracleUpdate(producers, thresholdKey, candidates, honestPartyViews, retryNonce, voters, activeProducers, heartbeats, broadcasters, broadcastMessages, signatureRandomization);
  }
  private BroadcastTracker deserializeBroadcastTracker(AbiInput _input) {
    Map<BlockchainAddress, Hash> messages = null;
    var messages_isSome = _input.readBoolean();
    if (messages_isSome) {
      var messages_option_mapLength = _input.readI32();
      Map<BlockchainAddress, Hash> messages_option = new HashMap<>();
      for (int messages_option_i = 0; messages_option_i < messages_option_mapLength; messages_option_i++) {
        BlockchainAddress messages_option_key = null;
        var messages_option_key_isSome = _input.readBoolean();
        if (messages_option_key_isSome) {
          BlockchainAddress messages_option_key_option = _input.readAddress();
          messages_option_key = messages_option_key_option;
        }
        Hash messages_option_value = null;
        var messages_option_value_isSome = _input.readBoolean();
        if (messages_option_value_isSome) {
          Hash messages_option_value_option = _input.readHash();
          messages_option_value = messages_option_value_option;
        }
        messages_option.put(messages_option_key, messages_option_value);
      }
      messages = messages_option;
    }
    Map<BlockchainAddress, Bitmap> bitmaps = null;
    var bitmaps_isSome = _input.readBoolean();
    if (bitmaps_isSome) {
      var bitmaps_option_mapLength = _input.readI32();
      Map<BlockchainAddress, Bitmap> bitmaps_option = new HashMap<>();
      for (int bitmaps_option_i = 0; bitmaps_option_i < bitmaps_option_mapLength; bitmaps_option_i++) {
        BlockchainAddress bitmaps_option_key = null;
        var bitmaps_option_key_isSome = _input.readBoolean();
        if (bitmaps_option_key_isSome) {
          BlockchainAddress bitmaps_option_key_option = _input.readAddress();
          bitmaps_option_key = bitmaps_option_key_option;
        }
        Bitmap bitmaps_option_value = null;
        var bitmaps_option_value_isSome = _input.readBoolean();
        if (bitmaps_option_value_isSome) {
          Bitmap bitmaps_option_value_option = deserializeBitmap(_input);
          bitmaps_option_value = bitmaps_option_value_option;
        }
        bitmaps_option.put(bitmaps_option_key, bitmaps_option_value);
      }
      bitmaps = bitmaps_option;
    }
    Bitmap echos = null;
    var echos_isSome = _input.readBoolean();
    if (echos_isSome) {
      Bitmap echos_option = deserializeBitmap(_input);
      echos = echos_option;
    }
    long startTime = _input.readI64();
    return new BroadcastTracker(messages, bitmaps, echos, startTime);
  }
  private BlockProducerStatus deserializeBlockProducerStatus(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeBlockProducerStatusWaitingForCallback(_input);
    } else if (discriminant == 1) {
      return deserializeBlockProducerStatusPending(_input);
    } else if (discriminant == 2) {
      return deserializeBlockProducerStatusConfirmed(_input);
    } else if (discriminant == 3) {
      return deserializeBlockProducerStatusPendingUpdate(_input);
    } else if (discriminant == 4) {
      return deserializeBlockProducerStatusRemoved(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private BlockProducerStatusWaitingForCallback deserializeBlockProducerStatusWaitingForCallback(AbiInput _input) {
    return new BlockProducerStatusWaitingForCallback();
  }
  private BlockProducerStatusPending deserializeBlockProducerStatusPending(AbiInput _input) {
    return new BlockProducerStatusPending();
  }
  private BlockProducerStatusConfirmed deserializeBlockProducerStatusConfirmed(AbiInput _input) {
    return new BlockProducerStatusConfirmed();
  }
  private BlockProducerStatusPendingUpdate deserializeBlockProducerStatusPendingUpdate(AbiInput _input) {
    return new BlockProducerStatusPendingUpdate();
  }
  private BlockProducerStatusRemoved deserializeBlockProducerStatusRemoved(AbiInput _input) {
    return new BlockProducerStatusRemoved();
  }
  private CommitteeSignature deserializeCommitteeSignature(AbiInput _input) {
    List<BlockProducer> committeeMembers = null;
    var committeeMembers_isSome = _input.readBoolean();
    if (committeeMembers_isSome) {
      var committeeMembers_option_vecLength = _input.readI32();
      List<BlockProducer> committeeMembers_option = new ArrayList<>();
      for (int committeeMembers_option_i = 0; committeeMembers_option_i < committeeMembers_option_vecLength; committeeMembers_option_i++) {
        BlockProducer committeeMembers_option_elem = null;
        var committeeMembers_option_elem_isSome = _input.readBoolean();
        if (committeeMembers_option_elem_isSome) {
          BlockProducer committeeMembers_option_elem_option = deserializeBlockProducer(_input);
          committeeMembers_option_elem = committeeMembers_option_elem_option;
        }
        committeeMembers_option.add(committeeMembers_option_elem);
      }
      committeeMembers = committeeMembers_option;
    }
    Signature signature = null;
    var signature_isSome = _input.readBoolean();
    if (signature_isSome) {
      Signature signature_option = _input.readSignature();
      signature = signature_option;
    }
    BlockchainPublicKey thresholdKey = null;
    var thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      BlockchainPublicKey thresholdKey_option = _input.readPublicKey();
      thresholdKey = thresholdKey_option;
    }
    return new CommitteeSignature(committeeMembers, signature, thresholdKey);
  }
  private ThresholdKey deserializeThresholdKey(AbiInput _input) {
    BlockchainPublicKey key = null;
    var key_isSome = _input.readBoolean();
    if (key_isSome) {
      BlockchainPublicKey key_option = _input.readPublicKey();
      key = key_option;
    }
    String keyId = null;
    var keyId_isSome = _input.readBoolean();
    if (keyId_isSome) {
      String keyId_option = _input.readString();
      keyId = keyId_option;
    }
    return new ThresholdKey(key, keyId);
  }
  private CandidateKey deserializeCandidateKey(AbiInput _input) {
    ThresholdKey thresholdKey = null;
    var thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      ThresholdKey thresholdKey_option = deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    int votes = _input.readI32();
    return new CandidateKey(thresholdKey, votes);
  }
  private Bitmap deserializeBitmap(AbiInput _input) {
    byte[] bits = null;
    var bits_isSome = _input.readBoolean();
    if (bits_isSome) {
      var bits_option_vecLength = _input.readI32();
      byte[] bits_option = _input.readBytes(bits_option_vecLength);
      bits = bits_option;
    }
    return new Bitmap(bits);
  }
  private BpOrchestrationContractState deserializeBpOrchestrationContractState(AbiInput _input) {
    ThresholdKey thresholdKey = null;
    var thresholdKey_isSome = _input.readBoolean();
    if (thresholdKey_isSome) {
      ThresholdKey thresholdKey_option = deserializeThresholdKey(_input);
      thresholdKey = thresholdKey_option;
    }
    List<BlockchainAddress> kycAddresses = null;
    var kycAddresses_isSome = _input.readBoolean();
    if (kycAddresses_isSome) {
      var kycAddresses_option_vecLength = _input.readI32();
      List<BlockchainAddress> kycAddresses_option = new ArrayList<>();
      for (int kycAddresses_option_i = 0; kycAddresses_option_i < kycAddresses_option_vecLength; kycAddresses_option_i++) {
        BlockchainAddress kycAddresses_option_elem = null;
        var kycAddresses_option_elem_isSome = _input.readBoolean();
        if (kycAddresses_option_elem_isSome) {
          BlockchainAddress kycAddresses_option_elem_option = _input.readAddress();
          kycAddresses_option_elem = kycAddresses_option_elem_option;
        }
        kycAddresses_option.add(kycAddresses_option_elem);
      }
      kycAddresses = kycAddresses_option;
    }
    int sessionId = _input.readI32();
    int retryNonce = _input.readI32();
    LargeOracleUpdate oracleUpdate = null;
    var oracleUpdate_isSome = _input.readBoolean();
    if (oracleUpdate_isSome) {
      LargeOracleUpdate oracleUpdate_option = deserializeLargeOracleUpdate(_input);
      oracleUpdate = oracleUpdate_option;
    }
    Map<BlockchainAddress, BlockProducer> blockProducers = null;
    var blockProducers_isSome = _input.readBoolean();
    if (blockProducers_isSome) {
      var blockProducers_option_mapLength = _input.readI32();
      Map<BlockchainAddress, BlockProducer> blockProducers_option = new HashMap<>();
      for (int blockProducers_option_i = 0; blockProducers_option_i < blockProducers_option_mapLength; blockProducers_option_i++) {
        BlockchainAddress blockProducers_option_key = null;
        var blockProducers_option_key_isSome = _input.readBoolean();
        if (blockProducers_option_key_isSome) {
          BlockchainAddress blockProducers_option_key_option = _input.readAddress();
          blockProducers_option_key = blockProducers_option_key_option;
        }
        BlockProducer blockProducers_option_value = null;
        var blockProducers_option_value_isSome = _input.readBoolean();
        if (blockProducers_option_value_isSome) {
          BlockProducer blockProducers_option_value_option = deserializeBlockProducer(_input);
          blockProducers_option_value = blockProducers_option_value_option;
        }
        blockProducers_option.put(blockProducers_option_key, blockProducers_option_value);
      }
      blockProducers = blockProducers_option;
    }
    long confirmedSinceLastCommittee = _input.readI64();
    List<BlockProducer> committee = null;
    var committee_isSome = _input.readBoolean();
    if (committee_isSome) {
      var committee_option_vecLength = _input.readI32();
      List<BlockProducer> committee_option = new ArrayList<>();
      for (int committee_option_i = 0; committee_option_i < committee_option_vecLength; committee_option_i++) {
        BlockProducer committee_option_elem = null;
        var committee_option_elem_isSome = _input.readBoolean();
        if (committee_option_elem_isSome) {
          BlockProducer committee_option_elem_option = deserializeBlockProducer(_input);
          committee_option_elem = committee_option_elem_option;
        }
        committee_option.add(committee_option_elem);
      }
      committee = committee_option;
    }
    BlockchainAddress largeOracleContract = null;
    var largeOracleContract_isSome = _input.readBoolean();
    if (largeOracleContract_isSome) {
      BlockchainAddress largeOracleContract_option = _input.readAddress();
      largeOracleContract = largeOracleContract_option;
    }
    BlockchainAddress systemUpdateContractAddress = null;
    var systemUpdateContractAddress_isSome = _input.readBoolean();
    if (systemUpdateContractAddress_isSome) {
      BlockchainAddress systemUpdateContractAddress_option = _input.readAddress();
      systemUpdateContractAddress = systemUpdateContractAddress_option;
    }
    BlockchainAddress rewardsContractAddress = null;
    var rewardsContractAddress_isSome = _input.readBoolean();
    if (rewardsContractAddress_isSome) {
      BlockchainAddress rewardsContractAddress_option = _input.readAddress();
      rewardsContractAddress = rewardsContractAddress_option;
    }
    Map<Integer, CommitteeSignature> committeeLog = null;
    var committeeLog_isSome = _input.readBoolean();
    if (committeeLog_isSome) {
      var committeeLog_option_mapLength = _input.readI32();
      Map<Integer, CommitteeSignature> committeeLog_option = new HashMap<>();
      for (int committeeLog_option_i = 0; committeeLog_option_i < committeeLog_option_mapLength; committeeLog_option_i++) {
        Integer committeeLog_option_key = null;
        var committeeLog_option_key_isSome = _input.readBoolean();
        if (committeeLog_option_key_isSome) {
          int committeeLog_option_key_option = _input.readI32();
          committeeLog_option_key = committeeLog_option_key_option;
        }
        CommitteeSignature committeeLog_option_value = null;
        var committeeLog_option_value_isSome = _input.readBoolean();
        if (committeeLog_option_value_isSome) {
          CommitteeSignature committeeLog_option_value_option = deserializeCommitteeSignature(_input);
          committeeLog_option_value = committeeLog_option_value_option;
        }
        committeeLog_option.put(committeeLog_option_key, committeeLog_option_value);
      }
      committeeLog = committeeLog_option;
    }
    Hash domainSeparator = null;
    var domainSeparator_isSome = _input.readBoolean();
    if (domainSeparator_isSome) {
      Hash domainSeparator_option = _input.readHash();
      domainSeparator = domainSeparator_option;
    }
    long broadcastRoundDelay = _input.readI64();
    return new BpOrchestrationContractState(thresholdKey, kycAddresses, sessionId, retryNonce, oracleUpdate, blockProducers, confirmedSinceLastCommittee, committee, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, committeeLog, domainSeparator, broadcastRoundDelay);
  }
  public BpOrchestrationContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeBpOrchestrationContractState(input);
  }

  private RegisterAction deserializeRegisterAction(AbiInput _input) {
    BlockProducerInformation producer = deserializeBlockProducerInformation(_input);
    BlsSignature popSignature = _input.readBlsSignature();
    return new RegisterAction(producer, popSignature);
  }

  private ConfirmBpAction deserializeConfirmBpAction(AbiInput _input) {
    BlockchainAddress bpAddress = _input.readAddress();
    return new ConfirmBpAction(bpAddress);
  }

  private UpdateInfoAction deserializeUpdateInfoAction(AbiInput _input) {
    String updatedName = _input.readString();
    String updatedWebsite = _input.readString();
    String updatedAddress = _input.readString();
    int updatedEntityJurisdiction = _input.readI32();
    int updatedServerJurisdiction = _input.readI32();
    return new UpdateInfoAction(updatedName, updatedWebsite, updatedAddress, updatedEntityJurisdiction, updatedServerJurisdiction);
  }

  private GetProducerInfoAction deserializeGetProducerInfoAction(AbiInput _input) {
    BlockchainAddress identity = _input.readAddress();
    return new GetProducerInfoAction(identity);
  }

  private AddConfirmedBpAction deserializeAddConfirmedBpAction(AbiInput _input) {
    BlockchainAddress address = _input.readAddress();
    BlockProducerInformation producer = deserializeBlockProducerInformation(_input);
    BlsSignature popSignature = _input.readBlsSignature();
    return new AddConfirmedBpAction(address, producer, popSignature);
  }

  private RemoveBpAction deserializeRemoveBpAction(AbiInput _input) {
    return new RemoveBpAction();
  }

  private AddCandidateKeyAction deserializeAddCandidateKeyAction(AbiInput _input) {
    BlockchainPublicKey candidateKey = _input.readPublicKey();
    String keyId = _input.readString();
    Bitmap bitmap = deserializeBitmap(_input);
    return new AddCandidateKeyAction(candidateKey, keyId, bitmap);
  }

  private AuthorizeNewOracleAction deserializeAuthorizeNewOracleAction(AbiInput _input) {
    Signature signature = _input.readSignature();
    return new AuthorizeNewOracleAction(signature);
  }

  private AddHeartbeatAction deserializeAddHeartbeatAction(AbiInput _input) {
    Bitmap bitmap = deserializeBitmap(_input);
    return new AddHeartbeatAction(bitmap);
  }

  private AddBroadcastHashAction deserializeAddBroadcastHashAction(AbiInput _input) {
    int sessionId = _input.readI32();
    int retryNonce = _input.readI32();
    int broadcastRound = _input.readI32();
    Hash broadcast = _input.readHash();
    return new AddBroadcastHashAction(sessionId, retryNonce, broadcastRound, broadcast);
  }

  private AddBroadcastBitsAction deserializeAddBroadcastBitsAction(AbiInput _input) {
    int sessionId = _input.readI32();
    int retryNonce = _input.readI32();
    int broadcastRound = _input.readI32();
    var bits_vecLength = _input.readI32();
    byte[] bits = _input.readBytes(bits_vecLength);
    return new AddBroadcastBitsAction(sessionId, retryNonce, broadcastRound, bits);
  }

  private PokeOngoingUpdateAction deserializePokeOngoingUpdateAction(AbiInput _input) {
    return new PokeOngoingUpdateAction();
  }

  private TriggerNewCommitteeAction deserializeTriggerNewCommitteeAction(AbiInput _input) {
    return new TriggerNewCommitteeAction();
  }

  private InitInit deserializeInitInit(AbiInput _input) {
    var kycAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> kycAddresses = new ArrayList<>();
    for (int kycAddresses_i = 0; kycAddresses_i < kycAddresses_vecLength; kycAddresses_i++) {
      BlockchainAddress kycAddresses_elem = _input.readAddress();
      kycAddresses.add(kycAddresses_elem);
    }
    var initialProducers_vecLength = _input.readI32();
    List<BlockProducer> initialProducers = new ArrayList<>();
    for (int initialProducers_i = 0; initialProducers_i < initialProducers_vecLength; initialProducers_i++) {
      BlockProducer initialProducers_elem = deserializeBlockProducer(_input);
      initialProducers.add(initialProducers_elem);
    }
    BlockchainPublicKey initialThresholdKey = _input.readPublicKey();
    String initialThresholdKeyId = _input.readString();
    BlockchainAddress largeOracleContract = _input.readAddress();
    BlockchainAddress systemUpdateContractAddress = _input.readAddress();
    BlockchainAddress rewardsContractAddress = _input.readAddress();
    Hash domainSeparator = _input.readHash();
    long broadcastRoundDelay = _input.readI64();
    return new InitInit(kycAddresses, initialProducers, initialThresholdKey, initialThresholdKeyId, largeOracleContract, systemUpdateContractAddress, rewardsContractAddress, domainSeparator, broadcastRoundDelay);
  }


  @AbiGenerated
  public record BlockProducer(String name, String address, String website, BlockchainAddress identity, int entityJurisdiction, int serverJurisdiction, BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey, long numberOfVotes, BlockProducerStatus status) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(name != null);
      if (name != null) {
        _out.writeString(name);
      }
      _out.writeBoolean(address != null);
      if (address != null) {
        _out.writeString(address);
      }
      _out.writeBoolean(website != null);
      if (website != null) {
        _out.writeString(website);
      }
      _out.writeBoolean(identity != null);
      if (identity != null) {
        _out.writeAddress(identity);
      }
      _out.writeI32(entityJurisdiction);
      _out.writeI32(serverJurisdiction);
      _out.writeBoolean(publicKey != null);
      if (publicKey != null) {
        _out.writePublicKey(publicKey);
      }
      _out.writeBoolean(blsPublicKey != null);
      if (blsPublicKey != null) {
        _out.writeBlsPublicKey(blsPublicKey);
      }
      _out.writeI64(numberOfVotes);
      _out.writeBoolean(status != null);
      if (status != null) {
        status.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record BlockProducerInformation(String name, String website, String address, BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey, int entityJurisdiction, int serverJurisdiction) {
    public void serialize(AbiOutput _out) {
      _out.writeString(name);
      _out.writeString(website);
      _out.writeString(address);
      _out.writePublicKey(publicKey);
      _out.writeBlsPublicKey(blsPublicKey);
      _out.writeI32(entityJurisdiction);
      _out.writeI32(serverJurisdiction);
    }
  }

  @AbiGenerated
  public record LargeOracleUpdate(List<BlockProducer> producers, ThresholdKey thresholdKey, List<CandidateKey> candidates, Map<BlockchainAddress, Bitmap> honestPartyViews, int retryNonce, Bitmap voters, Bitmap activeProducers, Map<BlockchainAddress, Bitmap> heartbeats, Bitmap broadcasters, List<BroadcastTracker> broadcastMessages, Hash signatureRandomization) {
  }

  @AbiGenerated
  public record BroadcastTracker(Map<BlockchainAddress, Hash> messages, Map<BlockchainAddress, Bitmap> bitmaps, Bitmap echos, long startTime) {
  }

  @AbiGenerated
  public enum BlockProducerStatusD {
    WAITING_FOR_CALLBACK(0),
    PENDING(1),
    CONFIRMED(2),
    PENDING_UPDATE(3),
    REMOVED(4),
    ;
    private final int value;
    BlockProducerStatusD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface BlockProducerStatus {
    BlockProducerStatusD discriminant();
    void serialize(AbiOutput out);
  }

  @AbiGenerated
  public record BlockProducerStatusWaitingForCallback() implements BlockProducerStatus {
    public BlockProducerStatusD discriminant() {
      return BlockProducerStatusD.WAITING_FOR_CALLBACK;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record BlockProducerStatusPending() implements BlockProducerStatus {
    public BlockProducerStatusD discriminant() {
      return BlockProducerStatusD.PENDING;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record BlockProducerStatusConfirmed() implements BlockProducerStatus {
    public BlockProducerStatusD discriminant() {
      return BlockProducerStatusD.CONFIRMED;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record BlockProducerStatusPendingUpdate() implements BlockProducerStatus {
    public BlockProducerStatusD discriminant() {
      return BlockProducerStatusD.PENDING_UPDATE;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record BlockProducerStatusRemoved() implements BlockProducerStatus {
    public BlockProducerStatusD discriminant() {
      return BlockProducerStatusD.REMOVED;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record CommitteeSignature(List<BlockProducer> committeeMembers, Signature signature, BlockchainPublicKey thresholdKey) {
  }

  @AbiGenerated
  public record ThresholdKey(BlockchainPublicKey key, String keyId) {
  }

  @AbiGenerated
  public record CandidateKey(ThresholdKey thresholdKey, int votes) {
  }

  @AbiGenerated
  public record Bitmap(byte[] bits) {
    public void serialize(AbiOutput _out) {
      _out.writeBoolean(bits != null);
      if (bits != null) {
        _out.writeI32(bits.length);
        _out.writeBytes(bits);
      }
    }
  }

  @AbiGenerated
  public record BpOrchestrationContractState(ThresholdKey thresholdKey, List<BlockchainAddress> kycAddresses, int sessionId, int retryNonce, LargeOracleUpdate oracleUpdate, Map<BlockchainAddress, BlockProducer> blockProducers, long confirmedSinceLastCommittee, List<BlockProducer> committee, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, Map<Integer, CommitteeSignature> committeeLog, Hash domainSeparator, long broadcastRoundDelay) {
    public static BpOrchestrationContractState deserialize(byte[] bytes) {
      return BpOrchestration_SDK_13_4_0.deserializeState(bytes);
    }
  }

  public static byte[] init(List<BlockchainAddress> kycAddresses, List<BlockProducer> initialProducers, BlockchainPublicKey initialThresholdKey, String initialThresholdKeyId, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, Hash domainSeparator, long broadcastRoundDelay) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(kycAddresses.size());
      for (BlockchainAddress kycAddresses_vec : kycAddresses) {
        _out.writeAddress(kycAddresses_vec);
      }
      _out.writeI32(initialProducers.size());
      for (BlockProducer initialProducers_vec : initialProducers) {
        initialProducers_vec.serialize(_out);
      }
      _out.writePublicKey(initialThresholdKey);
      _out.writeString(initialThresholdKeyId);
      _out.writeAddress(largeOracleContract);
      _out.writeAddress(systemUpdateContractAddress);
      _out.writeAddress(rewardsContractAddress);
      _out.writeHash(domainSeparator);
      _out.writeI64(broadcastRoundDelay);
    });
  }

  public static byte[] register(BlockProducerInformation producer, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("00"));
      producer.serialize(_out);
      _out.writeBlsSignature(popSignature);
    });
  }

  public static byte[] confirmBp(BlockchainAddress bpAddress) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(bpAddress);
    });
  }

  public static byte[] updateInfo(String updatedName, String updatedWebsite, String updatedAddress, int updatedEntityJurisdiction, int updatedServerJurisdiction) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeString(updatedName);
      _out.writeString(updatedWebsite);
      _out.writeString(updatedAddress);
      _out.writeI32(updatedEntityJurisdiction);
      _out.writeI32(updatedServerJurisdiction);
    });
  }

  public static byte[] getProducerInfo(BlockchainAddress identity) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("09"));
      _out.writeAddress(identity);
    });
  }

  public static byte[] addConfirmedBp(BlockchainAddress address, BlockProducerInformation producer, BlsSignature popSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0a"));
      _out.writeAddress(address);
      producer.serialize(_out);
      _out.writeBlsSignature(popSignature);
    });
  }

  public static byte[] removeBp() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0b"));
    });
  }

  public static byte[] addCandidateKey(BlockchainPublicKey candidateKey, String keyId, Bitmap bitmap) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writePublicKey(candidateKey);
      _out.writeString(keyId);
      bitmap.serialize(_out);
    });
  }

  public static byte[] authorizeNewOracle(Signature signature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeSignature(signature);
    });
  }

  public static byte[] addHeartbeat(Bitmap bitmap) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      bitmap.serialize(_out);
    });
  }

  public static byte[] addBroadcastHash(int sessionId, int retryNonce, int broadcastRound, Hash broadcast) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeI32(sessionId);
      _out.writeI32(retryNonce);
      _out.writeI32(broadcastRound);
      _out.writeHash(broadcast);
    });
  }

  public static byte[] addBroadcastBits(int sessionId, int retryNonce, int broadcastRound, byte[] bits) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0c"));
      _out.writeI32(sessionId);
      _out.writeI32(retryNonce);
      _out.writeI32(broadcastRound);
      _out.writeI32(bits.length);
      _out.writeBytes(bits);
    });
  }

  public static byte[] pokeOngoingUpdate() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("08"));
    });
  }

  public static byte[] triggerNewCommittee() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("0d"));
    });
  }

  public static BpOrchestrationContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new BpOrchestration_SDK_13_4_0(client, address).deserializeBpOrchestrationContractState(input);
  }
  
  public static BpOrchestrationContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static BpOrchestrationContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record RegisterAction(BlockProducerInformation producer, BlsSignature popSignature) implements Action {
  }
  @AbiGenerated
  public record ConfirmBpAction(BlockchainAddress bpAddress) implements Action {
  }
  @AbiGenerated
  public record UpdateInfoAction(String updatedName, String updatedWebsite, String updatedAddress, int updatedEntityJurisdiction, int updatedServerJurisdiction) implements Action {
  }
  @AbiGenerated
  public record GetProducerInfoAction(BlockchainAddress identity) implements Action {
  }
  @AbiGenerated
  public record AddConfirmedBpAction(BlockchainAddress address, BlockProducerInformation producer, BlsSignature popSignature) implements Action {
  }
  @AbiGenerated
  public record RemoveBpAction() implements Action {
  }
  @AbiGenerated
  public record AddCandidateKeyAction(BlockchainPublicKey candidateKey, String keyId, Bitmap bitmap) implements Action {
  }
  @AbiGenerated
  public record AuthorizeNewOracleAction(Signature signature) implements Action {
  }
  @AbiGenerated
  public record AddHeartbeatAction(Bitmap bitmap) implements Action {
  }
  @AbiGenerated
  public record AddBroadcastHashAction(int sessionId, int retryNonce, int broadcastRound, Hash broadcast) implements Action {
  }
  @AbiGenerated
  public record AddBroadcastBitsAction(int sessionId, int retryNonce, int broadcastRound, byte[] bits) implements Action {
  }
  @AbiGenerated
  public record PokeOngoingUpdateAction() implements Action {
  }
  @AbiGenerated
  public record TriggerNewCommitteeAction() implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new BpOrchestration_SDK_13_4_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeRegisterAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeConfirmBpAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeUpdateInfoAction(input);
    } else if (shortname.equals("09")) {
      return contract.deserializeGetProducerInfoAction(input);
    } else if (shortname.equals("0a")) {
      return contract.deserializeAddConfirmedBpAction(input);
    } else if (shortname.equals("0b")) {
      return contract.deserializeRemoveBpAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeAddCandidateKeyAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeAuthorizeNewOracleAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeAddHeartbeatAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeAddBroadcastHashAction(input);
    } else if (shortname.equals("0c")) {
      return contract.deserializeAddBroadcastBitsAction(input);
    } else if (shortname.equals("08")) {
      return contract.deserializePokeOngoingUpdateAction(input);
    } else if (shortname.equals("0d")) {
      return contract.deserializeTriggerNewCommitteeAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitInit(List<BlockchainAddress> kycAddresses, List<BlockProducer> initialProducers, BlockchainPublicKey initialThresholdKey, String initialThresholdKeyId, BlockchainAddress largeOracleContract, BlockchainAddress systemUpdateContractAddress, BlockchainAddress rewardsContractAddress, Hash domainSeparator, long broadcastRoundDelay) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new BpOrchestration_SDK_13_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
