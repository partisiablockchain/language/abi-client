// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class ByocIncoming_SDK_10_1_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeDeposit(_input: AbiInput): Deposit {
    const receiver: BlockchainAddress = _input.readAddress();
    const amount: BN = _input.readU64();
    const signaturesRegistered: number = _input.readU32();
    return { receiver, amount, signaturesRegistered };
  }
  public deserializeDispute(_input: AbiInput): Dispute {
    const challenger: BlockchainAddress = _input.readAddress();
    const claims_vecLength = _input.readI32();
    const claims: Transaction[] = [];
    for (let claims_i = 0; claims_i < claims_vecLength; claims_i++) {
      const claims_elem: Transaction = this.deserializeTransaction(_input);
      claims.push(claims_elem);
    }
    const votingResult: number = _input.readU32();
    return { challenger, claims, votingResult };
  }
  public deserializeEpoch(_input: AbiInput): Epoch {
    const fromDepositNonce: BN = _input.readU64();
    const toDepositNonce: BN = _input.readU64();
    const oracles_vecLength = _input.readI32();
    const oracles: BlockchainAddress[] = [];
    for (let oracles_i = 0; oracles_i < oracles_vecLength; oracles_i++) {
      const oracles_elem: BlockchainAddress = _input.readAddress();
      oracles.push(oracles_elem);
    }
    return { fromDepositNonce, toDepositNonce, oracles };
  }
  public deserializeTransaction(_input: AbiInput): Transaction {
    const nonce: BN = _input.readU64();
    const receiver: BlockchainAddress = _input.readAddress();
    const amount: Buffer = _input.readBytes(32);
    return { nonce, receiver, amount };
  }
  public deserializeBlockchainPublicKey(_input: AbiInput): BlockchainPublicKey {
    const ecPoint: Buffer = _input.readBytes(33);
    return { ecPoint };
  }
  public deserializeByocIncomingContractState(_input: AbiInput): ByocIncomingContractState {
    const largeOracleAddress: BlockchainAddress = _input.readAddress();
    const depositNonce: BN = _input.readU64();
    const deposits_mapLength = _input.readI32();
    const deposits: Map<BN, Deposit> = new Map();
    for (let deposits_i = 0; deposits_i < deposits_mapLength; deposits_i++) {
      const deposits_key: BN = _input.readU64();
      const deposits_value: Deposit = this.deserializeDeposit(_input);
      deposits.set(deposits_key, deposits_value);
    }
    const symbol: string = _input.readString();
    const oracleNodes_vecLength = _input.readI32();
    const oracleNodes: BlockchainAddress[] = [];
    for (let oracleNodes_i = 0; oracleNodes_i < oracleNodes_vecLength; oracleNodes_i++) {
      const oracleNodes_elem: BlockchainAddress = _input.readAddress();
      oracleNodes.push(oracleNodes_elem);
    }
    const deposit: Deposit = this.deserializeDeposit(_input);
    const depositSum: Buffer = _input.readBytes(32);
    const maximumDepositPerEpoch: Buffer = _input.readBytes(32);
    const disputes_mapLength = _input.readI32();
    const disputes: Map<BN, Dispute> = new Map();
    for (let disputes_i = 0; disputes_i < disputes_mapLength; disputes_i++) {
      const disputes_key: BN = _input.readU64();
      const disputes_value: Dispute = this.deserializeDispute(_input);
      disputes.set(disputes_key, disputes_value);
    }
    const oracleNonce: BN = _input.readU64();
    const epochs_mapLength = _input.readI32();
    const epochs: Map<BN, Epoch> = new Map();
    for (let epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      const epochs_key: BN = _input.readU64();
      const epochs_value: Epoch = this.deserializeEpoch(_input);
      epochs.set(epochs_key, epochs_value);
    }
    return { largeOracleAddress, depositNonce, deposits, symbol, oracleNodes, deposit, depositSum, maximumDepositPerEpoch, disputes, oracleNonce, epochs };
  }
  public async getState(): Promise<ByocIncomingContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeByocIncomingContractState(input);
  }

  public deserializeDepositAction(_input: AbiInput): DepositAction {
    const depositNonce: BN = _input.readU64();
    const receiver: BlockchainAddress = _input.readAddress();
    const amount: Buffer = _input.readBytes(32);
    return { discriminant: "deposit", depositNonce, receiver, amount };
  }

  public deserializeDisputeCreateAction(_input: AbiInput): DisputeCreateAction {
    const transaction: Transaction = this.deserializeTransaction(_input);
    return { discriminant: "dispute_create", transaction };
  }

  public deserializeDisputeCounterClaimAction(_input: AbiInput): DisputeCounterClaimAction {
    const transaction: Transaction = this.deserializeTransaction(_input);
    return { discriminant: "dispute_counter_claim", transaction };
  }

  public deserializeDisputeResultAction(_input: AbiInput): DisputeResultAction {
    const disputeNonce: BN = _input.readU64();
    const disputeResult: number = _input.readU32();
    return { discriminant: "dispute_result", disputeNonce, disputeResult };
  }

  public deserializeUpdateOracleAction(_input: AbiInput): UpdateOracleAction {
    const newOracles_vecLength = _input.readI32();
    const newOracles: BlockchainAddress[] = [];
    for (let newOracles_i = 0; newOracles_i < newOracles_vecLength; newOracles_i++) {
      const newOracles_elem: BlockchainAddress = _input.readAddress();
      newOracles.push(newOracles_elem);
    }
    const publicKeys_vecLength = _input.readI32();
    const publicKeys: BlockchainPublicKey[] = [];
    for (let publicKeys_i = 0; publicKeys_i < publicKeys_vecLength; publicKeys_i++) {
      const publicKeys_elem: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
      publicKeys.push(publicKeys_elem);
    }
    return { discriminant: "update_oracle", newOracles, publicKeys };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const largeOracleAddress: BlockchainAddress = _input.readAddress();
    const symbol: string = _input.readString();
    const oracleNodes_vecLength = _input.readI32();
    const oracleNodes: BlockchainAddress[] = [];
    for (let oracleNodes_i = 0; oracleNodes_i < oracleNodes_vecLength; oracleNodes_i++) {
      const oracleNodes_elem: BlockchainAddress = _input.readAddress();
      oracleNodes.push(oracleNodes_elem);
    }
    const maximumDepositPerEpoch: Buffer = _input.readBytes(32);
    return { discriminant: "init", largeOracleAddress, symbol, oracleNodes, maximumDepositPerEpoch };
  }

}
export interface Deposit {
  receiver: BlockchainAddress;
  amount: BN;
  signaturesRegistered: number;
}

export interface Dispute {
  challenger: BlockchainAddress;
  claims: Transaction[];
  votingResult: number;
}

export interface Epoch {
  fromDepositNonce: BN;
  toDepositNonce: BN;
  oracles: BlockchainAddress[];
}

export interface Transaction {
  nonce: BN;
  receiver: BlockchainAddress;
  amount: Buffer;
}
function serializeTransaction(_out: AbiOutput, _value: Transaction): void {
  const { nonce, receiver, amount } = _value;
  _out.writeU64(nonce);
  _out.writeAddress(receiver);
  if (amount.length != 32) {
    throw new Error("Length of amount does not match expected 32");
  }
  _out.writeBytes(amount);
}

export interface BlockchainPublicKey {
  ecPoint: Buffer;
}
function serializeBlockchainPublicKey(_out: AbiOutput, _value: BlockchainPublicKey): void {
  const { ecPoint } = _value;
  if (ecPoint.length != 33) {
    throw new Error("Length of ecPoint does not match expected 33");
  }
  _out.writeBytes(ecPoint);
}

export interface ByocIncomingContractState {
  largeOracleAddress: BlockchainAddress;
  depositNonce: BN;
  deposits: Map<BN, Deposit>;
  symbol: string;
  oracleNodes: BlockchainAddress[];
  deposit: Deposit;
  depositSum: Buffer;
  maximumDepositPerEpoch: Buffer;
  disputes: Map<BN, Dispute>;
  oracleNonce: BN;
  epochs: Map<BN, Epoch>;
}

export function init(largeOracleAddress: BlockchainAddress, symbol: string, oracleNodes: BlockchainAddress[], maximumDepositPerEpoch: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(largeOracleAddress);
    _out.writeString(symbol);
    _out.writeI32(oracleNodes.length);
    for (const oracleNodes_vec of oracleNodes) {
      _out.writeAddress(oracleNodes_vec);
    }
    if (maximumDepositPerEpoch.length != 32) {
      throw new Error("Length of maximumDepositPerEpoch does not match expected 32");
    }
    _out.writeBytes(maximumDepositPerEpoch);
  });
}

export function deposit(depositNonce: BN, receiver: BlockchainAddress, amount: Buffer): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("00", "hex"));
    _out.writeU64(depositNonce);
    _out.writeAddress(receiver);
    if (amount.length != 32) {
      throw new Error("Length of amount does not match expected 32");
    }
    _out.writeBytes(amount);
  });
}

export function disputeCreate(transaction: Transaction): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    serializeTransaction(_out, transaction);
  });
}

export function disputeCounterClaim(transaction: Transaction): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    serializeTransaction(_out, transaction);
  });
}

export function disputeResult(disputeNonce: BN, disputeResult: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeU64(disputeNonce);
    _out.writeU32(disputeResult);
  });
}

export function updateOracle(newOracles: BlockchainAddress[], publicKeys: BlockchainPublicKey[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeI32(newOracles.length);
    for (const newOracles_vec of newOracles) {
      _out.writeAddress(newOracles_vec);
    }
    _out.writeI32(publicKeys.length);
    for (const publicKeys_vec of publicKeys) {
      serializeBlockchainPublicKey(_out, publicKeys_vec);
    }
  });
}

export function deserializeState(state: StateWithClient): ByocIncomingContractState;
export function deserializeState(bytes: Buffer): ByocIncomingContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): ByocIncomingContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): ByocIncomingContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new ByocIncoming_SDK_10_1_0(client, address).deserializeByocIncomingContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new ByocIncoming_SDK_10_1_0(
      state.client,
      state.address
    ).deserializeByocIncomingContractState(input);
  }
}

export type Action =
  | DepositAction
  | DisputeCreateAction
  | DisputeCounterClaimAction
  | DisputeResultAction
  | UpdateOracleAction;

export interface DepositAction {
  discriminant: "deposit";
  depositNonce: BN;
  receiver: BlockchainAddress;
  amount: Buffer;
}
export interface DisputeCreateAction {
  discriminant: "dispute_create";
  transaction: Transaction;
}
export interface DisputeCounterClaimAction {
  discriminant: "dispute_counter_claim";
  transaction: Transaction;
}
export interface DisputeResultAction {
  discriminant: "dispute_result";
  disputeNonce: BN;
  disputeResult: number;
}
export interface UpdateOracleAction {
  discriminant: "update_oracle";
  newOracles: BlockchainAddress[];
  publicKeys: BlockchainPublicKey[];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocIncoming_SDK_10_1_0(undefined, undefined);
  if (shortname === "00") {
    return contract.deserializeDepositAction(input);
  } else if (shortname === "01") {
    return contract.deserializeDisputeCreateAction(input);
  } else if (shortname === "02") {
    return contract.deserializeDisputeCounterClaimAction(input);
  } else if (shortname === "03") {
    return contract.deserializeDisputeResultAction(input);
  } else if (shortname === "04") {
    return contract.deserializeUpdateOracleAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  largeOracleAddress: BlockchainAddress;
  symbol: string;
  oracleNodes: BlockchainAddress[];
  maximumDepositPerEpoch: Buffer;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new ByocIncoming_SDK_10_1_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

