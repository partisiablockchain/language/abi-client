// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ZkContract_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ZkContract_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    long ownVariableId = _input.readU64();
    return new ContractState(ownVariableId);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }

  private SomeActionAction deserializeSomeActionAction(AbiInput _input) {
    return new SomeActionAction();
  }

  private ToContractDoneAction deserializeToContractDoneAction(AbiInput _input) {
    return new ToContractDoneAction();
  }

  private StartComputationOneOutputAction deserializeStartComputationOneOutputAction(AbiInput _input) {
    return new StartComputationOneOutputAction();
  }

  private StartComputationNoOutputsAction deserializeStartComputationNoOutputsAction(AbiInput _input) {
    return new StartComputationNoOutputsAction();
  }

  private StartComputationTwoOutputsAction deserializeStartComputationTwoOutputsAction(AbiInput _input) {
    return new StartComputationTwoOutputsAction();
  }

  private NewComputationAction deserializeNewComputationAction(AbiInput _input) {
    return new NewComputationAction();
  }

  private OutputCompleteDeleteVariableAction deserializeOutputCompleteDeleteVariableAction(AbiInput _input) {
    return new OutputCompleteDeleteVariableAction();
  }

  private ContractDoneAction deserializeContractDoneAction(AbiInput _input) {
    return new ContractDoneAction();
  }

  private TransferVariableOneAction deserializeTransferVariableOneAction(AbiInput _input) {
    return new TransferVariableOneAction();
  }

  private DeleteVariableOneAction deserializeDeleteVariableOneAction(AbiInput _input) {
    return new DeleteVariableOneAction();
  }

  private DeletePendingInputOneAction deserializeDeletePendingInputOneAction(AbiInput _input) {
    return new DeletePendingInputOneAction();
  }

  private OpenVariableOneAction deserializeOpenVariableOneAction(AbiInput _input) {
    return new OpenVariableOneAction();
  }

  private DeleteVariableThreeAction deserializeDeleteVariableThreeAction(AbiInput _input) {
    return new DeleteVariableThreeAction();
  }

  private VerifyOnePendingInputAction deserializeVerifyOnePendingInputAction(AbiInput _input) {
    return new VerifyOnePendingInputAction();
  }

  private VerifyOneVariableAction deserializeVerifyOneVariableAction(AbiInput _input) {
    return new VerifyOneVariableAction();
  }

  private UpdateStateAction deserializeUpdateStateAction(AbiInput _input) {
    long currentState = _input.readU64();
    long nextState = _input.readU64();
    return new UpdateStateAction(currentState, nextState);
  }

  private AttestDataAction deserializeAttestDataAction(AbiInput _input) {
    var dataToAttest_vecLength = _input.readI32();
    byte[] dataToAttest = _input.readBytes(dataToAttest_vecLength);
    return new AttestDataAction(dataToAttest);
  }

  private OnCallbackCallback deserializeOnCallbackCallback(AbiInput _input) {
    return new OnCallbackCallback();
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    byte mode = _input.readU8();
    return new InitializeInit(mode);
  }


  @AbiGenerated
  public record ContractState(long ownVariableId) {
    public static ContractState deserialize(byte[] bytes) {
      return ZkContract_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte mode) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(mode);
    });
  }

  public static byte[] someAction() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("00"));
    });
  }

  public static byte[] toContractDone() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static byte[] startComputationOneOutput() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] startComputationNoOutputs() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0b"));
    });
  }

  public static byte[] startComputationTwoOutputs() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0c"));
    });
  }

  public static byte[] newComputation() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] outputCompleteDeleteVariable() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] contractDone() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] transferVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static byte[] deleteVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("07"));
    });
  }

  public static byte[] deletePendingInputOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0f"));
    });
  }

  public static byte[] openVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("08"));
    });
  }

  public static byte[] deleteVariableThree() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("09"));
    });
  }

  public static byte[] verifyOnePendingInput() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0a"));
    });
  }

  public static byte[] verifyOneVariable() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("10"));
    });
  }

  public static byte[] updateState(long currentState, long nextState) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0d"));
      _out.writeU64(currentState);
      _out.writeU64(nextState);
    });
  }

  public static byte[] attestData(byte[] dataToAttest) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("11"));
      _out.writeI32(dataToAttest.length);
      _out.writeBytes(dataToAttest);
    });
  }

  public static SecretInputBuilder<Integer> secretInputBitLength10() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("60"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength1010() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("61"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength1() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("62"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength1001x1() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("63"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength321010() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("64"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength3232() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("65"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength12() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("66"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLength3211() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("67"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputBitLengthOneVariadic(int bitlength) {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("68"));
      _out.writeU32(bitlength);
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Integer> secretInputSealedSingleBitWithInfo() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("69"));
    });
    Function<Integer, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI32(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static byte[] onCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ZkContract_SDK_15_4_0(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record SomeActionAction() implements Action {
  }
  @AbiGenerated
  public record ToContractDoneAction() implements Action {
  }
  @AbiGenerated
  public record StartComputationOneOutputAction() implements Action {
  }
  @AbiGenerated
  public record StartComputationNoOutputsAction() implements Action {
  }
  @AbiGenerated
  public record StartComputationTwoOutputsAction() implements Action {
  }
  @AbiGenerated
  public record NewComputationAction() implements Action {
  }
  @AbiGenerated
  public record OutputCompleteDeleteVariableAction() implements Action {
  }
  @AbiGenerated
  public record ContractDoneAction() implements Action {
  }
  @AbiGenerated
  public record TransferVariableOneAction() implements Action {
  }
  @AbiGenerated
  public record DeleteVariableOneAction() implements Action {
  }
  @AbiGenerated
  public record DeletePendingInputOneAction() implements Action {
  }
  @AbiGenerated
  public record OpenVariableOneAction() implements Action {
  }
  @AbiGenerated
  public record DeleteVariableThreeAction() implements Action {
  }
  @AbiGenerated
  public record VerifyOnePendingInputAction() implements Action {
  }
  @AbiGenerated
  public record VerifyOneVariableAction() implements Action {
  }
  @AbiGenerated
  public record UpdateStateAction(long currentState, long nextState) implements Action {
  }
  @AbiGenerated
  public record AttestDataAction(byte[] dataToAttest) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    input.readU8();
    var shortname = input.readShortnameString();
    var contract = new ZkContract_SDK_15_4_0(null, null);
    if (shortname.equals("00")) {
      return contract.deserializeSomeActionAction(input);
    } else if (shortname.equals("01")) {
      return contract.deserializeToContractDoneAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeStartComputationOneOutputAction(input);
    } else if (shortname.equals("0b")) {
      return contract.deserializeStartComputationNoOutputsAction(input);
    } else if (shortname.equals("0c")) {
      return contract.deserializeStartComputationTwoOutputsAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeNewComputationAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeOutputCompleteDeleteVariableAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeContractDoneAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeTransferVariableOneAction(input);
    } else if (shortname.equals("07")) {
      return contract.deserializeDeleteVariableOneAction(input);
    } else if (shortname.equals("0f")) {
      return contract.deserializeDeletePendingInputOneAction(input);
    } else if (shortname.equals("08")) {
      return contract.deserializeOpenVariableOneAction(input);
    } else if (shortname.equals("09")) {
      return contract.deserializeDeleteVariableThreeAction(input);
    } else if (shortname.equals("0a")) {
      return contract.deserializeVerifyOnePendingInputAction(input);
    } else if (shortname.equals("10")) {
      return contract.deserializeVerifyOneVariableAction(input);
    } else if (shortname.equals("0d")) {
      return contract.deserializeUpdateStateAction(input);
    } else if (shortname.equals("11")) {
      return contract.deserializeAttestDataAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record OnCallbackCallback() implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkContract_SDK_15_4_0(null, null);
    if (shortname.equals("40")) {
      return contract.deserializeOnCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(byte mode) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ZkContract_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
