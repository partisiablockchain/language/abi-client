// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractEnumOtherOtherOptions {

  @AbiGenerated
  public enum VehicleD {
    BICYCLE(2),
    CAR(5),
    ;
    private final int value;
    VehicleD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Vehicle {
    VehicleD discriminant();
    void serialize(AbiOutput out);
  }

  @AbiGenerated
  public record VehicleBicycle(int wheelDiameter) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.BICYCLE;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
      _out.writeI32(wheelDiameter);
    }
  }

  @AbiGenerated
  public record VehicleCar(byte engineSize, boolean supportsTrailer) implements Vehicle {
    public VehicleD discriminant() {
      return VehicleD.CAR;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
      _out.writeU8(engineSize);
      _out.writeBoolean(supportsTrailer);
    }
  }

  public static byte[] initialize(Vehicle myEnum) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      myEnum.serialize(_out);
    });
  }

  public static byte[] updateEnum(Vehicle val) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0d"));
      val.serialize(_out);
    });
  }

}
