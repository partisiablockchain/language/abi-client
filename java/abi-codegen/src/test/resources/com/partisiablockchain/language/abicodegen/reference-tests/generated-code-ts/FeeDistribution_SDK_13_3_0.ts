// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class FeeDistribution_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeSigned256(_input: AbiInput): Signed256 {
    const value: Buffer = _input.readBytes(32);
    const sign: boolean = _input.readBoolean();
    return { value, sign };
  }
  public deserializeConvertedCoin(_input: AbiInput): ConvertedCoin {
    const convertedCoins: Buffer = _input.readBytes(32);
    const convertedGasSum: Buffer = _input.readBytes(32);
    return { convertedCoins, convertedGasSum };
  }
  public deserializeEpochMetrics(_input: AbiInput): EpochMetrics {
    const signatureFrequencies_vecLength = _input.readI32();
    const signatureFrequencies: number[] = [];
    for (let signatureFrequencies_i = 0; signatureFrequencies_i < signatureFrequencies_vecLength; signatureFrequencies_i++) {
      const signatureFrequencies_elem: number = _input.readI32();
      signatureFrequencies.push(signatureFrequencies_elem);
    }
    return { signatureFrequencies };
  }
  public deserializeShardInformation(_input: AbiInput): ShardInformation {
    const notifyingBlockProducers_vecLength = _input.readI32();
    const notifyingBlockProducers: BlockchainAddress[] = [];
    for (let notifyingBlockProducers_i = 0; notifyingBlockProducers_i < notifyingBlockProducers_vecLength; notifyingBlockProducers_i++) {
      const notifyingBlockProducers_elem: BlockchainAddress = _input.readAddress();
      notifyingBlockProducers.push(notifyingBlockProducers_elem);
    }
    const metrics_mapLength = _input.readI32();
    const metrics: Map<BlockchainAddress, EpochMetrics> = new Map();
    for (let metrics_i = 0; metrics_i < metrics_mapLength; metrics_i++) {
      const metrics_key: BlockchainAddress = _input.readAddress();
      const metrics_value: EpochMetrics = this.deserializeEpochMetrics(_input);
      metrics.set(metrics_key, metrics_value);
    }
    return { notifyingBlockProducers, metrics };
  }
  public deserializeEpochInformation(_input: AbiInput): EpochInformation {
    const shards_mapLength = _input.readI32();
    const shards: Map<Option<string>, ShardInformation> = new Map();
    for (let shards_i = 0; shards_i < shards_mapLength; shards_i++) {
      let shards_key: Option<string> = undefined;
      const shards_key_isSome = _input.readBoolean();
      if (shards_key_isSome) {
        const shards_key_option: string = _input.readString();
        shards_key = shards_key_option;
      }
      const shards_value: ShardInformation = this.deserializeShardInformation(_input);
      shards.set(shards_key, shards_value);
    }
    return { shards };
  }
  public deserializeFeeDistributionContractState(_input: AbiInput): FeeDistributionContractState {
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const blockProducers_vecLength = _input.readI32();
    const blockProducers: BlockchainAddress[] = [];
    for (let blockProducers_i = 0; blockProducers_i < blockProducers_vecLength; blockProducers_i++) {
      const blockProducers_elem: BlockchainAddress = _input.readAddress();
      blockProducers.push(blockProducers_elem);
    }
    const externalCoins_vecLength = _input.readI32();
    const externalCoins: ConvertedCoin[] = [];
    for (let externalCoins_i = 0; externalCoins_i < externalCoins_vecLength; externalCoins_i++) {
      const externalCoins_elem: ConvertedCoin = this.deserializeConvertedCoin(_input);
      externalCoins.push(externalCoins_elem);
    }
    const feesToDistribute_mapLength = _input.readI32();
    const feesToDistribute: Map<BlockchainAddress, Buffer> = new Map();
    for (let feesToDistribute_i = 0; feesToDistribute_i < feesToDistribute_mapLength; feesToDistribute_i++) {
      const feesToDistribute_key: BlockchainAddress = _input.readAddress();
      const feesToDistribute_value: Buffer = _input.readBytes(32);
      feesToDistribute.set(feesToDistribute_key, feesToDistribute_value);
    }
    const epochs_mapLength = _input.readI32();
    const epochs: Map<BN, EpochInformation> = new Map();
    for (let epochs_i = 0; epochs_i < epochs_mapLength; epochs_i++) {
      const epochs_key: BN = _input.readI64();
      const epochs_value: EpochInformation = this.deserializeEpochInformation(_input);
      epochs.set(epochs_key, epochs_value);
    }
    const lastEpochCollected: BN = _input.readI64();
    const shardIds_vecLength = _input.readI32();
    const shardIds: Array<Option<string>> = [];
    for (let shardIds_i = 0; shardIds_i < shardIds_vecLength; shardIds_i++) {
      let shardIds_elem: Option<string> = undefined;
      const shardIds_elem_isSome = _input.readBoolean();
      if (shardIds_elem_isSome) {
        const shardIds_elem_option: string = _input.readString();
        shardIds_elem = shardIds_elem_option;
      }
      shardIds.push(shardIds_elem);
    }
    const gasRewardPool: Signed256 = this.deserializeSigned256(_input);
    return { bpOrchestrationContract, blockProducers, externalCoins, feesToDistribute, epochs, lastEpochCollected, shardIds, gasRewardPool };
  }
  public async getState(): Promise<FeeDistributionContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeFeeDistributionContractState(input);
  }

  public deserializeNotifyWithMetricsAction(_input: AbiInput): NotifyWithMetricsAction {
    let shard: Option<string> = undefined;
    const shard_isSome = _input.readBoolean();
    if (shard_isSome) {
      const shard_option: string = _input.readString();
      shard = shard_option;
    }
    const endedEpoch: BN = _input.readI64();
    const metrics_vecLength = _input.readI32();
    const metrics: number[] = [];
    for (let metrics_i = 0; metrics_i < metrics_vecLength; metrics_i++) {
      const metrics_elem: number = _input.readU16();
      metrics.push(metrics_elem);
    }
    return { discriminant: "notify_with_metrics", shard, endedEpoch, metrics };
  }

  public deserializeUpdateCommitteeAction(_input: AbiInput): UpdateCommitteeAction {
    const newBlockProducers_vecLength = _input.readI32();
    const newBlockProducers: BlockchainAddress[] = [];
    for (let newBlockProducers_i = 0; newBlockProducers_i < newBlockProducers_vecLength; newBlockProducers_i++) {
      const newBlockProducers_elem: BlockchainAddress = _input.readAddress();
      newBlockProducers.push(newBlockProducers_elem);
    }
    return { discriminant: "update_committee", newBlockProducers };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    const bpOrchestrationContract: BlockchainAddress = _input.readAddress();
    const shards_vecLength = _input.readI32();
    const shards: Array<Option<string>> = [];
    for (let shards_i = 0; shards_i < shards_vecLength; shards_i++) {
      let shards_elem: Option<string> = undefined;
      const shards_elem_isSome = _input.readBoolean();
      if (shards_elem_isSome) {
        const shards_elem_option: string = _input.readString();
        shards_elem = shards_elem_option;
      }
      shards.push(shards_elem);
    }
    return { discriminant: "init", bpOrchestrationContract, shards };
  }

}
export interface Signed256 {
  value: Buffer;
  sign: boolean;
}

export interface ConvertedCoin {
  convertedCoins: Buffer;
  convertedGasSum: Buffer;
}

export interface EpochMetrics {
  signatureFrequencies: number[];
}

export interface ShardInformation {
  notifyingBlockProducers: BlockchainAddress[];
  metrics: Map<BlockchainAddress, EpochMetrics>;
}

export interface EpochInformation {
  shards: Map<Option<string>, ShardInformation>;
}

export interface FeeDistributionContractState {
  bpOrchestrationContract: BlockchainAddress;
  blockProducers: BlockchainAddress[];
  externalCoins: ConvertedCoin[];
  feesToDistribute: Map<BlockchainAddress, Buffer>;
  epochs: Map<BN, EpochInformation>;
  lastEpochCollected: BN;
  shardIds: Array<Option<string>>;
  gasRewardPool: Signed256;
}

export function init(bpOrchestrationContract: BlockchainAddress, shards: Array<Option<string>>): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeAddress(bpOrchestrationContract);
    _out.writeI32(shards.length);
    for (const shards_vec of shards) {
      _out.writeBoolean(shards_vec !== undefined);
      if (shards_vec !== undefined) {
        _out.writeString(shards_vec);
      }
    }
  });
}

export function notifyWithMetrics(shard: Option<string>, endedEpoch: BN, metrics: number[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeBoolean(shard !== undefined);
    if (shard !== undefined) {
      _out.writeString(shard);
    }
    _out.writeI64(endedEpoch);
    _out.writeI32(metrics.length);
    for (const metrics_vec of metrics) {
      _out.writeU16(metrics_vec);
    }
  });
}

export function updateCommittee(newBlockProducers: BlockchainAddress[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeI32(newBlockProducers.length);
    for (const newBlockProducers_vec of newBlockProducers) {
      _out.writeAddress(newBlockProducers_vec);
    }
  });
}

export function deserializeState(state: StateWithClient): FeeDistributionContractState;
export function deserializeState(bytes: Buffer): FeeDistributionContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): FeeDistributionContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): FeeDistributionContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new FeeDistribution_SDK_13_3_0(client, address).deserializeFeeDistributionContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new FeeDistribution_SDK_13_3_0(
      state.client,
      state.address
    ).deserializeFeeDistributionContractState(input);
  }
}

export type Action =
  | NotifyWithMetricsAction
  | UpdateCommitteeAction;

export interface NotifyWithMetricsAction {
  discriminant: "notify_with_metrics";
  shard: Option<string>;
  endedEpoch: BN;
  metrics: number[];
}
export interface UpdateCommitteeAction {
  discriminant: "update_committee";
  newBlockProducers: BlockchainAddress[];
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new FeeDistribution_SDK_13_3_0(undefined, undefined);
  if (shortname === "04") {
    return contract.deserializeNotifyWithMetricsAction(input);
  } else if (shortname === "01") {
    return contract.deserializeUpdateCommitteeAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
  bpOrchestrationContract: BlockchainAddress;
  shards: Array<Option<string>>;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new FeeDistribution_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

