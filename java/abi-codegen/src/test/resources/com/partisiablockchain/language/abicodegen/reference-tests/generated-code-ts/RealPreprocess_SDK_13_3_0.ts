// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class RealPreprocess_SDK_13_3_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializePendingBatch(_input: AbiInput): PendingBatch {
    const batchType: number = _input.readU8();
    const receiver: BlockchainAddress = _input.readAddress();
    const computationEngines_vecLength = _input.readI32();
    const computationEngines: EngineInformation[] = [];
    for (let computationEngines_i = 0; computationEngines_i < computationEngines_vecLength; computationEngines_i++) {
      const computationEngines_elem: EngineInformation = this.deserializeEngineInformation(_input);
      computationEngines.push(computationEngines_elem);
    }
    const preProcessEngines_vecLength = _input.readI32();
    const preProcessEngines: EngineInformation[] = [];
    for (let preProcessEngines_i = 0; preProcessEngines_i < preProcessEngines_vecLength; preProcessEngines_i++) {
      const preProcessEngines_elem: EngineInformation = this.deserializeEngineInformation(_input);
      preProcessEngines.push(preProcessEngines_elem);
    }
    const finished: number = _input.readU8();
    const accepted: number = _input.readU8();
    return { batchType, receiver, computationEngines, preProcessEngines, finished, accepted };
  }
  public deserializeEngineInformation(_input: AbiInput): EngineInformation {
    const identity: BlockchainAddress = _input.readAddress();
    const publicKey: BlockchainPublicKey = this.deserializeBlockchainPublicKey(_input);
    return { identity, publicKey };
  }
  public deserializeBlockchainPublicKey(_input: AbiInput): BlockchainPublicKey {
    const ecPoint: Buffer = _input.readBytes(33);
    return { ecPoint };
  }
  public deserializePreProcessState(_input: AbiInput): PreProcessState {
    const nextBatchId: number = _input.readI32();
    const batches_mapLength = _input.readI32();
    const batches: Map<number, PendingBatch> = new Map();
    for (let batches_i = 0; batches_i < batches_mapLength; batches_i++) {
      const batches_key: number = _input.readI32();
      const batches_value: PendingBatch = this.deserializePendingBatch(_input);
      batches.set(batches_key, batches_value);
    }
    return { nextBatchId, batches };
  }
  public async getState(): Promise<PreProcessState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializePreProcessState(input);
  }

  public deserializeGenerateBatchV2Action(_input: AbiInput): GenerateBatchV2Action {
    const batchType: number = _input.readU8();
    const count: number = _input.readU8();
    const computationEngine0: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine1: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine2: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine3: EngineInformation = this.deserializeEngineInformation(_input);
    return { discriminant: "generate_batch_v2", batchType, count, computationEngine0, computationEngine1, computationEngine2, computationEngine3 };
  }

  public deserializeGenerateBatchV3Action(_input: AbiInput): GenerateBatchV3Action {
    const batchType: number = _input.readU8();
    const count: number = _input.readU8();
    const computationEngine0: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine1: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine2: EngineInformation = this.deserializeEngineInformation(_input);
    const computationEngine3: EngineInformation = this.deserializeEngineInformation(_input);
    return { discriminant: "generate_batch_v3", batchType, count, computationEngine0, computationEngine1, computationEngine2, computationEngine3 };
  }

  public deserializeBatchFailedAction(_input: AbiInput): BatchFailedAction {
    const batchId: number = _input.readI32();
    return { discriminant: "batch_failed", batchId };
  }

  public deserializeBatchFinishedAction(_input: AbiInput): BatchFinishedAction {
    const batchId: number = _input.readI32();
    return { discriminant: "batch_finished", batchId };
  }

  public deserializeRejectBatchAction(_input: AbiInput): RejectBatchAction {
    const batchId: number = _input.readI32();
    return { discriminant: "reject_batch", batchId };
  }

  public deserializeAcceptBatchAction(_input: AbiInput): AcceptBatchAction {
    const batchId: number = _input.readI32();
    return { discriminant: "accept_batch", batchId };
  }

  public deserializeInitInit(_input: AbiInput): InitInit {
    return { discriminant: "init",  };
  }

}
export interface PendingBatch {
  batchType: number;
  receiver: BlockchainAddress;
  computationEngines: EngineInformation[];
  preProcessEngines: EngineInformation[];
  finished: number;
  accepted: number;
}

export interface EngineInformation {
  identity: BlockchainAddress;
  publicKey: BlockchainPublicKey;
}
function serializeEngineInformation(_out: AbiOutput, _value: EngineInformation): void {
  const { identity, publicKey } = _value;
  _out.writeAddress(identity);
  serializeBlockchainPublicKey(_out, publicKey);
}

export interface BlockchainPublicKey {
  ecPoint: Buffer;
}
function serializeBlockchainPublicKey(_out: AbiOutput, _value: BlockchainPublicKey): void {
  const { ecPoint } = _value;
  if (ecPoint.length != 33) {
    throw new Error("Length of ecPoint does not match expected 33");
  }
  _out.writeBytes(ecPoint);
}

export interface PreProcessState {
  nextBatchId: number;
  batches: Map<number, PendingBatch>;
}

export function init(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
  });
}

export function generateBatchV2(batchType: number, count: number, computationEngine0: EngineInformation, computationEngine1: EngineInformation, computationEngine2: EngineInformation, computationEngine3: EngineInformation): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeU8(batchType);
    _out.writeU8(count);
    serializeEngineInformation(_out, computationEngine0);
    serializeEngineInformation(_out, computationEngine1);
    serializeEngineInformation(_out, computationEngine2);
    serializeEngineInformation(_out, computationEngine3);
  });
}

export function generateBatchV3(batchType: number, count: number, computationEngine0: EngineInformation, computationEngine1: EngineInformation, computationEngine2: EngineInformation, computationEngine3: EngineInformation): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("06", "hex"));
    _out.writeU8(batchType);
    _out.writeU8(count);
    serializeEngineInformation(_out, computationEngine0);
    serializeEngineInformation(_out, computationEngine1);
    serializeEngineInformation(_out, computationEngine2);
    serializeEngineInformation(_out, computationEngine3);
  });
}

export function batchFailed(batchId: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeI32(batchId);
  });
}

export function batchFinished(batchId: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeI32(batchId);
  });
}

export function rejectBatch(batchId: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeI32(batchId);
  });
}

export function acceptBatch(batchId: number): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeI32(batchId);
  });
}

export function deserializeState(state: StateWithClient): PreProcessState;
export function deserializeState(bytes: Buffer): PreProcessState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): PreProcessState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): PreProcessState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new RealPreprocess_SDK_13_3_0(client, address).deserializePreProcessState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new RealPreprocess_SDK_13_3_0(
      state.client,
      state.address
    ).deserializePreProcessState(input);
  }
}

export type Action =
  | GenerateBatchV2Action
  | GenerateBatchV3Action
  | BatchFailedAction
  | BatchFinishedAction
  | RejectBatchAction
  | AcceptBatchAction;

export interface GenerateBatchV2Action {
  discriminant: "generate_batch_v2";
  batchType: number;
  count: number;
  computationEngine0: EngineInformation;
  computationEngine1: EngineInformation;
  computationEngine2: EngineInformation;
  computationEngine3: EngineInformation;
}
export interface GenerateBatchV3Action {
  discriminant: "generate_batch_v3";
  batchType: number;
  count: number;
  computationEngine0: EngineInformation;
  computationEngine1: EngineInformation;
  computationEngine2: EngineInformation;
  computationEngine3: EngineInformation;
}
export interface BatchFailedAction {
  discriminant: "batch_failed";
  batchId: number;
}
export interface BatchFinishedAction {
  discriminant: "batch_finished";
  batchId: number;
}
export interface RejectBatchAction {
  discriminant: "reject_batch";
  batchId: number;
}
export interface AcceptBatchAction {
  discriminant: "accept_batch";
  batchId: number;
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RealPreprocess_SDK_13_3_0(undefined, undefined);
  if (shortname === "05") {
    return contract.deserializeGenerateBatchV2Action(input);
  } else if (shortname === "06") {
    return contract.deserializeGenerateBatchV3Action(input);
  } else if (shortname === "01") {
    return contract.deserializeBatchFailedAction(input);
  } else if (shortname === "02") {
    return contract.deserializeBatchFinishedAction(input);
  } else if (shortname === "03") {
    return contract.deserializeRejectBatchAction(input);
  } else if (shortname === "04") {
    return contract.deserializeAcceptBatchAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitInit;

export interface InitInit {
  discriminant: "init";
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new RealPreprocess_SDK_13_3_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

