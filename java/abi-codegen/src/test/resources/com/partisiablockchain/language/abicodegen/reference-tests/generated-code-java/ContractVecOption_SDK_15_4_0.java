// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractVecOption_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractVecOption_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    var myVec_vecLength = _input.readI32();
    List<Long> myVec = new ArrayList<>();
    for (int myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      Long myVec_elem = null;
      var myVec_elem_isSome = _input.readBoolean();
      if (myVec_elem_isSome) {
        long myVec_elem_option = _input.readU64();
        myVec_elem = myVec_elem_option;
      }
      myVec.add(myVec_elem);
    }
    var mySet_setLength = _input.readI32();
    List<Long> mySet = new ArrayList<>();
    for (int mySet_i = 0; mySet_i < mySet_setLength; mySet_i++) {
      Long mySet_elem = null;
      var mySet_elem_isSome = _input.readBoolean();
      if (mySet_elem_isSome) {
        long mySet_elem_option = _input.readU64();
        mySet_elem = mySet_elem_option;
      }
      mySet.add(mySet_elem);
    }
    return new ExampleContractState(myVec, mySet);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateMyVecAction deserializeUpdateMyVecAction(AbiInput _input) {
    var value_vecLength = _input.readI32();
    List<Long> value = new ArrayList<>();
    for (int value_i = 0; value_i < value_vecLength; value_i++) {
      Long value_elem = null;
      var value_elem_isSome = _input.readBoolean();
      if (value_elem_isSome) {
        long value_elem_option = _input.readU64();
        value_elem = value_elem_option;
      }
      value.add(value_elem);
    }
    return new UpdateMyVecAction(value);
  }

  private UpdateMySetAction deserializeUpdateMySetAction(AbiInput _input) {
    Long value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      long value_option = _input.readU64();
      value = value_option;
    }
    return new UpdateMySetAction(value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    var myVec_vecLength = _input.readI32();
    List<Long> myVec = new ArrayList<>();
    for (int myVec_i = 0; myVec_i < myVec_vecLength; myVec_i++) {
      Long myVec_elem = null;
      var myVec_elem_isSome = _input.readBoolean();
      if (myVec_elem_isSome) {
        long myVec_elem_option = _input.readU64();
        myVec_elem = myVec_elem_option;
      }
      myVec.add(myVec_elem);
    }
    return new InitializeInit(myVec);
  }


  @AbiGenerated
  public record ExampleContractState(List<Long> myVec, List<Long> mySet) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractVecOption_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(List<Long> myVec) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeI32(myVec.size());
      for (Long myVec_vec : myVec) {
        _out.writeBoolean(myVec_vec != null);
        if (myVec_vec != null) {
          _out.writeU64(myVec_vec);
        }
      }
    });
  }

  public static byte[] updateMyVec(List<Long> value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("9d99e6e401"));
      _out.writeI32(value.size());
      for (Long value_vec : value) {
        _out.writeBoolean(value_vec != null);
        if (value_vec != null) {
          _out.writeU64(value_vec);
        }
      }
    });
  }

  public static byte[] updateMySet(Long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("c7e1eca904"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeU64(value);
      }
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractVecOption_SDK_15_4_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateMyVecAction(List<Long> value) implements Action {
  }
  @AbiGenerated
  public record UpdateMySetAction(Long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractVecOption_SDK_15_4_0(null, null);
    if (shortname.equals("9d99e6e401")) {
      return contract.deserializeUpdateMyVecAction(input);
    } else if (shortname.equals("c7e1eca904")) {
      return contract.deserializeUpdateMySetAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(List<Long> myVec) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractVecOption_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
