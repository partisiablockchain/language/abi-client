// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class VotingContract_SDK_9_1_2 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public VotingContract_SDK_9_1_2(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private VotingContractState deserializeVotingContractState(AbiInput _input) {
    long proposalId = _input.readU64();
    var mpAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> mpAddresses = new ArrayList<>();
    for (int mpAddresses_i = 0; mpAddresses_i < mpAddresses_vecLength; mpAddresses_i++) {
      BlockchainAddress mpAddresses_elem = _input.readAddress();
      mpAddresses.add(mpAddresses_elem);
    }
    var votes_mapLength = _input.readI32();
    Map<BlockchainAddress, Byte> votes = new HashMap<>();
    for (int votes_i = 0; votes_i < votes_mapLength; votes_i++) {
      BlockchainAddress votes_key = _input.readAddress();
      byte votes_value = _input.readU8();
      votes.put(votes_key, votes_value);
    }
    byte closed = _input.readU8();
    return new VotingContractState(proposalId, mpAddresses, votes, closed);
  }
  public VotingContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeVotingContractState(input);
  }

  private VoteAction deserializeVoteAction(AbiInput _input) {
    byte vote = _input.readU8();
    return new VoteAction(vote);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    long proposalId = _input.readU64();
    var mpAddresses_vecLength = _input.readI32();
    List<BlockchainAddress> mpAddresses = new ArrayList<>();
    for (int mpAddresses_i = 0; mpAddresses_i < mpAddresses_vecLength; mpAddresses_i++) {
      BlockchainAddress mpAddresses_elem = _input.readAddress();
      mpAddresses.add(mpAddresses_elem);
    }
    return new InitializeInit(proposalId, mpAddresses);
  }


  @AbiGenerated
  public record VotingContractState(long proposalId, List<BlockchainAddress> mpAddresses, Map<BlockchainAddress, Byte> votes, byte closed) {
    public static VotingContractState deserialize(byte[] bytes) {
      return VotingContract_SDK_9_1_2.deserializeState(bytes);
    }
  }

  public static byte[] initialize(long proposalId, List<BlockchainAddress> mpAddresses) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU64(proposalId);
      _out.writeI32(mpAddresses.size());
      for (BlockchainAddress mpAddresses_vec : mpAddresses) {
        _out.writeAddress(mpAddresses_vec);
      }
    });
  }

  public static byte[] vote(byte vote) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f4889dd90a"));
      _out.writeU8(vote);
    });
  }

  public static VotingContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new VotingContract_SDK_9_1_2(client, address).deserializeVotingContractState(input);
  }
  
  public static VotingContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static VotingContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record VoteAction(byte vote) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new VotingContract_SDK_9_1_2(null, null);
    if (shortname.equals("f4889dd90a")) {
      return contract.deserializeVoteAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(long proposalId, List<BlockchainAddress> mpAddresses) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new VotingContract_SDK_9_1_2(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
