// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class LiquiditySwap_SDK_15_4_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public LiquiditySwap_SDK_15_4_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private Token deserializeToken(AbiInput _input) {
    var discriminant = _input.readU8();
    if (discriminant == 0) {
      return deserializeTokenTokenA(_input);
    } else if (discriminant == 1) {
      return deserializeTokenTokenB(_input);
    } else if (discriminant == 2) {
      return deserializeTokenLiquidityToken(_input);
    }
    throw new RuntimeException("Unknown discriminant: " + discriminant);
  }
  private TokenTokenA deserializeTokenTokenA(AbiInput _input) {
    return new TokenTokenA();
  }
  private TokenTokenB deserializeTokenTokenB(AbiInput _input) {
    return new TokenTokenB();
  }
  private TokenLiquidityToken deserializeTokenLiquidityToken(AbiInput _input) {
    return new TokenLiquidityToken();
  }
  private TokenBalance deserializeTokenBalance(AbiInput _input) {
    BigInteger aTokens = _input.readUnsignedBigInteger(16);
    BigInteger bTokens = _input.readUnsignedBigInteger(16);
    BigInteger liquidityTokens = _input.readUnsignedBigInteger(16);
    return new TokenBalance(aTokens, bTokens, liquidityTokens);
  }
  private LiquiditySwapContractState deserializeLiquiditySwapContractState(AbiInput _input) {
    BlockchainAddress contract = _input.readAddress();
    BlockchainAddress tokenAAddress = _input.readAddress();
    BlockchainAddress tokenBAddress = _input.readAddress();
    BigInteger swapFeePerMille = _input.readUnsignedBigInteger(16);
    var tokenBalances_mapLength = _input.readI32();
    Map<BlockchainAddress, TokenBalance> tokenBalances = new HashMap<>();
    for (int tokenBalances_i = 0; tokenBalances_i < tokenBalances_mapLength; tokenBalances_i++) {
      BlockchainAddress tokenBalances_key = _input.readAddress();
      TokenBalance tokenBalances_value = deserializeTokenBalance(_input);
      tokenBalances.put(tokenBalances_key, tokenBalances_value);
    }
    return new LiquiditySwapContractState(contract, tokenAAddress, tokenBAddress, swapFeePerMille, tokenBalances);
  }
  public LiquiditySwapContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeLiquiditySwapContractState(input);
  }

  private DepositAction deserializeDepositAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositAction(tokenAddress, amount);
  }

  private SwapAction deserializeSwapAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new SwapAction(tokenAddress, amount);
  }

  private WithdrawAction deserializeWithdrawAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new WithdrawAction(tokenAddress, amount);
  }

  private ProvideLiquidityAction deserializeProvideLiquidityAction(AbiInput _input) {
    BlockchainAddress tokenAddress = _input.readAddress();
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new ProvideLiquidityAction(tokenAddress, amount);
  }

  private ReclaimLiquidityAction deserializeReclaimLiquidityAction(AbiInput _input) {
    BigInteger liquidityTokenAmount = _input.readUnsignedBigInteger(16);
    return new ReclaimLiquidityAction(liquidityTokenAmount);
  }

  private ProvideInitialLiquidityAction deserializeProvideInitialLiquidityAction(AbiInput _input) {
    BigInteger tokenAAmount = _input.readUnsignedBigInteger(16);
    BigInteger tokenBAmount = _input.readUnsignedBigInteger(16);
    return new ProvideInitialLiquidityAction(tokenAAmount, tokenBAmount);
  }

  private DepositCallbackCallback deserializeDepositCallbackCallback(AbiInput _input) {
    Token token = deserializeToken(_input);
    BigInteger amount = _input.readUnsignedBigInteger(16);
    return new DepositCallbackCallback(token, amount);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    BlockchainAddress tokenAAddress = _input.readAddress();
    BlockchainAddress tokenBAddress = _input.readAddress();
    BigInteger swapFeePerMille = _input.readUnsignedBigInteger(16);
    return new InitializeInit(tokenAAddress, tokenBAddress, swapFeePerMille);
  }


  @AbiGenerated
  public enum TokenD {
    TOKEN_A(0),
    TOKEN_B(1),
    LIQUIDITY_TOKEN(2),
    ;
    private final int value;
    TokenD(int value) {
      this.value = value;
    }
  }
  @AbiGenerated
  public interface Token {
    TokenD discriminant();
    void serialize(AbiOutput out);
  }

  @AbiGenerated
  public record TokenTokenA() implements Token {
    public TokenD discriminant() {
      return TokenD.TOKEN_A;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record TokenTokenB() implements Token {
    public TokenD discriminant() {
      return TokenD.TOKEN_B;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record TokenLiquidityToken() implements Token {
    public TokenD discriminant() {
      return TokenD.LIQUIDITY_TOKEN;
    }
    public void serialize(AbiOutput _out) {
      _out.writeU8(discriminant().value);
    }
  }

  @AbiGenerated
  public record TokenBalance(BigInteger aTokens, BigInteger bTokens, BigInteger liquidityTokens) {
  }

  @AbiGenerated
  public record LiquiditySwapContractState(BlockchainAddress contract, BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress, BigInteger swapFeePerMille, Map<BlockchainAddress, TokenBalance> tokenBalances) {
    public static LiquiditySwapContractState deserialize(byte[] bytes) {
      return LiquiditySwap_SDK_15_4_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress, BigInteger swapFeePerMille) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeAddress(tokenAAddress);
      _out.writeAddress(tokenBAddress);
      _out.writeUnsignedBigInteger(swapFeePerMille, 16);
    });
  }

  public static byte[] deposit(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] swap(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] withdraw(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] provideLiquidity(BlockchainAddress tokenAddress, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeAddress(tokenAddress);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static byte[] reclaimLiquidity(BigInteger liquidityTokenAmount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeUnsignedBigInteger(liquidityTokenAmount, 16);
    });
  }

  public static byte[] provideInitialLiquidity(BigInteger tokenAAmount, BigInteger tokenBAmount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeUnsignedBigInteger(tokenAAmount, 16);
      _out.writeUnsignedBigInteger(tokenBAmount, 16);
    });
  }

  public static byte[] depositCallback(Token token, BigInteger amount) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("10"));
      token.serialize(_out);
      _out.writeUnsignedBigInteger(amount, 16);
    });
  }

  public static LiquiditySwapContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new LiquiditySwap_SDK_15_4_0(client, address).deserializeLiquiditySwapContractState(input);
  }
  
  public static LiquiditySwapContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static LiquiditySwapContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record DepositAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record SwapAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record WithdrawAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ProvideLiquidityAction(BlockchainAddress tokenAddress, BigInteger amount) implements Action {
  }
  @AbiGenerated
  public record ReclaimLiquidityAction(BigInteger liquidityTokenAmount) implements Action {
  }
  @AbiGenerated
  public record ProvideInitialLiquidityAction(BigInteger tokenAAmount, BigInteger tokenBAmount) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_15_4_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeDepositAction(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeSwapAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeWithdrawAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeProvideLiquidityAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeReclaimLiquidityAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeProvideInitialLiquidityAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Callback {
  }

  @AbiGenerated
  public record DepositCallbackCallback(Token token, BigInteger amount) implements Callback {
  }
  public static Callback deserializeCallback(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_15_4_0(null, null);
    if (shortname.equals("10")) {
      return contract.deserializeDepositCallbackCallback(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(BlockchainAddress tokenAAddress, BlockchainAddress tokenBAddress, BigInteger swapFeePerMille) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new LiquiditySwap_SDK_15_4_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
