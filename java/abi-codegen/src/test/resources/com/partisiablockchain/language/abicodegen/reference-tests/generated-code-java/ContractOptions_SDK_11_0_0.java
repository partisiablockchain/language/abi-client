// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractOptions_SDK_11_0_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractOptions_SDK_11_0_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ExampleContractState deserializeExampleContractState(AbiInput _input) {
    Long optionU64 = null;
    var optionU64_isSome = _input.readBoolean();
    if (optionU64_isSome) {
      long optionU64_option = _input.readU64();
      optionU64 = optionU64_option;
    }
    String optionString = null;
    var optionString_isSome = _input.readBoolean();
    if (optionString_isSome) {
      String optionString_option = _input.readString();
      optionString = optionString_option;
    }
    BlockchainAddress optionAddress = null;
    var optionAddress_isSome = _input.readBoolean();
    if (optionAddress_isSome) {
      BlockchainAddress optionAddress_option = _input.readAddress();
      optionAddress = optionAddress_option;
    }
    Boolean optionBoolean = null;
    var optionBoolean_isSome = _input.readBoolean();
    if (optionBoolean_isSome) {
      boolean optionBoolean_option = _input.readBoolean();
      optionBoolean = optionBoolean_option;
    }
    Map<Long, Long> optionMap = null;
    var optionMap_isSome = _input.readBoolean();
    if (optionMap_isSome) {
      var optionMap_option_mapLength = _input.readI32();
      Map<Long, Long> optionMap_option = new HashMap<>();
      for (int optionMap_option_i = 0; optionMap_option_i < optionMap_option_mapLength; optionMap_option_i++) {
        long optionMap_option_key = _input.readU64();
        long optionMap_option_value = _input.readU64();
        optionMap_option.put(optionMap_option_key, optionMap_option_value);
      }
      optionMap = optionMap_option;
    }
    return new ExampleContractState(optionU64, optionString, optionAddress, optionBoolean, optionMap);
  }
  public ExampleContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeExampleContractState(input);
  }

  private UpdateU64Action deserializeUpdateU64Action(AbiInput _input) {
    Long value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      long value_option = _input.readU64();
      value = value_option;
    }
    return new UpdateU64Action(value);
  }

  private UpdateStringAction deserializeUpdateStringAction(AbiInput _input) {
    String value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      String value_option = _input.readString();
      value = value_option;
    }
    return new UpdateStringAction(value);
  }

  private UpdateAddressAction deserializeUpdateAddressAction(AbiInput _input) {
    BlockchainAddress value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      BlockchainAddress value_option = _input.readAddress();
      value = value_option;
    }
    return new UpdateAddressAction(value);
  }

  private UpdateBooleanAction deserializeUpdateBooleanAction(AbiInput _input) {
    Boolean value = null;
    var value_isSome = _input.readBoolean();
    if (value_isSome) {
      boolean value_option = _input.readBoolean();
      value = value_option;
    }
    return new UpdateBooleanAction(value);
  }

  private AddEntryToMapAction deserializeAddEntryToMapAction(AbiInput _input) {
    long key = _input.readU64();
    long value = _input.readU64();
    return new AddEntryToMapAction(key, value);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    Long optionU64 = null;
    var optionU64_isSome = _input.readBoolean();
    if (optionU64_isSome) {
      long optionU64_option = _input.readU64();
      optionU64 = optionU64_option;
    }
    String optionString = null;
    var optionString_isSome = _input.readBoolean();
    if (optionString_isSome) {
      String optionString_option = _input.readString();
      optionString = optionString_option;
    }
    BlockchainAddress optionAddress = null;
    var optionAddress_isSome = _input.readBoolean();
    if (optionAddress_isSome) {
      BlockchainAddress optionAddress_option = _input.readAddress();
      optionAddress = optionAddress_option;
    }
    Boolean optionBoolean = null;
    var optionBoolean_isSome = _input.readBoolean();
    if (optionBoolean_isSome) {
      boolean optionBoolean_option = _input.readBoolean();
      optionBoolean = optionBoolean_option;
    }
    return new InitializeInit(optionU64, optionString, optionAddress, optionBoolean);
  }


  @AbiGenerated
  public record ExampleContractState(Long optionU64, String optionString, BlockchainAddress optionAddress, Boolean optionBoolean, Map<Long, Long> optionMap) {
    public static ExampleContractState deserialize(byte[] bytes) {
      return ContractOptions_SDK_11_0_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize(Long optionU64, String optionString, BlockchainAddress optionAddress, Boolean optionBoolean) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeBoolean(optionU64 != null);
      if (optionU64 != null) {
        _out.writeU64(optionU64);
      }
      _out.writeBoolean(optionString != null);
      if (optionString != null) {
        _out.writeString(optionString);
      }
      _out.writeBoolean(optionAddress != null);
      if (optionAddress != null) {
        _out.writeAddress(optionAddress);
      }
      _out.writeBoolean(optionBoolean != null);
      if (optionBoolean != null) {
        _out.writeBoolean(optionBoolean);
      }
    });
  }

  public static byte[] updateU64(Long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("cf9cffe90b"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeU64(value);
      }
    });
  }

  public static byte[] updateString(String value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("f3eae9b808"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeString(value);
      }
    });
  }

  public static byte[] updateAddress(BlockchainAddress value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("acabb1e901"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeAddress(value);
      }
    });
  }

  public static byte[] updateBoolean(Boolean value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("fc8ecfda06"));
      _out.writeBoolean(value != null);
      if (value != null) {
        _out.writeBoolean(value);
      }
    });
  }

  public static byte[] addEntryToMap(long key, long value) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("dbcbb3e00c"));
      _out.writeU64(key);
      _out.writeU64(value);
    });
  }

  public static ExampleContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractOptions_SDK_11_0_0(client, address).deserializeExampleContractState(input);
  }
  
  public static ExampleContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ExampleContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record UpdateU64Action(Long value) implements Action {
  }
  @AbiGenerated
  public record UpdateStringAction(String value) implements Action {
  }
  @AbiGenerated
  public record UpdateAddressAction(BlockchainAddress value) implements Action {
  }
  @AbiGenerated
  public record UpdateBooleanAction(Boolean value) implements Action {
  }
  @AbiGenerated
  public record AddEntryToMapAction(long key, long value) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptions_SDK_11_0_0(null, null);
    if (shortname.equals("cf9cffe90b")) {
      return contract.deserializeUpdateU64Action(input);
    } else if (shortname.equals("f3eae9b808")) {
      return contract.deserializeUpdateStringAction(input);
    } else if (shortname.equals("acabb1e901")) {
      return contract.deserializeUpdateAddressAction(input);
    } else if (shortname.equals("fc8ecfda06")) {
      return contract.deserializeUpdateBooleanAction(input);
    } else if (shortname.equals("dbcbb3e00c")) {
      return contract.deserializeAddEntryToMapAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit(Long optionU64, String optionString, BlockchainAddress optionAddress, Boolean optionBoolean) implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractOptions_SDK_11_0_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
