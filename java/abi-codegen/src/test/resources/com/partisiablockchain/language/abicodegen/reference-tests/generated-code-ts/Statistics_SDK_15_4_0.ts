// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class Statistics_SDK_15_4_0 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeZkOutput(_input: AbiInput): ZkOutput {
    const ageCounts: AgeCounts = this.deserializeAgeCounts(_input);
    const genderCounts: GenderCounts = this.deserializeGenderCounts(_input);
    const colorCounts: ColorCounts = this.deserializeColorCounts(_input);
    return { ageCounts, genderCounts, colorCounts };
  }
  public deserializeAgeCounts(_input: AbiInput): AgeCounts {
    const age0to19: number = _input.readI32();
    const age20to39: number = _input.readI32();
    const age40to59: number = _input.readI32();
    const age60plus: number = _input.readI32();
    return { age0to19, age20to39, age40to59, age60plus };
  }
  public deserializeGenderCounts(_input: AbiInput): GenderCounts {
    const male: number = _input.readI32();
    const female: number = _input.readI32();
    const other: number = _input.readI32();
    return { male, female, other };
  }
  public deserializeColorCounts(_input: AbiInput): ColorCounts {
    const red: number = _input.readI32();
    const blue: number = _input.readI32();
    const green: number = _input.readI32();
    const yellow: number = _input.readI32();
    return { red, blue, green, yellow };
  }
  public deserializeZkInput(_input: AbiInput): ZkInput {
    const ageChoice: number = _input.readI8();
    const genderChoice: number = _input.readI8();
    const colorChoice: number = _input.readI8();
    return { ageChoice, genderChoice, colorChoice };
  }
  public deserializeStatisticsContractState(_input: AbiInput): StatisticsContractState {
    const deadline: BN = _input.readI64();
    let result: Option<ZkOutput> = undefined;
    const result_isSome = _input.readBoolean();
    if (result_isSome) {
      const result_option: ZkOutput = this.deserializeZkOutput(_input);
      result = result_option;
    }
    return { deadline, result };
  }
  public async getState(): Promise<StatisticsContractState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeStatisticsContractState(input);
  }

  public deserializeComputeStatisticsAction(_input: AbiInput): ComputeStatisticsAction {
    return { discriminant: "compute_statistics",  };
  }

  public deserializeInitializeInit(_input: AbiInput): InitializeInit {
    const millisUntilDeadline: BN = _input.readU64();
    return { discriminant: "initialize", millisUntilDeadline };
  }

}
export interface ZkOutput {
  ageCounts: AgeCounts;
  genderCounts: GenderCounts;
  colorCounts: ColorCounts;
}

export interface AgeCounts {
  age0to19: number;
  age20to39: number;
  age40to59: number;
  age60plus: number;
}

export interface GenderCounts {
  male: number;
  female: number;
  other: number;
}

export interface ColorCounts {
  red: number;
  blue: number;
  green: number;
  yellow: number;
}

export interface ZkInput {
  ageChoice: number;
  genderChoice: number;
  colorChoice: number;
}
function serializeZkInput(_out: AbiOutput, _value: ZkInput): void {
  const { ageChoice, genderChoice, colorChoice } = _value;
  _out.writeI8(ageChoice);
  _out.writeI8(genderChoice);
  _out.writeI8(colorChoice);
}

export interface StatisticsContractState {
  deadline: BN;
  result: Option<ZkOutput>;
}

export function initialize(millisUntilDeadline: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU64(millisUntilDeadline);
  });
}

export function computeStatistics(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeU8(0x09);
    _out.writeBytes(Buffer.from("01", "hex"));
  });
}

export function addData(): SecretInputBuilder<ZkInput> {
  const _publicRpc: Buffer = AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("40", "hex"));
  });
  const _secretInput = (secret_input_lambda: ZkInput): CompactBitArray => AbiBitOutput.serialize((_out) => {
    serializeZkInput(_out, secret_input_lambda);
  });
  return new SecretInputBuilder<>(_publicRpc, _secretInput);
}

export function deserializeState(state: StateWithClient): StatisticsContractState;
export function deserializeState(bytes: Buffer): StatisticsContractState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): StatisticsContractState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): StatisticsContractState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new Statistics_SDK_15_4_0(client, address).deserializeStatisticsContractState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new Statistics_SDK_15_4_0(
      state.client,
      state.address
    ).deserializeStatisticsContractState(input);
  }
}

export type Action =
  | ComputeStatisticsAction;

export interface ComputeStatisticsAction {
  discriminant: "compute_statistics";
}
export function deserializeAction(bytes: Buffer): Action {
  const input = AbiByteInput.createBigEndian(bytes);
  input.readU8();
  const shortname = input.readShortnameString();
  const contract = new Statistics_SDK_15_4_0(undefined, undefined);
  if (shortname === "01") {
    return contract.deserializeComputeStatisticsAction(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

export type Init =
  | InitializeInit;

export interface InitializeInit {
  discriminant: "initialize";
  millisUntilDeadline: BN;
}
export function deserializeInit(bytes: Buffer): Init {
  const input = AbiByteInput.createBigEndian(bytes);
  const shortname = input.readShortnameString();
  const contract = new Statistics_SDK_15_4_0(undefined, undefined);
  if (shortname === "ffffffff0f") {
    return contract.deserializeInitializeInit(input);
  }
  throw new Error("Illegal shortname: " + shortname);
}

