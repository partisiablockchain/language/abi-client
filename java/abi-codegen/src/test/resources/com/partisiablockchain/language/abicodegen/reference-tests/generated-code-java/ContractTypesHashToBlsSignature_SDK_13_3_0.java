// This file is auto-generated from an abi-file using AbiCodegen.
package com.partisiablockchain.language.abicodegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class ContractTypesHashToBlsSignature_SDK_13_3_0 {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public ContractTypesHashToBlsSignature_SDK_13_3_0(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private InputRecord deserializeInputRecord(AbiInput _input) {
    var u256_vecLength = _input.readI32();
    List<BigInteger> u256 = new ArrayList<>();
    for (int u256_i = 0; u256_i < u256_vecLength; u256_i++) {
      BigInteger u256_elem = _input.readUnsignedBigInteger(32);
      u256.add(u256_elem);
    }
    var hash_vecLength = _input.readI32();
    List<Hash> hash = new ArrayList<>();
    for (int hash_i = 0; hash_i < hash_vecLength; hash_i++) {
      Hash hash_elem = _input.readHash();
      hash.add(hash_elem);
    }
    var publicKey_vecLength = _input.readI32();
    List<BlockchainPublicKey> publicKey = new ArrayList<>();
    for (int publicKey_i = 0; publicKey_i < publicKey_vecLength; publicKey_i++) {
      BlockchainPublicKey publicKey_elem = _input.readPublicKey();
      publicKey.add(publicKey_elem);
    }
    var signature_vecLength = _input.readI32();
    List<Signature> signature = new ArrayList<>();
    for (int signature_i = 0; signature_i < signature_vecLength; signature_i++) {
      Signature signature_elem = _input.readSignature();
      signature.add(signature_elem);
    }
    var blsPublicKey_vecLength = _input.readI32();
    List<BlsPublicKey> blsPublicKey = new ArrayList<>();
    for (int blsPublicKey_i = 0; blsPublicKey_i < blsPublicKey_vecLength; blsPublicKey_i++) {
      BlsPublicKey blsPublicKey_elem = _input.readBlsPublicKey();
      blsPublicKey.add(blsPublicKey_elem);
    }
    var blsSignature_vecLength = _input.readI32();
    List<BlsSignature> blsSignature = new ArrayList<>();
    for (int blsSignature_i = 0; blsSignature_i < blsSignature_vecLength; blsSignature_i++) {
      BlsSignature blsSignature_elem = _input.readBlsSignature();
      blsSignature.add(blsSignature_elem);
    }
    return new InputRecord(u256, hash, publicKey, signature, blsPublicKey, blsSignature);
  }
  private State deserializeState(AbiInput _input) {
    BigInteger u256 = _input.readUnsignedBigInteger(32);
    Hash hash = _input.readHash();
    BlockchainPublicKey publicKey = _input.readPublicKey();
    Signature signature = _input.readSignature();
    BlsPublicKey blsPublicKey = _input.readBlsPublicKey();
    BlsSignature blsSignature = _input.readBlsSignature();
    InputRecord typeVectors = deserializeInputRecord(_input);
    return new State(u256, hash, publicKey, signature, blsPublicKey, blsSignature, typeVectors);
  }
  public State getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeState(input);
  }

  private SetU256Action deserializeSetU256Action(AbiInput _input) {
    BigInteger u256 = _input.readUnsignedBigInteger(32);
    return new SetU256Action(u256);
  }

  private SetHashAction deserializeSetHashAction(AbiInput _input) {
    Hash hash = _input.readHash();
    return new SetHashAction(hash);
  }

  private SetPublicKeyAction deserializeSetPublicKeyAction(AbiInput _input) {
    BlockchainPublicKey publicKey = _input.readPublicKey();
    return new SetPublicKeyAction(publicKey);
  }

  private SetSignatureAction deserializeSetSignatureAction(AbiInput _input) {
    Signature signature = _input.readSignature();
    return new SetSignatureAction(signature);
  }

  private SetBlsPublicKeyAction deserializeSetBlsPublicKeyAction(AbiInput _input) {
    BlsPublicKey blsPublicKey = _input.readBlsPublicKey();
    return new SetBlsPublicKeyAction(blsPublicKey);
  }

  private SetBlsSignatureAction deserializeSetBlsSignatureAction(AbiInput _input) {
    BlsSignature blsSignature = _input.readBlsSignature();
    return new SetBlsSignatureAction(blsSignature);
  }

  private InitializeInit deserializeInitializeInit(AbiInput _input) {
    return new InitializeInit();
  }


  @AbiGenerated
  public record InputRecord(List<BigInteger> u256, List<Hash> hash, List<BlockchainPublicKey> publicKey, List<Signature> signature, List<BlsPublicKey> blsPublicKey, List<BlsSignature> blsSignature) {
  }

  @AbiGenerated
  public record State(BigInteger u256, Hash hash, BlockchainPublicKey publicKey, Signature signature, BlsPublicKey blsPublicKey, BlsSignature blsSignature, InputRecord typeVectors) {
    public static State deserialize(byte[] bytes) {
      return ContractTypesHashToBlsSignature_SDK_13_3_0.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] setU256(BigInteger u256) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeUnsignedBigInteger(u256, 32);
    });
  }

  public static byte[] setHash(Hash hash) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
      _out.writeHash(hash);
    });
  }

  public static byte[] setPublicKey(BlockchainPublicKey publicKey) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
      _out.writePublicKey(publicKey);
    });
  }

  public static byte[] setSignature(Signature signature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
      _out.writeSignature(signature);
    });
  }

  public static byte[] setBlsPublicKey(BlsPublicKey blsPublicKey) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
      _out.writeBlsPublicKey(blsPublicKey);
    });
  }

  public static byte[] setBlsSignature(BlsSignature blsSignature) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeBlsSignature(blsSignature);
    });
  }

  public static State deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new ContractTypesHashToBlsSignature_SDK_13_3_0(client, address).deserializeState(input);
  }
  
  public static State deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static State deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

  @AbiGenerated
  public interface Action {
  }

  @AbiGenerated
  public record SetU256Action(BigInteger u256) implements Action {
  }
  @AbiGenerated
  public record SetHashAction(Hash hash) implements Action {
  }
  @AbiGenerated
  public record SetPublicKeyAction(BlockchainPublicKey publicKey) implements Action {
  }
  @AbiGenerated
  public record SetSignatureAction(Signature signature) implements Action {
  }
  @AbiGenerated
  public record SetBlsPublicKeyAction(BlsPublicKey blsPublicKey) implements Action {
  }
  @AbiGenerated
  public record SetBlsSignatureAction(BlsSignature blsSignature) implements Action {
  }
  public static Action deserializeAction(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractTypesHashToBlsSignature_SDK_13_3_0(null, null);
    if (shortname.equals("01")) {
      return contract.deserializeSetU256Action(input);
    } else if (shortname.equals("02")) {
      return contract.deserializeSetHashAction(input);
    } else if (shortname.equals("03")) {
      return contract.deserializeSetPublicKeyAction(input);
    } else if (shortname.equals("04")) {
      return contract.deserializeSetSignatureAction(input);
    } else if (shortname.equals("05")) {
      return contract.deserializeSetBlsPublicKeyAction(input);
    } else if (shortname.equals("06")) {
      return contract.deserializeSetBlsSignatureAction(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

  @AbiGenerated
  public interface Init {
  }

  @AbiGenerated
  public record InitializeInit() implements Init {
  }
  public static Init deserializeInit(byte[] bytes) {
    var input = AbiByteInput.createBigEndian(bytes);
    var shortname = input.readShortnameString();
    var contract = new ContractTypesHashToBlsSignature_SDK_13_3_0(null, null);
    if (shortname.equals("ffffffff0f")) {
      return contract.deserializeInitializeInit(input);
    }
    throw new RuntimeException("Illegal shortname: " + shortname);
  }

}
