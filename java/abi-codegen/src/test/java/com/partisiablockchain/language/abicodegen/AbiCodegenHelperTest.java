package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.AvlTreeMapTypeSpec;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AbiCodegenHelperTest {

  StructTypeSpec innerStruct = new StructTypeSpec("inner", List.of());

  /** findStateStructs finds the innerStruct when it is behind a composite type. */
  @Test
  void findStateStructs() {
    assertThat(AbiCodegenHelper.findStateStructs(getAbi(new VecTypeSpec(new NamedTypeRef(0)))))
        .contains(innerStruct);
    assertThat(AbiCodegenHelper.findStateStructs(getAbi(new SetTypeSpec(new NamedTypeRef(0)))))
        .contains(innerStruct);
    assertThat(AbiCodegenHelper.findStateStructs(getAbi(new OptionTypeSpec(new NamedTypeRef(0)))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findStateStructs(
                getAbi(
                    new MapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u32), new NamedTypeRef(0)))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findStateStructs(
                getAbi(
                    new MapTypeSpec(
                        new NamedTypeRef(0), new SimpleTypeSpec(TypeSpec.TypeIndex.u32)))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findStateStructs(
                getAbi(
                    new AvlTreeMapTypeSpec(
                        new SimpleTypeSpec(TypeSpec.TypeIndex.u32), new NamedTypeRef(0)))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findStateStructs(
                getAbi(
                    new AvlTreeMapTypeSpec(
                        new NamedTypeRef(0), new SimpleTypeSpec(TypeSpec.TypeIndex.u32)))))
        .contains(innerStruct);
  }

  /**
   * findAvlKeyStructs finds the innerStruct when it is the key of an avl tree and the tree is
   * behind some composite type.
   */
  @Test
  void findAvlKeyStructs() {
    assertThat(AbiCodegenHelper.findAvlKeyStructs(getAbi(new VecTypeSpec(avlType()))))
        .contains(innerStruct);
    assertThat(AbiCodegenHelper.findAvlKeyStructs(getAbi(new SetTypeSpec(avlType()))))
        .contains(innerStruct);
    assertThat(AbiCodegenHelper.findAvlKeyStructs(getAbi(new OptionTypeSpec(avlType()))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findAvlKeyStructs(
                getAbi(new MapTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u32), avlType()))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findAvlKeyStructs(
                getAbi(new MapTypeSpec(avlType(), new SimpleTypeSpec(TypeSpec.TypeIndex.u32)))))
        .contains(innerStruct);
    assertThat(
            AbiCodegenHelper.findAvlKeyStructs(
                getAbi(
                    new AvlTreeMapTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u32), avlType()))))
        .contains(innerStruct);
  }

  /** findAvlKeyStructs finds the innerStruct when the avl tree is in some enum variant. */
  @Test
  void findAvlKeyInEnum() {
    ContractAbi abi =
        new ContractAbi(
            BinderType.Public,
            List.of(),
            List.of(
                innerStruct,
                new StructTypeSpec("state", List.of(new FieldAbi("field", new NamedTypeRef(2)))),
                new EnumTypeSpec("name", List.of(new EnumVariant(0, new NamedTypeRef(3)))),
                new StructTypeSpec("enum", List.of(new FieldAbi("f", avlType())))),
            List.of(),
            List.of(),
            new NamedTypeRef(1));
    assertThat(AbiCodegenHelper.findAvlKeyStructs(abi)).contains(innerStruct);
  }

  private ContractAbi getAbi(TypeSpec fieldType) {
    return new ContractAbi(
        BinderType.Public,
        List.of(),
        List.of(
            innerStruct, new StructTypeSpec("state", List.of(new FieldAbi("field", fieldType)))),
        List.of(),
        List.of(),
        new NamedTypeRef(1));
  }

  private TypeSpec avlType() {
    return new AvlTreeMapTypeSpec(new NamedTypeRef(0), new SimpleTypeSpec(TypeSpec.TypeIndex.u32));
  }
}
