package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * This class tests the AbiCodeGen by generating code based on a number of ABIs, and tests them
 * against a suite of reference files, to check that no unwanted changes have been made.
 */
final class CodegenReferenceTest {

  private static final String abiFilesPath =
      "src/test/resources/com/partisiablockchain/language/abicodegen/reference-tests/abi-files/";
  private static final String generatedPath =
      "src/test/resources/com/partisiablockchain/language/"
          + "abicodegen/reference-tests/generated-code";

  private static final String DEFAULT_PACKAGE_NAME = "com.partisiablockchain.language.abicodegen";

  private static final boolean OVERWRITE_REFERENCE = false;

  @Test
  void overwriteIsFalse() {
    assertThat(OVERWRITE_REFERENCE)
        .withFailMessage("Set the overwrite value to false before committing.")
        .isFalse();
  }

  @ParameterizedTest
  @MethodSource("provideTestCases")
  void checkReferences(File abiFile) {
    checkReference(abiFile, CodegenOptions.TargetLanguage.JAVA);
    checkReference(abiFile, CodegenOptions.TargetLanguage.TS);
  }

  @Test
  void testDisableAnnotation() throws IOException {
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.JAVA,
            "TransactionAndEventDeserialization",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            true,
            false,
            false,
            false,
            Set.of(),
            false);
    String abiFile = abiFilesPath + "transaction_and_event_deserialization-SDK-13.4.0.abi";

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(abiFile))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    assertThat(code).doesNotContain("@AbiGenerated");
  }

  private void checkReference(File abiFile, CodegenOptions.TargetLanguage targetLanguage) {
    String language = targetLanguage.name().toLowerCase(Locale.ENGLISH);
    String languagePath = generatedPath + "-" + language + "/";
    String name = getContractName(abiFile);
    var referencePath = Path.of(languagePath + name + "." + language);
    CodegenOptions options =
        new CodegenOptions(
            targetLanguage,
            name,
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            true,
            true,
            true,
            true,
            Set.of("SignedTransaction", "ExecutableEvent"),
            true);
    byte[] abiBytes = ExceptionConverter.call(() -> Files.readAllBytes(abiFile.toPath()));
    ContractAbi abi = new AbiParser(abiBytes).parseContractAbi();
    String generatedCode = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(referencePath, generatedCode);
  }

  static Stream<File> provideTestCases() {
    return Arrays.stream(new File(abiFilesPath).listFiles());
  }

  @Test
  void contractEnumOtherOptionsJava() throws IOException {
    var enumAbiPath = abiFilesPath + "contract_enum-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-java/ContractEnumOtherOptions.java";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.JAVA,
            "ContractEnumOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            false,
            false,
            false,
            Set.of("Vehicle"),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void tokenContractOtherOptionsJava() throws IOException {
    var enumAbiPath = abiFilesPath + "token_contract-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-java/TokenContractOtherOptions.java";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.JAVA,
            "TokenContractOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            false,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();

    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void contractEnumOtherOptionsTs() throws IOException {
    var enumAbiPath = abiFilesPath + "contract_enum-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-ts/ContractEnumOtherOptions.ts";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.TS,
            "ContractEnumOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            false,
            false,
            false,
            Set.of("Vehicle"),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void tokenContractOtherOptionsTs() throws IOException {
    var enumAbiPath = abiFilesPath + "token_contract-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-ts/TokenContractOtherOptions.ts";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.TS,
            "TokenContractOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            false,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void contractEnumOtherOtherOptionsJava() throws IOException {
    var enumAbiPath = abiFilesPath + "contract_enum-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-java/ContractEnumOtherOtherOptions.java";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.JAVA,
            "ContractEnumOtherOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            true,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void tokenContractOtherOtherOptionsJava() throws IOException {
    var enumAbiPath = abiFilesPath + "token_contract-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-java/TokenContractOtherOtherOptions.java";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.JAVA,
            "TokenContractOtherOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            true,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();

    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void contractEnumOtherOtherOptionsTs() throws IOException {
    var enumAbiPath = abiFilesPath + "contract_enum-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-ts/ContractEnumOtherOtherOptions.ts";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.TS,
            "ContractEnumOtherOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            true,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void tokenContractOtherOtherOptionsTs() throws IOException {
    var enumAbiPath = abiFilesPath + "token_contract-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-ts/TokenContractOtherOtherOptions.ts";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.TS,
            "TokenContractOtherOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            true,
            false,
            false,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseContractAbi();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  @Test
  void contractEnumDeserializeRpc() throws IOException {
    var enumAbiPath = abiFilesPath + "contract_enum-SDK-11.0.0.abi";
    var referencePath = generatedPath + "-ts/contractEnumDeserializeRpc.ts";
    CodegenOptions options =
        new CodegenOptions(
            CodegenOptions.TargetLanguage.TS,
            "ContractEnumOtherOptions",
            AbiParser.Strictness.STRICT,
            DEFAULT_PACKAGE_NAME,
            false,
            false,
            false,
            true,
            Set.of(),
            true);

    ContractAbi abi = new AbiParser(Files.readAllBytes(Path.of(enumAbiPath))).parseAbi().contract();
    String code = new AbiCodegen(abi, options).generateCode();
    compareOrUpdateReference(Path.of(referencePath), code);
  }

  static void compareOrUpdateReference(Path referencePath, String generatedCode) {
    if (OVERWRITE_REFERENCE) {
      System.out.println("Overwriting reference file " + referencePath);
      ExceptionConverter.run(() -> Files.writeString(referencePath, generatedCode));
    }
    var referenceCode =
        ExceptionConverter.call(
            () -> Files.readString(referencePath), "Could not load file: " + referencePath);

    assertThat(generatedCode).isEqualToNormalizingNewlines(referenceCode);
  }

  private static String getContractName(File file) {
    var fileName = file.toPath().getFileName().toString();
    var endIndex = fileName.lastIndexOf(".");
    var name = fileName.substring(0, endIndex);
    name =
        Arrays.stream(name.split("_"))
            .map(str -> str.substring(0, 1).toUpperCase(Locale.ENGLISH) + str.substring(1))
            .collect(Collectors.joining());
    name = name.replace("-", "_");
    name = name.replace(".", "_");
    return name;
  }
}
