package com.partisiablockchain.language.deployabi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;

@AutoSysContract(AllTypesContractState.class)
final class AllTypesContract {

  static final byte[] ENCODED_EC_POINT =
      Curve.CURVE.getG().multiply(BigInteger.valueOf(777)).getEncoded(false);
  static final BlsKeyPair BLS_KEY_PAIR = new BlsKeyPair(BigInteger.TEN);

  @Init
  public AllTypesContractState init(SafeDataInputStream rpc) {
    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));

    RecursiveClass recursiveClass = new RecursiveClass(new RecursiveClass(null, 11), 12);
    FixedList<Integer> myList = FixedList.create(List.of(1, 2, 3, 4, 5));
    AvlTree<BlockchainAddress, Integer> myMap = AvlTree.create();
    myMap =
        myMap.set(BlockchainAddress.fromString("000000000000000000000000000000000000000001"), 42);
    myMap =
        myMap.set(BlockchainAddress.fromString("000000000000000000000000000000000000000002"), 43);

    return new AllTypesContractState(
        (byte) 1,
        (byte) 2,
        (short) 3,
        (short) 4,
        5,
        6,
        7L,
        8L,
        new StateLong(9L),
        "Hello",
        new StateString("World"),
        true,
        false,
        new StateBoolean(true),
        BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
        new LargeByteArray(new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}),
        Unsigned256.create(16L),
        BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT),
        BLS_KEY_PAIR.getPublicKey(),
        BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))),
        signature,
        Hash.fromString("0400000000000000000000000000000000000000000000000000000000000004"),
        null,
        recursiveClass,
        myList,
        myMap);
  }

  @Upgrade
  public AllTypesContractState upgrade(StateAccessor oldState, SafeDataInputStream rpc) {
    return null;
  }
}
