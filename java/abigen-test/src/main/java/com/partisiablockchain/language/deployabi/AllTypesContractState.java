package com.partisiablockchain.language.deployabi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

@SuppressWarnings("FieldCanBeLocal")
@Immutable
final class AllTypesContractState implements StateSerializable {
  // Fields are sorted alphabetically
  private final byte aaByte;
  private final Byte bbByte;
  private final short ccShort;
  private final Short ddShort;
  private final int eeInt;
  private final Integer ffInt;
  private final long ggLong;
  private final Long hhLong;
  private final StateLong iiStateLong;
  private final String jjString;
  private final StateString kkStateString;
  private final boolean llBool;
  private final Boolean mmBool;
  private final StateBoolean nnStateBool;
  private final BlockchainAddress ooAddress;
  private final LargeByteArray ppLargeArray;
  private final Unsigned256 qqUnsigned256;
  private final BlockchainPublicKey rrBlockchainPublicKey;
  private final BlsPublicKey ssBlsPublicKey;
  private final BlsSignature ttBlsSignature;
  private final Signature uuSignature;
  private final Hash vvHash;
  private final String wwNullString;
  private final RecursiveClass recursiveClass;
  private final FixedList<Integer> myList;
  private final AvlTree<BlockchainAddress, Integer> myMap;

  AllTypesContractState(
      byte aaByte,
      Byte bbByte,
      short ccShort,
      Short ddShort,
      int eeInt,
      Integer ffInt,
      long ggLong,
      Long hhLong,
      StateLong iiStateLong,
      String jjString,
      StateString kkStateString,
      boolean llBool,
      Boolean mmBool,
      StateBoolean nnStateBool,
      BlockchainAddress ooAddress,
      LargeByteArray ppLargeArray,
      Unsigned256 qqUnsigned256,
      BlockchainPublicKey rrBlockchainPublicKey,
      BlsPublicKey ssBlsPublicKey,
      BlsSignature ttBlsSignature,
      Signature uuSignature,
      Hash vvHash,
      String wwNullString,
      RecursiveClass recursiveClass,
      FixedList<Integer> myList,
      AvlTree<BlockchainAddress, Integer> myMap) {
    this.aaByte = aaByte;
    this.bbByte = bbByte;
    this.ccShort = ccShort;
    this.ddShort = ddShort;
    this.eeInt = eeInt;
    this.ffInt = ffInt;
    this.ggLong = ggLong;
    this.hhLong = hhLong;
    this.iiStateLong = iiStateLong;
    this.jjString = jjString;
    this.kkStateString = kkStateString;
    this.llBool = llBool;
    this.mmBool = mmBool;
    this.nnStateBool = nnStateBool;
    this.ooAddress = ooAddress;
    this.ppLargeArray = ppLargeArray;
    this.qqUnsigned256 = qqUnsigned256;
    this.rrBlockchainPublicKey = rrBlockchainPublicKey;
    this.ssBlsPublicKey = ssBlsPublicKey;
    this.ttBlsSignature = ttBlsSignature;
    this.uuSignature = uuSignature;
    this.vvHash = vvHash;
    this.wwNullString = wwNullString;
    this.recursiveClass = recursiveClass;
    this.myList = myList;
    this.myMap = myMap;
  }

  byte aaByte() {
    return aaByte;
  }

  Byte bbByte() {
    return bbByte;
  }

  short ccShort() {
    return ccShort;
  }

  Short ddShort() {
    return ddShort;
  }

  int eeInt() {
    return eeInt;
  }

  Integer ffInt() {
    return ffInt;
  }

  long ggLong() {
    return ggLong;
  }

  Long hhLong() {
    return hhLong;
  }

  StateLong iiStateLong() {
    return iiStateLong;
  }

  String jjString() {
    return jjString;
  }

  StateString kkStateString() {
    return kkStateString;
  }

  boolean llBool() {
    return llBool;
  }

  Boolean mmBool() {
    return mmBool;
  }

  StateBoolean nnStateBool() {
    return nnStateBool;
  }

  BlockchainAddress ooAddress() {
    return ooAddress;
  }

  LargeByteArray ppLargeArray() {
    return ppLargeArray;
  }

  Unsigned256 qqUnsigned256() {
    return qqUnsigned256;
  }

  BlockchainPublicKey rrBlockchainPublicKey() {
    return rrBlockchainPublicKey;
  }

  BlsPublicKey ssBlsPublicKey() {
    return ssBlsPublicKey;
  }

  BlsSignature ttBlsSignature() {
    return ttBlsSignature;
  }

  Signature uuSignature() {
    return uuSignature;
  }

  Hash vvHash() {
    return vvHash;
  }

  String wwNullString() {
    return wwNullString;
  }

  RecursiveClass recursiveClass() {
    return recursiveClass;
  }

  FixedList<Integer> myList() {
    return myList;
  }

  AvlTree<BlockchainAddress, Integer> myMap() {
    return myMap;
  }
}
