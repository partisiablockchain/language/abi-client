package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abicodegen.TokenContract_SDK_11_0_0;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Tests for Token Contract. */
final class TokenContract_SDK_11_0_0_Test {

  @Test
  void deserializeState() {
    var owner = BlockchainAddress.fromString("007c7ec6da62ef25991f6584cdef170e457c4377c8");
    var address = BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    var expected =
        new TokenContract_SDK_11_0_0.TokenContractState(
            "Test",
            (byte) 123,
            "S",
            owner,
            4200000L,
            Map.ofEntries(Map.entry(address, 369L), Map.entry(owner, 4199631L)),
            Map.of());
    byte[] stateBytes =
        HexFormat.of()
            .parseHex(
                "04000000546573747b0100000053007c7ec6da62ef25991f6584cdef170e457c4377"
                    + "c8401640000000000002000000007c7ec6da62ef25991f6584cdef170e457c4377c8cf"
                    + "1440000000000001000000000000000000000000000000000000000071010000000000"
                    + "0000000000");
    var actual = TokenContract_SDK_11_0_0.TokenContractState.deserialize(stateBytes);
    TestingHelper.deepEquality(expected, actual);
  }

  @Test
  void bulkTransfer() {
    var expected =
        HexFormat.of()
            .parseHex(
                "020000000201000000000000000000000000000000000000000000000000000000"
                    + "7b0100000000000000000000000000000000000000010000000000000141");
    var actual =
        TokenContract_SDK_11_0_0.bulkTransfer(
            List.of(
                new TokenContract_SDK_11_0_0.Transfer(
                    BlockchainAddress.fromString("010000000000000000000000000000000000000000"),
                    123L),
                new TokenContract_SDK_11_0_0.Transfer(
                    BlockchainAddress.fromString("010000000000000000000000000000000000000001"),
                    321L)));
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void readRpc() {
    var transferList =
        List.of(
            new TokenContract_SDK_11_0_0.Transfer(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"), 123L),
            new TokenContract_SDK_11_0_0.Transfer(
                BlockchainAddress.fromString("010000000000000000000000000000000000000001"), 321L));
    var expected = new TokenContract_SDK_11_0_0.BulkTransferAction(transferList);
    var actual =
        TokenContract_SDK_11_0_0.deserializeAction(
            TokenContract_SDK_11_0_0.bulkTransfer(transferList));
    TestingHelper.deepEquality(expected, actual);
  }

  @Test
  void buildInitRpc() {
    var initBytes = TokenContract_SDK_11_0_0.initialize("Test", "T", (byte) 123, 12345L);
    var initStruct = TokenContract_SDK_11_0_0.deserializeInit(initBytes);
    var expected = new TokenContract_SDK_11_0_0.InitializeInit("Test", "T", (byte) 123, 12345L);
    TestingHelper.deepEquality(initStruct, expected);
  }
}
