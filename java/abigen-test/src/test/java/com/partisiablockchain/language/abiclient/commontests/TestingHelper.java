package com.partisiablockchain.language.abiclient.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.abiclient.rpc.RpcContractBuilder;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.stream.BigEndianByteInput;
import com.secata.stream.BigEndianByteOutput;
import com.secata.stream.LittleEndianByteOutput;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/** Helper class for testing the abi-client. */
public final class TestingHelper {

  /**
   * Helper record for reading the transactions from the blockchain.
   *
   * @param transactionPayload the payload of the transaction.
   * @param isEvent whether the executed transaction is a signed transaction or an event.
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record ExecutedTransaction(byte[] transactionPayload, boolean isEvent) {}

  /**
   * Returns the list of ExecutedTransactions located in src/test/resources/testdata/transactions.
   * The transactions were generated using the {@code DeserializeBlockchain} main Test.
   *
   * @return the list of executed transactions.
   */
  public static List<ExecutedTransaction> loadExecutedTransactions() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    Path transactionsDir = Paths.get("src/test/resources/testdata/transactions");
    List<ExecutedTransaction> transactions = new ArrayList<>();
    try (Stream<Path> paths = Files.walk(transactionsDir)) {
      List<File> files = paths.filter(Files::isRegularFile).map(Path::toFile).toList();
      for (File file : files) {
        ExecutedTransaction transaction = objectMapper.readValue(file, ExecutedTransaction.class);
        transactions.add(transaction);
      }
    } catch (Exception e) {
      throw new RuntimeException("Failed to load executed transactions");
    }
    return transactions;
  }

  /**
   * Returns an AbiParser for a specified input file.
   *
   * @param inputFile the input abi file
   * @return abi parser object
   */
  public static AbiParser loadAbiParserFromFile(String inputFile) {
    return abiParserFromBytes(readBinaryFile(inputFile));
  }

  /**
   * Returns an AbiParser from bytes.
   *
   * @param bytes The input bytes
   * @return The AbiParser
   */
  public static AbiParser abiParserFromBytes(byte[] bytes) {
    return new AbiParser(new BigEndianByteInput(new ByteArrayInputStream(bytes)));
  }

  /**
   * Returns a byte array from the contents of the specified binary test file.
   *
   * @param fileName the input file
   * @return byte array contained in file
   */
  public static byte[] readBinaryFile(String fileName) {
    try (InputStream in = Files.newInputStream(binaryPath(fileName))) {
      return in.readAllBytes();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * List binary test files.
   *
   * @return the list of files in the testdata/binary folder
   */
  public static List<Path> listBinaryFiles() {
    try (Stream<Path> stream = Files.list(Paths.get("src/test/resources/testdata/binary"))) {
      return stream.toList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * List JSON test files.
   *
   * @return the list of files in the testdata/json folder
   */
  public static List<Path> listJsonFiles() {
    try (Stream<Path> stream = Files.list(Paths.get("src/test/resources/testdata/json"))) {
      return stream.toList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static Path binaryPath(String fileName) {
    return Paths.get("src/test/resources/testdata/binary", fileName);
  }

  /**
   * Resolve the file within the JSON testdata folder.
   *
   * @param fileName the filename
   * @return the resulting path
   */
  public static Path jsonPath(String fileName) {
    return Paths.get("src/test/resources/testdata/json", fileName);
  }

  /**
   * Find the matching JSON file for the given binary file.
   *
   * @param binaryFile the binary file
   * @return the matching JSON file, may or may not exist
   */
  public static Path findMatchingJson(Path binaryFile) {
    var jsonFileName = binaryFile.getFileName().toString().replaceAll("\\.abi$", ".json");
    return jsonPath(jsonFileName);
  }

  /**
   * Gets contract abi from file.
   *
   * @param filename the file name
   * @return a contract abi
   */
  public static ContractAbi getContractAbiFromFile(String filename) {
    AbiParser parser = loadAbiParserFromFile(filename);
    FileAbi model = parser.parseAbi();
    return (ContractAbi) model.chainComponent();
  }

  /**
   * Get FnRpcBuilder from file.
   *
   * @param filename the file name
   * @param actionName the action name
   * @return an FnRpcBuilder
   */
  public static RpcContractBuilder createBuilderFromFile(String filename, String actionName) {
    return new RpcContractBuilder(getContractAbiFromFile(filename), actionName);
  }

  /**
   * Helper function to concat bytes.
   *
   * @param bytes The bytes to concat
   * @return The concatenated bytes
   */
  public static byte[] concatBytes(byte[]... bytes) {
    var out = new ByteArrayOutputStream();
    for (byte[] array : bytes) {
      out.writeBytes(array);
    }
    return out.toByteArray();
  }

  /**
   * Helper function to serialize a consumer into bytes.
   *
   * @param consumer The consumer to serialize
   * @return The output bytes
   */
  public static byte[] serialize(Consumer<BigEndianByteOutput> consumer) {
    var out = new ByteArrayOutputStream();
    consumer.accept(new BigEndianByteOutput(out));
    return out.toByteArray();
  }

  /**
   * Helper function to turn a hex string into a byte array.
   *
   * @param hex The hex string
   * @return The output byte array
   */
  public static byte[] bytesFromHex(String hex) {
    return HexFormat.of().parseHex(hex);
  }

  /**
   * Helper function to turn a string into a byte array. The length of the string is encoded as a
   * big-endian integer and put in front.
   *
   * @param string The input string
   * @return The output bytes
   */
  public static byte[] bytesFromStringBe(String string) {
    return serialize(out -> out.writeString(string));
  }

  /**
   * Helper function to get the payload from an executed transaction.
   *
   * @param executedTransaction the transaction to get the payload from.
   * @return The payload bytes
   */
  public static byte[] transactionPayloadFromExecutedTransaction(
      ExecutedTransaction executedTransaction) {
    return executedTransaction.transactionPayload();
  }

  /**
   * Helper function to serialize a consumer into bytes under little endian.
   *
   * @param consumer The consumer to serialize
   * @return The output bytes
   */
  public static byte[] serializeLe(Consumer<LittleEndianByteOutput> consumer) {
    var out = new ByteArrayOutputStream();
    consumer.accept(new LittleEndianByteOutput(out));
    return out.toByteArray();
  }

  /**
   * Helper function to turn a string into a byte array. The length of the string is encoded as a
   * little-endian integer and put in front.
   *
   * @param string The input string
   * @return The output bytes
   */
  public static byte[] bytesFromStringLe(String string) {
    return serializeLe(out -> out.writeString(string));
  }

  /**
   * Helper function for equality when objects require equals overridden.
   *
   * @param expected expected object
   * @param actual actual object
   */
  public static void deepEquality(Object expected, Object actual) {
    assert expected.equals(actual);
  }
}
