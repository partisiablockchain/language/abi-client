package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abicodegen.TestBlockchainStateClient;
import com.partisiablockchain.language.abicodegen.TokenV2;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Tests for Token V2 Contract. */
final class TokenV2Test {

  @Test
  void deserializeState() {
    var owner = BlockchainAddress.fromString("007c7ec6da62ef25991f6584cdef170e457c4377c8");
    var expected =
        new TokenV2.TokenState(
            "Test",
            (byte) 123,
            "S",
            owner,
            BigInteger.valueOf(4200000L),
            new AvlTreeMap<>(0),
            new AvlTreeMap<>(1));
    byte[] stateBytes =
        HexFormat.of()
            .parseHex(
                "04000000546573747b0100000053007c7ec6da62ef25991f6584cdef170e457c4377"
                    + "c8401640000000000000000000000000000000000001000000");
    var actual = TokenV2.deserializeState(stateBytes);
    TestingHelper.deepEquality(expected, actual);
  }

  @Test
  void deserializeStateAvl() {
    byte[] stateBytes =
        HexFormat.of()
            .parseHex(
                "04000000546573747b0100000053007c7ec6da62ef25991f6584cdef170e457c4377"
                    + "c8401640000000000000000000000000000000000001000000");
    TestBlockchainStateClient client = new TestBlockchainStateClient(stateBytes);
    var address = BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    TokenV2 contract = new TokenV2(client, address);
    TokenV2.TokenState actual = contract.getState();

    var owner = BlockchainAddress.fromString("007c7ec6da62ef25991f6584cdef170e457c4377c8");
    TokenV2.TokenState expected =
        new TokenV2.TokenState(
            "Test",
            (byte) 123,
            "S",
            owner,
            BigInteger.valueOf(4200000L),
            new AvlTreeMap<>(0),
            new AvlTreeMap<>(1));

    TestingHelper.deepEquality(expected, actual);
    assertThat(actual.balances().size()).isEqualTo(1);
    assertThat(actual.balances().get(owner)).isEqualTo(BigInteger.valueOf(42));
    assertThat(actual.balances().getNextN(null, 1))
        .containsExactly(Map.entry(address, BigInteger.ZERO));
  }

  @Test
  void bulkTransfer() {
    var expected =
        HexFormat.of()
            .parseHex(
                "0200000002"
                    + "0100000000000000000000000000000000000000"
                    + "000000000000000000000000000000007b"
                    + "010000000000000000000000000000000000000001"
                    + "00000000000000000000000000000141");
    var actual =
        TokenV2.bulkTransfer(
            List.of(
                new TokenV2.Transfer(
                    BlockchainAddress.fromString("010000000000000000000000000000000000000000"),
                    BigInteger.valueOf(123L)),
                new TokenV2.Transfer(
                    BlockchainAddress.fromString("010000000000000000000000000000000000000001"),
                    BigInteger.valueOf(321L))));
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void readRpc() {
    var transferList =
        List.of(
            new TokenV2.Transfer(
                BlockchainAddress.fromString("010000000000000000000000000000000000000000"),
                BigInteger.valueOf(123L)),
            new TokenV2.Transfer(
                BlockchainAddress.fromString("010000000000000000000000000000000000000001"),
                BigInteger.valueOf(321L)));
    var expected = new TokenV2.BulkTransferAction(transferList);
    var actual = TokenV2.deserializeAction(TokenV2.bulkTransfer(transferList));
    TestingHelper.deepEquality(expected, actual);
  }

  @Test
  void buildInitRpc() {
    var initBytes = TokenV2.initialize("Test", "T", (byte) 123, BigInteger.valueOf(12345L));
    var initStruct = TokenV2.deserializeInit(initBytes);
    var expected = new TokenV2.InitializeInit("Test", "T", (byte) 123, BigInteger.valueOf(12345L));
    TestingHelper.deepEquality(initStruct, expected);
  }
}
