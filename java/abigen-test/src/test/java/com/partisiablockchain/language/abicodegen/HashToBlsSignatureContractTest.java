package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

final class HashToBlsSignatureContractTest {
  private final KeyPair keyPair = new KeyPair(BigInteger.valueOf(52234));
  private final BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(13213));
  private final String hashString =
      "beefcafebeefcafebeefcafebeefcafebeefcafebeefcafebeefcafebeefcafe";

  @Test
  void deserializeState() {
    final BigInteger u256 = BigInteger.valueOf(42);
    final Hash hash = Hash.fromString(hashString);
    final BlockchainPublicKey publicKey = keyPair.getPublic();
    final Signature signature = keyPair.sign(hash);
    final BlsPublicKey blsPublicKey = blsKeyPair.getPublicKey();
    final BlsSignature blsSignature = blsKeyPair.sign(hash);
    final ContractTypesHashToBlsSignature_SDK_13_4_0.InputRecord emptyInputRecordStruct =
        new ContractTypesHashToBlsSignature_SDK_13_4_0.InputRecord(
            List.of(), List.of(), List.of(), List.of(), List.of(), List.of());

    ContractTypesHashToBlsSignature_SDK_13_4_0.State expected =
        new ContractTypesHashToBlsSignature_SDK_13_4_0.State(
            u256, hash, publicKey, signature, blsPublicKey, blsSignature, emptyInputRecordStruct);

    final String publicKeyString =
        HexFormat.of().formatHex(SafeDataOutputStream.serialize(publicKey::write));
    final String signatureString =
        HexFormat.of().formatHex(SafeDataOutputStream.serialize(signature::write));
    final String blsPublicKeyString =
        HexFormat.of().formatHex(SafeDataOutputStream.serialize(blsPublicKey::write));
    final String blsSignatureString =
        HexFormat.of().formatHex(SafeDataOutputStream.serialize(blsSignature::write));
    final String emptyInputRecordStructString =
        "000000000000000000000000000000000000000000000000"; // 6 empty vectors (24 bytes)
    final byte[] stateBytes =
        HexFormat.of()
            .parseHex(
                "2A00000000000000000000000000000000000000000000000000000000000000" // u256
                    + hashString
                    + publicKeyString
                    + signatureString
                    + blsPublicKeyString
                    + blsSignatureString
                    + emptyInputRecordStructString);

    assertThat(ContractTypesHashToBlsSignature_SDK_13_4_0.deserializeState(stateBytes))
        .isEqualTo(expected);
  }

  @Test
  void setU256() {
    final BigInteger u256 = BigInteger.valueOf(42);
    byte[] setU256Rpc = ContractTypesHashToBlsSignature_SDK_13_4_0.setU256(u256);
    String setU256Shortname = "01";
    byte[] expected =
        HexFormat.of()
            .parseHex(
                setU256Shortname
                    + "000000000000000000000000000000000000000000000000000000000000002A");
    assertThat(setU256Rpc).isEqualTo(expected);
  }

  @Test
  void setHash() {
    final Hash hash = Hash.fromString(hashString);
    byte[] setHashRpc = ContractTypesHashToBlsSignature_SDK_13_4_0.setHash(hash);
    String setHashShortname = "02";
    byte[] expected = HexFormat.of().parseHex(setHashShortname + hashString);
    assertThat(setHashRpc).isEqualTo(expected);
  }

  @Test
  void setPublicKey() {
    final BlockchainPublicKey publicKey = keyPair.getPublic();
    byte[] setPublicKeyRpc = ContractTypesHashToBlsSignature_SDK_13_4_0.setPublicKey(publicKey);
    String setPublicKeyShortname = "03";
    byte[] expected =
        HexFormat.of()
            .parseHex(
                setPublicKeyShortname
                    + HexFormat.of().formatHex(SafeDataOutputStream.serialize(publicKey::write)));
    assertThat(setPublicKeyRpc).isEqualTo(expected);
  }

  @Test
  void setSignature() {
    final Signature signature = keyPair.sign(Hash.fromString(hashString));
    byte[] setSignatureRpc = ContractTypesHashToBlsSignature_SDK_13_4_0.setSignature(signature);
    String setSignatureShortname = "04";
    byte[] expected =
        HexFormat.of()
            .parseHex(
                setSignatureShortname
                    + HexFormat.of().formatHex(SafeDataOutputStream.serialize(signature::write)));
    assertThat(setSignatureRpc).isEqualTo(expected);
  }

  @Test
  void setBlsPublicKey() {
    final BlsPublicKey blsPublicKey = blsKeyPair.getPublicKey();
    byte[] setBlsPublicKeyRpc =
        ContractTypesHashToBlsSignature_SDK_13_4_0.setBlsPublicKey(blsPublicKey);
    String setBlsPublicKeyShortname = "05";
    byte[] expected =
        HexFormat.of()
            .parseHex(
                setBlsPublicKeyShortname
                    + HexFormat.of()
                        .formatHex(SafeDataOutputStream.serialize(blsPublicKey::write)));
    assertThat(setBlsPublicKeyRpc).isEqualTo(expected);
  }

  @Test
  void setBlsSignature() {
    final BlsSignature blsSignature = blsKeyPair.sign(Hash.fromString(hashString));
    byte[] setBlsSignatureRpc =
        ContractTypesHashToBlsSignature_SDK_13_4_0.setBlsSignature(blsSignature);
    String setBlsSignatureShortname = "06";
    byte[] expected =
        HexFormat.of()
            .parseHex(
                setBlsSignatureShortname
                    + HexFormat.of()
                        .formatHex(SafeDataOutputStream.serialize(blsSignature::write)));
    assertThat(setBlsSignatureRpc).isEqualTo(expected);
  }
}
