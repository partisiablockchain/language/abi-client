package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abicodegen.ContractRecursiveTypes_SDK_11_0_0;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Tests for Contract Recursive Types. */
final class ContractRecursiveTypes_SDK_11_0_0_Test {
  @Test
  void rpcUpdateSelfRecursive() {
    var actual =
        ContractRecursiveTypes_SDK_11_0_0.updateSelfRecursive(
            new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(
                List.of(
                    new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of()),
                    new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of()))));
    var expected = HexFormat.of().parseHex("dcc3fe0f000000020000000000000000");
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void deserializeState() {
    var expected =
        new ContractRecursiveTypes_SDK_11_0_0.ExampleContractState(
            new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(
                List.of(
                    new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of()),
                    new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of()))),
            new ContractRecursiveTypes_SDK_11_0_0.MutualOne(
                new ContractRecursiveTypes_SDK_11_0_0.MutualTwo(
                    List.of(
                        new ContractRecursiveTypes_SDK_11_0_0.MutualOne(
                            new ContractRecursiveTypes_SDK_11_0_0.MutualTwo(List.of()))))));
    var actual =
        ContractRecursiveTypes_SDK_11_0_0.deserializeState(
            HexFormat.of().parseHex("0200000000000000000000000100000000000000"));
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void deserializeRpc() {
    var struct =
        new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(
            List.of(
                new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of()),
                new ContractRecursiveTypes_SDK_11_0_0.SelfRecursiveType(List.of())));
    var expected = new ContractRecursiveTypes_SDK_11_0_0.UpdateSelfRecursiveAction(struct);
    var actual =
        ContractRecursiveTypes_SDK_11_0_0.deserializeAction(
            ContractRecursiveTypes_SDK_11_0_0.updateSelfRecursive(struct));
    assertThat(actual).isEqualTo(expected);
  }
}
