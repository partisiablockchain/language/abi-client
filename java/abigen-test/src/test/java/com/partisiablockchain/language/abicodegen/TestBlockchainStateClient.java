package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import java.util.List;
import java.util.Map;

/** Test implementation of {@link BlockchainStateClient}. */
public final class TestBlockchainStateClient implements BlockchainStateClient {
  private final byte[] state;

  /**
   * Create new {@link TestBlockchainStateClient}.
   *
   * @param state state to return
   */
  public TestBlockchainStateClient(byte[] state) {
    this.state = state;
  }

  @Override
  public byte[] getContractStateBinary(BlockchainAddress address) {
    return state;
  }

  @Override
  public byte[] getContractStateAvlValue(BlockchainAddress address, int treeId, byte[] key) {
    byte[] bytes = new byte[16];
    bytes[0] = 42;
    return bytes;
  }

  @Override
  public List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n) {
    return List.of(Map.entry(new byte[21], new byte[32]));
  }

  @Override
  public int getContractStateAvlSize(BlockchainAddress address, int treeId) {
    return 1;
  }
}
