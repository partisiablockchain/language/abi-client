package com.partisiablockchain.language.deployabi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.server.rest.StateAbiSerializer;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

final class AllTypesContractStateTest {

  @Test
  void serializeDeserialize() {
    AllTypesContractState initState = new AllTypesContract().init(null);
    byte[] stateBytes = StateAbiSerializer.serialize(initState);
    AllTypes.AllTypesContractState genState = AllTypes.deserializeState(stateBytes);
    assertThat(initState.aaByte()).isEqualTo(genState.aaByte());
    assertThat(initState.bbByte()).isEqualTo(genState.bbByte());
    assertThat(initState.ccShort()).isEqualTo(genState.ccShort());
    assertThat(initState.ddShort()).isEqualTo(genState.ddShort());
    assertThat(initState.eeInt()).isEqualTo(genState.eeInt());
    assertThat(initState.ffInt()).isEqualTo(genState.ffInt());
    assertThat(initState.ggLong()).isEqualTo(genState.ggLong());
    assertThat(initState.hhLong()).isEqualTo(genState.hhLong());
    assertThat(initState.iiStateLong().value()).isEqualTo(genState.iiStateLong());
    assertThat(initState.jjString()).isEqualTo(genState.jjString());
    assertThat(initState.kkStateString().value()).isEqualTo(genState.kkStateString());
    assertThat(initState.llBool()).isEqualTo(genState.llBool());
    assertThat(initState.mmBool()).isEqualTo(genState.mmBool());
    assertThat(initState.nnStateBool().value()).isEqualTo(genState.nnStateBool());
    assertThat(initState.ooAddress()).isEqualTo(genState.ooAddress());
    assertThat(initState.ppLargeArray().getData()).isEqualTo(genState.ppLargeArray());
    assertThat(new BigInteger(initState.qqUnsigned256().serialize()))
        .isEqualTo(genState.qqUnsigned256());
    assertThat(initState.rrBlockchainPublicKey()).isEqualTo(genState.rrBlockchainPublicKey());
    assertThat(initState.ssBlsPublicKey()).isEqualTo(genState.ssBlsPublicKey());
    assertThat(initState.ttBlsSignature()).isEqualTo(genState.ttBlsSignature());
    assertThat(initState.uuSignature()).isEqualTo(genState.uuSignature());
    assertThat(initState.vvHash()).isEqualTo(genState.vvHash());
    assertThat(initState.wwNullString()).isEqualTo(genState.wwNullString());

    assertThat(initState.recursiveClass().myInt()).isEqualTo(genState.recursiveClass().myInt());
    assertThat(initState.recursiveClass().recursive().myInt())
        .isEqualTo(genState.recursiveClass().recursive().myInt());
    assertThat(initState.recursiveClass().recursive().recursive()).isNull();
    assertThat(genState.recursiveClass().recursive().recursive()).isNull();

    assertThat(initState.myList().size()).isEqualTo(genState.myList().size());
    for (int i = 0; i < initState.myList().size(); i++) {
      assertThat(initState.myList().get(i)).isEqualTo(genState.myList().get(i));
    }

    var key1 = BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    var key2 = BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    assertThat(initState.myMap().getValue(key1)).isEqualTo(genState.myMap().get(key1));
    assertThat(initState.myMap().getValue(key2)).isEqualTo(genState.myMap().get(key2));

    // Pitest
    assertThat(initState.ttBlsSignature())
        .isEqualTo(AllTypesContract.BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))));
  }

  @Test
  void coverage() {
    assertThat(new AllTypesContract().upgrade(null, null)).isNull();

    var pitestState =
        new AllTypesContractState(
            (byte) 1,
            (byte) 2,
            (short) 3,
            (short) 4,
            5,
            6,
            7L,
            8L,
            new StateLong(9L),
            "Hello",
            new StateString("World"),
            false,
            true,
            new StateBoolean(true),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null);

    assertThat(pitestState.llBool()).isFalse();
    assertThat(pitestState.mmBool()).isTrue();
  }
}
