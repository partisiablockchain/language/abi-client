package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.builder.StructProducer;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.secata.stream.BigEndianByteInput;
import com.secata.stream.BigEndianByteOutput;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.function.Consumer;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class EventDeserializationSdk13Test {

  private static final String HEADER =
      "0100000006536861726430c739dcad5257ba5d7aa209261a7d92690bd8c0edef871b6402ea3bcee130ced1";

  private static final String FOOTER =
      "006df142b1c3f820ffc07df2ab7358cad2202da18"
          + "f00000000000000000104fe17d1009372c8ed3ac5b790b32e349359c2c7e90000"
          + "001e03010000000653686172643000000000000c23b000040029003c0047004d0"
          + "1000000065368617264310000000000003fb10000000000000001000000000000"
          + "00050100";

  private FileAbi abi;

  @BeforeEach
  void setUp() {
    AbiParser abiParser =
        new AbiParser(
            new BigEndianByteInput(
                getClass().getResourceAsStream("/transaction_and_event_deserialization.abi")),
            AbiParser.Strictness.LENIENT);
    abi = abiParser.parseAbi();
  }

  @Test
  void testDeserializeCallback() {
    byte[] expectedRpc = Hex.decode("BEEFBABE");

    byte[] bytes =
        buildRpc(
            "CallbackToContract",
            builder -> {
              builder.addAddress("019999999999999999999999999999999999999999");
              builder.addHash("9999999999999999999999999999999999999999999999999999999999999999");
              builder.addAddress("012222222222222222222222222222222222222222");
              builder.addI64(1732);
              builder.addVecU8(expectedRpc);
            });

    var payload =
        Hex.decode(
            HEADER
                + "01" // Callback
                + Hex.toHexString(bytes)
                + FOOTER);

    var event =
        TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(payload);
    assertThat(event.originShard()).isEqualTo("Shard0");
    TransactionAndEventDeserialization_SDK_13_4_0.InnerEvent innerEvent =
        event.eventTransaction().innerEvent();
    assertThat(innerEvent.discriminant())
        .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerEventD.CALLBACK);
    TransactionAndEventDeserialization_SDK_13_4_0.InnerEventCallback callback =
        (TransactionAndEventDeserialization_SDK_13_4_0.InnerEventCallback) innerEvent;
    assertThat(callback.callbackToContract().callbackRpc()).containsExactly(expectedRpc);
  }

  @Test
  void testDeserializeSync() {
    byte[] bytes =
        buildRpc(
            "SyncEvent",
            builder -> {
              AbstractBuilder accountTransferBuilder = builder.addVec().addStruct();
              accountTransferBuilder.addAddress("019999999999999999999999999999999999999999");
              accountTransferBuilder.addI64(16);

              AbstractBuilder contractTransferBuilder = builder.addVec().addStruct();
              contractTransferBuilder.addAddress("021111111111111111111111111111111111111111");
              // state hash
              contractTransferBuilder.addHash(
                  "9999999999999999999999999999999999999999999999999999999999999999");
              // plugin state
              contractTransferBuilder.addHash(
                  "5555555555555555555555555555555555555555555555555555555555555555");

              // empty 'stateStorage'
              builder.addVec();
            });
    var payload =
        Hex.decode(
            HEADER
                + "03" // Sync
                + Hex.toHexString(bytes)
                + FOOTER);

    var event =
        TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(payload);
    TransactionAndEventDeserialization_SDK_13_4_0.InnerEvent innerEvent =
        event.eventTransaction().innerEvent();
    assertThat(innerEvent.discriminant())
        .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerEventD.SYNC);
    TransactionAndEventDeserialization_SDK_13_4_0.InnerEventSync sync =
        (TransactionAndEventDeserialization_SDK_13_4_0.InnerEventSync) innerEvent;
    TransactionAndEventDeserialization_SDK_13_4_0.SyncEvent syncEvent = sync.syncEvent();
    assertThat(syncEvent.accounts()).hasSize(1);

    assertThat(syncEvent.accounts().get(0).address().writeAsString())
        .isEqualTo("019999999999999999999999999999999999999999");
    assertThat(syncEvent.accounts().get(0).nonce()).isEqualTo(16);

    List<TransactionAndEventDeserialization_SDK_13_4_0.ContractTransfer> contracts =
        syncEvent.contracts();
    assertThat(contracts).hasSize(1);
    TransactionAndEventDeserialization_SDK_13_4_0.ContractTransfer transfer = contracts.get(0);
    assertThat(transfer.address().writeAsString())
        .isEqualTo("021111111111111111111111111111111111111111");

    assertThat(transfer.contractStateHash().toString())
        .isEqualTo("9999999999999999999999999999999999999999999999999999999999999999");
    assertThat(transfer.pluginStateHash().toString())
        .isEqualTo("5555555555555555555555555555555555555555555555555555555555555555");
  }

  @Test
  void testDeserializeCreateShard() {
    TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCreateShard wrapped =
        deserializeSystemEvent(
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.CREATE_SHARD,
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCreateShard.class,
            "CreateShard",
            builder -> {
              builder.addStruct().addString("Shard9");
            });

    var event = wrapped.createShardEvent();
    assertThat(event.shardId()).isEqualTo("Shard9");
  }

  @Test
  void testDeserializeRemoveShard() {
    TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventRemoveShard wrapped =
        deserializeSystemEvent(
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.REMOVE_SHARD,
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventRemoveShard.class,
            "RemoveShard",
            builder -> {
              builder.addStruct().addString("Shard9");
            });

    var event = wrapped.removeShardEvent();
    assertThat(event.shardId()).isEqualTo("Shard9");
  }

  @Test
  void testCreateAccount() {
    TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCreateAccount wrapped =
        deserializeSystemEvent(
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.CREATE_ACCOUNT,
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCreateAccount.class,
            "CreateAccount",
            builder -> {
              builder.addStruct().addAddress("019999999999999999999999999999999999999999");
            });

    var event = wrapped.createAccountEvent();
    assertThat(event.toCreate().writeAsString())
        .isEqualTo("019999999999999999999999999999999999999999");
  }

  @Test
  void testRemoveContract() {
    TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventRemoveContract wrapped =
        deserializeSystemEvent(
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.REMOVE_CONTRACT,
            TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventRemoveContract.class,
            "RemoveContract",
            builder -> {
              builder.addStruct().addAddress("029999999999999999999999999999999999999999");
            });

    var event = wrapped.removeContract();
    assertThat(event.contract().writeAsString())
        .isEqualTo("029999999999999999999999999999999999999999");
  }

  @Test
  void updateLocalPluginState() {
    byte[] expectedRpc = Hex.decode("BEEFBABE");

    TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpdateContextFreePluginState
        wrapped =
            deserializeSystemEvent(
                TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD
                    .UPDATE_CONTEXT_FREE_PLUGIN_STATE,
                TransactionAndEventDeserialization_SDK_13_4_0
                    .InnerSystemEventUpdateContextFreePluginState.class,
                "UpdateContextFreePluginState",
                builder -> {
                  AbstractBuilder struct = builder.addStruct();
                  struct.addEnumVariant(2);
                  struct.addVecU8(expectedRpc);
                });
    var event = wrapped.updateContextFreePluginState();
    assertThat(event.chainPluginType().discriminant())
        .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginTypeD.ROUTING);
    assertThat(event.rpc()).containsExactly(expectedRpc);
  }

  <T> T deserializeSystemEvent(
      TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD eventDiscriminant,
      Class<T> innerEventClass,
      String structName,
      Consumer<AbstractBuilder> buildRpc) {
    byte[] byteArray = buildRpc(structName, buildRpc);

    @SuppressWarnings("EnumOrdinal")
    var payload =
        Hex.decode(
            HEADER
                + "02" // System
                + Hex.toHexString(new byte[] {(byte) eventDiscriminant.ordinal()})
                + Hex.toHexString(byteArray)
                + FOOTER);

    var event =
        TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(payload);
    assertThat(event.originShard()).isEqualTo("Shard0");
    TransactionAndEventDeserialization_SDK_13_4_0.InnerEvent innerEvent =
        event.eventTransaction().innerEvent();
    assertThat(innerEvent.discriminant())
        .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerEventD.SYSTEM);

    var sys = (TransactionAndEventDeserialization_SDK_13_4_0.InnerEventSystem) innerEvent;
    assertThat(sys.innerSystemEvent().discriminant()).isEqualTo(eventDiscriminant);
    return innerEventClass.cast(sys.innerSystemEvent());
  }

  private byte[] buildRpc(String structName, Consumer<AbstractBuilder> buildRpc) {
    NamedTypeSpec innerEventSpec = abi.chainComponent().getNamedType(structName);

    StructProducer producer = new StructProducer((StructTypeSpec) innerEventSpec, "base");
    AbstractBuilder builder =
        new AbstractBuilder(((ContractAbi) abi.chainComponent()).namedTypes(), "/", producer) {};

    buildRpc.accept(builder);

    ByteArrayOutputStream buf = new ByteArrayOutputStream();
    producer.write(new AbiByteOutput(new BigEndianByteOutput(buf)));
    return buf.toByteArray();
  }
}
