package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abiclient.commontests.TestingHelper;
import com.partisiablockchain.language.abicodegen.ContractOptions_SDK_11_0_0;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Tests for Option Contract. */
final class ContractOptions_SDK_11_0_0_Test {
  @Test
  void deserializeState() {
    var expected =
        new ContractOptions_SDK_11_0_0.ExampleContractState(
            0x42L,
            null, // undefined
            null, // undefined
            null, // undefined
            null // undefined
            );
    var actual =
        ContractOptions_SDK_11_0_0.deserializeState(
            HexFormat.of().parseHex("01420000000000000000000000"));
    TestingHelper.deepEquality(expected, actual);
  }

  @Test
  void rpcOptionsSome() {
    var expected = HexFormat.of().parseHex("cf9cffe90b010000000000000042");
    var actual = ContractOptions_SDK_11_0_0.updateU64(0x42L);
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void rpcOptionsNone() {
    var expected = HexFormat.of().parseHex("cf9cffe90b00");
    var actual =
        ContractOptions_SDK_11_0_0.updateU64(
            null // undefined
            );
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void deserializeRpc() {
    var option = 0x42L;
    var expected = new ContractOptions_SDK_11_0_0.UpdateU64Action(option);
    var actual =
        ContractOptions_SDK_11_0_0.deserializeAction(ContractOptions_SDK_11_0_0.updateU64(option));
    TestingHelper.deepEquality(expected, actual);
  }
}
