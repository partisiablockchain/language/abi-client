package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abicodegen.ContractEnum_SDK_11_0_0;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Tests for Enum Contract. */
public final class ContractEnum_SDK_11_0_0_Test {

  @Test
  void deserializeState() {
    var expected =
        new ContractEnum_SDK_11_0_0.EnumContractState(
            new ContractEnum_SDK_11_0_0.VehicleCar((byte) 1, false));
    var stateBytes = HexFormat.of().parseHex("050100");
    var actual = ContractEnum_SDK_11_0_0.deserializeState(stateBytes);
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void updateCar() {
    var bytes =
        ContractEnum_SDK_11_0_0.updateEnum(new ContractEnum_SDK_11_0_0.VehicleCar((byte) 1, false));
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("ffffffff0d050100"));
  }

  @Test
  void updateBike() {
    var bytes =
        ContractEnum_SDK_11_0_0.updateEnum(new ContractEnum_SDK_11_0_0.VehicleBicycle(0x42));
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("ffffffff0d0200000042"));
  }

  @Test
  void deserializeRpc() {
    var car = new ContractEnum_SDK_11_0_0.VehicleCar((byte) 1, false);
    var expected = new ContractEnum_SDK_11_0_0.UpdateEnumAction(car);
    var actual = ContractEnum_SDK_11_0_0.deserializeAction(ContractEnum_SDK_11_0_0.updateEnum(car));
    assertThat(actual).isEqualTo(expected);
  }
}
