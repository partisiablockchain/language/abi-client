package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Tests for TransactionAndEventDeserialization, comparing results against reference files. The
 * 'transaction-inputs' folder holds a collection of transactions from <a
 * href="https://testnet.partisiablockchain.com/">the testnet</a>. The 'reference-outputs' folder
 * holds the generated references which the inputs files are compared against. To add new test
 * cases, create a new transaction file of the transaction which needs to be tested, place it under
 * the 'transaction-inputs' folder and regenerate the reference files by running the main method.
 */
public final class TransactionAndEventDeserializationReferenceTest {

  /** Path of input transactions directory. */
  private static final String TRANSACTION_INPUTS =
      "src/test/resources/com/partisiablockchain/language/abicodegen/transaction-inputs/";

  /** Path of reference output root folder. */
  private static final String REFERENCE_OUTPUTS =
      "src/test/resources/com/partisiablockchain/language/abicodegen/reference-outputs/";

  /** Path of event references. */
  private static final String REFERENCE_EVENTS = REFERENCE_OUTPUTS + "events/";

  /** Path of transaction references. */
  private static final String REFERENCE_TRANSACTIONS = REFERENCE_OUTPUTS + "transactions/";

  /** Json file extension. */
  private static final String JSON_EXT = ".json";

  private static final ObjectMapper stateObjectMapper = StateObjectMapper.createObjectMapper();
  private static final ObjectWriter writer = stateObjectMapper.writerWithDefaultPrettyPrinter();
  private static Path tmpDir;

  @BeforeEach
  void setUp() throws IOException {
    tmpDir = Files.createTempDirectory(Path.of("target"), "tmp");
  }

  @AfterEach
  void tearDown() {
    deleteDirectory(tmpDir.toFile());
  }

  static void deleteDirectory(File dir) {
    File[] contents = dir.listFiles();
    if (contents != null) {
      for (File file : contents) {
        deleteDirectory(file);
      }
    }
    dir.delete();
  }

  /* Transactions and events provided by:
  https://testnet.partisiablockchain.com/ */

  /**
   * Provides test cases from the 'transaction-inputs' folder.
   *
   * @return list of files in the 'transaction-inputs' folder.
   */
  static List<File> transactionInputs() {
    return Arrays.stream(Objects.requireNonNull(new File(TRANSACTION_INPUTS).listFiles())).toList();
  }

  /**
   * Provides expected references of events from the 'reference-outputs/events' folder.
   *
   * @return list of files in the 'reference-outputs/events' folder.
   */
  static List<File> eventReferences() {
    return Arrays.stream(Objects.requireNonNull(new File(REFERENCE_EVENTS).listFiles())).toList();
  }

  /**
   * Provides expected references of transactions from the 'reference-outputs/transactions' folder.
   *
   * @return list of files in the 'reference-outputs/transactions' folder.
   */
  static List<File> transactionReferences() {
    return Arrays.stream(Objects.requireNonNull(new File(REFERENCE_TRANSACTIONS).listFiles()))
        .toList();
  }

  /**
   * Regenerates expected reference files from the transaction inputs. This is done by iterating
   * over each input in the 'transactions-inputs' folder, reading the contents of the transaction
   * into an ExecutedTransaction and deserializing depending on if it represents a signed
   * transaction or an event.
   *
   * @param args unused.
   */
  public static void main(String[] args) {
    regenerateFiles();
  }

  @ParameterizedTest
  @MethodSource("transactionInputs")
  void validateReference(File inputFile) throws IOException {
    ExecutedTransaction executedTransaction =
        stateObjectMapper.readValue(inputFile, ExecutedTransaction.class);

    File tmpOut = new File(tmpDir.resolve(inputFile.getName()).toString());
    Path actualPath = Path.of(tmpOut.getPath());

    if (executedTransaction.isEvent()) {
      var event =
          TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(
              executedTransaction.transactionPayload());

      writer.writeValue(tmpOut, event);

      Path expectedReferencePath = Path.of(REFERENCE_EVENTS + inputFile.getName() + JSON_EXT);
      compareFiles(expectedReferencePath, actualPath);
    } else {
      var transaction =
          TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialSignedTransaction(
              executedTransaction.transactionPayload());
      writer.writeValue(tmpOut, transaction);
      Path expectedPath = Path.of(REFERENCE_TRANSACTIONS + inputFile.getName() + JSON_EXT);
      compareFiles(expectedPath, actualPath);
    }
  }

  /**
   * Generates an ExecutableEvent from an input file and compares it against the corresponding
   * expected output reference.
   *
   * @param expectedReference the file containing the expected result.
   */
  @ParameterizedTest
  @MethodSource("eventReferences")
  void verifyDeserializeAgainstEventRef(File expectedReference) {
    String filename = expectedReference.getName();
    String referenceName = filename.substring(0, filename.lastIndexOf("."));

    JsonNode reference = referenceNodeFor(expectedReference);

    File transaction = new File(TRANSACTION_INPUTS + referenceName);
    TransactionAndEventDeserialization_SDK_13_4_0.ExecutableEvent actual =
        deserializeEventFrom(transaction);

    assertExecutableEvent(reference, actual);
  }

  /**
   * Generates a SignedTransaction from an input file and compares it against the corresponding
   * expected output reference.
   *
   * @param expectedReference the file containing the expected result.
   */
  @ParameterizedTest
  @MethodSource("transactionReferences")
  void verifyDeserializeAgainstTransactionRef(File expectedReference) {
    String filename = expectedReference.getName();
    String referenceName = filename.substring(0, filename.lastIndexOf("."));

    JsonNode reference = referenceNodeFor(expectedReference);

    File transaction = new File(TRANSACTION_INPUTS + referenceName);
    TransactionAndEventDeserialization_SDK_13_4_0.SignedTransaction actual =
        transactionFrom(transaction);

    assertSignedTransaction(reference, actual);
  }

  /**
   * Compares the contents of two files, ignoring new lines and asserts equality.
   *
   * @param first the first file
   * @param second the second file
   */
  private void compareFiles(Path first, Path second) {
    String expected = ExceptionConverter.call(() -> Files.readString(first));
    String actual = ExceptionConverter.call(() -> Files.readString(second));
    assertThat(expected).isEqualToIgnoringNewLines(actual);
  }

  /**
   * Regenerates reference files by iterating over the transaction inputs, deserializing the event
   * and overwriting the output reference file.
   */
  private static void regenerateFiles() {
    File[] files = new File(TRANSACTION_INPUTS).listFiles();
    for (File file : files) {
      try {
        ExecutedTransaction executedTransaction =
            stateObjectMapper.readValue(file, ExecutedTransaction.class);

        if (executedTransaction.isEvent()) {
          regenerateEvent(file.getName(), executedTransaction);
        } else {
          regenerateTransaction(file.getName(), executedTransaction);
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Regenerates an event reference file.
   *
   * @param fileName the filename of the input file.
   * @param executedEvent the ExecutedTransaction reference to generate new reference for.
   */
  private static void regenerateEvent(String fileName, ExecutedTransaction executedEvent)
      throws IOException {
    File outFile = new File(REFERENCE_EVENTS + fileName + JSON_EXT);
    var deserializedEvent =
        TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(
            executedEvent.transactionPayload());
    writer.writeValue(outFile, deserializedEvent);
  }

  /**
   * Regenerates a transaction reference file.
   *
   * @param fileName the filename of the input file.
   * @param executedTransaction the ExecutedTransaction reference to generate new reference for.
   */
  private static void regenerateTransaction(
      String fileName, ExecutedTransaction executedTransaction) throws IOException {
    File outFile = new File(REFERENCE_TRANSACTIONS + fileName + JSON_EXT);
    var deserializedTransaction =
        TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialSignedTransaction(
            executedTransaction.transactionPayload());

    writer.writeValue(outFile, deserializedTransaction);
  }

  /**
   * Retrieve root JsonNode from generated json reference.
   *
   * @param referenceFile the reference file
   * @return root JsonNode from the reference hash file.
   */
  private JsonNode referenceNodeFor(File referenceFile) {
    try {
      return stateObjectMapper.readValue(referenceFile, JsonNode.class);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Retrieve the ExecutableEvent from a reference input file.
   *
   * @param inputFile the transaction input file.
   * @return ExecutableEvent read from the reference input file.
   */
  private TransactionAndEventDeserialization_SDK_13_4_0.ExecutableEvent deserializeEventFrom(
      File inputFile) {
    try {
      byte[] payload =
          stateObjectMapper.readValue(inputFile, ExecutedTransaction.class).transactionPayload();
      return TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialExecutableEvent(
          payload);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Retrieve the SignedTransaction from a reference input file.
   *
   * @param inputFile the transaction input file.
   * @return SignedTransaction read from the reference input file.
   */
  private TransactionAndEventDeserialization_SDK_13_4_0.SignedTransaction transactionFrom(
      File inputFile) {
    try {
      byte[] payload =
          stateObjectMapper.readValue(inputFile, ExecutedTransaction.class).transactionPayload();
      return TransactionAndEventDeserialization_SDK_13_4_0.deserializeSpecialSignedTransaction(
          payload);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Asserts equality between a JsonNode describing a SignedTransaction and an actual
   * SignedTransaction. Iterates through the expected data structure, retrieving the appropriate
   * JsonNodes and compares with the corresponding values.
   *
   * @param signedTransactionNode JsonNode representing the SignedTransaction
   * @param actual the actual SignedTransaction.
   */
  private void assertSignedTransaction(
      JsonNode signedTransactionNode,
      TransactionAndEventDeserialization_SDK_13_4_0.SignedTransaction actual) {

    JsonNode innerPartNode = signedTransactionNode.get("innerPart");
    assertInnerPart(innerPartNode, actual.innerPart());

    JsonNode signatureNode = signedTransactionNode.get("signature");
    assertSignature(signatureNode, actual.signature());
  }

  /**
   * Asserts equality between a JsonNode describing an ExecutableEvent and an actual
   * ExecutableEvent. Iterates through the expected data structure, retrieving the appropriate
   * JsonNodes and compares with the corresponding values.
   *
   * @param executableEvent JsonNode representing the ExecutableEvent.
   * @param actual the actual ExecutableEvent.
   */
  private void assertExecutableEvent(
      JsonNode executableEvent,
      TransactionAndEventDeserialization_SDK_13_4_0.ExecutableEvent actual) {

    JsonNode expectedOriginShardNode = executableEvent.get("originShard");
    if (expectedOriginShardNode == null) {
      assertThat(actual.originShard()).isEqualTo(null);
    } else {
      assertThat(expectedOriginShardNode.asText()).isEqualTo(actual.originShard());
    }

    JsonNode eventNode = executableEvent.get("eventTransaction");
    var actualEvent = actual.eventTransaction();

    JsonNode committeId = eventNode.get("committeeId");
    assertThat(actualEvent.committeeId()).isEqualTo(committeId.asLong());

    JsonNode governanceVersion = eventNode.get("governanceVersion");
    assertThat(actualEvent.governanceVersion()).isEqualTo(governanceVersion.asLong());

    JsonNode hashNode = eventNode.get("hash");
    Hash expectedHash = Hash.fromString(hashNode.asText());
    assertThat(actualEvent.hash()).isEqualTo(expectedHash);

    JsonNode height = eventNode.get("height");
    assertThat(actualEvent.height()).isEqualTo((byte) height.asInt());

    JsonNode innerEventNode = eventNode.get("innerEvent");
    assertInnerEvent(innerEventNode, actualEvent.innerEvent());

    assertShardRoute(eventNode.get("shardRoute"), actualEvent.shardRoute());
    assertReturnEnvelope(eventNode.get("returnEnvelope"), actualEvent.returnEnvelope());
  }

  /// Assertions between JsonNodes and actual values.

  private void assertInnerPart(
      JsonNode innerPartNode, TransactionAndEventDeserialization_SDK_13_4_0.InnerPart innerPart) {

    assertCore(innerPartNode.get("core"), innerPart.core());
    assertInteractWithContract(innerPartNode.get("transaction"), innerPart.transaction());
  }

  private void assertCore(
      JsonNode node, TransactionAndEventDeserialization_SDK_13_4_0.CoreTransactionPart actual) {
    assertThat(node.get("cost").asLong()).isEqualTo(actual.cost());
    assertThat(node.get("nonce").asLong()).isEqualTo(actual.nonce());
    assertThat(node.get("validToTime").asLong()).isEqualTo(actual.validToTime());
  }

  private void assertSignature(JsonNode signatureNode, Signature signature) {
    String expected = signatureNode.asText();
    String actual = signature.writeAsString();
    assertThat(expected).isEqualTo(actual);
  }

  private void assertInnerEvent(
      JsonNode innerEventNode, TransactionAndEventDeserialization_SDK_13_4_0.InnerEvent actual) {

    // Handle specific cases
    if (actual instanceof TransactionAndEventDeserialization_SDK_13_4_0.InnerEventSystem sysEvent) {
      JsonNode innerSysEventNode = innerEventNode.get("innerSystemEvent");
      assertInnerSystemEvent(innerSysEventNode, sysEvent.innerSystemEvent());
      assertThat(actual.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerEventD.SYSTEM);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerEventTransaction transaction) {
      JsonNode innerTransactionNode = innerEventNode.get("innerTransaction");
      assertInnerTransaction(innerTransactionNode, transaction.innerTransaction());
      assertThat(actual.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerEventD.TRANSACTION);
    } else {
      throw new UnsupportedOperationException("Add support when case is possible");
    }
  }

  private void assertInnerTransaction(
      JsonNode innerTransactionNode,
      TransactionAndEventDeserialization_SDK_13_4_0.InnerTransaction actual) {
    BlockchainAddress expectedFrom =
        BlockchainAddress.fromString(innerTransactionNode.get("from").asText());
    assertThat(expectedFrom).isEqualTo(actual.from());

    assertThat(actual.cost()).isEqualTo(innerTransactionNode.get("cost").asLong());

    JsonNode transactionNode = innerTransactionNode.get("transaction");
    assertTransaction(transactionNode, actual.transaction());
  }

  private void assertTransaction(
      JsonNode transactionNode, TransactionAndEventDeserialization_SDK_13_4_0.Transaction actual) {

    // Handle specific cases
    if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.TransactionInteractContract
                interactContract) {
      assertThat(interactContract.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.TransactionD.INTERACT_CONTRACT);

      assertInteractWithContract(
          transactionNode.get("interactWithContractTransaction"),
          interactContract.interactWithContractTransaction());
    } else if (actual
        instanceof TransactionAndEventDeserialization_SDK_13_4_0.TransactionDeployContract deploy) {
      assertThat(deploy.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.TransactionD.DEPLOY_CONTRACT);
      assertCreateContract(
          transactionNode.get("createContractTransaction"), deploy.createContractTransaction());
    }
  }

  private void assertCreateContract(
      JsonNode node,
      TransactionAndEventDeserialization_SDK_13_4_0.CreateContractTransaction actual) {
    assertBase64EncodedByteArray(node.get("abi").asText(), actual.abi());
    assertBase64EncodedByteArray(node.get("binderJar").asText(), actual.binderJar());
    assertBase64EncodedByteArray(node.get("contractJar").asText(), actual.contractJar());
    assertBase64EncodedByteArray(node.get("rpc").asText(), actual.rpc());

    var expectedAddress = BlockchainAddress.fromString(node.get("address").asText());
    assertThat(actual.address()).isEqualTo(expectedAddress);
  }

  private void assertInteractWithContract(
      JsonNode node,
      TransactionAndEventDeserialization_SDK_13_4_0.InteractWithContractTransaction actual) {
    var expectedAddress = BlockchainAddress.fromString(node.get("contractId").asText());
    assertThat(expectedAddress).isEqualTo(actual.contractId());
    assertBase64EncodedByteArray(node.get("payload").asText(), actual.payload());
  }

  private void assertInnerSystemEvent(
      JsonNode innerSysEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEvent actual) {

    // Handle specific cases
    if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCallback callback) {
      JsonNode callbackEventNode = innerSysEventNode.get("callbackEvent");
      assertThat(actual.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.CALLBACK);
      assertCallback(callbackEventNode, callback);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpdateContextFreePluginState
                updateFreeState) {

      JsonNode updateContextFreePluginStateNode =
          innerSysEventNode.get("updateContextFreePluginState");
      assertUpdateContextFreePluginState(
          updateContextFreePluginStateNode, updateFreeState.updateContextFreePluginState());
      assertThat(actual.discriminant())
          .isEqualTo(
              TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD
                  .UPDATE_CONTEXT_FREE_PLUGIN_STATE);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpdatePlugin updatePlugin) {

      JsonNode node = innerSysEventNode.get("updatePluginEvent");
      assertUpdatePluginEvent(node, updatePlugin.updatePluginEvent());
      assertThat(actual.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.UPDATE_PLUGIN);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpdateLocalPluginState
                updateLocalPluginState) {

      JsonNode updateLocalPluginStateNode = innerSysEventNode.get("updateLocalPluginStateEvent");
      assertUpdateLocalPluginStateEvent(
          updateLocalPluginStateNode, updateLocalPluginState.updateLocalPluginStateEvent());
      assertThat(actual.discriminant())
          .isEqualTo(
              TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD
                  .UPDATE_LOCAL_PLUGIN_STATE);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventSetFeature setFeature) {

      JsonNode setFeatureEventNode = innerSysEventNode.get("setFeatureEvent");
      assertSetFeatureEvent(setFeatureEventNode, setFeature.setFeatureEvent());
      assertThat(actual.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.SET_FEATURE);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpgradeSystemContract
                upgradeSystemContract) {

      JsonNode upgradeSystemContractEventNode = innerSysEventNode.get("upgradeSystemContractEvent");
      assertSystemContractEvent(
          upgradeSystemContractEventNode, upgradeSystemContract.upgradeSystemContractEvent());
      assertThat(actual.discriminant())
          .isEqualTo(
              TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD
                  .UPGRADE_SYSTEM_CONTRACT);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCheckExistence
                checkExistence) {

      JsonNode checkExistenceEventNode = innerSysEventNode.get("checkExistenceEvent");
      assertCheckExistenceEvent(checkExistenceEventNode, checkExistence.checkExistenceEvent());
      assertThat(actual.discriminant())
          .isEqualTo(
              TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD.CHECK_EXISTENCE);
    } else if (actual
        instanceof
        TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventUpdateGlobalPluginState
                updateGlobalPluginState) {

      JsonNode updateGlobalNode = innerSysEventNode.get("updateGlobalPluginStateEvent");
      assertUpdateGlobalPluginStateEvent(
          updateGlobalNode, updateGlobalPluginState.updateGlobalPluginStateEvent());
      assertThat(actual.discriminant())
          .isEqualTo(
              TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventD
                  .UPDATE_GLOBAL_PLUGIN_STATE);
    } else {
      throw new UnsupportedOperationException("Add support when case is possible");
    }
  }

  private void assertUpdateContextFreePluginState(
      JsonNode updateContextFreePluginStateNode,
      TransactionAndEventDeserialization_SDK_13_4_0.UpdateContextFreePluginState
          updateContextFreePluginState) {

    assertChainPluginType(updateContextFreePluginState.chainPluginType());

    JsonNode rpcNode = updateContextFreePluginStateNode.get("rpc");

    assertBase64EncodedByteArray(rpcNode.asText(), updateContextFreePluginState.rpc());
  }

  private void assertUpdateGlobalPluginStateEvent(
      JsonNode updateGlobalNode,
      TransactionAndEventDeserialization_SDK_13_4_0.UpdateGlobalPluginStateEvent
          updateGlobalPluginStateEvent) {

    assertChainPluginType(updateGlobalPluginStateEvent.chainPluginType());

    JsonNode updateNode = updateGlobalNode.get("update");
    assertUpdateGlobal(updateNode, updateGlobalPluginStateEvent.update());
  }

  private void assertUpdateGlobal(
      JsonNode updateNode,
      TransactionAndEventDeserialization_SDK_13_4_0.GlobalPluginStateUpdate update) {

    JsonNode rpcNode = updateNode.get("rpc");
    assertBase64EncodedByteArray(rpcNode.asText(), update.rpc());
  }

  private void assertCheckExistenceEvent(
      JsonNode checkExistenceEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.CheckExistenceEvent checkExistenceEvent) {

    JsonNode contractOrAccountAddressNode = checkExistenceEventNode.get("contractOrAccountAddress");
    var expectedAddress = BlockchainAddress.fromString(contractOrAccountAddressNode.asText());
    assertThat(expectedAddress).isEqualTo(checkExistenceEvent.contractOrAccountAddress());
  }

  private void assertSystemContractEvent(
      JsonNode upgradeSystemContractEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.UpgradeSystemContractEvent
          upgradeSystemContractEvent) {

    JsonNode abiNode = upgradeSystemContractEventNode.get("abi");
    assertBase64EncodedByteArray(abiNode.asText(), upgradeSystemContractEvent.abi());

    JsonNode binderNode = upgradeSystemContractEventNode.get("binderJar");
    assertBase64EncodedByteArray(binderNode.asText(), upgradeSystemContractEvent.binderJar());

    JsonNode address = upgradeSystemContractEventNode.get("contractAddress");
    assertThat(BlockchainAddress.fromString(address.asText()))
        .isEqualTo(upgradeSystemContractEvent.contractAddress());

    JsonNode contractJarNode = upgradeSystemContractEventNode.get("contractJar");
    assertBase64EncodedByteArray(
        contractJarNode.asText(), upgradeSystemContractEvent.contractJar());

    JsonNode rpcNode = upgradeSystemContractEventNode.get("rpc");
    assertBase64EncodedByteArray(rpcNode.asText(), upgradeSystemContractEvent.rpc());
  }

  private void assertSetFeatureEvent(
      JsonNode setFeatureEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.SetFeatureEvent setFeatureEvent) {

    JsonNode keyNode = setFeatureEventNode.get("key");
    assertThat(keyNode.asText()).isEqualTo(setFeatureEvent.key());

    JsonNode valueNode = setFeatureEventNode.get("value");
    assertThat(valueNode.asText()).isEqualTo(setFeatureEvent.value());
  }

  private void assertUpdateLocalPluginStateEvent(
      JsonNode updateLocalPluginStateNode,
      TransactionAndEventDeserialization_SDK_13_4_0.UpdateLocalPluginStateEvent
          updateLocalPluginStateEvent) {
    assertChainPluginType(updateLocalPluginStateEvent.chainPluginType());

    JsonNode updateNode = updateLocalPluginStateNode.get("update");
    assertUpdate(updateNode, updateLocalPluginStateEvent.update());
  }

  private void assertUpdate(
      JsonNode updateNode,
      TransactionAndEventDeserialization_SDK_13_4_0.LocalPluginStateUpdate update) {

    JsonNode contextNode = updateNode.get("context");
    BlockchainAddress expected = BlockchainAddress.fromString(contextNode.asText());
    assertThat(expected).isEqualTo(update.context());

    JsonNode rpcNode = updateNode.get("rpc");
    assertBase64EncodedByteArray(rpcNode.asText(), update.rpc());
  }

  private void assertUpdatePluginEvent(
      JsonNode updatePluginEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.UpdatePluginEvent updatePluginEvent) {

    assertChainPluginType(updatePluginEvent.chainPluginType());

    JsonNode invocationNode = updatePluginEventNode.get("invocation");
    assertBase64EncodedByteArray(invocationNode.asText(), updatePluginEvent.invocation());

    JsonNode jarNode = updatePluginEventNode.get("jar");
    assertBase64EncodedByteArray(jarNode.asText(), updatePluginEvent.jar());
  }

  private void assertChainPluginType(
      TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginType chainPluginType) {

    if (chainPluginType
        instanceof TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginTypeAccount) {
      assertThat(chainPluginType.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginTypeD.ACCOUNT);
    } else if (chainPluginType
        instanceof TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginTypeConsensus) {
      assertThat(chainPluginType.discriminant())
          .isEqualTo(TransactionAndEventDeserialization_SDK_13_4_0.ChainPluginTypeD.CONSENSUS);
    } else {
      throw new UnsupportedOperationException("Add support when case is possible");
    }
  }

  private void assertCallback(
      JsonNode callbackEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.InnerSystemEventCallback actual) {
    assertCallbackEvent(callbackEventNode, actual.callbackEvent());
  }

  private void assertCallbackEvent(
      JsonNode callbackEventNode,
      TransactionAndEventDeserialization_SDK_13_4_0.CallbackEvent actual) {
    var expectedHash = Hash.fromString(callbackEventNode.get("completedTransaction").asText());
    assertThat(actual.completedTransaction()).isEqualTo(expectedHash);

    JsonNode returnEnvelopeNode = callbackEventNode.get("returnEnvelope");
    assertReturnEnvelope(returnEnvelopeNode, actual.returnEnvelope());

    assertThat(actual.success()).isEqualTo(callbackEventNode.get("success").asBoolean());

    JsonNode returnValNode = callbackEventNode.get("returnValue");
    assertBase64EncodedByteArray(returnValNode.asText(), actual.returnValue());
  }

  /**
   * Asserts the contents of a base64 encoding against an actual byte array.
   *
   * @param base64Encoding base64 encoded bytes
   * @param actual actual byte array.
   */
  private void assertBase64EncodedByteArray(String base64Encoding, byte[] actual) {

    byte[] bytes = Base64.decode(base64Encoding);

    for (int i = 0; i < bytes.length; i++) {
      assertThat(bytes[i]).isEqualTo(actual[i]);
    }

    // Ensure everything is read
    assertThat(bytes.length).isEqualTo(actual.length);
  }

  private void assertShardRoute(
      JsonNode shardRouteNode, TransactionAndEventDeserialization_SDK_13_4_0.ShardRoute actual) {
    assertThat(actual.nonce()).isEqualTo(shardRouteNode.get("nonce").asLong());

    JsonNode targetShardNode = shardRouteNode.get("targetShard");
    // 'None' check
    if (targetShardNode == null) {
      assertThat(actual.targetShard()).isEqualTo(null);
    } else {
      assertThat(actual.targetShard()).isEqualTo(targetShardNode.asText());
    }
  }

  private void assertReturnEnvelope(
      JsonNode returnEnvelopeNode,
      TransactionAndEventDeserialization_SDK_13_4_0.ReturnEnvelope actual) {
    // 'None' check
    if (returnEnvelopeNode == null) {
      assertThat(actual).isEqualTo(null);
    } else {
      BlockchainAddress expected =
          BlockchainAddress.fromString(returnEnvelopeNode.get("contract").asText());
      assertThat(expected).isEqualTo(actual.contract());
    }
  }
}
