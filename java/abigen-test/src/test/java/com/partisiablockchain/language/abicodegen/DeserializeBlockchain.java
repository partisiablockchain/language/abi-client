package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.shards.ShardManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.language.abiclient.transaction.TransactionReader;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Test that all transactions on the public blockchain can be deserialized, to do this check that no
 * error occurs, and all bytes are read.
 */
final class DeserializeBlockchain {

  /**
   * Main method that starts the sharded clients and deserializes all transactions on the blockchain
   * in parallel. If the deserialized value contains a struct not yet found then the hash is saved.
   */
  @SuppressWarnings("FutureReturnValueIgnored")
  public static void main(String[] args) throws InterruptedException {
    // main-net: https://reader.partisiablockchain.com
    // testnet: https://node1.testnet.partisiablockchain.com
    int numberOfShards = 3;
    BlockchainClient client =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", numberOfShards);
    ShardManager shardManager = new ShardManager(numberOfShards);

    Set<String> namedValuesFound = new HashSet<>();

    ExecutorService executorService = Executors.newCachedThreadPool();
    for (ShardId shardI : shardManager.getShards()) {

      executorService.submit(
          () -> {
            try {
              BlockState latestBlock = client.getLatestBlock(shardI);
              System.out.println("Deserializing transactions on " + shardI.name());
              for (long i = 0; i <= latestBlock.blockTime(); i++) {
                System.out.print("\rBlock: " + i + " / " + latestBlock.blockTime());
                BlockState block = client.getBlockByNumber(shardI, i);

                List<String> transactions = new ArrayList<>(block.transactions());
                transactions.addAll(block.events());
                for (String transactionHash : transactions) {
                  ExecutedTransaction transaction =
                      client.getTransaction(shardI, Hash.fromString(transactionHash));
                  ScValueStruct deserializedValue = deserializeExecutedTransaction(transaction);
                  if (checkNew(deserializedValue, namedValuesFound)) {
                    writeInterestingTransaction(transactionHash, shardI.name(), transaction);
                  }
                }
              }
            } catch (Throwable e) {
              System.out.println("\nFailed deserializing transaction due to error: ");
              System.out.println(e.getMessage());
            }
          });
    }
    executorService.shutdown();
    executorService.awaitTermination(1, TimeUnit.HOURS);
    System.out.println("\nNamedValuesFound: ");
    for (String s : namedValuesFound) {
      System.out.println(s);
    }
  }

  private static boolean checkNew(ScValue value, Set<String> namedValuesFound) {
    if (value instanceof ScValueEnum scValueEnum) {
      return checkNew(scValueEnum.item(), namedValuesFound);
    } else if (value instanceof ScValueStruct scValueStruct) {
      boolean isNew = false;
      if (!namedValuesFound.contains(scValueStruct.name())) {
        namedValuesFound.add(scValueStruct.name());
        isNew = true;
      }

      for (ScValue field : scValueStruct.fieldsMap().values()) {
        isNew = isNew || checkNew(field, namedValuesFound);
      }
      return isNew;
    } else {
      return false;
    }
  }

  private static void writeInterestingTransaction(
      String transactionHash, String shard, ExecutedTransaction transaction) {
    ObjectMapper objectMapper = new ObjectMapper();
    String shardName = Objects.requireNonNullElse(shard, "genesis");
    try {
      objectMapper.writeValue(new File("target/" + shardName + "-" + transactionHash), transaction);
    } catch (IOException e) {
      System.out.println("Failed to write transaction: " + shardName + "-" + transactionHash);
      System.out.println(e.getMessage());
    }
  }

  private static ScValueStruct deserializeExecutedTransaction(
      ExecutedTransaction executedTransaction) {
    if (executedTransaction.isEvent()) {
      return TransactionReader.deserializeEventPayload(executedTransaction.transactionPayload());
    } else {
      return TransactionReader.deserializeTransactionPayload(
          executedTransaction.transactionPayload());
    }
  }
}
