package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abicodegen.ZkClassification;
import com.partisiablockchain.language.abistreams.AbiBitInput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.codegenlib.SecretInput;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Testing codegen secret input. */
public final class SecretInputTest {

  /** Codegenned contracts can create secret inputs. */
  @Test
  void secretInput() {
    SecretInput secretInput =
        ZkClassification.addInputSample(
                new ZkClassification.SecretVarId(1),
                BlockchainAddress.fromString("000000000000000000000000000000000000000000"))
            .secretInput(
                new ZkClassification.Sample(
                    List.of(
                        (short) 1,
                        (short) 2,
                        (short) 3,
                        (short) 4,
                        (short) 5,
                        (short) 6,
                        (short) 7,
                        (short) 8,
                        (short) 9,
                        (short) 10)));

    assertThat(secretInput.publicRpc().length).isEqualTo(26);
    AbiByteInput publicRpc = AbiByteInput.createBigEndian(secretInput.publicRpc());
    assertThat(publicRpc.readShortname()).isEqualTo(new byte[] {0x41});
    assertThat(publicRpc.readI32()).isEqualTo(1);
    assertThat(publicRpc.readAddress())
        .isEqualTo(BlockchainAddress.fromString("000000000000000000000000000000000000000000"));

    assertThat(secretInput.secretInput().size()).isEqualTo(160);
    AbiBitInput input = AbiBitInput.create(secretInput.secretInput());
    assertThat(input.readI16()).isEqualTo((short) 1);
    assertThat(input.readI16()).isEqualTo((short) 2);
    assertThat(input.readI16()).isEqualTo((short) 3);
    assertThat(input.readI16()).isEqualTo((short) 4);
    assertThat(input.readI16()).isEqualTo((short) 5);
    assertThat(input.readI16()).isEqualTo((short) 6);
    assertThat(input.readI16()).isEqualTo((short) 7);
    assertThat(input.readI16()).isEqualTo((short) 8);
    assertThat(input.readI16()).isEqualTo((short) 9);
    assertThat(input.readI16()).isEqualTo((short) 10);
  }
}
