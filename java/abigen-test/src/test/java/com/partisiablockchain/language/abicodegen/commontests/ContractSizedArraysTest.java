package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abicodegen.ContractSizedArrays;
import com.secata.stream.LittleEndianByteOutput;
import java.util.Arrays;
import java.util.Collections;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractSizedArraysTest {
  /** Generated code correct deserializes generic arrays into a java type. */
  @Test
  void deserializeTest() {
    var expected =
        new ContractSizedArrays.ExampleContractState(
            HexFormat.of().parseHex("ffffffffffffffffffffffff"),
            Collections.nCopies(16, 1),
            List.of(
                Collections.nCopies(4, 2),
                Collections.nCopies(4, 3),
                Collections.nCopies(4, 4),
                Collections.nCopies(4, 5)),
            Arrays.asList(null, null, null, 6, null));

    byte[] stateBytes =
        LittleEndianByteOutput.serialize(
            stream -> {
              stream.writeBytes(HexFormat.of().parseHex("ffffffffffffffffffffffff"));
              for (int i = 0; i < 16; i++) {
                stream.writeI32(1);
              }
              for (int i = 0; i < 4; i++) {
                stream.writeI32(2);
              }
              for (int i = 0; i < 4; i++) {
                stream.writeI32(3);
              }
              for (int i = 0; i < 4; i++) {
                stream.writeI32(4);
              }
              for (int i = 0; i < 4; i++) {
                stream.writeI32(5);
              }
              stream.writeBoolean(false);
              stream.writeBoolean(false);
              stream.writeBoolean(false);
              stream.writeBoolean(true);
              stream.writeI32(6);
              stream.writeBoolean(false);
            });

    assertThat(ContractSizedArrays.deserializeState(stateBytes))
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  /** Generated code throws error if trying to serialize sized array with wrong size. */
  @Test
  void serializeWrongSize() {
    assertThatThrownBy(() -> ContractSizedArrays.updateMyArray(List.of(1, 2, 3)))
        .hasMessage("Length of value does not match expected 16");
  }

  /** Generated code correctly serializes sized arrays to rpc format. */
  @Test
  void serializeArray() {
    assertThat(
            HexFormat.of()
                .formatHex(
                    ContractSizedArrays.updateMyArray3(Arrays.asList(null, null, null, 4, null))))
        .isEqualTo(
            "f3d683af06" // Shortname
                + "000000010000000400");
  }

  /** Generated code throws error if trying to serialize sized array with wrong size. */
  @Test
  void serializeByteArrayWrongSize() {
    assertThatThrownBy(() -> ContractSizedArrays.updateByteArray(new byte[21]))
        .hasMessageContaining("Length of value does not match expected 12");
  }

  /** Generated code correctly serializes sized byte arrays to rpc format. */
  @Test
  void serializeByteArray() {
    assertThat(
            HexFormat.of()
                .formatHex(
                    ContractSizedArrays.updateByteArray(
                        HexFormat.of().parseHex("111111111111111111111111"))))
        .isEqualTo(
            "9df1bab60d" // Shortname
                + "111111111111111111111111");
  }
}
