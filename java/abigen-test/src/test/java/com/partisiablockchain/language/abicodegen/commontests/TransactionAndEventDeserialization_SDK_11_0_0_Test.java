package com.partisiablockchain.language.abicodegen.commontests;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abicodegen.TransactionAndEventDeserialization_SDK_11_0_0;
import java.util.Base64;
import java.util.HexFormat;
import org.junit.jupiter.api.Test;

/** Tests for Transaction And Event Deserialization Abi. */
final class TransactionAndEventDeserialization_SDK_11_0_0_Test {

  @Test
  void testDeserialize() {
    // payload from
    // https://node1.testnet.partisiablockchain.com/shards/Shard1/blockchain/transaction/95027622345365ad1bbb06dff5775fd9282748601f8fa8159c5d0dc80b71b750
    var payload =
        Base64.getDecoder()
            .decode(
                "AQAAAAZTaGFyZDDHOdytUle6XXqiCSYafZJpC9jA7e+HG2QC6jvO4TDO0QAA"
                    + "bfFCscP4IP/AffKrc1jK0iAtoY8AAAAAAAAAAAEE/hfRAJNyyO06xbeQsy40"
                    + "k1nCx+kAAAAeAwEAAAAGU2hhcmQwAAAAAAAMI7AABAApADwARwBNAQAAAAZT"
                    + "aGFyZDEAAAAAAAA/sQAAAAAAAAABAAAAAAAAAAUBAA==");
    var event =
        TransactionAndEventDeserialization_SDK_11_0_0.deserializeSpecialExecutableEvent(payload);
    assertThat(event.originShard()).isEqualTo("Shard0");
    assertThat(event.eventTransaction().committeeId()).isEqualTo(1L);
    assertThat(event.eventTransaction().governanceVersion()).isEqualTo(5L);
    var transaction =
        (TransactionAndEventDeserialization_SDK_11_0_0.InnerEventTransaction)
            event.eventTransaction().innerEvent();
    assertThat(transaction.innerTransaction().cost()).isEqualTo(0L);
    var interact =
        (TransactionAndEventDeserialization_SDK_11_0_0.TransactionInteractContract)
            transaction.innerTransaction().transaction();

    // The payload of the rpc call
    var expected =
        HexFormat.of().parseHex("03010000000653686172643000000000000c23b000040029003c0047004d");
    var rpcPayload = interact.interactWithContractTransaction().payload();
    assertThat(rpcPayload).isEqualTo(expected);
  }
}
