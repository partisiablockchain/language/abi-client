package com.partisiablockchain.language.abicodegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.abicodegen.CodegenOptions.TargetLanguage.TS;

import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/** Generate ts code for ts codegen tests. */
public final class GenerateTypescript {

  /** Generate ts code. */
  public static void main(String[] args) {
    generateCode(Arrays.asList(args));
  }

  @SuppressWarnings("LeftCurlyNl")
  private static void generateCode(List<String> args) {
    var inputPath = Path.of(args.get(0));
    var outputPath = Path.of(args.get(1));
    var options = getCodegenOptions(outputPath);
    byte[] abiBytes = readAbiBytes(inputPath);
    String code;
    var codeGen =
        new AbiCodegen(
            new AbiParser(abiBytes, AbiParser.Strictness.STRICT).parseContractAbi(), options);
    code = codeGen.generateCode();
    writeFile(outputPath, code.getBytes(StandardCharsets.UTF_8));
  }

  private static CodegenOptions getCodegenOptions(Path outputPath) {
    var fileName = outputPath.getFileName().toString();
    int endIndex = fileName.indexOf(".ts");
    String contractName = fileName.substring(0, endIndex);
    HashSet<String> namedType = new HashSet<>();
    namedType.add("ExecutableEvent");

    var options =
        new CodegenOptions(
            TS,
            contractName,
            AbiParser.Strictness.STRICT,
            "com.partisiablockchain.language.abicodegen",
            true,
            true,
            false,
            true,
            namedType,
            true);
    return options;
  }

  private static void writeFile(Path path, byte[] bytes) {
    ExceptionConverter.run(
        () -> {
          var parent = path.getParent();
          if (parent != null) {
            Files.createDirectories(parent);
          }
          Files.write(path, bytes);
        },
        "Unable to write file");
  }

  private static byte[] readAbiBytes(Path inputFilePath) {
    return ExceptionConverter.call(
        () -> Files.readAllBytes(inputFilePath), "unable to read input file: " + inputFilePath);
  }
}
