package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.model.AvlInformation;
import com.partisiablockchain.api.transactionclient.model.AvlStateEntry;
import com.partisiablockchain.api.transactionclient.model.AvlStateValue;
import com.partisiablockchain.api.transactionclient.model.Contract;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainStateClientTest {

  private static final BlockchainAddress TEST_ADDRESS =
      BlockchainAddress.fromString("020000000000000000000000000000000000000000");

  /** Creating client with base url creates client pointing to that url. */
  @Test
  void createFromBasePath() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create("test-url");
    assertThat(client.chainController.getApiClient().getBasePath()).isEqualTo("test-url");
  }

  /** getSerializedContract returns the state bytes of the contract. */
  @Test
  void getSerializedContract() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    byte[] stateBytes = client.getContractStateBinary(TEST_ADDRESS);
    assertThat(stateBytes).containsExactly(1, 2);
  }

  /** getContractStateAvlValue returns the corresponding avl value to a given key. */
  @Test
  void getAvlValue() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    byte[] avlValue = client.getContractStateAvlValue(TEST_ADDRESS, 0, new byte[] {1});
    assertThat(avlValue).containsExactly(5, 6);
  }

  /** getContractStateAvlValue returns null if the given key does not exist. */
  @Test
  void getAvlValueNotFound() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    byte[] avlValue = client.getContractStateAvlValue(TEST_ADDRESS, 0, new byte[0]);
    assertThat(avlValue).isNull();
  }

  /** getContractStateAvlNextN with a specified key returns the entries following that key. */
  @Test
  void getAvlNextN() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    List<Map.Entry<byte[], byte[]>> nextN =
        client.getContractStateAvlNextN(TEST_ADDRESS, 0, new byte[] {1}, 10);
    assertThat(nextN).hasSize(1);
    assertThat(nextN.get(0).getKey()).containsExactly(3);
    assertThat(nextN.get(0).getValue()).containsExactly(4);
  }

  /** getContractStateAvlNextN with a null key returns the entries from the start. */
  @Test
  void getAvlNextNullKey() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    List<Map.Entry<byte[], byte[]>> nextN =
        client.getContractStateAvlNextN(TEST_ADDRESS, 0, null, 10);
    assertThat(nextN).hasSize(2);
    assertThat(nextN.get(0).getKey()).containsExactly(1);
    assertThat(nextN.get(0).getValue()).containsExactly(2);
    assertThat(nextN.get(1).getKey()).containsExactly(3);
    assertThat(nextN.get(1).getValue()).containsExactly(4);
  }

  /** getContractStateAvlSize returns the size of the avl tree. */
  @Test
  void getAvlSize() {
    BlockchainStateClientImpl client = BlockchainStateClientImpl.create(new ChainControllerStub());
    assertThat(client.getContractStateAvlSize(TEST_ADDRESS, 0)).isEqualTo(3);
  }

  /** Test stub. */
  static final class ChainControllerStub extends ChainControllerApi {
    @Override
    public Contract getContract(String address, Long blockTime) throws ApiException {
      return new Contract().serializedContract(new byte[] {1, 2});
    }

    @Override
    public AvlStateValue getContractAvlValue(
        String address, Integer treeId, String key, Long blockTime) throws ApiException {
      if (key.isEmpty()) {
        throw new ApiException("404 not found");
      }
      return new AvlStateValue().data(new byte[] {5, 6});
    }

    @Override
    public List<AvlStateEntry> getContractAvlNextN(
        String address, Integer treeId, String key, Integer n, Integer skip, Long blockTime)
        throws ApiException {

      if (key == null) {
        return List.of(
            new AvlStateEntry().key("01").value(new byte[] {2}),
            new AvlStateEntry().key("03").value(new byte[] {4}));
      } else {
        return List.of(new AvlStateEntry().key("03").value(new byte[] {4}));
      }
    }

    @Override
    public AvlInformation getContractAvlInformation(String address, Integer treeId, Long blockTime)
        throws ApiException {
      return new AvlInformation().size(3);
    }
  }
}
