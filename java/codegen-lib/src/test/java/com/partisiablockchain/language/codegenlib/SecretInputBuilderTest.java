package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abistreams.AbiBitOutput;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Create SecretInput with codegen. */
public final class SecretInputBuilderTest {
  /** Can create a SecretInput with the SecretInputBuilder. */
  @Test
  void secretInput() {
    byte[] publicRpc = {0x40};
    SecretInputBuilder<Integer> secretInputBuilder =
        new SecretInputBuilder<>(publicRpc, (i) -> AbiBitOutput.serialize(out -> out.writeI32(i)));
    SecretInput secretInput = secretInputBuilder.secretInput(4);
    Assertions.assertThat(secretInput.publicRpc()).isEqualTo(publicRpc);
    Assertions.assertThat(secretInput.secretInput().size()).isEqualTo(32);
    Assertions.assertThat(secretInput.secretInput().data()).isEqualTo(new byte[] {4, 0, 0, 0});
  }
}
