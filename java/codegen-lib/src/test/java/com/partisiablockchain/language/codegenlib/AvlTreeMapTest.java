package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AvlTreeMapTest {

  AvlTreeMap<Byte, Boolean> avlTreeMap =
      new AvlTreeMap<>(
          0,
          new TestBlockchainStateClient(),
          null,
          byt -> byt == null ? null : new byte[] {byt},
          bytes -> bytes[0],
          bytes -> bytes[0] != 0);

  @Test
  void treeId() {
    assertThat(avlTreeMap.treeId()).isEqualTo(0);
    AvlTreeMap<String, String> otherTree = new AvlTreeMap<>(42);
    assertThat(otherTree.treeId()).isEqualTo(42);
  }

  @Test
  void getValue() {
    assertThat(avlTreeMap.get((byte) 0)).isFalse();
    assertThat(avlTreeMap.get((byte) 1)).isTrue();
    assertThat(avlTreeMap.get(null)).isNull();
  }

  @Test
  void getNextN() {
    assertThat(avlTreeMap.getNextN((byte) 0, 3))
        .containsExactly(
            Map.entry((byte) 0, false), Map.entry((byte) 0, false), Map.entry((byte) 0, false));
    assertThat(avlTreeMap.getNextN((byte) 1, 2))
        .containsExactly(Map.entry((byte) 1, true), Map.entry((byte) 1, true));
    assertThat(avlTreeMap.getNextN(null, 1)).containsExactly(Map.entry((byte) 0, false));
  }

  @Test
  void serializeKey() {
    assertThat(avlTreeMap.serializeKey((byte) 3)).containsExactly(3);
    assertThat(avlTreeMap.serializeKey((byte) 6)).containsExactly(6);
  }

  @Test
  void deserializeKey() {
    assertThat(avlTreeMap.deserializeKey(new byte[] {3})).isEqualTo((byte) 3);
    assertThat(avlTreeMap.deserializeKey(new byte[] {6})).isEqualTo((byte) 6);
  }

  @Test
  void deserializeValue() {
    assertThat(avlTreeMap.deserializeValue(new byte[] {0})).isEqualTo(false);
    assertThat(avlTreeMap.deserializeValue(new byte[] {1})).isEqualTo(true);
  }

  @Test
  void getSize() {
    assertThat(avlTreeMap.size()).isEqualTo(4);
  }

  @Test
  void equals() {
    EqualsVerifier.forClass(AvlTreeMap.class).withOnlyTheseFields("treeId").verify();
  }

  @Test
  void hashcode() {
    AvlTreeMap<Byte, Boolean> otherTree = new AvlTreeMap<>(0);
    AvlTreeMap<Byte, Boolean> otherTree2 = new AvlTreeMap<>(1);
    assertThat(avlTreeMap.hashCode()).isEqualTo(otherTree.hashCode());
    assertThat(avlTreeMap.hashCode()).isNotEqualTo(otherTree2.hashCode());
  }
}
