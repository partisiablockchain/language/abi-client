package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/** Test implementation of {@link BlockchainStateClient}. */
public final class TestBlockchainStateClient implements BlockchainStateClient {

  @Override
  public byte[] getContractStateBinary(BlockchainAddress address) {
    return new byte[0];
  }

  @Override
  public byte[] getContractStateAvlValue(BlockchainAddress address, int treeId, byte[] key) {
    return key;
  }

  @Override
  public List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n) {
    if (key == null) {
      return Collections.nCopies(n, Map.entry(new byte[1], new byte[1]));
    }
    return Collections.nCopies(n, Map.entry(key, key));
  }

  @Override
  public int getContractStateAvlSize(BlockchainAddress address, int treeId) {
    return 4;
  }
}
