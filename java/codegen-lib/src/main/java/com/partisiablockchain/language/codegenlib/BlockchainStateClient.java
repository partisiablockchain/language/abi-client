package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import java.util.Map;

/** Minimum interface for querying avl data. */
public interface BlockchainStateClient {

  /**
   * Get the contract state as binary.
   *
   * @param address address of the contract
   * @return the contract state
   */
  byte[] getContractStateBinary(BlockchainAddress address);

  /**
   * Get the value corresponding to the given key from the state Avl Tree.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key.
   * @return The corresponding value or null if it doesn't exist.
   */
  byte[] getContractStateAvlValue(BlockchainAddress address, int treeId, byte[] key);

  /**
   * Get the next n entries of state Avl Tree based from a given key (Excluding the key itself). If
   * key is null, gets the first n entries.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key to base the search from. If null, gets the first n entries.
   * @param n The number of entries to get.
   * @return List of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n);

  /**
   * Get the size of the Avl Tree at the specified address and tree id.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @return size of the Avl Tree
   */
  int getContractStateAvlSize(BlockchainAddress address, int treeId);
}
