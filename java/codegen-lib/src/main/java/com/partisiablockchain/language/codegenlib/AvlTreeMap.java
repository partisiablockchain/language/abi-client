package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/** An avl tree map state. */
public final class AvlTreeMap<K, V> {
  private final int treeId;
  private final BlockchainStateClient client;
  private final BlockchainAddress address;
  private final Function<K, byte[]> keySerializer;
  private final Function<byte[], K> keyDeserializer;
  private final Function<byte[], V> valueDeserializer;

  /**
   * Constructor.
   *
   * @param treeId tree id
   * @param client client
   * @param address address of the contract
   * @param keySerializer serializer for keys
   * @param keyDeserializer deserializer for keys
   * @param valueDeserializer deserializer for values
   */
  public AvlTreeMap(
      int treeId,
      BlockchainStateClient client,
      BlockchainAddress address,
      Function<K, byte[]> keySerializer,
      Function<byte[], K> keyDeserializer,
      Function<byte[], V> valueDeserializer) {
    this.client = client;
    this.address = address;
    this.treeId = treeId;
    this.keySerializer = keySerializer;
    this.keyDeserializer = keyDeserializer;
    this.valueDeserializer = valueDeserializer;
  }

  /**
   * Construct an AvlTreeMap with tree id.
   *
   * @param treeId tree id
   */
  public AvlTreeMap(int treeId) {
    this(treeId, null, null, null, null, null);
  }

  /**
   * Get tree id.
   *
   * @return tree id
   */
  public int treeId() {
    return treeId;
  }

  /**
   * Gets the value associated with the given key. Serializes the key before querying the blockchain
   * for the value mapped in this avl tree. Then deserializes into the returned value.
   *
   * @param key key
   * @return value
   */
  public V get(K key) {
    byte[] keyBytes = keySerializer.apply(key);
    byte[] valueBytes = client.getContractStateAvlValue(address, treeId, keyBytes);
    return valueBytes == null ? null : valueDeserializer.apply(valueBytes);
  }

  /**
   * Gets the next n key value pairs from the given key (exclusive). If the key is null gets the
   * first n entries. Serializes the key before querying the blockchain for the next n values in
   * this avl tree. Then deserializes into the returned key value pairs.
   *
   * @param key key
   * @param n number to get
   * @return n key value pairs
   */
  public List<Map.Entry<K, V>> getNextN(K key, int n) {
    byte[] keyBytes = key == null ? null : keySerializer.apply(key);
    List<Map.Entry<byte[], byte[]>> nextN =
        client.getContractStateAvlNextN(address, treeId, keyBytes, n);
    return nextN.stream()
        .map(
            entry ->
                Map.entry(
                    keyDeserializer.apply(entry.getKey()),
                    valueDeserializer.apply(entry.getValue())))
        .toList();
  }

  /**
   * Deserializes bytes into the key type.
   *
   * @param bytes serialized key bytes
   * @return deserialized key
   */
  public K deserializeKey(byte[] bytes) {
    return keyDeserializer.apply(bytes);
  }

  /**
   * Deserializes bytes into the value type.
   *
   * @param bytes serialized value bytes
   * @return deserialized value
   */
  public V deserializeValue(byte[] bytes) {
    return valueDeserializer.apply(bytes);
  }

  /**
   * Serializes a key into bytes.
   *
   * @param key the key
   * @return serialized bytes
   */
  public byte[] serializeKey(K key) {
    return keySerializer.apply(key);
  }

  /**
   * Get the number of entries of the avl tree. Queries the blockchain for the size directly.
   *
   * @return the size of the avl tree
   */
  public int size() {
    return client.getContractStateAvlSize(address, treeId);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvlTreeMap<?, ?> that = (AvlTreeMap<?, ?>) o;
    return treeId == that.treeId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(treeId);
  }
}
