package com.partisiablockchain.language.codegenlib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.model.AvlStateEntry;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link BlockchainStateClient}, using the openapi client to send http requests.
 */
public final class BlockchainStateClientImpl implements BlockchainStateClient {

  final ChainControllerApi chainController;

  BlockchainStateClientImpl(ChainControllerApi chainController) {
    this.chainController = chainController;
  }

  /**
   * Create a BlockchainStateClient from a chainController.
   *
   * @param chainController underlying chainController
   * @return the new client
   */
  public static BlockchainStateClientImpl create(ChainControllerApi chainController) {
    return new BlockchainStateClientImpl(chainController);
  }

  /**
   * Create a BlockchainStateClient from a base url.
   *
   * @param baseUrl the base url of the target chain
   * @return the new client
   */
  public static BlockchainStateClientImpl create(String baseUrl) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(baseUrl);
    return new BlockchainStateClientImpl(new ChainControllerApi(apiClient));
  }

  @Override
  public byte[] getContractStateBinary(BlockchainAddress address) {
    return ExceptionConverter.call(() -> chainController.getContract(address.writeAsString(), null))
        .getSerializedContract();
  }

  @Override
  public byte[] getContractStateAvlValue(BlockchainAddress address, int treeId, byte[] key) {
    try {
      return chainController
          .getContractAvlValue(address.writeAsString(), treeId, HexFormat.of().formatHex(key), null)
          .getData();
    } catch (ApiException ignored) {
      return null;
    }
  }

  @Override
  public List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n) {
    String keyHex = key == null ? null : HexFormat.of().formatHex(key);
    List<AvlStateEntry> avlEntries =
        ExceptionConverter.call(
            () ->
                chainController.getContractAvlNextN(
                    address.writeAsString(), treeId, keyHex, n, null, null));
    return avlEntries.stream()
        .map(entry -> Map.entry(HexFormat.of().parseHex(entry.getKey()), entry.getValue()))
        .toList();
  }

  @Override
  public int getContractStateAvlSize(BlockchainAddress address, int treeId) {
    return ExceptionConverter.call(
            () -> chainController.getContractAvlInformation(address.writeAsString(), treeId, null))
        .getSize();
  }
}
