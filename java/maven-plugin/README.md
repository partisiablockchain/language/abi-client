# abi-generation-maven-plugin

This maven plugin has the goals `abi-code-gen`, that can automatically generate a java class from an ABI file able to
serialize actions and deserialize state and rpc.

## Compilation errors in IDEA

This project acts as an annotation preprocessor and IDEAs code caches sometimes get confused.
A normal `mvn compile testCompile` fixes the issue, `Build -> Rebuild project` in IDEA does too.

## Usage

First add the relevant plugin repository to your `pom.xml`:

``` xml
<pluginRepositories>
  <pluginRepository>
    <id>secata</id>
    <url>https://gitlab.com/api/v4/groups/12499775/-/packages/maven/</url>
  </pluginRepository>
</pluginRepositories>
 ```

Add the plugin to your `pom.xml`, with the relevant goal:

``` xml
<build>
  <plugins>
    <plugin>
      <groupId>com.partisiablockchain.language</groupId>
      <artifactId>abi-generation-maven-plugin</artifactId>
      <version>INSERT NEWEST VERSION HERE</version>
      <executions>
          <execution>
            <goals>
              <goal>abi-code-gen</goal>
            </goals>
            <configuration>
              <abiPath><!-- path to abi file, required --></abiPath>
              <generateTestSources><!-- defaults to false --></generateTestSources>
              <deserializeRpc><!-- defaults to false --></deserializeRpc>
              <deserializeState><!-- defaults to true --></deserializeState>
              <serializeActions><!-- defaults to true --></serializeActions>
              <arguments>
                <!-- list of additional arguments given to abi-code-gen -->
              </arguments>
            </configuration>
          </execution>
        </executions>
    </plugin>
  </plugins>
</build>
```

Add `codegen-lib` as a dependency:

```xml
<dependency>
  <groupId>com.partisiablockchain.language</groupId>
  <artifactId>codegen-lib</artifactId>
  <version>INSERT NEWEST VERSION HERE</version>
</dependency>
```
