<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <artifactId>abi-generation-maven-plugin</artifactId>
  <packaging>maven-plugin</packaging>

  <licenses>
    <license>
      <name>AGPL-3.0-or-later</name>
      <url>https://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <repositories>
    <repository>
      <id>gitlab-partisiablockchain</id>
      <url>https://gitlab.com/api/v4/groups/12499775/-/packages/maven/</url>
    </repository>
  </repositories>

  <parent>
    <groupId>com.partisiablockchain.language</groupId>
    <artifactId>abi</artifactId>
    <version>5.147.0</version>
    <relativePath>../pom.xml</relativePath>
  </parent>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-plugin-plugin</artifactId>
        <version>3.6.4</version>
        <executions>
          <execution>
            <goals>
              <goal>descriptor</goal>
            </goals>
            <phase>compile</phase>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <id>errorprone</id>
    </profile>
    <profile>
      <id>include-ast</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>${org.apache.maven.plugins.maven-compiler-plugin.version}</version>
            <configuration>
              <showWarnings>true</showWarnings>
              <fork>true</fork>
              <compilerArgs>
                <!-- Don't process the code with the processor we are compiling -->
                <arg>-proc:none</arg>

                <!-- From root pom -->
                <arg>-Xlint:all</arg>
                <arg>-Xlint:-processing</arg>
                <arg>-Xlint:-options</arg>
                <arg>-Werror</arg>
              </compilerArgs>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <dependencies>
    <dependency>
      <groupId>jakarta.annotation</groupId>
      <artifactId>jakarta.annotation-api</artifactId>
      <version>2.1.1</version>
    </dependency>
    <dependency>
      <groupId>com.secata.tools</groupId>
      <artifactId>rest</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-compress</artifactId>
      <version>1.26.0</version>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-lang3</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
      <version>3.8.6</version>
      <scope>provided</scope>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>org.codehaus.plexus</groupId>
          <artifactId>plexus-component-annotations</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.codehaus.plexus</groupId>
          <artifactId>plexus-utils</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.codehaus.plexus</groupId>
          <artifactId>plexus-classworlds</artifactId>
        </exclusion>
        <exclusion>
          <groupId>com.google.inject</groupId>
          <artifactId>guice</artifactId>
        </exclusion>
        <exclusion>
          <groupId>javax.annotation</groupId>
          <artifactId>javax.annotation-api</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-lang3</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.maven.plugin-tools</groupId>
      <artifactId>maven-plugin-annotations</artifactId>
      <version>3.6.4</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-model</artifactId>
      <version>3.8.6</version>
      <scope>provided</scope>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>org.codehaus.plexus</groupId>
          <artifactId>plexus-utils</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-utils</artifactId>
      <version>3.3.1</version>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-core</artifactId>
      <version>3.8.6</version>
      <scope>provided</scope>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-lang3</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.apache.maven.shared</groupId>
          <artifactId>maven-shared-utils</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.codehaus.plexus</groupId>
          <artifactId>plexus-utils</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.eclipse.sisu</groupId>
          <artifactId>org.eclipse.sisu.inject</artifactId>
        </exclusion>
        <exclusion>
          <groupId>com.google.guava</groupId>
          <artifactId>guava</artifactId>
        </exclusion>
        <exclusion>
          <groupId>junit</groupId>
          <artifactId>junit</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.partisiablockchain.core</groupId>
      <artifactId>contract</artifactId>
      <version>5.34.0</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-text</artifactId>
      <version>1.10.0</version>
    </dependency>
    <dependency>
      <groupId>com.google.auto</groupId>
      <artifactId>auto-common</artifactId>
      <version>1.2.1</version>
    </dependency>
    <dependency>
      <groupId>com.partisiablockchain.language</groupId>
      <artifactId>sections</artifactId>
      <version>5.45.0</version>
    </dependency>
    <dependency>
      <groupId>com.google.testing.compile</groupId>
      <artifactId>compile-testing</artifactId>
      <version>0.21.0</version>
      <scope>test</scope>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>com.google.guava</groupId>
          <artifactId>guava</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.checkerframework</groupId>
          <artifactId>checker-qual</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <scope>test</scope>
      <exclusions>
        <!-- Excluded because of dependency convergence -->
        <exclusion>
          <groupId>org.objenesis</groupId>
          <artifactId>objenesis</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.partisiablockchain.language</groupId>
      <artifactId>abi-codegen</artifactId>
      <version>${project.version}</version>
      <scope>compile</scope>
    </dependency>
  </dependencies>
</project>
