package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.auto.common.MoreTypes;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateString;
import java.util.List;
import javax.lang.model.type.TypeMirror;

/** Contains a list of types that are a part of the system. */
public final class BuiltInTypes {

  /** The types that are a part of the system. */
  public static final List<Class<?>> VALUES =
      List.of(
          Byte.class,
          Boolean.class,
          Short.class,
          Integer.class,
          Long.class,
          String.class,
          List.class,
          BlockchainAddress.class,
          Hash.class,
          BlockchainPublicKey.class,
          BlsPublicKey.class,
          Signature.class,
          BlsSignature.class,
          Unsigned256.class);

  /** The state types that are part of the system. */
  public static final List<Class<?>> STATE_VALUES =
      List.of(StateBoolean.class, StateLong.class, StateString.class, LargeByteArray.class);

  /**
   * Test whether the type is built-in.
   *
   * @param typeMirror the type to test
   * @return true if it's built-on, false otherwise.
   */
  public static boolean isBuiltIn(TypeMirror typeMirror) {
    for (Class<?> value : VALUES) {
      if (MoreTypes.isTypeOf(value, typeMirror)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Test whether the type is a built-in state type.
   *
   * @param typeMirror the type to test
   * @return true if it's state built-on, false otherwise.
   */
  public static boolean isStateBuiltIn(TypeMirror typeMirror) {
    for (Class<?> value : STATE_VALUES) {
      if (MoreTypes.isTypeOf(value, typeMirror)) {
        return true;
      }
    }
    return false;
  }

  private BuiltInTypes() {}
}
