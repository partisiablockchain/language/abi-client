package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.AbiCodegen;
import com.partisiablockchain.language.abicodegen.CodegenOptions;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.netrc.DependencyFetcher;
import com.partisiablockchain.language.netrc.NetrcFile;
import com.partisiablockchain.language.netrc.NetrcFile.NetrcEntry;
import com.partisiablockchain.language.netrc.NetrcParser;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.lang3.SystemUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.impl.ArtifactResolver;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;

/** Mojo for generating java code from an ABI file using abi-client's AbiCodeGen. */
@Mojo(name = "abi-code-gen", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public final class AbiCode extends AbstractMojo {

  static final String PATH_TO_CONTRACT_ARTIFACTS = "target/contract-artifacts";

  @Parameter(defaultValue = "${session}", required = true, readonly = true)
  MavenSession session;

  /** Path to abi files to be generated code for. */
  @Parameter String abiPath;

  /** Path to pbc files containing the abi to generate code for. */
  @Parameter String pbcPath;

  /** Whether the code should be generated as test sources instead. */
  @Parameter(property = "abi-code-gen.generateTestSources", defaultValue = "false")
  boolean generateTestSources;

  /** Whether code will be generated to deserialize contract state. */
  @Parameter(defaultValue = "true")
  boolean deserializeState;

  /** Whether code will be generated to deserialize rpc. */
  @Parameter(defaultValue = "false")
  boolean deserializeRpc;

  /** Whether code will be generated to serialize actions. */
  @Parameter(defaultValue = "true")
  boolean serializeActions;

  /** Whether code will be generated to serialize callbacks. */
  @Parameter(defaultValue = "false")
  boolean serializeCallbacks;

  /** List of named types generate serialization code for. */
  @Parameter Set<String> namedTypes;

  /** The package name for which the generated code will be placed in. */
  @Parameter(defaultValue = "com.partisiablockchain.language.abicodegen")
  String packageName;

  /** The class name for the generated code. */
  @Parameter String className;

  /** List of abi dependencies to generate code for. */
  @Parameter() List<AbiDependency> abis;

  /** List of rust dependencies to generate code for. */
  @Parameter() List<RustContractDependency> rustDependencies;

  /** Whether code annotation '@AbiGenerated' should be generated. */
  @Parameter(defaultValue = "true")
  boolean generateAnnotations;

  @Component ArtifactResolver resolver;

  NetrcFile netrcFile = null;

  DependencyFetcher dependencyFetcher = this::fetchRawDependency;

  EnvironmentVariableMap envVar = () -> System.getenv();

  private enum FileType {
    Abi,
    Pbc
  }

  @Override
  public void execute() throws MojoExecutionException {
    if (abiPath != null && abiPath.equals(pbcPath)) {
      throw new MojoExecutionException("<abiPath> and <pbcPath> are not allowed to be the same");
    }

    List<GenerateCode> generateCodes = new ArrayList<>();

    if (abis != null) {
      generateCodes.addAll(resolveAbiFromMavenDependencies());
    }
    if (rustDependencies != null) {
      generateCodes.addAll(resolveAbiFromRustDependencies());
    }
    if (abiPath != null) {
      generateCodes.addAll(readAbiFromFileSystem(FileType.Abi, abiPath));
    }
    if (pbcPath != null) {
      generateCodes.addAll(readAbiFromFileSystem(FileType.Pbc, pbcPath));
    }

    execute(generateCodes);
  }

  private void execute(List<GenerateCode> abiFiles) {

    checkOptions(abiFiles);

    MavenProject project = session.getCurrentProject();
    Set<String> namedTypesToGenerateFor = namedTypes == null ? Set.of() : namedTypes;

    var rootOutputPath =
        project
            .getBasedir()
            .toPath()
            .resolve(
                generateTestSources
                    ? "target/generated-test-sources/generated-abi"
                    : "target/generated-sources/generated-abi");

    final var outputPath = rootOutputPath.resolve(packageName.replace(".", "/"));

    for (var abi : abiFiles) {

      Path outputFile = Path.of(outputPath + "/" + abi.className() + ".java");

      CodegenOptions options = createCodegenOptions(abi.className(), namedTypesToGenerateFor);

      ContractAbi contract = new AbiParser(abi.abi()).parseContractAbi();

      AbiCodegen codeGen = new AbiCodegen(contract, options);

      String s = codeGen.generateCode();
      writeFile(outputFile, s.getBytes(StandardCharsets.UTF_8));
    }

    if (generateTestSources) {
      project.addTestCompileSourceRoot(rootOutputPath.toString());
    } else {
      project.addCompileSourceRoot(rootOutputPath.toString());
    }
  }

  private void checkOptions(List<GenerateCode> abiToGenerateFor) {
    if (className != null) {
      if (abis != null || rustDependencies != null) {
        throw new RuntimeException(
            "Cannot specify an outer classname, when using dependencies, try specifying the"
                + " classname in the dependency.");
      }
      if (abiToGenerateFor.size() > 1) {
        throw new RuntimeException(
            "Cannot specify classname, when there is more than one abi in the folder.");
      }
    }
  }

  private CodegenOptions createCodegenOptions(
      String contractName, Set<String> namedTypesToGenerateFor) {

    return new CodegenOptions(
        CodegenOptions.TargetLanguage.JAVA,
        contractName,
        AbiParser.Strictness.STRICT,
        packageName,
        deserializeState,
        serializeActions,
        serializeCallbacks,
        deserializeRpc,
        namedTypesToGenerateFor,
        generateAnnotations);
  }

  private List<GenerateCode> readAbiFromFileSystem(FileType fileType, String path)
      throws MojoExecutionException {
    List<GenerateCode> abiFiles;
    Path projectFolder = session.getCurrentProject().getBasedir().toPath();
    Path fullAbiPath = projectFolder.resolve(path);

    if (!Files.exists(fullAbiPath)) {
      throw new MojoExecutionException(
          (fileType == FileType.Abi) ? "Abi file does not exist" : "Pbc file does not exist");
    }
    var abiFile = fullAbiPath.toFile();
    var subFiles = abiFile.listFiles();
    if (subFiles == null) {
      subFiles = new File[] {abiFile};
    }
    abiFiles =
        Arrays.stream(subFiles)
            .filter(f -> f.getName().endsWith((fileType == FileType.Abi) ? ".abi" : ".pbc"))
            .map(
                file ->
                    ExceptionConverter.call(
                        () ->
                            new GenerateCode(
                                getContractName(file.getName(), className),
                                (fileType == FileType.Abi)
                                    ? Files.readAllBytes(file.toPath())
                                    : PbcFile.fromBytes(Files.readAllBytes(file.toPath()))
                                        .getAbiBytes())))
            .toList();
    if (abiFiles.isEmpty()) {
      throw new MojoExecutionException(
          (fileType == FileType.Abi)
              ? "Problem with <abiPath>. No ABI file with extension .abi was found."
              : "Problem with <pbcPath>. No PBC file with extension .pbc was found.");
    }
    return abiFiles;
  }

  private List<GenerateCode> resolveAbiFromMavenDependencies() {

    return abis.stream()
        .map(
            abiDependency -> {
              ArtifactRequest request =
                  new ArtifactRequest()
                      .setArtifact(
                          new DefaultArtifact(
                              abiDependency.groupId,
                              abiDependency.artifactId,
                              "abi",
                              "abi",
                              abiDependency.version))
                      .setRepositories(session.getCurrentProject().getRemoteProjectRepositories());

              ArtifactResult result =
                  ExceptionConverter.call(
                      () -> resolver.resolveArtifact(session.getRepositorySession(), request));

              File artifactFile = result.getArtifact().getFile();
              return new GenerateCode(
                  getContractName(
                      String.format("%s-%s.abi", abiDependency.artifactId, abiDependency.version),
                      abiDependency.className),
                  ExceptionConverter.call(() -> Files.readAllBytes(artifactFile.toPath())));
            })
        .toList();
  }

  /**
   * Get code generation for the specified rust dependencies.
   *
   * @return Code generation for the rust dependencies.
   */
  private List<GenerateCode> resolveAbiFromRustDependencies() {
    List<RustContractForGeneration> rustContractsForGeneration = new ArrayList<>();

    for (RustContractDependency rustDependency : rustDependencies) {
      InputStream gzipCompressedFile = dependencyFetcher.fetchDependency(rustDependency.url);

      TarArchiveInputStream tarArchive = decompressRustDependency(gzipCompressedFile);

      List<RustContractForGeneration> rustDependencyContracts =
          convertRustDependency(rustDependency, tarArchive);
      rustContractsForGeneration.addAll(rustDependencyContracts);
    }

    return generateCodeForRustContracts(rustContractsForGeneration);
  }

  /**
   * Fetch a dependency using HTTPS.
   *
   * @param dependencyUrl The url of the dependency to fetch.
   * @return The input stream of the file bytes.
   */
  InputStream fetchRawDependency(String dependencyUrl) {
    Path homeDirectory = Paths.get(SystemUtils.getUserHome().getPath());
    if (netrcFile == null) {
      String netrcLines = getNetrcContents(homeDirectory, envVar.getEnvironmentVariables());
      netrcFile = NetrcParser.parseNetrcFile(netrcLines);
    }

    int responseCode = -1;

    try {
      URL url = new URL(dependencyUrl);
      HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

      String hostname = url.getHost();

      if (netrcFile.containsEntry(hostname)) {
        String authEncoded = getAuthentication(hostname);
        connection.setRequestProperty("Authorization", "Basic " + authEncoded);
      }

      responseCode = connection.getResponseCode();

      return connection.getInputStream();
    } catch (Exception e) {
      if (responseCode == -1) {
        throw new RuntimeException(
            "Error occured while fetching ABI dependency " + dependencyUrl + ", got error: " + e);
      }

      throw new RuntimeException(
          "HTTPS error occured while fetching ABI dependency "
              + dependencyUrl
              + "HTTP error: response code "
              + responseCode);
    }
  }

  /**
   * Get the contents of the netrc file.
   *
   * @param homeDirectory home directory of where to first check for the .netrc file
   * @param systemEnv system environment to look afterward for the netrc path
   * @return The contents of the netrc file
   */
  static String getNetrcContents(Path homeDirectory, Map<String, String> systemEnv) {

    boolean envVarExists = systemEnv.containsKey("NETRC");
    String netrcPath;
    if (envVarExists) {
      netrcPath = systemEnv.get("NETRC");
    } else {
      netrcPath = homeDirectory.resolve(".netrc").toString();
    }

    return ExceptionConverter.call(() -> Files.readString(Path.of(netrcPath)));
  }

  /**
   * Get the authentication credentials at the given host. The credentials are in the format
   * "username:password", and are encoded in Base64.
   *
   * @return The credentials.
   */
  String getAuthentication(String hostname) {
    NetrcEntry netrcEntry = netrcFile.getEntry(hostname);

    String login = netrcEntry.login();
    if (login == null) {
      throw new RuntimeException("Login not set in netrc file");
    }

    String password = netrcEntry.password();
    if (password == null) {
      throw new RuntimeException("Password not set in netrc file");
    }

    String authString = login + ":" + password;

    return Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8));
  }

  /**
   * Decompress a tar+gzip compressed rust dependency.
   *
   * @param gzipCompressedFile The file bytes to decompress.
   * @return The decompressed file bytes.
   */
  TarArchiveInputStream decompressRustDependency(InputStream gzipCompressedFile) {
    GzipCompressorInputStream gzi =
        ExceptionConverter.call(
            () -> new GzipCompressorInputStream(gzipCompressedFile),
            "Could not decompress the given file");

    return new TarArchiveInputStream(gzi);
  }

  /**
   * Convert rust dependencies to their internal representation {@link RustContractForGeneration}.
   *
   * @param rustDependency The rust dependency.
   * @param inputStream The files from the rust dependency.
   * @return The rust contracts to generate code for, from the dependency.
   */
  private List<RustContractForGeneration> convertRustDependency(
      RustContractDependency rustDependency, TarArchiveInputStream inputStream) {
    return ExceptionConverter.call(
        () -> {
          List<RustContractForGeneration> contractForGeneration = new ArrayList<>();
          TarArchiveEntry file;
          Map<String, Path> pbcFiles = new HashMap<>();
          Path savePath =
              session.getCurrentProject().getBasedir().toPath().resolve(PATH_TO_CONTRACT_ARTIFACTS);
          Files.createDirectories(savePath);
          while ((file = inputStream.getNextEntry()) != null) {
            Path artifactPath = savePath.resolve(file.getName());
            Files.copy(inputStream, artifactPath, StandardCopyOption.REPLACE_EXISTING);
            if (file.getName().endsWith(".pbc")) {
              String fileName = file.getName().replaceFirst(".pbc", "");
              pbcFiles.put(fileName, artifactPath);
            }
          }

          if (rustDependency.contracts == null) {
            for (Entry<String, Path> entry : pbcFiles.entrySet()) {
              contractForGeneration.add(
                  new RustContractForGeneration(
                      convertToCamelCase(entry.getKey()), Files.readAllBytes(entry.getValue())));
            }
          } else {
            List<RustContract> contractsMissingInFolder = new ArrayList<>(rustDependency.contracts);
            for (RustContract rustContract : rustDependency.contracts) {
              String contractName =
                  rustContract.contractName.toLowerCase(Locale.getDefault()).replace("-", "_");

              if (pbcFiles.containsKey(contractName)) {
                contractsMissingInFolder.remove(rustContract);
                String classNameToUse =
                    rustContract.className != null
                        ? rustContract.className
                        : convertToCamelCase(contractName);
                contractForGeneration.add(
                    new RustContractForGeneration(
                        classNameToUse, Files.readAllBytes(pbcFiles.get(contractName))));
              }
            }
            if (!contractsMissingInFolder.isEmpty()) {
              throw new RuntimeException(
                  "The contracts "
                      + contractsMissingInFolder.stream()
                          .map(rustContract -> rustContract.contractName)
                          .collect(Collectors.joining(", "))
                      + " were not found");
            }
          }
          return contractForGeneration;
        });
  }

  private List<GenerateCode> generateCodeForRustContracts(
      List<RustContractForGeneration> rustContractsForGeneration) {
    ArrayList<GenerateCode> generateCodes = new ArrayList<>();
    for (RustContractForGeneration rustContractForGeneration : rustContractsForGeneration) {
      String fileName = rustContractForGeneration.fileName;

      byte[] abiFileBytes = PbcFile.fromBytes(rustContractForGeneration.fileBytes).getAbiBytes();
      generateCodes.add(new GenerateCode(fileName, abiFileBytes));
    }
    return generateCodes;
  }

  private static String getContractName(String fileName, String className) {
    if (className != null) {
      return className;
    }

    var endIndex = fileName.lastIndexOf(".");
    var name = fileName.substring(0, endIndex);
    name = convertToCamelCase(name);
    name = name.replace("-", "_");
    name = name.replace(".", "_");
    return name;
  }

  private static String convertToCamelCase(String snakeCase) {
    return Arrays.stream(snakeCase.split("_"))
        .map(str -> str.substring(0, 1).toUpperCase(Locale.ROOT) + str.substring(1))
        .collect(Collectors.joining());
  }

  private static void writeFile(Path path, byte[] bytes) {
    ExceptionConverter.run(
        () -> {
          // If parent exists, ensure directories are created.
          var parent = path.getParent();

          Files.createDirectories(parent);

          Files.write(path, bytes);
        },
        "Unable to write file");
  }

  @SuppressWarnings("ArrayRecordComponent")
  private record GenerateCode(String className, byte[] abi) {}

  /** A contract ABI dependency, used for specifying Abi artifacts to generate code for. */
  public static final class AbiDependency {

    /** The group id of the maven artifact. */
    public String groupId;

    /** The artifact id of the maven artifact. */
    public String artifactId;

    /** The version of the maven artifact. */
    public String version;

    /** The name to use for the generated java class. */
    public String className;
  }

  /** A rust contract dependency, used for specifying rust contracts to generate code for. */
  public static final class RustContractDependency {

    /** The url from which to get the contracts. */
    public String url;

    /**
     * The list of contracts to generate code for. If this is null, code will be generated for all
     * contracts in the dependency .
     */
    public List<RustContract> contracts;
  }

  /** A rust contract to generate code for. */
  public static final class RustContract {

    /**
     * The file name of the contract, without file extensions. Here we ignore capitalization, and
     * allow for using '-' and '_' interchangeably.
     */
    public String contractName;

    /** The classname to give the generated contract file and class. */
    public String className;
  }

  @SuppressWarnings("ArrayRecordComponent")
  private record RustContractForGeneration(String fileName, byte[] fileBytes) {}

  @FunctionalInterface
  interface EnvironmentVariableMap {

    /**
     * Get an environment variable.
     *
     * @return the map of the envirentment variables.
     */
    Map<String, String> getEnvironmentVariables();
  }
}
