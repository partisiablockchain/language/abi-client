package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.CompilationFailedException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.RecordComponentElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/** Validates RPC types at compile time. */
public final class RpcTypeValidation {

  private final TypeMirror stateType;
  private final Types types;
  private Set<Element> validatedRecords = new HashSet<>();

  /**
   * Construct an RPC type validator.
   *
   * @param types the types utility
   * @param stateType the contract state type mirror
   */
  public RpcTypeValidation(Types types, TypeMirror stateType) {
    this.types = types;
    this.stateType = stateType;
  }

  /**
   * Validate the parameter types for the method.
   *
   * @param method the method to process
   */
  public void validateMethodParameters(ExecutableElement method) {
    List<? extends Element> parameters = method.getParameters();
    List<AstType> parameterTypes = typesFromElements(parameters);

    for (int i = 0; i < parameters.size(); i++) {
      Element parameter = parameters.get(i);
      AstType parameterType = parameterTypes.get(i);

      boolean isSystemParam =
          parameterType.isTypeOf(stateType) || parameterType.isPartOfRpcSignature();
      if (!isSystemParam) {
        validateParameter(parameter, parameterType);
      }
    }
  }

  /**
   * Validate a single method parameter.
   *
   * @param parameter the parameter to validate
   * @param paramType the type of the parameter
   */
  void validateParameter(Element parameter, AstType paramType) {
    RpcTypeMirror rpcType = RpcTypeMirror.fromElement(parameter);
    validate(parameter, paramType, rpcType);

    if (paramType.isList()) {
      TypeMirror generic = paramType.extractGeneric();
      validateParameter(types.asElement(generic), createAstType(generic));
    } else if (paramType.isRecord()) {
      TypeElement recordElement = paramType.asTypeElementUnsafe();

      if (validatedRecords.add(recordElement)) {
        List<? extends RecordComponentElement> components = recordElement.getRecordComponents();
        for (RecordComponentElement component : components) {
          TypeMirror componentType = component.asType();
          validateParameter(component, createAstType(componentType));
        }
      }
    } else if (isUnknown(paramType)) {
      throw new CompilationFailedException(parameter, "Invalid type: %s", paramType.getWrapped());
    }
  }

  private AstType createAstType(TypeMirror generic) {
    return new AstType(generic, types);
  }

  private boolean isUnknown(AstType type) {
    if (type.isEnum() || type.isNotDeclared()) {
      return false;
    } else {
      boolean builtIn = type.isBuiltInType();
      return !builtIn;
    }
  }

  private List<AstType> typesFromElements(List<? extends Element> parameters) {
    return parameters.stream().map(Element::asType).map(this::createAstType).toList();
  }

  void validate(Element parameter, AstType parameterType, RpcTypeMirror rpcType) {
    if (rpcType.nullable()) {
      assertNullableAllowed(parameter, parameterType);
    }

    if (rpcType.size() != -1
        && !parameterType.isByteArray()
        && !AbiTypes.isCoercionAllowed(parameterType, rpcType)) {
      throw new CompilationFailedException(
          parameter,
          "Cannot coerce type wanted ABI type '%s' into declared type '%s'",
          rpcType,
          parameterType.getWrapped());
    }
  }

  /**
   * Fail the compilation if the given type is allowed to be nullable.
   *
   * @param parameter the parameterN
   * @param type the type
   */
  private void assertNullableAllowed(Element parameter, AstType type) {
    if (type.isPrimitive() || type.isList() || type.isByteArray()) {
      throw new CompilationFailedException(
          parameter, "Type '%s' cannot be nullable.", type.getWrapped());
    }
  }
}
