package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A simple code writer that helps handle nested scopes and produces nicely indented source code.
 * The code writer supports using {@link Templates}.
 */
final class CodeWriter {

  private final Scope rootScope;

  CodeWriter() {
    rootScope = new Scope(null, new ArrayList<>());
  }

  Scope getRootScope() {
    return rootScope;
  }

  String getGenerated() {
    return rootScope.writeAsString(0);
  }

  /** A piece of writeable code. */
  private interface Code {}

  /**
   * A statement or an expression.
   *
   * @param code the literal code
   */
  @SuppressWarnings("unused")
  private record StatementOrExpression(String code) implements Code {}

  /**
   * A code scope like an if, a method or a case.
   *
   * @param beginningCode be start code of the scope, must end with an open bracket {
   * @param statements the code within the scope
   */
  record Scope(String beginningCode, List<Code> statements) implements Code {

    /**
     * Add a statement to the scope using a template string replacing a named key with a value.
     *
     * @param template the template
     * @param k1 the template key
     * @param v1 the value
     * @return this scope
     */
    Scope addStatement(String template, String k1, Object v1) {
      return addStatement(Templates.replace(template, Map.of(k1, v1)));
    }

    /**
     * Add a statement to the scope using a template string replacing two named keys with a value.
     *
     * @param template the template
     * @param k1 the first template key
     * @param v1 the first value
     * @param k2 the second template key
     * @param v2 the second value
     * @return this scope
     */
    Scope addStatement(String template, String k1, Object v1, String k2, Object v2) {
      return addStatement(Templates.replace(template, Map.of(k1, v1, k2, v2)));
    }

    /**
     * Add a code literal to this scope.
     *
     * @param code the code to add
     * @return this scope
     */
    Scope addStatement(String code) {
      return addCode(new StatementOrExpression(code));
    }

    /**
     * Add a line break to this scope.
     *
     * @return this scope
     */
    Scope newLine() {
      return addStatement(System.lineSeparator());
    }

    private Scope addCode(Code statementOrExpression) {
      statements.add(statementOrExpression);
      return this;
    }

    /**
     * Add a new scope within this scope.
     *
     * @param beginningCode the beginning statement of the scope, must end with an open bracket {
     * @return the new scope
     */
    Scope addScope(String beginningCode) {
      var scope = new Scope(beginningCode, new ArrayList<>());
      addCode(scope);
      return scope;
    }

    /**
     * Add a new scope within this scope using a template string replacing a named template with a
     * value.
     *
     * @param beginningCodeTemplate the template
     * @param k1 the template key
     * @param v1 the value
     * @return the new scope
     */
    Scope addScope(String beginningCodeTemplate, String k1, Object v1) {
      return addScope(Templates.replace(beginningCodeTemplate, Map.of(k1, v1)));
    }

    private String writeAsString(int indent) {
      var out = new StringBuilder();
      for (Code statement : statements()) {
        if (statement instanceof Scope scope) {
          out.append(scope.beginningCode().indent(indent * 2));
          out.append(scope.writeAsString(indent + 1));
          out.append("}".indent(indent * 2)).append(System.lineSeparator());
        } else {
          StatementOrExpression expression = (StatementOrExpression) statement;
          var code = expression.code();
          if (code.equals(System.lineSeparator())) {
            // string.indent removes newlines
            out.append(code);
          } else {
            out.append(code.indent(indent * 2));
          }
        }
      }
      return out.toString();
    }
  }
}
