package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.auto.common.MoreTypes;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.language.ast.AbiMetaFromAst;
import com.partisiablockchain.language.ast.ActionExecutableElement;
import com.partisiablockchain.language.ast.AstType;
import com.partisiablockchain.language.ast.RpcTypeValidation;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

/**
 * Generate an ABI and type hints from a java system smart contract with annotations from {@link
 * com.partisiablockchain.contract.reflect}.
 */
final class GenerateAbiFromAnnotation {

  private final Types types;
  private final Elements elements;

  private final TypeElement contractElement;
  private final TypeMirror stateType;

  GenerateAbiFromAnnotation(
      Types types, Elements elements, TypeElement contractElement, TypeMirror stateType) {
    this.elements = elements;
    this.types = types;
    this.contractElement = contractElement;
    this.stateType = stateType;
  }

  AbiWithTypeHint generate() {
    ActionExecutableElement init = parseAndValidateInit();
    ActionExecutableElement upgrade = parseAndValidateUpgrade();

    List<ActionExecutableElement> actionMap = parseAndValidateActions();
    List<ActionExecutableElement> callbackMap = parseAndValidateCallbacks();

    AbiMetaFromAst abiProcessor = new AbiMetaFromAst(types, stateType);
    abiProcessor.collectTypes(List.of(init));
    abiProcessor.collectTypes(actionMap);
    abiProcessor.collectTypes(callbackMap);
    abiProcessor.collectUpgradeTypes(upgrade);
    abiProcessor.disambiguateNames();

    // Build ABI
    abiProcessor.addInit(init.method());
    abiProcessor.addUpgrade(upgrade.method());
    abiProcessor.addActions(actionMap);
    abiProcessor.addCallbacks(callbackMap);

    String simpleName = contractElement.getSimpleName().toString();
    String packageName = elements.getPackageOf(contractElement).getQualifiedName().toString();

    return abiProcessor.toFileAbi(packageName, simpleName);
  }

  private int countAnnotatedWith(
      List<? extends Element> elements, Class<? extends Annotation> annotation) {
    var count = 0;
    for (Element element : elements) {
      if (element.getAnnotation(annotation) != null) {
        count++;
      }
    }
    return count;
  }

  private ActionExecutableElement parseAndValidateUpgrade() {
    validateOnlyOneAnnotatedElement(Upgrade.class);

    ExecutableElement method = findAnnotatedMethods(contractElement, Upgrade.class).get(0);
    validateReturnType(method, "upgrade");
    validateModifiers(method, "Upgrade");
    validateParameterIndicesForUpgrade(method, method.getParameters());
    validateRpcTypes(method);

    return new ActionExecutableElement(-1, method);
  }

  private ActionExecutableElement parseAndValidateInit() {
    validateOnlyOneAnnotatedElement(Init.class);

    ExecutableElement method = findAnnotatedMethods(contractElement, Init.class).get(0);
    validateReturnType(method, "init");
    validateModifiers(method, "Init");
    validateParameterIndicesForInit(stateType, method.getParameters());
    validateRpcTypes(method);

    return new ActionExecutableElement(-1, method);
  }

  private void validateRpcTypes(ExecutableElement method) {
    var typeValidation = new RpcTypeValidation(types, stateType);
    typeValidation.validateMethodParameters(method);
  }

  private List<ActionExecutableElement> parseAndValidateActions() {
    Map<Integer, ExecutableElement> actions = new TreeMap<>();
    for (ExecutableElement method : findAnnotatedMethods(contractElement, Action.class)) {
      Name actionName = method.getSimpleName();
      Action actionAnnotation = method.getAnnotation(Action.class);
      int shortname = actionAnnotation.value();
      if (shortname < 0 || shortname >= 128) {
        throw new CompilationFailedException(
            method,
            "Shortname on action '%s' must be in range 0 to 127, but was: %d",
            actionName,
            shortname);
      }

      if (actions.containsKey(shortname)) {
        Element otherElement = actions.get(shortname);
        throw new CompilationFailedException(
            method,
            "Duplicate shortname %d on '%s'. First defined on action '%s'.",
            shortname,
            actionName,
            otherElement.getSimpleName());
      }

      validateReturnType(method, "action");
      validateModifiers(method, "Action");
      validateParameterIndicesForActions(stateType, method.getParameters());
      validateRpcTypes(method);

      actions.put(shortname, method);
    }

    return shortnameMapToList(actions);
  }

  private List<ActionExecutableElement> parseAndValidateCallbacks() {
    Map<Integer, ExecutableElement> callbacks = new TreeMap<>();
    for (ExecutableElement method : findAnnotatedMethods(contractElement, Callback.class)) {
      Name actionName = method.getSimpleName();
      Callback annotation = method.getAnnotation(Callback.class);
      int shortname = annotation.value();
      if (shortname < 0 || shortname >= 128) {
        throw new CompilationFailedException(
            method,
            "Shortname on callback '%s' must be in range 0 to 127, but was: %d",
            actionName,
            shortname);
      }

      if (callbacks.containsKey(shortname)) {
        Element otherElement = callbacks.get(shortname);
        throw new CompilationFailedException(
            method,
            "Duplicate callback %d on '%s'. First defined on action '%s'.",
            shortname,
            actionName,
            otherElement.getSimpleName());
      }

      validateReturnType(method, "callback");
      validateModifiers(method, "Callback");
      validateRpcTypes(method);
      validateParameterIndicesForCallbacks(method.getParameters());

      callbacks.put(shortname, method);
    }
    return shortnameMapToList(callbacks);
  }

  private List<ActionExecutableElement> shortnameMapToList(Map<Integer, ExecutableElement> map) {
    return map.entrySet().stream()
        .map(
            entry -> {
              int shortname = entry.getKey();
              ExecutableElement method = entry.getValue();
              return new ActionExecutableElement(shortname, method);
            })
        .toList();
  }

  private List<ExecutableElement> findAnnotatedMethods(
      TypeElement element, Class<? extends Annotation> annotation) {
    return element.getEnclosedElements().stream()
        .filter(e -> e.getAnnotation(annotation) != null)
        .map(ExecutableElement.class::cast)
        .toList();
  }

  private void validateOnlyOneAnnotatedElement(Class<? extends Annotation> annotation) {
    int count = countAnnotatedWith(contractElement.getEnclosedElements(), annotation);
    if (count != 1) {
      throw new CompilationFailedException(
          contractElement,
          "The contract must have exactly 1 method annotated with @%s",
          annotation.getSimpleName());
    }
  }

  /**
   * Check that the action method returns the state type.
   *
   * @param actionMethod the method to check
   * @param methodType the method type action or callback
   */
  private void validateReturnType(ExecutableElement actionMethod, String methodType) {
    Name actionName = actionMethod.getSimpleName();

    if (!createAstType(actionMethod.getReturnType()).isTypeOf(stateType)) {
      throw new CompilationFailedException(
          actionMethod,
          "Expected return type '%s' on %s method '%s', but got '%s'",
          stateType,
          methodType,
          actionName,
          actionMethod.getReturnType());
    }
  }

  /**
   * Validate that the annotation action method has the relevant modifiers.
   *
   * @param method the method to test
   * @param methodType the type of method
   */
  private void validateModifiers(ExecutableElement method, String methodType) {
    Set<Modifier> modifiers = method.getModifiers();
    boolean isPublic = modifiers.size() == 1 && modifiers.contains(Modifier.PUBLIC);
    if (!isPublic) {
      String modifiersAsText;
      if (modifiers.isEmpty()) {
        modifiersAsText = "package-private";
      } else {
        modifiersAsText =
            modifiers.stream().map(Objects::toString).collect(Collectors.joining(" "));
      }

      throw new CompilationFailedException(
          method,
          "%s method '%s' must be public non-static, but was '%s'.",
          methodType,
          method.getSimpleName(),
          modifiersAsText);
    }
  }

  /**
   * Validate that the {@link SysContractContext} and rpc stream are at valid indices.
   *
   * @param stateType the state type
   * @param parameters the parameters
   */
  private void validateParameterIndicesForInit(
      TypeMirror stateType, List<? extends VariableElement> parameters) {
    final int allowedContextIndex = 0;

    for (int i = 0; i < parameters.size(); i++) {
      VariableElement param = parameters.get(i);
      AstType paramType = createAstType(param.asType());

      if (paramType.isSysContext()) {
        validateParameterIndex(param, param.getSimpleName(), allowedContextIndex, i);
      } else if (paramType.isTypeOf(stateType)) {
        throw new CompilationFailedException(
            param, "Init method cannot have the state as a parameter");
      } else if (paramType.isRpcStream()) {
        validateParameterIndex(param, param.getSimpleName(), parameters.size() - 1, i);
      }
    }
  }

  /**
   * Validate that the {@link SysContractContext}, state and rpc stream are at valid indices.
   *
   * @param stateType the state type
   * @param parameters the parameters
   */
  private void validateParameterIndicesForActions(
      TypeMirror stateType, List<? extends VariableElement> parameters) {
    final int allowedContextIndex = 0;
    int allowedStateIndex = 0;

    if (hasSysContext(parameters)) {
      allowedStateIndex = 1;
    }

    for (int i = 0; i < parameters.size(); i++) {
      VariableElement param = parameters.get(i);
      AstType paramType = createAstType(param.asType());

      if (paramType.isSysContext()) {
        validateParameterIndex(param, param.getSimpleName(), allowedContextIndex, i);
      } else if (paramType.isTypeOf(stateType)) {
        validateParameterIndex(param, param.getSimpleName(), allowedStateIndex, i);
      } else if (paramType.isRpcStream()) {
        validateParameterIndex(param, param.getSimpleName(), parameters.size() - 1, i);
      }
    }
  }

  /**
   * Validate that the {@link SysContractContext}, state and rpc stream are at valid indices.
   *
   * @param parameters the parameters
   */
  private void validateParameterIndicesForUpgrade(
      ExecutableElement method, List<? extends VariableElement> parameters) {
    if (parameters.isEmpty() || !createAstType(parameters.get(0).asType()).isStateAccessor()) {
      throw new CompilationFailedException(
          method, "Upgrade method must have a StateAccessor as the first parameter.");
    }

    for (int i = 0; i < parameters.size(); i++) {
      VariableElement param = parameters.get(i);
      AstType paramType = createAstType(param.asType());

      if (paramType.isTypeOf(stateType)) {
        throw new CompilationFailedException(
            param, "Upgrade method cannot have the state as a parameter");
      } else if (paramType.isRpcStream()) {
        validateParameterIndex(param, param.getSimpleName(), parameters.size() - 1, i);
      }
    }
  }

  private AstType createAstType(TypeMirror type) {
    return new AstType(type, types);
  }

  /**
   * Validate that the {@link SysContractContext}, state and rpc stream are at valid indices.
   *
   * @param parameters the parameters
   */
  private void validateParameterIndicesForCallbacks(List<? extends VariableElement> parameters) {
    final int allowedSysContextIndex = 0;

    int allowedStateIndex = 0;

    if (hasSysContext(parameters)) {
      allowedStateIndex = 1;
    }

    int allowedCallbackContextIndex = allowedStateIndex;
    if (hasParameterOfType(parameters, stateType)) {
      allowedCallbackContextIndex = allowedStateIndex + 1;
    }

    for (int i = 0; i < parameters.size(); i++) {
      VariableElement param = parameters.get(i);
      AstType paramType = createAstType(param.asType());

      if (paramType.isSysContext()) {
        validateParameterIndex(param, param.getSimpleName(), allowedSysContextIndex, i);
      } else if (paramType.isTypeOf(stateType)) {
        validateParameterIndex(param, param.getSimpleName(), allowedStateIndex, i);
      } else if (paramType.isCallbackContext()) {
        validateParameterIndex(param, param.getSimpleName(), allowedCallbackContextIndex, i);
      } else if (paramType.isRpcStream()) {
        validateParameterIndex(param, param.getSimpleName(), parameters.size() - 1, i);
      }
    }
  }

  private boolean hasParameterOfType(List<? extends VariableElement> parameters, TypeMirror type) {
    for (VariableElement param : parameters) {
      AstType astType = createAstType(param.asType());
      if (astType.isTypeOf(type)) {
        return true;
      }
    }
    return false;
  }

  private void validateParameterIndex(
      Element element, Name parameterName, int allowedIndex, int actualIndex) {
    if (allowedIndex != actualIndex) {
      throw new CompilationFailedException(
          element,
          "The %s parameter was expected at index %d but was at index %d.",
          parameterName,
          allowedIndex,
          actualIndex);
    }
  }

  private boolean hasSysContext(List<? extends VariableElement> parameters) {
    for (VariableElement parameter : parameters) {
      AstType astType = createAstType(parameter.asType());
      if (astType.isSysContext()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Extracts the contract state as a {@link TypeElement} from the contract element.
   *
   * @param contractElement the contract element
   * @return the type element
   */
  static TypeMirror extractStateType(TypeElement contractElement) {
    List<? extends AnnotationMirror> annotationMirrors = contractElement.getAnnotationMirrors();
    for (AnnotationMirror annotationMirror : annotationMirrors) {
      if (MoreTypes.isTypeOf(AutoSysContract.class, annotationMirror.getAnnotationType())) {
        AnnotationValue annotationValue =
            annotationMirror.getElementValues().values().iterator().next();
        return (TypeMirror) annotationValue.getValue();
      }
    }
    return null;
  }
}
