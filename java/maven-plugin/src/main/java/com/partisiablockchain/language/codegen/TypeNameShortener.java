package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.BuiltInTypes;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/** A small utility that simplifies type names for known and/or built-in types. */
final class TypeNameShortener {

  private static final Pattern PATTERN_GENERIC =
      Pattern.compile("^(?<base>[\\w\\\\.]+)<(?<genericParams>.*)>$");

  private static final Map<String, String> simpleNames =
      BuiltInTypes.VALUES.stream()
          .collect(Collectors.toMap(Class::getCanonicalName, Class::getSimpleName));

  private TypeNameShortener() {}

  /**
   * Simplify the type name and use simple names instead of fully qualified where applicable.
   *
   * @param typeName the type name
   * @return a string where all applicable types are listed using their simple name
   */
  static String shorten(String typeName) {
    TypeName parsed = parse(typeName);
    return parsed.recursivelyReplace(simpleNames).toJava();
  }

  private static TypeName parse(String string) {
    if (isGeneric(string)) {
      return parseGeneric(string);
    } else {
      return new TypeName(string, List.of());
    }
  }

  /**
   * Parse a type spec with generics.
   *
   * @param string the string to parse
   * @return the resulting type
   */
  static TypeName parseGeneric(String string) {
    Matcher matcher = PATTERN_GENERIC.matcher(string);

    if (!matcher.matches()) {
      return null;
    }

    String genericString = matcher.group("genericParams");
    final List<TypeName> generics =
        semanticSplitAtComma(genericString).stream().map(TypeNameShortener::parse).toList();

    String base = matcher.group("base");
    return new TypeName(base, generics);
  }

  private static boolean isGeneric(String string) {
    Matcher generic = PATTERN_GENERIC.matcher(string);
    return generic.matches();
  }

  /**
   * Split the generic part of a type description at the correct comma. The naive approach does not
   * work for nested generics such as Map&lt;String, Map&lt;String, String&gt;&gt;.
   *
   * @param generic the generic part of a type
   * @return the list of types
   */
  private static List<String> semanticSplitAtComma(String generic) {
    int splitIndex = findSemanticSplitIndex(generic);

    List<String> split;
    if (splitIndex != -1) {
      String left = generic.substring(0, splitIndex).trim();
      String right = generic.substring(splitIndex + 1).trim();
      split = List.of(left, right);
    } else {
      split = List.of(generic);
    }
    return split;
  }

  /**
   * Find the index at which the part between &lt; and &gt; should be split.
   *
   * @param generic the generic to split
   * @return the split index
   */
  private static int findSemanticSplitIndex(String generic) {
    int nesting = 0;
    for (int i = 0; i < generic.length(); i++) {
      char c = generic.charAt(i);
      if (c == '<') {
        nesting++;
      } else if (c == '>') {
        nesting--;
      } else if (c == ',' && nesting == 0) {
        return i;
      }
    }
    return -1;
  }

  record TypeName(String name, List<TypeName> generics) {

    TypeName recursivelyReplace(Map<String, String> replacements) {
      List<TypeName> newGenerics =
          generics().stream().map(t -> t.recursivelyReplace(replacements)).toList();

      return new TypeName(replace(name(), replacements), newGenerics);
    }

    String toJava() {
      if (generics.isEmpty()) {
        return name;
      } else {
        List<String> genericParts = generics.stream().map(TypeName::toJava).toList();
        return "%s<%s>".formatted(name, String.join(", ", genericParts));
      }
    }

    private String replace(String input, Map<String, String> replacements) {
      return replacements.getOrDefault(input, input);
    }
  }
}
