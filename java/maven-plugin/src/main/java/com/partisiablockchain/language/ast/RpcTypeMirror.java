package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.reflect.RpcType;
import javax.lang.model.element.Element;

/**
 * A copy of {@link com.partisiablockchain.contract.reflect.RpcType} we can easily construct.
 *
 * @param nullable whether the type is nullable
 * @param signed whether the type is signed
 * @param size the size of the type in bytes
 * @see RpcType
 */
record RpcTypeMirror(boolean nullable, boolean signed, int size) {

  static RpcTypeMirror fromElement(Element element) {
    RpcType annotation = element.getAnnotation(RpcType.class);
    if (annotation == null) {
      return new RpcTypeMirror(false, true, -1);
    } else {
      return new RpcTypeMirror(annotation.nullable(), annotation.signed(), annotation.size());
    }
  }
}
