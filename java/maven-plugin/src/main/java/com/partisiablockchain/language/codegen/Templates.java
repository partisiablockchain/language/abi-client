package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.AutoSysContractProcessor;
import java.util.List;
import java.util.Map;
import org.apache.commons.text.StringSubstitutor;

/**
 * Code generation templates using commons-text syntax for variable substitution.
 *
 * @see org.apache.commons.text.StringSubstitutor
 */
final class Templates {

  private static final Template SYS_CONTRACT_HEADER =
      wrap(
          """
          package ${package};

          import com.partisiablockchain.contract.reflect.*;

          import com.partisiablockchain.BlockchainAddress;
          import com.partisiablockchain.crypto.BlockchainPublicKey;
          import com.partisiablockchain.crypto.BlsPublicKey;
          import com.partisiablockchain.crypto.BlsSignature;
          import com.partisiablockchain.crypto.Hash;
          import com.partisiablockchain.crypto.Signature;
          import com.partisiablockchain.math.Unsigned256;
          import java.util.List;
          import java.util.ArrayList;

          import com.partisiablockchain.contract.CallbackContext;
          import com.partisiablockchain.contract.sys.SysContract;
          import com.partisiablockchain.contract.sys.SysContractContext;
          import com.partisiablockchain.serialization.StateAccessor;

          import com.secata.stream.SafeDataInputStream;

          import ${import};
          """);

  private static final Template SYS_CONTRACT_DECLARE =
      wrap(
          """
          /**
           * Generated invoker for contract {@link ${delegate}}.
           */
          @com.partisiablockchain.contract.reflect.Generated
          public final class ${cls} extends SysContract<${state}> {

            private final ${delegate} delegate;

            /**
             * Default constructor.
             */
            public ${cls}() {
              try {
                this.delegate = new ${delegate}();
              } catch (Exception e) {
                throw new RuntimeException(e);
              }
            }

          """);

  private Templates() {}

  /**
   * Wrap the string template into a {@link Template} object.
   *
   * @param template the string template
   * @return the rich template object
   */
  private static Template wrap(String template) {
    return values -> {
      var templateEngine = new StringSubstitutor(values);
      templateEngine.setEnableSubstitutionInVariables(true);
      templateEngine.setEnableUndefinedVariableException(true);

      return templateEngine.replace(template);
    };
  }

  static String rpcRead(CharSequence type, String variableName, String method, String args) {
    Map<String, Object> vars =
        Map.of(
            "type", type,
            "variableName", variableName,
            "readMethod", method,
            "args", args);

    return wrap("${type} ${variableName} = rpc.${readMethod}(${args});").replace(vars);
  }

  static String readNamedType(String type, String variable, String method) {
    Map<String, Object> vars =
        Map.of(
            "type", type,
            "variable", variable,
            "method", method);

    return wrap("${type} ${variable} = ${method}(rpc);").replace(vars);
  }

  static String readUnsignedInt(CharSequence type, String variableName) {
    Map<String, Object> vars =
        Map.of(
            "type", type,
            "variableName", variableName);

    return wrap("${type} ${variableName} = (${type}) (rpc.readInt() & 0xFFFFFFFFL);").replace(vars);
  }

  static String readUnsigned256(CharSequence type, String variableName, int byteCount) {
    Map<String, Object> vars =
        Map.of(
            "type", type,
            "variableName", variableName,
            "byteCount", byteCount);

    return wrap("${type} ${variableName} = Unsigned256.create(rpc.readBytes(${byteCount}));")
        .replace(vars);
  }

  static String staticRead(CharSequence type, String variableName) {
    Map<String, Object> vars = Map.of("type", type, "variableName", variableName);
    return wrap("${type} ${variableName} = ${type}.read(rpc);").replace(vars);
  }

  public static String sysContractClassDeclaration(String contractName, String stateName) {
    Map<String, Object> vars =
        Map.of(
            "generator",
            AutoSysContractProcessor.class.getCanonicalName(),
            "cls",
            contractName + "Invoker",
            "delegate",
            contractName,
            "state",
            stateName);

    return SYS_CONTRACT_DECLARE.replace(vars);
  }

  static String sysContractHeader(String contractPackage, String stateName) {
    Map<String, Object> vars =
        Map.of(
            "package", contractPackage,
            "import", stateName,
            "generator", AutoSysContractProcessor.class.getName());

    return SYS_CONTRACT_HEADER.replace(vars);
  }

  public static String forLoopToCount(String lengthName, String counterName) {
    Map<String, Object> vars =
        Map.of(
            "counterName", counterName,
            "lengthName", lengthName);

    return wrap("for (int ${counterName} = 0; ${counterName} < ${lengthName}; ${counterName}++) {")
        .replace(vars);
  }

  public static String listDeclare(
      CharSequence type, String variableName, String counterName, String lengthName) {
    Map<String, Object> vars =
        Map.of(
            "type",
            type,
            "variableName",
            variableName,
            "counterName",
            counterName,
            "lengthName",
            lengthName);
    return wrap("List<${type}> ${variableName} = new ArrayList<>(${lengthName});").replace(vars);
  }

  static String listAdd(String vectorName, String item) {
    Map<String, Object> vars =
        Map.of(
            "vectorName", vectorName,
            "itemName", item);

    return wrap("${vectorName}.add(${itemName});").replace(vars);
  }

  static String varDeclare(String type, String variableName) {
    return wrap("${type} ${variableName} = ${value};")
        .replace(Map.of("type", type, "variableName", variableName, "value", "null"));
  }

  public static String readStructMethod(String qualifiedName, String methodName) {
    Map<String, Object> vars = Map.of("type", qualifiedName, "methodName", methodName);
    return wrap("private static ${type} ${methodName}(SafeDataInputStream rpc) {").replace(vars);
  }

  public static String readStructReturn(String qualifiedName, List<String> fields) {
    Map<String, Object> vars = Map.of("type", qualifiedName, "fields", String.join(", ", fields));
    return wrap("return new ${type}(${fields});").replace(vars);
  }

  public static String readEnum(String qualifiedName) {
    Map<String, Object> vars = Map.of("type", qualifiedName);
    return wrap("return rpc.readEnum(${type}.values());").replace(vars);
  }

  public static String replace(String template, Map<String, Object> vars) {
    return wrap(template).replace(vars);
  }

  /** A template using commons-text syntax. */
  @FunctionalInterface
  public interface Template {

    /**
     * Render the template using the specified variables.
     *
     * @param variables the variables
     * @return the rendered template
     */
    String replace(Map<String, ?> variables);
  }
}
