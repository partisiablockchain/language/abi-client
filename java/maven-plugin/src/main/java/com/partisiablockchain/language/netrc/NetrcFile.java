package com.partisiablockchain.language.netrc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.HashMap;

/**
 * A netrc file, containing netrc entries for some number of machines. The .netrc file specification
 * can be found <a
 * href="https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html">here</a>
 */
public record NetrcFile(HashMap<String, NetrcEntry> entries, NetrcEntry defaultEntry) {

  /**
   * Get the netrc entry for a given machine. Defaults to the default entry if the default entry
   * exists, and the machine has no entry in the netrc file.
   *
   * @param machine The machine to get the netrc entry for.
   * @return The netrc entry of the machine.
   */
  public NetrcEntry getEntry(String machine) {
    return entries.getOrDefault(machine, defaultEntry);
  }

  /**
   * Whether the netrc file contains an entry for a given machine.
   *
   * @param machine The machine to lookup in the netrc file.
   * @return True if the netrc file contains an entry for the queried machine, false otherwise.
   */
  public boolean containsEntry(String machine) {
    return entries.containsKey(machine);
  }

  /** An entry in a {@link NetrcFile}. */
  public record NetrcEntry(String login, String password, String account, String macdef) {}
}
