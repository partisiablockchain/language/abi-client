package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.AbiWithTypeHint;
import com.partisiablockchain.language.CodegenTypeHint;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Generates Java code from a type-hinted ABI. */
public final class ContractGenerator {

  private final Prefix prefix = Prefix.empty();

  private final Map<TypeSpec.TypeIndex, RpcReadCodeGenerator> abiToGenerator;

  private final ContractAbi contractAbi;
  private final CodegenTypeHint typeHint;

  /**
   * Create a new contract generator.
   *
   * @param typeHinted the ABI+type hint
   */
  public ContractGenerator(AbiWithTypeHint typeHinted) {
    this.contractAbi = (ContractAbi) typeHinted.abi().chainComponent();
    this.typeHint = typeHinted.typeHint();

    HashMap<TypeSpec.TypeIndex, RpcReadCodeGenerator> abiToGenerator = new HashMap<>();
    abiToGenerator.put(TypeSpec.TypeIndex.bool, rpcMethod("readBoolean"));
    abiToGenerator.put(TypeSpec.TypeIndex.String, rpcMethod("readString"));
    abiToGenerator.put(TypeSpec.TypeIndex.Address, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.i8, rpcMethod("readSignedByte"));
    abiToGenerator.put(TypeSpec.TypeIndex.u8, rpcMethod("readUnsignedByte"));
    abiToGenerator.put(TypeSpec.TypeIndex.i16, rpcMethod("readShort"));
    abiToGenerator.put(TypeSpec.TypeIndex.u16, rpcMethod("readUnsignedShort"));
    abiToGenerator.put(TypeSpec.TypeIndex.i32, rpcMethod("readInt"));
    abiToGenerator.put(TypeSpec.TypeIndex.u32, Templates::readUnsignedInt);
    abiToGenerator.put(TypeSpec.TypeIndex.i64, rpcMethod("readLong"));
    abiToGenerator.put(TypeSpec.TypeIndex.u64, unsigned256(8));
    abiToGenerator.put(TypeSpec.TypeIndex.u128, unsigned256(16));
    abiToGenerator.put(TypeSpec.TypeIndex.u256, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.BlsPublicKey, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.BlsSignature, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.Hash, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.PublicKey, Templates::staticRead);
    abiToGenerator.put(TypeSpec.TypeIndex.Signature, Templates::staticRead);
    this.abiToGenerator = Map.copyOf(abiToGenerator);
  }

  private RpcReadCodeGenerator unsigned256(int byteCoung) {
    return (type, variableName) -> Templates.readUnsigned256(type, variableName, byteCoung);
  }

  static String namedTypeReadMethodName(String qualifiedName) {
    return "rpc_read__" + qualifiedName.replace(".", "_");
  }

  private void generateReadMethodsForNamedTypes(CodeWriter.Scope scope, NamedTypeSpec namedType) {
    if (namedType instanceof StructTypeSpec struct) {
      generateReadStruct(scope, struct);
    } else {
      generateReadEnum(scope, (EnumTypeSpec) namedType);
    }
  }

  private void generateReadEnum(CodeWriter.Scope outerScope, EnumTypeSpec namedType) {
    String qualifiedName = typeHint.lookupQualifiedName(namedType.name());
    String methodName = namedTypeReadMethodName(qualifiedName);

    CodeWriter.Scope readMethod =
        outerScope.addScope(Templates.readStructMethod(qualifiedName, methodName));
    readMethod.addStatement(Templates.readEnum(qualifiedName));
  }

  private void generateReadStruct(CodeWriter.Scope outerScope, StructTypeSpec struct) {
    String qualifiedName = typeHint.lookupQualifiedName(struct.name());
    String methodName = namedTypeReadMethodName(qualifiedName);

    CodeWriter.Scope readMethod =
        outerScope.addScope(Templates.readStructMethod(qualifiedName, methodName));

    List<FieldAbi> fields = struct.fields();
    for (FieldAbi field : fields) {
      generateReadFromRpc(readMethod, field.name(), field.type());
    }

    List<String> fieldNames = struct.fields().stream().map(FieldAbi::name).toList();
    readMethod.addStatement(Templates.readStructReturn(qualifiedName, fieldNames));
  }

  private void generateDispatch(CodeWriter.Scope scope, ContractInvocation contractInvocation) {
    CodegenTypeHint.MethodSignature signature = typeHint.getSignature(contractInvocation);
    List<String> variables = createParameterNamesFromSignature(signature);

    variables.addAll(generateReadRpcCodeForFunction(scope, contractInvocation.arguments()));

    if (signature.rpc) {
      variables.add("rpc");
    }

    scope.addStatement(
        "return delegate.${action}(${parameters});",
        "action",
        contractInvocation.name(),
        "parameters",
        String.join(", ", variables));
  }

  private List<String> generateReadRpcCodeForFunction(
      CodeWriter.Scope scope, List<ArgumentAbi> arguments) {
    List<String> variables = new ArrayList<>();
    for (ArgumentAbi argument : arguments) {
      variables.add(argument.name());

      generateReadFromRpc(scope, argument.name(), argument.type());
    }
    return variables;
  }

  private static List<String> createParameterNamesFromSignature(
      CodegenTypeHint.MethodSignature signature) {
    List<String> variables = new ArrayList<>();

    if (signature.sysContext) {
      variables.add("context");
    }

    if (signature.state) {
      variables.add("state");
    }

    if (signature.callbackContext) {
      variables.add("callbackContext");
    }

    return variables;
  }

  private void generateReadSimple(CodeWriter.Scope writer, String variable, SimpleTypeSpec spec) {
    RpcReadCodeGenerator generator = abiToGenerator.get(spec.typeIndex());
    Class<?> type = TypeSpecToAbi.classFromAbi(spec);

    String code = generator.generate(type.getSimpleName(), prefix.varName(variable));
    writer.addStatement(code);
  }

  private void generateReadFromRpc(CodeWriter.Scope writer, String variable, TypeSpec spec) {
    if (spec instanceof SimpleTypeSpec simple) {
      generateReadSimple(writer, variable, simple);
    } else if (spec instanceof OptionTypeSpec optional) {
      TypeSpec inner = optional.getGenerics().get(0);
      generateReadOptional(writer, variable, inner);
    } else if (spec instanceof NamedTypeRef namedRef) {
      NamedTypeSpec namedTypeSpec = typeHint.getNamedTypesWithUpgradeTypes().get(namedRef.index());
      String qualifiedName = typeHint.lookupQualifiedName(namedTypeSpec.name());
      String methodName = namedTypeReadMethodName(qualifiedName);

      String code = Templates.readNamedType(qualifiedName, prefix.varName(variable), methodName);
      writer.addStatement(code);
    } else if (spec instanceof VecTypeSpec vec) {
      TypeSpec inner = vec.getGenerics().get(0);
      generateReadVec(writer, variable, inner);
    } else {
      SizedArrayTypeSpec sized = (SizedArrayTypeSpec) spec;
      RpcReadCodeGenerator generator =
          rpcMethodWithArgs("readBytes", String.valueOf(sized.length()));

      String code = generator.generate("byte[]", prefix.varName(variable));
      writer.addStatement(code);
    }
  }

  /**
   * Generate the contract code.
   *
   * @return the generated code
   */
  public String generate() {
    String stateSimpleName = typeHint.stateSimpleName();
    String stateName = typeHint.lookupQualifiedName(stateSimpleName);

    var writer = new CodeWriter();
    writer
        .getRootScope()
        .addStatement(Templates.sysContractHeader(typeHint.packageName(), stateName))
        .newLine();

    String simpleName = typeHint.contractSimpleName();
    CodeWriter.Scope classScope =
        writer
            .getRootScope()
            .addScope(Templates.sysContractClassDeclaration(simpleName, stateSimpleName));

    ContractInvocation init = null;
    List<ContractInvocation> actions = new ArrayList<>();
    List<ContractInvocation> callbacks = new ArrayList<>();
    for (ContractInvocation function : contractAbi.contractInvocations()) {
      if (function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.INIT.kindId()) {
        init = function;
      } else if (function.kind().kindId() == ImplicitBinderAbi.DefaultKinds.CALLBACK.kindId()) {
        callbacks.add(function);
      } else {
        actions.add(function);
      }
    }

    generateInit(classScope, stateSimpleName, init);
    generateOnInvoke(classScope, stateSimpleName, actions);
    generateCallbacks(classScope, stateSimpleName, callbacks);
    generateUpgrade(classScope, stateSimpleName);

    List<NamedTypeSpec> namedTypes = typeHint.getNamedTypesWithUpgradeTypes();
    for (NamedTypeSpec namedType : namedTypes) {
      if (typeHint.isRpcRead(namedType)) {
        generateReadMethodsForNamedTypes(classScope, namedType);
      }
    }

    return writer.getGenerated();
  }

  private void generateUpgrade(CodeWriter.Scope classScope, String stateName) {
    String begin =
        "@Override\n"
            + "public ${state} onUpgrade(StateAccessor oldState, SafeDataInputStream rpc) {";

    CodeWriter.Scope methodScope = classScope.addScope(begin, "state", stateName);

    CodegenTypeHint.UpgradeSignature signature = typeHint.getUpgradeSignature();
    List<String> variables = new ArrayList<>(List.of("oldState"));

    variables.addAll(generateReadRpcCodeForFunction(methodScope, signature.arguments()));

    if (signature.rpc()) {
      variables.add("rpc");
    }

    methodScope.addStatement(
        "return delegate.${action}(${parameters});",
        "action",
        signature.name(),
        "parameters",
        String.join(", ", variables));
  }

  private void generateInit(
      CodeWriter.Scope classScope, String stateName, ContractInvocation init) {
    String begin =
        "@Override\n"
            + "public ${state} onCreate(SysContractContext context, SafeDataInputStream rpc) {";
    CodeWriter.Scope methodScope = classScope.addScope(begin, "state", stateName);
    generateDispatch(methodScope, init);
  }

  private void generateOnInvoke(
      CodeWriter.Scope classScope, String stateName, List<ContractInvocation> actions) {
    String template =
        "@Override\n"
            + "public ${state} onInvoke(SysContractContext context, ${state} state,"
            + " SafeDataInputStream rpc) {";

    String beginCode = Templates.replace(template, Map.of("state", stateName));
    generateDispatchWithSwitchOnCase(classScope, beginCode, actions);
  }

  private void generateCallbacks(
      CodeWriter.Scope classScope, String stateName, List<ContractInvocation> actions) {
    String template =
        "@Override\n"
            + "public ${state} onCallback(SysContractContext context, ${state} state,"
            + " CallbackContext callbackContext, SafeDataInputStream rpc) {";

    String beginCode = Templates.replace(template, Map.of("state", stateName));
    generateDispatchWithSwitchOnCase(classScope, beginCode, actions);
  }

  private void generateDispatchWithSwitchOnCase(
      CodeWriter.Scope classScope, String beginCode, List<ContractInvocation> functions) {

    CodeWriter.Scope switchScope = createSwitchOnShortname(classScope, beginCode);
    for (ContractInvocation action : functions) {
      int shortname = action.shortname()[0];
      CodeWriter.Scope caseScope =
          switchScope.addScope("case ${shortname} -> {", "shortname", shortname);

      generateDispatch(caseScope, action);
    }

    switchScope.addStatement(
        "default -> throw new IllegalArgumentException(\"Invalid shortname: \" + shortname);");
  }

  private static CodeWriter.Scope createSwitchOnShortname(
      CodeWriter.Scope classScope, String beginCode) {
    CodeWriter.Scope methodScope = classScope.addScope(beginCode);
    methodScope.addStatement("int shortname = rpc.readUnsignedByte();").newLine();

    return methodScope.addScope("switch (shortname) {");
  }

  private static RpcReadCodeGenerator rpcMethod(String method) {
    return rpcMethodWithArgs(method, "");
  }

  private static RpcReadCodeGenerator rpcMethodWithArgs(String method, String args) {
    return (type, variableName) -> Templates.rpcRead(type, variableName, method, args);
  }

  private void generateReadVectorLength(CodeWriter.Scope writer) {
    RpcReadCodeGenerator generator = rpcMethodWithArgs("readInt", "");

    String variableName = prefix.varName("len");
    writer.addStatement(generator.generate("int", variableName));
  }

  private String getNullableTypeName(TypeSpec spec) {
    if (spec instanceof SimpleTypeSpec simple) {
      Class<?> nullableClass = TypeSpecToAbi.nullableClassFromAbi(simple);
      return TypeNameShortener.shorten(nullableClass.getName());
    } else if (spec instanceof NamedTypeRef ref) {
      NamedTypeSpec namedTypeSpec = contractAbi.namedTypes().get(ref.index());

      String qualified = typeHint.lookupQualifiedName(namedTypeSpec.name());
      return TypeNameShortener.shorten(qualified);
    } else {
      VecTypeSpec vec = (VecTypeSpec) spec;
      TypeSpec inner = vec.getGenerics().get(0);
      String innerName = getNullableTypeName(inner);

      return Templates.replace("List<${inner}>", Map.of("inner", innerName));
    }
  }

  private void generateReadOptional(CodeWriter.Scope outerScope, String variable, TypeSpec inner) {
    String outerVarName = prefix.varName(variable);
    outerScope.addStatement(Templates.varDeclare(getNullableTypeName(inner), outerVarName));

    prefix.push("null");

    CodeWriter.Scope ifScope = outerScope.addScope("if (rpc.readBoolean()) {");
    generateReadFromRpc(ifScope, outerVarName, inner);

    ifScope.addStatement(
        "${outer} = ${inner};", "outer", outerVarName, "inner", prefix.varName(outerVarName));

    prefix.pop();
  }

  private void generateReadVec(CodeWriter.Scope writer, String variable, TypeSpec inner) {
    if (inner.typeIndex() == TypeSpec.TypeIndex.u8) {
      writer.addStatement("byte[] ${variable} = rpc.readDynamicBytes();", "variable", variable);
    } else {
      String vectorVariableName = prefix.varName(variable);

      prefix.push(variable);

      final String lengthName = prefix.varName("len");
      final String counterName = prefix.varName("i");

      generateReadVectorLength(writer);

      String genericTypeName = getNullableTypeName(inner);
      String declare =
          Templates.listDeclare(genericTypeName, vectorVariableName, counterName, lengthName);

      writer.addStatement(declare);

      CodeWriter.Scope forLoop = writer.addScope(Templates.forLoopToCount(lengthName, counterName));
      generateReadFromRpc(forLoop, "item", inner);
      forLoop.addStatement(Templates.listAdd(vectorVariableName, prefix.varName("item")));

      prefix.pop();
    }
  }

  /** A function that can generate code to read RPC. */
  @FunctionalInterface
  private interface RpcReadCodeGenerator {

    /**
     * Generate RPC read code for a type and variable name.
     *
     * @param type the type
     * @param variableName the variable name
     * @return the RPC read code
     */
    String generate(String type, String variableName);
  }
}
