package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;

/**
 * Handles the mapping of names of types and enum constants to an index in the ABI.
 *
 * <p>All structs, enums and enum constants are added to this class and get a unique index.
 *
 * <p>Java qualified names get transformed into their simplest form. If a collision exists the names
 * are disambiguated so two types a.b.C and a.x.C will become b.C and x.C.
 */
final class NamedTypeCollector {

  /** This is used to keep track of the type indices for state elements. */
  private final List<String> stateElementNames = new ArrayList<>();

  /** This is used to keep track of the type indices for rpc elements. */
  private final List<String> rpcElementNames = new ArrayList<>();

  /** List of element names for which there is both a rpc and state version. */
  private final List<String> clashingElementNames = new ArrayList<>();

  /** Map from a fully qualified name to the AST element represented by the name. */
  private final Map<String, Element> elements = new LinkedHashMap<>();

  /** Map from a fully qualified name to the mutable equivalent. */
  private final Map<String, NameWithMutablePackage> identifiers = new HashMap<>();

  /** Set containing names of the struct types which need to be read from rpc. */
  private final List<String> rpcReadNames = new ArrayList<>();

  NamedTypeCollector() {}

  /**
   * Add the AST element to the list of known types. Adds the name to the list of types which needs
   * to be read from rpc. If the element is a record and is known as a state type, it adds it to
   * clashing names.
   *
   * @param element the element
   * @return true if the list was updated, false otherwise.
   */
  boolean addNamedRpcType(Element element) {
    String qualifiedName = getFullyQualifiedName(element);
    ElementKind kind = element.getKind();
    if (kind == ElementKind.RECORD || kind == ElementKind.ENUM) {
      rpcReadNames.add(qualifiedName);
    }
    int index = rpcElementNames.indexOf(qualifiedName);
    boolean addElement = kind == ElementKind.RECORD || !stateElementNames.contains(qualifiedName);
    if (index == -1 && addElement) {
      rpcElementNames.add(qualifiedName);
      registerIdentifier(element, qualifiedName);
      if (stateElementNames.contains(qualifiedName)) {
        clashingElementNames.add(qualifiedName);
      }
      return true;
    }
    return false;
  }

  /**
   * Add the AST element to the list of known types reporting if the list was updated.
   *
   * @param element the element
   * @return true if the list was updated, false otherwise.
   */
  boolean addNamedStateType(Element element) {
    String qualifiedName = getFullyQualifiedName(element);
    int index = stateElementNames.indexOf(qualifiedName);
    if (index == -1) {
      stateElementNames.add(qualifiedName);
      registerIdentifier(element, qualifiedName);
      return true;
    }
    return false;
  }

  /**
   * Look up the index of the element.
   *
   * @param element the element to look up
   * @return the index of the element or -1 if it does not exist
   */
  int indexOf(Element element, boolean isState) {
    String name = getFullyQualifiedName(element);
    if (isState) {
      return stateElementNames.indexOf(name);
    } else {
      int rpcIndex = rpcElementNames.indexOf(name);
      if (rpcIndex == -1) {
        // In case it is an element that can be shared between state and rpc
        return stateElementNames.indexOf(name);
      }
      return stateElementNames.size() + rpcIndex;
    }
  }

  private String getFullyQualifiedName(Element element) {
    ElementKind kind = element.getKind();
    if (kind == ElementKind.RECORD || kind == ElementKind.ENUM || kind == ElementKind.CLASS) {
      TypeElement type = (TypeElement) element;
      return type.getQualifiedName().toString();
    } else {
      // Enum constant
      return getFullyQualifiedName(element.getEnclosingElement()) + "$" + element.getSimpleName();
    }
  }

  private void registerIdentifier(Element element, String qualifiedName) {
    elements.put(qualifiedName, element);

    List<String> nameComponents = splitToString(qualifiedName);

    List<String> packageComponents = nameComponents.subList(0, nameComponents.size() - 1);
    String className = nameComponents.get(nameComponents.size() - 1);

    var value = new NameWithMutablePackage(className, packageComponents);
    identifiers.put(qualifiedName, value);
  }

  /**
   * Get the ABI name for the element. If the name exists for both rpc and state, it adds $Rpc or
   * $State to the name.
   *
   * @param element the element to look up the name for
   * @param isState whether the function is called under the context of state or not.
   * @return the ABI name
   */
  String getAbiName(Element element, boolean isState) {
    String result;
    String name = getFullyQualifiedName(element);
    if (element.getKind() == ElementKind.CLASS) {
      TypeElement typeElement = (TypeElement) element;
      result = lookupShortestNonClashingName(typeElement.getQualifiedName().toString());
    } else {
      result = lookupShortestNonClashingName(name);
    }
    if (clashingElementNames.contains(name)) {
      result = result + (isState ? "$State" : "$Rpc");
    }
    return result;
  }

  /**
   * Lookup the shortest possible transformation of the qualified name.
   *
   * @param fullyQualifiedName the fully qualified name
   * @return the shortest non-clashing name
   */
  private String lookupShortestNonClashingName(String fullyQualifiedName) {
    NameWithMutablePackage nameWithPackage = identifiers.get(fullyQualifiedName);
    return nameWithPackage.reconstructWithPackage();
  }

  /**
   * Disambiguate all registered names into their simplest possible form that does not cause name
   * collisions.
   *
   * <p>For every clashing name increment the package delimiter index so e.g. a.b.C goes from C to
   * b.C.
   */
  void disambiguate() {
    for (List<NameWithMutablePackage> clashingNames : buildListOfClashingNames()) {
      clashingNames.forEach(NameWithMutablePackage::incrementPackagePointer);
    }

    if (!buildListOfClashingNames().isEmpty()) {
      disambiguate();
    }
  }

  /**
   * Create a map from the unique, shortened ABI name to the fully qualified name of the type.
   *
   * @return the map
   */
  Map<String, String> abiNameToQualified() {
    var result = new HashMap<String, String>();
    for (Map.Entry<String, NameWithMutablePackage> entry : identifiers.entrySet()) {
      String abiName = entry.getValue().reconstructWithPackage();
      if (clashingElementNames.contains(entry.getKey())) {
        result.put(abiName + "$State", entry.getKey());
        result.put(abiName + "$Rpc", entry.getKey());
      } else {
        result.put(abiName, entry.getKey());
      }
    }
    return result;
  }

  /**
   * Build a list of type name that clash when shortened.
   *
   * @return a list of clashing names
   */
  private Collection<List<NameWithMutablePackage>> buildListOfClashingNames() {
    return identifiers.values().stream()
        .collect(Collectors.groupingBy(NameWithMutablePackage::reconstructWithPackage))
        .values()
        .stream()
        .filter(list -> list.size() > 1)
        .toList();
  }

  private List<String> splitToString(String packageName) {
    return Arrays.stream(packageName.split("\\.", -1)).toList();
  }

  /**
   * Get the list of struct names which need to be read from rpc.
   *
   * @return the list of rpc read names.
   */
  List<String> getRpcReadNames() {
    var result = new ArrayList<String>();
    for (String name : rpcReadNames) {
      result.add(getAbiName(elements.get(name), false));
    }
    return result;
  }

  List<Element> getStateElements() {
    return stateElementNames.stream().map(elements::get).toList();
  }

  List<Element> getRpcElements() {
    return rpcElementNames.stream().map(elements::get).toList();
  }

  /**
   * This represents a Java-like type name that consists of a package and a simple name with methods
   * to cut the package at a position.
   */
  static final class NameWithMutablePackage {

    private final String simpleName;
    private final List<String> packageComponents;

    private int pointer;

    NameWithMutablePackage(String simpleName, List<String> packageComponents) {
      this.simpleName = simpleName;
      this.packageComponents = packageComponents;
      this.pointer = 0;
    }

    /**
     * Reconstruct the name with the package components cut at the pointer.
     *
     * @return the reconstructed name
     */
    String reconstructWithPackage() {
      int fromIndex = packageComponents.size() - pointer;
      int toIndex = packageComponents.size();

      List<String> nameComponents = new ArrayList<>(packageComponents.subList(fromIndex, toIndex));
      nameComponents.add(simpleName);

      return String.join("_", nameComponents);
    }

    /**
     * Increment the package pointer.
     *
     * @return whether the pointer was changed
     */
    boolean incrementPackagePointer() {
      int oldPointer = pointer;
      pointer = Math.min(pointer + 1, packageComponents.size());
      return oldPointer != pointer;
    }
  }
}
