package com.partisiablockchain.language.netrc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.netrc.NetrcFile.NetrcEntry;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse netrc files. <a
 * href="https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html">here</a>
 */
public final class NetrcParser {

  private static final String LOGIN = "login";
  private static final String PASSWORD = "password";
  private static final String ACCOUNT = "account";
  private static final String MACDEF = "macdef";

  private NetrcParser() {}

  /**
   * Parse the given String to a Netrc file.
   *
   * @param netrcString the read netrc file as a string.
   * @return the netrc file.
   */
  public static NetrcFile parseNetrcFile(String netrcString) {
    HashMap<String, NetrcEntry> netrcEntries = new HashMap<>();

    String entryRegex =
        "(?s)"
            + "[ \\t\\n]*"
            + "(?<type>machine (?<machine>[^ \\n\\t]+)|default)"
            + "(?<fields>(?:(?!machine|default).)+)"
            + "*";
    Matcher entryMatcher = Pattern.compile(entryRegex).matcher(netrcString);

    String entireFilePattern = "[ \\t\\n]*(" + entryRegex + "[ \\t\\n]*)*";
    Matcher entireFileMatcher = Pattern.compile(entireFilePattern).matcher(netrcString);

    if (!entireFileMatcher.matches()) {
      throw new RuntimeException("netrc file was malformed");
    }

    while (entryMatcher.find()) {
      String fieldString = entryMatcher.group("fields");

      NetrcEntry netrcEntry = netrcEntryFromString(fieldString == null ? "" : fieldString.trim());

      if (entryMatcher.group("type").equals("default")) {
        if (!entryMatcher.hitEnd()) {
          throw new RuntimeException("Default entry must be last in netrc file");
        }

        return new NetrcFile(netrcEntries, netrcEntry);
      } else {
        netrcEntries.put(entryMatcher.group("machine"), netrcEntry);
      }
    }
    return new NetrcFile(netrcEntries, null);
  }

  static NetrcEntry netrcEntryFromString(String netrcEntry) {
    @SuppressWarnings("StringSplitter")
    String[] fields = netrcEntry.split("[ \n\t]");

    if (fields[0].isEmpty()) {
      return new NetrcEntry(null, null, null, null);
    }

    String login = null;
    String password = null;
    String account = null;
    String macdef = null;

    for (int i = 0; i < fields.length; i++) {
      String field = fields[i++];

      if (i == fields.length) {
        throw new RuntimeException("netrc file was malformed");
      }

      String fieldName = fields[i];

      switch (field) {
        case LOGIN -> {
          if (login != null) {
            throw new RuntimeException("netrc file has duplicate field: login");
          }
          login = fieldName;
        }
        case PASSWORD -> {
          if (password != null) {
            throw new RuntimeException("netrc file has duplicate field: password");
          }
          password = fieldName;
        }
        case ACCOUNT -> {
          if (account != null) {
            throw new RuntimeException("netrc file has duplicate field: account");
          }
          account = fieldName;
        }
        case MACDEF -> {
          if (macdef != null) {
            throw new RuntimeException("netrc file has duplicate field: macdef");
          }
          macdef = fieldName;
        }
        default -> throw new RuntimeException("Unknown netrc field: " + field);
      }
    }

    return new NetrcEntry(login, password, account, macdef);
  }
}
