package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.AbiWithTypeHint;
import com.partisiablockchain.language.CodegenTypeHint;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.BinderType;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.ContractInvocationKind;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.ImplicitBinderAbi;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.RecordComponentElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/** Generates ABI and java type hinting information from AST methods and types. */
public final class AbiMetaFromAst {

  private static final byte[] SHORTNAME_INIT =
      new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x0F};

  private final Types types;
  private final AstType stateType;

  private final NamedTypeCollector collector;
  private final List<FnAbiWithSignature> actions = new ArrayList<>();
  private FnAbiWithSignature upgradeAction;
  private int numNamedTypes;

  /**
   * Construct the ABI from AST creator.
   *
   * @param types the types utility
   * @param stateType the contract state type mirror
   */
  public AbiMetaFromAst(Types types, TypeMirror stateType) {
    this.types = types;
    this.stateType = createAstType(stateType);

    this.collector = new NamedTypeCollector();
    collectStateTypes(this.stateType);
  }

  private static List<? extends Element> getEnumConstants(TypeElement element) {
    return element.getEnclosedElements().stream()
        .filter(e -> e.getKind() == ElementKind.ENUM_CONSTANT)
        .toList();
  }

  /**
   * Add the upgrade action.
   *
   * @param method the AST method representing the upgrade method
   */
  public void addUpgrade(ExecutableElement method) {
    // FnKind and shortname is ignored
    this.upgradeAction =
        buildAbiForMethod(ImplicitBinderAbi.DefaultKinds.ACTION, new byte[] {-1}, method);
  }

  /**
   * A function ABI with an enriched signature.
   *
   * @param contractInvocation the function ABI
   * @param methodSignature the enriched method signature
   */
  @SuppressWarnings("unused")
  private record FnAbiWithSignature(
      ContractInvocation contractInvocation, CodegenTypeHint.MethodSignature methodSignature) {}

  /**
   * UpgradeImplicitBinderAbi.DefaultFunctionKinds.CALLBACKof contract actions.
   *
   * @param actions the actions to process
   */
  public void addActions(List<ActionExecutableElement> actions) {
    for (ActionExecutableElement actionExecutableElement : actions) {
      int shortname = actionExecutableElement.shortname();
      ExecutableElement method = actionExecutableElement.method();

      this.actions.add(
          buildAbiForMethod(
              ImplicitBinderAbi.DefaultKinds.ACTION, shortnameAsBytes(shortname), method));
    }
  }

  /**
   * Add a list of contract callbacks.
   *
   * @param callbacks the callbacks to process
   */
  public void addCallbacks(List<ActionExecutableElement> callbacks) {
    for (ActionExecutableElement actionExecutableElement : callbacks) {
      int shortname = actionExecutableElement.shortname();
      ExecutableElement method = actionExecutableElement.method();

      this.actions.add(
          buildAbiForMethod(
              ImplicitBinderAbi.DefaultKinds.CALLBACK, shortnameAsBytes(shortname), method));
    }
  }

  /**
   * Add a contract initializer.
   *
   * @param method the method to process
   */
  public void addInit(ExecutableElement method) {
    this.actions.add(
        buildAbiForMethod(ImplicitBinderAbi.DefaultKinds.INIT, SHORTNAME_INIT, method));
  }

  /**
   * Create a type-hinted ABI.
   *
   * @param packageName the destination package
   * @param contractClassName the contract class name
   * @return the type-hinted ABI
   */
  public AbiWithTypeHint toFileAbi(String packageName, String contractClassName) {
    var typeHint =
        new CodegenTypeHint(
            contractClassName,
            packageName,
            stateType.asTypeElementUnsafe().getSimpleName().toString(),
            collector.abiNameToQualified(),
            collector.getRpcReadNames());

    var signature =
        new CodegenTypeHint.UpgradeSignature(
            upgradeAction.contractInvocation.name(),
            upgradeAction.contractInvocation.arguments(),
            upgradeAction.methodSignature.rpc);
    typeHint.setUpgradeSignature(signature);

    for (FnAbiWithSignature action : actions) {
      ContractInvocation contractInvocation = action.contractInvocation();
      typeHint.addMethodSignatures(
          contractInvocation.kind().kindId(), contractInvocation.name(), action.methodSignature());
    }

    List<NamedTypeSpec> namedTypeSpecsWithUpgradeTypes = new ArrayList<>();
    for (Element element : collector.getStateElements()) {
      buildNamedType(namedTypeSpecsWithUpgradeTypes, element, true);
    }
    for (Element element : collector.getRpcElements()) {
      buildNamedType(namedTypeSpecsWithUpgradeTypes, element, false);
    }

    typeHint.setNamedTypesWithUpgradeTypes(namedTypeSpecsWithUpgradeTypes);

    List<NamedTypeSpec> namedTypeSpecs = namedTypeSpecsWithUpgradeTypes.subList(0, numNamedTypes);

    int stateIndex = 0;

    List<ContractInvocation> contractInvocationList =
        actions.stream()
            .map(FnAbiWithSignature::contractInvocation)
            .sorted(AbiParser.FN_ABI_SORT_ORDER_KIND_ID_AND_SHORTNAME)
            .toList();
    ContractAbi contractAbi =
        new ContractAbi(
            BinderType.Sys,
            List.of(),
            namedTypeSpecs,
            List.of(),
            contractInvocationList,
            new NamedTypeRef(stateIndex));

    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(9, 0, 0), new AbiVersion(5, 1, 0), null, contractAbi);

    return new AbiWithTypeHint(abi, typeHint);
  }

  private void buildNamedType(
      List<NamedTypeSpec> namedTypeSpecs, Element element, boolean isState) {
    ElementKind kind = element.getKind();
    if (kind == ElementKind.RECORD) {
      TypeElement typeElement = (TypeElement) element;
      if (isState) {
        namedTypeSpecs.add(buildStructAbiForState(typeElement));
      } else {
        namedTypeSpecs.add(buildStructAbiForRpc(typeElement));
      }
    } else if (kind == ElementKind.ENUM) {
      namedTypeSpecs.add(buildEnumAbi((TypeElement) element, isState));
    } else if (kind == ElementKind.CLASS) {
      TypeElement typeElement = (TypeElement) element;
      namedTypeSpecs.add(buildStructAbiForState(typeElement));
    } else {
      // Enum constant
      StructTypeSpec structTypeSpec =
          new StructTypeSpec(collector.getAbiName(element, isState), List.of());
      namedTypeSpecs.add(structTypeSpec);
    }
  }

  private FnAbiWithSignature buildAbiForMethod(
      ContractInvocationKind kind, byte[] shortname, ExecutableElement method) {
    List<? extends Element> parameters = method.getParameters();
    List<TypeMirror> parameterTypes = typesFromElements(parameters);

    List<ArgumentAbi> argumentAbis = new ArrayList<>();

    CodegenTypeHint.MethodSignature signature = new CodegenTypeHint.MethodSignature();
    for (int i = 0; i < parameters.size(); i++) {
      Element parameter = parameters.get(i);

      TypeMirror parameterType = parameterTypes.get(i);
      AstType astType = createAstType(parameterType);

      AbiParameterTypeSpecFromAst typeSpecFromAst =
          new AbiParameterTypeSpecFromAst(types, collector);
      if (isUserDefined(astType)) {
        TypeSpec typeSpec = typeSpecFromAst.createSpecFromParameter(parameter, astType, false);
        argumentAbis.add(new ArgumentAbi(parameter.getSimpleName().toString(), typeSpec));
      }

      signature.sysContext |= astType.isSysContext();
      signature.callbackContext |= astType.isCallbackContext();
      signature.state |= astType.isTypeOf(stateType.getWrapped());
      signature.rpc |= astType.isRpcStream();
    }

    String functionName = method.getSimpleName().toString();
    ContractInvocation contractInvocation =
        new ContractInvocation(kind, functionName, shortname, argumentAbis);
    return new FnAbiWithSignature(contractInvocation, signature);
  }

  private EnumTypeSpec buildEnumAbi(TypeElement element, boolean isState) {
    List<EnumVariant> variants = new ArrayList<>();
    TypeElement enumeration = (TypeElement) types.asElement(element.asType());

    List<? extends Element> enumConstants = getEnumConstants(enumeration);
    for (int discriminant = 0; discriminant < enumConstants.size(); discriminant++) {
      Element constant = enumConstants.get(discriminant);

      NamedTypeRef ref = new NamedTypeRef(collector.indexOf(constant, isState));
      variants.add(new EnumVariant(discriminant, ref));
    }
    return new EnumTypeSpec(collector.getAbiName(element, isState), variants);
  }

  /**
   * Builds a struct type spec based on the fields of the record represented by the parameter
   * element. Field order is in declaration order of the record fields. Precondition:
   * element.getKind() == ElementKind.RECORD
   *
   * @param element the type element to build the struct abi from.
   * @return the struct type spec representing element.
   */
  private StructTypeSpec buildStructAbiForRpc(TypeElement element) {
    List<FieldAbi> fields = new ArrayList<>();

    TypeElement record = (TypeElement) types.asElement(element.asType());
    for (RecordComponentElement component : record.getRecordComponents()) {
      TypeMirror parameterType = component.asType();

      AbiParameterTypeSpecFromAst typeSpecFromAst =
          new AbiParameterTypeSpecFromAst(types, collector);
      TypeSpec typeSpec =
          typeSpecFromAst.createSpecFromParameter(component, createAstType(parameterType), false);

      fields.add(new FieldAbi(component.getSimpleName().toString(), typeSpec));
    }

    String structName = collector.getAbiName(record, false);
    return new StructTypeSpec(structName, fields);
  }

  /**
   * Builds a struct type spec based on the declared fields of the class represented by the
   * parameter element. Fields are sorted alphabetically.
   *
   * @param element the type element to build the struct abi from.
   * @return the struct type spec representing element.
   */
  private StructTypeSpec buildStructAbiForState(TypeElement element) {
    List<FieldAbi> fields = new ArrayList<>();

    TypeElement clazz = (TypeElement) types.asElement(element.asType());
    var clazzFields = getClazzFields(clazz);
    for (Element component : clazzFields) {
      TypeMirror parameterType = component.asType();

      AbiParameterTypeSpecFromAst typeSpecFromAst =
          new AbiParameterTypeSpecFromAst(types, collector);
      TypeSpec typeSpec =
          typeSpecFromAst.createSpecFromParameter(component, createAstType(parameterType), true);

      fields.add(new FieldAbi(component.getSimpleName().toString(), typeSpec));
    }

    String structName = collector.getAbiName(clazz, true);
    return new StructTypeSpec(structName, fields);
  }

  private static List<? extends Element> getClazzFields(TypeElement clazz) {
    return clazz.getEnclosedElements().stream()
        .filter(o -> o.getKind().isField())
        .filter(
            o ->
                !(o.getModifiers().contains(Modifier.STATIC)
                    || o.getModifiers().contains(Modifier.TRANSIENT)))
        .sorted(Comparator.comparing(o -> o.getSimpleName().toString()))
        .toList();
  }

  /** Disambiguate all registered names. */
  public void disambiguateNames() {
    collector.disambiguate();
  }

  /**
   * Collect the types from the parameter of the upgrade method. Must be called after collecting the
   * types for the other AST methods. Saves the number of collect named types before the upgrade
   * method.
   *
   * @param upgradeMethod the upgrade method to collect
   */
  public void collectUpgradeTypes(ActionExecutableElement upgradeMethod) {
    numNamedTypes = collector.getStateElements().size() + collector.getRpcElements().size();
    collectTypes(List.of(upgradeMethod));
  }

  /**
   * Collect the types from the parameters of AST methods.
   *
   * @param methods the methods to look at
   */
  public void collectTypes(Collection<ActionExecutableElement> methods) {
    for (ActionExecutableElement ast : methods) {
      ExecutableElement method = ast.method();
      List<? extends Element> parameters = method.getParameters();
      List<TypeMirror> parameterTypes = typesFromElements(parameters);

      for (int i = 0; i < parameters.size(); i++) {
        TypeMirror parameterType = parameterTypes.get(i);
        collectTypes(createAstType(parameterType));
      }
    }
  }

  void collectTypes(AstType parameterType) {
    if (parameterType.isTypeOf(stateType.getWrapped())) {
      return;
    }

    if (parameterType.isList()) {
      TypeMirror generic = parameterType.extractGeneric();
      collectTypes(createAstType(generic));
    } else if (parameterType.isRecord()) {
      TypeElement record = parameterType.asTypeElementUnsafe();

      if (collector.addNamedRpcType(record)) {
        for (RecordComponentElement component : record.getRecordComponents()) {
          collectTypes(createAstType(component.asType()));
        }
      }
    } else if (parameterType.isEnum()) {
      TypeElement enumElement = parameterType.asTypeElementUnsafe();
      collector.addNamedRpcType(enumElement);

      List<? extends Element> constants = getEnumConstants(enumElement);
      for (Element constant : constants) {
        collector.addNamedRpcType(constant);
      }
    }
  }

  void collectStateTypes(AstType parameterType) {
    if (parameterType.isList()) {
      TypeMirror generic = parameterType.extractGeneric();
      collectStateTypes(createAstType(generic));
    } else if (parameterType.isAvlTree()) {
      TypeMirror genericKey = parameterType.extractGeneric();
      collectStateTypes(createAstType(genericKey));
      TypeMirror genericValue = parameterType.extractSecondGeneric();
      collectStateTypes(createAstType(genericValue));
    } else if (parameterType.isClass() || parameterType.isRecord()) {
      TypeElement clazz = parameterType.asTypeElementUnsafe();
      if (collector.addNamedStateType(clazz)) {
        var fields = getClazzFields(clazz);
        for (Element field : fields) {
          collectStateTypes(createAstType(field.asType()));
        }
      }
    } else if (parameterType.isEnum()) {
      TypeElement enumElement = parameterType.asTypeElementUnsafe();
      collector.addNamedStateType(enumElement);

      List<? extends Element> constants = getEnumConstants(enumElement);
      for (Element constant : constants) {
        collector.addNamedStateType(constant);
      }
    }
  }

  private AstType createAstType(TypeMirror generic) {
    return new AstType(generic, types);
  }

  private boolean isUserDefined(AstType astType) {
    boolean builtInOrState =
        astType.isPartOfRpcSignature() || astType.isTypeOf(stateType.getWrapped());
    return !builtInOrState;
  }

  private static List<TypeMirror> typesFromElements(List<? extends Element> parameters) {
    return parameters.stream().map(Element::asType).toList();
  }

  private static byte[] shortnameAsBytes(int shortname) {
    return new byte[] {(byte) shortname};
  }
}
