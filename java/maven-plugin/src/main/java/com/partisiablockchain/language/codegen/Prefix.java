package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * A class for representing and generating variable name prefixes to ensure unique names when
 * generating reads for e.g. records.
 */
public final class Prefix {

  private final Deque<String> elements;

  private Prefix(Deque<String> elements) {
    this.elements = elements;
  }

  /**
   * Append something to the prefix and return a new one.
   *
   * @param part the part to append
   */
  void push(CharSequence part) {
    elements.add(part.toString());
  }

  /** Pop an element. */
  void pop() {
    elements.removeLast();
  }

  /**
   * Create a variable name with a prefix.
   *
   * @param name the name of the variable
   * @return the prefixed variable name
   */
  String varName(CharSequence name) {
    if (elements.isEmpty()) {
      return name.toString();
    } else {
      return String.join("_", elements) + "_" + name;
    }
  }

  /**
   * Create an empty prefix.
   *
   * @return the new prefix
   */
  static Prefix empty() {
    return new Prefix(new ArrayDeque<>());
  }
}
