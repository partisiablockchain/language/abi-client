package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.BinaryAbiWriter;
import com.partisiablockchain.language.codegen.ContractGenerator;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

/**
 * Annotation processor that creates an ABI file and generates the RPC code for a java smart
 * contract that is annotated with {@link AutoSysContract} and has members with the relevant
 * annotations from <code>com.partisiablockchain.contract.reflect</code>.
 */
@SupportedAnnotationTypes("com.partisiablockchain.contract.reflect.AutoSysContract")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
public final class AutoSysContractProcessor extends AbstractProcessor {

  private Elements elements;
  private Types types;

  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);

    this.elements = processingEnv.getElementUtils();
    this.types = processingEnv.getTypeUtils();
  }

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    try {
      processInner(roundEnv);

      return false;
    } catch (CompilationFailedException e) {
      Messager messager = processingEnv.getMessager();
      messager.printMessage(Diagnostic.Kind.MANDATORY_WARNING, e.getMessage(), e.getElement());

      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private void processInner(RoundEnvironment roundEnv) throws Exception {
    Set<? extends Element> contracts = roundEnv.getElementsAnnotatedWith(AutoSysContract.class);
    for (Element classElement : contracts) {
      TypeElement contractElement = (TypeElement) classElement;
      TypeMirror stateType = GenerateAbiFromAnnotation.extractStateType(contractElement);

      Filer filer = processingEnv.getFiler();

      var contractFromAnnotation =
          new GenerateAbiFromAnnotation(types, elements, contractElement, stateType);

      final String qualifiedContractName = contractElement.getQualifiedName().toString();
      final String qualifiedInvokerName = qualifiedContractName + "Invoker";

      AbiWithTypeHint model = contractFromAnnotation.generate();

      writeAbiToFile(filer, model.abi(), qualifiedContractName);
      writeCodeToFile(filer, model, qualifiedInvokerName);
      writeMain(filer, qualifiedInvokerName);
    }
  }

  private static void writeAbiToFile(Filer filer, FileAbi abi, String qualifiedName)
      throws IOException {
    FileObject resource =
        filer.createResource(StandardLocation.CLASS_OUTPUT, "", qualifiedName + ".abi");
    try (OutputStream out = resource.openOutputStream()) {
      out.write(BinaryAbiWriter.serialize(abi));
    }
  }

  private static void writeMain(Filer filer, String qualifiedContractName) throws IOException {
    FileObject main = filer.createResource(StandardLocation.CLASS_OUTPUT, "", "main");
    try (Writer mainWriter = main.openWriter()) {
      mainWriter.append(qualifiedContractName).append(System.lineSeparator());
    }
  }

  private static void writeCodeToFile(
      Filer filer, AbiWithTypeHint model, String qualifiedContractName) throws IOException {
    var contractGenerator = new ContractGenerator(model);
    JavaFileObject sourceFile = filer.createSourceFile(qualifiedContractName);
    try (Writer sourceWriter = sourceFile.openWriter()) {
      sourceWriter.append(contractGenerator.generate()).close();
    }
  }
}
