package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.math.Unsigned256;
import java.util.HashMap;
import java.util.Map;

final class TypeSpecToAbi {

  public static final Map<TypeSpec.TypeIndex, Class<?>> ABI_TO_CLASS = new HashMap<>();
  public static final Map<TypeSpec.TypeIndex, Class<?>> ABI_TO_NULLABLE_CLASS = new HashMap<>();

  static {
    put(Boolean.TYPE, Boolean.class, TypeSpec.TypeIndex.bool);
    put(Boolean.TYPE, Boolean.class, TypeSpec.TypeIndex.bool);
    put(String.class, String.class, TypeSpec.TypeIndex.String);
    put(Byte.TYPE, Byte.class, TypeSpec.TypeIndex.i8);
    put(Short.TYPE, Short.class, TypeSpec.TypeIndex.i16);
    put(
        Integer.TYPE,
        Integer.class,
        TypeSpec.TypeIndex.u8,
        TypeSpec.TypeIndex.u16,
        TypeSpec.TypeIndex.i32);
    put(Long.TYPE, Long.class, TypeSpec.TypeIndex.u32, TypeSpec.TypeIndex.i64);
    put(BlockchainAddress.class, BlockchainAddress.class, TypeSpec.TypeIndex.Address);
    put(Hash.class, Hash.class, TypeSpec.TypeIndex.Hash);
    put(BlockchainPublicKey.class, BlockchainPublicKey.class, TypeSpec.TypeIndex.PublicKey);
    put(Signature.class, Signature.class, TypeSpec.TypeIndex.Signature);
    put(BlsPublicKey.class, BlsPublicKey.class, TypeSpec.TypeIndex.BlsPublicKey);
    put(BlsSignature.class, BlsSignature.class, TypeSpec.TypeIndex.BlsSignature);
    put(
        Unsigned256.class,
        Unsigned256.class,
        TypeSpec.TypeIndex.u64,
        TypeSpec.TypeIndex.u128,
        TypeSpec.TypeIndex.u256);
  }

  private static void put(
      Class<?> defaultClass, Class<?> nullableClass, TypeSpec.TypeIndex... indices) {
    for (TypeSpec.TypeIndex index : indices) {
      ABI_TO_CLASS.put(index, defaultClass);
      ABI_TO_NULLABLE_CLASS.put(index, nullableClass);
    }
  }

  /**
   * Get the nullable class for the given ABI type.
   *
   * @param spec the abi type
   * @return the nullable class
   */
  public static Class<?> nullableClassFromAbi(SimpleTypeSpec spec) {
    return ABI_TO_NULLABLE_CLASS.get(spec.typeIndex());
  }

  /**
   * Get the Java class for the given ABI type.
   *
   * @param spec the abi type
   * @return the Java class
   */
  public static Class<?> classFromAbi(SimpleTypeSpec spec) {
    return ABI_TO_CLASS.get(spec.typeIndex());
  }

  private TypeSpecToAbi() {}
}
