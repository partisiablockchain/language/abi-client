package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import java.io.Serial;
import javax.lang.model.element.Element;

/** Exception that is thrown when compilation fails. */
public final class CompilationFailedException extends RuntimeException {

  @Serial private static final long serialVersionUID = 1L;

  /** The AST element that caused the error. */
  private final Element element;

  /**
   * Construct a new exception.
   *
   * @param element the locator element
   * @param template the message template
   * @param args the arguments for the template
   */
  @FormatMethod
  public CompilationFailedException(Element element, String template, Object... args) {
    super(template.formatted(args));
    this.element = element;
  }

  /**
   * The element at which the compile error occurred.
   *
   * @return the element
   */
  public Element getElement() {
    return element;
  }
}
