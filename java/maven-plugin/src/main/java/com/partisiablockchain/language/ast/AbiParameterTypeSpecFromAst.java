package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.auto.common.MoreTypes;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abimodel.model.MapTypeSpec;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

final class AbiParameterTypeSpecFromAst {

  private final Types types;
  private final NamedTypeCollector collector;

  AbiParameterTypeSpecFromAst(Types types, NamedTypeCollector collector) {
    this.types = types;
    this.collector = collector;
  }

  TypeSpec createSpecFromParameter(Element parameter, AstType parameterType, boolean isState) {
    RpcTypeMirror rpcType = RpcTypeMirror.fromElement(parameter);

    TypeSpec nonOptional = createSpecInner(parameter, parameterType, rpcType, isState);
    boolean stateNullable = isState && !parameterType.isPrimitive();
    boolean rpcNullable = !isState && rpcType.nullable();
    if (stateNullable || rpcNullable) {
      return new OptionTypeSpec(nonOptional);
    } else {
      return nonOptional;
    }
  }

  private TypeSpec createSpecInner(
      Element parameter, AstType parameterType, RpcTypeMirror rpcType, boolean isState) {
    if (parameterType.isNumber()) {
      Class<?> numberType = parameterType.asNumberClassUnsafe();
      return createForNumber(numberType, rpcType, isState);
    } else if (parameterType.isString()) {
      return new SimpleTypeSpec(TypeSpec.TypeIndex.String);
    } else if (parameterType.isBoolean()) {
      return new SimpleTypeSpec(TypeSpec.TypeIndex.bool);
    } else if (parameterType.isList()) {
      TypeMirror generic = parameterType.extractGeneric();
      TypeSpec inner =
          createSpecFromParameter(types.asElement(generic), createAstType(generic), isState);
      return new VecTypeSpec(inner);
    } else if (parameterType.isAvlTree()) {
      TypeMirror genericKey = parameterType.extractGeneric();
      TypeMirror genericValue = parameterType.extractSecondGeneric();
      TypeSpec key =
          createSpecFromParameter(types.asElement(genericKey), createAstType(genericKey), isState);
      TypeSpec value =
          createSpecFromParameter(
              types.asElement(genericValue), createAstType(genericValue), isState);
      return new MapTypeSpec(key, value);
    } else if (parameterType.isByteArray()) {
      if (rpcType.size() == -1) {
        return new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
      } else {
        return SizedArrayTypeSpec.byteArray(rpcType.size());
      }
    } else if (parameterType.isLargeByteArray()) {
      return new VecTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.u8));
    } else if (parameterType.isRecord() || parameterType.isClass()) {
      TypeElement recordElement = parameterType.asTypeElementUnsafe();
      int index = collector.indexOf(recordElement, isState);
      return new NamedTypeRef(index);
    } else if (parameterType.isEnum()) {
      TypeElement typeElement = MoreTypes.asTypeElement(parameter.asType());
      int index = collector.indexOf(typeElement, isState);
      return new NamedTypeRef(index);
    } else {
      AstType astType = createAstType(parameter.asType());
      return createForOther(astType);
    }
  }

  TypeSpec createForNumber(Class<?> clazz, RpcTypeMirror rpcType, boolean isState) {
    if (isState || rpcType.size() == -1) {
      return new SimpleTypeSpec(AbiTypes.fromNumberClass(clazz));
    } else {
      return new SimpleTypeSpec(AbiTypes.fromRpcType(rpcType));
    }
  }

  TypeSpec createForOther(AstType type) {
    if (type.isNotDeclared()) {
      throw new RuntimeException("Invalid parameter type: " + type.getWrapped());
    }

    return new SimpleTypeSpec(fromBuiltIn(type));
  }

  static TypeSpec.TypeIndex fromBuiltIn(AstType type) {
    if (type.isTypeOf(BlockchainAddress.class)) {
      return TypeSpec.TypeIndex.Address;
    } else if (type.isTypeOf(Hash.class)) {
      return TypeSpec.TypeIndex.Hash;
    } else if (type.isTypeOf(BlockchainPublicKey.class)) {
      return TypeSpec.TypeIndex.PublicKey;
    } else if (type.isTypeOf(BlsPublicKey.class)) {
      return TypeSpec.TypeIndex.BlsPublicKey;
    } else if (type.isTypeOf(Signature.class)) {
      return TypeSpec.TypeIndex.Signature;
    } else if (type.isTypeOf(BlsSignature.class)) {
      return TypeSpec.TypeIndex.BlsSignature;
    } else /* if (type.isTypeOf(StateLong.class)) */ {
      return TypeSpec.TypeIndex.i64;
    }
  }

  private AstType createAstType(TypeMirror generic) {
    return new AstType(generic, types);
  }
}
