package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.math.Unsigned256;
import java.util.Arrays;
import java.util.List;

/** A collection of utility methods to handle conversion between ABI and Java. */
public final class AbiTypes {

  private AbiTypes() {}

  private static final List<TypeInfo> TYPE_INFOS;

  static {
    TYPE_INFOS =
        List.of(
            new TypeInfo(Byte.TYPE, Byte.class, IntSize.i8),
            new TypeInfo(Short.TYPE, Short.class, IntSize.i16),
            new TypeInfo(Integer.TYPE, Integer.class, IntSize.u8, IntSize.u16, IntSize.i32),
            new TypeInfo(Long.TYPE, Long.class, IntSize.u32, IntSize.i64),
            new TypeInfo(
                Unsigned256.class, Unsigned256.class, IntSize.u64, IntSize.u128, IntSize.u256),
            new TypeInfo(
                BlockchainAddress.class,
                BlockchainAddress.class,
                List.of(),
                List.of(TypeSpec.TypeIndex.Address)),
            new TypeInfo(Boolean.TYPE, Boolean.class, List.of(), List.of(TypeSpec.TypeIndex.bool)),
            new TypeInfo(
                String.class, String.class, List.of(), List.of(TypeSpec.TypeIndex.String)));
  }

  record TypeInfo(
      Class<?> defaultClass,
      Class<?> nullableClass,
      List<IntSize> allowedCoercions,
      List<TypeSpec.TypeIndex> abiTypeIndices) {

    TypeInfo(Class<?> defaultClass, Class<?> nullableClass, IntSize... allowedCoercions) {
      this(
          defaultClass,
          nullableClass,
          List.of(allowedCoercions),
          Arrays.stream(allowedCoercions).map(IntSize::toTypeIndex).toList());
    }

    boolean matchesClass(Class<?> that) {
      return that.equals(defaultClass()) || that.equals(nullableClass());
    }

    boolean matchesAstType(AstType parameterType) {
      return parameterType.isTypeOf(defaultClass()) || parameterType.isTypeOf(nullableClass());
    }

    boolean allowsCoercion(RpcTypeMirror wantedType) {
      for (IntSize size : allowedCoercions()) {
        if (size.equalSignature(wantedType.size(), wantedType.signed())) {
          return true;
        }
      }
      return false;
    }
  }

  private enum IntSize {
    i8(1, true),
    u8(1, false),
    i16(2, true),
    u16(2, false),
    i32(4, true),
    u32(4, false),
    i64(8, true),
    u64(8, false),
    i128(16, true),
    u128(16, false),
    u256(32, false);

    private final int size;
    private final boolean signed;

    IntSize(int size, boolean signed) {
      this.size = size;
      this.signed = signed;
    }

    boolean equalSignature(int size, boolean signed) {
      return this.size == size && this.signed == signed;
    }

    TypeSpec.TypeIndex toTypeIndex() {
      return TypeSpec.TypeIndex.valueOf(name());
    }
  }

  /**
   * Get an ABI type from the given class.
   *
   * @param type the number class
   * @return the ABI type
   */
  static TypeSpec.TypeIndex fromNumberClass(Class<?> type) {
    for (TypeInfo typeInfo : TYPE_INFOS) {
      if (typeInfo.matchesClass(type)) {
        List<TypeSpec.TypeIndex> indices = typeInfo.abiTypeIndices();
        return indices.get(indices.size() - 1);
      }
    }
    throw new IllegalArgumentException("Invalid number type: " + type);
  }

  /**
   * Get the ABI type from an RPC type mirror.
   *
   * @param rpcType the RPC type
   * @return the ABI type
   */
  static TypeSpec.TypeIndex fromRpcType(RpcTypeMirror rpcType) {
    for (IntSize value : IntSize.values()) {
      if (value.equalSignature(rpcType.size(), rpcType.signed())) {
        return value.toTypeIndex();
      }
    }
    throw new IllegalArgumentException("Invalid RPC type: " + rpcType);
  }

  /**
   * Checks if the parameter type can be cast to the wanted ABI type.
   *
   * @param parameterType the parameter type
   * @param wantedType the wanted type
   * @return whether a cast is allowed
   */
  static boolean isCoercionAllowed(AstType parameterType, RpcTypeMirror wantedType) {
    for (TypeInfo typeInfo : TYPE_INFOS) {
      if (typeInfo.allowsCoercion(wantedType) && typeInfo.matchesAstType(parameterType)) {
        return true;
      }
    }

    return false;
  }
}
