package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.auto.common.MoreTypes;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.language.BuiltInTypes;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleTypeVisitor14;
import javax.lang.model.util.Types;

/** Wraps {@link TypeMirror} and exposes nice-to-have methods. */
public final class AstType {

  private static final Map<TypeKind, Class<? extends Number>> TYPE_KIND_TO_CLASS =
      Map.of(
          TypeKind.BYTE, Byte.TYPE,
          TypeKind.SHORT, Short.TYPE,
          TypeKind.INT, Integer.TYPE,
          TypeKind.LONG, Long.TYPE);

  private final TypeMirror wrapped;
  private final Types types;

  /**
   * Wrap an AST type.
   *
   * @param wrapped the type mirror to wrap
   * @param types the types utility
   */
  public AstType(TypeMirror wrapped, Types types) {
    this.wrapped = wrapped;
    this.types = types;
  }

  boolean isTypeOf(Class<?> that) {
    return MoreTypes.isTypeOf(that, wrapped);
  }

  /**
   * Determine whether a type is equivalent to the wrapped one.
   *
   * @param that the type to test
   * @return whether the types are equivalent
   */
  public boolean isTypeOf(TypeMirror that) {
    return MoreTypes.equivalence().equivalent(that, wrapped);
  }

  private boolean isOneOf(List<Class<?>> list) {
    for (Class<?> that : list) {
      if (isTypeOf(that)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if the wrapped type is a {@link SysContractContext}.
   *
   * @return whether the wrapped type is a {@link SysContractContext}.
   */
  public boolean isSysContext() {
    return isTypeOf(SysContractContext.class);
  }

  /**
   * Check if the wrapped type is a {@link CallbackContext}.
   *
   * @return whether the wrapped type is a {@link CallbackContext}.
   */
  public boolean isCallbackContext() {
    return isTypeOf(CallbackContext.class);
  }

  /**
   * Check if the wrapped type is an {@link SafeDataInputStream}.
   *
   * @return whether the wrapped type is an {@link SafeDataInputStream}.
   */
  public boolean isRpcStream() {
    return isTypeOf(SafeDataInputStream.class);
  }

  /**
   * Check if the wrapped type is a {@link StateAccessor}.
   *
   * @return whether the wrapped type is a {@link StateAccessor}.
   */
  public boolean isStateAccessor() {
    return isTypeOf(StateAccessor.class);
  }

  boolean isPartOfRpcSignature() {
    return isSysContext() || isCallbackContext() || isStateAccessor() || isRpcStream();
  }

  boolean isNumber() {
    return isTypeKind(TypeKind.BYTE, TypeKind.SHORT, TypeKind.INT, TypeKind.LONG)
        || isOneOf(List.of(Byte.class, Short.class, Integer.class, Long.class, Unsigned256.class));
  }

  boolean isBoolean() {
    return isTypeKind(TypeKind.BOOLEAN) || isTypeOf(Boolean.class) || isTypeOf(StateBoolean.class);
  }

  private boolean isTypeKind(TypeKind... kinds) {
    return List.of(kinds).contains(wrapped.getKind());
  }

  boolean isRecord() {
    if (isNotDeclared()) {
      return false;
    } else {
      Element element = types.asElement(wrapped);
      return element.getKind() == ElementKind.RECORD;
    }
  }

  boolean isClass() {
    if (isNotDeclared() || isBuiltInType() || isStateBuiltInType()) {
      return false;
    } else {
      Element element = types.asElement(wrapped);
      return element.getKind() == ElementKind.CLASS;
    }
  }

  TypeElement asTypeElementUnsafe() {
    return (TypeElement) types.asElement(wrapped);
  }

  boolean isList() {
    return isTypeOf(List.class) || isTypeOf(FixedList.class);
  }

  boolean isAvlTree() {
    return isTypeOf(AvlTree.class);
  }

  boolean isNotDeclared() {
    return wrapped.getKind() != TypeKind.DECLARED;
  }

  boolean isEnum() {
    if (isNotDeclared()) {
      return false;
    } else {
      Element element = MoreTypes.asElement(wrapped);
      return element.getKind() == ElementKind.ENUM;
    }
  }

  boolean isPrimitive() {
    return wrapped.getKind().isPrimitive();
  }

  boolean isString() {
    return isTypeOf(String.class) || isTypeOf(StateString.class);
  }

  boolean isByteArray() {
    return isTypeOf(byte[].class);
  }

  boolean isLargeByteArray() {
    return isTypeOf(LargeByteArray.class);
  }

  boolean isBuiltInType() {
    return BuiltInTypes.isBuiltIn(wrapped);
  }

  boolean isStateBuiltInType() {
    return BuiltInTypes.isStateBuiltIn(wrapped);
  }

  TypeMirror extractGeneric() {
    var ref = new AtomicReference<TypeMirror>();
    wrapped.accept(
        new SimpleTypeVisitor14<>() {

          @Override
          public Object visitDeclared(DeclaredType t, Object o) {
            ref.set(t.getTypeArguments().get(0));
            return null;
          }
        },
        null);

    return ref.get();
  }

  TypeMirror extractSecondGeneric() {
    var ref = new AtomicReference<TypeMirror>();
    wrapped.accept(
        new SimpleTypeVisitor14<>() {

          @Override
          public Object visitDeclared(DeclaredType t, Object o) {
            ref.set(t.getTypeArguments().get(1));
            return null;
          }
        },
        null);

    return ref.get();
  }

  TypeMirror getWrapped() {
    return wrapped;
  }

  /**
   * Convert a number type to a number class if the type is supported, throw an execption if not.
   *
   * @return the class
   */
  Class<?> asNumberClassUnsafe() {
    TypeKind kind = wrapped.getKind();
    if (TYPE_KIND_TO_CLASS.containsKey(kind)) {
      return TYPE_KIND_TO_CLASS.get(kind);
    } else {
      List<Class<?>> validNumberTypes =
          List.of(Byte.class, Short.class, Integer.class, Long.class, Unsigned256.class);

      for (Class<?> numberType : validNumberTypes) {
        if (isTypeOf(numberType)) {
          return numberType;
        }
      }

      throw new IllegalArgumentException("Not a number type: " + wrapped);
    }
  }
}
