package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class CodeWriterTest {

  @Test
  void scoped() {
    var writer = new CodeWriter();
    writer
        .getRootScope()
        .addStatement("// ${comment}", "comment", "This is a comment")
        .addStatement("${name} = ${value}", "name", "John Doe", "value", "'missing'")
        .addStatement("y = ${a}x^2 + ${b}x", "a", "42", "b", "3")
        .newLine()
        .addScope("if (y == 0) {")
        .addStatement("println('Zero');");

    var generated = writer.getGenerated();
    Assertions.assertThat(generated)
        .isEqualToIgnoringNewLines(
            """
            // This is a comment
            John Doe = 'missing'
            y = 42x^2 + 3x

            if (y == 0) {
              println('Zero');
            }""");
  }
}
