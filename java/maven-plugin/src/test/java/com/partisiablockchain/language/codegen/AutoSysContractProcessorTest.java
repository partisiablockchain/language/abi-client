package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.google.testing.compile.Compiler.javac;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.io.ByteSource;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import com.partisiablockchain.language.AutoSysContractProcessor;
import com.partisiablockchain.language.CompilationFailedException;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.ContractInvocation;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.tools.JavaFileObject;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Tests of the annotation processor. */
final class AutoSysContractProcessorTest {

  private static final boolean OVERWRITE_REFERENCE = false;

  @Test
  void overwriteIsFalse() {
    assertThat(OVERWRITE_REFERENCE)
        .withFailMessage("Set the overwrite value to false before committing.")
        .isFalse();
  }

  @Test
  void processResult() {
    var processor = new AutoSysContractProcessor();
    var roundEnv = roundEnv(null);
    assertThat(processor.process(Set.of(), roundEnv)).isFalse();
  }

  @Test
  void exceptionInProcess() {
    var processor = new AutoSysContractProcessor();
    var roundEnv = roundEnv(new RuntimeException("MESSAGE"));
    Assertions.assertThatThrownBy(() -> processor.process(Set.of(), roundEnv))
        .hasMessageContaining("MESSAGE");
  }

  @Test
  void messager() {
    var processor = new AutoSysContractProcessor();
    var roundEnv = roundEnv(new CompilationFailedException(null, "MESSAGE"));
    processor.init(throwingProcessingEnv());

    Assertions.assertThatThrownBy(() -> processor.process(Set.of(), roundEnv))
        .hasMessageContaining("PRINT MESSAGE");
  }

  @Test
  void actionShortnameUsedTwice() {
    assertCompileFail(
        "ActionShortnameUsedTwice",
        "Duplicate shortname 1 on 'action2'. First defined on action 'action1'.");
  }

  @Test
  void actionShortnameNegative() {
    assertCompileFail(
        "ActionShortnameNegative",
        "Shortname on action 'action1' must be in range 0 to 127, but was: -1");
  }

  @Test
  void actionShortnameOutOfBounds() {
    assertCompileFail(
        "ActionShortnameOutOfBounds",
        "Shortname on action 'action1' must be in range 0 to 127, but was: 128");
  }

  @Test
  void callbackShortnameUsedTwice() {
    assertCompileFail(
        "CallbackShortnameUsedTwice",
        "Duplicate callback 1 on 'callback2'. First defined on action 'callback1'.");
  }

  @Test
  void callbackShortnameNegative() {
    assertCompileFail(
        "CallbackShortnameNegative",
        "Shortname on callback 'callback1' must be in range 0 to 127, but was: -1");
  }

  @Test
  void callbackShortnameOutOfBounds() {
    assertCompileFail(
        "CallbackShortnameOutOfBounds",
        "Shortname on callback 'callback1' must be in range 0 to 127, but was: 128");
  }

  @Test
  void actionModifiersNonPublic() {
    assertCompileFail(
        "ModifiersNonPublic",
        "Action method 'action1' must be public non-static, but was 'package-private'.");
  }

  @Test
  void actionModifiersStatic() {
    assertCompileFail(
        "ModifiersStatic",
        "Action method 'action1' must be public non-static, but was 'public static'");
  }

  @Test
  void actionModifiersPrivate() {
    assertCompileFail(
        "ModifiersPrivate", "Action method 'action1' must be public non-static, but was 'private'");
  }

  @Test
  void actionParameterIndicesContext() {
    assertCompileFail(
        "ActionParameterIndicesContext",
        "The context parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void actionParameterIndicesState1() {
    assertCompileFail(
        "ActionParameterIndicesState1",
        "The state parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void actionParameterIndicesState2() {
    assertCompileFail(
        "ActionParameterIndicesState2",
        "The state parameter was expected at index 1 but was at index 2.");
  }

  @Test
  void actionParameterIndicesRpc() {
    assertCompileFail(
        "ActionParameterIndicesRpc",
        "The rpc parameter was expected at index 3 but was at index 2");
  }

  @Test
  void callbackParameterIndicesSysContext() {
    assertCompileFail(
        "CallbackParameterIndicesSysContext",
        "The context parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void callbackParameterIndicesCallbackContext() {
    assertCompileFail(
        "CallbackParameterIndicesCallbackContext",
        "The context parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void callbackParameterIndicesState1() {
    assertCompileFail(
        "CallbackParameterIndicesState1",
        "The state parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void callbackParameterIndicesState2() {
    assertCompileFail(
        "CallbackParameterIndicesState2",
        "The state parameter was expected at index 1 but was at index 2.");
  }

  @Test
  void callbackParameterIndicesRpc() {
    assertCompileFail(
        "CallbackParameterIndicesRpc",
        "The rpc parameter was expected at index 3 but was at index 2");
  }

  @Test
  void actionReturnTypeInt() {
    assertCompileFail(
        "ReturnTypeInt",
        "Expected return type 'com.partisiablockchain.serialization.StateVoid' on action method"
            + " 'action1', but got 'int'");
  }

  @Test
  void actionReturnTypeVoid() {
    assertCompileFail(
        "ReturnTypeVoid",
        "Expected return type 'com.partisiablockchain.serialization.StateVoid' on action method"
            + " 'action1', but got 'void'");
  }

  @Test
  void callbackInvalidReturnType() {
    assertCompileFail(
        "CallbackInvalidReturnType",
        "Expected return type 'com.partisiablockchain.serialization.StateVoid' on callback method");
  }

  @Test
  void callbackInvalidRpcParam() {
    assertCompileFail("CallbackInvalidRpcParam", "Invalid type: java.math.BigInteger");
  }

  @Test
  void callbackInvalidModifier() {
    assertCompileFail(
        "CallbackInvalidModifier",
        "Callback method 'callback1' must be public non-static, but was 'package-private'");
  }

  @Test
  void nullabilityFailBoolean() {
    assertCompileFail("NullabilityFailBoolean", "Type 'boolean' cannot be nullable.");
  }

  @Test
  void nullabilityFailByte() {
    assertCompileFail("NullabilityFailByte", "Type 'byte' cannot be nullable.");
  }

  @Test
  void nullabilityFailShort() {
    assertCompileFail("NullabilityFailShort", "Type 'short' cannot be nullable.");
  }

  @Test
  void nullabilityFailInt() {
    assertCompileFail("NullabilityFailInt", "Type 'int' cannot be nullable.");
  }

  @Test
  void nullabilityFailLong() {
    assertCompileFail("NullabilityFailLong", "Type 'long' cannot be nullable.");
  }

  @Test
  void nullabilityFailByteArray() {
    assertCompileFail("NullabilityFailByteArray", "Type 'byte[]' cannot be nullable.");
  }

  @Test
  void nullabilityFailVec() {
    assertCompileFail(
        "NullabilityFailVec", "Type 'java.util.List<java.lang.Byte>' cannot be nullable.");
  }

  @Test
  void unhandledTypeAbiInRpc() {
    assertCompileFail(
        "UnhandledTypeAbiInRpc",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=99]'"
            + " into declared type 'int'");
  }

  @Test
  void unhandledTypeInRpc() {
    assertCompileFail("UnhandledTypeInRpc", "Invalid type: java.math.BigInteger");
  }

  @Test
  void unhandledArrayInRpc() {
    assertCompileFail("UnhandledArrayInRpc", "Invalid parameter type: java.lang.Object[]");
  }

  @Test
  void genericTypeInvalid() {
    assertCompileFail("GenericTypeInvalid", "Invalid type: java.math.BigInteger");
  }

  @Test
  void typeInvalidInRecord() {
    assertCompileFail("TypeInvalidInRecord", "Invalid type: java.math.BigInteger");
  }

  @Test
  void upgradeInvalidType() {
    assertCompileFail("UpgradeInvalidType", "Invalid type: java.math.BigInteger");
  }

  @Test
  void validContractWithNamedConstantsAsShortnames() throws Exception {
    assertSuccess("ValidContractWithNamedConstantsAsShortnames");
  }

  @Test
  void validTypes() throws Exception {
    SuccessfulCompile result = assertSuccess("ValidTypes");

    ContractAbi abi = (ContractAbi) result.abi().chainComponent();

    assertThat(abi.namedTypes()).hasSize(6);
    assertThat(abi.namedTypes().get(4).name()).isEqualTo("MyEnum$A");

    List<ContractInvocation> functions = abi.contractInvocations();
    assertThat(functions).hasSize(6);

    ContractInvocation builtins =
        functions.stream()
            .filter(fnAbi -> fnAbi.name().equals("builtins"))
            .findFirst()
            .orElseThrow();

    List<TypeSpec.TypeIndex> expectedTypes =
        List.of(
            TypeSpec.TypeIndex.Hash,
            TypeSpec.TypeIndex.PublicKey,
            TypeSpec.TypeIndex.BlsPublicKey,
            TypeSpec.TypeIndex.Signature,
            TypeSpec.TypeIndex.BlsSignature,
            TypeSpec.TypeIndex.u256);

    List<ArgumentAbi> arguments = builtins.arguments();
    for (int i = 0; i < arguments.size(); i++) {
      ArgumentAbi argument = arguments.get(i);

      assertThat(argument.type()).isInstanceOf(SimpleTypeSpec.class);

      SimpleTypeSpec typeSpec = (SimpleTypeSpec) argument.type();
      assertThat(typeSpec.typeIndex()).isEqualTo(expectedTypes.get(i));
    }
  }

  @Test
  void namedTypeCollection() throws Exception {
    SuccessfulCompile result = assertSuccess("NamedTypeCollection");
    ContractAbi abi = (ContractAbi) result.abi().chainComponent();

    var namedTypes = abi.namedTypes();
    assertThat(namedTypes).hasSize(4);
    assertThat(namedTypes.get(0).name()).isEqualTo("StateVoid");
    assertThat(namedTypes.get(1).name()).isEqualTo("A");
    assertThat(namedTypes.get(2).name()).isEqualTo("B");
    assertThat(namedTypes.get(3).name()).isEqualTo("C");
  }

  @Test
  void namedTypeEnumCollection() throws Exception {
    SuccessfulCompile result = assertSuccess("NamedTypeEnumCollection");
    ContractAbi abi = (ContractAbi) result.abi().chainComponent();

    var namedTypes = abi.namedTypes();
    assertThat(namedTypes).hasSize(8);
    assertThat(namedTypes.get(0).name()).isEqualTo("StateVoid");
    assertThat(namedTypes.get(1).name()).isEqualTo("A");
    assertThat(namedTypes.get(2).name()).isEqualTo("A$X");
    assertThat(namedTypes.get(3).name()).isEqualTo("B");
    assertThat(namedTypes.get(4).name()).isEqualTo("B$Y");
    assertThat(namedTypes.get(5).name()).isEqualTo("C");
    assertThat(namedTypes.get(6).name()).isEqualTo("C$Z");
    assertThat(namedTypes.get(7).name()).isEqualTo("C$ZZ");

    EnumTypeSpec enumC = (EnumTypeSpec) namedTypes.get(5);
    assertThat(enumC.variants()).hasSize(2);
  }

  @Test
  void ambiguousNames() throws Exception {
    SuccessfulCompile result = assertSuccess("AmbiguousNames");

    ContractAbi abi = (ContractAbi) result.abi().chainComponent();

    List<ContractInvocation> functions = abi.contractInvocations();
    assertThat(functions).hasSize(2);

    assertThat(functions.get(0).name()).isEqualTo("init");
    assertThat(functions.get(1).name()).isEqualTo("action1");
    assertThat(abi.namedTypes()).hasSize(3);
  }

  @Test
  void abiCoercionSuccess() throws Exception {
    assertSuccess("AbiCoercionSuccess");
  }

  @Test
  void callbackAndActionSuccess() throws Exception {
    assertSuccess("CallbackAndActionSuccess");
  }

  @Test
  void failNoUpgrade() {
    assertCompileFail(
        "FailNoUpgrade", "The contract must have exactly 1 method annotated with @Upgrade");
  }

  @Test
  void failNoInit() {
    assertCompileFail("FailNoInit", "The contract must have exactly 1 method annotated with @Init");
  }

  @Test
  void abiCoercionFailOnNumber() {
    assertCompileFail(
        "AbiCoercionFailOnNumber",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=1]'"
            + " into declared type 'int'");
  }

  @Test
  void abiCoercionFailOnString() {
    assertCompileFail(
        "AbiCoercionFailOnString",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=2]'"
            + " into declared type 'java.lang.String'");
  }

  @Test
  void abiCoercionFailOnVec() {
    assertCompileFail(
        "AbiCoercionFailOnVec",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=1]'"
            + " into declared type 'java.util.List<java.lang.Byte>'");
  }

  @Test
  void abiCoercionFailOnEnum() {
    assertCompileFail(
        "AbiCoercionFailOnEnum",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=1]'"
            + " into declared type 'codegen.AbiCoercionFailOnEnum.MyEnum'");
  }

  @Test
  void abiCoercionFailOnRecord() {
    assertCompileFail(
        "AbiCoercionFailOnRecord",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=1]'"
            + " into declared type 'codegen.AbiCoercionFailOnRecord.MyRecord'");
  }

  @Test
  void abiCoercionFailOnBlockchainAddress() {
    assertCompileFail(
        "AbiCoercionFailOnBlockchainAddress",
        "Cannot coerce type wanted ABI type 'RpcTypeMirror[nullable=false, signed=true, size=1]'"
            + " into declared type 'com.partisiablockchain.BlockchainAddress'");
  }

  @Test
  void initCannotHaveState() {
    assertCompileFail("InitCannotHaveState", "Init method cannot have the state as a parameter");
  }

  @Test
  void initParameterIndicesContext() {
    assertCompileFail(
        "InitParameterIndicesContext",
        "The context parameter was expected at index 0 but was at index 1.");
  }

  @Test
  void initParameterIndicesRpc() {
    assertCompileFail(
        "InitParameterIndicesRpc", "The rpc parameter was expected at index 2 but was at index 1.");
  }

  @Test
  void initReturnType() {
    assertCompileFail(
        "InitReturnType",
        "Expected return type 'com.partisiablockchain.serialization.StateVoid' on init method"
            + " 'init', but got 'void'");
  }

  @Test
  void initVisibility() {
    assertCompileFail(
        "InitVisibility",
        "Init method 'init' must be public non-static, but was 'package-private'.");
  }

  @Test
  void upgradeCannotHaveState() {
    assertCompileFail(
        "UpgradeCannotHaveState", "Upgrade method cannot have the state as a parameter");
  }

  @Test
  void upgradeParameterIndicesStateAccessor() {
    assertCompileFail(
        "UpgradeParameterIndicesStateAccessor",
        "Upgrade method must have a StateAccessor as the first parameter.");
  }

  @Test
  void upgradeParameterIndicesRpc() {
    assertCompileFail(
        "UpgradeParameterIndicesRpc",
        "The rpc parameter was expected at index 2 but was at index 1.");
  }

  @Test
  void upgradeReturnType() {
    assertCompileFail(
        "UpgradeReturnType",
        "Expected return type 'com.partisiablockchain.serialization.StateVoid' on upgrade method"
            + " 'upgrade', but got 'void'");
  }

  @Test
  void upgradeVisibility() {
    assertCompileFail(
        "UpgradeVisibility",
        "Upgrade method 'upgrade' must be public non-static, but was 'package-private'.");
  }

  @Test
  void upgradeNoStateAccessor() {
    assertCompileFail(
        "UpgradeNoStateAccessor",
        "Upgrade method must have a StateAccessor as the first parameter.");
  }

  @Test
  void upgradeNoParams() {
    assertCompileFail(
        "UpgradeNoParams", "Upgrade method must have a StateAccessor as the first parameter.");
  }

  @Test
  void stateIsRecord() throws Exception {
    assertSuccess("StateIsRecord");
  }

  @Test
  void stateIsRecordWithAvlTree() throws Exception {
    assertSuccess("StateIsRecordWithAvlTree");
  }

  @Test
  void recordParamWithState() {
    assertCompileFail(
        "RecordParamWithState", "Invalid type: codegen.RecordParamWithState.StateStruct");
  }

  @Test
  void recordParamWithContext() {
    assertCompileFail(
        "RecordParamWithContext",
        "Invalid type: com.partisiablockchain.contract.sys.SysContractContext");
  }

  @Test
  void upgradeWithRecords() throws Exception {
    assertSuccess("UpgradeWithRecords");
  }

  @Test
  void stateType() throws Exception {
    assertSuccess("StateType");
  }

  @Test
  void stateTypeComplex() throws Exception {
    assertSuccess("StateTypeComplex");
  }

  @Test
  void stateSharedRpcType() throws Exception {
    assertSuccess("StateSharedRpcType");
  }

  @Test
  void stateRecursive() throws Exception {
    assertSuccess("StateRecursive");
  }

  @Test
  void rpcRecursive() throws Exception {
    assertSuccess("RpcRecursive");
  }

  /** Making sure named types are sorted correctly. */
  @Test
  void abiSortedNamedTypes() throws Exception {
    assertSuccess("AbiSortedNamedTypes");
  }

  /** The upgrade method and named types unique to it are not included in the abi. */
  @Test
  void upgradeWithRecordsMixed() throws Exception {
    assertSuccess("UpgradeWithRecordsMixed");
  }

  /** State records collect their types depth first sorted on field names. */
  @Test
  void stateIsRecordDifferentOrder() throws Exception {
    assertSuccess("StateIsRecordDifferentOrder");
  }

  private SuccessfulCompile assertSuccess(String name) throws Exception {
    Compilation compilation = compile(name + ".java");

    List<JavaFileObject> files = extractFileObjects(compilation);

    byte[] rawAbi = null;
    String generatedSource = null;

    String expectedClassName = null;
    String writtenClassName = null;
    for (JavaFileObject file : files) {
      String fileName = Path.of(file.getName()).getFileName().toString();
      if (file.getKind() == JavaFileObject.Kind.OTHER) {
        if (fileName.endsWith(".abi")) {
          rawAbi = readFileObject(file);

          var target = Path.of("target", "classes", fileName);
          Files.write(target, rawAbi);
        } else if (fileName.equals("main")) {
          writtenClassName = new String(readFileObject(file), StandardCharsets.UTF_8).trim();
        }
      } else if (file.getKind() == JavaFileObject.Kind.SOURCE) {
        generatedSource = file.getCharContent(true).toString();
        expectedClassName = "codegen." + fileName.replace(".java", "");
      }
    }

    if (compilation.status() == Compilation.Status.SUCCESS) {
      assertThat(writtenClassName).isEqualTo(expectedClassName);
    } else {
      for (JavaFileObject file : files) {
        if (file.getKind() == JavaFileObject.Kind.SOURCE) {
          System.out.println("Source code for " + file.getName());
          System.out.println(file.getCharContent(true));
          System.out.println();
        }
      }

      Assertions.fail("Compilation should have succeeded but did not.");
    }

    assertCompiledFileIsEqualToReference(name, compilation.generatedSourceFiles().get(0));
    assertGeneratedAbiIsEqualToReference(name, rawAbi);
    assertGeneratedAbiParsesWithClientVersion570(rawAbi);

    FileAbi generatedAbi = new AbiParser(Objects.requireNonNull(rawAbi)).parseAbi();
    return new SuccessfulCompile(generatedAbi, generatedSource);
  }

  private void assertGeneratedAbiIsEqualToReference(String referenceName, byte[] actual)
      throws Exception {
    String resourceName = "src/test/resources/codegen/reference/" + referenceName + ".ref.abi.show";
    File referenceFile = new File(resourceName);
    FileAbi contractAbi = new AbiParser(actual).parseAbi();
    maybeOverwriteRef(
        AbiParser.printModel(contractAbi).getBytes(StandardCharsets.UTF_8), referenceFile);

    assertReferenceEquals(
        referenceName,
        ".ref.abi",
        actual,
        bytes -> {
          FileAbi abi = new AbiParser(bytes).parseAbi();
          return AbiParser.printModel(abi);
        });
  }

  @SuppressWarnings("resource")
  private void assertCompiledFileIsEqualToReference(String referenceName, JavaFileObject compiled)
      throws Exception {
    byte[] bytes = compiled.openInputStream().readAllBytes();
    assertReferenceEquals(
        referenceName,
        ".ref.txt",
        bytes,
        raw -> {
          var content = new String(raw, StandardCharsets.UTF_8);
          return content.replaceAll("\\r\\n?", "\n");
        });
  }

  /**
   * Parse the generated abi as if it had abi version 5.7.0. This checks that the ABIs generated
   * follow the new requirements wrt. named types and function sorting without having to update
   * their abi version yet.
   *
   * <p>This check can be removed once we update the version of the generated ABI.
   *
   * @param rawAbi abi bytes of generated abi
   */
  private void assertGeneratedAbiParsesWithClientVersion570(byte[] rawAbi) {
    byte[] copied = new byte[rawAbi.length];
    System.arraycopy(rawAbi, 0, copied, 0, rawAbi.length);
    copied[9] = 5;
    copied[10] = 7;
    copied[11] = 0;
    new AbiParser(copied).parseAbi();
  }

  @SuppressWarnings("resource")
  private void assertReferenceEquals(
      String name, String suffix, byte[] actualBytes, Function<byte[], Object> toHumanReadable)
      throws Exception {

    String resourceName = "src/test/resources/codegen/reference/" + name + suffix;

    File referenceFile = new File(resourceName);
    if (!referenceFile.exists()) {
      maybeOverwriteRef(actualBytes, referenceFile);

      Assertions.fail("No ABI reference found for %s", name);
    } else {
      byte[] referenceBytes = new FileInputStream(referenceFile).readAllBytes();

      maybeOverwriteRef(actualBytes, referenceFile);

      if (!Arrays.equals(actualBytes, referenceBytes)) {
        Object actualHumanReadable = toHumanReadable.apply(actualBytes);
        Object referenceHumanReadable = toHumanReadable.apply(referenceBytes);
        assertThat(actualHumanReadable).isEqualTo(referenceHumanReadable);
      }
    }
  }

  private static void maybeOverwriteRef(byte[] actualBytes, File referenceFile) throws IOException {
    if (OVERWRITE_REFERENCE) {
      System.out.println("Overwriting reference file " + referenceFile);
      try (FileOutputStream stream = new FileOutputStream(referenceFile)) {
        stream.write(actualBytes);
      }
    }
  }

  @SuppressWarnings({"unchecked", "ConstantConditions"})
  private static List<JavaFileObject> extractFileObjects(Compilation compile)
      throws NoSuchFieldException, IllegalAccessException {
    var field = Compilation.class.getDeclaredField("generatedFiles");
    field.setAccessible(true);
    return (List<JavaFileObject>) field.get(compile);
  }

  @SuppressWarnings({"OptionalGetWithoutIsPresent", "unchecked"})
  private static byte[] readFileObject(JavaFileObject file)
      throws NoSuchFieldException, IllegalAccessException, IOException {
    Field dataField = file.getClass().getDeclaredField("data");
    dataField.setAccessible(true);

    Optional<ByteSource> data = (Optional<ByteSource>) dataField.get(file);
    ByteSource source = data.get();
    return source.read();
  }

  private void assertCompileFail(String className, String message) {
    assertThat(message).isNotBlank();
    String sourceFile = className + ".java";
    Assertions.assertThatThrownBy(() -> compile(sourceFile)).hasMessageContaining(message);
  }

  private Compilation compile(String sourceFile) {
    var source = JavaFileObjects.forResource("codegen/" + sourceFile);
    return javac().withProcessors(new AutoSysContractProcessor()).compile(source);
  }

  @SuppressWarnings("unchecked")
  private static RoundEnvironment roundEnv(Throwable throwMe) {
    var mock = Mockito.mock(RoundEnvironment.class);
    Mockito.when(mock.getElementsAnnotatedWith((Class<? extends Annotation>) Mockito.any()))
        .thenAnswer(
            invocationOnMock -> {
              if (throwMe != null) {
                if (throwMe instanceof CompilationFailedException compilationFailedException) {
                  throw compilationFailedException;
                } else {
                  throw new RuntimeException(throwMe);
                }
              }
              return Set.of();
            });
    return mock;
  }

  private static Messager throwingMessager() {
    var mock = Mockito.mock(Messager.class);
    Mockito.doThrow(new RuntimeException("PRINT MESSAGE"))
        .when(mock)
        .printMessage(Mockito.any(), Mockito.any(), Mockito.any());
    return mock;
  }

  private static ProcessingEnvironment throwingProcessingEnv() {
    var mock = Mockito.mock(ProcessingEnvironment.class);
    var messager = throwingMessager();
    Mockito.when(mock.getMessager()).thenReturn(messager);
    return mock;
  }

  @SuppressWarnings("unused")
  private record SuccessfulCompile(FileAbi abi, String generatedSource) {}
}
