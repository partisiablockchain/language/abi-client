package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.netrc.NetrcParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.apache.maven.artifact.repository.MavenArtifactRepository;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.artifact.ProjectArtifact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class AbiCodeTest {

  private MavenProject project;
  private MavenSession session;
  private AbiCode mojo;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void setUp() {
    final Build build = new Build();
    build.setDirectory("target");

    final Model model = new Model();
    model.setBuild(build);
    model.setVersion("1.0.0");
    model.setGroupId("com.example");
    model.setArtifactId("attachabi-test-artifact");

    project = new MavenProject();
    project.setArtifact(new ProjectArtifact(project));
    project.setModel(model);
    project.setFile(new File("pom.xml").getAbsoluteFile());

    project.setReleaseArtifactRepository(new MavenArtifactRepository());

    session = new MavenSession(null, new DefaultMavenExecutionRequest(), null, List.of(project));

    mojo = createTestMojo();
  }

  @AfterEach
  void deleteGenerated() {
    File sourceFile = Path.of("target/generated-sources/generated-abi").toFile();
    File testSourceFile = Path.of("target/generated-test-sources/generated-abi").toFile();
    deleteDirectory(sourceFile);
    deleteDirectory(testSourceFile);
  }

  @Test
  void generateCode() throws MojoExecutionException, IOException {
    assertThat(project.getCompileSourceRoots().size()).isEqualTo(0);
    mojo.execute();
    Path generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    assertThat(Path.of(project.getCompileSourceRoots().get(0))).isEqualTo(generatedSourceRoot);
    String code =
        Files.readString(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java"));

    assertThat(!code.contains("TransferAction deserializeTransferAction(AbiByteInput _input) {"))
        .isTrue();
    assertThat(
            !code.contains("public static Signature deserializeSpecialSignature(byte[] bytes) {"))
        .isTrue();
    assertThat(code.contains("public static byte[] transfer(BlockchainAddress to, long value) {"))
        .isTrue();
    assertThat(code.contains("public static TokenContractState deserializeState(byte[] bytes) {"))
        .isTrue();
    assertThat(code).contains("@AbiGenerated");
  }

  @Test
  void generateTestCode() throws MojoExecutionException {
    mojo.generateTestSources = true;
    assertThat(project.getTestCompileSourceRoots().size()).isEqualTo(0);
    mojo.execute();
    Path generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-test-sources/generated-abi");
    assertThat(Path.of(project.getTestCompileSourceRoots().get(0))).isEqualTo(generatedSourceRoot);
    assertThat(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java")
                .toFile()
                .exists())
        .isTrue();
  }

  @Test
  void generateCodeAbiPathDir() throws MojoExecutionException {
    mojo.abiPath = "src/test/resources/language";
    mojo.execute();
    assertThat(getFileAsPath("TokenContract_SDK_11_0_0.java").toFile().exists()).isTrue();
  }

  @Test
  void generateCodePbc() throws MojoExecutionException {
    mojo.pbcPath = "src/test/resources/language/token_contract-SDK-11.0.0-pbc.pbc";
    mojo.execute();
    assertThat(getFileAsPath("TokenContract_SDK_11_0_0_pbc.java").toFile().exists()).isTrue();
  }

  @Test
  void generateCodeArgs() throws MojoExecutionException, IOException {
    mojo.deserializeRpc = true;
    mojo.deserializeState = false;
    mojo.serializeActions = false;
    mojo.generateAnnotations = false;
    mojo.namedTypes = Set.of("Signature");
    mojo.execute();
    String code = getFileAsString("TokenContract_SDK_11_0_0.java");

    assertThat(code.contains("TransferAction deserializeTransferAction(AbiInput _input) {"))
        .isTrue();
    assertThat(code.contains("public static Signature deserializeSpecialSignature(byte[] bytes) {"))
        .isTrue();
    assertThat(!code.contains("public static byte[] transfer(BlockchainAddress to, long value) {"))
        .isTrue();
    assertThat(!code.contains("public static TokenContractState deserializeState(byte[] bytes) {"))
        .isTrue();
    assertThat(code).doesNotContain("@AbiGenerated");
  }

  @Test
  void generateCodeFromAbiDirectoryNotExists() {
    mojo.abiPath = "src/does/not/exist";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Abi file does not exist");
  }

  @Test
  void generateCodeFromPbcDirectoryNotExists() {
    mojo.abiPath = null;
    mojo.pbcPath = "src/does/not/exist";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Pbc file does not exist");
  }

  @Test
  void generateCodeDirWithNoAbi() {
    mojo.abiPath = "src/test/resources/dirNoAbi";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Problem with <abiPath>. No ABI file with extension .abi was found.");
  }

  @Test
  void generateCodeDirWithNoPbc() {
    mojo.abiPath = null;
    mojo.pbcPath = "src/test/resources/dirNoAbi";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Problem with <pbcPath>. No PBC file with extension .pbc was found.");
  }

  @Test
  void generateCodeAbiPathPbcPathEqual() {
    mojo.abiPath = "some/directory/with/files";
    mojo.pbcPath = "some/directory/with/files";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("<abiPath> and <pbcPath> are not allowed to be the same");
  }

  @Test
  void generateCodeWrongFileExtension() {
    mojo.abiPath = "src/test/resources/dirNoAbi/text.txt";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Problem with <abiPath>. No ABI file with extension .abi was found.");
  }

  /** Having multiple dependencies defined and an outer classname is not allowed. */
  @Test
  void multipleDependenciesAndClassName() {
    TestArtifactResolver resolver = new TestArtifactResolver();

    mojo.resolver = resolver;

    AbiCode.AbiDependency pubDeploy = new AbiCode.AbiDependency();
    pubDeploy.groupId = "com.partisiablockchain.governance";
    pubDeploy.artifactId = "pubdeploy";
    pubDeploy.version = "4.135.0";

    AbiCode.AbiDependency bpOrchestration = new AbiCode.AbiDependency();
    bpOrchestration.groupId = "com.partisiablockchain.governance";
    bpOrchestration.artifactId = "bp-orchestration-contract";
    bpOrchestration.version = "4.147.0";
    bpOrchestration.className = "BpOrchestration";

    mojo.abis = List.of(pubDeploy, bpOrchestration);
    mojo.className = "FailureClassName";

    assertThatThrownBy(() -> mojo.execute())
        .hasMessageContaining(
            "Cannot specify an outer classname, when using dependencies, try specifying the"
                + " classname in the dependency.");
  }

  /** Having multiple rust dependencies defined and an outer classname is not allowed. */
  @Test
  void multipleRustDependenciesAndClassName() {
    mojo.resolver = new TestArtifactResolver();

    AbiCode.RustContractDependency exampleContractsDependency = getExampleContractsRustDependency();
    mojo.rustDependencies = List.of(exampleContractsDependency);

    mojo.dependencyFetcher = this::stubDependency;

    mojo.className = "FailureClassName";

    assertThatThrownBy(() -> mojo.execute())
        .hasMessageContaining(
            "Cannot specify an outer classname, when using dependencies, try specifying the"
                + " classname in the dependency.");
  }

  /** An outer classname cannot be specified, when there are more than 1 abi in the folder. */
  @Test
  void multipleAbiFilesInFolderAndSpecificClassname() {

    mojo.className = "FailureClassName";
    mojo.abiPath = "src/test/resources/language";
    assertThatThrownBy(() -> mojo.execute())
        .hasMessageContaining(
            "Cannot specify classname, when there is more than one abi in the folder.");
  }

  /**
   * Specifying the classname and only a single ABI is in the folder, generates a class with the
   * given classname using the ABI.
   *
   * @throws MojoExecutionException mojo failures
   * @throws IOException missing files.
   */
  @Test
  void classNameAndSingleFileInFolder() throws MojoExecutionException, IOException {

    mojo.className = "SuccessClassName";

    mojo.abiPath = "src/test/resources/language/single-file-dir";

    ExceptionConverter.run(() -> mojo.execute());

    Path generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    assertThat(Path.of(project.getCompileSourceRoots().get(0))).isEqualTo(generatedSourceRoot);
    String code =
        Files.readString(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("SuccessClassName.java"));
    assertThat(!code.contains("public static TransferRpc deserializeRpc(byte[] bytes) {")).isTrue();
    assertThat(!code.contains("public static Signature deserialize(byte[] bytes) {")).isTrue();
    assertThat(code.contains("public static byte[] transfer(BlockchainAddress to, long value) {"))
        .isTrue();
    assertThat(code.contains("public static TokenContractState deserializeState(byte[] bytes) {"))
        .isTrue();
    assertThat(code).contains("@AbiGenerated");
  }

  /**
   * Generate code for two ABIs specified as dependencies.
   *
   * @throws MojoExecutionException mojo failures
   * @throws IOException missing files.
   */
  @Test
  void generateCodeForMavenArtifact() throws MojoExecutionException, IOException {

    TestArtifactResolver resolver = new TestArtifactResolver();

    mojo.resolver = resolver;

    AbiCode.AbiDependency pubDeploy = new AbiCode.AbiDependency();
    pubDeploy.groupId = "com.partisiablockchain.governance";
    pubDeploy.artifactId = "pubdeploy";
    pubDeploy.version = "4.135.0";

    AbiCode.AbiDependency bpOrchestration = new AbiCode.AbiDependency();
    bpOrchestration.groupId = "com.partisiablockchain.governance";
    bpOrchestration.artifactId = "bp-orchestration-contract";
    bpOrchestration.version = "4.147.0";
    bpOrchestration.className = "BpOrchestration";

    mojo.abiPath = null;
    mojo.abis = List.of(pubDeploy, bpOrchestration);
    mojo.execute();

    String pubDeployCode = getFileAsString("Pubdeploy_4_135_0.java");

    assertThat(pubDeployCode.contains("public final class Pubdeploy_4_135_0 {")).isTrue();
    assertThat(pubDeployCode.contains("public record PublicDeployContractState")).isTrue();
    assertThat(
            pubDeployCode.contains(
                "public static PublicDeployContractState deserializeState(byte[] bytes) {"))
        .isTrue();
    assertThat(
            pubDeployCode.contains(
                "public static byte[] deployContract(byte[] contract, byte[] abi, byte[]"
                    + " initialization) {"))
        .isTrue();

    String bpOrchestrationCode = getFileAsString("BpOrchestration.java");

    assertThat(bpOrchestrationCode.contains("public final class BpOrchestration {")).isTrue();
    assertThat(bpOrchestrationCode.contains("public record BpOrchestrationContractState")).isTrue();
    assertThat(
            bpOrchestrationCode.contains(
                "public enum BlockProducerStatusD {\n"
                    + "    BLOCK_PRODUCER_STATUS$WAITING_FOR_CALLBACK(0),\n"
                    + "    BLOCK_PRODUCER_STATUS$PENDING(1),\n"
                    + "    BLOCK_PRODUCER_STATUS$CONFIRMED(2),\n"
                    + "    BLOCK_PRODUCER_STATUS$PENDING_UPDATE(3),\n"
                    + "    BLOCK_PRODUCER_STATUS$REMOVED(4),\n"))
        .isTrue();
  }

  /**
   * The Maven plugin can generate code for a rust-based contract.
   *
   * @throws MojoExecutionException mojo failures.
   * @throws IOException missing files.
   */
  @Test
  void generateCodeForRustContract() throws MojoExecutionException, IOException {

    AbiCode.RustContractDependency defiContracts = getDefiDependency();

    mojo.rustDependencies = List.of(defiContracts);

    mojo.dependencyFetcher = this::stubDependency;

    mojo.execute();

    assertGeneratedDefi();
  }

  /**
   * The Maven plugin can generate code for a rust-based contract that requires authentication.
   *
   * @throws MojoExecutionException mojo failures.
   * @throws IOException missing files.
   */
  @Test
  void generateCodeForAuthenticatedRustContract() throws MojoExecutionException, IOException {
    mojo.netrcFile =
        NetrcParser.parseNetrcFile(
            "machine someauthenticatedendpoint.com login reallogin password realpassword");

    AbiCode.RustContractDependency authenticatedDependency = getAuthenticatedDependency();

    mojo.rustDependencies = List.of(authenticatedDependency);

    mojo.dependencyFetcher = this::stubDependency;

    mojo.execute();

    assertGeneratedDefi();
  }

  /**
   * The Maven plugin can fetch using HTTPS. This means that it does not stub the {@link
   * com.partisiablockchain.language.netrc.DependencyFetcher}.
   */
  @Test
  void generateCodeForFetchedAuthenticatedRustContract() throws IOException {

    mojo.netrcFile =
        NetrcParser.parseNetrcFile("machine postman-echo.com login postman password password");

    InputStream result =
        mojo.dependencyFetcher.fetchDependency("https://postman-echo.com/basic-auth");
    String resultString = new String(result.readAllBytes(), StandardCharsets.UTF_8);

    assertThat(resultString)
        .isEqualTo(
            """
            {
              "authenticated": true
            }\
            """);
  }

  /**
   * The Maven plugin can generate code for a rust based dependency with multiple rust-based
   * contracts.
   *
   * @throws MojoExecutionException mojo failures.
   * @throws IOException missing files.
   */
  @Test
  void generateCodeForMultipleRustContracts() throws MojoExecutionException, IOException {

    AbiCode.RustContractDependency exampleContractsDependency = getExampleContractsRustDependency();

    mojo.rustDependencies = List.of(exampleContractsDependency);

    mojo.dependencyFetcher = this::stubDependency;

    mojo.execute();

    assertThatThrownBy(() -> getFileAsString("mia_game.java"))
        .isInstanceOf(NoSuchFileException.class);

    assertGeneratedExampleContracts();
  }

  /**
   * The Maven plugin can generate code for multiple rust based dependencies.
   *
   * @throws MojoExecutionException mojo failures.
   * @throws IOException missing files.
   */
  @Test
  void generateCodeForMultipleRustDependencies() throws MojoExecutionException, IOException {

    AbiCode.RustContractDependency exampleContractsDependency = getExampleContractsRustDependency();

    AbiCode.RustContractDependency defiContracts = getDefiDependency();

    mojo.rustDependencies = List.of(exampleContractsDependency, defiContracts);

    mojo.dependencyFetcher = this::stubDependency;

    mojo.execute();

    assertThatThrownBy(() -> getFileAsString("mia_game.java"))
        .isInstanceOf(NoSuchFileException.class);

    assertGeneratedExampleContracts();
    assertGeneratedDefi();
  }

  /** The Maven plugin cannot generate code for a rust-based ABI, when the url is malformed. */
  @Test
  void generateCodeForRustContractMalformedUrl() {
    AbiCode.RustContractDependency malformedDependency = new AbiCode.RustContractDependency();
    malformedDependency.url =
        "ht://gitlab.com/api/v4/projects/51173535/packages/"
            + "generic/defi/4.479.0/defi-4.479.0.tar.gz";

    mojo.rustDependencies = List.of(malformedDependency);

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("MalformedURLException");

    assertThatThrownBy(() -> getFileAsString("TokenV2.java"))
        .isInstanceOf(NoSuchFileException.class);
  }

  /**
   * The Maven plugin cannot generate code for multiple rust dependencies, when the url of one of
   * them is malformed.
   */
  @Test
  void generateCodeForMultipleRustDependenciesMalformedUrl() {

    AbiCode.RustContractDependency malformedUrlDependency = new AbiCode.RustContractDependency();
    malformedUrlDependency.url = "ht:/.gz";

    AbiCode.RustContractDependency exampleContractsDependency = getExampleContractsRustDependency();

    mojo.rustDependencies = List.of(malformedUrlDependency, exampleContractsDependency);

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("MalformedURLException");
  }

  /**
   * The Maven plugin cannot generate code for a rust-based ABI, when the dependency cannot be
   * found.
   */
  @Test
  void generateCodeForRustDependencyNotFound() {
    AbiCode.RustContractDependency malformedDependency = new AbiCode.RustContractDependency();
    malformedDependency.url =
        "https://gitlab.com/api/v4/projects/51173535/packages/generic/defi/4.479.0/defi-4.4.0.tar.gz";

    mojo.rustDependencies = List.of(malformedDependency);

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("HTTP error: response code 404");

    assertThatThrownBy(() -> getFileAsString("TokenV2.java"))
        .isInstanceOf(NoSuchFileException.class);
  }

  /**
   * The Maven plugin cannot generate code for a rust dependency with some specified contract, when
   * one or more of the contracts cannot be found.
   */
  @Test
  void generateCodeForRustContractNotFound() {
    AbiCode.RustContractDependency rustAbiDependency = new AbiCode.RustContractDependency();

    rustAbiDependency.url =
        "https://gitlab.com/api/v4/projects/46029388/packages/generic/example-contracts/5.76.0/example-contracts-5.76.0.tar.gz";

    AbiCode.RustContract pingContract = new AbiCode.RustContract();
    pingContract.contractName = "ping-contract";
    pingContract.className = "Ping";

    AbiCode.RustContract nonExistingContract = new AbiCode.RustContract();
    nonExistingContract.contractName = "non-existing-contract";

    rustAbiDependency.contracts = List.of(pingContract, nonExistingContract);

    mojo.rustDependencies = List.of(rustAbiDependency);

    mojo.dependencyFetcher = this::stubDependency;

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("The contracts non-existing-contract were not found");

    assertThatThrownBy(() -> getFileAsString("Ping.java")).isInstanceOf(NoSuchFileException.class);

    assertThatThrownBy(() -> getFileAsString("NonExistingContract1.java"))
        .isInstanceOf(NoSuchFileException.class);
  }

  /**
   * The Maven plugin cannot generate code for a contract, when the login is not given in the netrc
   * file.
   */
  @Test
  void noLoginProvided() {
    mojo.netrcFile = NetrcParser.parseNetrcFile("machine gitlab.com password somepassword");

    AbiCode.RustContractDependency defiDependency = getDefiDependency();

    mojo.rustDependencies = List.of(defiDependency);

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Login not set in netrc file");
  }

  /**
   * The Maven plugin cannot generate code for a contract, when the password is not given in the
   * netrc file.
   */
  @Test
  void noPasswordProvided() {
    mojo.netrcFile = NetrcParser.parseNetrcFile("machine gitlab.com login somelogin");

    AbiCode.RustContractDependency defiDependency = getDefiDependency();

    mojo.rustDependencies = List.of(defiDependency);

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Password not set in netrc file");
  }

  /**
   * The Maven plugin cannot generate code when the netrc file does not contain the necessary
   * credentials.
   */
  @Test
  void credentialsNotProvided() {
    mojo.netrcFile =
        NetrcParser.parseNetrcFile("machine gitlab.com login gitlablogin password gitlabpassword");

    AbiCode.RustContractDependency dependency = getAuthenticatedDependency();

    mojo.rustDependencies = List.of(dependency);

    mojo.dependencyFetcher = this::stubDependency;

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("HTTP error: response code 401");
  }

  /**
   * The Maven plugin cannot generate code when the credentials given in the netrc file do not
   * authenticate.
   */
  @Test
  void invalidCredentials() {
    mojo.netrcFile =
        NetrcParser.parseNetrcFile(
            "machine authenticateddependency.com login fakelogin password fakepassword");

    AbiCode.RustContractDependency dependency = getAuthenticatedDependency();

    mojo.rustDependencies = List.of(dependency);

    mojo.dependencyFetcher = this::stubDependency;

    assertThatThrownBy(() -> mojo.execute())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("HTTP error: response code 401");
  }

  /** The plugin prioritizes the system environment variables over the netrc in homedir. */
  @Test
  void useNetrcSystemEnv(@TempDir Path temp) throws IOException {
    Files.writeString(temp.resolve(".netrc"), "");

    mojo.netrcFile = null;
    mojo.envVar = () -> Map.of("NETRC", temp.resolve(".netrc").toAbsolutePath().toString());

    assertThatThrownBy(
            () ->
                mojo.fetchRawDependency(
                    "hxxps://someauthenticatedendpoint.com/defi-4.479.0.tar.gz"))
        .isInstanceOf(RuntimeException.class);
  }

  /**
   * If the environment variable "NETRC" is not set, and there is not file in the home dir then
   * reading netrc fails.
   */
  @Test
  void noNetrcInHomeDir(@TempDir Path temp) throws IOException {
    Path homeDir = temp.resolve("home");
    Files.createDirectory(homeDir);

    assertThatThrownBy(() -> AbiCode.getNetrcContents(homeDir, Map.of()))
        .isInstanceOf(RuntimeException.class);
  }

  /**
   * If the environment variable "NETRC" is not set, the plugin finds the file called '.netrc', in
   * the homedir to find the path to the netrc file.
   */
  @Test
  void netrcFromHomeDir(@TempDir Path temp) throws IOException {
    Path homeDir = temp.resolve("home");
    Files.createDirectory(homeDir);

    Path netrcPath = homeDir.resolve(".netrc");
    netrcPath.toFile().createNewFile();
    Files.writeString(netrcPath, "netrc contents");

    assertThat(AbiCode.getNetrcContents(homeDir, Map.of())).isEqualTo("netrc contents");
  }

  /**
   * A file with a file extension other than tar+gzip cannot be decompressed.
   *
   * @throws FileNotFoundException File not found.
   */
  @Test
  void decompressNonCompressedFile() throws FileNotFoundException {
    File abiFile = new File("src/test/resources/language/pubdeploy-4.135.0-abi.abi");

    InputStream inputStream = new FileInputStream(abiFile);

    assertThatThrownBy(() -> mojo.decompressRustDependency(inputStream))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not decompress the given file");
  }

  /** The plugin uses the system environment variables by default to find the netrc file. */
  @Test
  void defaultEnvironmentVariablesUsedAreSystemEnv() {
    Map<String, String> envVarUsed = mojo.envVar.getEnvironmentVariables();
    Map<String, String> sysEnvVars = System.getenv();
    assertThat(envVarUsed).isEqualTo(sysEnvVars);
  }

  private AbiCode.RustContractDependency getDefiDependency() {
    // defi v 4.479.0
    AbiCode.RustContractDependency rustAbiDependency = new AbiCode.RustContractDependency();

    rustAbiDependency.url =
        "https://gitlab.com/api/v4/projects/51173535/packages/generic/defi/4.479.0/defi-4.479.0.tar.gz";

    return rustAbiDependency;
  }

  private AbiCode.RustContractDependency getExampleContractsRustDependency() {
    // example-contracts v 5.76.0
    AbiCode.RustContractDependency rustAbiDependency = new AbiCode.RustContractDependency();

    rustAbiDependency.url =
        "https://gitlab.com/api/v4/projects/46029388/packages/generic/example-contracts/5.76.0/example-contracts-5.76.0.tar.gz";

    AbiCode.RustContract averageSalaryContract = new AbiCode.RustContract();
    averageSalaryContract.contractName = "average_salary";

    AbiCode.RustContract pingContract = new AbiCode.RustContract();
    pingContract.contractName = "ping-contract";
    pingContract.className = "Ping";

    AbiCode.RustContract petitionContract = new AbiCode.RustContract();
    petitionContract.contractName = "Petition";
    petitionContract.className = "Petition";

    rustAbiDependency.contracts = List.of(averageSalaryContract, pingContract, petitionContract);

    return rustAbiDependency;
  }

  private AbiCode.RustContractDependency getAuthenticatedDependency() {
    AbiCode.RustContractDependency dependency = new AbiCode.RustContractDependency();

    dependency.url = "hxxps://someauthenticatedendpoint.com/defi-4.479.0.tar.gz";

    return dependency;
  }

  private void assertGeneratedDefi() throws IOException {
    String tokenContract = getFileAsString("TokenV2.java");

    assertThat(tokenContract.contains("public final class TokenV2 {")).isTrue();
    assertThat(
            tokenContract.contains(
                "public record TokenState(String name, byte decimals, String symbol,"
                    + " BlockchainAddress owner, BigInteger totalSupply,"
                    + " AvlTreeMap<BlockchainAddress, BigInteger> balances,"
                    + " AvlTreeMap<AllowedAddress, BigInteger> allowed) {"))
        .isTrue();
    assertThat(tokenContract.contains("public static TokenState deserializeState(")).isTrue();
  }

  private void assertGeneratedExampleContracts() throws IOException {
    String averageSalaryCode = getFileAsString("AverageSalary.java");

    assertThat(averageSalaryCode.contains("public final class AverageSalary {")).isTrue();
    assertThat(
            averageSalaryCode.contains(
                "public record ContractState(BlockchainAddress administrator, Integer"
                    + " averageSalaryResult, Integer numEmployees) {"))
        .isTrue();

    String pingContractCode = getFileAsString("Ping.java");

    assertThat(pingContractCode.contains("public final class Ping {")).isTrue();
    assertThat(pingContractCode.contains("public record PingContractState() {")).isTrue();

    String petitionCode = getFileAsString("Petition.java");

    assertThat(petitionCode.contains("public final class Petition {")).isTrue();
    assertThat(
            petitionCode.contains(
                "public record PetitionState(List<BlockchainAddress> signedBy, String description)"
                    + " {"))
        .isTrue();
  }

  private AbiCode createTestMojo() {
    AbiCode mojo = new AbiCode();

    mojo.session = session;
    mojo.abiPath = "src/test/resources/language/token_contract-SDK-11.0.0.abi";
    mojo.generateTestSources = false;
    mojo.serializeActions = true;
    mojo.serializeCallbacks = false;
    mojo.deserializeState = true;
    mojo.deserializeRpc = false;
    mojo.generateAnnotations = true;
    mojo.packageName = "com.partisiablockchain.language.abicodegen";
    mojo.netrcFile = NetrcParser.parseNetrcFile("");

    return mojo;
  }

  private FileInputStream stubDependency(String url) {
    File dependency = dependencyUrlToFile(url);

    return ExceptionConverter.call(() -> new FileInputStream(dependency));
  }

  private File dependencyUrlToFile(String url) {
    return switch (url) {
      case "https://gitlab.com/api/v4/projects/51173535/packages/generic/defi/4.479.0/defi-4.479.0.tar.gz" ->
          new File("src/test/resources/codegen/dependencies/defi-4.479.0.tar.gz");
      case "https://gitlab.com/api/v4/projects/46029388/packages/generic/example-contracts/5.76.0/example-contracts-5.76.0.tar.gz" ->
          new File("src/test/resources/codegen/dependencies/example-contracts-5.76.0.tar.gz");
      case "hxxps://someauthenticatedendpoint.com/defi-4.479.0.tar.gz" -> {
        if (!mojo.netrcFile.containsEntry("someauthenticatedendpoint.com")) {
          throw new RuntimeException("HTTP error: response code 401");
        }

        String auth = mojo.getAuthentication("someauthenticatedendpoint.com");

        if (Objects.equals(
            auth,
            Base64.getEncoder()
                .encodeToString("reallogin:realpassword".getBytes(StandardCharsets.UTF_8)))) {
          yield new File("src/test/resources/codegen/dependencies/defi-4.479.0.tar.gz");
        } else {
          throw new RuntimeException("HTTP error: response code 401");
        }
      }
      default -> throw new RuntimeException("Unexpected url for mojo dependency: " + url);
    };
  }

  private static void deleteDirectory(File dir) {
    File[] contents = dir.listFiles();
    if (contents != null) {
      for (File file : contents) {
        deleteDirectory(file);
      }
    }
    dir.delete();
  }

  private String getFileAsString(String fileName) throws IOException {
    return Files.readString(getFileAsPath(fileName));
  }

  private Path getFileAsPath(String fileName) {
    Path generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    return generatedSourceRoot.resolve(mojo.packageName.replace(".", "/")).resolve(fileName);
  }
}
