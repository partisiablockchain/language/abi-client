package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.handler.manager.DefaultArtifactHandlerManager;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.monitor.logging.DefaultLog;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.DefaultMavenProjectHelper;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.artifact.ProjectArtifact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class DeployAbiTest {

  private CapturingLogger capturingLogger;

  private MavenProject project;
  private MavenSession session;
  private DeployAbi mojo;

  private Path target;
  private Path mainTxt;

  @AfterEach
  void tearDown() throws IOException {
    Files.deleteIfExists(mainTxt);
    Files.deleteIfExists(target.resolve("classes"));
    Files.deleteIfExists(target);
  }

  @BeforeEach
  @SuppressWarnings("deprecation")
  void setUp() throws IOException {
    target = Files.createTempDirectory("target");
    Files.createDirectories(target.resolve("classes"));
    mainTxt = target.resolve("classes").resolve("main");

    final var build = new Build();
    build.setDirectory(target.toAbsolutePath().toString());

    final var model = new Model();
    model.setBuild(build);
    model.setGroupId("com.example");
    model.setArtifactId("attachabi-test-artifact");
    model.setVersion("1.0.0");

    project = new MavenProject();
    project.setModel(model);
    project.setFile(new File("pom.xml").getAbsoluteFile());
    project.setArtifact(new ProjectArtifact(project));

    Files.writeString(mainTxt, "com.example.ContractInvoker");

    session = new MavenSession(null, new DefaultMavenExecutionRequest(), null, List.of(project));
    capturingLogger = new CapturingLogger();
    mojo = createTestMojo();
  }

  @Test
  void getProjectFile() {
    assertThat(DeployAbi.projectFile(project)).exists();
  }

  @Test
  void targetFile() {
    assertThat(DeployAbi.targetFile(project, ".")).isDirectory();
  }

  @Test
  void generateAndAttach() throws MojoExecutionException {
    assertThat(project.getAttachedArtifacts()).isEmpty();

    mojo.execute();

    List<Artifact> artifacts = project.getAttachedArtifacts();
    assertThat(artifacts).hasSize(1);
    assertThat(artifacts.get(0).getClassifier()).isEqualTo("abi");
  }

  @Test
  void logsCorrectStuff() throws MojoExecutionException {
    assertThat(capturingLogger.info).isEmpty();

    mojo.execute();

    assertThat(capturingLogger.info).hasSize(1);

    CapturingLogger.Message message = capturingLogger.info.get(0);
    assertThat(message.throwable()).isNull();

    assertThat(message.message()).contains("com.example.Contract");
  }

  @Test
  void noTaggedAutoSysContractThrowsException() throws IOException {
    assertThat(capturingLogger.info).isEmpty();

    Files.deleteIfExists(mainTxt);
    assertThatThrownBy(() -> mojo.execute())
        .hasMessage(
            "No 'main' file generated. Check that you have a class tagged with"
                + " '@AutoSysContract'.");
  }

  /**
   * Create a project helper using reflection to avoid pulling in a DI framework.
   *
   * @return a maven project helper
   */
  private DefaultMavenProjectHelper createProjectHelper() {
    var helper = new DefaultMavenProjectHelper();
    try {
      var field = DefaultMavenProjectHelper.class.getDeclaredField("artifactHandlerManager");
      field.setAccessible(true);
      var artifactHandlerManager = new DefaultArtifactHandlerManager();
      artifactHandlerManager.addHandlers(Map.of("abi", new DefaultArtifactHandler("abi")));

      field.set(helper, artifactHandlerManager);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return helper;
  }

  private DeployAbi createTestMojo() {
    var mojo = new DeployAbi();
    mojo.session = session;
    mojo.mavenProjectHelper = createProjectHelper();
    mojo.setLog(new DefaultLog(capturingLogger));
    return mojo;
  }
}
