package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.lang.annotation.Annotation;
import java.util.List;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeVisitor;
import org.junit.jupiter.api.Test;

final class AbiTypesTest {

  @Test
  void fromNumberClass() {
    assertThat(AbiTypes.fromNumberClass(Byte.TYPE)).isEqualTo(TypeSpec.TypeIndex.i8);
    assertThat(AbiTypes.fromNumberClass(Byte.class)).isEqualTo(TypeSpec.TypeIndex.i8);

    assertThat(AbiTypes.fromNumberClass(Short.TYPE)).isEqualTo(TypeSpec.TypeIndex.i16);
    assertThat(AbiTypes.fromNumberClass(Short.class)).isEqualTo(TypeSpec.TypeIndex.i16);

    assertThat(AbiTypes.fromNumberClass(Integer.TYPE)).isEqualTo(TypeSpec.TypeIndex.i32);
    assertThat(AbiTypes.fromNumberClass(Integer.class)).isEqualTo(TypeSpec.TypeIndex.i32);

    assertThat(AbiTypes.fromNumberClass(Long.TYPE)).isEqualTo(TypeSpec.TypeIndex.i64);
    assertThat(AbiTypes.fromNumberClass(Long.class)).isEqualTo(TypeSpec.TypeIndex.i64);
  }

  @Test
  void invalidFromNumberClass() {
    assertThatThrownBy(() -> AbiTypes.fromNumberClass(Float.class))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid number type: class java.lang.Float");
  }

  @Test
  void fromRpcType() {
    assertThat(AbiTypes.fromRpcType(new RpcTypeMirror(false, true, 8)))
        .isEqualTo(TypeSpec.TypeIndex.i64);
  }

  @Test
  void invalidFromRpcType() {
    assertThatThrownBy(() -> AbiTypes.fromRpcType(new RpcTypeMirror(false, true, 41)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("size=41");
  }

  @Test
  void asNumberClassUnsafe() {
    var type = new AstType(new FloatType(), null);
    assertThatThrownBy(type::asNumberClassUnsafe)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("TestFloatType");
  }

  private static final class FloatType implements PrimitiveType {

    @Override
    public TypeKind getKind() {
      return TypeKind.FLOAT;
    }

    @Override
    public List<? extends AnnotationMirror> getAnnotationMirrors() {
      return List.of();
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
      return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
      return (A[]) new Annotation[0];
    }

    @Override
    public <R, P> R accept(TypeVisitor<R, P> v, P p) {
      return v.visitPrimitive(this, p);
    }

    @Override
    public String toString() {
      return "TestFloatType";
    }
  }
}
