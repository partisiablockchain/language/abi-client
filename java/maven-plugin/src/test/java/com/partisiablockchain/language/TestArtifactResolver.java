package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.util.Collection;
import java.util.List;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.impl.ArtifactResolver;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;

/** Resolve local resource files as artifacts during a test run. */
public final class TestArtifactResolver implements ArtifactResolver {

  @Override
  public ArtifactResult resolveArtifact(RepositorySystemSession session, ArtifactRequest request) {

    File abiFile;
    String artifactId = request.getArtifact().getArtifactId();
    if (artifactId.equals("pubdeploy")) {
      abiFile = new File("src/test/resources/language/pubdeploy-4.135.0-abi.abi");
    } else if (artifactId.equals("bp-orchestration-contract")) {
      abiFile = new File("src/test/resources/language/bp-orchestration-contract-4.147.0-abi.abi");
    } else {
      throw new RuntimeException("Could not recognize maven artifact.");
    }

    TestArtifact artifact = new TestArtifact(abiFile);
    ArtifactResult artifactResult = new ArtifactResult(request);
    artifactResult.setArtifact(artifact);
    return artifactResult;
  }

  @Override
  public List<ArtifactResult> resolveArtifacts(
      RepositorySystemSession session, Collection<? extends ArtifactRequest> requests) {
    return null;
  }
}
