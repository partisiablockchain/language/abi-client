package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import org.codehaus.plexus.logging.AbstractLogger;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.logging.console.ConsoleLogger;

/** A logger that captures all messages in appropriate lists. */
final class CapturingLogger extends AbstractLogger {

  private final Logger delegate = new ConsoleLogger(0, "");

  final List<Message> debug = new ArrayList<>();
  final List<Message> info = new ArrayList<>();
  final List<Message> warn = new ArrayList<>();
  final List<Message> error = new ArrayList<>();
  final List<Message> fatal = new ArrayList<>();

  CapturingLogger() {
    super(0, "CapturingLogger");
  }

  @Override
  public void debug(String message, Throwable throwable) {
    debug.add(new Message(message, throwable));
    delegate.debug(message, throwable);
  }

  @Override
  public void info(String message, Throwable throwable) {
    info.add(new Message(message, throwable));
    delegate.info(message, throwable);
  }

  @Override
  public void warn(String message, Throwable throwable) {
    warn.add(new Message(message, throwable));
    delegate.warn(message, throwable);
  }

  @Override
  public void error(String message, Throwable throwable) {
    error.add(new Message(message, throwable));
    delegate.error(message, throwable);
  }

  @Override
  public void fatalError(String message, Throwable throwable) {
    fatal.add(new Message(message, throwable));
    delegate.fatalError(message, throwable);
  }

  @Override
  public Logger getChildLogger(String name) {
    return null;
  }

  record Message(String message, Throwable throwable) {}
}
