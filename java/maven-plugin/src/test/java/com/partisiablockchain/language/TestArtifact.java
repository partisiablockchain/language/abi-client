package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.util.Map;
import org.eclipse.aether.artifact.Artifact;

/** A local file represented as an Artifact, during a test run. */
public final class TestArtifact implements Artifact {

  private final File abiFile;

  /**
   * Create new {@link TestArtifact}.
   *
   * @param abiFile local abi file
   */
  public TestArtifact(File abiFile) {
    this.abiFile = abiFile;
  }

  @Override
  public String getGroupId() {
    return null;
  }

  @Override
  public String getArtifactId() {
    return null;
  }

  @Override
  public String getVersion() {
    return null;
  }

  @Override
  public Artifact setVersion(String s) {
    return null;
  }

  @Override
  public String getBaseVersion() {
    return null;
  }

  @Override
  public boolean isSnapshot() {
    return false;
  }

  @Override
  public String getClassifier() {
    return null;
  }

  @Override
  public String getExtension() {
    return null;
  }

  @Override
  public File getFile() {
    return abiFile;
  }

  @Override
  public Artifact setFile(File file) {
    return null;
  }

  @Override
  public String getProperty(String s, String s1) {
    return null;
  }

  @Override
  public Map<String, String> getProperties() {
    return null;
  }

  @Override
  public Artifact setProperties(Map<String, String> map) {
    return null;
  }
}
