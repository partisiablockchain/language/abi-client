package com.partisiablockchain.language.codegen;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

final class TypeNameShortenerTest {

  @Test
  void notGeneric() {
    assertSimplified("java.util.List", "List");
    assertSimplified("java.lang.String", "String");
  }

  @Test
  void simplifyKnown() {
    assertSimplified("java.util.List<java.lang.String>", "List<String>");
    assertSimplified("java.util.List<java.lang.String, java.lang.String>", "List<String, String>");
  }

  @Test
  void simplifyKnownNested() {
    assertSimplified(
        "Map<Map<String, String>, Map<String, java.lang.String>>",
        "Map<Map<String, String>, Map<String, String>>");
  }

  @Test
  void simplifyMixed() {
    assertSimplified(
        "java.util.List<java.lang.String, java.util.Vector>", "List<String, java.util.Vector>");
  }

  @Test
  void simplifyUnknown() {
    assertSimplified(
        "java.util.Map<java.util.Vector, java.util.Vector>",
        "java.util.Map<java.util.Vector, java.util.Vector>");

    assertSimplified("java.util.List<java.util.Vector>", "List<java.util.Vector>");
  }

  @Test
  void parseInvalidGeneric() {
    assertThat(TypeNameShortener.parseGeneric("List")).isNull();
  }

  private void assertSimplified(String complicated, String expected) {
    assertThat(TypeNameShortener.shorten(complicated)).isEqualTo(expected);
  }
}
