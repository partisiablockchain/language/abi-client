package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.netrc.NetrcFile;
import com.partisiablockchain.language.netrc.NetrcFile.NetrcEntry;
import com.partisiablockchain.language.netrc.NetrcParser;
import org.junit.jupiter.api.Test;

final class NetrcTest {

  /** The netrc parser can find the nexus entry in a netrc file. */
  @Test
  void validNetrc() {
    NetrcFile netrcFile =
        NetrcParser.parseNetrcFile("machine nexus.secata.com login nexuslogin password nexuspw");

    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isEqualTo("nexuslogin");
    assertThat(nexusNetrcEntry.password()).isEqualTo("nexuspw");
  }

  /** The netrc can find a specified entry in a netrc file with multiple entries. */
  @Test
  void multipleMachines() {
    NetrcFile netrcFile =
        NetrcParser.parseNetrcFile(
            """
            machine m1 login login1 password pw1
            machine m2 login login2 password pw2
            machine m3 login login3 password pw3
            machine nexus.secata.com login nexuslogin password nexuspw
            machine m4 login login4 password pw4\
            """);
    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isEqualTo("nexuslogin");
    assertThat(nexusNetrcEntry.password()).isEqualTo("nexuspw");
  }

  /**
   * The netrc parser can find a specified entry in a netrc file where any number of tabs, spaces
   * and newlines are used as separators.
   */
  @Test
  void netrcFormatSeparators() {
    NetrcFile netrcFile =
        NetrcParser.parseNetrcFile(
            """
            machine m1                 login login1
            password pw1


            machine nexus.secata.com
            login login1
            password pw1
            """);

    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isEqualTo("login1");
    assertThat(nexusNetrcEntry.password()).isEqualTo("pw1");
  }

  /** The netrc parser finds the default entry in a netrc file with a default entry. */
  @Test
  void defaultEntry() {
    NetrcFile netrcFile =
        NetrcParser.parseNetrcFile(
            """
            machine m1 login login1 password pw1

            machine m2 login login2 password pw2

            machine m3 login login3 password pw3

            default login login1 password pw1
            """);

    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isEqualTo("login1");
    assertThat(nexusNetrcEntry.password()).isEqualTo("pw1");
  }

  /**
   * The netrc parser can find the nexus entry in a netrc file, where additional fields are also
   * given.
   */
  @Test
  void additionalFields() {
    NetrcFile netrcFile =
        NetrcParser.parseNetrcFile(
            "machine nexus.secata.com login login1 password pw1 account account1 macdef md1");

    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isEqualTo("login1");
    assertThat(nexusNetrcEntry.password()).isEqualTo("pw1");
    assertThat(nexusNetrcEntry.account()).isEqualTo("account1");
    assertThat(nexusNetrcEntry.macdef()).isEqualTo("md1");
  }

  /** The netrc parser can parse netrc files with the fields given in any order. */
  @Test
  void randomFieldOrder() {
    assertThatCode(
            () ->
                NetrcParser.parseNetrcFile(
                    """
                    machine m1 macdef md1 login login1 account account1 password pw1
                    machine m2 account account2 login login2 macdef md2
                    machine m3 macdef md3 account account3 login login3\
                    """))
        .doesNotThrowAnyException();
  }

  /** Parsing a netrc file with no field results in a NetrcEntry where all fields are null. */
  @Test
  void machineNoFields() {
    NetrcFile netrcFile = NetrcParser.parseNetrcFile("machine nexus.secata.com");

    NetrcEntry nexusNetrcEntry = netrcFile.getEntry("nexus.secata.com");

    assertThat(nexusNetrcEntry.login()).isNull();
    assertThat(nexusNetrcEntry.account()).isNull();
    assertThat(nexusNetrcEntry.macdef()).isNull();
    assertThat(nexusNetrcEntry.password()).isNull();
  }

  /**
   * The netrc parser cannot parse a netrc file, where the netrc format is violated by having
   * machines after the default entry.
   */
  @Test
  void machineAfterDefault() {
    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
                    machine m1 login login1 password pw1
                    machine m2 login login2 password pw2
                    machine m3 login login3 password pw3
                    default login login1 password pw1
                    machine m4 login login4 password pw4
                    """))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Default entry must be last in netrc file");
  }

  /** The netrc parser cannot parse a netrc file that is malformed by having fields for an entry. */
  @Test
  void malformedNetrc() {
    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
                    machine m1 login login1 password pw1
                    machine m2 login login2 password pw2 hello123
                    machine m3 login login3 password pw3
                    """))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file was malformed");
  }

  /** The netrc parser cannot parse a netrc file that is malformed by having empty machine entry. */
  @Test
  void malformedMachineNetrc() {

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
                       machine     login login1 password pw1


                    """))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file was malformed");
  }

  /** The netrc parser cannot parse a netrc file that contains a non-recognized netrc field. */
  @Test
  void invalidField() {

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
                    machine m1 login login1 password pw1
                    machine m2 login login2 password pw2 hello123 120
                    machine m3 login login3 password pw3
                    """))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Unknown netrc field: hello123");
  }

  /** The netrc parser cannot parse a netrc file with duplicate fields for a given machine. */
  @Test
  void duplicateFields() {

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
machine nexus.secata.com login login1 password pw1 account account1 macdef md1 login login2
"""))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file has duplicate field: login");

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
machine nexus.secata.com login login1 password pw1 account account1 macdef md1 macdef macdef2
"""))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file has duplicate field: macdef");

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
machine nexus.secata.com login login1 password pw1 account account1 macdef md1 account account2
"""))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file has duplicate field: account");

    assertThatThrownBy(
            () ->
                NetrcParser.parseNetrcFile(
                    """
machine nexus.secata.com login login1 password pw1 account account1 macdef md1 password password2
"""))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("netrc file has duplicate field: password");
  }
}
