package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

final class NamedTypeCollectorTest {

  @Test
  void disambiguate() {
    var id1 = "a.b.c.D";
    var id2 = "x.b.c.D";
    var id3 = "x.C";

    TypeElement elem1 = mockedElement(id1, ElementKind.RECORD);
    TypeElement elem2 = mockedElement(id2, ElementKind.RECORD);
    TypeElement elem3 = mockedElement(id3, ElementKind.RECORD);

    var disambiguate = new NamedTypeCollector();
    disambiguate.addNamedStateType(mockedElement("a.b.c.d.f.g.X", ElementKind.RECORD));
    disambiguate.addNamedRpcType(elem1);
    disambiguate.addNamedRpcType(elem2);
    disambiguate.addNamedRpcType(elem3);

    disambiguate.disambiguate();

    assertThat(disambiguate.getAbiName(elem1, false)).isEqualTo("a_b_c_D");
    assertThat(disambiguate.getAbiName(elem2, false)).isEqualTo("x_b_c_D");
    assertThat(disambiguate.getAbiName(elem3, false)).isEqualTo("C");
  }

  @Test
  void incrementPackageNames() {
    var identifier =
        new NamedTypeCollector.NameWithMutablePackage("Tester", List.of("com", "example", "foo"));
    assertThat(identifier.reconstructWithPackage()).isEqualTo("Tester");

    assertThat(identifier.incrementPackagePointer()).isTrue();
    assertThat(identifier.reconstructWithPackage()).isEqualTo("foo_Tester");

    assertThat(identifier.incrementPackagePointer()).isTrue();
    assertThat(identifier.reconstructWithPackage()).isEqualTo("example_foo_Tester");

    assertThat(identifier.incrementPackagePointer()).isTrue();
    assertThat(identifier.reconstructWithPackage()).isEqualTo("com_example_foo_Tester");

    assertThat(identifier.incrementPackagePointer()).isFalse();
    assertThat(identifier.reconstructWithPackage()).isEqualTo("com_example_foo_Tester");
  }

  @Test
  void addNamedType() {
    var collector = new NamedTypeCollector();
    collector.addNamedStateType(mockedElement("a.b.c.D", ElementKind.CLASS));

    final Element elemA = mockedElement("com.example.A", ElementKind.RECORD);
    final Element elemB = mockedElement("com.example.B", ElementKind.RECORD);

    assertThat(collector.indexOf(elemA, false)).isEqualTo(-1);
    collector.addNamedRpcType(elemA);
    assertThat(collector.indexOf(elemA, false)).isEqualTo(1);

    assertThat(collector.indexOf(elemB, false)).isEqualTo(-1);
    collector.addNamedRpcType(elemB);
    assertThat(collector.indexOf(elemB, false)).isEqualTo(2);
  }

  TypeElement mockedElement(String name, ElementKind kind) {
    var mockedName = name(name);

    var mock = Mockito.mock(TypeElement.class);
    Mockito.when(mock.getKind()).thenReturn(kind);
    Mockito.when(mock.getQualifiedName()).thenReturn(mockedName);
    return mock;
  }

  Name name(String content) {
    var name = Mockito.mock(Name.class);
    Mockito.when(name.toString()).thenReturn(content);
    return name;
  }
}
