package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;

@SuppressWarnings("test")
@AutoSysContract(StateSharedRpcType.StateStruct.class)
public final class StateSharedRpcType {

  public enum MyEnum {
    A, B;
  }

  public final class StateStruct implements StateSerializable {
    private final Struct a;
    private final MyEnum myEnum;

    public StateStruct(Struct a, MyEnum myEnum) {
      this.a = a;
      this.myEnum = myEnum;
    }
  }

  public record Struct(@RpcType(size = 2, signed = false) int shortWithMask,
                       @RpcType(nullable = true) Boolean b,
                       Boolean c) implements StateSerializable {}



  @Action(2)
  public StateStruct normalAction(SysContractContext context, StateStruct state, Struct x, MyEnum myEnum,
                                  SafeDataInputStream rpc) {
    return state;
  }

  @Init
  public StateStruct init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateStruct upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
