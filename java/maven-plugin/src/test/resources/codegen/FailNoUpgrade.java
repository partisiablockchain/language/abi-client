package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class FailNoUpgrade {

  @Action(1)
  public StateVoid action1(int i1, long l1) {
    return null;
  }

  @Init
  public StateVoid init(int x, int y) {
    return null;
  }
}
