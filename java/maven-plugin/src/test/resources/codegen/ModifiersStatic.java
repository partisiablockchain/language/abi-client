package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class ModifiersStatic {

  @Action(1)
  public static StateVoid action1() {
    return null;
  }

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState) {
    return null;
  }
}
