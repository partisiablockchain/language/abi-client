package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class UpgradeNoParams {

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
 public StateVoid upgrade() {
    return null;
  }
}
