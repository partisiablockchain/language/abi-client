package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.*;
import com.secata.stream.SafeDataInputStream;

@AutoSysContract(RecordParamWithState.StateStruct.class)
public final class RecordParamWithState {

  public record RecordA(StateStruct one) implements StateSerializable {}

  public final class StateStruct implements StateSerializable {

    private final int s;

    public StateStruct(int s) {
      this.s = s;
    }
  }

  @Action(2)
  public StateStruct action(SysContractContext context, StateStruct state, RecordA recordA) {
    return state;
  }

  @Init
  public StateStruct init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateStruct upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
