package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class ValidContractWithNamedConstantsAsShortnames {

  @Action(Actions.ACTION1)
  public StateVoid action1() {
    return null;
  }

  @Action(Actions.ACTION2)
  public StateVoid action2() {
    return null;
  }

  @Callback(Callbacks.CALLBACK1)
  public StateVoid callback1() {
    return null;
  }

  @Init
  public StateVoid init(int x, int y) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor stateAccessor) {
    return null;
  }

  public static final class Actions {
    public static final int ACTION1 = 0;
    public static final int ACTION2 = 1;
  }

  public static final class Callbacks {
    public static final int CALLBACK1 = 42;
  }
}
