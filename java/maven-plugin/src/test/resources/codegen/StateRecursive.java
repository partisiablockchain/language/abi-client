package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;

@SuppressWarnings("test")
@AutoSysContract(StateRecursive.StateStruct.class)
public final class StateRecursive {

  static final class StateStruct implements StateSerializable {
    private final RecursiveClass recursiveClass;
    private final RecursiveRecord recursiveRecord;

    StateStruct(RecursiveClass recursiveClass, RecursiveRecord recursiveRecord) {
      this.recursiveClass = recursiveClass;
      this.recursiveRecord = recursiveRecord;
    }
  }

  static final class RecursiveClass implements StateSerializable {
    private final RecursiveClass recursive;

    RecursiveClass(RecursiveClass recursive) {
      this.recursive = recursive;
    }
  }

  public record RecursiveRecord(RecursiveRecord recursive) implements StateSerializable {}

  @Action(2)
  public StateStruct normalAction(SysContractContext context, StateStruct state, int x,
                                  SafeDataInputStream rpc) {
    return state;
  }

  @Init
  public StateStruct init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateStruct upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}