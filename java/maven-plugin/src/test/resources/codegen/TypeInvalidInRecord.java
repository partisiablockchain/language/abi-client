package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import java.math.BigInteger;
import java.util.List;

@AutoSysContract(StateVoid.class)
public final class TypeInvalidInRecord {

  record RecordA(List<BigInteger> list) {

  }

  @Init
  public StateVoid init(RecordA x) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState) {
    return null;
  }
}
