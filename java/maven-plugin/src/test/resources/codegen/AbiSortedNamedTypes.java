package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

@AutoSysContract(AbiSortedNamedTypes.State.class)
public final class AbiSortedNamedTypes {
  @Callback(2)
  public State callback2(E e) {
    return null;
  }

  @Callback(1)
  public State callback1(D d) {
    return null;
  }

  @Action(2)
  public State action(C c) {
    return null;
  }

  @Action(1)
  public State action1(B b) {
    return null;
  }

  @Init
  public State init(A a) {
    return null;
  }

  @Upgrade
  public State upgrade(StateAccessor oldState) {
    return null;
  }

  record State(InnerA innerA, InnerB innerB) implements StateSerializable {}

  record InnerA(InnerMost innerMost) implements StateSerializable {}

  record InnerMost() implements StateSerializable {}

  record InnerB() implements StateSerializable {}

  record A() {}

  record B() {}

  record C() {}

  record D() {}

  record E() {}
}
