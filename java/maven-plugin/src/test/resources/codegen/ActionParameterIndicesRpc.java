package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;

@AutoSysContract(StateVoid.class)
public final class ActionParameterIndicesRpc {

  @Action(1)
  public StateVoid action1(SysContractContext context,
      StateVoid state, SafeDataInputStream rpc, int x) {
    return null;
  }

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState) {
    return null;
  }
}
