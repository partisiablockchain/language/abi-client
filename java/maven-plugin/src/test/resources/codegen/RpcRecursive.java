package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;

@SuppressWarnings("test")
@AutoSysContract(StateVoid.class)
public final class RpcRecursive {

  public record RecursiveRecord(RecursiveRecord recursive) {}

  @Action(2)
  public StateVoid normalAction(SysContractContext context, StateVoid state, RecursiveRecord recursiveRecord,
                                  SafeDataInputStream rpc) {
    return state;
  }

  @Init
  public StateVoid init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}