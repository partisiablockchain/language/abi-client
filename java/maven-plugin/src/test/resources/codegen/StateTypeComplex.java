package codegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings("test")
@AutoSysContract(StateTypeComplex.StateStruct.class)
public final class StateTypeComplex {

  public final class Key implements StateSerializable, Comparable<Key> {
    private final BlockchainAddress address;
    public Key(BlockchainAddress address) {
      this.address = address;
    }

    @Override
    public int compareTo(@NotNull Key o) {
      return 0;
    }
  }

  public final class Value implements StateSerializable {
    private final int i;
    private final Integer i2;
    private final Hash hash;
    public Value(int i, Integer i2, Hash hash) {
      this.i = i;
      this.i2 = i2;
      this.hash = hash;
    }
  }

  public final class Info implements StateSerializable {
    private final long l;
    private final Hash h;
    public Info(long l, Hash h) {
      this.l = l;
      this.h = h;
    }
  }

  public final class StateStruct implements StateSerializable {
    private final String uuid;
    private final AvlTree<Key, Value> a;
    private final FixedList<Info> b;
    public StateStruct(AvlTree<Key, Value> a, FixedList<Info> b, String uuid) {
      this.a = a;
      this.b = b;
      this.uuid = uuid;
    }
  }



  @Action(2)
  public StateStruct normalAction(SysContractContext context, StateStruct state, int x,
                                  SafeDataInputStream rpc) {
    return state;
  }

  @Init
  public StateStruct init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateStruct upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
