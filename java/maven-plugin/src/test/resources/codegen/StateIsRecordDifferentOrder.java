package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

@AutoSysContract(StateIsRecordDifferentOrder.RecordState.class)
public final class StateIsRecordDifferentOrder {

  public record RecordState(RecordB b, RecordA a) implements StateSerializable {
  }

  public record RecordA() implements StateSerializable {}
  public record RecordB() implements StateSerializable {}

  @Init
  public RecordState init() {
    return null;
  }

  @Action(1)
  public RecordState action1(RecordState state) {
    return null;
  }

  @Upgrade
  public RecordState upgrade(StateAccessor oldState) {
    return null;
  }
}

