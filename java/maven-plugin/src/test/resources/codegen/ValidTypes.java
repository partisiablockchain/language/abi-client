package codegen;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import java.util.List;

@SuppressWarnings("test")
@AutoSysContract(StateVoid.class)
public final class ValidTypes {

  public enum MyEnum {
    A, B;
  }

  public record RecordB(int x,
                        @RpcType(size = 2, signed = false) int shortWithMask,
                        @RpcType(size = 4, signed = false) long withMask,
                        @RpcType(size = 4) int withoutMask,
                        Long y) {

  }

  public record RecordA(RecordB a, RecordB b, String uuid) {

  }

  @Action(1)
  public StateVoid actionWithAllTypes(
      byte a0,
      byte a,
      Byte b,
      short c,
      Short d,
      int e,
      Integer f,
      long g,
      Long h,
      String i,
      byte[] j,
      boolean bool1,
      Boolean bool2,
      BlockchainAddress address,
      Hash hash,
      BlockchainPublicKey blockchainPublicKey,
      BlsPublicKey blsPublicKey,
      Signature signature,
      BlsSignature blsSignature,
      @RpcType(size = 8, signed = false) Unsigned256 u64,
      @RpcType(size = 16, signed = false) Unsigned256 u128,
      @RpcType(nullable = true) Unsigned256 firstU256,
      @RpcType(nullable = false) Unsigned256 secondU256,
      @RpcType(nullable = false) RecordA myRecord,
      @RpcType(size = 32) byte[] sizedArray,
      List<String> notNestedVec,
      List<List<List<List<List<List<String>>>>>> nestedVec,
      @RpcType(nullable = true) String nullableString,
      MyEnum enumeration,
      @RpcType(nullable = true)
      MyEnum nullableEnumeration
  ) {
    return null;
  }

  @Action(2)
  public StateVoid normalAction(SysContractContext context, StateVoid state, int x,
      SafeDataInputStream rpc) {
    return state;
  }

  @Callback(0)
  public StateVoid bytes(
      @RpcType(size = 1, signed = true) byte b1,
      @RpcType(size = 1, signed = false) int b2,
      byte b3,
      Byte b4,
      @RpcType(nullable = true) Byte b5
  ) {
    return null;
  }

  @Action(0)
  public StateVoid shorts(
      @RpcType(size = 2, signed = true) short s1,
      @RpcType(size = 2, signed = false) int s2,
      short s3,
      Short s4,
      @RpcType(nullable = true) Short s5
  ) {
    return null;
  }

  @Action(42)
  public StateVoid builtins(
      Hash hash,
      BlockchainPublicKey blockchainPublicKey,
      BlsPublicKey blsPublicKey,
      Signature signature,
      BlsSignature blsSignature,
      Unsigned256 unsigned256
  ) {
    return null;
  }

  @Init
  public StateVoid init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
