package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class UpgradeParameterIndicesStateAccessor {

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(int x, StateAccessor accessor) {
    return null;
  }
}
