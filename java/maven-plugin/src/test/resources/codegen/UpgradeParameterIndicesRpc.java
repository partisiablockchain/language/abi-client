package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;

@AutoSysContract(StateVoid.class)
public final class UpgradeParameterIndicesRpc {

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, SafeDataInputStream rpc, int x) {
    return null;
  }
}
