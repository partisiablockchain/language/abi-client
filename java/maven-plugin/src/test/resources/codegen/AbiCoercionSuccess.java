package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class AbiCoercionSuccess {

  @Action(1)
  public StateVoid action1(
      @RpcType(size = 1, signed = false) int i1,
      @RpcType(size = 2, signed = false) int i2,
      @RpcType(size = 4) int i3,
      @RpcType(size = 2) short s1,
      @RpcType(size = 4, signed = false) long l1,
      @RpcType(size = 8) long l2,
      @RpcType(size = 8) Long l3,
      @RpcType(size = 8, signed = false) Unsigned256 l4,
      @RpcType(size = 16, signed = false) Unsigned256 l5
  ) {
    return null;
  }

  @Init
  public StateVoid init(int x, int y) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor stateAccessor) {
    return null;
  }
}
