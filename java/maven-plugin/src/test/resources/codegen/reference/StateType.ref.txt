package codegen;

import com.partisiablockchain.contract.reflect.*;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;
import java.util.ArrayList;

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;

import com.secata.stream.SafeDataInputStream;

import codegen.StateType.StateStruct;

/**
 * Generated invoker for contract {@link StateType}.
 */
@com.partisiablockchain.contract.reflect.Generated
public final class StateTypeInvoker extends SysContract<StateStruct> {

  private final StateType delegate;

  /**
   * Default constructor.
   */
  public StateTypeInvoker() {
    try {
      this.delegate = new StateType();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public StateStruct onCreate(SysContractContext context, SafeDataInputStream rpc) {
    return delegate.init(rpc);
  }

  @Override
  public StateStruct onInvoke(SysContractContext context, StateStruct state, SafeDataInputStream rpc) {
    int shortname = rpc.readUnsignedByte();

    switch (shortname) {
      case 2 -> {
        int x = rpc.readInt();
        return delegate.normalAction(context, state, x, rpc);
      }

      default -> throw new IllegalArgumentException("Invalid shortname: " + shortname);
    }

  }

  @Override
  public StateStruct onCallback(SysContractContext context, StateStruct state, CallbackContext callbackContext, SafeDataInputStream rpc) {
    int shortname = rpc.readUnsignedByte();

    switch (shortname) {
      default -> throw new IllegalArgumentException("Invalid shortname: " + shortname);
    }

  }

  @Override
  public StateStruct onUpgrade(StateAccessor oldState, SafeDataInputStream rpc) {
    int x = rpc.readInt();
    return delegate.upgrade(oldState, x, rpc);
  }

}

