package codegen;

import com.partisiablockchain.contract.reflect.*;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;
import java.util.ArrayList;

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;

import com.secata.stream.SafeDataInputStream;

import codegen.StateSharedRpcType.StateStruct;

/**
 * Generated invoker for contract {@link StateSharedRpcType}.
 */
@com.partisiablockchain.contract.reflect.Generated
public final class StateSharedRpcTypeInvoker extends SysContract<StateStruct> {

  private final StateSharedRpcType delegate;

  /**
   * Default constructor.
   */
  public StateSharedRpcTypeInvoker() {
    try {
      this.delegate = new StateSharedRpcType();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public StateStruct onCreate(SysContractContext context, SafeDataInputStream rpc) {
    return delegate.init(rpc);
  }

  @Override
  public StateStruct onInvoke(SysContractContext context, StateStruct state, SafeDataInputStream rpc) {
    int shortname = rpc.readUnsignedByte();

    switch (shortname) {
      case 2 -> {
        codegen.StateSharedRpcType.Struct x = rpc_read__codegen_StateSharedRpcType_Struct(rpc);
        codegen.StateSharedRpcType.MyEnum myEnum = rpc_read__codegen_StateSharedRpcType_MyEnum(rpc);
        return delegate.normalAction(context, state, x, myEnum, rpc);
      }

      default -> throw new IllegalArgumentException("Invalid shortname: " + shortname);
    }

  }

  @Override
  public StateStruct onCallback(SysContractContext context, StateStruct state, CallbackContext callbackContext, SafeDataInputStream rpc) {
    int shortname = rpc.readUnsignedByte();

    switch (shortname) {
      default -> throw new IllegalArgumentException("Invalid shortname: " + shortname);
    }

  }

  @Override
  public StateStruct onUpgrade(StateAccessor oldState, SafeDataInputStream rpc) {
    int x = rpc.readInt();
    return delegate.upgrade(oldState, x, rpc);
  }

  private static codegen.StateSharedRpcType.MyEnum rpc_read__codegen_StateSharedRpcType_MyEnum(SafeDataInputStream rpc) {
    return rpc.readEnum(codegen.StateSharedRpcType.MyEnum.values());
  }

  private static codegen.StateSharedRpcType.Struct rpc_read__codegen_StateSharedRpcType_Struct(SafeDataInputStream rpc) {
    int shortWithMask = rpc.readUnsignedShort();
    Boolean b = null;
    if (rpc.readBoolean()) {
      boolean null_b = rpc.readBoolean();
      b = null_b;
    }

    boolean c = rpc.readBoolean();
    return new codegen.StateSharedRpcType.Struct(shortWithMask, b, c);
  }

}

