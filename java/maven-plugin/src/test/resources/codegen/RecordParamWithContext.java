package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.*;
import com.secata.stream.SafeDataInputStream;

@AutoSysContract(StateVoid.class)
public final class RecordParamWithContext {

  public record RecordA(SysContractContext one) implements StateSerializable {}

  @Action(2)
  public StateVoid action(SysContractContext context, StateVoid state, RecordA recordA) {
    return state;
  }

  @Init
  public StateVoid init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
