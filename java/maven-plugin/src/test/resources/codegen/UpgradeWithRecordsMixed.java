package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class UpgradeWithRecordsMixed {

  public record RecordX(RecordA a, RecordB b) {}

  public record RecordA(int y) {}

  public record RecordB() {}

  public record RecordC() {}

  public record RecordD() {}

  @Init
  public StateVoid init() {
    return null;
  }

  @Action(1)
  public StateVoid action1(RecordA d, RecordC c) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, RecordX val, RecordC c, RecordD d) {
    return null;
  }
}
