package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import java.math.BigInteger;

@AutoSysContract(StateVoid.class)
public final class UpgradeInvalidType {

  @Init
  public StateVoid init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState, BigInteger integer) {
    return null;
  }
}
