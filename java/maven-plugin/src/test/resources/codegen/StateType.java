package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.*;
import com.secata.stream.SafeDataInputStream;

@SuppressWarnings("test")
@AutoSysContract(StateType.StateStruct.class)
public final class StateType {

  public enum MyEnum {
    A, B;
  }

  public record RecordA(RecordB one, RecordB two) implements StateSerializable {}

  public record RecordB(int x,
                        @RpcType(size = 2, signed = false) int shortWithMask,
                        @RpcType(size = 4, signed = false) long withMask,
                        @RpcType(size = 4, nullable = true) int withoutMask,
                        Long y) implements StateSerializable {

  }

  public final class StateStruct implements StateSerializable {

    private static int s;
    private transient int t;
    private final RecordA a;
    private final MyEnum c;
    private final String uuid;
    private final StateBoolean stateBoolean;
    private final StateLong stateLong;
    private final StateString stateString;
    private final LargeByteArray largeByteArray;
    public StateStruct(RecordA a, MyEnum c, String uuid, StateBoolean stateBoolean,
                       StateLong stateLong, StateString stateString, LargeByteArray largeByteArray) {
      this.a = a;
      this.c = c;
      this.uuid = uuid;
      this.stateBoolean = stateBoolean;
      this.stateLong = stateLong;
      this.stateString = stateString;
      this.largeByteArray = largeByteArray;
    }
  }



  @Action(2)
  public StateStruct normalAction(SysContractContext context, StateStruct state, int x,
                                  SafeDataInputStream rpc) {
    return state;
  }

  @Init
  public StateStruct init(SafeDataInputStream rpc) {
    return null;
  }

  @Upgrade
  public StateStruct upgrade(StateAccessor oldState, int x, SafeDataInputStream rpc) {
    return null;
  }
}
