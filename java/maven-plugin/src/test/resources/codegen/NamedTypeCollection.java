package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class NamedTypeCollection {

  record A() {

  }

  record B() {

  }

  record C() {

  }

  @Init
  public StateVoid init(A a) {
    return null;
  }

  @Action(0)
  public StateVoid action0(B b) {
    return null;
  }

  @Callback(0)
  public StateVoid callback0(C c) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor stateAccessor) {
    return null;
  }
}
