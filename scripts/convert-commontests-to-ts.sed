# Delete import statements and class signature
0,/ class /d
# replace test function signature
/@Test/N;s/@Test\s*void \([a-zA-Z0-9]*\)() {/test("\1", () => {/m;
s/@Test\s*public void \([a-zA-Z0-9]*\)() {/test("\1", () => {/m;

# replace BeforeEach function signature
/@BeforeEach/N;s/@BeforeEach\s*void \([a-zA-Z0-9]*\)() {/beforeEach(() => {/m;

# replace private functions with zero or one argument signature
# (variable number of arguments not possible with sed)
# 0 variables
s/private void \([a-zA-Z0-9]*\)() {/const \1 =(() => {/g
s/private \([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)() {/const \2 = ((): \1 => {/g
# 1 variable
s/private void \([a-zA-Z0-9]*\)(\([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)) {/const \1 = ((\3: \2) => {/g
s/private \([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)(\([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)) {/const \2 = ((\4: \3): \1 => {/g
# 2 variables
s/private void \([a-zA-Z0-9]*\)(\([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\), \([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)) {/const \1 = ((\3: \2, \5: \4) => {/g
s/private \([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)(\([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\), \([\.<>a-zA-Z0-9]*\) \([a-zA-Z0-9]*\)) {/const \2 = ((\4: \3, \6: \5): \1 => {/g

# Remove .get from optional
s/.get()//g

## Multiline Consumer
/Consumer<.*=$/N;s/Consumer<\(\S\+\)> \([a-zA-Z0-9]*\) =\s*\([a-zA-Z0-9]*\) ->/const \2 = (\3: \1) =>/g
s/\.accept(/(/g

# BN does not need valueOf
s/BigInteger\.valueOf/new BN/g

# Simple syntax differences
s/final var /const /g
s/var /const /g
s/assertThatThrownBy(/expect(/g
s/assertThat(/expect(/g
s/\.isEqualTo(/\.toEqual(/g
s/\.isEqualToNormalizingNewlines(/\.toEqual(/g
s/\.isNotEqualTo(/\.not.toEqual(/g
s/\.hasSize(/\.toHaveLength(/g
s/\.isSameAs(/\.toBe(/g
/commontests-expect-null/!s/\.isNull(/\.toBeUndefined(/g
s/\.isNull(/\.toBeNull(/g
s/\.isNotNull(/\.toBeDefined(/g
s/\.hasMessage(/\.toThrowError(/g
s/\.isFalse()/\.toBeFalsy()/g
s/\.isTrue()/\.toBeTruthy()/g
/^\s*\.isInstanceOf(.*)\s*$/d
s/\.isInstanceOf([^()]*);/\.toThrowError();/g
s/->/=>/g
/commontests-ignore-BN/!s/BigInteger/BN/g

# ScValueOption - valueOrXX
s/valueOrNull/valueOrUndefined/g

# Simple Lambda
s/(\([a-zA-Z0-9]*\)::\([a-zA-Z0-9]*\))/(() => \1\.\2())/g

# Simple type annotations
/const/!s/^\(\s*\)\([\.a-zA-Z0-9<>]\+\) \([a-zA-Z0-9]\+\) =/\1const \3: \2 =/

# Optional to undefined
s/Optional\.of(/(/g
s/Optional\.empty()/undefined/g

# difference between null and undefined for options - turn null, //undefined into undefined
s/null,\s*\/\/\s*undefined/undefined,/g
s/null\s*\/\/\s*undefined/undefined,/g


# FnKinds
s/FnKind\.isZkKind(/FnKindUtil.isZkKind(/g
s/FnKind\.AS_CONTRACT_KINDS/FnKindUtil.defaultFunctionKinds/g
s/FnKind\.\(.\)/FnKinds\.\l\1/g

# Test Constants - must start with private final
s/^  private final \([a-zA-Z0-9\.)]\+\) \([a-zA-Z0-9)]\+\) =/  const \2: \1 =/g
# Test variables starts with private (no final) and might get assigned later
s/^  private \([a-zA-Z0-9\.)]\+\) \([a-zA-Z0-9)]\+\) =/  let \2: \1 =/g
s/^  private \([a-zA-Z0-9\.)]\+\) \([a-zA-Z0-9)]\+\);/  let \2: \1;/g

## Enums
s/TypeSpec\.TypeIndex\./TypeIndex\./g
s/Configuration\.FunctionFormat\./FunctionFormat\./g
s/Configuration\.ShortnameType\.LEB128/ShortnameType\.leb128/g
s/Configuration\.ShortnameType\.HASH/ShortnameType\.hash/g
s/Configuration\.NamedTypesFormat\./NamedTypesFormat\./g

## Classes
# Abi types
s/new SimpleTypeSpec(/TestingHelper.simpleTypeSpec(/g
s/new VecTypeSpec(/TestingHelper.vecTypeSpec(/g
s/new SetTypeSpec(/TestingHelper.setTypeSpec(/g
s/new MapTypeSpec(/TestingHelper.mapTypeSpec(/g
s/new OptionTypeSpec(/TestingHelper.optionTypeSpec(/g
s/new SizedArrayTypeSpec(/TestingHelper.sizedArrayTypeSpec(/g
s/new NamedTypeRef(/TestingHelper.namedTypeRef(/g
s/new EnumVariant(/TestingHelper.enumVariant(/g
s/new FieldAbi(/TestingHelper.fieldAbi(/g
s/new ArgumentAbi(/TestingHelper.argumentAbi(/g
s/new StructProducer<>(/new StructProducer(/g
s/new EnumVariantProducer<>(/new EnumVariantProducer(/g
s/new ByteArrayInputStream//g
s/new ByteArrayOutputStream()//g
s/new StringBuilder()/[]/g
s/StringBuilder/string[]/g
s/RustSyntaxPrettyPrinter.DocBuilder/DocBuilder/g
s/\.toByteArray()/\.toBuffer()/g
s/new ScValue\([UI][0-9]\+\)(/new ScValueNumber(TypeIndex.\l\1, /g
s/new LinkedHashMap<String, /new Map<string, /g
s/LinkedHashMap/HashMap/g
s/new AvlTreeMapTypeSpec(/TestingHelper.avlTreeMapTypeSpec(/g
s/new StateBytes(/TestingHelper.stateBytes(/g

s/\.put(/\.set(/g

# StructTypeSpec has .field
s/\.field(\([^)]\+\))/\.fields[\1]/g

# ContractInvocation has .shortnameAsString
s/\.shortnameAsString()/\.shortname.toString("hex")/g

# ScValue numbers
s/new ScValueU8(/new ScValueNumber(TypeIndex.u8, /g
s/\.asInt()/\.asNumber()/g
s/\.asLong()/\.asBN()/g

# Hex
s/HexFormat\.of()\.parseHex(/bytesFromHex(/g

# multiline HexFormat.of().parseHex(
/HexFormat.of()$/N;s/HexFormat.of()\s*.parseHex(/bytesFromHex(/gm;

# Base64
s/Base64\.decode(/bytesFromBase64(/g
/Base64\.getDecoder()$/N;s/Base64\.getDecoder()\s*\.decode(/bytesFromBase64(/g

# getter to properties
s/\.item()/\.item/g
s/\.variants()/\.variants/g
s/\.namedTypes()/\.namedTypes/g
s/\.contract()/\.contract/g
s/\.shortnameLength()/\.shortnameLength/g
s/\.structTypes()/\.structTypes/g
/commontests-ignore-name/!s/\.name()/\.name/g
s/\.fields()/\.fields/g
s/\.fieldsMap()/\.fieldsMap/g
s/\.contractInvocations()/\.contractInvocations/g
/commontests-ignore-arg/!s/\.arguments()/\.arguments/g
s/\.stateType()/\.stateType/g
s/\.header()/\.header/g
s/\.versionClient()/\.versionClient/g
s/\.versionBinder()/\.versionBinder/g
s/\.shortname()/\.shortname/g
s/\.type()/\.type/g
s/\.index()/\.index/g
s/\.innerValue()/\.innerValue/g
s/\.contractInvocation()/\.contractInvocation/g
s/\.kind()/\.kind/g
s/\.length()/\.length/g
# s/\.values()/\.values/g
s/\.keyType()/\.keyType/g
s/\.valueType()/\.valueType/g
s/\.typeIndex()/\.typeIndex/g
s/\.originShard()/\.originShard/g
s/\.eventTransaction()/\.eventTransaction/g
s/\.innerEvent()/\.innerEvent/g
s/\.committeeId()/\.committeeId/g
s/\.governanceVersion()/\.governanceVersion/g
s/\.innerTransaction()/\.innerTransaction/g
s/\.cost()/\.cost/g
s/\.transaction()/\.transaction/g
s/\.interactWithContractTransaction()/\.interactWithContractTransaction/g
s/\.payload()/\.payload/g
s/\.secretArgument()/\.secretArgument/g
s/\.data()/\.data/g
s/\.treeId()/\.treeId/g
s/\.map()/\.map/g
s/\.state()/\.state/g
s/\.avlTrees()/\.avlTrees/g
s/\.isEvent()/\.isEvent/g
s/\.doc()/\.doc/g
s/\.kindId()/\.kindId/g
s/\.hasShortname()/\.hasShortname/g
s/\.binderInvocations()/\.binderInvocations/g
## Differences
s/\.bytes()/\.value/g

## Arrays
# array indexing, maybe conflicts with other .get methods
/commontests-ignore-array/!s/\.get(\([^()]*\))/[\1]/g
# for loop
s/for (int /for (let /g
# for each loop
s/for (\(.*\):\(.*\)) {/for (\1of\2) {/g
# HashMap size
/commontests-map-size/s/\.size()/\.size/g
# size() to length
/commontests-ignore-array/!s/\.size()/\.length/g
# subList to slice
s/\.subList(/\.slice(/g

# Bytes alloc
s/new byte\[\([^\[]\+\)\]/Buffer.alloc(\1)/g
# Bytes constructor
s/new byte\[\]\s\?{\([^}]*\)}/Buffer\.from([\1])/g
# Byte[] type
s/byte\[\] \([a-zA-Z0-9)]\+\) =/const \1 =/g
# System.arraycopy for byte[]
s/System.arraycopy(/arraycopy(/g

# List.of
s/List\.of(\([^()]*\))/[\1]/g
s/List\.<\([\.a-zA-Z]\+\)>of(/list<\1>(/g
s/List\.of(/list(/g
s/List<\([\.a-zA-Z]\+\)>/\1[]/g
s/new ArrayList<>()/[]/g
/commontests-ignore-array/!s/\.add(/\.push(/g

# Map.of
s/Map\.of()/new Map()/g
# Handle single entry
s/Map\.of(\([^()]\+\), \([^()]\+\))/new Map([[\1, \2]])/g
# If multiple entries must use .ofEntries (for now only handles one line if needed can
# be expanded to multiline match
s/Map\.ofEntries(\(\(Map\.entry([^()]\+, [^()]\+),\?\s*\)\+\))/new Map([\1])/g
s/Map\.entry(\([^()]\+\), \([^()]\+\))/[\1, \2]/g

# Long numbers
s/_\([0-9a-fA-F]\+\)_\([0-9a-fA-F]\+\)_\([0-9a-fA-F]\+\)L/\1\2\3L/g
s/-0x\([0-9a-fA-F_]\+\)L\([^0-9a-zA-Z]\)/new BN("-\1", "hex")\2/g
s/0x\([0-9a-fA-F_]\+\)L\([^0-9a-zA-Z]\)/new BN("\1", "hex")\2/g
s/-\([0-9_]\+\)L\([^0-9a-zA-Z]\)/new BN("-\1")\2/g
s/\([0-9_]\+\)L\([^0-9a-zA-Z]\)/new BN("\1")\2/g

# Set
s/new HashSet<\([^<]+\)>(/new Set<\1>(/g
s/new HashSet<>(/new Set(/g

## Casting
# byte and short casts are not needed
s/(byte) //g
s/(short) //g
# cast must be on own line.
s/ = (\([a-zA-Z0-9)]\+\)) \(.*\);$/ = \2 as \1;/g

## BN cannot be compared with .toEqual
s/)\.toEqual(\(new BN(.*)\));/.eq(\1))\.toBeTruthy();/g
s/\.equals(new BN/\.eq(new BN/g

# compareTo for BN in TS is cmp
s/\.compareTo/\.cmp/g

## Static methods with generic type
s/\([a-zA-Z0-9)]\+\)\.<\([a-zA-Z0-9)]\+\)>\([a-zA-Z0-9)]\+\)(/\1\.\3<\2>(/g

## Delete custom lines
/commontests-delete/d

# Fix closing brackets
/^}/d
s/^  }/  });/

# Indentation
s/^  //g

# InputStream
s/new DataInputStream/Buffer.from/g

# BigEndianByteOutput throws different error message on TypeScript-side
s/\"Length of number larger than number of bytes\"/\"byte array longer than desired length\"/g

# BitOutput/BitInput
s/BitInput\.create(/new BitInput(/g
s/\.readSignedInt(/\.readSignedNumber(/g
s/\.readUnsignedInt(/\.readUnsignedNumber(/g
s/\.readSignedLong(/\.readSignedBN(/g
s/\.readUnsignedLong(/\.readUnsignedBN(/g

# Conversion of string variable to Json node
s/String\.format("\\"%s\\"", /JSON.stringify(/g

# BlockchainAddress.fromString to Buffer.from
s/BlockchainAddress.fromString(\([^()]\+\))/Buffer.from(\1, "hex")/g
# Multi-line BlockchainAddress.from(
/BlockchainAddress.fromString($/N
s/BlockchainAddress.fromString(\s*"\([a-zA-Z0-9]\+\)")/Buffer.from("\1", "hex")/g

# String
s/: String/: string/g
s/HashMap<String, String>/Map<string, string>/g
s/Set<String>/Set<string>/g

# New binder differences

s/\.contractCall()/?.contractCall!/g
s/\.binderInvocation()/.binderInvocation/g
s/\.chainComponent()/.chainComponent/g
s/\.invocations()/.invocations/g
s/\.binderArguments()/.binderArguments/g
s/\.contractArguments()/.contractArguments/g
s/\.fn()/.fn/g

s/ContractInvocationKind\.fromFnKind(/FunctionKindUtil.fromFnKind(/g
s/new BinderInvocation(/TestingHelper.binderInvocation(/g
s/new BinderFunction(/TestingHelper.binderFunction(/g
s/new ContractInvocationKind(/TestingHelper.contractInvocationKind(/g
s/\.getKindId()/.kindId/g