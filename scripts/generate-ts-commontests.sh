#!/usr/bin/env bash

set -e
# This script takes the java tests placed in the subfolder commontests and converts them into typescript tests
# using the sed script convert-commontests-to-ts.sed

JAVA_TESTS_PATH_MODEL="java/abi-model/src/test/java/com/partisiablockchain/language/abimodel/commontests"
JAVA_TESTS_PATH_CLIENT="java/client/src/test/java/com/partisiablockchain/language/abiclient/commontests"
TS_TESTS_PATH="ts/src/test/commontests"

for file in {$JAVA_TESTS_PATH_MODEL,$JAVA_TESTS_PATH_CLIENT}/*/*; do
  # Get name of output
  S1="${file%"Test.java"}.test.ts"
  S2=${S1##*commontests}
  mkdir -p "$(dirname "${TS_TESTS_PATH}${S2}")"
  # Convert using specified rules and insert imports afterwards
  sed -f scripts/convert-commontests-to-ts.sed "${file}" | sed -f scripts/insert-imports.sed > "${TS_TESTS_PATH}${S2}"

done

# Copy all test ABI generated with the FileAbiBuilder.java
JAVA_TEST_GENERATED_ABI_REFERENCES="java/abi-model/src/test/resources/testdata/cached-references/."
TS_COPIED_ABI_REFERENCES="ts/src/test-resources/cached-references"

mkdir -p "${TS_COPIED_ABI_REFERENCES}"
cp -avr "${JAVA_TEST_GENERATED_ABI_REFERENCES}" "${TS_COPIED_ABI_REFERENCES}"
