#!/usr/bin/env bash

set -e
# This scripts downloads the abi-client version specified by the 'ABI_CLIENT_VERSION' variable and
# generates contract code for all abis present in the 'src/test/resources/abis' folder, placing it in 'target/generated-code/'.

CONTRACT_OUT_PATH="../ts/src/test/abigen-test/generated-contracts/"
ABI_PATHS="../ts/src/test-resources/abigen-test/abis"

mkdir -p ts/src/test/abigen-test/generated-contracts
cd java || exit
# Generate code from all abis
for file in "${ABI_PATHS}"/*; do
  ABI_NAME=${file##*/}
  TMP=${ABI_NAME%.*}
  # Mirror naming convention defined by Abi Maven Generation Plugin; PascalCase contract names and
  # replace '.', '-' with '_'. Initially translate '_' to space, PascalCase using sed and replace rest
  NAME=$(echo "$TMP" | tr '_' ' ' | sed "s/\b\(.\)/\u\1/g" | tr -d ' ' | tr '-' '_' | tr '.' '_')

  ARGS="${file} ${CONTRACT_OUT_PATH}${NAME}.ts"
  # Invoke abi-client cli to generate code
  mvn test-compile -Pabigen-test-main -pl abigen-test -am -Dexec.args="${ARGS}"
  # Replace abi-client import to main import
  sed -i "s/@partisiablockchain\/abi-client/..\/..\/..\/main/g" "${CONTRACT_OUT_PATH}${NAME}.ts"
done

cd .. || exit
