# Add header
1i // This file is auto-generated from Java to ensure TS and Java API's are identical \
// See https://gitlab.com/secata/pbc/language/abi-client/.\
/* eslint-disable */\
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";\
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";\
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";\
import { DocumentationFunction } from "../../../main/model/DocumentationFunction";\
import { DocumentationNamedType } from "../../../main/model/DocumentationNamedType";\
import {AccountPluginInvocationReader} from "../../../main/rpc/AccountPluginInvocationReader";\
import { RealBinderInvocationReader } from "../../../main/rpc/RealBinderInvocationReader";\
import {\
  arraycopy,\
  bytesFromBase64, bytesFromHex, bytesFromStringBe, bytesFromStringLe,\
  castNamedTypeRef, concatBytes, list,\
  requireNonNull,\
  TestingHelper\
} from "../../commontests-util/TestingHelper";\
import {\
  AbiParser,\
  AbiVersion, ArgumentAbi,\
  BinderType,\
  Configuration,\
  ContractAbi,\
  ContractInvocation,\
  FileAbi, FieldAbi,\
  FunctionFormat,\
  MapTypeSpec,\
  OptionTypeSpec,\
  RpcContractBuilder,\
  RpcBuilder,\
  RpcValue,\
  ScValue,\
  ScValueAddress,\
  ScValueArray,\
  ScValueBlsPublicKey,\
  ScValueBlsSignature,\
  ScValueEnum,\
  ScValueHash,\
  ScValueMap, ScValueNumber,\
  ScValueOption,\
  ScValuePublicKey,\
  ScValueSet,\
  ScValueSignature,\
  ScValueString,\
  ScValueStruct,\
  ScValueVector,\
  SetTypeSpec,\
  ShortnameType,\
  SimpleTypeSpec,\
  SizedArrayTypeSpec, StateReader,\
  NamedTypeRef,\
  NamedTypeSpec,\
  NamedTypesFormat,\
  StructTypeSpec,\
  TypeIndex,\
  TypeSpec,\
  VecTypeSpec,\
  AbstractBuilder,\
  RpcReader,\
  JsonRpcConverter,\
  ScValueBool,\
  JsonValueConverter,\
  HashMap,\
  EnumTypeSpec,\
  EnumVariant,\
  RustSyntaxPrettyPrinter,\
  TransactionReader,\
  BigEndianReader,\
  ZkInputBuilder,\
  StateBuilder,\
  ZkSecretReader,\
  TransactionBuilder,\
  AbiBitInput,\
  AbiByteInput,\
  AbiBitOutput,\
  AbiByteOutput,\
  AvlTreeMapTypeSpec,\
  ScValueAvlTreeMap,\
  StateBytes,\
  TransactionKind,\
  BinderAbi,\
  BinderInvocation,\
  ImplicitBinderAbi,\
  ContractInvocationKind,\
} from "../../../main";\
import { BuilderHelper } from "../../commontests-util/BuilderHelper";\
import { StateReaderHelper } from "../../commontests-util/StateReaderHelper";\
import { RpcReaderHelper } from "../../commontests-util/RpcReaderHelper";\
import { ParserHelper } from "../../commontests-util/ParserHelper";\
import { ValueHelper } from "../../commontests-util/ValueHelper";\
import {\
  BigEndianByteOutput,\
  LittleEndianByteInput,\
  LittleEndianByteOutput,\
  BigEndianByteInput, BitOutput, BitInput\
} from "@secata-public/bitmanipulation-ts";\
import BN from "bn.js";\
import { ValidTestHexValues } from "../../ValidTestHexValues";\
import { StructProducer } from "../../../main/builder/StructProducer";\
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";\
import {FileAbiBuilder} from "../../commontests-util/FileAbiBuilder";\
import {FnAbiBuilder} from "../../commontests-util/FnAbiBuilder";\
import {StructTypeBuilder} from "../../commontests-util/StructTypeBuilder";\
import {EnumTypeBuilder} from "../../commontests-util/EnumTypeBuilder";\
import { BinderInvocationBuilder } from "../../commontests-util/BinderInvocationBuilder";\
