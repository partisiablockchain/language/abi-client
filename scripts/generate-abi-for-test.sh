#!/usr/bin/env bash
# This script takes all the contracts located in language/contracts and compiles them using different versions
# of the SDK. The resulting abi files are then stored in resources.

RESOURCE_PATH="client/src/test/resources/com/partisiablockchain/language/abiclient/reference-tests/abi-files"
CONTRACTS_PATH="../../contracts"
ABI_CLIENT_PATH=$PWD

# exclude some contracts
# byoc-outgoing

# Get SDK version as parameter
SDK_TAG="15.4.0"

compile_contract() {
  echo "Running build for ${contract}"
  # shellcheck disable=SC2164
  cd "$1"
  cargo clean
  cargo partisia-contract build --quiet --sdk "git: https://git@gitlab.com/partisiablockchain/language/contract-sdk.git, tag: ${SDK_TAG}"
  # shellcheck disable=SC2164
  cd -P "$ABI_CLIENT_PATH"
  for abi_file in "$1/target/wasm32-unknown-unknown/debug/"*.abi; do
    file="$(basename "${abi_file}")"
    cp -v "${abi_file}" "${RESOURCE_PATH}/${file%.abi}-SDK-${SDK_TAG}.abi"
  done
}
find_contracts_and_compile() {
  mkdir -p "${RESOURCE_PATH}"

  for contract in "$1"/*; do
    # Check that cargo toml exist if it does then compile
    if test -f "${contract}/Cargo.toml"; then
      compile_contract "${contract}"
    # If no toml exists but it is a directory try going further down
    elif test -d "${contract}"; then
      find_contracts_and_compile "${contract}"
    fi
  done
}

# compile all contracts and get the abi file
find_contracts_and_compile ${CONTRACTS_PATH}
