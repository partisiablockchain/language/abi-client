/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiByteInput } from "../abistreams/AbiByteInput";
import { AbstractReader } from "../AbstractReader";
import { HashMap } from "../HashMap";
import { ContractAbi } from "../model/ContractAbi";
import {
  AvlTreeMapTypeSpec,
  BinderType,
  MapTypeSpec,
  NamedTypeRef,
  NamedTypeSpec,
  SetTypeSpec,
  TypeIndex,
  TypeSpec,
} from "../model/Abi";
import { LittleEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { ScValue } from "../value/ScValue";
import { ScValueMap } from "../value/ScValueMap";
import { ScValueSet } from "../value/ScValueSet";
import { ScValueAvlTreeMap } from "../value/ScValueAvlTreeMap";

export class StateReader extends AbstractReader {
  private readonly stateType: TypeSpec | null;
  private readonly avlTrees: Map<number, Array<[Buffer, Buffer]>> | undefined;

  private constructor(
    input: LittleEndianByteInput | Buffer,
    namedTypes: NamedTypeSpec[],
    stateType: TypeSpec | null,
    avlTrees?: Buffer | Map<number, Array<[Buffer, Buffer]>>
  ) {
    let byteInput;
    if ("length" in input) {
      byteInput = new LittleEndianByteInput(input);
    } else {
      byteInput = new LittleEndianByteInput(input.readRemaining());
    }
    super(new AbiByteInput(byteInput), namedTypes);
    this.stateType = stateType;

    if (Buffer.isBuffer(avlTrees)) {
      this.avlTrees = StateReader.readAvlTrees(avlTrees);
    } else {
      this.avlTrees = avlTrees;
    }
  }

  public static create(
    bytes: Buffer,
    contractAbi: ContractAbi | null,
    avlTrees?: Buffer
  ): StateReader {
    const input = new LittleEndianByteInput(bytes);
    if (contractAbi === null) {
      return new StateReader(input, [], null, avlTrees);
    } else {
      const namedTypes = contractAbi.namedTypes;
      const stateType = contractAbi.stateType;
      return new StateReader(input, namedTypes, stateType, avlTrees);
    }
  }

  protected readSet(spec: SetTypeSpec): ScValueSet {
    const length = this.input.readU32();
    const vec: ScValue[] = [];
    for (let i = 0; i < length; i++) {
      vec.push(this.readGeneric(spec.valueType));
    }
    return new ScValueSet(vec);
  }

  protected readMap(spec: MapTypeSpec): ScValueMap {
    const stateMap = new HashMap<ScValue, ScValue>();
    const length = this.input.readU32();
    for (let i = 0; i < length; i++) {
      const key = this.readGeneric(spec.keyType);
      const value = this.readGeneric(spec.valueType);
      stateMap.set(key, value);
    }
    return new ScValueMap(stateMap);
  }

  protected readAvlTreeMap(typeSpec: AvlTreeMapTypeSpec): ScValueAvlTreeMap {
    const treeId = this.input.readI32();
    if (this.avlTrees === undefined) {
      return new ScValueAvlTreeMap(treeId, null);
    }
    const tree = this.avlTrees.get(treeId);
    if (tree === undefined) {
      throw new Error(`Tried to read Avl Tree Map with tree id ${treeId}, but it didn't exist`);
    }
    const map: HashMap<ScValue, ScValue> = new HashMap<ScValue, ScValue>();
    tree.forEach(([keyBytes, valueBytes]) => {
      const key = new StateReader(
        keyBytes,
        this.namedTypes,
        this.stateType,
        this.avlTrees
      ).readGeneric(typeSpec.keyType);
      const value = new StateReader(
        valueBytes,
        this.namedTypes,
        this.stateType,
        this.avlTrees
      ).readGeneric(typeSpec.valueType);
      map.set(key, value);
    });
    return new ScValueAvlTreeMap(treeId, map);
  }

  public readState() {
    const structRef = this.stateType as NamedTypeRef;
    const typeSpec = this.namedTypes[structRef.index] as StructTypeSpec;
    return this.readStruct(typeSpec);
  }

  public readStateValue(spec: TypeSpec): ScValue {
    return this.readGeneric(spec);
  }

  private static readAvlTrees(avlTrees: Buffer): Map<number, Array<[Buffer, Buffer]>> {
    const type: TypeSpec = {
      typeIndex: TypeIndex.Map,
      keyType: { typeIndex: TypeIndex.Option, valueType: { typeIndex: TypeIndex.i32 } },
      valueType: {
        typeIndex: TypeIndex.Option,
        valueType: {
          typeIndex: TypeIndex.Option,
          valueType: {
            typeIndex: TypeIndex.Map,
            keyType: {
              typeIndex: TypeIndex.Option,
              valueType: {
                typeIndex: TypeIndex.Option,
                valueType: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
              },
            },
            valueType: {
              typeIndex: TypeIndex.Option,
              valueType: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
            },
          },
        },
      },
    };
    const contract: ContractAbi = new ContractAbi(BinderType.Public, [], [], [], [], {
      typeIndex: TypeIndex.i32,
    });
    const treeValue = StateReader.create(avlTrees, contract).readGeneric(type);
    const map = new Map();
    /* eslint-disable @typescript-eslint/no-non-null-assertion */
    treeValue
      .mapValue()
      .map.forEach((value, key) =>
        map.set(key.optionValue().innerValue!.asNumber(), StateReader.convertSingleTree(value))
      );
    return map;
  }

  private static convertSingleTree(singleTree: ScValue): Array<[Buffer, Buffer]> {
    const list: Array<[Buffer, Buffer]> = [];
    singleTree
      .optionValue()
      .innerValue!.optionValue()
      .innerValue!.mapValue()
      .map.forEach((value, key) =>
        list.push([
          key.optionValue().innerValue!.optionValue().innerValue!.vecU8Value(),
          value.optionValue().innerValue!.vecU8Value(),
        ])
      );
    return list;
  }
}

export interface StateBytes {
  state: Buffer;
  avlTrees?: Buffer;
}
