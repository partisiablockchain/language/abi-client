/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  Contract,
  SentTransaction,
  SignedTransaction,
  Transaction,
  TransactionTree,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { AbiVersion } from "../model/AbiVersion";

/** Blockchain client for getting contract state and building and sending transactions. */
export interface BlockchainClientForDeployment {
  /**
   * Get the contract state for a given address.
   *
   * @param address contract address
   * @returns contract state
   */
  getContract(address: string): Promise<Contract>;
  /**
   * Sign a transaction.
   *
   * @param transaction transaction to sign
   * @param gasCost gas cost of the transaction
   * @returns the signed transaction
   */
  sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction>;
  /**
   * Send a signed transaction.
   *
   * @param signedTransaction the signed transaction to send
   * @returns the sent transaction
   */
  send(signedTransaction: SignedTransaction): Promise<SentTransaction>;
  /**
   * Wait for a transaction and its spawned events.
   *
   * @param sentTransaction the sent transaction to wait for
   * @returns the executed transaction tree of transaction and its events
   */
  waitForSpawnedEvents(sentTransaction: SentTransaction): Promise<TransactionTree>;
}

export interface VersionInterval {
  min: AbiVersion;
  max: AbiVersion;
}

/**
 * Result of a contract deployment.
 *
 * @param contractAddress address of the deployed contract
 * @param executedTransaction executed transaction tree of the deployment transaction and its
 *     spawned events
 */
export interface DeployResult {
  contractAddress: string;
  executedTransactions: TransactionTree;
}
