/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainClientForDeployment } from "./DeploymentTypes";
import { PbcFile } from "@partisiablockchain/sections";
import { AbiParser } from "../parser/AbiParser";
import {
  SignedTransaction,
  TransactionTree,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { FileAbi } from "../model/FileAbi";
import { ContractAbi } from "../model/ContractAbi";
import {
  DeploymentBuilder,
  findBinderId,
  findFailureCause,
  getSupportedBinderVersions,
} from "./DeploymentBuilder";
import { RpcContractBuilder } from "../rpc/RpcContractBuilder";

/** Builder for upgrading contracts through the {@link DeploymentClient}. */
export class UpgradeBuilder {
  static readonly DEFAULT_UPGRADE_COST = 2_000_000;
  private readonly blockchainClient: BlockchainClientForDeployment;
  private abiBytes: Buffer | undefined;
  private contractBytes: Buffer | undefined;
  private upgradeRpcBytes: Buffer = Buffer.from([]);
  private contractAddressField: string | undefined;
  private gasCostField: number = UpgradeBuilder.DEFAULT_UPGRADE_COST;

  /**
   * Constructor for {@link UpgradeBuilder}.
   *
   * @param blockchainClient client for getting state and building and sending transactions
   */
  constructor(blockchainClient: BlockchainClientForDeployment) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Set the abi and contract bytes to upgrade to through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and {@link #wasm}.
   *
   * @param pbcFile the bytes of the pbc file to upgrade to
   * @returns this builder
   */
  public pbcFile(pbcFile: PbcFile | Buffer): UpgradeBuilder {
    let pbc: PbcFile;
    if (Buffer.isBuffer(pbcFile)) {
      pbc = PbcFile.fromBytes(pbcFile);
    } else {
      pbc = pbcFile;
    }

    this.abiBytes = pbc.getAbiBytes();
    if (pbc.isZk as boolean) {
      throw new Error("Cannot upgrade a zk contract");
    } else {
      this.contractBytes = pbc.getWasmBytes();
    }
    return this;
  }

  /**
   * Set the abi of the contract to upgrade to.
   *
   * <p>It is required to provide the abi either through this method or through {@link #pbcFile}.
   *
   * @param abi abi bytes to upgrade to
   * @returns this builder
   */
  public abi(abi: Buffer): UpgradeBuilder {
    this.abiBytes = abi;
    return this;
  }

  /**
   * Set the contract bytes of the contract to upgrade to.
   *
   * <p>It is required to provide the contract bytes either though this method, or {@link #pbcFile}.
   *
   * @param wasm contract bytes to upgrade to
   * @returns this builder
   */
  public wasm(wasm: Buffer): UpgradeBuilder {
    this.contractBytes = wasm;
    return this;
  }

  /**
   * Set the address of the contract to be upgraded.
   *
   * <p>This is a required parameter
   *
   * @param contractAddress address of the contract to be upgraded
   * @returns this builder
   */
  public contractAddress(contractAddress: string): UpgradeBuilder {
    this.contractAddressField = contractAddress;
    return this;
  }

  /**
   * Set the rpc given to contract on its upgrade. If not set an empty upgrade rpc is used
   *
   * @param upgradeRpc the upgrade rpc
   * @returns this builder
   */
  public upgradeRpc(upgradeRpc: Buffer): UpgradeBuilder {
    this.upgradeRpcBytes = upgradeRpc;
    return this;
  }

  /**
   * Set the gas cost used to upgrade the contract. If not set a default of 2_000_000 gas is used.
   *
   * @param gasCost gas cost used to upgrade the contract
   * @returns this builder
   */
  public gasCost(gasCost: number): UpgradeBuilder {
    this.gasCostField = gasCost;
    return this;
  }

  /**
   * Build the rpc for the upgrade transaction to the deploy-contract.
   *
   * @returns the rpc for the upgrade transaction
   */
  public async buildRpc(): Promise<Buffer> {
    this.validateRequiredParameters();

    const deployContract = await this.blockchainClient
      .getContract(DeploymentBuilder.DEFAULT_PUB_ADDRESS)
      .catch(() => {
        throw new Error("Unable to get deploy contract state");
      });
    const deployAbiBytes = Buffer.from(deployContract.abi, "base64");
    const deployState = Buffer.from(deployContract.serializedContract!, "base64");
    const deployAbi = new AbiParser(deployAbiBytes).parseAbi().contract();

    const abiToBeDeployed = new AbiParser(this.abiBytes!).parseAbi();
    const binderId = this.determineBinderId(abiToBeDeployed, deployAbi, deployState);

    return this.createUpgradeBytes(deployAbi, binderId);
  }

  /**
   * Build the signed transaction of the upgrade call.
   *
   * @returns the signed transaction of the upgrade call.
   */
  public async build(): Promise<SignedTransaction> {
    const rpc = await this.buildRpc();
    return this.blockchainClient.sign(
      { address: DeploymentBuilder.DEFAULT_PUB_ADDRESS!, rpc },
      this.gasCostField
    );
  }

  /**
   * Build and send the upgrade signed transaction to the pub-deploy contract.
   *
   * @returns the executed transaction tree of the transaction.
   */
  public async upgrade(): Promise<TransactionTree> {
    const signedTransaction = await this.build();
    const sentTransaction = await this.blockchainClient.send(signedTransaction).catch(() => {
      throw new Error("Failed to send upgrade transaction");
    });
    const transactionTree = await this.blockchainClient.waitForSpawnedEvents(sentTransaction);
    if (transactionTree.hasFailures()) {
      throw new Error(
        `Transaction '${signedTransaction.identifier()}' failed with cause: ${findFailureCause(transactionTree)}`
      );
    }
    return transactionTree;
  }

  private validateRequiredParameters(): void {
    if (this.contractBytes === undefined) {
      throw new Error("Missing contract bytes");
    }
    if (this.abiBytes === undefined) {
      throw new Error("Missing abi bytes");
    }
    if (this.contractAddressField === undefined) {
      throw new Error("Missing contract address");
    }
  }

  private determineBinderId(
    abiToBeDeployed: FileAbi,
    deployAbi: ContractAbi,
    deployState: Buffer
  ): number {
    const binderVersions = getSupportedBinderVersions(deployState, deployAbi);

    const binderVersion = abiToBeDeployed.versionBinder;
    return findBinderId(binderVersions, binderVersion);
  }

  private createUpgradeBytes(deployAbi: ContractAbi, binderId: number) {
    const fnBuilder = new RpcContractBuilder(deployAbi, "upgradeContract");
    fnBuilder.addAddress(this.contractAddressField!);
    fnBuilder.addI32(binderId);
    fnBuilder.addVecU8(this.contractBytes!);
    fnBuilder.addVecU8(this.abiBytes!);
    fnBuilder.addVecU8(this.upgradeRpcBytes);

    return fnBuilder.getBytes();
  }
}
