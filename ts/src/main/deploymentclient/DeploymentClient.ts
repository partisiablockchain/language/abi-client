/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainClientForDeployment } from "./DeploymentTypes";
import {
  BlockchainTransactionClient,
  ChainControllerApi,
  Configuration,
  SenderAuthentication,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainClientForDeploymentImpl } from "./BlockchainClientForDeploymentImpl";
import { DeploymentBuilder } from "./DeploymentBuilder";
import { UpgradeBuilder } from "./UpgradeBuilder";

/** Client for deploying contracts. */
export class DeploymentClient {
  private readonly blockchainClient: BlockchainClientForDeployment;

  /**
   * Constructor for {@link DeploymentClient}.
   *
   * @param blockchainClient client for getting state and building and sending transactions
   */
  constructor(blockchainClient: BlockchainClientForDeployment) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Create a {@link DeploymentClient} with our default transaction client and state client.
   *
   * @param transactionClient transaction client building and sending transactions
   * @param chainController blockchain client
   * @returns a new {@link DeploymentClient}
   */
  public static create(
    transactionClient: BlockchainTransactionClient,
    chainController: ChainControllerApi
  ) {
    return new DeploymentClient(
      new BlockchainClientForDeploymentImpl(transactionClient, chainController)
    );
  }

  /**
   * Create a {@link DeploymentClient} with targeting a specific base url.
   *
   * @param baseUrl base url of the blockchain to target
   * @param senderAuthentication sender authentication for signing deploy transactions
   * @returns a new {@link DeploymentClient}
   */
  public static createWithUrl(baseUrl: string, senderAuthentication: SenderAuthentication) {
    const configuration = new Configuration({ basePath: baseUrl });
    const chainController: ChainControllerApi = new ChainControllerApi(configuration);
    const transactionClient = BlockchainTransactionClient.create(baseUrl, senderAuthentication);
    return this.create(transactionClient, chainController);
  }

  /**
   * Create a builder for deploying a contract.
   *
   * @returns a DeploymentBuilder
   */
  public builder(): DeploymentBuilder {
    return new DeploymentBuilder(this.blockchainClient);
  }

  /**
   * Create a builder for upgrading a contract.
   *
   * @returns a {@link UpgradeBuilder}
   */
  public upgradeBuilder(): UpgradeBuilder {
    return new UpgradeBuilder(this.blockchainClient);
  }
}
