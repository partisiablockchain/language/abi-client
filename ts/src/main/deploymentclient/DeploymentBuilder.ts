/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainClientForDeployment, DeployResult, VersionInterval } from "./DeploymentTypes";
import { PbcFile } from "@partisiablockchain/sections";
import { AbiParser } from "../parser/AbiParser";
import { ContractAbi } from "../model/ContractAbi";
import { StateReader } from "../state/StateReader";
import { AbiVersion } from "../model/AbiVersion";
import { ScValueStruct } from "../value/ScValueStruct";
import {
  ExecutedTransaction,
  SignedTransaction,
  TransactionTree,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { AbstractBuilder } from "../builder/AbstractBuilder";
import { FileAbi } from "../model/FileAbi";
import { RpcContractBuilder } from "../rpc/RpcContractBuilder";

export class DeploymentBuilder {
  static readonly DEFAULT_PUB_ADDRESS: string = "0197a0e238e924025bad144aa0c4913e46308f9a4d";
  static readonly DEFAULT_REAL_ADDRESS: string = "018bc1ccbb672b87710327713c97d43204905082cb";
  private static readonly DEFAULT_ALLOWED_JURISDICTION: number[][] = [];
  private static readonly DEFAULT_REQUIRED_STAKES: number = 2000_0000;
  private static readonly DEFAULT_PUB_GAS_COST: number = 800_000;
  private static readonly DEFAULT_REAL_GAS_COST: number = 2_000_000;

  private readonly blockchainClient: BlockchainClientForDeployment;

  private isZk: boolean | undefined;
  private contractBytes: Buffer | undefined;
  private abiBytes: Buffer | undefined;
  private initRpcBytes: Buffer | ((builder: AbstractBuilder) => void) | undefined;
  private deployContractAddressField: string | undefined;
  private allowedJurisdictionsField: number[][] | undefined;
  private requiredStakesField: number | undefined;
  private gasCostField: number | undefined;
  private binderIdField: number | undefined;

  constructor(blockchainClient: BlockchainClientForDeployment) {
    this.blockchainClient = blockchainClient;
  }

  /**
   * Set the abi and contract bytes to deploy through a pbc file.
   *
   * <p>It is required to provide the abi and the contract bytes, either through this method or
   * through a combination of {@link #abi} and either {@link #wasm} or {@link #zkwa}.
   *
   * @param pbcFile the pbc file to deploy
   * @returns this builder
   */
  public pbcFile(pbcFile: PbcFile | Buffer): DeploymentBuilder {
    let pbc: PbcFile;
    if (Buffer.isBuffer(pbcFile)) {
      pbc = PbcFile.fromBytes(pbcFile);
    } else {
      pbc = pbcFile;
    }

    this.isZk = pbc.isZk;
    this.abiBytes = pbc.getAbiBytes();
    if (pbc.isZk as boolean) {
      this.contractBytes = pbc.getZkwaBytes();
    } else {
      this.contractBytes = pbc.getWasmBytes();
    }
    return this;
  }

  /**
   * Set the abi of the contract to deploy.
   *
   * <p>It is required to provide the abi either through this method or through {@link #pbcFile}.
   *
   * @param abi abi bytes to deploy
   * @returns this builder
   */
  public abi(abi: Buffer): DeploymentBuilder {
    this.abiBytes = abi;
    return this;
  }

  /**
   * Set the contract bytes of the contract to deploy. Using this function denotes that it is a zk
   * contract you are trying to deploy.
   *
   * <p>It is required to provide the contract bytes either though this method, {@link wasm}, or
   * {@link pbcFile}.
   *
   * @param zkwa contract bytes to deploy
   * @returns this builder
   */
  public zkwa(zkwa: Buffer): DeploymentBuilder {
    this.isZk = true;
    this.contractBytes = zkwa;
    return this;
  }

  /**
   * Set the contract bytes of the contract to deploy. Using this function denotes that it is a
   * public contract you are trying to deploy.
   *
   * <p>It is required to provide the contract bytes either though this method, {@link #zkwa}, or
   * {@link #pbcFile}.
   *
   * @param wasm contract bytes to deploy
   * @returns this builder
   */
  public wasm(wasm: Buffer): DeploymentBuilder {
    this.isZk = false;
    this.contractBytes = wasm;
    return this;
  }

  /**
   * Set the initialization bytes of the contract to deploy.
   *
   * <p>This is a required parameter.
   *
   * @param initRpc the initialization bytes
   * @returns this builder
   */
  public initRpc(initRpc: Buffer | ((builder: AbstractBuilder) => void)): DeploymentBuilder {
    this.initRpcBytes = initRpc;
    return this;
  }

  /**
   * Set the contract address of the deploy contract to target.
   *
   * <p>Not a required parameter. The default deploy contract address used depends on if you are
   * deploying a public or a zk contract.
   *
   * <ul>
   *   <li>Default pub deploy address: 0197a0e238e924025bad144aa0c4913e46308f9a4d
   *   <li>Default zk deploy address: 018bc1ccbb672b87710327713c97d43204905082cb
   * </ul>
   *
   * @param deployContractAddress address of the deploy contract to target
   * @returns this builder
   */
  public deployContractAddress(deployContractAddress: string): DeploymentBuilder {
    this.deployContractAddressField = deployContractAddress;
    return this;
  }

  /**
   * Set the allowed jurisdictions from which zk nodes can be selected. Follows the ISO 3166-1
   * numeric format, meaning jurisdictions are integers in [0-999].
   *
   * <p>This argument is not allowed if trying to deploy a public contract. It is not required if
   * deploying a zk contract and a default of all jurisdictions allowed is used if it is not set.
   *
   * <p>There are three cases for the jurisdictions:
   *
   * <ul>
   *   <li>An outer list containing nothing: All jurisdictions are allowed for all requested nodes.
   *   <li>An outer list containing one inner list: Only jurisdictions in the inner list are allowed
   *       for all requested nodes.
   *   <li>An outer list containing multiple inner lists: Jurisdictions in the first inner list are
   *       allowed for the first requested node, jurisdictions in the second list for the second
   *       requested node etc.
   * </ul>
   *
   * @param allowedJurisdictions the jurisdictions from which zk nodes can be selected. Follows the
   *     ISO 3166-1 numeric format, meaning jurisdictions are integers in [0-999]
   * @returns this builder
   */
  public allowedJurisdictions(allowedJurisdictions: number[][]): DeploymentBuilder {
    this.allowedJurisdictionsField = allowedJurisdictions;
    return this;
  }

  /**
   * Set the required stakes from the nodes selected for the deployed zk contract.
   *
   * <p>This argument is not allowed if trying to deploy a public contract.
   *
   * <p>It is not required if deploying a zk contract and a default of the minimum amount of stakes
   * '2_000_0000' is used if it is not set.
   *
   * @param requiredStakes the amount of stakes required for the computation.
   * @returns this builder
   */
  public requiredStakes(requiredStakes: number): DeploymentBuilder {
    this.requiredStakesField = requiredStakes;
    return this;
  }

  /**
   * Set the binder id used to determine which binder to deploy the contract with.
   *
   * <p>This argument is not required. By default it will read the state of the deploy contract to
   * find a binder which supports the given contract.
   *
   * @param binderId binder id
   * @returns this builder
   */
  public binderId(binderId: number): DeploymentBuilder {
    this.binderIdField = binderId;
    return this;
  }

  /**
   * Set the gas cost used to deploy the contract.
   *
   * <p>The default gas cost depends on if you are deploying a public or a zk contract
   *
   * <ul>
   *   <li>Default public gas cost: 800_000
   *   <li>Default public zk cost: 2_000_000
   * </ul>
   *
   * @param gasCost gas cost used to deploy the contract
   * @returns this builder
   */
  public gasCost(gasCost: number): DeploymentBuilder {
    this.gasCostField = gasCost;
    return this;
  }

  /**
   * Build the rpc for the transaction to the deploy contract.
   *
   * @returns the rpc for the deploy contract
   */
  public async buildRpc(): Promise<Buffer> {
    this.validateRequiredParameters();
    this.validateOptionalParameters();

    const deployContract = await this.blockchainClient
      .getContract(this.deployContractAddressField!)
      .catch(() => {
        throw new Error("Unable to get deploy contract state");
      });
    const deployAbiBytes = Buffer.from(deployContract.abi, "base64");
    const deployState = Buffer.from(deployContract.serializedContract!, "base64");
    const deployAbi = new AbiParser(deployAbiBytes).parseAbi().contract();

    const abiToBeDeployed = new AbiParser(this.abiBytes!).parseAbi();
    const binderId = this.determineBinderId(abiToBeDeployed, deployAbi, deployState);

    const initializationBytes = this.getInitializationBytes(abiToBeDeployed);

    if (this.isZk!) {
      return this.realDeployRpc(deployAbi, initializationBytes, binderId);
    } else {
      return this.pubDeployRpc(deployAbi, initializationBytes, binderId);
    }
  }

  /**
   * Build the signed transaction of the deploy call.
   *
   * @returns the signed transaction of the deploy call.
   */
  public async build(): Promise<SignedTransaction> {
    const rpc = await this.buildRpc();
    let gasCost: number;
    if (this.gasCostField !== undefined) {
      gasCost = this.gasCostField;
    } else if (this.isZk!) {
      gasCost = DeploymentBuilder.DEFAULT_REAL_GAS_COST;
    } else {
      gasCost = DeploymentBuilder.DEFAULT_PUB_GAS_COST;
    }
    return this.blockchainClient.sign({ address: this.deployContractAddressField!, rpc }, gasCost);
  }

  /**
   * Build and send the signed transaction to the deploy contract.
   *
   * @returns the contract address of deployed contract, and the executed transaction tree of the
   *     transaction.
   */
  public async deploy(): Promise<DeployResult> {
    const signedTransaction = await this.build();
    const sentTransaction = await this.blockchainClient.send(signedTransaction).catch(() => {
      throw new Error("Failed to send deployment transaction");
    });
    const transactionTree = await this.blockchainClient.waitForSpawnedEvents(sentTransaction);
    if (transactionTree.hasFailures()) {
      throw new Error(
        `Deployment '${signedTransaction.identifier()}' failed with cause: ${findFailureCause(transactionTree)}`
      );
    }
    const addressType = this.isZk! ? "03" : "02";
    const contractAddress = addressType + signedTransaction.identifier().slice(24, 64);
    return { contractAddress, executedTransactions: transactionTree };
  }

  private validateRequiredParameters(): void {
    if (this.contractBytes === undefined) {
      throw new Error("Missing contract bytes");
    }
    if (this.abiBytes === undefined) {
      throw new Error("Missing abi bytes");
    }
    if (this.initRpcBytes === undefined) {
      throw new Error("Missing initialization bytes");
    }
  }

  private validateOptionalParameters(): void {
    if (this.deployContractAddressField === undefined) {
      if (this.isZk!) {
        this.deployContractAddressField = DeploymentBuilder.DEFAULT_REAL_ADDRESS;
      } else {
        this.deployContractAddressField = DeploymentBuilder.DEFAULT_PUB_ADDRESS;
      }
    }

    if (this.isZk! && this.allowedJurisdictionsField === undefined) {
      this.allowedJurisdictionsField = DeploymentBuilder.DEFAULT_ALLOWED_JURISDICTION;
    }
    if (!this.isZk! && this.allowedJurisdictionsField !== undefined) {
      throw new Error("Allowed jurisdictions is not allowed as argument to pub deploy");
    }
    if (this.isZk! && this.requiredStakesField === undefined) {
      this.requiredStakesField = DeploymentBuilder.DEFAULT_REQUIRED_STAKES;
    }
    if (!this.isZk! && this.requiredStakesField !== undefined) {
      throw new Error("Required stakes is not allowed as argument to pub deploy");
    }
  }

  private determineBinderId(
    abiToBeDeployed: FileAbi,
    deployAbi: ContractAbi,
    deployState: Buffer
  ): number {
    if (this.binderIdField !== undefined) {
      return this.binderIdField;
    }

    const binderVersions = getSupportedBinderVersions(deployState, deployAbi);

    const binderVersion = abiToBeDeployed.versionBinder;
    return findBinderId(binderVersions, binderVersion);
  }

  private realDeployRpc(
    deployAbi: ContractAbi,
    initializationBytes: Buffer,
    binderId: number
  ): Buffer {
    const fnBuilder = new RpcContractBuilder(deployAbi, "deployContractV3");
    fnBuilder.addVecU8(this.contractBytes!);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addVecU8(this.abiBytes!);
    fnBuilder.addI64(this.requiredStakesField!);
    const vecBuilder = fnBuilder.addVec();
    for (const allowedJurisdiction of this.allowedJurisdictionsField!) {
      const vecVecBuilder = vecBuilder.addVec();
      for (const allowed of allowedJurisdiction) {
        vecVecBuilder.addI32(allowed);
      }
    }
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  private pubDeployRpc(
    deployAbi: ContractAbi,
    initializationBytes: Buffer,
    binderId: number
  ): Buffer {
    const fnBuilder = new RpcContractBuilder(deployAbi, "deployContractWithBinderId");
    fnBuilder.addVecU8(this.contractBytes!);
    fnBuilder.addVecU8(this.abiBytes!);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  private getInitializationBytes(abiToBeDeployed: FileAbi): Buffer {
    if (Buffer.isBuffer(this.initRpcBytes)) {
      return this.initRpcBytes;
    }
    const init = abiToBeDeployed.contract().init();
    const fnBuilder: RpcContractBuilder = new RpcContractBuilder(
      abiToBeDeployed.contract(),
      Buffer.from("ffffffff0f", "hex"),
      init!.kind
    );
    this.initRpcBytes!(fnBuilder);
    return fnBuilder.getBytes();
  }
}

export function getSupportedBinderVersions(
  deployState: Buffer,
  deployAbi: ContractAbi
): Map<number, VersionInterval> {
  const state = StateReader.create(deployState, deployAbi).readState();
  const binders = state.getFieldValue("binders")!.optionValue().innerValue!.mapValue();
  const entries: Array<[number, VersionInterval]> = [...binders.map.entries()].map(([k, v]) => [
    k.optionValue().innerValue!.asNumber(),
    versionIntervalFromScBinderInfo(v.optionValue().innerValue!.structValue()),
  ]);
  return new Map<number, VersionInterval>(entries);
}

function versionIntervalFromScBinderInfo(binderInfo: ScValueStruct): VersionInterval {
  const versionInterval = binderInfo
    .getFieldValue("versionInterval")!
    .optionValue()
    .innerValue!.structValue();
  const scMin = versionInterval
    .getFieldValue("supportedBinderVersionMin")!
    .optionValue()
    .innerValue!.structValue();
  const scMax = versionInterval
    .getFieldValue("supportedBinderVersionMax")!
    .optionValue()
    .innerValue!.structValue();
  const min = new AbiVersion(
    scMin.getFieldValue("major")!.asNumber(),
    scMin.getFieldValue("minor")!.asNumber(),
    scMin.getFieldValue("patch")!.asNumber()
  );
  const max = new AbiVersion(
    scMax.getFieldValue("major")!.asNumber(),
    scMax.getFieldValue("minor")!.asNumber(),
    scMax.getFieldValue("patch")!.asNumber()
  );
  return { min, max };
}

export function findBinderId(
  binders: Map<number, VersionInterval>,
  binderVersion: AbiVersion
): number {
  const entry = [...binders.entries()].find(([_, v]) => checkDeployVersion(binderVersion, v));

  if (entry === undefined) {
    throw new Error("No binders exists supporting version " + binderVersion.toString());
  }
  return entry[0];
}

function checkDeployVersion(binderVersion: AbiVersion, versionInterval: VersionInterval) {
  return checkVersion(
    binderVersion,
    versionInterval.max.major,
    versionInterval.max.minor,
    versionInterval.min.major,
    versionInterval.min.minor
  );
}

function checkVersion(
  binderVersion: AbiVersion,
  maxMajor: number,
  maxMinor: number,
  minMajor: number,
  minMinor: number
) {
  return (
    maxMajor >= binderVersion.major &&
    maxMinor >= binderVersion.minor &&
    minMajor <= binderVersion.major &&
    minMinor <= binderVersion.minor
  );
}

export function findFailureCause(transactionTree: TransactionTree) {
  const errorMsg = executedToErrorMsg(transactionTree.transaction);
  if (errorMsg !== undefined) {
    return errorMsg;
  }
  return transactionTree.events.map((t) => executedToErrorMsg(t)).find((p) => p !== undefined);
}

function executedToErrorMsg(executedTransaction: ExecutedTransaction): string | undefined {
  if (executedTransaction.executionStatus!.success) {
    return undefined;
  }
  return executedTransaction.executionStatus!.failure!.errorMessage;
}
