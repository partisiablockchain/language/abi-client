/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  Transaction,
  SignedTransaction,
  SentTransaction,
  TransactionTree,
  BlockchainTransactionClient,
  ChainControllerApi,
  Contract,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainClientForDeployment } from "./DeploymentTypes";

/**
 * Default implementation of BlockchainClientForDeployment using
 * BlockchainTransactionClient and ChainControllerApi.
 */
export class BlockchainClientForDeploymentImpl implements BlockchainClientForDeployment {
  private readonly transactionClient: BlockchainTransactionClient;
  private readonly chainController: ChainControllerApi;

  /**
   * Constructor for {@link BlockchainClientForDeploymentImpl}.
   *
   * @param transactionClient the underlying transaction client
   * @param chainController the underlying chainController
   */
  constructor(transactionClient: BlockchainTransactionClient, chainController: ChainControllerApi) {
    this.transactionClient = transactionClient;
    this.chainController = chainController;
  }

  getContract(address: string): Promise<Contract> {
    return this.chainController.getContract({ address });
  }

  sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction> {
    return this.transactionClient.sign(transaction, gasCost);
  }

  send(signedTransaction: SignedTransaction): Promise<SentTransaction> {
    return this.transactionClient.send(signedTransaction);
  }

  waitForSpawnedEvents(sentTransaction: SentTransaction): Promise<TransactionTree> {
    return this.transactionClient.waitForSpawnedEvents(sentTransaction);
  }
}
