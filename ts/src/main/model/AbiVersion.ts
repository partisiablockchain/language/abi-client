/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

export class AbiVersion {
  public readonly major: number;
  public readonly minor: number;
  public readonly patch: number;

  constructor(major: number, minor: number, patch: number) {
    this.major = major;
    this.minor = minor;
    this.patch = patch;
  }

  public toString(): string {
    return `${this.major}.${this.minor}.${this.patch}`;
  }

  public withZeroPatch(): AbiVersion {
    return new AbiVersion(this.major, this.minor, 0);
  }

  public static parseFromString(stringVersion: string): AbiVersion {
    const pattern = /^(\d+)\.(\d+)\.(\d+)$/;
    const matches = stringVersion.match(pattern);
    if (pattern.test(stringVersion) && matches !== null) {
      const major = parseInt(matches[1]);
      const minor = parseInt(matches[2]);
      const patch = parseInt(matches[3]);

      return new AbiVersion(major, minor, patch);
    } else {
      throw new Error("Invalid version: " + stringVersion);
    }
  }
}
