/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BinderInvocation,
  BinderType,
  ChainComponent,
  ContractInvocationKind,
  TransactionKind,
  NamedTypeRef,
  NamedTypeSpec,
  TypeSpec,
} from "./Abi";
import { StructTypeSpec } from "./StructTypeSpec";
import { ContractInvocation } from "./ContractInvocation";

export class ContractAbi implements ChainComponent {
  public readonly binderType: BinderType;
  public readonly invocationKinds: ContractInvocationKind[];
  public readonly namedTypes: NamedTypeSpec[];
  public readonly binderInvocations: BinderInvocation[];
  public readonly contractInvocations: ContractInvocation[];
  public readonly stateType: TypeSpec;

  constructor(
    binderType: BinderType,
    functionKinds: ContractInvocationKind[],
    namedTypes: NamedTypeSpec[],
    binderInvocations: BinderInvocation[],
    contractInvocations: ContractInvocation[],
    stateType: TypeSpec
  ) {
    this.binderType = binderType;
    this.invocationKinds = functionKinds;
    this.namedTypes = namedTypes;
    this.binderInvocations = binderInvocations;
    this.contractInvocations = contractInvocations;
    this.stateType = stateType;
  }

  public init(): ContractInvocation | undefined {
    const initBinderInvocation = this.initBinderInvocation();
    const initKindId = initBinderInvocation!.contractCallKindId;
    return this.contractInvocations.find((v) => v.kind.kindId === initKindId);
  }

  public initBinderInvocation(): BinderInvocation | undefined {
    return this.binderInvocations.find((v) => v.kind === TransactionKind.Init);
  }

  public getFunctionByName(name: string): ContractInvocation | undefined {
    return this.contractInvocations.find((v) => v.name === name);
  }

  public getFunction(
    shortname: Buffer,
    kind: ContractInvocationKind
  ): ContractInvocation | undefined {
    return this.contractInvocations.find(
      (h) => h.shortname.equals(shortname) && h.kind.kindId === kind.kindId
    );
  }

  public getBinderInvocation(
    binderShortname: number | null,
    kind: TransactionKind
  ): BinderInvocation | undefined {
    return this.binderInvocations.find((h) => h.shortname === binderShortname && h.kind === kind);
  }

  public getBinderInvocationByName(name: string): BinderInvocation | undefined {
    return this.binderInvocations.find((h) => h.name === name);
  }

  public getStateStruct(): StructTypeSpec {
    return this.namedTypes[(this.stateType as NamedTypeRef).index] as StructTypeSpec;
  }

  public isZk(): boolean {
    return this.binderType === BinderType.Zk;
  }

  public getNamedType(struct: NamedTypeRef): NamedTypeSpec;
  public getNamedType(name: string): NamedTypeSpec | undefined;
  public getNamedType(struct: NamedTypeRef | string): NamedTypeSpec | undefined {
    if (typeof struct === "string") {
      return this.namedTypes.find((namedType) => namedType.name === struct);
    } else {
      return this.namedTypes[struct.index];
    }
  }

  public getContractInvocations(binderInvocationNames: string | string[]): ContractInvocation[] {
    let invocationNames;
    if (typeof binderInvocationNames === "string") {
      invocationNames = [binderInvocationNames];
    } else {
      invocationNames = binderInvocationNames;
    }

    const contractKinds = this.binderInvocations
      .filter((p) => invocationNames.includes(p.name))
      .map((invocation) => invocation.contractCallKindId);
    return this.contractInvocations.filter((p) => contractKinds.includes(p.kind.kindId));
  }
}
