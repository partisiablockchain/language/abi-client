/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Configuration } from "../parser/Configuration";
import { AbiVersion } from "./AbiVersion";
import { ChainComponent } from "./Abi";
import { ContractAbi } from "./ContractAbi";

export class FileAbi {
  public readonly header: string;
  public readonly description: string[];
  public readonly versionBinder: AbiVersion;
  public readonly versionClient: AbiVersion;
  public readonly shortnameLength: number | null;
  public readonly chainComponent: ChainComponent;

  constructor(
    header: string,
    versionBinder: AbiVersion,
    versionClient: AbiVersion,
    shortnameLength: number | null,
    chainComponent: ChainComponent,
    description?: string[]
  ) {
    this.header = header;
    this.description = description ?? [];
    this.versionBinder = versionBinder;
    this.versionClient = versionClient;
    this.shortnameLength = shortnameLength;
    this.chainComponent = chainComponent;
  }

  public format(): Configuration {
    return Configuration.fromClientVersion(this.versionClient, this.shortnameLength);
  }

  public contract(): ContractAbi {
    return this.chainComponent as ContractAbi;
  }
}
