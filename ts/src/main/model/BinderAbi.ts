/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BinderInvocation,
  BinderType,
  ChainComponent,
  ContractInvocationKind,
  NamedTypeRef,
  NamedTypeSpec,
  TypeIndex,
  TypeSpec,
} from "./Abi";
import { StructTypeSpec } from "./StructTypeSpec";

export class BinderAbi implements ChainComponent {
  public readonly binderType: BinderType;
  public readonly invocationKinds: ContractInvocationKind[];
  public readonly namedTypes: NamedTypeSpec[];
  public readonly binderInvocations: BinderInvocation[];
  public readonly stateType: TypeSpec;

  constructor(
    binderType: BinderType,
    invocationKinds: ContractInvocationKind[],
    namedTypes: NamedTypeSpec[],
    binderInvocations: BinderInvocation[],
    stateType: TypeSpec
  ) {
    this.binderType = binderType;
    this.invocationKinds = invocationKinds;
    this.namedTypes = namedTypes;
    this.binderInvocations = binderInvocations;
    this.stateType = stateType;
  }

  public getNamedType(struct: NamedTypeRef): NamedTypeSpec;
  public getNamedType(name: string): NamedTypeSpec | undefined;
  public getNamedType(struct: NamedTypeRef | string): NamedTypeSpec | undefined {
    if (typeof struct === "string") {
      return this.namedTypes.find((namedType) => namedType.name === struct);
    } else {
      return this.namedTypes[struct.index];
    }
  }

  /**
   * Returns the state struct or null if the state is not a struct.
   *
   * @return the state struct
   */
  public getStateStruct(): StructTypeSpec | undefined {
    if (this.stateType.typeIndex == TypeIndex.Named) {
      return this.namedTypes[this.stateType.index] as StructTypeSpec;
    }
  }
}
