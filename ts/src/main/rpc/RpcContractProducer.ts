/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ContractAbi } from "../model/ContractAbi";
import { ContractInvocationKind, TypeSpec } from "../model/Abi";
import { ContractInvocation } from "../model/ContractInvocation";
import { Producer } from "../model/Producer";
import { AggregateProducer } from "../model/AggregateProducer";
import { AbiOutput } from "../abistreams/AbiOutput";

export class RpcContractProducer implements AggregateProducer {
  private readonly elements: Producer[] = [];
  private readonly shortname: Buffer;
  readonly action: ContractInvocation | null;

  constructor(shortname: Buffer, action: ContractInvocation | null) {
    this.shortname = shortname;
    this.action = action;
  }

  public static create(
    contractAbi: ContractAbi,
    nameOrShortname?: string | Buffer,
    functionKind?: ContractInvocationKind
  ): RpcContractProducer {
    if (typeof nameOrShortname === "string") {
      const action = contractAbi.getFunctionByName(nameOrShortname);
      if (action == undefined) {
        throw new Error(`Contract does not contain contract action with name '${nameOrShortname}'`);
      }
      return new RpcContractProducer(action.shortname, action);
    } else {
      const shortname = nameOrShortname as Buffer;
      const kind = functionKind as ContractInvocationKind;
      const action = contractAbi.getFunction(shortname, kind);
      if (action == undefined) {
        throw new Error(
          `Contract does not contain contract action with kind '${kind.name.toLowerCase()}' and shortname '0x${shortname.toString(
            "hex"
          )}'"`
        );
      }
      return new RpcContractProducer(action.shortname, action);
    }
  }

  public getTypeSpecForElement(): TypeSpec | null {
    if (this.action == null || this.action.arguments.length === this.elements.length) {
      return null;
    }
    const nextIndex = this.elements.length;
    return this.action.arguments[nextIndex].type;
  }

  public addElement(argument: Producer) {
    if (this.action !== null && this.action.arguments.length === this.elements.length) {
      throw new Error("Cannot add more arguments than the action expects.");
    }
    this.elements.push(argument);
  }

  public write(out: AbiOutput): void {
    if (this.action == null) {
      out.writeBytes(this.shortname);
    } else if (this.action.kind.hasShortname) {
      out.writeBytes(this.action.shortname);
    }
    for (const element of this.elements) {
      element.write(out);
    }
    this.validate();
  }

  private validate() {
    if (this.action !== null && this.elements.length !== this.action.arguments.length) {
      const missingArgument = this.action.arguments[this.elements.length];
      throw new Error(`Missing argument '${missingArgument.name}'`);
    }
  }

  getFieldName(): string {
    if (this.action !== null && this.action.arguments.length !== this.elements.length) {
      const nextIndex = this.elements.length;
      const nextArgument = this.action.arguments[nextIndex];
      return this.action.name + "/" + nextArgument.name;
    }
    return "";
  }
}
