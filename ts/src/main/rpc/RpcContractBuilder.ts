/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractBuilder } from "../builder/AbstractBuilder";
import { ContractInvocationKind } from "../model/Abi";
import { RpcContractProducer } from "./RpcContractProducer";
import { AbiByteOutput } from "../abistreams/AbiByteOutput";
import { ContractAbi } from "../model/ContractAbi";

export class RpcContractBuilder extends AbstractBuilder {
  public constructor(contractAbi: ContractAbi, name: string);
  public constructor(contractAbi: ContractAbi, shortname: Buffer, kind: ContractInvocationKind);
  public constructor(shortname: Buffer);
  public constructor(
    abiOrShortname: ContractAbi | Buffer,
    nameOrShortname?: string | Buffer,
    kind?: ContractInvocationKind
  ) {
    if (Buffer.isBuffer(abiOrShortname)) {
      super(null, "", new RpcContractProducer(abiOrShortname, null));
    } else {
      super(
        abiOrShortname.namedTypes,
        "",
        RpcContractProducer.create(abiOrShortname, nameOrShortname, kind)
      );
    }
  }

  public getBytes(): Buffer {
    return AbiByteOutput.serializeBigEndian((out) => this.getAggregateProducer().write(out));
  }
}
