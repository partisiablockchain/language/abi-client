/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { RpcValue } from "./RpcValue";
import { FileAbi } from "../model/FileAbi";
import {
  CONTEXT_FREE,
  GLOBAL_STATE,
  LOCAL_STATE_ACCOUNT,
  LOCAL_STATE_ACCOUNT_CONTRACT,
} from "../AccountPluginInvocationDeserialization";
import { AbiParser } from "../parser/AbiParser";
import { RpcReader } from "./RpcReader";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { TransactionKind } from "../model/Abi";

export class AccountPluginInvocationReader {
  private static readonly LOCAL_STATE_ACCOUNT_ABI: FileAbi = new AbiParser(
    LOCAL_STATE_ACCOUNT
  ).parseAbi();
  private static readonly LOCAL_STATE_ACCOUNT_CONTRACT_ABI: FileAbi = new AbiParser(
    LOCAL_STATE_ACCOUNT_CONTRACT
  ).parseAbi();
  private static readonly GLOBAL_STATE_ABI: FileAbi = new AbiParser(GLOBAL_STATE).parseAbi();
  private static readonly CONTEXT_FREE_ABI: FileAbi = new AbiParser(CONTEXT_FREE).parseAbi();

  public constructor() {}

  public deserializeContextFreeInvocation(input: Buffer): RpcValue {
    return RpcReader.create(
      input,
      AccountPluginInvocationReader.CONTEXT_FREE_ABI,
      TransactionKind.Action
    ).readRpc();
  }

  public deserializeLocalStateUpdate(address: BlockchainAddress | Buffer, input: Buffer): RpcValue {
    if (address.toString("hex").startsWith("00")) {
      return RpcReader.create(
        input,
        AccountPluginInvocationReader.LOCAL_STATE_ACCOUNT_ABI,
        TransactionKind.Action
      ).readRpc();
    } else {
      return RpcReader.create(
        input,
        AccountPluginInvocationReader.LOCAL_STATE_ACCOUNT_CONTRACT_ABI,
        TransactionKind.Action
      ).readRpc();
    }
  }

  public deserializeGlobalStateUpdate(input: Buffer): RpcValue {
    return RpcReader.create(
      input,
      AccountPluginInvocationReader.GLOBAL_STATE_ABI,
      TransactionKind.Action
    ).readRpc();
  }
}
