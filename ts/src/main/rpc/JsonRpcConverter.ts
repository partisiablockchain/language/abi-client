/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { RpcValue } from "./RpcValue";
import { ValueJSON } from "../value/ValueJson";
import { JsonValueConverter } from "../value/JsonValueConverter";

export interface RpcValueJson {
  binderInvocationName: string;
  binderArguments: Record<string, ValueJSON>;
  actionName?: string;
  contractArguments?: Record<string, ValueJSON>;
}

export const JsonRpcConverter = {
  toJson: (rpcValueFn: RpcValue): RpcValueJson => {
    const binderArgs: Record<string, ValueJSON> = {};
    rpcValueFn.binderInvocation.arguments.forEach((argAbi, index) => {
      binderArgs[argAbi.name] = JsonValueConverter.toJson(rpcValueFn.binderArguments[index]);
    });
    const fn = rpcValueFn.contractInvocation;
    let actionName = undefined;
    let contractArguments: Record<string, ValueJSON> | undefined = undefined;
    if (fn !== null) {
      actionName = fn.name;
      contractArguments = {};
      fn.arguments.forEach((argAbi, index) => {
        contractArguments![argAbi.name] = JsonValueConverter.toJson(
          rpcValueFn.contractArguments[index]
        );
      });
    }
    return {
      binderInvocationName: rpcValueFn.binderInvocation.name,
      binderArguments: binderArgs,
      actionName,
      contractArguments,
    };
  },
  toJsonString: (rpcValueFn: RpcValue): string =>
    JSON.stringify(JsonRpcConverter.toJson(rpcValueFn), null, 2),
};
