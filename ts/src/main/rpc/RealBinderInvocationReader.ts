/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TransactionKind, TypeIndex } from "../model/Abi";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { RpcValue } from "./RpcValue";
import { ScValueNumber } from "../value/ScValueNumber";
import { ScValue } from "../value/ScValue";
import { ScValueVector } from "../value/ScValueVector";
import { ContractAbi } from "../model/ContractAbi";
import { toHexString } from "../util/NumberToHex";

export class RealBinderInvocationReader {
  public static readonly COMMIT_RESULT_VARIABLE = 0x00;
  public static readonly ON_CHAIN_OUTPUT = 0x01;
  public static readonly OPEN_MASKED_INPUT = 0x03;
  public static readonly ON_COMPUTE_COMPLETE = 0x0c;
  public static readonly ON_VARIABLE_INPUTTED = 0x0f;
  public static readonly ADD_EXTERNAL_EVENT = 0x14;

  private readonly contract: ContractAbi;
  private readonly input: BigEndianByteInput;

  constructor(bytes: Buffer, contract: ContractAbi) {
    this.input = new BigEndianByteInput(bytes);
    this.contract = contract;
  }

  public static isAbiUnfriendly(invocationShortname: number): boolean {
    return [
      this.COMMIT_RESULT_VARIABLE,
      this.ON_CHAIN_OUTPUT,
      this.OPEN_MASKED_INPUT,
      this.ON_COMPUTE_COMPLETE,
      this.ON_VARIABLE_INPUTTED,
      this.ADD_EXTERNAL_EVENT,
    ].includes(invocationShortname);
  }

  public readAbiUnfriendlyBinderInvocation() {
    const invocationShortname = this.input.readU8();
    switch (invocationShortname) {
      case RealBinderInvocationReader.ON_CHAIN_OUTPUT:
        return this.deserializeOnChainOutput();
      case RealBinderInvocationReader.OPEN_MASKED_INPUT:
        return this.deserializeOpenMaskedInput();
      case RealBinderInvocationReader.COMMIT_RESULT_VARIABLE:
        return this.deserializeCommitResultVariable();
      case RealBinderInvocationReader.ON_COMPUTE_COMPLETE:
        return this.deserializeOnComputeComplete();
      case RealBinderInvocationReader.ON_VARIABLE_INPUTTED:
        return this.deserializeOnVariableInputted();
      case RealBinderInvocationReader.ADD_EXTERNAL_EVENT:
        return this.deserializeAddExternalEvent();
      default:
        throw new Error(
          `Binder invocation '0x${toHexString(invocationShortname)}' is not an abi unfriendly invocation`
        );
    }
  }

  private deserializeCommitResultVariable(): RpcValue {
    const id = this.input.readI64();
    const list = ScValueVector.fromBytes(this.input.readRemaining());
    const func = this.contract.getBinderInvocation(
      RealBinderInvocationReader.COMMIT_RESULT_VARIABLE,
      TransactionKind.Action
    )!;

    const invocationArguments: ScValue[] = [new ScValueNumber(TypeIndex.i64, id), list];
    return new RpcValue(func, null, invocationArguments, []);
  }

  private deserializeOnChainOutput(): RpcValue {
    const id = this.input.readI32();
    const list = ScValueVector.fromBytes(this.input.readRemaining());
    const func = this.contract.getBinderInvocation(
      RealBinderInvocationReader.ON_CHAIN_OUTPUT,
      TransactionKind.Action
    )!;

    const invocationArguments: ScValue[] = [new ScValueNumber(TypeIndex.i32, id), list];
    return new RpcValue(func, null, invocationArguments, []);
  }

  private deserializeOpenMaskedInput(): RpcValue {
    const id = this.input.readI32();
    const maskedValue = ScValueVector.fromBytes(this.input.readRemaining());

    const func = this.contract.getBinderInvocation(
      RealBinderInvocationReader.OPEN_MASKED_INPUT,
      TransactionKind.Action
    )!;

    const invocationArguments: ScValue[] = [new ScValueNumber(TypeIndex.i32, id), maskedValue];
    return new RpcValue(func, null, invocationArguments, []);
  }

  private deserializeAddExternalEvent() {
    const id = this.input.readI32();
    const remaining = ScValueVector.fromBytes(this.input.readRemaining());
    const func = this.contract.getBinderInvocation(
      RealBinderInvocationReader.ADD_EXTERNAL_EVENT,
      TransactionKind.Action
    )!;

    const invocationArguments: ScValue[] = [new ScValueNumber(TypeIndex.i32, id), remaining];

    return new RpcValue(func, null, invocationArguments, []);
  }

  private deserializeOnComputeComplete(): RpcValue {
    const idLength = this.input.readI32();
    const ids = [];
    for (let i = 0; i < idLength; i++) {
      ids.push(new ScValueNumber(TypeIndex.i32, this.input.readI32()));
    }
    const idsVector = new ScValueVector(ids);

    const action = this.contract.getBinderInvocation(
      RealBinderInvocationReader.ON_COMPUTE_COMPLETE,
      TransactionKind.Action
    )!;

    return new RpcValue(action, null, [idsVector], []);
  }

  private deserializeOnVariableInputted(): RpcValue {
    const id = new ScValueNumber(TypeIndex.i32, this.input.readI32());

    const action = this.contract.getBinderInvocation(
      RealBinderInvocationReader.ON_VARIABLE_INPUTTED,
      TransactionKind.Action
    )!;

    return new RpcValue(action, null, [id], []);
  }
}
