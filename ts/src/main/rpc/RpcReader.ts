/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiByteInput } from "../abistreams/AbiByteInput";
import { AbstractReader } from "../AbstractReader";
import { ChainComponentFormat, TransactionKind, ShortnameType } from "../model/Abi";
import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { FileAbi } from "../model/FileAbi";
import { ScValueMap } from "../value/ScValueMap";
import { ScValueSet } from "../value/ScValueSet";
import { RpcValue } from "./RpcValue";
import { RealBinderInvocationReader } from "./RealBinderInvocationReader";
import { ScValueAvlTreeMap } from "../value/ScValueAvlTreeMap";
import { ContractAbi } from "../model/ContractAbi";
import { Configuration } from "../parser/Configuration";
import { AbiParser } from "../parser/AbiParser";

export class RpcReader extends AbstractReader {
  private readonly contract: ContractAbi;
  private readonly kind: TransactionKind;
  private readonly configuration: Configuration;
  private readonly bytes: Buffer;

  private constructor(
    bytes: Buffer,
    contract: ContractAbi,
    kind: TransactionKind,
    configuration: Configuration
  ) {
    const byteInput = new BigEndianByteInput(bytes);
    super(new AbiByteInput(byteInput), contract.namedTypes);
    this.contract = contract;
    this.configuration = configuration;
    this.bytes = bytes;
    this.kind = kind;
  }

  public static create(bytes: Buffer, fileAbi: FileAbi, kind: TransactionKind) {
    return new RpcReader(bytes, fileAbi.chainComponent as ContractAbi, kind, fileAbi.format());
  }

  public readRpc(): RpcValue {
    if (this.kind === TransactionKind.Action && this.bytes.length === 0) {
      const empty = this.contract.getBinderInvocation(null, TransactionKind.EmptyAction);
      if (empty !== undefined) {
        return new RpcValue(empty, null, [], []);
      }
    }

    if (
      this.configuration.chainComponentFormat === ChainComponentFormat.OnlyContract &&
      this.contract.isZk()
    ) {
      const invocationShortname = this.bytes[0];
      if (RealBinderInvocationReader.isAbiUnfriendly(invocationShortname)) {
        return new RealBinderInvocationReader(
          this.bytes,
          this.contract
        ).readAbiUnfriendlyBinderInvocation();
      }
    }

    let binderShortname = null;
    let binderInvocation = this.contract.getBinderInvocation(null, this.kind);
    if (binderInvocation === undefined) {
      binderShortname = this.input.readU8();
      binderInvocation = this.contract.getBinderInvocation(binderShortname, this.kind);
    }

    if (binderInvocation === undefined) {
      throw new Error(
        `No binder invocation with kind '${
          TransactionKind[this.kind]
        }' and shortname '${binderShortname}'`
      );
    }

    const binderArguments = binderInvocation.arguments.map((arg) => this.readGeneric(arg.type));

    if (binderInvocation.contractCallKindId === null) {
      return new RpcValue(binderInvocation, null, binderArguments, []);
    }

    const contractKind = AbiParser.getKind(
      this.contract.invocationKinds,
      binderInvocation.contractCallKindId
    )!;

    let func;
    if (contractKind.hasShortname) {
      const contractShortname = this.readShortname();
      func = this.contract.getFunction(contractShortname, contractKind);
      if (func === undefined) {
        throw new Error(
          `No contract invocation with kind '${contractKind.name.toLowerCase()}' and shortname '0x${contractShortname.toString(
            "hex"
          )}'`
        );
      }
    } else {
      func = this.contract.contractInvocations.find((f) => f.kind.kindId === contractKind.kindId);
      if (func === undefined) {
        throw new Error(`No contract invocation with kind '${contractKind.name.toLowerCase()}'`);
      }
    }

    const contractArguments = func.arguments.map((arg) => this.readGeneric(arg.type));

    return new RpcValue(binderInvocation, func, binderArguments, contractArguments);
  }

  private parseLeb128(): Buffer {
    const bufferWriter = new BigEndianByteOutput();
    for (let i = 0; i < 5; i++) {
      const currentByte = this.input.readU8();
      bufferWriter.writeU8(currentByte);
      if (currentByte < 128) {
        return bufferWriter.toBuffer();
      }
    }
    throw new Error(
      "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5 bytes)"
    );
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in rpc");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in rpc");
  }

  protected readAvlTreeMap(): ScValueAvlTreeMap {
    throw new Error("Type AvlTreeMap is not supported in rpc");
  }

  private readShortname(): Buffer {
    let shortname;
    if (this.configuration.shortnameType === ShortnameType.leb128) {
      shortname = this.parseLeb128();
    } else {
      shortname = this.input.readBytes(this.configuration.shortnameLength as number);
    }
    return shortname;
  }
}
