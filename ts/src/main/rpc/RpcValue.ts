/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValue } from "../value/ScValue";
import { ContractInvocation } from "../model/ContractInvocation";
import { BinderInvocation } from "../model/Abi";

export class RpcValue {
  public readonly binderInvocation: BinderInvocation;
  public readonly contractInvocation: ContractInvocation | null;
  public readonly binderArguments: ScValue[];
  public readonly contractArguments: ScValue[];

  constructor(
    binderInvocation: BinderInvocation,
    fn: ContractInvocation | null,
    binderArguments: ScValue[],
    contractArguments: ScValue[]
  ) {
    this.binderInvocation = binderInvocation;
    this.contractInvocation = fn;
    this.binderArguments = binderArguments;
    this.contractArguments = contractArguments;
  }

  public arguments(): ScValue[] {
    return this.binderArguments.concat(this.contractArguments);
  }

  public size(): number {
    return this.arguments().length;
  }

  public get(index: number): ScValue {
    return this.arguments()[index];
  }
}
