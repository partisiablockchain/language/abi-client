/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractBuilder } from "../builder/AbstractBuilder";
import { TransactionKind } from "../model/Abi";
import { RpcProducer } from "./RpcProducer";
import { ContractAbi } from "../model/ContractAbi";
import { RpcContractProducer } from "./RpcContractProducer";
import { AbiParser } from "../parser/AbiParser";
import { AbiByteOutput } from "../abistreams/AbiByteOutput";

export class RpcBuilder extends AbstractBuilder {
  private readonly contractAbi: ContractAbi;
  public constructor(contractAbi: ContractAbi, actionName: string);
  public constructor(
    contractAbi: ContractAbi,
    binderShortname: number | null,
    kind: TransactionKind
  );
  public constructor(
    contractAbi: ContractAbi,
    nameOrShortname: string | number | null,
    nameOrKind?: TransactionKind
  ) {
    super(contractAbi.namedTypes, "", RpcProducer.create(contractAbi, nameOrShortname, nameOrKind));
    this.contractAbi = contractAbi;
  }

  public getBytes(): Buffer {
    return AbiByteOutput.serializeBigEndian((out) => this.getAggregateProducer().write(out));
  }

  public contractCall(name: string | Buffer): AbstractBuilder {
    const producer = this.getAggregateProducer() as RpcProducer;
    const binderInvocation = producer.binderInvocation;
    if (binderInvocation.contractCallKindId === null) {
      throw new Error(`Binder invocation '${binderInvocation.name}' does not call contract`);
    }

    if (typeof name === "string") {
      const rpcActionProducer = RpcContractProducer.create(this.contractAbi, name);
      const func = rpcActionProducer.action!;
      if (binderInvocation.contractCallKindId !== func.kind.kindId) {
        throw new Error(
          `Binder invocation '${binderInvocation.name}' attempts to call kind '${binderInvocation.contractCallKindId}', but action '${func.name}' has kind '${func.kind.kindId}'`
        );
      }
      producer.addContractCall(rpcActionProducer);
      return new (class extends AbstractBuilder {})(
        this.contractAbi.namedTypes,
        "contractCall/",
        rpcActionProducer
      );
    } else {
      const expectedFunctionKind = AbiParser.getKind(
        this.contractAbi.invocationKinds,
        binderInvocation.contractCallKindId
      )!;
      const rpcActionProducer = RpcContractProducer.create(
        this.contractAbi,
        name,
        expectedFunctionKind
      );
      producer.addContractCall(rpcActionProducer);
      return new (class extends AbstractBuilder {})(
        this.contractAbi.namedTypes,
        "contractCall/",
        rpcActionProducer
      );
    }
  }
}
