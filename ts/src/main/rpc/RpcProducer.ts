/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AggregateProducer } from "../model/AggregateProducer";
import { Producer } from "../model/Producer";
import { BinderInvocation, TransactionKind, TypeSpec } from "../model/Abi";
import { RpcContractProducer } from "./RpcContractProducer";
import { ContractAbi } from "../model/ContractAbi";
import { AbiOutput } from "../abistreams/AbiOutput";

export class RpcProducer implements AggregateProducer {
  private readonly elements: Producer[] = [];
  readonly binderInvocation: BinderInvocation;
  private contractCall: RpcContractProducer | undefined;

  constructor(binderInvocation: BinderInvocation) {
    this.binderInvocation = binderInvocation;
  }

  static create(
    contractAbi: ContractAbi,
    nameOrShortname: string | number | null,
    kind?: TransactionKind
  ): RpcProducer {
    if (typeof nameOrShortname === "string") {
      const func = contractAbi.getBinderInvocationByName(nameOrShortname);
      if (func === undefined) {
        throw new Error(`Contract does not contain function with name '${nameOrShortname}'`);
      }
      return new RpcProducer(func);
    } else {
      const func = contractAbi.getBinderInvocation(nameOrShortname, kind!);
      if (func === undefined) {
        const binderErrorText =
          nameOrShortname != null
            ? `binder shortname '0x${nameOrShortname.toString(16).padStart(2, "0")}'`
            : "no binder shortname";
        throw new Error(
          `Contract does not contain function with ${binderErrorText} and kind '${TransactionKind[kind!]}'`
        );
      }
      return new RpcProducer(func);
    }
  }

  addElement(argument: Producer): void {
    if (this.elements.length < this.binderInvocation.arguments.length) {
      this.elements.push(argument);
    } else {
      throw new Error("Cannot add more arguments than the invocation expects");
    }
  }

  getFieldName(): string {
    if (this.elements.length < this.binderInvocation.arguments.length) {
      const nextIndex = this.elements.length;
      const nextArgument = this.binderInvocation.arguments[nextIndex];
      return `${this.binderInvocation.name}/${nextArgument.name}`;
    } else {
      // Stryker disable next-line StringLiteral: This branch will be caught as error in addElement
      return "";
    }
  }

  getTypeSpecForElement(): TypeSpec | null {
    if (this.elements.length < this.binderInvocation.arguments.length) {
      const nextIndex = this.elements.length;
      const nextArgument = this.binderInvocation.arguments[nextIndex];
      return nextArgument.type;
    } else {
      return null;
    }
  }

  write(out: AbiOutput): void {
    if (this.binderInvocation.shortname !== null) {
      out.writeU8(this.binderInvocation.shortname);
    }
    for (const element of this.elements) {
      element.write(out);
    }
    this.validate();
    if (this.contractCall != undefined) {
      this.contractCall.write(out);
    }
  }

  private validate() {
    if (this.elements.length < this.binderInvocation.arguments.length) {
      const missingArgument = this.binderInvocation.arguments[this.elements.length];
      throw new Error(`Missing argument '${missingArgument.name}'`);
    }
    if (this.binderInvocation.contractCallKindId != null && this.contractCall == null) {
      throw new Error("Missing contract call");
    }
  }

  public addContractCall(contractCall: RpcContractProducer): void {
    this.contractCall = contractCall;
  }
}
