/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";

export { BN };
export { hashBuffer } from "./util/BufferUtil";
export {
  TypeSpec,
  VecTypeSpec,
  TypeIndex,
  EnumVariant,
  OptionTypeSpec,
  SizedArrayTypeSpec,
  SimpleTypeIndex,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  MapTypeSpec,
  NumericTypeIndex,
  SetTypeSpec,
  NumericTypeSpec,
  SimpleTypeSpec,
  FieldAbi,
  CompositeTypeSpec,
  ConfigOption,
  ShortnameType,
  ArgumentAbi,
  FunctionFormat,
  GenericTypeAbi,
  U8TypeSpec,
  U16TypeSpec,
  U32TypeSpec,
  U64TypeSpec,
  U128TypeSpec,
  I8TypeSpec,
  I16TypeSpec,
  I32TypeSpec,
  I64TypeSpec,
  I128TypeSpec,
  StringTypeSpec,
  AddressTypeSpec,
  BoolTypeSpec,
  Header,
  AvlTreeMapTypeSpec,
  BinderInvocation,
  TransactionKind,
  ContractInvocationKind,
  BinderType,
} from "./model/Abi";
export { ContractInvocation } from "./model/ContractInvocation";
export { FileAbi } from "./model/FileAbi";
export { AbiVersion } from "./model/AbiVersion";
export { EnumTypeSpec } from "./model/EnumTypeSpec";
export { StructTypeSpec } from "./model/StructTypeSpec";
export { AbiParser } from "./parser/AbiParser";
export { Configuration } from "./parser/Configuration";
export { ContractAbi } from "./model/ContractAbi";
export { HashMap } from "./HashMap";
export { ScValue } from "./value/ScValue";
export { StateReader, StateBytes } from "./state/StateReader";
export { ScValueStruct } from "./value/ScValueStruct";
export { ScValueMap } from "./value/ScValueMap";
export { ScValueEnum } from "./value/ScValueEnum";
export { ScValueSet } from "./value/ScValueSet";
export { ScValueVector } from "./value/ScValueVector";
export { ScValueBool } from "./value/ScValueBool";
export { ScValueNumber } from "./value/ScValueNumber";
export { ScValueOption } from "./value/ScValueOption";
export { ScValueString } from "./value/ScValueString";
export { ScValueAddress } from "./value/ScValueAddress";
export { ScValueHash } from "./value/ScValueHash";
export { ScValuePublicKey } from "./value/ScValuePublicKey";
export { ScValueSignature } from "./value/ScValueSignature";
export { ScValueBlsPublicKey } from "./value/ScValueBlsPublicKey";
export { ScValueBlsSignature } from "./value/ScValueBlsSignature";
export { ScValueAvlTreeMap } from "./value/ScValueAvlTreeMap";
export { ScValueArray } from "./value/ScValueArray";
export { RpcReader } from "./rpc/RpcReader";
export { RpcValue } from "./rpc/RpcValue";
export { RustSyntaxStatePrinter } from "./state/RustSyntaxStatePrinter";
export { RustSyntaxPrettyPrinter } from "./parser/RustSyntaxPrettyPrinter";
export { TransactionReader } from "./transaction/TransactionReader";
export { JsonValueConverter } from "./value/JsonValueConverter";
export { RpcValueJson, JsonRpcConverter } from "./rpc/JsonRpcConverter";
export { AbstractBuilder } from "./builder/AbstractBuilder";
export { BigEndianReader } from "./BigEndianReader";
export { ExecutedTransaction } from "../main/transaction/ExecutedTransaction";
export { ZkInputBuilder } from "./zk/ZkInputBuilder";
export { ZkSecretReader } from "./zk/ZkSecretReader";
export { RpcContractBuilder } from "./rpc/RpcContractBuilder";
export { StateBuilder } from "./state/StateBuilder";
export { TransactionBuilder } from "./transaction/TransactionBuilder";

export { ImplicitBinderAbi } from "./parser/ImplicitBinderAbi";
export { BinderAbi } from "./model/BinderAbi";
export { RpcBuilder } from "./rpc/RpcBuilder";

// Codegen-lib
export { Hash } from "./codegen-lib/Hash";
export { BlockchainAddress } from "./codegen-lib/BlockchainAddress";
export { BlockchainPublicKey } from "./codegen-lib/BlockchainPublicKey";
export { Signature } from "./codegen-lib/Signature";
export { BlsSignature } from "./codegen-lib/BlsSignature";
export { BlsPublicKey } from "./codegen-lib/BlsPublicKey";
export { AvlTreeMap } from "./codegen-lib/AvlTreeMap";
export { StateWithClient, Entry, SecretInput } from "./codegen-lib/types";
export { SecretInputBuilder } from "./codegen-lib/SecretInputBuilder";

export {
  BlockchainStateClient,
  BlockchainStateClientImpl,
} from "./codegen-lib/BlockchainStateClient";

// Abi streams
export { AbiInput } from "./abistreams/AbiInput";
export { AbiOutput } from "./abistreams/AbiOutput";
export { AbiBitInput } from "./abistreams/AbiBitInput";
export { AbiBitOutput } from "./abistreams/AbiBitOutput";
export { AbiByteInput } from "./abistreams/AbiByteInput";
export { AbiByteOutput } from "./abistreams/AbiByteOutput";

// Deployment client
export { BlockchainClientForDeployment, DeployResult } from "./deploymentclient/DeploymentTypes";
export { DeploymentBuilder } from "./deploymentclient/DeploymentBuilder";
export { DeploymentClient } from "./deploymentclient/DeploymentClient";
export { BlockchainClientForDeploymentImpl } from "./deploymentclient/BlockchainClientForDeploymentImpl";
