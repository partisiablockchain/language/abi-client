/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ArgumentAbi,
  BinderInvocation,
  BinderType,
  ChainComponent,
  ChainComponentFormat,
  EnumVariant,
  FieldAbi,
  FunctionFormat,
  ContractInvocationKind,
  Header,
  TransactionKind,
  NamedTypeIndex,
  NamedTypesFormat,
  NamedTypeSpec,
  ShortnameType,
  TypeAndFunctionOrder,
  TypeIndex,
  TypeSpec,
} from "../model/Abi";
import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { AbiVersion } from "../model/AbiVersion";
import { DocumentationNamedType } from "../model/DocumentationNamedType";
import { EnumTypeSpec } from "../model/EnumTypeSpec";
import { FileAbi } from "../model/FileAbi";
import { ContractInvocation } from "../model/ContractInvocation";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { hashBuffer } from "../util/BufferUtil";
import { toHexString } from "../util/NumberToHex";
import { Configuration } from "./Configuration";
import { ContractAbi } from "../model/ContractAbi";
import { RustSyntaxPrettyPrinter } from "./RustSyntaxPrettyPrinter";
import { BinderAbi } from "../model/BinderAbi";
import { BinderRustSyntaxPrettyPrinter } from "./BinderRustSyntaxPrettyPrinter";
import { ImplicitBinderAbi } from "./ImplicitBinderAbi";

export class AbiParser {
  private readonly bufferReader: BigEndianByteInput;

  constructor(buffer: Buffer) {
    this.bufferReader = new BigEndianByteInput(buffer);
  }

  public parseHeader(): Header {
    const headerBuffer = this.bufferReader.readBytes(6);
    const header = headerBuffer.toString("ascii");

    const versionBinder = new AbiVersion(
      this.bufferReader.readU8(),
      this.bufferReader.readU8(),
      this.bufferReader.readU8()
    );

    const versionClient = new AbiVersion(
      this.bufferReader.readU8(),
      this.bufferReader.readU8(),
      this.bufferReader.readU8()
    );

    return { header, versionBinder, versionClient };
  }

  public parseChainComponent(header: Header): FileAbi {
    const shortnameLength = this.readShortnameLength(header);
    const configuration = Configuration.fromClientVersion(header.versionClient, shortnameLength);
    let chainComponent: ChainComponent;
    if (configuration.chainComponentFormat === ChainComponentFormat.ChainComponents) {
      const componentIdentifier = this.bufferReader.readU8();
      if (componentIdentifier === 0x10) {
        chainComponent = this.parseContractAbiInternal(configuration);
      } else if (componentIdentifier === 0x20) {
        chainComponent = this.parseBinderAbiInternal(configuration);
      } else {
        throw new Error(
          `Unknown chain component identifier: 0x${componentIdentifier.toString(16)}`
        );
      }
    } else {
      chainComponent = this.parseContractAbiInternal(configuration);
    }
    return new FileAbi(
      header.header,
      header.versionBinder,
      header.versionClient,
      shortnameLength,
      chainComponent
    );
  }

  private parseBinderAbiInternal(configuration: Configuration): BinderAbi {
    const binderType: BinderType = this.bufferReader.readU8();
    const functionKinds = this.parseFunctionKinds();
    const namedTypes = this.parseNamedTypes(configuration);

    const invocations = this.parseInvocations();
    this.validateInvocations(invocations);

    const stateType = this.parseTypeSpec();

    // Validate functions are ordered by KindId and then shortname
    this.validateBinderInvocationsSorted(invocations);
    // Validate the types are ordered depth-first when traversing the type tree from each root
    // (state type and function argument types).
    const typeRoots = this.findTypeRoots(stateType, invocations, []);
    this.validateTypesSortedDepthFirst(namedTypes, typeRoots);

    return new BinderAbi(binderType, functionKinds, namedTypes, invocations, stateType);
  }

  private validateInvocations(invocations: BinderInvocation[]) {
    const grouped: Map<TransactionKind, BinderInvocation[]> = new Map<
      TransactionKind,
      BinderInvocation[]
    >();
    for (const invocation of invocations) {
      const list = grouped.get(invocation.kind);
      if (list === undefined) {
        grouped.set(invocation.kind, [invocation]);
      } else {
        list.push(invocation);
      }
    }
    for (const [kind, invoList] of grouped) {
      if (invoList.some((invo) => invo.shortname == null) && invoList.length != 1) {
        throw new Error(
          `Binder cannot contain more than one invocation of kind '${TransactionKind[kind]}', when one exists without a shortname`
        );
      }
    }
  }

  private parseInvocations(): BinderInvocation[] {
    const invocations: BinderInvocation[] = [];
    const size = this.bufferReader.readI32();

    for (let i = 0; i < size; i++) {
      const binderInvocation = this.parseBinderInvocation();

      invocations.push(binderInvocation);
    }
    return invocations;
  }

  private parseBinderInvocation(): BinderInvocation {
    const kind: TransactionKind = this.bufferReader.readU8();
    if (!(kind in TransactionKind)) {
      throw new Error(`"Unsupported TransactionKind type 0x${kind.toString(16)} specified"`);
    }

    const name = this.bufferReader.readString();

    let shortname = null;
    if (this.bufferReader.readBoolean()) {
      shortname = this.bufferReader.readU8();
    }

    const argumentAbis = this.parseArguments();
    let contractCallId = null;
    if (this.bufferReader.readBoolean()) {
      contractCallId = this.bufferReader.readU8();
    }
    return { kind, name, shortname, arguments: argumentAbis, contractCallKindId: contractCallId };
  }

  private parseFunctionKinds(): ContractInvocationKind[] {
    const functionKinds: ContractInvocationKind[] = [];
    const size = this.bufferReader.readI32();
    for (let i = 0; i < size; i++) {
      const kindId = this.bufferReader.readU8();
      const name = this.bufferReader.readString();
      const hasShortname = this.bufferReader.readBoolean();
      functionKinds.push({ kindId, name, hasShortname });
    }
    return functionKinds;
  }

  private readShortnameLength(header: Header): number | null {
    if (header.header !== "PBCABI") {
      throw new Error(`Malformed header bytes, expecting PBCABI but was ${header.header}`);
    }

    const options = Configuration.getOptionsOrThrow(header.versionClient);

    let shortnameLength;
    if (options.shortnameType === ShortnameType.hash) {
      shortnameLength = this.bufferReader.readU8();
    } else {
      shortnameLength = null;
    }

    return shortnameLength;
  }

  public parseAbi(): FileAbi {
    const header = this.parseHeader();
    return this.parseChainComponent(header);
  }

  public parseContractAbi(): ContractAbi {
    const chainComponent = this.parseAbi().chainComponent;
    if (chainComponent instanceof ContractAbi) {
      return chainComponent;
    } else {
      throw new Error("Failed to parse abi as a contract abi");
    }
  }

  private parseContractAbiInternal(config: Configuration): ContractAbi {
    let functionKinds: ContractInvocationKind[];
    let binderType: BinderType;
    if (config.chainComponentFormat === ChainComponentFormat.ChainComponents) {
      binderType = this.bufferReader.readU8();
      functionKinds = this.parseFunctionKinds();
    }

    const namedTypes = this.parseNamedTypes(config);

    let contractCalls;
    let binderInvocations;
    if (config.chainComponentFormat === ChainComponentFormat.ChainComponents) {
      binderInvocations = this.parseInvocations();
      contractCalls = this.parseFunctions(config, functionKinds!);
    } else {
      contractCalls = this.parseFunctions(config, ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS);
      const isZk = contractCalls.some((h) => ImplicitBinderAbi.isZkKind(h.kind.kindId));
      if (isZk) {
        binderType = BinderType.Zk;
        functionKinds = ImplicitBinderAbi.DEFAULT_ZK_FUNCTION_KINDS;
        binderInvocations = ImplicitBinderAbi.defaultZkInvocations();
      } else {
        binderType = BinderType.Public;
        functionKinds = ImplicitBinderAbi.DEFAULT_PUB_FUNCTION_KINDS;
        binderInvocations = ImplicitBinderAbi.defaultPubInvocations();
      }
    }

    const stateType = this.parseTypeSpec();

    const remainingSize = this.bufferReader.readRemaining().length;
    if (remainingSize !== 0) {
      throw new Error(
        `Expected EOF after parsed ABI, but stream had ${remainingSize} bytes remaining`
      );
    }

    if (config.typeAndFunctionOrder == TypeAndFunctionOrder.DepthFirst) {
      // Validate contractInvocations are ordered by KindId and then shortname
      this.validateBinderInvocationsSorted(binderInvocations);
      this.validateFunctionsSorted(contractCalls);
      // Validate the types are ordered depth-first when traversing the type tree from each root
      // (state type and function argument types).
      const typeRoots = this.findTypeRoots(stateType, binderInvocations, contractCalls);
      this.validateTypesSortedDepthFirst(namedTypes, typeRoots);
    } else {
      // Sort actions
      contractCalls.sort((a, b) => a.kind.kindId - b.kind.kindId);
    }

    return new ContractAbi(
      binderType!,
      functionKinds!,
      namedTypes,
      binderInvocations,
      contractCalls,
      stateType
    );
  }

  private validateBinderInvocationsSorted(binderInvocations: BinderInvocation[]): void {
    for (let i = 0; i < binderInvocations.length - 1; i++) {
      if (!this.invocationOrderStrictlyAscending(binderInvocations[i], binderInvocations[i + 1])) {
        const previous = this.formatBinderInvocationForError(binderInvocations[i]);
        const next = this.formatBinderInvocationForError(binderInvocations[i + 1]);
        const errorMessage = `BinderInvocations are unordered: Must be ordered by invocation kind and by shortname in ascending order but ${previous} appears before ${next}`;
        throw new Error(errorMessage);
      }
    }
  }

  private validateFunctionsSorted(functions: ContractInvocation[]): void {
    for (let i = 0; i < functions.length - 1; i++) {
      if (!this.functionOrderStrictlyAscending(functions[i], functions[i + 1])) {
        const previous = this.formatFnAbiForError(functions[i]);
        const next = this.formatFnAbiForError(functions[i + 1]);
        const errorMessage = `Functions are unordered: Must be ordered by function kind and by shortname in ascending order but ${previous} appears before ${next}`;
        throw new Error(errorMessage);
      }
    }
  }

  private formatFnAbiForError(fnAbi: ContractInvocation): string {
    const kindName = fnAbi.kind.name;
    const kindId = fnAbi.kind.kindId;
    const shortname = fnAbi.shortname.toString("hex");
    return `${kindName}(${kindId}) with shortname ${shortname}`;
  }

  private formatBinderInvocationForError(binderInvocation: BinderInvocation): string {
    const kindName = TransactionKind[binderInvocation.kind];
    const kindId = binderInvocation.kind;
    const shortname = binderInvocation.shortname;
    return `${kindName}(${kindId}) with shortname ${shortname}`;
  }

  private functionOrderStrictlyAscending(
    previous: ContractInvocation,
    next: ContractInvocation
  ): boolean {
    if (previous.kind.kindId < next.kind.kindId) {
      return true;
    } else if (previous.kind.kindId == next.kind.kindId) {
      const previousShortname = previous.shortname.toString("hex");
      const nextShortname = next.shortname.toString("hex");
      return previousShortname.localeCompare(nextShortname) < 0;
    } else {
      return false;
    }
  }

  private invocationOrderStrictlyAscending(
    previous: BinderInvocation,
    next: BinderInvocation
  ): boolean {
    if (previous.kind < next.kind) {
      return true;
    } else if (previous.kind == next.kind) {
      const previousShortname = previous.shortname as number;
      const nextShortname = next.shortname as number;
      return previousShortname < nextShortname;
    } else {
      return false;
    }
  }

  private findTypeRoots(
    stateType: TypeSpec,
    binderInvocations: BinderInvocation[],
    functions: ContractInvocation[]
  ): TypeSpec[] {
    const typeRoots: TypeSpec[] = [];
    typeRoots.push(stateType);
    for (const binderInvocation of binderInvocations) {
      for (const argument of binderInvocation.arguments) {
        typeRoots.push(argument.type);
      }
    }
    for (const fnAbi of functions) {
      for (const argument of fnAbi.arguments) {
        typeRoots.push(argument.type);
      }
      if (fnAbi.secretArgument != undefined) {
        typeRoots.push(fnAbi.secretArgument.type);
      }
    }
    return typeRoots;
  }

  private validateTypesSortedDepthFirst(namedTypes: NamedTypeSpec[], roots: TypeSpec[]) {
    const visitedTypes: NamedTypeSpec[] = [];
    for (const root of roots) {
      this.visitTypeSpec(namedTypes, visitedTypes, root);
    }
    if (namedTypes.length != visitedTypes.length) {
      const difference = namedTypes.filter((type) => visitedTypes.indexOf(type) < 0);
      const asString = difference.map((type) => type.name).join(", ");
      throw new Error(`Named Types list contained unreachable type(s) [${asString}]`);
    }
  }

  private visitIndex(allTypes: NamedTypeSpec[], visitedTypes: NamedTypeSpec[], index: number) {
    if (allTypes.length <= index) {
      throw Error(`No known types at index ${index}`);
    }
    const namedType = allTypes[index];
    if (visitedTypes.indexOf(namedType) < 0) {
      const expectedIndex = visitedTypes.length;
      if (index != expectedIndex) {
        throw new Error(
          `Expected named type '${namedType.name}' to have index ${expectedIndex} but was ${index}`
        );
      }
      visitedTypes.push(namedType);
      this.visitNamedType(allTypes, visitedTypes, namedType);
    }
  }

  private visitNamedType(
    allTypes: NamedTypeSpec[],
    visitedTypes: NamedTypeSpec[],
    currentNamedType: NamedTypeSpec
  ) {
    if (currentNamedType instanceof StructTypeSpec) {
      for (const field of currentNamedType.fields) {
        const type = field.type;
        this.visitTypeSpec(allTypes, visitedTypes, type);
      }
    } else if (currentNamedType instanceof EnumTypeSpec) {
      for (const variant of currentNamedType.variants) {
        const type = variant.def;
        this.visitTypeSpec(allTypes, visitedTypes, type);
      }
    }
  }

  private visitTypeSpec(
    allTypes: NamedTypeSpec[],
    visitedTypes: NamedTypeSpec[],
    currentSpec: TypeSpec
  ) {
    if (currentSpec.typeIndex == TypeIndex.Named) {
      const index = currentSpec.index;
      this.visitIndex(allTypes, visitedTypes, index);
    } else if (
      currentSpec.typeIndex == TypeIndex.Vec ||
      currentSpec.typeIndex == TypeIndex.Set ||
      currentSpec.typeIndex == TypeIndex.SizedArray ||
      currentSpec.typeIndex == TypeIndex.Option
    ) {
      const type = currentSpec.valueType;
      this.visitTypeSpec(allTypes, visitedTypes, type);
    } else if (
      currentSpec.typeIndex == TypeIndex.Map ||
      currentSpec.typeIndex == TypeIndex.AvlTreeMap
    ) {
      const keyType = currentSpec.keyType;
      this.visitTypeSpec(allTypes, visitedTypes, keyType);
      const valueType = currentSpec.valueType;
      this.visitTypeSpec(allTypes, visitedTypes, valueType);
    }
  }

  private parseNamedTypes(config: Configuration): NamedTypeSpec[] {
    const namedTypesSize = this.bufferReader.readI32();
    const namedTypes: NamedTypeSpec[] = [];
    for (let i = 0; i < namedTypesSize; i++) {
      if (config.namedTypesFormat == NamedTypesFormat.StructsAndEnum) {
        const namedType = this.bufferReader.readU8();
        if (namedType === NamedTypeIndex.Struct) {
          namedTypes.push(this.parseStructTypeSpec());
        } else if (namedType === NamedTypeIndex.Enum) {
          namedTypes.push(this.parseEnumTypeSpec());
        } else {
          throw new Error(
            `Bad byte 0x${toHexString(namedType)} used for namedTypeSpec index ` +
              `should be either 0x${toHexString(NamedTypeIndex.Struct)} for a struct ` +
              `or 0x${toHexString(NamedTypeIndex.Enum)} for an enum`
          );
        }
      } else {
        namedTypes.push(this.parseStructTypeSpec());
      }
    }
    return namedTypes;
  }

  private parseStructTypeSpec(): StructTypeSpec {
    const name = this.bufferReader.readString();
    const fieldsSize = this.bufferReader.readI32();
    const fields: FieldAbi[] = [];
    for (let j = 0; j < fieldsSize; j++) {
      const fieldName = this.bufferReader.readString();
      const type = this.parseTypeSpec();
      fields.push({ name: fieldName, type });
    }
    return new StructTypeSpec(name, fields, DocumentationNamedType.empty());
  }

  private parseEnumTypeSpec(): EnumTypeSpec {
    const name = this.bufferReader.readString();
    const numVariants = this.bufferReader.readI32();
    const variants: EnumVariant[] = [];
    for (let i = 0; i < numVariants; i++) {
      const discriminant = this.bufferReader.readU8();
      const typeIndex = this.bufferReader.readU8();
      if (typeIndex !== TypeIndex.Named) {
        throw new Error(
          `Non named type ${TypeIndex[typeIndex] ?? "Unknown"} used as an enum variant, ` +
            "each variant should be a reference to a struct"
        );
      }
      const index = this.bufferReader.readU8();
      variants.push({ discriminant, def: { typeIndex, index } });
    }
    return new EnumTypeSpec(name, variants);
  }

  public parseTypeSpec(): TypeSpec {
    const typeIndex = this.bufferReader.readU8();

    if (!(typeIndex in TypeIndex)) {
      throw new Error(`Unknown type index: 0x${toHexString(typeIndex)}`);
    }

    if (typeIndex === TypeIndex.Named) {
      const index = this.bufferReader.readU8();
      return { typeIndex, index };
    } else if (typeIndex === TypeIndex.Vec) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.Option) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.Map || typeIndex == TypeIndex.AvlTreeMap) {
      const keyType = this.parseTypeSpec();
      const valueType = this.parseTypeSpec();
      return { typeIndex, keyType, valueType };
    } else if (typeIndex === TypeIndex.Set) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.SizedByteArray) {
      const valueType: TypeSpec = { typeIndex: TypeIndex.u8 };
      const length = this.bufferReader.readU8();
      return { typeIndex: TypeIndex.SizedArray, valueType, length };
    } else if (typeIndex === TypeIndex.SizedArray) {
      const valueType = this.parseTypeSpec();
      const length = this.bufferReader.readU8();
      return { typeIndex, valueType, length };
    } else {
      return { typeIndex };
    }
  }

  private parseContractCall(
    config: Configuration,
    defaultFnKind: ContractInvocationKind,
    functionKinds: ContractInvocationKind[]
  ): ContractInvocation {
    let fnKind;
    if (config.fnType === FunctionFormat.FnKind) {
      const kindId = this.bufferReader.readU8();
      fnKind = AbiParser.getKind(functionKinds, kindId);
      if (fnKind === undefined) {
        throw new Error(`Unsupported FnKind type 0x${toHexString(kindId)} specified`);
      }
    } else {
      fnKind = defaultFnKind;
    }

    const name = this.bufferReader.readString();
    let shortname;
    if (config.shortnameType === ShortnameType.leb128) {
      shortname = this.parseLeb128();
      if (shortname.length > 5) {
        throw new Error(
          "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5 bytes)"
        );
      }
    } else {
      if (fnKind.kindId === 1) {
        shortname = Buffer.from([0, 0, 0, 0]);
      } else {
        shortname = AbiParser.parseHashShortname(config.shortnameLength as number, name);
      }
    }

    const fnArguments = this.parseArguments();

    let secretArgument: ArgumentAbi | null = null;
    if (config.chainComponentFormat === ChainComponentFormat.ChainComponents) {
      if (this.bufferReader.readBoolean()) {
        secretArgument = this.parseArgument();
      }
    } else {
      if (
        fnKind.kindId ===
        ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId
      ) {
        secretArgument = this.parseArgument();
      } else if (fnKind.kindId === ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT.kindId) {
        // Use default secret argument type
        fnKind = ImplicitBinderAbi.DefaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE;
        secretArgument = { name: "secret_input", type: { typeIndex: TypeIndex.i32 } };
      }
    }
    return new ContractInvocation(fnKind, name, shortname, fnArguments, secretArgument);
  }

  private parseArguments(): ArgumentAbi[] {
    const fnArguments = [];
    const argumentsSize = this.bufferReader.readI32();
    for (let i = 0; i < argumentsSize; i++) {
      fnArguments.push(this.parseArgument());
    }
    return fnArguments;
  }

  private parseArgument(): ArgumentAbi {
    const nameArg = this.bufferReader.readString();
    const argType = this.parseTypeSpec();
    return { name: nameArg, type: argType };
  }

  private static parseHashShortname(shortnameLength: number, name: string): Buffer {
    const nameBuffer = Buffer.from(name);
    const hash = hashBuffer(nameBuffer);
    return hash.slice(0, shortnameLength);
  }

  private parseLeb128(): Buffer {
    let byte: number;
    const bufferWriter = new BigEndianByteOutput();
    do {
      byte = this.bufferReader.readU8();
      bufferWriter.writeU8(byte);
    } while (byte >= 128);
    return bufferWriter.toBuffer();
  }

  private parseFunctions(
    config: Configuration,
    functionKinds: ContractInvocationKind[]
  ): ContractInvocation[] {
    const actionList = [];
    if (config.fnType === FunctionFormat.InitSeparately) {
      const initCall = this.parseContractCall(
        config,
        ImplicitBinderAbi.DefaultKinds.INIT,
        functionKinds
      );
      actionList.push(initCall);
    }
    const actionsSize = this.bufferReader.readI32();
    for (let i = 0; i < actionsSize; i++) {
      const action = this.parseContractCall(
        config,
        ImplicitBinderAbi.DefaultKinds.ACTION,
        functionKinds
      );
      actionList.push(action);
    }

    return actionList;
  }

  public static printModel(model: FileAbi): string {
    if (model.chainComponent instanceof ContractAbi) {
      const printer = new RustSyntaxPrettyPrinter(model);
      return printer.printModel();
    } else {
      const printer = new BinderRustSyntaxPrettyPrinter(model);
      return printer.printModel();
    }
  }

  public static printFunction(model: FileAbi, fnAbi: ContractInvocation): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printFunction(fnAbi);
  }

  public static printBinderInvocation(model: FileAbi, binderInvocation: BinderInvocation): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printBinderInvocation(binderInvocation);
  }

  public static printStruct(model: FileAbi, struct: StructTypeSpec): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printStruct(struct);
  }

  public static getKind(
    kinds: ContractInvocationKind[],
    kindId: number
  ): ContractInvocationKind | undefined {
    return kinds.find((kind) => kind.kindId === kindId);
  }
}
