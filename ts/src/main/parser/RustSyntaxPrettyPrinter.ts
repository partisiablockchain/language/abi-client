/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ArgumentAbi,
  BinderInvocation,
  BinderType,
  EnumVariant,
  FieldAbi,
  ContractInvocationKind,
  GenericTypeAbi,
  TransactionKind,
  NamedTypeSpec,
  TypeIndex,
  TypeSpec,
} from "../model/Abi";
import { AbiVersion } from "../model/AbiVersion";
import { DocumentationFunction } from "../model/DocumentationFunction";
import { DocumentationNamedType } from "../model/DocumentationNamedType";
import { EnumTypeSpec } from "../model/EnumTypeSpec";
import { FileAbi } from "../model/FileAbi";
import { ContractInvocation } from "../model/ContractInvocation";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { TypeSpecStringifier } from "./TypeSpecStringifier";
import { ContractAbi } from "../model/ContractAbi";
import { AbiParser } from "./AbiParser";
import { ImplicitBinderAbi } from "./ImplicitBinderAbi";

export class RustSyntaxPrettyPrinter {
  private readonly model;
  private readonly contract: ContractAbi;
  private readonly typeStringifier: TypeSpecStringifier;

  constructor(model: FileAbi) {
    this.model = model;
    this.contract = model.contract();
    this.typeStringifier = new TypeSpecStringifier(
      model.chainComponent.namedTypes.map((t) => t.name)
    );
  }

  printModel(): string {
    const builder: string[] = [];

    const headerBuilder = new DocBuilder("//! ", builder, this.typeStringifier);
    for (const line of this.model.description) {
      headerBuilder.appendLine(line);
    }

    if (this.model.description.length > 0) {
      headerBuilder.appendLine("\n\n");
    }

    // Build version information
    builder.push(this.printBinderVersion());
    builder.push(this.printClientVersion());
    builder.push("\n");

    // Build binder type
    builder.push("// Binder Type: ");
    builder.push(BinderType[this.contract.binderType]);
    builder.push("\n\n");

    // Build list of struct definition
    RustSyntaxPrettyPrinter.buildStateDefinition(
      builder,
      this.typeStringifier,
      this.contract.namedTypes,
      this.contract.stateType
    );
    RustSyntaxPrettyPrinter.buildNamedTypes(
      builder,
      this.typeStringifier,
      this.contract.namedTypes,
      this.contract.stateType
    );
    const binderInvocations = this.contract.binderInvocations;
    for (const func of binderInvocations) {
      RustSyntaxPrettyPrinter.buildBinderInvocation(
        builder,
        this.typeStringifier,
        this.contract.invocationKinds,
        func
      );
    }

    // Build all functions
    builder.push(this.printAllInvocations());

    return builder.join("");
  }

  static buildNamedTypes(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    namedTypes: NamedTypeSpec[],
    stateType: TypeSpec
  ) {
    const namedTypesCopy = [...namedTypes];
    if (stateType.typeIndex === TypeIndex.Named) {
      namedTypesCopy.splice(stateType.index, 1);
    }
    for (const namedTypeSpec of namedTypesCopy) {
      this.buildNamedDefinition(builder, typeStringifier, namedTypesCopy, namedTypeSpec);
    }
  }

  static buildVersion(builder: string[], version: AbiVersion) {
    builder.push(`${version.major}.${version.minor}.${version.patch}\n`);
  }

  private printFunctions(builder: string[], functionKinds: (f: ContractInvocation) => boolean) {
    const contract = this.model.contract();
    const functions: ContractInvocation[] = contract.contractInvocations.filter((f) =>
      functionKinds(f)
    );
    for (const fun of functions) {
      this.buildFunction(builder, fun);
      builder.push("\n");
    }
  }

  printAllInvocations(): string {
    const builder: string[] = [];
    this.printFunctions(builder, (_) => true);
    return builder.join("");
  }

  printActions(): string {
    const builder: string[] = [];
    this.printFunctions(
      builder,
      (f: ContractInvocation) =>
        f.kind.kindId === ImplicitBinderAbi.DefaultKinds.ACTION.kindId ||
        f.kind.kindId === ImplicitBinderAbi.DefaultKinds.INIT.kindId
    );
    return builder.join("");
  }

  printCallbacks(): string {
    const builder: string[] = [];
    this.printFunctions(
      builder,
      (f: ContractInvocation) => f.kind.kindId === ImplicitBinderAbi.DefaultKinds.CALLBACK.kindId
    );
    return builder.join("");
  }

  printBinderVersion(): string {
    const builder: string[] = [];
    builder.push("// Binder Version: ");
    RustSyntaxPrettyPrinter.buildVersion(builder, this.model.versionBinder);
    return builder.join("");
  }

  printClientVersion(): string {
    const builder: string[] = [];
    builder.push("// Client Version: ");
    RustSyntaxPrettyPrinter.buildVersion(builder, this.model.versionClient);
    return builder.join("");
  }

  printFunction(func: ContractInvocation): string {
    const builder: string[] = [];
    this.buildFunction(builder, func);
    return builder.join("");
  }

  printBinderInvocation(func: BinderInvocation): string {
    const builder: string[] = [];
    RustSyntaxPrettyPrinter.buildBinderInvocation(
      builder,
      this.typeStringifier,
      this.contract.invocationKinds,
      func
    );
    return builder.join("");
  }

  printState(): string {
    const builder: string[] = [];
    RustSyntaxPrettyPrinter.buildStateDefinition(
      builder,
      this.typeStringifier,
      this.contract.namedTypes,
      this.contract.stateType
    );
    return builder.join("");
  }

  printStruct(struct: StructTypeSpec): string {
    const builder: string[] = [];
    const stateStruct = (this.model.chainComponent as ContractAbi).getStateStruct();
    if (struct === stateStruct) {
      RustSyntaxPrettyPrinter.buildStateDefinition(
        builder,
        this.typeStringifier,
        this.contract.namedTypes,
        this.contract.stateType
      );
    } else {
      RustSyntaxPrettyPrinter.buildStructDefinition(builder, this.typeStringifier, struct);
    }

    return builder.join("");
  }

  private buildFunction(builder: string[], func: ContractInvocation) {
    this.buildFunctionCall(builder, func, func.arguments);
  }

  private buildFunctionCall(
    builder: string[],
    func: ContractInvocation,
    allArguments: ArgumentAbi[]
  ) {
    RustSyntaxPrettyPrinter.buildDocumentationForFn(builder, this.typeStringifier, func, func.doc);
    builder.push("#[");
    builder.push(RustSyntaxPrettyPrinter.fnKindToRustName(func.kind));

    if (func.kind.hasShortname) {
      builder.push(`(shortname = 0x${func.shortname.toString("hex")}`);
      const type = func.secretArgument?.type;
      // This implicitly means that it is secret input with explicit type
      if (type !== undefined) {
        builder.push(', secret_type = "');
        this.typeStringifier.stringify(builder, type);
        builder.push('"');
      }
      builder.push(")");
    }
    builder.push("]\n");

    builder.push(`pub fn ${func.name} (`);
    if (allArguments.length > 0) {
      builder.push("\n");
    }
    for (const arg of allArguments) {
      RustSyntaxPrettyPrinter.buildArgument(builder, this.typeStringifier, arg);
    }
    builder.push(")\n");
  }

  static fnKindToRustName(kind: ContractInvocationKind): string {
    return kind.name.toLowerCase();
  }

  static buildDocumentationForStruct(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    structAbi: StructTypeSpec,
    doc: DocumentationNamedType
  ) {
    const docBuilder = new DocBuilder("/// ", builder, typeStringifier);
    docBuilder.appendParagraph(doc.description);
    docBuilder.appendFields(structAbi.fields, doc.fields);
  }

  static buildDocumentationForFn(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    fnAbi: ContractInvocation,
    doc: DocumentationFunction
  ) {
    const docBuilder = new DocBuilder("/// ", builder, typeStringifier);
    docBuilder.appendParagraph(doc.description);
    docBuilder.appendArguments(fnAbi.arguments, doc.arguments);

    if (doc.returns != null) {
      docBuilder.appendReturns(doc.returns);
    }
  }

  static buildArgument(builder: string[], typeStringifier: TypeSpecStringifier, arg: ArgumentAbi) {
    builder.push(`    ${arg.name}: `);
    typeStringifier.stringify(builder, arg.type);
    builder.push(",\n");
  }

  static buildNamedDefinition(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    namedTypes: NamedTypeSpec[],
    named: NamedTypeSpec
  ) {
    if (
      named instanceof StructTypeSpec &&
      !this.isEnumVariant(named as StructTypeSpec, namedTypes)
    ) {
      this.buildStructDefinition(builder, typeStringifier, named as StructTypeSpec);
    } else if (named instanceof EnumTypeSpec) {
      this.buildEnumDefinition(builder, typeStringifier, namedTypes, named as EnumTypeSpec);
    }
  }

  static isEnumVariant(structTypeSpec: StructTypeSpec, namedTypes: NamedTypeSpec[]): boolean {
    const namedTypeSpecs = namedTypes;
    for (const namedTypeSpec of namedTypeSpecs) {
      if (namedTypeSpec instanceof EnumTypeSpec) {
        const enumTypeSpec = namedTypeSpec as EnumTypeSpec;
        for (const variant of enumTypeSpec.variants) {
          if (namedTypes[variant.def.index] === structTypeSpec) {
            return true;
          }
        }
      }
    }
    return false;
  }

  static buildEnumDefinition(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    namedTypes: NamedTypeSpec[],
    enumTypeSpec: EnumTypeSpec
  ) {
    builder.push("enum " + enumTypeSpec.name + " {\n");
    for (const variant of enumTypeSpec.variants) {
      this.buildEnumVariant(builder, typeStringifier, namedTypes, variant);
    }
    builder.push("}\n\n");
  }

  static buildEnumVariant(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    namedTypes: NamedTypeSpec[],
    variant: EnumVariant
  ) {
    const namedTypeSpec = namedTypes[variant.def.index] as StructTypeSpec;
    builder.push("    " + "#[discriminant(" + variant.discriminant + ")]\n");
    builder.push("    " + namedTypeSpec.name + " {");

    for (let i = 0; i < namedTypeSpec.fields.length; i++) {
      const field = namedTypeSpec.fields[i];
      builder.push(" " + field.name + ": ");
      typeStringifier.stringify(builder, field.type);
      if (i < namedTypeSpec.fields.length - 1) {
        builder.push(",");
      } else {
        builder.push(" ");
      }
    }
    builder.push("},\n");
  }

  static buildStateDefinition(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    namedType: NamedTypeSpec[],
    stateType: TypeSpec
  ) {
    if (stateType.typeIndex == TypeIndex.Named) {
      const state = namedType[stateType.index] as StructTypeSpec;
      this.buildDocumentationForStruct(builder, typeStringifier, state, state.doc);
      builder.push("#[state]\n");
      builder.push(`pub struct ${state.name} {\n`);

      for (const field of state.fields) {
        this.buildFields(builder, typeStringifier, field);
      }
      builder.push("}\n\n");
    } else {
      builder.push("#[state]\n");
      typeStringifier.stringify(builder, stateType);
      builder.push("\n\n");
    }
  }

  static buildStructDefinition(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    struct: StructTypeSpec
  ) {
    this.buildDocumentationForStruct(builder, typeStringifier, struct, struct.doc);

    builder.push(`pub struct ${struct.name} {\n`);

    for (const field of struct.fields) {
      this.buildFields(builder, typeStringifier, field);
    }
    builder.push("}\n\n");
  }

  static buildFields(builder: string[], typeStringifier: TypeSpecStringifier, field: FieldAbi) {
    builder.push(`    ${field.name}: `);
    typeStringifier.stringify(builder, field.type);
    builder.push(",\n");
  }

  static buildBinderInvocation(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    functionKinds: ContractInvocationKind[],
    binderInvocation: BinderInvocation
  ) {
    RustSyntaxPrettyPrinter.buildBinderInvocationHeader(builder, functionKinds, binderInvocation);
    RustSyntaxPrettyPrinter.buildBinderInvocationSignature(
      builder,
      typeStringifier,
      binderInvocation
    );
  }

  static buildBinderInvocationSignature(
    builder: string[],
    typeStringifier: TypeSpecStringifier,
    binderInvocation: BinderInvocation
  ) {
    builder.push(`pub fn ${binderInvocation.name} (`);
    if (binderInvocation.arguments.length > 0) {
      builder.push("\n");
    }
    for (const arg of binderInvocation.arguments) {
      this.buildArgument(builder, typeStringifier, arg);
    }
    builder.push(")\n\n");
  }

  private static readonly INVOCATION_KIND_MACRO_NAMES: Map<TransactionKind, string> = new Map<
    TransactionKind,
    string
  >([
    [TransactionKind.Init, "init"],
    [TransactionKind.Action, "action"],
    [TransactionKind.Callback, "callback"],
    [TransactionKind.Upgrade, "upgrade"],
    [TransactionKind.EmptyAction, "empty_action"],
  ]);

  static buildBinderInvocationHeader(
    builder: string[],
    functionKinds: ContractInvocationKind[],
    invocation: BinderInvocation
  ) {
    const kind = invocation.kind;
    builder.push(`#[binder_${RustSyntaxPrettyPrinter.INVOCATION_KIND_MACRO_NAMES.get(kind)}`);
    const modifiers = [];
    if (invocation.shortname != null) {
      const hexShortname = invocation.shortname.toString(16).padStart(2, "0");
      modifiers.push(`shortname = 0x${hexShortname}`);
    }
    if (invocation.contractCallKindId !== null) {
      const callKindName = AbiParser.getKind(
        functionKinds,
        invocation.contractCallKindId
      )!.name.toLowerCase();
      modifiers.push("contract_call = " + callKindName);
    }
    if (modifiers.length !== 0) {
      builder.push("(");
      builder.push(modifiers.join(", "));
      builder.push(")");
    }
    builder.push("]\n");
  }
}

export class DocBuilder {
  private readonly prefix: string;
  private readonly builder: string[];
  private readonly typeSpecPrinter: TypeSpecStringifier;

  constructor(prefix: string, builder: string[], typeSpecPrinter: TypeSpecStringifier) {
    this.prefix = prefix;
    this.builder = builder;
    this.typeSpecPrinter = typeSpecPrinter;
  }

  appendParagraph(paragraph: string[]) {
    for (const line of paragraph) {
      this.appendLine(line);
    }

    if (paragraph.length > 0) {
      this.appendLine("");
    }
  }

  appendLine(line: string) {
    this.builder.push(this.prefix);
    this.builder.push(line);
    this.builder.push("\n");
  }

  appendArguments(args: ArgumentAbi[], descriptions: Map<string, string>) {
    this.appendGenericParamList("# Parameters", args, descriptions);
  }

  appendFields(fields: FieldAbi[], descriptions: Map<string, string>) {
    this.appendGenericParamList("### Fields", fields, descriptions);
  }

  appendGenericParamList(
    sectionHeader: string,
    params: Array<GenericTypeAbi<TypeSpec>>,
    paramDescriptions: Map<string, string>
  ) {
    if (this.countParamLines(params, paramDescriptions) > 0) {
      this.appendLine(sectionHeader);
      this.appendLine("");

      for (const arg of params) {
        const description = paramDescriptions.get(arg.name);
        if (description !== undefined) {
          this.appendArgOrField(arg.name, arg.type, description);
        }
      }
      this.appendLine("");
    }
  }

  countParamLines(
    params: Array<GenericTypeAbi<TypeSpec>>,
    descriptions: Map<string, string>
  ): number {
    let count = 0;
    for (const arg of params) {
      if (descriptions.get(arg.name) !== undefined) {
        count += 1;
      }
    }
    return count;
  }

  appendArgOrField(name: string, type: TypeSpec, description: string) {
    this.builder.push(...[this.prefix, "* `", name, "`: "]);

    this.builder.push("[`");
    this.typeSpecPrinter.stringify(this.builder, type);
    this.builder.push("`], ");

    this.builder.push(...[description, "\n"]);
  }

  appendReturns(returns: string) {
    this.appendLine("# Returns:");
    this.appendLine("");
    this.appendLine(returns);
    this.appendLine("");
  }

  build(): string {
    return this.builder.join("");
  }
}
