/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  MapTypeSpec,
  NamedTypeRef,
  OptionTypeSpec,
  SetTypeSpec,
  SizedArrayTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
} from "../model/Abi";

export class TypeSpecStringifier {
  private readonly namedTypes;

  constructor(namedTypes: string[]) {
    this.namedTypes = namedTypes;
  }

  public stringify(builder: string[], type: TypeSpec) {
    const typeIndex = type.typeIndex;

    if (typeIndex === TypeIndex.Named) {
      this.buildStructRef(builder, type);
    } else if (typeIndex === TypeIndex.Vec) {
      this.buildVec(builder, type);
    } else if (typeIndex === TypeIndex.Option) {
      this.buildOption(builder, type);
    } else if (typeIndex === TypeIndex.Map) {
      this.buildMap(builder, type);
    } else if (typeIndex === TypeIndex.Set) {
      this.buildSet(builder, type);
    } else if (typeIndex === TypeIndex.SizedArray) {
      this.buildSizedArray(builder, type);
    } else {
      builder.push(TypeIndex[typeIndex]);
    }
  }

  private buildStructRef(builder: string[], type: NamedTypeRef) {
    builder.push(this.namedTypes[type.index]);
  }

  private buildVec(builder: string[], type: VecTypeSpec) {
    builder.push("Vec<");
    this.stringify(builder, type.valueType);
    builder.push(">");
  }

  private buildOption(builder: string[], type: OptionTypeSpec) {
    builder.push("Option<");
    this.stringify(builder, type.valueType);
    builder.push(">");
  }

  private buildMap(builder: string[], type: MapTypeSpec) {
    builder.push("Map<");
    this.stringify(builder, type.keyType);
    builder.push(", ");
    this.stringify(builder, type.valueType);
    builder.push(">");
  }

  private buildSet(builder: string[], type: SetTypeSpec) {
    builder.push("Set<");
    this.stringify(builder, type.valueType);
    builder.push(">");
  }

  private buildSizedArray(builder: string[], type: SizedArrayTypeSpec) {
    builder.push("[");
    this.stringify(builder, type.valueType);
    builder.push(`; ${type.length}]`);
  }
}
