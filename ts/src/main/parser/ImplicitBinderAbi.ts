/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinderInvocation, TransactionKind, TypeIndex } from "../model/Abi";

const defaultKinds = {
  INIT: { kindId: 0x01, name: "INIT", hasShortname: true },
  ACTION: { kindId: 0x02, name: "ACTION", hasShortname: true },
  CALLBACK: { kindId: 0x03, name: "CALLBACK", hasShortname: true },
  ZK_ON_SECRET_INPUT: { kindId: 0x10, name: "ZK_ON_SECRET_INPUT", hasShortname: true },
  ZK_ON_VARIABLE_INPUTTED: { kindId: 0x11, name: "ZK_ON_VARIABLE_INPUTTED", hasShortname: true },
  ZK_ON_VARIABLE_REJECTED: { kindId: 0x12, name: "ZK_ON_VARIABLE_REJECTED", hasShortname: false },
  ZK_ON_COMPUTE_COMPLETE: { kindId: 0x13, name: "ZK_ON_COMPUTE_COMPLETE", hasShortname: true },
  ZK_ON_VARIABLES_OPENED: { kindId: 0x14, name: "ZK_ON_VARIABLES_OPENED", hasShortname: false },
  ZK_ON_USER_VARIABLES_OPENED: {
    kindId: 0x15,
    name: "ZK_ON_USER_VARIABLES_OPENED",
    hasShortname: false,
  },
  ZK_ON_ATTESTATION_COMPLETE: {
    kindId: 0x16,
    name: "ZK_ON_ATTESTATION_COMPLETE",
    hasShortname: false,
  },
  ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE: {
    kindId: 0x17,
    name: "ZK_ON_SECRET_INPUT",
    hasShortname: true,
  },
  ZK_ON_EXTERNAL_EVENT: { kindId: 0x18, name: "ZK_ON_EXTERNAL_EVENT", hasShortname: false },
};

export const ImplicitBinderAbi = {
  DefaultKinds: defaultKinds,

  DEFAULT_ZK_FUNCTION_KINDS: [
    defaultKinds.INIT,
    defaultKinds.ACTION,
    defaultKinds.CALLBACK,
    defaultKinds.ZK_ON_SECRET_INPUT,
    defaultKinds.ZK_ON_VARIABLE_INPUTTED,
    defaultKinds.ZK_ON_VARIABLE_REJECTED,
    defaultKinds.ZK_ON_COMPUTE_COMPLETE,
    defaultKinds.ZK_ON_VARIABLES_OPENED,
    defaultKinds.ZK_ON_USER_VARIABLES_OPENED,
    defaultKinds.ZK_ON_ATTESTATION_COMPLETE,
    defaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE,
    defaultKinds.ZK_ON_EXTERNAL_EVENT,
  ],

  DEFAULT_PUB_FUNCTION_KINDS: [defaultKinds.INIT, defaultKinds.ACTION, defaultKinds.CALLBACK],

  isZkKind(kindId: number): boolean {
    return (
      kindId === defaultKinds.ZK_ON_SECRET_INPUT.kindId ||
      kindId === defaultKinds.ZK_ON_COMPUTE_COMPLETE.kindId ||
      kindId === defaultKinds.ZK_ON_USER_VARIABLES_OPENED.kindId ||
      kindId === defaultKinds.ZK_ON_VARIABLE_INPUTTED.kindId ||
      kindId === defaultKinds.ZK_ON_VARIABLES_OPENED.kindId ||
      kindId === defaultKinds.ZK_ON_VARIABLE_REJECTED.kindId ||
      kindId === defaultKinds.ZK_ON_ATTESTATION_COMPLETE.kindId ||
      kindId === defaultKinds.ZK_ON_SECRET_INPUT_WITH_EXPLICIT_TYPE.kindId ||
      kindId === defaultKinds.ZK_ON_EXTERNAL_EVENT.kindId
    );
  },

  defaultPubInvocations(): BinderInvocation[] {
    return [
      {
        kind: TransactionKind.Init,
        name: "create",
        shortname: null,
        arguments: [],
        contractCallKindId: 0x01,
      },
      {
        kind: TransactionKind.Action,
        name: "invoke",
        shortname: null,
        arguments: [],
        contractCallKindId: 0x02,
      },
      {
        kind: TransactionKind.Callback,
        name: "callback",
        shortname: null,
        arguments: [],
        contractCallKindId: 0x03,
      },
      {
        kind: TransactionKind.EmptyAction,
        name: "topUpGas",
        shortname: null,
        arguments: [],
        contractCallKindId: null,
      },
    ];
  },

  defaultZkInvocations(): BinderInvocation[] {
    return [
      {
        kind: TransactionKind.Init,
        name: "create",
        shortname: null,
        arguments: [],
        contractCallKindId: 0x01,
      },
      {
        kind: TransactionKind.Action,
        name: "commitResultVariable",
        shortname: 0x00,
        arguments: [
          { name: "calculationFor", type: { typeIndex: TypeIndex.i64 } },
          {
            name: "restOfBinderArgs",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
          },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "onChainOutput",
        shortname: 0x01,
        arguments: [
          { name: "outputId", type: { typeIndex: TypeIndex.i32 } },
          {
            name: "engineShares",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
          },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "unableToCalculate",
        shortname: 0x02,
        arguments: [{ name: "calculationFor", type: { typeIndex: TypeIndex.i64 } }],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "openMaskedInput",
        shortname: 0x03,
        arguments: [
          { name: "variableId", type: { typeIndex: TypeIndex.i32 } },
          {
            name: "maskedInput",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
          },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "zeroKnowledgeInputOffChain",
        shortname: 0x04,
        arguments: [
          {
            name: "bitLengths",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.i32 } },
          },
          { name: "commitment1", type: { typeIndex: TypeIndex.Hash } },
          { name: "commitment2", type: { typeIndex: TypeIndex.Hash } },
          { name: "commitment3", type: { typeIndex: TypeIndex.Hash } },
          { name: "commitment4", type: { typeIndex: TypeIndex.Hash } },
        ],
        contractCallKindId: 0x17,
      },
      {
        kind: TransactionKind.Action,
        name: "zeroKnowledgeInputOnChain",
        shortname: 0x05,
        arguments: [
          {
            name: "bitLengths",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.i32 } },
          },
          { name: "publicKey", type: { typeIndex: TypeIndex.PublicKey } },
          {
            name: "encryptedShares",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
          },
        ],
        contractCallKindId: 0x17,
      },
      {
        kind: TransactionKind.Action,
        name: "rejectInput",
        shortname: 0x06,
        arguments: [{ name: "variableId", type: { typeIndex: TypeIndex.i32 } }],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "openInvocation",
        shortname: 0x09,
        arguments: [],
        contractCallKindId: 0x02,
      },
      {
        kind: TransactionKind.Action,
        name: "addAttestationSignature",
        shortname: 0x0a,
        arguments: [
          { name: "attestationId", type: { typeIndex: TypeIndex.i32 } },
          { name: "signature", type: { typeIndex: TypeIndex.Signature } },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "getComputationDeadline",
        shortname: 0x0b,
        arguments: [],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "onComputeComplete",
        shortname: 0x0c,
        arguments: [
          {
            name: "ids",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.i32 } },
          },
        ],
        contractCallKindId: 0x13,
      },
      {
        kind: TransactionKind.Action,
        name: "onVariablesOpened",
        shortname: 0x0d,
        arguments: [
          {
            name: "ids",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.i32 } },
          },
        ],
        contractCallKindId: 0x14,
      },
      {
        kind: TransactionKind.Action,
        name: "onAttestationComplete",
        shortname: 0x0e,
        arguments: [{ name: "id", type: { typeIndex: TypeIndex.i32 } }],
        contractCallKindId: 0x16,
      },
      {
        kind: TransactionKind.Action,
        name: "onVariableInputted",
        shortname: 0x0f,
        arguments: [{ name: "variableId", type: { typeIndex: TypeIndex.i32 } }],
        contractCallKindId: 0x11,
      },
      {
        kind: TransactionKind.Action,
        name: "onVariableRejected",
        shortname: 0x10,
        arguments: [{ name: "variableId", type: { typeIndex: TypeIndex.i32 } }],
        contractCallKindId: 0x12,
      },
      {
        kind: TransactionKind.Action,
        name: "onUserVariablesOpened",
        shortname: 0x11,
        arguments: [
          {
            name: "ids",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.i32 } },
          },
        ],
        contractCallKindId: 0x15,
      },
      {
        kind: TransactionKind.Action,
        name: "addBatches",
        shortname: 0x12,
        arguments: [
          { name: "batchType", type: { typeIndex: TypeIndex.u8 } },
          { name: "batchId", type: { typeIndex: TypeIndex.i32 } },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "extendZkComputationDeadline",
        shortname: 0x13,
        arguments: [
          { name: "msPerGasNumerator", type: { typeIndex: TypeIndex.i64 } },
          { name: "msPerGasDenominator", type: { typeIndex: TypeIndex.i64 } },
          { name: "minExtension", type: { typeIndex: TypeIndex.i64 } },
          { name: "maxExtension", type: { typeIndex: TypeIndex.i64 } },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "addExternalEvent",
        shortname: 0x14,
        arguments: [
          { name: "subscriptionId", type: { typeIndex: TypeIndex.i32 } },
          {
            name: "restOfBinderArgs",
            type: { typeIndex: TypeIndex.Vec, valueType: { typeIndex: TypeIndex.u8 } },
          },
        ],
        contractCallKindId: null,
      },
      {
        kind: TransactionKind.Action,
        name: "onExternalEvent",
        shortname: 0x15,
        arguments: [
          { name: "subscriptionId", type: { typeIndex: TypeIndex.i32 } },
          { name: "eventId", type: { typeIndex: TypeIndex.i32 } },
        ],
        contractCallKindId: 0x18,
      },
      {
        kind: TransactionKind.Callback,
        name: "callback",
        shortname: null,
        arguments: [],
        contractCallKindId: 0x03,
      },
      {
        kind: TransactionKind.EmptyAction,
        name: "topUpGas",
        shortname: null,
        arguments: [],
        contractCallKindId: null,
      },
    ];
  },
};
