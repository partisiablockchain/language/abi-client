/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ChainComponentFormat,
  ConfigOption,
  FunctionFormat,
  NamedTypesFormat,
  ShortnameType,
  TypeAndFunctionOrder,
} from "../model/Abi";
import { AbiVersion } from "../model/AbiVersion";

export class Configuration {
  public readonly shortnameType: ShortnameType;
  public readonly fnType: FunctionFormat;
  public readonly shortnameLength: number | null;
  public readonly namedTypesFormat: NamedTypesFormat;
  public readonly typeAndFunctionOrder: TypeAndFunctionOrder;
  public readonly chainComponentFormat: ChainComponentFormat;

  private static readonly supportedClientVersions: Map<string, ConfigOption> = new Map([
    [
      "1.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "2.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "3.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "3.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "4.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "4.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.2.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.3.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.4.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.5.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.6.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.Unordered,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "5.7.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.DepthFirst,
        chainComponentFormat: ChainComponentFormat.OnlyContract,
      },
    ],
    [
      "6.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
        typeAndFunctionOrder: TypeAndFunctionOrder.DepthFirst,
        chainComponentFormat: ChainComponentFormat.ChainComponents,
      },
    ],
  ]);

  constructor(
    shortnameType: ShortnameType,
    fnType: FunctionFormat,
    shortnameLength: number | null,
    namedTypesFormat: NamedTypesFormat,
    typeAndFunctionOrder: TypeAndFunctionOrder,
    chainComponentFormat: ChainComponentFormat
  ) {
    this.shortnameType = shortnameType;
    this.fnType = fnType;
    this.shortnameLength = shortnameLength;
    this.namedTypesFormat = namedTypesFormat;
    this.typeAndFunctionOrder = typeAndFunctionOrder;
    this.chainComponentFormat = chainComponentFormat;
  }

  public static fromClientVersion(
    version: AbiVersion,
    shortnameLength: number | null
  ): Configuration {
    const options = Configuration.getOptionsOrThrow(version);
    return new Configuration(
      options.shortnameType,
      options.fnType,
      shortnameLength,
      options.namedTypesFormat,
      options.typeAndFunctionOrder,
      options.chainComponentFormat
    );
  }

  public static getOptionsOrThrow(clientVersion: AbiVersion): ConfigOption {
    const options = this.supportedClientVersions.get(clientVersion.withZeroPatch().toString());
    if (options === undefined) {
      throw new Error(`Unsupported Version ${clientVersion} for Version Client.`);
    } else {
      return options;
    }
  }

  public static isSupportedVersion(clientVersion: AbiVersion): boolean {
    return this.supportedClientVersions.has(clientVersion.withZeroPatch().toString());
  }
}
