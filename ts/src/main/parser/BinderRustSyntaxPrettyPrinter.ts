/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeSpecStringifier } from "./TypeSpecStringifier";
import { FileAbi } from "../model/FileAbi";
import { RustSyntaxPrettyPrinter } from "./RustSyntaxPrettyPrinter";
import { BinderAbi } from "../model/BinderAbi";
import { BinderType, ContractInvocationKind } from "../model/Abi";

export class BinderRustSyntaxPrettyPrinter {
  private readonly model;
  private readonly binder: BinderAbi;
  private readonly typeStringifier: TypeSpecStringifier;

  constructor(model: FileAbi) {
    this.model = model;
    this.binder = model.chainComponent as BinderAbi;
    this.typeStringifier = new TypeSpecStringifier(
      model.chainComponent.namedTypes.map((t) => t.name)
    );
  }

  printModel(): string {
    const builder: string[] = [];

    // Build version information
    builder.push("// Version Binder: ");
    RustSyntaxPrettyPrinter.buildVersion(builder, this.model.versionBinder);
    builder.push("// Version Client: ");
    RustSyntaxPrettyPrinter.buildVersion(builder, this.model.versionClient);
    builder.push("\n");

    // Build binder type
    builder.push("// Binder Type: ");
    builder.push(BinderType[this.binder.binderType]);
    builder.push("\n\n");

    this.buildBinderOnly(builder);

    return builder.join("");
  }

  private buildBinderOnly(builder: string[]) {
    BinderRustSyntaxPrettyPrinter.buildCallableKinds(builder, this.binder.invocationKinds);
    RustSyntaxPrettyPrinter.buildStateDefinition(
      builder,
      this.typeStringifier,
      this.binder.namedTypes,
      this.binder.stateType
    );
    RustSyntaxPrettyPrinter.buildNamedTypes(
      builder,
      this.typeStringifier,
      this.binder.namedTypes,
      this.binder.stateType
    );
    for (const invocation of this.binder.binderInvocations) {
      RustSyntaxPrettyPrinter.buildBinderInvocation(
        builder,
        this.typeStringifier,
        this.binder.invocationKinds,
        invocation
      );
    }
  }

  private static buildCallableKinds(builder: string[], functionKinds: ContractInvocationKind[]) {
    builder.push("enum ContractCallableKinds {\n");
    for (const functionKind of functionKinds) {
      builder.push(`  ${functionKind.name} = ${functionKind.kindId},\n`);
    }
    builder.push("}\n\n");
  }
}
