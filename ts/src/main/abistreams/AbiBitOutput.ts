/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiOutput } from "./AbiOutput";
import { BitOutput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { BlockchainAddress } from "../codegen-lib/BlockchainAddress";
import { BlockchainPublicKey } from "../codegen-lib/BlockchainPublicKey";
import { Signature } from "../codegen-lib/Signature";
import { Hash } from "../codegen-lib/Hash";
import { BlsSignature } from "../codegen-lib/BlsSignature";
import { BlsPublicKey } from "../codegen-lib/BlsPublicKey";

export class AbiBitOutput implements AbiOutput {
  private readonly output: BitOutput;

  public constructor(output: BitOutput) {
    this.output = output;
  }

  public static serialize(serializer: (out: AbiBitOutput) => void): CompactBitArray {
    return BitOutput.serializeBits((stream) => {
      const abiOut = new AbiBitOutput(stream);
      serializer(abiOut);
    });
  }

  writeBoolean(value: boolean): void {
    this.output.writeBoolean(value);
  }

  writeBytes(values: Buffer): void {
    this.output.writeBytes(values);
  }

  writeI16(value: number): void {
    this.output.writeSignedNumber(value, 16);
  }

  writeI32(value: number): void {
    this.output.writeSignedNumber(value, 32);
  }

  writeI64(value: BN): void {
    this.output.writeSignedBN(value, 64);
  }

  writeI8(value: number): void {
    this.output.writeSignedNumber(value, 8);
  }

  writeSignedBigInteger(value: BN, noBytes: number): void {
    this.output.writeSignedBN(value, noBytes * 8);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  writeString(value: string): void {
    throw new Error("Unsupported write string for a bit output");
  }

  writeU16(value: number): void {
    this.output.writeUnsignedNumber(value, 16);
  }

  writeU32(value: number): void {
    this.output.writeUnsignedNumber(value, 32);
  }

  writeU64(value: BN): void {
    this.output.writeUnsignedBN(value, 64);
  }

  writeU8(value: number): void {
    this.output.writeUnsignedNumber(value, 8);
  }

  writeUnsignedBigInteger(value: BN, noBytes: number): void {
    this.output.writeUnsignedBN(value, noBytes * 8);
  }

  /**
   * Writes a blockchain address.
   *
   * @param address the address to write to the stream
   */
  writeAddress(address: BlockchainAddress) {
    this.writeBytes(address.asBuffer());
  }

  /**
   * Writes a public key.
   *
   * @param publicKey the public key to write to the stream
   */
  writePublicKey(publicKey: BlockchainPublicKey) {
    this.writeBytes(publicKey.asBuffer());
  }

  /**
   * Writes a signature.
   *
   * @param signature the signature to write to the stream
   */
  writeSignature(signature: Signature) {
    this.writeBytes(signature.asBuffer());
  }

  /**
   * Writes a hash.
   *
   * @param hash the hash to write to the stream
   */
  writeHash(hash: Hash) {
    this.writeBytes(hash.asBuffer());
  }

  /**
   * Writes a bls signature.
   *
   * @param blsSignature the bls signature to write to the stream
   */
  writeBlsSignature(blsSignature: BlsSignature) {
    this.writeBytes(blsSignature.asBuffer());
  }

  /**
   * Writes a bls public key.
   *
   * @param blsPublicKey the bls public key to write to the stream
   */
  writeBlsPublicKey(blsPublicKey: BlsPublicKey) {
    this.writeBytes(blsPublicKey.asBuffer());
  }
}
