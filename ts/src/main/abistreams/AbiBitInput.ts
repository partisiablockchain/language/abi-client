/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BitInput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { AbiInput } from "./AbiInput";
import * as Buffer from "buffer";
import { BlsSignature } from "../codegen-lib/BlsSignature";
import { BlockchainPublicKey } from "../codegen-lib/BlockchainPublicKey";
import { Signature } from "../codegen-lib/Signature";
import { BlsPublicKey } from "../codegen-lib/BlsPublicKey";
import { Hash } from "../codegen-lib/Hash";
import { BlockchainAddress } from "../codegen-lib/BlockchainAddress";

export class AbiBitInput implements AbiInput {
  private readonly output: BitInput;

  public constructor(output: BitInput) {
    this.output = output;
  }

  public static create(bits: CompactBitArray): AbiBitInput {
    return new AbiBitInput(new BitInput(bits.data));
  }

  readBoolean(): boolean {
    return this.output.readBoolean();
  }

  readBytes(noBytes: number): Buffer {
    return this.output.readBytes(noBytes);
  }

  readI16(): number {
    return this.output.readSignedNumber(16);
  }

  readI32(): number {
    return this.output.readSignedNumber(32);
  }

  readI64(): BN {
    return this.output.readSignedBN(64);
  }

  readI8(): number {
    return this.output.readSignedNumber(8);
  }

  readSignedBigInteger(noBytes: number): BN {
    return this.output.readSignedBN(noBytes * 8);
  }

  readString(): string {
    throw new Error("Unsupported read string for a bit input");
  }

  readU8(): number {
    return this.output.readUnsignedNumber(8);
  }

  readU16(): number {
    return this.output.readUnsignedNumber(16);
  }

  readU32(): number {
    return this.output.readUnsignedNumber(32);
  }

  readU64(): BN {
    return this.output.readUnsignedBN(64);
  }

  readUnsignedBigInteger(noBytes: number): BN {
    return this.output.readUnsignedBN(noBytes * 8);
  }

  readRemaining(): Buffer {
    throw new Error("Cannot read remaining bytes of input when reading bits.");
  }

  /**
   * Reads 21 bytes from the stream as a Blockchain address.
   *
   * @return the read blockchain address
   */
  public readAddress(): BlockchainAddress {
    return BlockchainAddress.fromBuffer(this.readBytes(21));
  }

  /**
   * Reads 65 bytes from the stream as a Signature.
   *
   * @return the read signature
   */
  public readSignature(): Signature {
    return Signature.fromBuffer(this.readBytes(65));
  }

  /**
   * Reads 32 bytes from the stream as a hash.
   *
   * @return the read hash
   */
  public readHash(): Hash {
    return Hash.fromBuffer(this.readBytes(32));
  }

  /**
   * Reads 33 bytes from the stream as a public key.
   *
   * @return the read public key
   */
  public readPublicKey(): BlockchainPublicKey {
    return BlockchainPublicKey.fromBuffer(this.readBytes(33));
  }

  /**
   * Reads 96 bytes from the stream as a bls public key.
   *
   * @return the read bls public key
   */
  public readBlsPublicKey(): BlsPublicKey {
    return BlsPublicKey.fromBuffer(this.readBytes(96));
  }

  /**
   * Reads 48 bytes from the stream as a bls signature.
   *
   * @return the read bls signature
   */
  public readBlsSignature(): BlsSignature {
    return BlsSignature.fromBuffer(this.readBytes(48));
  }
}
