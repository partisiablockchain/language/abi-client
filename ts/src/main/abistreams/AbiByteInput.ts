/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BigEndianByteInput,
  BigEndianByteOutput,
  ByteInput,
  LittleEndianByteInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { BlockchainAddress } from "../codegen-lib/BlockchainAddress";
import { Signature } from "../codegen-lib/Signature";
import { Hash } from "../codegen-lib/Hash";
import { BlockchainPublicKey } from "../codegen-lib/BlockchainPublicKey";
import { BlsPublicKey } from "../codegen-lib/BlsPublicKey";
import { BlsSignature } from "../codegen-lib/BlsSignature";
import { AbiInput } from "./AbiInput";

/** Byte input for reading abi types. */
export class AbiByteInput implements AbiInput {
  private readonly input: ByteInput;

  public constructor(input: ByteInput) {
    this.input = input;
  }

  /**
   * Create a little-endian input stream from bytes.
   *
   * @param bytes input bytes
   * @return input stream
   */
  public static createLittleEndian(bytes: Buffer) {
    return new AbiByteInput(new LittleEndianByteInput(bytes));
  }

  /**
   * Create a big-endian input stream from bytes.
   *
   * @param bytes input bytes
   * @return input stream
   */
  public static createBigEndian(bytes: Buffer) {
    return new AbiByteInput(new BigEndianByteInput(bytes));
  }

  /**
   * Reads an array of bytes.
   *
   * @param noBytes the number of bytes
   * @return the read bytes
   */
  readBytes(noBytes: number): Buffer {
    return this.input.readBytes(noBytes);
  }

  /**
   * Reads a single byte from the stream as a boolean.
   *
   * @return false if the read byte is 0 and true otherwise
   */
  readBoolean(): boolean {
    return this.input.readBoolean();
  }

  /**
   * Reads a signed 8-bit number from the stream.
   *
   * @return the read number as a java byte
   */
  readI8(): number {
    return this.input.readI8();
  }

  /**
   * Reads a signed 16-bit number from the stream.
   *
   * @return the read number as a java short
   */
  readI16(): number {
    return this.input.readI16();
  }

  /**
   * Reads a signed 32-bit number from the stream.
   *
   * @return the read number as a java int
   */
  readI32(): number {
    return this.input.readI32();
  }

  /**
   * Reads a signed 64-bit number from the stream.
   *
   * @return the read number as a java long
   */
  readI64(): BN {
    return this.input.readI64();
  }

  /**
   * Reads a signed BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  readSignedBigInteger(noBytes: number): BN {
    return this.input.readSignedBigInteger(noBytes);
  }

  /**
   * Reads an unsigned 8-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java byte
   */
  readU8(): number {
    return this.input.readU8();
  }

  /**
   * Reads an unsigned 16-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java short
   */
  readU16(): number {
    return this.input.readU16();
  }

  /**
   * Reads an unsigned 32-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java int
   */
  readU32(): number {
    return this.input.readU32();
  }

  /**
   * Reads an unsigned 64-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java long
   */
  readU64(): BN {
    return this.input.readU64();
  }

  /**
   * Reads an unsigned BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  readUnsignedBigInteger(noBytes: number): BN {
    return this.input.readUnsignedBigInteger(noBytes);
  }

  /**
   * Reads a string.
   *
   * @return the read string
   */
  readString(): string {
    return this.input.readString();
  }

  /**
   * Reads 21 bytes from the stream as a Blockchain address.
   *
   * @return the read blockchain address
   */
  public readAddress(): BlockchainAddress {
    return BlockchainAddress.fromBuffer(this.input.readBytes(21));
  }

  /**
   * Reads 65 bytes from the stream as a Signature.
   *
   * @return the read signature
   */
  public readSignature(): Signature {
    return Signature.fromBuffer(this.input.readBytes(65));
  }

  /**
   * Reads 32 bytes from the stream as a hash.
   *
   * @return the read hash
   */
  public readHash(): Hash {
    return Hash.fromBuffer(this.input.readBytes(32));
  }

  /**
   * Reads 33 bytes from the stream as a public key.
   *
   * @return the read public key
   */
  public readPublicKey(): BlockchainPublicKey {
    return BlockchainPublicKey.fromBuffer(this.input.readBytes(33));
  }

  /**
   * Reads 96 bytes from the stream as a bls public key.
   *
   * @return the read bls public key
   */
  public readBlsPublicKey(): BlsPublicKey {
    return BlsPublicKey.fromBuffer(this.input.readBytes(96));
  }

  /**
   * Reads 48 bytes from the stream as a bls signature.
   *
   * @return the read bls signature
   */
  public readBlsSignature(): BlsSignature {
    return BlsSignature.fromBuffer(this.input.readBytes(48));
  }

  readRemaining(): Buffer {
    return this.input.readRemaining();
  }

  /**
   * Reads a shortname as a leb128 encoding.
   *
   * @return the read shortname in a hex encoding
   */
  public readShortnameString(): string {
    return this.readShortname().toString("hex");
  }

  /**
   * Reads a shortname as a leb128 encoding.
   *
   * @return the read shortname as a byte array
   */
  private readShortname(): Buffer {
    let byte: number;
    const bufferWriter = new BigEndianByteOutput();
    for (let i = 0; i < 5; i++) {
      byte = this.input.readU8();
      bufferWriter.writeU8(byte);
      if (byte < 128) {
        return bufferWriter.toBuffer();
      }
    }

    throw new Error(
      "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5" +
        " bytes)"
    );
  }
}
