/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import { BlockchainAddress } from "../codegen-lib/BlockchainAddress";
import { Signature } from "../codegen-lib/Signature";
import { Hash } from "../codegen-lib/Hash";
import { BlockchainPublicKey } from "../codegen-lib/BlockchainPublicKey";
import { BlsPublicKey } from "../codegen-lib/BlsPublicKey";
import { BlsSignature } from "../codegen-lib/BlsSignature";

export interface AbiInput {
  /**
   * Reads a byte array.
   *
   * @return values the byte array read from the stream
   */
  readBytes(noBytes: number): Buffer;

  /**
   * Reads a boolean.
   *
   */
  readBoolean(): boolean;

  /**
   * Reads a signed 8-bit number.
   *
   * @return the number read from stream
   */
  readI8(): number;

  /**
   * Reads a signed 16-bit number.
   *
   * @return the number read from stream
   */
  readI16(): number;

  /**
   * Reads a signed 32-bit number.
   *
   * @return the number read from stream
   */
  readI32(): number;

  /**
   * Reads a signed 64-bit number.
   *
   * @return the BN read from stream
   */
  readI64(): BN;

  /**
   * Reads a signed BN.
   *
   * @param noBytes the number of bytes read
   * @return the number read from stream
   */
  readSignedBigInteger(noBytes: number): BN;

  /**
   * Reads an unsigned 8-bit number.
   *
   * @return the number read from stream
   */
  readU8(): number;

  /**
   * Reads an unsigned 16-bit number.
   *
   * @return the number read from stream
   */
  readU16(): number;

  /**
   * Reads an unsigned 32-bit number.
   *
   * @return the number read from stream
   */
  readU32(): number;

  /**
   * Reads an unsigned 64-bit number.
   *
   * @return the BN read from the stream
   */
  readU64(): BN;

  /**
   * Reads an unsigned BN.
   *
   * @param noBytes the number of bytes read
   * @return the BN read from stream
   */
  readUnsignedBigInteger(noBytes: number): BN;

  /**
   * Reads a string.
   *
   * @return the string read from stream
   */
  readString(): string;

  /**
   * Read the remaining input into a byte array.
   *
   * @return the remaining data in a byte array.
   */
  readRemaining(): Buffer;

  /**
   * Reads 21 bytes from the stream as a Blockchain address.
   *
   * @return the read blockchain address
   */
  readAddress(): BlockchainAddress;

  /**
   * Reads 65 bytes from the stream as a Signature.
   *
   * @return the read signature
   */
  readSignature(): Signature;

  /**
   * Reads 32 bytes from the stream as a hash.
   *
   * @return the read hash
   */
  readHash(): Hash;

  /**
   * Reads 33 bytes from the stream as a public key.
   *
   * @return the read public key
   */
  readPublicKey(): BlockchainPublicKey;

  /**
   * Reads 96 bytes from the stream as a bls public key.
   *
   * @return the read bls public key
   */
  readBlsPublicKey(): BlsPublicKey;

  /**
   * Reads 48 bytes from the stream as a bls signature.
   *
   * @return the read bls signature
   */
  readBlsSignature(): BlsSignature;
}
