/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BigEndianByteOutput,
  ByteOutput,
  LittleEndianByteOutput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { BlockchainAddress } from "../codegen-lib/BlockchainAddress";
import { BlockchainPublicKey } from "../codegen-lib/BlockchainPublicKey";
import { Signature } from "../codegen-lib/Signature";
import { BlsSignature } from "../codegen-lib/BlsSignature";
import { BlsPublicKey } from "../codegen-lib/BlsPublicKey";
import { Hash } from "../codegen-lib/Hash";
import { AbiOutput } from "./AbiOutput";

/** ByteOutput for writing abi types. */
export class AbiByteOutput implements AbiOutput {
  private readonly out: ByteOutput;

  public constructor(out: ByteOutput) {
    this.out = out;
  }

  /**
   * Creates an AbiByteOutput, runs the serialization and returns the bytes as little-endian.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static serializeLittleEndian(serializer: (out: AbiByteOutput) => void) {
    return LittleEndianByteOutput.serialize((out) => {
      const abiOut = new AbiByteOutput(out);
      serializer(abiOut);
    });
  }

  /**
   * Creates an AbiByteOutput, runs the serialization and returns the bytes as big-endian.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static serializeBigEndian(serializer: (out: AbiByteOutput) => void) {
    return BigEndianByteOutput.serialize((out) => {
      const abiOut = new AbiByteOutput(out);
      serializer(abiOut);
    });
  }

  /**
   * Writes a byte array.
   *
   * @param values the byte array to write to the stream
   */
  writeBytes(values: Buffer) {
    this.out.writeBytes(values);
  }

  /**
   * Writes a boolean.
   *
   * @param value the boolean to write to the stream
   */
  writeBoolean(value: boolean) {
    this.out.writeBoolean(value);
  }

  /**
   * Writes a signed byte.
   *
   * @param value the byte to write to the stream
   */
  writeI8(value: number) {
    this.out.writeI8(value);
  }

  /**
   * Writes a signed short.
   *
   * @param value the short to write to the stream
   */
  writeI16(value: number) {
    this.out.writeI16(value);
  }

  /**
   * Writes a signed int.
   *
   * @param value the int to write to the stream
   */
  writeI32(value: number) {
    this.out.writeI32(value);
  }

  /**
   * Writes a signed long.
   *
   * @param value the long to write to the stream
   */
  writeI64(value: BN) {
    this.out.writeI64(value);
  }

  /**
   * Writes a signed BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeSignedBigInteger(value: BN, noBytes: number) {
    this.out.writeSignedBigInteger(value, noBytes);
  }

  /**
   * Writes an unsigned byte.
   *
   * @param value the byte to write to the stream
   */
  writeU8(value: number) {
    this.out.writeU8(value);
  }

  /**
   * Writes an unsigned short.
   *
   * @param value the short to write to the stream
   */
  writeU16(value: number) {
    this.out.writeU16(value);
  }

  /**
   * Writes an unsigned int.
   *
   * @param value the int to write to the stream
   */
  writeU32(value: number) {
    this.out.writeU32(value);
  }

  /**
   * Writes an unsigned long.
   *
   * @param value the long to write to the stream
   */
  writeU64(value: BN) {
    this.out.writeU64(value);
  }

  /**
   * Writes an unsigned BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeUnsignedBigInteger(value: BN, noBytes: number) {
    this.out.writeUnsignedBigInteger(value, noBytes);
  }

  /**
   * Writes a string.
   *
   * @param value the string to write to the stream
   */
  writeString(value: string) {
    this.out.writeString(value);
  }

  /**
   * Writes a blockchain address.
   *
   * @param address the address to write to the stream
   */
  writeAddress(address: BlockchainAddress) {
    this.out.writeBytes(address.asBuffer());
  }

  /**
   * Writes a public key.
   *
   * @param publicKey the public key to write to the stream
   */
  writePublicKey(publicKey: BlockchainPublicKey) {
    this.out.writeBytes(publicKey.asBuffer());
  }

  /**
   * Writes a signature.
   *
   * @param signature the signature to write to the stream
   */
  writeSignature(signature: Signature) {
    this.out.writeBytes(signature.asBuffer());
  }

  /**
   * Writes a hash.
   *
   * @param hash the hash to write to the stream
   */
  writeHash(hash: Hash) {
    this.out.writeBytes(hash.asBuffer());
  }

  /**
   * Writes a bls signature.
   *
   * @param blsSignature the bls signature to write to the stream
   */
  writeBlsSignature(blsSignature: BlsSignature) {
    this.out.writeBytes(blsSignature.asBuffer());
  }

  /**
   * Writes a bls public key.
   *
   * @param blsPublicKey the bls public key to write to the stream
   */
  writeBlsPublicKey(blsPublicKey: BlsPublicKey) {
    this.out.writeBytes(blsPublicKey.asBuffer());
  }
}
