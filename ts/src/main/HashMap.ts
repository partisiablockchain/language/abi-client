/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { JsonValueConverter } from "./value/JsonValueConverter";
import { ScValue } from "./value/ScValue";

export class HashMap<K extends ScValue, V extends ScValue> implements Map<K, V> {
  private elements: Map<string, [K, V]>;

  constructor(entries?: Array<[K, V]>) {
    if (typeof entries === "undefined") {
      this.elements = new Map();
      this.size = 0;
    } else {
      const newEntries: Array<[string, [K, V]]> = entries.map(([k, v]) => [
        JsonValueConverter.toJsonString(k),
        [k, v],
      ]);
      this.elements = new Map<string, [K, V]>(newEntries);
      this.size = this.elements.size;
    }
  }

  readonly [Symbol.toStringTag]!: string;
  size: number;

  [Symbol.iterator](): MapIterator<[K, V]> {
    return this.elements.values();
  }

  clear(): void {
    this.elements = new Map();
    this.size = 0;
  }

  delete(key: K): boolean {
    const b = this.elements.delete(JsonValueConverter.toJsonString(key));
    this.size = this.elements.size;
    return b;
  }

  entries(): MapIterator<[K, V]> {
    return this.elements.values();
  }

  forEach(callbackfn: (value: V, key: K, map: Map<K, V>) => void): void {
    this.elements.forEach(([k, v]) => callbackfn(v, k, this));
  }

  get(key: K): V | undefined {
    const x = this.elements.get(JsonValueConverter.toJsonString(key));
    return x !== undefined ? x[1] : undefined;
  }

  has(key: K): boolean {
    return this.elements.has(JsonValueConverter.toJsonString(key));
  }

  keys(): MapIterator<K> {
    throw new Error("Not implemented");
  }

  set(key: K, value: V): this {
    this.elements.set(JsonValueConverter.toJsonString(key), [key, value]);
    this.size = this.elements.size;
    return this;
  }

  values(): MapIterator<V> {
    throw new Error("Not implemented");
  }
}
