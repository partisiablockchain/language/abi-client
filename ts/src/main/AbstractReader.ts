/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import { AbiInput } from "./abistreams/AbiInput";
import {
  AvlTreeMapTypeSpec,
  MapTypeSpec,
  NamedTypeRef,
  NamedTypeSpec,
  NumericTypeIndex,
  OptionTypeSpec,
  SetTypeSpec,
  SimpleTypeIndex,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  SizedArrayTypeSpec,
} from "./model/Abi";
import { EnumTypeSpec } from "./model/EnumTypeSpec";
import { StructTypeSpec } from "./model/StructTypeSpec";
import { ScValue } from "./value/ScValue";
import { ScValueAddress } from "./value/ScValueAddress";
import { ScValueBool } from "./value/ScValueBool";
import { ScValueEnum } from "./value/ScValueEnum";
import { ScValueMap } from "./value/ScValueMap";
import { ScValueNumber } from "./value/ScValueNumber";
import { ScValueOption } from "./value/ScValueOption";
import { ScValueSet } from "./value/ScValueSet";
import { ScValueString } from "./value/ScValueString";
import { ScValueStruct } from "./value/ScValueStruct";
import { ScValueVector } from "./value/ScValueVector";
import { ScValueHash } from "./value/ScValueHash";
import { ScValuePublicKey } from "./value/ScValuePublicKey";
import { ScValueSignature } from "./value/ScValueSignature";
import { ScValueBlsPublicKey } from "./value/ScValueBlsPublicKey";
import { ScValueBlsSignature } from "./value/ScValueBlsSignature";
import { ScValueAvlTreeMap } from "./value/ScValueAvlTreeMap";
import { ScValueArray } from "./value/ScValueArray";

export abstract class AbstractReader {
  protected readonly input: AbiInput;
  protected readonly namedTypes: NamedTypeSpec[];

  protected constructor(input: AbiInput, namedTypes: NamedTypeSpec[]) {
    this.input = input;
    this.namedTypes = namedTypes;
  }

  readGeneric(spec: TypeSpec): ScValue {
    const typeIndex = spec.typeIndex;

    if (typeIndex === TypeIndex.Map) {
      return this.readMap(spec);
    } else if (typeIndex === TypeIndex.Set) {
      return this.readSet(spec);
    } else if (typeIndex === TypeIndex.Named) {
      return this.readNamedType(spec);
    } else if (typeIndex === TypeIndex.Vec) {
      return this.readVec(spec);
    } else if (typeIndex === TypeIndex.Option) {
      return this.readOptional(spec);
    } else if (typeIndex === TypeIndex.SizedArray) {
      return this.readSizedArray(spec);
    } else if (typeIndex === TypeIndex.AvlTreeMap) {
      return this.readAvlTreeMap(spec);
    } else {
      return this.readSimpleType(typeIndex);
    }
  }

  public readSimpleType(typeIndex: SimpleTypeIndex): ScValue {
    if (typeIndex === TypeIndex.String) {
      const value = this.input.readString();
      return new ScValueString(value);
    } else if (typeIndex === TypeIndex.bool) {
      const value = this.input.readBoolean();
      return new ScValueBool(value);
    } else if (typeIndex === TypeIndex.Address) {
      const value = this.input.readBytes(ScValueAddress.ADDRESS_LENGTH);
      return new ScValueAddress(value);
    } else if (typeIndex === TypeIndex.Hash) {
      const value = this.input.readBytes(ScValueHash.HASH_LENGTH);
      return new ScValueHash(value);
    } else if (typeIndex === TypeIndex.PublicKey) {
      const value = this.input.readBytes(ScValuePublicKey.PUBLIC_KEY_LENGTH);
      return new ScValuePublicKey(value);
    } else if (typeIndex === TypeIndex.Signature) {
      const value = this.input.readBytes(ScValueSignature.SIGNATURE_LENGTH);
      return new ScValueSignature(value);
    } else if (typeIndex === TypeIndex.BlsPublicKey) {
      const value = this.input.readBytes(ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH);
      return new ScValueBlsPublicKey(value);
    } else if (typeIndex === TypeIndex.BlsSignature) {
      const value = this.input.readBytes(ScValueBlsSignature.BLS_SIGNATURE_LENGTH);
      return new ScValueBlsSignature(value);
    } else {
      return new ScValueNumber(typeIndex, this.readNumber(typeIndex));
    }
  }

  protected readNumber(typeIndex: NumericTypeIndex): BN | number {
    if (typeIndex === TypeIndex.u8) {
      return this.input.readU8();
    } else if (typeIndex === TypeIndex.i8) {
      return this.input.readI8();
    } else if (typeIndex === TypeIndex.u16) {
      return this.input.readU16();
    } else if (typeIndex === TypeIndex.i16) {
      return this.input.readI16();
    } else if (typeIndex === TypeIndex.u32) {
      return this.input.readU32();
    } else if (typeIndex === TypeIndex.i32) {
      return this.input.readI32();
    } else if (typeIndex === TypeIndex.u64) {
      return this.input.readU64();
    } else if (typeIndex === TypeIndex.i64) {
      return this.input.readI64();
    } else if (typeIndex === TypeIndex.u128) {
      return this.input.readUnsignedBigInteger(16);
    } else if (typeIndex === TypeIndex.u256) {
      return this.input.readUnsignedBigInteger(32);
    } else {
      return this.input.readSignedBigInteger(16);
    }
  }

  protected readOptional(option: OptionTypeSpec): ScValueOption {
    const hasElement = this.input.readBoolean();
    if (hasElement) {
      return new ScValueOption(this.readGeneric(option.valueType));
    } else {
      return new ScValueOption(null);
    }
  }

  protected readVec(spec: VecTypeSpec): ScValueVector {
    const length = this.input.readU32();
    const elements = this.readElements(spec.valueType, length);
    return new ScValueVector(elements);
  }

  protected readSizedArray(spec: SizedArrayTypeSpec): ScValueArray {
    const elements = this.readElements(spec.valueType, spec.length);
    return new ScValueArray(elements);
  }

  private readElements(valueType: TypeSpec, length: number): ScValue[] {
    const vec: ScValue[] = [];
    for (let i = 0; i < length; i++) {
      vec.push(this.readGeneric(valueType));
    }
    return vec;
  }

  readNamedType(spec: NamedTypeRef): ScValueStruct | ScValueEnum {
    const typeSpec = this.namedTypes[spec.index];
    if (typeSpec instanceof StructTypeSpec) {
      return this.readStruct(typeSpec as StructTypeSpec);
    } else {
      return this.readEnum(typeSpec as EnumTypeSpec);
    }
  }

  readStruct(typeSpec: StructTypeSpec) {
    const fieldsMap = new Map<string, ScValue>();
    for (const field of typeSpec.fields) {
      const value = this.readGeneric(field.type);
      fieldsMap.set(field.name, value);
    }
    return new ScValueStruct(typeSpec.name, fieldsMap);
  }

  protected readEnum(typeSpec: EnumTypeSpec): ScValueEnum {
    const discriminant = this.input.readU8();
    for (const variant of typeSpec.variants) {
      if (variant.discriminant === discriminant) {
        const typeSpec = this.namedTypes[variant.def.index];
        return new ScValueEnum(typeSpec.name, this.readStruct(typeSpec as StructTypeSpec));
      }
    }
    throw new Error(`Undefined EnumVariant ${discriminant} used in ${typeSpec.name}`);
  }

  protected abstract readMap(spec: MapTypeSpec): ScValueMap;

  protected abstract readSet(spec: SetTypeSpec): ScValueSet;

  protected abstract readAvlTreeMap(typeSpec: AvlTreeMapTypeSpec): ScValueAvlTreeMap;
}
