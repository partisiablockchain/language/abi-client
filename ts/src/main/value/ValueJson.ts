/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

export type ValueJSON = SimpleJSON | ArrayJSON | StructJSON | MapJSON | OptionJSON | AvlTreeMapJson;

export type SimpleJSON = string | number | boolean;

export type ArrayJSON = ValueJSON[];

export interface StructJSON {
  [field: string]: ValueJSON;
}

export type MapJSON = MapIndex[];

export interface MapIndex {
  key: ValueJSON;
  value: ValueJSON;
}

export type OptionJSON = OptionNone | OptionSome;

export interface OptionNone {
  isSome: false;
}

export interface OptionSome {
  isSome: true;
  innerValue: ValueJSON;
}

export interface AvlTreeMapJson {
  treeId: number;
  map?: MapJSON;
}
