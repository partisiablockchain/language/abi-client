/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValue } from "./ScValue";
import { TypeIndex } from "../model/Abi";

export class ScValueStruct extends ScValue {
  public readonly name: string;
  public readonly fieldsMap: Map<string, ScValue>;

  constructor(name: string, fieldMap: Map<string, ScValue>) {
    super();
    this.name = name;
    this.fieldsMap = fieldMap;
  }

  public getType(): TypeIndex {
    return TypeIndex.Named;
  }

  public size(): number {
    return this.fieldsMap.size;
  }

  public getFieldValue(fieldName: string): ScValue | undefined {
    return this.fieldsMap.get(fieldName);
  }

  public override structValue() {
    return this;
  }
}
