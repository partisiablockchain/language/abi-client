/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeIndex } from "../model/Abi";
import { ScValue } from "./ScValue";

export class ScValuePublicKey extends ScValue {
  public readonly value: Buffer;
  static readonly PUBLIC_KEY_LENGTH = 33;

  constructor(value: Buffer) {
    super();
    if (value.length !== ScValuePublicKey.PUBLIC_KEY_LENGTH) {
      throw new Error(
        `PublicKey expects exactly ${ScValuePublicKey.PUBLIC_KEY_LENGTH} bytes, but found ${value.length}`
      );
    }
    this.value = value;
  }

  public asString() {
    return this.value.toString("hex");
  }

  public getType() {
    return TypeIndex.PublicKey;
  }

  public override publicKeyValue(): ScValuePublicKey {
    return this;
  }
}
