/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValue } from "./ScValue";
import { HashMap } from "../HashMap";
import { TypeIndex } from "../model/Abi";

export class ScValueAvlTreeMap extends ScValue {
  public treeId: number;
  public map: HashMap<ScValue, ScValue> | null;

  constructor(treeId: number, map: HashMap<ScValue, ScValue> | null) {
    super();
    this.treeId = treeId;
    this.map = map;
  }

  getType(): TypeIndex {
    return TypeIndex.AvlTreeMap;
  }

  public mapKeysValues<K, V>(
    keyMapper: (sc: ScValue) => K,
    valueMapper: (sc: ScValue) => V
  ): Map<K, V> | undefined {
    if (this.map != null) {
      return new Map([...this.map].map(([key, value]) => [keyMapper(key), valueMapper(value)]));
    }
  }

  public avlTreeMapValue(): ScValueAvlTreeMap {
    return this;
  }
}
