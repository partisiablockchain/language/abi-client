/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValue } from "./ScValue";
import { TypeIndex } from "../model/Abi";
import { ScValueNumber } from "./ScValueNumber";

export class ScValueArray extends ScValue {
  private readonly vals: ScValue[];

  constructor(values: ScValue[]) {
    super();
    this.vals = values;
  }

  public static fromBytes(bytes: Buffer): ScValueArray {
    const elements: ScValue[] = [];
    for (const byte of bytes) {
      elements.push(new ScValueNumber(TypeIndex.u8, byte));
    }
    return new ScValueArray(elements);
  }

  public size(): number {
    return this.vals.length;
  }

  public isEmpty(): boolean {
    return this.vals.length === 0;
  }

  public get(index: number): ScValue {
    return this.vals[index];
  }

  public getType(): TypeIndex {
    return TypeIndex.SizedArray;
  }

  public override arrayValue(): ScValueArray {
    return this;
  }

  public values(): ScValue[] {
    return this.vals;
  }

  public override vecU8Value(): Buffer {
    if (this.size() === 0) {
      return Buffer.from([]);
    } else if (this.get(0).getType() !== TypeIndex.u8) {
      throw new Error("Cannot read SizedArray u8 for current type");
    } else {
      return Buffer.from(this.vals.map((sc) => sc.asNumber()));
    }
  }
}
