/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeIndex } from "../model/Abi";
import { ScValueEnum } from "./ScValueEnum";
import { ScValueStruct } from "./ScValueStruct";
import { ScValueVector } from "./ScValueVector";
import { ScValueSet } from "./ScValueSet";
import { ScValueOption } from "./ScValueOption";
import { ScValueMap } from "./ScValueMap";
import { ScValueAddress } from "./ScValueAddress";
import BN from "bn.js";
import { ScValueHash } from "./ScValueHash";
import { ScValuePublicKey } from "./ScValuePublicKey";
import { ScValueSignature } from "./ScValueSignature";
import { ScValueBlsPublicKey } from "./ScValueBlsPublicKey";
import { ScValueBlsSignature } from "./ScValueBlsSignature";
import { ScValueAvlTreeMap } from "./ScValueAvlTreeMap";
import { ScValueArray } from "./ScValueArray";

export abstract class ScValue {
  abstract getType(): TypeIndex;

  public typeName(): string {
    return TypeIndex[this.getType()];
  }

  public structValue(): ScValueStruct {
    throw new Error(`Cannot read Struct for type: ${this.typeName()}`);
  }

  public vecValue(): ScValueVector {
    throw new Error(`Cannot read Vector for type: ${this.typeName()}`);
  }

  public setValue(): ScValueSet {
    throw new Error(`Cannot read Set for type: ${this.typeName()}`);
  }

  public optionValue(): ScValueOption {
    throw new Error(`Cannot read Option for type: ${this.typeName()}`);
  }

  public addressValue(): ScValueAddress {
    throw new Error(`Cannot read Address for type: ${this.typeName()}`);
  }

  public hashValue(): ScValueHash {
    throw new Error(`Cannot read Hash for type: ${this.typeName()}`);
  }

  public publicKeyValue(): ScValuePublicKey {
    throw new Error(`Cannot read PublicKey for type: ${this.typeName()}`);
  }

  public signatureValue(): ScValueSignature {
    throw new Error(`Cannot read Signature for type: ${this.typeName()}`);
  }

  public blsPublicKeyValue(): ScValueBlsPublicKey {
    throw new Error(`Cannot read BlsPublicKey for type: ${this.typeName()}`);
  }

  public blsSignatureValue(): ScValueBlsSignature {
    throw new Error(`Cannot read BlsSignature for type: ${this.typeName()}`);
  }

  public mapValue(): ScValueMap {
    throw new Error(`Cannot read Map for type: ${this.typeName()}`);
  }

  public enumValue(): ScValueEnum {
    throw new Error(`Cannot read Enum for type: ${this.typeName()}`);
  }

  public vecU8Value(): Buffer {
    throw new Error(`Cannot read Vec u8 for type: ${this.typeName()}`);
  }

  public boolValue(): boolean {
    throw new Error(`Cannot read bool for type: ${this.typeName()}`);
  }

  public stringValue(): string {
    throw new Error(`Cannot read String for type: ${this.typeName()}`);
  }

  public asNumber(): number {
    throw new Error(`Cannot read integer for type: ${this.typeName()}`);
  }

  public asBN(): BN {
    throw new Error(`Cannot read BN for type: ${this.typeName()}`);
  }

  public avlTreeMapValue(): ScValueAvlTreeMap {
    throw new Error(`Cannot read AvlTreeMap for type: ${this.typeName()}`);
  }

  public arrayValue(): ScValueArray {
    throw new Error(`Cannot read array for type: ${this.typeName()}`);
  }
}
