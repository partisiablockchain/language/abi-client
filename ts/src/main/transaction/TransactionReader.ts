/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { AbiByteInput } from "../abistreams/AbiByteInput";
import { AbstractReader } from "../AbstractReader";
import { AbiParser } from "../parser/AbiParser";
import { ContractAbi } from "../model/ContractAbi";
import { TRANSACTION_AND_EVENT_ABI } from "../TransactionAndEventDeserialization";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { ScValueMap } from "../value/ScValueMap";
import { ScValueSet } from "../value/ScValueSet";
import { ScValueStruct } from "../value/ScValueStruct";
import { ScValueAvlTreeMap } from "../value/ScValueAvlTreeMap";

export class TransactionReader extends AbstractReader {
  private readonly contract: ContractAbi;
  private static readonly CONTRACT_ABI = new AbiParser(TRANSACTION_AND_EVENT_ABI).parseAbi()
    .chainComponent as ContractAbi;

  constructor(input: BigEndianByteInput | Buffer, contract: ContractAbi) {
    let byteInput;
    if ("length" in input) {
      byteInput = new BigEndianByteInput(input);
    } else {
      byteInput = new BigEndianByteInput(input.readRemaining());
    }
    super(new AbiByteInput(byteInput), contract.namedTypes);
    this.contract = contract;
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in rpc");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in rpc");
  }

  protected readAvlTreeMap(): ScValueAvlTreeMap {
    throw new Error("Type AvlTreeMap is not supported in rpc");
  }

  checkAllBytesRead() {
    const remaining = this.input.readRemaining();
    if (remaining.length != 0) {
      throw new Error("The input wasn't fully read, " + remaining.length + " remaining bytes.");
    }
  }

  static deserializeEventPayload(payload: Buffer): ScValueStruct {
    const reader: TransactionReader = new TransactionReader(
      payload,
      TransactionReader.CONTRACT_ABI
    );

    const executableEvent = reader.contract.namedTypes.find(
      (namedType) => namedType.name === "ExecutableEvent"
    ) as StructTypeSpec;
    const res = reader.readStruct(executableEvent);
    reader.checkAllBytesRead();
    return res;
  }

  static deserializeTransactionPayload(payload: Buffer): ScValueStruct {
    const reader: TransactionReader = new TransactionReader(
      payload,
      TransactionReader.CONTRACT_ABI
    );

    const signedTransaction = reader.contract.namedTypes.find(
      (namedType) => namedType.name === "SignedTransaction"
    ) as StructTypeSpec;
    const res = reader.readStruct(signedTransaction);
    reader.checkAllBytesRead();
    return res;
  }

  public static getContractAbi(): ContractAbi {
    return this.CONTRACT_ABI;
  }
}
