/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractBuilder } from "../builder/AbstractBuilder";
import { ContractAbi } from "../model/ContractAbi";
import { AggregateProducer } from "../model/AggregateProducer";
import { AbiParser } from "../parser/AbiParser";
import { TRANSACTION_AND_EVENT_ABI } from "../TransactionAndEventDeserialization";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { StructProducer } from "../builder/StructProducer";
import { AbiByteOutput } from "../abistreams/AbiByteOutput";

export class TransactionBuilder extends AbstractBuilder {
  private constructor(contractAbi: ContractAbi, aggregateProducer: AggregateProducer) {
    super(contractAbi.namedTypes, "", aggregateProducer);
  }

  public static createTransactionBuilder(): TransactionBuilder {
    const transactionAbi = new AbiParser(TRANSACTION_AND_EVENT_ABI).parseAbi()
      .chainComponent as ContractAbi;
    const signedTransaction = transactionAbi.namedTypes.find(
      (namedType) => namedType.name === "SignedTransaction"
    ) as StructTypeSpec;
    return new TransactionBuilder(transactionAbi, new StructProducer(signedTransaction, "root"));
  }

  public static createEventBuilder(): TransactionBuilder {
    const transactionAbi = new AbiParser(TRANSACTION_AND_EVENT_ABI).parseAbi()
      .chainComponent as ContractAbi;
    const signedTransaction = transactionAbi.namedTypes.find(
      (namedType) => namedType.name === "ExecutableEvent"
    ) as StructTypeSpec;
    return new TransactionBuilder(transactionAbi, new StructProducer(signedTransaction, "root"));
  }

  public getBytes(): Buffer {
    return AbiByteOutput.serializeBigEndian((out) => this.getAggregateProducer().write(out));
  }
}
