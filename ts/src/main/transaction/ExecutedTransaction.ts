/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

export interface ExecutedTransaction {
  transactionPayload: string;
  block: string;
  productionTime: number;
  blockTime: number;
  from: string | undefined;
  identifier: string;
  executionSucceeded: boolean;
  events: EventDto[];
  finalized: boolean;
  isEvent: boolean;
  failureCause: FailureCauseDto | undefined;
  transactionCost?: TransactionCost;
}

export interface EventDto {
  identifier: string;
  destinationShard: string | null;
}

export interface FailureCauseDto {
  errorMessage: string;
  stackTrace: string;
}

export interface TransactionCost {
  allocatedForEvents: number;
  cpu: number;
  remaining: number;
  paidByContract: number;
  networkFees: NetworkFees;
}

export interface NetworkFees {
  [key: string]: number;
}
