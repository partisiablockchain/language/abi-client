/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Producer } from "../model/Producer";
import { ScValuePublicKey } from "../value/ScValuePublicKey";
import { AbiOutput } from "../abistreams/AbiOutput";

export class PublicKeyProducer implements Producer {
  private readonly publicKey: Buffer;

  constructor(publicKey: Buffer, errorPath: string) {
    if (publicKey.length !== ScValuePublicKey.PUBLIC_KEY_LENGTH) {
      throw new Error(
        `In ${errorPath}, Public key must have length ${
          ScValuePublicKey.PUBLIC_KEY_LENGTH
        } bytes, got length = ${publicKey.length}, value = ${publicKey.toString("hex")}`
      );
    }
    this.publicKey = publicKey;
  }

  public write(out: AbiOutput): void {
    out.writeBytes(this.publicKey);
  }
}
