/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Producer } from "../model/Producer";
import { ScValueHash } from "../value/ScValueHash";
import { AbiOutput } from "../abistreams/AbiOutput";

export class HashProducer implements Producer {
  private readonly hash: Buffer;

  constructor(hash: Buffer, errorPath: string) {
    if (hash.length !== ScValueHash.HASH_LENGTH) {
      throw new Error(
        `In ${errorPath}, Hash must have length ${ScValueHash.HASH_LENGTH} bytes, got length = ${
          hash.length
        }, value = ${hash.toString("hex")}`
      );
    }
    this.hash = hash;
  }

  public write(out: AbiOutput): void {
    out.writeBytes(this.hash);
  }
}
