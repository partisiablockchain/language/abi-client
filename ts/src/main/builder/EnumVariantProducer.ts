/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeSpec } from "../model/Abi";
import { Producer } from "../model/Producer";
import { StructProducer } from "./StructProducer";
import { AggregateProducer } from "../model/AggregateProducer";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { AbiOutput } from "../abistreams/AbiOutput";

export class EnumVariantProducer implements AggregateProducer {
  private readonly structProducer: StructProducer;
  private readonly discriminant: number;
  private readonly variantName: string;

  constructor(variantType: StructTypeSpec | null, discriminant: number, errorPath: string) {
    if (variantType == null) {
      this.variantName = "";
    } else {
      this.variantName = variantType.name;
    }

    this.structProducer = new StructProducer(variantType, errorPath);
    this.discriminant = discriminant;
  }

  public write(out: AbiOutput): void {
    out.writeU8(this.discriminant);
    this.structProducer.write(out);
  }

  public addElement(argument: Producer) {
    this.structProducer.addElement(argument);
  }

  public getTypeSpecForElement(): TypeSpec | null {
    return this.structProducer.getTypeSpecForElement();
  }

  public getFieldName(): string {
    return "/" + this.variantName + this.structProducer.getFieldName();
  }
}
