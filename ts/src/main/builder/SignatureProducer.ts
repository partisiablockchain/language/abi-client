/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Producer } from "../model/Producer";
import { ScValueSignature } from "../value/ScValueSignature";
import { AbiOutput } from "../abistreams/AbiOutput";

export class SignatureProducer implements Producer {
  private readonly signature: Buffer;

  constructor(signature: Buffer, errorPath: string) {
    if (signature.length !== ScValueSignature.SIGNATURE_LENGTH) {
      throw new Error(
        `In ${errorPath}, Signature must have length ${
          ScValueSignature.SIGNATURE_LENGTH
        } bytes, got length = ${signature.length}, value = ${signature.toString("hex")}`
      );
    }
    this.signature = signature;
  }

  public write(out: AbiOutput): void {
    out.writeBytes(this.signature);
  }
}
