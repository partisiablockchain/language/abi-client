/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import { EnumTypeSpec, NamedTypeSpec, StructTypeSpec } from "../index";
import {
  MapTypeSpec,
  NamedTypeRef,
  OptionTypeSpec,
  SizedArrayTypeSpec,
  TypeSpec,
  TypeIndex,
  VecTypeSpec,
} from "../model/Abi";
import { Producer } from "../model/Producer";
import { BlsPublicKeyProducer } from "./BlsPublicKeyProducer";
import { HashProducer } from "./HashProducer";
import { PublicKeyProducer } from "./PublicKeyProducer";
import { SignatureProducer } from "./SignatureProducer";
import { BlsSignatureProducer } from "./BlsSignatureProducer";
import { AggregateProducer } from "../model/AggregateProducer";
import { AddressProducer } from "./AddressProducer";
import { StructProducer } from "./StructProducer";
import { EnumVariantProducer } from "./EnumVariantProducer";
import { VecProducer } from "./VecProducer";
import { MapProducer } from "./MapProducer";
import { OptionProducer } from "./OptionProducer";
import { SizedArrayProducer } from "./SizedArrayProducer";
import { AbiOutput } from "../abistreams/AbiOutput";

export abstract class AbstractBuilder {
  private readonly namedTypes: NamedTypeSpec[] | null;
  private readonly errorPath: string;
  private readonly aggregateProducer: AggregateProducer;

  protected constructor(
    namedTypes: NamedTypeSpec[] | null,
    errorPath: string,
    aggregateProducer: AggregateProducer
  ) {
    this.namedTypes = namedTypes;
    this.errorPath = errorPath;
    this.aggregateProducer = aggregateProducer;
  }

  public addBool(value: boolean): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeBoolean(value) }, TypeIndex.bool);
    return this;
  }

  public addString(value: string): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeString(value) }, TypeIndex.String);
    return this;
  }

  public addAddress(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new AddressProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.Address
    );
    return this;
  }

  public addHash(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new HashProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.Hash
    );
    return this;
  }

  public addPublicKey(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new PublicKeyProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.PublicKey
    );
    return this;
  }

  public addSignature(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new SignatureProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.Signature
    );
    return this;
  }

  public addBlsPublicKey(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new BlsPublicKeyProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.BlsPublicKey
    );
    return this;
  }

  public addBlsSignature(value: Buffer | string): AbstractBuilder {
    let val: Buffer;
    if (typeof value === "string") {
      val = Buffer.from(value, "hex");
    } else {
      val = value;
    }
    this.typeCheckAndAddArg(
      new BlsSignatureProducer(val, this.errorPath + this.aggregateProducer.getFieldName()),
      TypeIndex.BlsSignature
    );
    return this;
  }

  public addU8(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeU8(value) }, TypeIndex.u8);
    return this;
  }

  public addU16(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeU16(value) }, TypeIndex.u16);
    return this;
  }

  public addU32(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeU32(value) }, TypeIndex.u32);
    return this;
  }

  public addU64(value: BN | number): AbstractBuilder {
    let value2: BN;
    if (typeof value === "number") {
      value2 = new BN(value);
    } else {
      value2 = value;
    }
    this.typeCheckAndAddArg({ write: (out) => out.writeU64(value2) }, TypeIndex.u64);
    return this;
  }

  public addU128(value: BN): AbstractBuilder {
    this.typeCheckAndAddArg(
      { write: (out) => out.writeUnsignedBigInteger(value, 16) },
      TypeIndex.u128
    );
    return this;
  }

  public addU256(value: BN): AbstractBuilder {
    this.typeCheckAndAddArg(
      { write: (out) => out.writeUnsignedBigInteger(value, 32) },
      TypeIndex.u256
    );
    return this;
  }

  public addI8(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeI8(value) }, TypeIndex.i8);
    return this;
  }

  public addI16(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeI16(value) }, TypeIndex.i16);
    return this;
  }

  public addI32(value: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeI32(value) }, TypeIndex.i32);
    return this;
  }

  public addI64(value: BN | number): AbstractBuilder {
    let value2: BN;
    if (typeof value === "number") {
      value2 = new BN(value);
    } else {
      value2 = value;
    }
    this.typeCheckAndAddArg({ write: (out) => out.writeI64(value2) }, TypeIndex.i64);
    return this;
  }

  public addI128(value: BN): AbstractBuilder {
    this.typeCheckAndAddArg(
      { write: (out) => out.writeSignedBigInteger(value, 16) },
      TypeIndex.i128
    );
    return this;
  }

  public addStruct(): AbstractBuilder {
    const struct = this.aggregateProducer.getTypeSpecForElement();
    this.typeCheck(TypeIndex.Named);
    let structType = null;
    if (this.namedTypes != null) {
      structType = this.namedTypes[(struct as NamedTypeRef).index] as StructTypeSpec;
    }
    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
    const structProducer = new StructProducer(structType, newErrorPath);
    this.aggregateProducer.addElement(structProducer);
    return new AggregateBuilder(this.namedTypes, newErrorPath, structProducer);
  }

  public addEnumVariant(discriminant: number): AggregateBuilder {
    const enumTypeRef = this.aggregateProducer.getTypeSpecForElement();
    this.typeCheck(TypeIndex.Named);
    const structRef = this.getEnumVariantTypeRef(enumTypeRef as NamedTypeRef, discriminant);
    let structTypeSpec;
    if (this.namedTypes == null || structRef == null) {
      structTypeSpec = null;
    } else {
      structTypeSpec = this.namedTypes[structRef.index] as StructTypeSpec;
    }
    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
    const enumVariantProducer = new EnumVariantProducer(structTypeSpec, discriminant, newErrorPath);
    this.aggregateProducer.addElement(enumVariantProducer);
    return new AggregateBuilder(this.namedTypes, newErrorPath, enumVariantProducer);
  }

  public addVec(): AggregateBuilder {
    const vec = this.aggregateProducer.getTypeSpecForElement();
    this.typeCheck(TypeIndex.Vec);

    const vecProducer = new VecProducer(vec as VecTypeSpec | null);
    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
    this.aggregateProducer.addElement(vecProducer);
    return new AggregateBuilder(this.namedTypes, newErrorPath, vecProducer);
  }

  public addVecU8(values: Buffer): AbstractBuilder {
    this.typeCheck(TypeIndex.Vec);
    const vecType = this.aggregateProducer.getTypeSpecForElement() as VecTypeSpec | null;

    if (vecType !== null) {
      const expectedType = vecType.valueType.typeIndex;
      if (expectedType !== TypeIndex.u8) {
        throw new Error(
          `In ${this.errorPath + this.aggregateProducer.getFieldName()}, Expected type Vec<${
            TypeIndex[expectedType]
          }>, but got Vec<u8>`
        );
      }
    }
    this.aggregateProducer.addElement({
      write: (out) => {
        out.writeI32(values.length);
        out.writeBytes(values);
      },
    });
    return this;
  }

  public addMap(): AggregateBuilder {
    const map = this.aggregateProducer.getTypeSpecForElement();
    this.typeCheck(TypeIndex.Map);

    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
    const mapProducer = new MapProducer(map as MapTypeSpec, newErrorPath);
    this.aggregateProducer.addElement(mapProducer);
    return new AggregateBuilder(this.namedTypes, newErrorPath, mapProducer);
  }

  public addSet(): AggregateBuilder {
    return this.addVec();
  }

  public addOption(): AggregateBuilder {
    const optionType = this.aggregateProducer.getTypeSpecForElement();
    this.typeCheck(TypeIndex.Option);

    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
    const optionProducer = new OptionProducer(optionType as OptionTypeSpec | null, newErrorPath);
    this.aggregateProducer.addElement(optionProducer);
    return new AggregateBuilder(this.namedTypes, newErrorPath, optionProducer);
  }

  public addSizedByteArray(values: Buffer): AbstractBuilder {
    const arrayBuilder: AbstractBuilder = this.addSizedArray();

    for (const v of values) {
      arrayBuilder.addU8(v);
    }
    return arrayBuilder;
  }

  /**
   * Adds a sized array argument to the builder.
   *
   * @param values list of values for the array
   * @return the updated builder
   */
  public addSizedArray(): AbstractBuilder {
    const spec = this.typeCheck(TypeIndex.SizedArray);

    const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();

    const arrayProducer = new SizedArrayProducer(spec as SizedArrayTypeSpec, newErrorPath);
    const fieldName = this.aggregateProducer.getFieldName();
    this.aggregateProducer.addElement(arrayProducer);
    return new AggregateBuilder(this.namedTypes, this.errorPath + fieldName, arrayProducer);
  }

  public addAvlTreeMap(treeId: number): AbstractBuilder {
    this.typeCheckAndAddArg({ write: (out) => out.writeI32(treeId) }, TypeIndex.AvlTreeMap);
    return this;
  }

  typeCheckAndAddArg(argument: Producer, expected: TypeIndex): void {
    this.typeCheck(expected);
    this.aggregateProducer.addElement(argument);
  }

  typeCheck(actualType: TypeIndex): TypeSpec | null {
    const typeSpecForArgument = this.aggregateProducer.getTypeSpecForElement();
    if (typeSpecForArgument != null) {
      const expectedType = typeSpecForArgument.typeIndex;
      if (expectedType !== actualType) {
        throw new Error(
          `In ${this.errorPath + this.aggregateProducer.getFieldName()}, Expected type ${
            TypeIndex[expectedType]
          }, but got ${TypeIndex[actualType]}`
        );
      }
    }
    return typeSpecForArgument;
  }

  public write(out: AbiOutput) {
    this.aggregateProducer.write(out);
  }

  private getEnumVariantTypeRef(
    enumTypeRef: NamedTypeRef | null,
    discriminant: number
  ): NamedTypeRef | null {
    if (this.namedTypes == null || enumTypeRef == null) {
      return null;
    } else {
      const enumTypeSpec = this.namedTypes[enumTypeRef.index] as EnumTypeSpec;
      const variant = enumTypeSpec.variant(discriminant);
      if (variant === null) {
        const newErrorPath = this.errorPath + this.aggregateProducer.getFieldName();
        throw new Error(
          `In ${newErrorPath}, Undefined variant discriminant ${discriminant} for ${enumTypeSpec.name}`
        );
      } else {
        return variant.def;
      }
    }
  }

  protected getAggregateProducer(): AggregateProducer {
    return this.aggregateProducer;
  }
}

class AggregateBuilder extends AbstractBuilder {
  constructor(
    namedTypes: NamedTypeSpec[] | null,
    errorPath: string,
    aggregateProducer: AggregateProducer
  ) {
    super(namedTypes, errorPath, aggregateProducer);
  }
}
