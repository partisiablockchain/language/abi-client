/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { MapTypeSpec, TypeSpec } from "../model/Abi";
import { Producer } from "../model/Producer";
import { AggregateProducer } from "../model/AggregateProducer";
import { AbiOutput } from "../abistreams/AbiOutput";

export class MapProducer implements AggregateProducer {
  private readonly mapTypeSpec: MapTypeSpec | null;
  private readonly elements: Producer[] = [];
  private readonly errorPath: string;

  constructor(mapTypeSpec: MapTypeSpec | null, errorPath: string) {
    this.mapTypeSpec = mapTypeSpec;
    this.errorPath = errorPath;
  }

  public write(out: AbiOutput): void {
    if (this.elements.length % 2 === 1) {
      throw new Error(`In ${this.errorPath}, Missing value for key`);
    }
    out.writeI32(this.size());
    for (const element of this.elements) {
      element.write(out);
    }
  }

  public addElement(argument: Producer) {
    this.elements.push(argument);
  }

  public getTypeSpecForElement(): TypeSpec | null {
    if (this.mapTypeSpec !== null) {
      if (this.elements.length % 2 == 0) {
        return this.mapTypeSpec.keyType;
      } else {
        return this.mapTypeSpec.valueType;
      }
    }
    return null;
  }

  private size(): number {
    return Math.trunc((this.elements.length + 1) / 2);
  }

  getFieldName(): string {
    const index = Math.trunc(this.elements.length / 2);
    if (this.elements.length % 2 == 0) {
      return "/" + index + "/key";
    } else {
      return "/" + index + "/value";
    }
  }
}
