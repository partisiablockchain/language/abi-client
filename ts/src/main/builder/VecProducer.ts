/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeSpec, VecTypeSpec } from "../model/Abi";
import { Producer } from "../model/Producer";
import { AggregateProducer } from "../model/AggregateProducer";
import { AbiOutput } from "../abistreams/AbiOutput";

export class VecProducer implements AggregateProducer {
  private readonly vec: VecTypeSpec | null;
  private readonly elements: Producer[] = [];

  constructor(vec: VecTypeSpec | null) {
    this.vec = vec;
  }

  public write(out: AbiOutput): void {
    out.writeI32(this.elements.length);
    for (const element of this.elements) {
      element.write(out);
    }
  }

  public addElement(argument: Producer) {
    this.elements.push(argument);
  }

  public getTypeSpecForElement(): TypeSpec | null {
    if (this.vec !== null) {
      return this.vec.valueType;
    }
    return null;
  }

  getFieldName(): string {
    return "/" + this.elements.length;
  }
}
