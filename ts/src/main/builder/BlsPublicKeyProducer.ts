/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Producer } from "../model/Producer";
import { ScValueBlsPublicKey } from "../value/ScValueBlsPublicKey";
import { AbiOutput } from "../abistreams/AbiOutput";

export class BlsPublicKeyProducer implements Producer {
  private readonly blsPublicKey: Buffer;

  constructor(blsPublicKey: Buffer, errorPath: string) {
    if (blsPublicKey.length !== ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH) {
      throw new Error(
        `In ${errorPath}, Bls public key must have length ${
          ScValueBlsPublicKey.BLS_PUBLIC_KEY_LENGTH
        } bytes, got length = ${blsPublicKey.length}, value = ${blsPublicKey.toString("hex")}`
      );
    }
    this.blsPublicKey = blsPublicKey;
  }

  public write(out: AbiOutput): void {
    out.writeBytes(this.blsPublicKey);
  }
}
