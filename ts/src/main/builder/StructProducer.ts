/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TypeSpec } from "../model/Abi";
import { Producer } from "../model/Producer";
import { StructTypeSpec } from "../model/StructTypeSpec";
import { AggregateProducer } from "../model/AggregateProducer";
import { AbiOutput } from "../abistreams/AbiOutput";

export class StructProducer implements AggregateProducer {
  private readonly structTypeSpec: StructTypeSpec | null;
  private readonly elements: Producer[] = [];
  private readonly errorPath: string;

  constructor(structTypeSpec: StructTypeSpec | null, errorPath: string) {
    this.structTypeSpec = structTypeSpec;
    this.errorPath = errorPath;
  }

  public write(out: AbiOutput): void {
    for (const element of this.elements) {
      element.write(out);
    }
    this.validate();
  }

  public addElement(argument: Producer) {
    this.elements.push(argument);
  }

  public getTypeSpecForElement(): TypeSpec | null {
    if (this.structTypeSpec !== null) {
      const lastIndex = this.elements.length;
      if (lastIndex >= this.structTypeSpec.fields.length) {
        throw new Error(
          `In ${this.errorPath}, Cannot add more arguments than the struct has fields.`
        );
      }
      return this.structTypeSpec.fields[lastIndex].type;
    }
    return null;
  }

  private validate() {
    if (
      this.structTypeSpec !== null &&
      this.elements.length !== this.structTypeSpec.fields.length
    ) {
      const missingArgument = this.structTypeSpec.fields[this.elements.length].name;
      throw new Error(`In ${this.errorPath}, Missing argument '${missingArgument}'`);
    }
  }

  getFieldName(): string {
    if (this.structTypeSpec !== null) {
      const lastIndex = this.elements.length;
      return "/" + this.structTypeSpec.fields[lastIndex].name;
    }
    return "";
  }
}
