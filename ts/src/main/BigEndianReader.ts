/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiByteInput } from "./abistreams/AbiByteInput";
import { AbstractReader } from "./AbstractReader";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { TypeSpec } from "./model/Abi";
import { ScValueMap } from "./value/ScValueMap";
import { ScValueSet } from "./value/ScValueSet";
import { ContractAbi } from "./model/ContractAbi";
import { ScValue } from "./value/ScValue";
import { ScValueAvlTreeMap } from "./value/ScValueAvlTreeMap";

export class BigEndianReader extends AbstractReader {
  constructor(input: Buffer, contract: ContractAbi) {
    super(new AbiByteInput(new BigEndianByteInput(input)), contract.namedTypes);
  }

  public read(type: TypeSpec): ScValue {
    return this.readGeneric(type);
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in rpc");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in rpc");
  }

  protected readAvlTreeMap(): ScValueAvlTreeMap {
    throw new Error("Type AvlTreeMap is not supported in rpc");
  }
}
