/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BitInput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import { AbiBitInput } from "../abistreams/AbiBitInput";
import { AbstractReader } from "../AbstractReader";
import { ContractAbi } from "../model/ContractAbi";
import { TypeSpec } from "../model/Abi";
import { ScValue } from "../value/ScValue";
import { ScValueAvlTreeMap } from "../value/ScValueAvlTreeMap";
import { ScValueMap } from "../value/ScValueMap";
import { ScValueSet } from "../value/ScValueSet";

export class ZkSecretReader extends AbstractReader {
  constructor(input: BitInput | CompactBitArray, contractAbi: ContractAbi) {
    if (input instanceof BitInput) {
      super(new AbiBitInput(input), contractAbi.namedTypes);
    } else {
      super(new AbiBitInput(new BitInput(input.data)), contractAbi.namedTypes);
    }
  }

  public readSecret(type: TypeSpec): ScValue {
    return this.readGeneric(type);
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in secret variables");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in secret variables");
  }

  protected readAvlTreeMap(): ScValueAvlTreeMap {
    throw new Error("Type AvlTreeMap is not supported in secret variables");
  }
}
