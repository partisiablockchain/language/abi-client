/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "./BlockchainAddress";
import { Entry } from "./types";
import {
  ChainControllerApi,
  Configuration,
} from "@partisiablockchain/blockchain-api-transaction-client";

export interface BlockchainStateClient {
  getContractStateBinary(address: BlockchainAddress): Promise<Buffer>;

  getContractStateAvlValue(
    address: BlockchainAddress,
    treeId: number,
    key: Buffer
  ): Promise<Buffer | undefined>;

  getContractStateAvlNextN(
    address: BlockchainAddress,
    treeId: number,
    key: Buffer | undefined,
    n: number
  ): Promise<Array<Entry<Buffer, Buffer>>>;

  getContractStateAvlSize(address: BlockchainAddress, treeId: number): Promise<number>;
}

export class BlockchainStateClientImpl implements BlockchainStateClient {
  private readonly client: ChainControllerApi;

  private constructor(client: ChainControllerApi) {
    this.client = client;
  }

  public static create(basePath: string): BlockchainStateClientImpl;
  public static create(client: ChainControllerApi): BlockchainStateClientImpl;
  public static create(arg: ChainControllerApi | string): BlockchainStateClientImpl {
    let client;
    if (typeof arg === "string") {
      client = new ChainControllerApi(new Configuration({ basePath: arg }));
    } else {
      client = arg;
    }
    return new BlockchainStateClientImpl(client);
  }

  async getContractStateAvlNextN(
    address: BlockchainAddress,
    treeId: number,
    key: Buffer | undefined,
    n: number
  ): Promise<Array<Entry<Buffer, Buffer>>> {
    const next = await this.client.getContractAvlNextN({
      address: address.asString(),
      treeId,
      key: key?.toString("hex"),
      n,
    });
    return next.map((entry) => {
      return { key: Buffer.from(entry.key, "hex"), value: Buffer.from(entry.value, "base64") };
    });
  }

  async getContractStateAvlSize(address: BlockchainAddress, treeId: number): Promise<number> {
    return (await this.client.getContractAvlInformation({ address: address.asString(), treeId }))
      .size;
  }

  async getContractStateAvlValue(
    address: BlockchainAddress,
    treeId: number,
    key: Buffer
  ): Promise<Buffer | undefined> {
    const avlValue = await this.client
      .getContractAvlValue({ address: address.asString(), treeId, key: key.toString("hex") })
      .catch(() => undefined);
    return avlValue == undefined ? undefined : Buffer.from(avlValue.data, "base64");
  }

  async getContractStateBinary(address: BlockchainAddress): Promise<Buffer> {
    const contract = await this.client.getContract({ address: address.asString() });
    if (contract.serializedContract === undefined) {
      throw new Error("Contract does not have any state");
    }
    return Buffer.from(contract.serializedContract, "base64");
  }
}
