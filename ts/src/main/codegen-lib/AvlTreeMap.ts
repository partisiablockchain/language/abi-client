/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Entry } from "./types";
import { BlockchainAddress } from "./BlockchainAddress";
import { BlockchainStateClient } from "./BlockchainStateClient";

export class AvlTreeMap<K, V> {
  public readonly treeId: number;
  private readonly client: BlockchainStateClient | undefined;
  private readonly address: BlockchainAddress | undefined;
  private readonly keySerializer: ((key: K) => Buffer) | undefined;
  private readonly keyDeserializer: ((bytes: Buffer) => K) | undefined;
  private readonly valueDeserializer: ((bytes: Buffer) => V) | undefined;

  constructor(treeId: number);
  constructor(
    treeId: number,
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined,
    keySerializer: (key: K) => Buffer,
    keyDeserializer: (bytes: Buffer) => K,
    valueDeserializer: (bytes: Buffer) => V
  );
  constructor(
    treeId: number,
    client?: BlockchainStateClient,
    address?: BlockchainAddress,
    keySerializer?: (key: K) => Buffer,
    keyDeserializer?: (bytes: Buffer) => K,
    valueDeserializer?: (bytes: Buffer) => V
  ) {
    this.client = client;
    this.address = address;
    this.treeId = treeId;
    this.keySerializer = keySerializer;
    this.keyDeserializer = keyDeserializer;
    this.valueDeserializer = valueDeserializer;
  }

  public async get(key: K): Promise<V | undefined> {
    if (
      this.client === undefined ||
      this.address === undefined ||
      this.keySerializer === undefined ||
      this.keyDeserializer === undefined ||
      this.valueDeserializer === undefined
    ) {
      throw new Error("Tried to get avl state with no client");
    }
    const keyBytes = this.keySerializer(key);

    const valueDeserializer = this.valueDeserializer;
    return this.client
      .getContractStateAvlValue(this.address, this.treeId, keyBytes)
      .then((valueBytes: Buffer | undefined) =>
        valueBytes == undefined ? undefined : valueDeserializer(valueBytes)
      );
  }

  public async getNextN(key: K | undefined, n: number): Promise<Array<Entry<K, V>>> {
    if (
      this.client === undefined ||
      this.address === undefined ||
      this.keySerializer === undefined ||
      this.keyDeserializer === undefined ||
      this.valueDeserializer === undefined
    ) {
      throw new Error("Tried to get avl state with no client");
    }
    const keyBytes = key === undefined ? undefined : this.keySerializer(key);
    const nextN = await this.client.getContractStateAvlNextN(
      this.address,
      this.treeId,
      keyBytes,
      n
    );
    const keyDeserializer = this.keyDeserializer;
    const valueDeserializer = this.valueDeserializer;
    return nextN.map(({ key, value }: Entry<Buffer, Buffer>) => {
      return { key: keyDeserializer(key), value: valueDeserializer(value) };
    });
  }

  public async size(): Promise<number> {
    if (this.client === undefined || this.address === undefined) {
      throw new Error("Tried to get avl state with no client");
    }
    return this.client.getContractStateAvlSize(this.address, this.treeId);
  }

  public deserializeKey(bytes: Buffer): K {
    const deserializer = this.keyDeserializer as (bytes: Buffer) => K;
    return deserializer(bytes);
  }

  public deserializeValue(bytes: Buffer): V {
    const deserializer = this.valueDeserializer as (bytes: Buffer) => V;
    return deserializer(bytes);
  }

  public serializeKey(key: K): Buffer {
    const serializer = this.keySerializer as (key: K) => Buffer;
    return serializer(key);
  }
}
