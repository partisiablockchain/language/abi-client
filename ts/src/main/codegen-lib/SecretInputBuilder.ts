/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { CompactBitArray } from "@secata-public/bitmanipulation-ts";
import { SecretInput } from "./types";

/** Codegen builder for creating secret inputs. */
export class SecretInputBuilder<SecretT> {
  private readonly publicRpc: Buffer;
  private readonly secretSerializer: (secret: SecretT) => CompactBitArray;

  /**
   * Create secret input builder. Called by the codegenned code.
   *
   * @param publicRpc the public rpc of the secret input function
   * @param secretSerializer function for serializing the secret input
   */
  constructor(publicRpc: Buffer, secretSerializer: (secret: SecretT) => CompactBitArray) {
    this.publicRpc = publicRpc;
    this.secretSerializer = secretSerializer;
  }

  /**
   * Add the secret input.
   *
   * @param secretInput the secret input of the function
   * @returns record containing the public rpc and the serialized secret input
   */
  public secretInput(secretInput: SecretT): SecretInput {
    return { publicRpc: this.publicRpc, secretInput: this.secretSerializer(secretInput) };
  }
}
