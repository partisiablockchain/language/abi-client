/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "./BlockchainAddress";
import { BlockchainStateClient } from "./BlockchainStateClient";
import { CompactBitArray } from "@secata-public/bitmanipulation-ts";

export interface Entry<K, V> {
  key: K;
  value: V;
}

export interface StateWithClient {
  bytes: Buffer;
  client: BlockchainStateClient;
  address: BlockchainAddress;
}

export interface SecretInput {
  publicRpc: Buffer;
  secretInput: CompactBitArray;
}
