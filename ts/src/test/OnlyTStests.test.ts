/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AbstractBuilder,
  Configuration,
  FunctionFormat,
  hashBuffer,
  NamedTypesFormat,
  NamedTypeSpec,
  RpcValue,
  RustSyntaxPrettyPrinter,
  ScValue,
  ScValueAvlTreeMap,
  ShortnameType,
} from "../main";

test("weird coverage for index", () => {
  expect(hashBuffer.name).toEqual("hashBuffer");
  expect(NamedTypeSpec.name).toEqual("NamedTypeSpec");
  expect(NamedTypesFormat.OnlyStructs).toEqual(1);
  expect(ShortnameType.hash).toEqual(1);
  expect(FunctionFormat.FnKind).toEqual(1);
  expect(Configuration.name).toEqual("Configuration");
  expect(ScValue.name).toEqual("ScValue");
  expect(RpcValue.name).toEqual("RpcValue");
  expect(RustSyntaxPrettyPrinter.name).toEqual("RustSyntaxPrettyPrinter");
  expect(AbstractBuilder.name).toEqual("AbstractBuilder");
  expect(ScValueAvlTreeMap.name).toEqual("ScValueAvlTreeMap");
});
