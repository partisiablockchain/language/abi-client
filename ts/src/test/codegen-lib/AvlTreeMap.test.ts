/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AvlTreeMap, BlockchainAddress, BlockchainStateClient, Entry } from "../../main";

const testBlockChainClient: BlockchainStateClient = {
  getContractStateAvlNextN(
    _address: BlockchainAddress,
    _treeId: number,
    key: Buffer | undefined,
    n: number
  ): Promise<Array<Entry<Buffer, Buffer>>> {
    if (key === undefined) {
      return Promise.resolve(Array(n).fill({ key: Buffer.alloc(1), value: Buffer.alloc(1) }));
    }
    return Promise.resolve(Array(n).fill({ key, value: key }));
  },
  getContractStateAvlValue(
    _address: BlockchainAddress,
    _treeId: number,
    key: Buffer
  ): Promise<Buffer | undefined> {
    if (key[0] === 43) {
      return Promise.resolve(undefined);
    }
    return Promise.resolve(key);
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getContractStateBinary(_address: BlockchainAddress): Promise<Buffer> {
    return Promise.resolve(Buffer.from([]));
  },

  getContractStateAvlSize(_address: BlockchainAddress, _treeId: number): Promise<number> {
    return Promise.resolve(3);
  },
};

const avlTreeMap: AvlTreeMap<number, boolean> = new AvlTreeMap(
  0,
  testBlockChainClient,
  BlockchainAddress.fromBuffer(Buffer.alloc(21)),
  (byt) => Buffer.from([byt]),
  (bytes) => bytes[0],
  (bytes) => bytes[0] != 0
);

test("treeId", () => {
  expect(avlTreeMap.treeId).toEqual(0);
  const otherTree = new AvlTreeMap(42);
  expect(otherTree.treeId).toEqual(42);
});

test("getValue", async () => {
  expect(await avlTreeMap.get(0)).toEqual(false);
  expect(await avlTreeMap.get(1)).toEqual(true);
  expect(await avlTreeMap.get(43)).toEqual(undefined);
});

test("getNextN", async () => {
  expect(await avlTreeMap.getNextN(0, 3)).toEqual([
    { key: 0, value: false },
    { key: 0, value: false },
    { key: 0, value: false },
  ]);
  expect(await avlTreeMap.getNextN(1, 2)).toEqual([
    { key: 1, value: true },
    { key: 1, value: true },
  ]);
  expect(await avlTreeMap.getNextN(undefined, 1)).toEqual([{ key: 0, value: false }]);
});

test("size", async () => {
  expect(await avlTreeMap.size()).toEqual(3);
});

test("avl no client", async () => {
  let avl = new AvlTreeMap<number, number>(1);
  await expect(avl.get(1)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.getNextN(1, 2)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.size()).rejects.toThrow("Tried to get avl state with no client");

  // Hack to hit stryker mutations
  avl = new AvlTreeMap<number, number>(
    1,
    testBlockChainClient,
    undefined,
    (k) => Buffer.alloc(k),
    (b) => b[0],
    (b) => b[0]
  );
  await expect(avl.get(1)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.getNextN(1, 2)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.size()).rejects.toThrow("Tried to get avl state with no client");

  avl = new AvlTreeMap<number, number>(
    1,
    testBlockChainClient,
    BlockchainAddress.fromBuffer(Buffer.alloc(21)),
    undefined as unknown as (k: number) => Buffer,
    (b) => b[0],
    (b) => b[0]
  );
  await expect(avl.get(1)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.getNextN(1, 2)).rejects.toThrow("Tried to get avl state with no client");

  avl = new AvlTreeMap<number, number>(
    1,
    testBlockChainClient,
    BlockchainAddress.fromBuffer(Buffer.alloc(21)),
    (k) => Buffer.alloc(k),
    undefined as unknown as (b: Buffer) => number,
    (b) => b[0]
  );
  await expect(avl.get(1)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.getNextN(1, 2)).rejects.toThrow("Tried to get avl state with no client");

  avl = new AvlTreeMap<number, number>(
    1,
    testBlockChainClient,
    BlockchainAddress.fromBuffer(Buffer.alloc(21)),
    (k) => Buffer.alloc(k),
    (b) => b[0],
    undefined as unknown as (b: Buffer) => number
  );
  await expect(avl.get(1)).rejects.toThrow("Tried to get avl state with no client");
  await expect(avl.getNextN(1, 2)).rejects.toThrow("Tried to get avl state with no client");
});

test("serialize key", () => {
  expect(avlTreeMap.serializeKey(3)).toEqual(Buffer.from([3]));
  expect(avlTreeMap.serializeKey(6)).toEqual(Buffer.from([6]));
});

test("deserialize key", () => {
  expect(avlTreeMap.deserializeKey(Buffer.from([3]))).toEqual(3);
  expect(avlTreeMap.deserializeKey(Buffer.from([6]))).toEqual(6);
});

test("deserialize key", () => {
  expect(avlTreeMap.deserializeValue(Buffer.from([0]))).toEqual(false);
  expect(avlTreeMap.deserializeValue(Buffer.from([1]))).toEqual(true);
});
