/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AbiBitInput,
  AbiBitOutput,
  BlockchainAddress,
  BlockchainPublicKey,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
} from "../../main";
import { CompactBitArray } from "@secata-public/bitmanipulation-ts";

test("numbers", () => {
  const bits: CompactBitArray = AbiBitOutput.serialize((out) => {
    out.writeU8(1);
    out.writeU16(2);
    out.writeU32(3);
    out.writeU64(new BN(4));
    out.writeI8(5);
    out.writeI16(6);
    out.writeI32(7);
    out.writeI64(new BN(8));
    out.writeUnsignedBigInteger(new BN(9), 16);
    out.writeSignedBigInteger(new BN(10), 16);
  });
  const input = AbiBitInput.create(bits);
  expect(input.readU8()).toEqual(1);
  expect(input.readU16()).toEqual(2);
  expect(input.readU32()).toEqual(3);
  expect(input.readU64().eqn(4)).toEqual(true);
  expect(input.readI8()).toEqual(5);
  expect(input.readI16()).toEqual(6);
  expect(input.readI32()).toEqual(7);
  expect(input.readI64().eqn(8)).toEqual(true);
  expect(input.readUnsignedBigInteger(16).eqn(9)).toEqual(true);
  expect(input.readSignedBigInteger(16).eqn(10)).toEqual(true);
});

test("booleans", () => {
  const bool = true;
  const bits = AbiBitOutput.serialize((out) => out.writeBoolean(bool));
  expect(AbiBitInput.create(bits).readBoolean()).toEqual(bool);
});

test("bits", () => {
  const bits1 = Buffer.from([1, 2, 3, 4, 5]);
  const bits = AbiBitOutput.serialize((out) => out.writeBytes(bits1));
  expect(AbiBitInput.create(bits).readBytes(5)).toEqual(bits1);
});

test("address", () => {
  const address: BlockchainAddress = BlockchainAddress.fromString(
    "010000000000000000000000000000000000000000"
  );
  const bits = AbiBitOutput.serialize((out) => out.writeAddress(address));
  expect(address.asBuffer()).toEqual(bits.data);
  expect(AbiBitInput.create(bits).readAddress()).toEqual(address);
});

test("publicKey", () => {
  const keyBytes = Buffer.from("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB", "base64");
  const publicKey: BlockchainPublicKey = BlockchainPublicKey.fromBuffer(keyBytes);
  const bits = AbiBitOutput.serialize((out) => out.writePublicKey(publicKey));
  expect(bits.data.toString("base64")).toEqual("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
  expect(AbiBitInput.create(bits).readPublicKey()).toEqual(publicKey);
});

test("signature", () => {
  const signature: Signature = Signature.fromString(
    "01000000000000000000000000000000000000000000000000000000000000000" +
      "00000000000000000000000000000000000000000000000000000000000000000"
  );
  const bits = AbiBitOutput.serialize((out) => out.writeSignature(signature));
  expect(signature.asBuffer()).toEqual(bits.data);
  expect(AbiBitInput.create(bits).readSignature()).toEqual(signature);
});

test("hash", () => {
  const hash: Hash = Hash.fromString(
    "0100000000000000000000000000000000000000000000000000000000000000"
  );
  const bits = AbiBitOutput.serialize((out) => out.writeHash(hash));
  expect(hash.asBuffer()).toEqual(bits.data);
  expect(AbiBitInput.create(bits).readHash()).toEqual(hash);
});

test("blsSignature", () => {
  const blsSignature: BlsSignature = BlsSignature.fromString(
    "8bb0425041f929baef4331752b5f6c0f40a1827fa021a586" +
      "0298c47cfe44eb0a428c479034943b1ff950fa554c6aca7d"
  );
  const bits = AbiBitOutput.serialize((out) => out.writeBlsSignature(blsSignature));
  expect(blsSignature.asBuffer()).toEqual(bits.data);
  expect(AbiBitInput.create(bits).readBlsSignature()).toEqual(blsSignature);
});

test("blsPublicKey", () => {
  const blsPublicKey: BlsPublicKey = BlsPublicKey.fromString(
    "991b2d6db43fafc2c9592f7e5f73981107975d3d92b84389" +
      "1e724dbc9f05b5eee5a3b2b1fc782ede8149f30830b84444" +
      "0c7fa63dfc38bbf3712e27a180391bca4ccabf609c5967a0" +
      "592eff420b6235f3f2b323051cb099acc3969aca310f7ff4"
  );
  const bits = AbiBitOutput.serialize((out) => out.writeBlsPublicKey(blsPublicKey));
  expect(blsPublicKey.asBuffer()).toEqual(bits.data);
  expect(AbiBitInput.create(bits).readBlsPublicKey()).toEqual(blsPublicKey);
});

test("cant write string", () => {
  const string = "Hello";
  expect(() => AbiBitOutput.serialize((out) => out.writeString(string))).toThrow(
    "Unsupported write string for a bit output"
  );
});

test("cant read string", () => {
  expect(() => AbiBitInput.create({ data: Buffer.from([]), length: 0 }).readString()).toThrow(
    "Unsupported read string for a bit input"
  );
});

test("read remaining", () => {
  const input = AbiBitInput.create({ data: Buffer.from([]), length: 0 });
  expect(() => input.readRemaining()).toThrow(
    "Cannot read remaining bytes of input when reading bits."
  );
});
