/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BlockchainAddress,
  BlockchainPublicKey,
  BlsPublicKey,
  BlsSignature,
  Hash,
  Signature,
} from "../../main";

test("signature", () => {
  expect(() => Signature.fromBuffer(Buffer.alloc(0))).toThrowError(
    "Signature expects exactly 65 bytes, but found 0"
  );
  expect(() => Signature.fromBuffer(Buffer.alloc(66))).toThrowError(
    "Signature expects exactly 65 bytes, but found 66"
  );
  const val1 = Buffer.alloc(65);
  const val2 = "00".repeat(65);
  const sig = Signature.fromBuffer(val1);
  expect(sig).toEqual(Signature.fromString(val2));
  expect(sig.asBuffer()).toEqual(val1);
  expect(sig.asString()).toEqual(val2);
});

test("blockchain address", () => {
  expect(() => BlockchainAddress.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlockchainAddress expects exactly 21 bytes, but found 0"
  );
  expect(() => BlockchainAddress.fromBuffer(Buffer.alloc(22))).toThrowError(
    "BlockchainAddress expects exactly 21 bytes, but found 22"
  );
  const val1 = Buffer.alloc(21);
  const val2 = "00".repeat(21);
  const ba = BlockchainAddress.fromBuffer(val1);
  expect(ba).toEqual(BlockchainAddress.fromString(val2));
  expect(ba.asBuffer()).toEqual(val1);
  expect(ba.asString()).toEqual(val2);
});

test("hash", () => {
  expect(() => Hash.fromBuffer(Buffer.alloc(0))).toThrowError(
    "Hash expects exactly 32 bytes, but found 0"
  );
  expect(() => Hash.fromBuffer(Buffer.alloc(33))).toThrowError(
    "Hash expects exactly 32 bytes, but found 33"
  );
  const val1 = Buffer.alloc(32);
  const val2 = "00".repeat(32);
  const hash = Hash.fromBuffer(val1);
  expect(hash).toEqual(Hash.fromString(val2));
  expect(hash.asBuffer()).toEqual(val1);
  expect(hash.asString()).toEqual(val2);
});

test("bls public key", () => {
  expect(() => BlsPublicKey.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlsPublicKey expects exactly 96 bytes, but found 0"
  );
  expect(() => BlsPublicKey.fromBuffer(Buffer.alloc(97))).toThrowError(
    "BlsPublicKey expects exactly 96 bytes, but found 97"
  );
  const val1 = Buffer.alloc(96);
  const val2 = "00".repeat(96);
  const bls = BlsPublicKey.fromBuffer(val1);
  expect(bls).toEqual(BlsPublicKey.fromString(val2));
  expect(bls.asBuffer()).toEqual(val1);
  expect(bls.asString()).toEqual(val2);
});

test("bls signature", () => {
  expect(() => BlsSignature.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlsSignature expects exactly 48 bytes, but found 0"
  );
  expect(() => BlsSignature.fromBuffer(Buffer.alloc(49))).toThrowError(
    "BlsSignature expects exactly 48 bytes, but found 49"
  );
  const val1 = Buffer.alloc(48);
  const val2 = "00".repeat(48);
  const bls = BlsSignature.fromBuffer(val1);
  expect(bls).toEqual(BlsSignature.fromString(val2));
  expect(bls.asBuffer()).toEqual(val1);
  expect(bls.asString()).toEqual(val2);
});

test("blockchain public key", () => {
  expect(() => BlockchainPublicKey.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlockchainPublicKey expects exactly 33 bytes, but found 0"
  );
  expect(() => BlockchainPublicKey.fromBuffer(Buffer.alloc(34))).toThrowError(
    "BlockchainPublicKey expects exactly 33 bytes, but found 34"
  );
  const val1 = Buffer.alloc(33);
  const val2 = "00".repeat(33);
  const pk = BlockchainPublicKey.fromBuffer(val1);
  expect(pk).toEqual(BlockchainPublicKey.fromString(val2));
  expect(pk.asBuffer()).toEqual(val1);
  expect(pk.asString()).toEqual(val2);
});
