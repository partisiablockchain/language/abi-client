/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AbiByteInput,
  AbiByteOutput,
  BlockchainAddress,
  BlockchainPublicKey,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
} from "../../main";

test("numbers", () => {
  const bytes: Buffer = AbiByteOutput.serializeBigEndian((out) => {
    out.writeU8(1);
    out.writeU16(2);
    out.writeU32(3);
    out.writeU64(new BN(4));
    out.writeI8(5);
    out.writeI16(6);
    out.writeI32(7);
    out.writeI64(new BN(8));
    out.writeUnsignedBigInteger(new BN(9), 16);
    out.writeSignedBigInteger(new BN(10), 16);
  });
  const input = AbiByteInput.createBigEndian(bytes);
  expect(input.readU8()).toEqual(1);
  expect(input.readU16()).toEqual(2);
  expect(input.readU32()).toEqual(3);
  expect(input.readU64().eqn(4)).toEqual(true);
  expect(input.readI8()).toEqual(5);
  expect(input.readI16()).toEqual(6);
  expect(input.readI32()).toEqual(7);
  expect(input.readI64().eqn(8)).toEqual(true);
  expect(input.readUnsignedBigInteger(16).eqn(9)).toEqual(true);
  expect(input.readSignedBigInteger(16).eqn(10)).toEqual(true);
});

test("endianess", () => {
  const leByte = AbiByteOutput.serializeLittleEndian((out) => out.writeI32(1));
  expect(leByte.toString("hex")).toEqual("01000000");
  const beByte = AbiByteOutput.serializeBigEndian((out) => out.writeI32(1));
  expect(beByte.toString("hex")).toEqual("00000001");

  expect(AbiByteInput.createLittleEndian(leByte).readI32()).toEqual(1);
  expect(AbiByteInput.createBigEndian(leByte).readI32()).toEqual(0x01000000);
});

test("string", () => {
  const string = "Hello";
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeString(string));
  expect(AbiByteInput.createBigEndian(bytes).readString()).toEqual(string);
});

test("booleans", () => {
  const bool = true;
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeBoolean(bool));
  expect(AbiByteInput.createBigEndian(bytes).readBoolean()).toEqual(bool);
});

test("bytes", () => {
  const bytes1 = Buffer.from([1, 2, 3, 4, 5]);
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeBytes(bytes1));
  expect(AbiByteInput.createBigEndian(bytes).readBytes(5)).toEqual(bytes1);
});

test("address", () => {
  const address: BlockchainAddress = BlockchainAddress.fromString(
    "010000000000000000000000000000000000000000"
  );
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeAddress(address));
  expect(address.asBuffer()).toEqual(bytes);
  expect(AbiByteInput.createBigEndian(bytes).readAddress()).toEqual(address);
});

test("publicKey", () => {
  const keyBytes = Buffer.from("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB", "base64");
  const publicKey: BlockchainPublicKey = BlockchainPublicKey.fromBuffer(keyBytes);
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writePublicKey(publicKey));
  expect(bytes.toString("base64")).toEqual("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
  expect(AbiByteInput.createBigEndian(bytes).readPublicKey()).toEqual(publicKey);
});

test("signature", () => {
  const signature: Signature = Signature.fromString(
    "01000000000000000000000000000000000000000000000000000000000000000" +
      "00000000000000000000000000000000000000000000000000000000000000000"
  );
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeSignature(signature));
  expect(signature.asBuffer()).toEqual(bytes);
  expect(AbiByteInput.createBigEndian(bytes).readSignature()).toEqual(signature);
});

test("hash", () => {
  const hash: Hash = Hash.fromString(
    "0100000000000000000000000000000000000000000000000000000000000000"
  );
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeHash(hash));
  expect(hash.asBuffer()).toEqual(bytes);
  expect(AbiByteInput.createBigEndian(bytes).readHash()).toEqual(hash);
});

test("blsSignature", () => {
  const blsSignature: BlsSignature = BlsSignature.fromString(
    "8bb0425041f929baef4331752b5f6c0f40a1827fa021a586" +
      "0298c47cfe44eb0a428c479034943b1ff950fa554c6aca7d"
  );
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeBlsSignature(blsSignature));
  expect(blsSignature.asBuffer()).toEqual(bytes);
  expect(AbiByteInput.createBigEndian(bytes).readBlsSignature()).toEqual(blsSignature);
});

test("blsPublicKey", () => {
  const blsPublicKey: BlsPublicKey = BlsPublicKey.fromString(
    "991b2d6db43fafc2c9592f7e5f73981107975d3d92b84389" +
      "1e724dbc9f05b5eee5a3b2b1fc782ede8149f30830b84444" +
      "0c7fa63dfc38bbf3712e27a180391bca4ccabf609c5967a0" +
      "592eff420b6235f3f2b323051cb099acc3969aca310f7ff4"
  );
  const bytes = AbiByteOutput.serializeBigEndian((out) => out.writeBlsPublicKey(blsPublicKey));
  expect(blsPublicKey.asBuffer()).toEqual(bytes);
  expect(AbiByteInput.createBigEndian(bytes).readBlsPublicKey()).toEqual(blsPublicKey);
});

test("shortname", () => {
  let initial = Buffer.from([1, -1, -1]);
  let shortname = AbiByteInput.createBigEndian(initial).readShortnameString();
  expect(shortname).toEqual("01");

  initial = Buffer.from([128, -1, -1, -1, 0]);
  shortname = AbiByteInput.createBigEndian(initial).readShortnameString();
  expect(shortname).toEqual("80ffffff00");

  initial = Buffer.from([-1, -1, -1, -1, -1, 0]);
  const finalInitial = initial;
  expect(() => AbiByteInput.createBigEndian(finalInitial).readShortnameString()).toThrowError(
    "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5" +
      " bytes)"
  );
});

test("read remaining", () => {
  const input = AbiByteInput.createBigEndian(Buffer.from([1, 2, 3, 4, 5]));
  input.readBoolean();
  expect(input.readRemaining()).toEqual(Buffer.from([2, 3, 4, 5]));
});
