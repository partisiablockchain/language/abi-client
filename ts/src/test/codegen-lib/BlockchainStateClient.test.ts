/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AvlInformation,
  AvlStateEntry,
  AvlStateValue,
  ChainControllerApi,
  Contract,
  GetContractAvlInformationRequest,
  GetContractAvlNextNRequest,
  GetContractAvlValueRequest,
  GetContractRequest,
  InitOverrideFunction,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainAddress, BlockchainStateClientImpl } from "../../main";
import fetchMock from "jest-fetch-mock";

const TEST_ADDRESS = BlockchainAddress.fromString("020000000000000000000000000000000000000000");

test("create with base path", async () => {
  const client = BlockchainStateClientImpl.create("test-url");
  fetchMock.resetMocks();
  fetchMock.mockResponseOnce('{"size": 3}');
  await client.getContractStateAvlSize(TEST_ADDRESS, 1);
  expect(fetchMock.mock.calls[0]["0"]).toContain("test-url");
});

test("get serialized contract", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  expect(await client.getContractStateBinary(TEST_ADDRESS)).toEqual(Buffer.from([1, 2]));
});

test("get serialized contract no state", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  await expect(
    client.getContractStateBinary(
      BlockchainAddress.fromString("000000000000000000000000000000000000000000")
    )
  ).rejects.toThrow("Contract does not have any state");
});

test("get avl value", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  expect(await client.getContractStateAvlValue(TEST_ADDRESS, 0, Buffer.from([1]))).toEqual(
    Buffer.from([5, 6])
  );
});

test("get avl value not found", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  expect(await client.getContractStateAvlValue(TEST_ADDRESS, 0, Buffer.from([]))).toEqual(
    undefined
  );
});

test("get avl next n", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  const nextN = await client.getContractStateAvlNextN(TEST_ADDRESS, 0, Buffer.from([1]), 10);
  expect(nextN.length).toEqual(1);
  expect(nextN[0].key).toEqual(Buffer.from([3]));
  expect(nextN[0].value).toEqual(Buffer.from([4]));
});

test("get avl next n undefined key", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  const nextN = await client.getContractStateAvlNextN(TEST_ADDRESS, 0, undefined, 10);
  expect(nextN.length).toEqual(2);
  expect(nextN[0].key).toEqual(Buffer.from([1]));
  expect(nextN[0].value).toEqual(Buffer.from([2]));
  expect(nextN[1].key).toEqual(Buffer.from([3]));
  expect(nextN[1].value).toEqual(Buffer.from([4]));
});

test("get avl size", async () => {
  const client = BlockchainStateClientImpl.create(new ChainControllerStub());
  expect(await client.getContractStateAvlSize(TEST_ADDRESS, 0)).toEqual(3);
});

class ChainControllerStub extends ChainControllerApi {
  override async getContract(
    _requestParameters: GetContractRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<Contract> {
    let serializedContract;
    if (_requestParameters.address === "000000000000000000000000000000000000000000") {
      serializedContract = undefined;
    } else {
      serializedContract = Buffer.from([1, 2]).toString("base64");
    }
    return {
      abi: "",
      binder: "",
      shardId: "",
      address: "",
      storageLength: 0,
      jar: "",
      serializedContract,
    };
  }

  override async getContractAvlInformation(
    _requestParameters: GetContractAvlInformationRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<AvlInformation> {
    return { size: 3 };
  }

  override async getContractAvlValue(
    _requestParameters: GetContractAvlValueRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<AvlStateValue> {
    if (_requestParameters.key === "") {
      throw new Error("404 not found");
    }
    return { data: Buffer.from([5, 6]).toString("base64") };
  }

  override async getContractAvlNextN(
    requestParameters: GetContractAvlNextNRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<AvlStateEntry[]> {
    if (requestParameters.key === undefined) {
      return [
        { key: "01", value: Buffer.from([2]).toString("base64") },
        { key: "03", value: Buffer.from([4]).toString("base64") },
      ];
    } else {
      return [{ key: "03", value: Buffer.from([4]).toString("base64") }];
    }
  }
}
