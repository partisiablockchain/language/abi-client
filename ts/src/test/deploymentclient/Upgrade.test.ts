/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Can build the rpc of pub deploy call wth the minimum required arguments of contract bytes, abi
 * and contract initialization bytes.
 */
import { BlockchainClientForDeployment, DeploymentBuilder, DeploymentClient } from "../../main";
import {
  DeploymentTransactionClientStub,
  readDynamicBytes,
  nicknameAbi,
  nicknameWasm,
  nicknamePbc,
  averagePbc,
  defaultExecutionStatus,
  pubDeployAbi,
  pubDeployState,
  defaultContract,
} from "./DeploymentTestHelper";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { PbcFile } from "@partisiablockchain/sections";
import {
  Contract,
  SenderAuthenticationKeyPair,
  SentTransaction,
  SignedTransaction,
  Transaction,
  TransactionTree,
} from "@partisiablockchain/blockchain-api-transaction-client";

/** Can build the rpc of an upgrade transaction. */
test("build upgrade rpc", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeRpc = await deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000")
    .buildRpc();

  assertDefaultUpgradeRpc(upgradeRpc);
});

/** Can build the upgrade rpc with the bytes of a pbc file. */
test("build upgrade rpc pbc bytes", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeRpc = await deploymentClient
    .upgradeBuilder()
    .pbcFile(nicknamePbc)
    .contractAddress("010000000000000000000000000000000000000000")
    .buildRpc();

  assertDefaultUpgradeRpc(upgradeRpc);
});

/** Can build the upgrade rpc with a pbc file. */
test("build upgrade rpc pbc file", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeRpc = await deploymentClient
    .upgradeBuilder()
    .pbcFile(PbcFile.fromBytes(nicknamePbc))
    .contractAddress("010000000000000000000000000000000000000000")
    .buildRpc();

  assertDefaultUpgradeRpc(upgradeRpc);
});

/** Can set the rpc received by the contract when upgrading. */
test("build upgrade rpc with contract rpc", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeRpc = await deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000001")
    .upgradeRpc(Buffer.from("010203", "hex"))
    .buildRpc();

  const stream = new BigEndianByteInput(upgradeRpc);
  expect(stream.readU8()).toEqual(5); // shortname
  expect(stream.readBytes(21).toString("hex")).toEqual(
    "010000000000000000000000000000000000000001"
  );
  expect(stream.readI32()).toEqual(1); // binder id
  expect(readDynamicBytes(stream).equals(nicknameWasm)).toBe(true);
  expect(readDynamicBytes(stream).equals(nicknameAbi)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("010203", "hex"))).toBe(true);
});

/** Can't build upgrade without providing abi. */
test("missing abi", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  await expect(() =>
    deploymentClient
      .upgradeBuilder()
      .wasm(nicknameWasm)
      .contractAddress("010000000000000000000000000000000000000000")
      .buildRpc()
  ).rejects.toThrow("Missing abi bytes");
});

/** Can't build upgrade without providing contract bytes. */
test("missing contract bytes", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  await expect(() =>
    deploymentClient
      .upgradeBuilder()
      .abi(nicknameAbi)
      .contractAddress("010000000000000000000000000000000000000000")
      .buildRpc()
  ).rejects.toThrow("Missing contract bytes");
});

/** Can't build upgrade without providing contract address. */
test("missing contract address", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  await expect(() =>
    deploymentClient.upgradeBuilder().abi(nicknameAbi).wasm(nicknameWasm).buildRpc()
  ).rejects.toThrow("Missing contract address");
});

/** Cannot upgrade a zk contract. */
test("cannot upgrade zk", () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  expect(() => deploymentClient.upgradeBuilder().pbcFile(averagePbc)).toThrow(
    "Cannot upgrade a zk contract"
  );
});

/** Can build the signed transaction of an upgrade invocation. */
test("build upgrade transaction", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeTx = await deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000")
    .build();

  expect(upgradeTx.getInner().address).toEqual(DeploymentBuilder.DEFAULT_PUB_ADDRESS);
  expect(upgradeTx.getGasCost()).toEqual(2_000_000);
  expect(upgradeTx.identifier()).toEqual(
    "63c64ff65186ddbf627b1079c7aaed8513f4eac6d1a18d65625fa7cdba70dd08"
  );
  assertDefaultUpgradeRpc(upgradeTx.getInner().rpc);
});

/** Can set the gas cost of the upgrade transaction. */
test("build upgrade transaction with gas cost", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const upgradeTx = await deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000")
    .gasCost(400000)
    .build();

  expect(upgradeTx.getInner().address).toEqual(DeploymentBuilder.DEFAULT_PUB_ADDRESS);
  expect(upgradeTx.getGasCost()).toEqual(400000);
  assertDefaultUpgradeRpc(upgradeTx.getInner().rpc);
});

/** Can send the upgrade transaction. */
test("send upgrade transaction", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const txTree = await deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000")
    .upgrade();

  expect(txTree.transaction.identifier).toEqual(
    "63c64ff65186ddbf627b1079c7aaed8513f4eac6d1a18d65625fa7cdba70dd08"
  );
});

/** If the upgrade transaction fails, an exception is thrown with the error message. */
test("send upgrade transaction fails", async () => {
  const executionsStatus = defaultExecutionStatus(false, {
    errorMessage: "Some failure message",
    stackTrace: "",
  });
  const deploymentClient = new DeploymentClient(
    new DeploymentTransactionClientStub(executionsStatus)
  );
  await expect(() =>
    deploymentClient
      .upgradeBuilder()
      .abi(nicknameAbi)
      .wasm(nicknameWasm)
      .contractAddress("010000000000000000000000000000000000000000")
      .upgrade()
  ).rejects.toThrow(
    "Transaction '63c64ff65186ddbf627b1079c7aaed8513f4eac6d1a18d65625fa7cdba70dd08' failed" +
      " with cause: Some failure message"
  );
});

/** If the client cannot get the deploy contract's state, an exception is thrown. */
test("unable to get deploy state", async () => {
  const deploymentClient = new DeploymentClient({
    async getContract(_address: string): Promise<Contract> {
      throw new Error("Error 404");
    },

    sign(_transaction: Transaction, _gasCost: number): Promise<SignedTransaction> {
      throw new Error("Should not be reached");
    },
    send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Should not be reached");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Should not be reached");
    },
  });

  const deployRpc = deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000");

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Unable to get deploy contract state");
});

/** If the client can't send the upgrade transaction an exception is thrown. */
test("fails to send upgrade transaction", async () => {
  const transactionClient: BlockchainClientForDeployment = {
    getContract(_address: string): Promise<Contract> {
      return Promise.resolve(defaultContract(pubDeployAbi, pubDeployState));
    },

    sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction> {
      const aa = SenderAuthenticationKeyPair.fromString("aa");
      return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
    },
    async send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Error 400");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Shouldn't reach");
    },
  };

  const deploymentClient = new DeploymentClient(transactionClient);

  const deployRpc = deploymentClient
    .upgradeBuilder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .contractAddress("010000000000000000000000000000000000000000");

  await expect(() => deployRpc.upgrade()).rejects.toThrow("Failed to send upgrade transaction");
});

function assertDefaultUpgradeRpc(upgradeRpc: Buffer) {
  const stream = new BigEndianByteInput(upgradeRpc);
  expect(stream.readU8()).toEqual(5); // shortname
  expect(stream.readBytes(21).toString("hex")).toEqual(
    "010000000000000000000000000000000000000000"
  );
  expect(stream.readI32()).toEqual(1); // binder id
  expect(readDynamicBytes(stream).equals(nicknameWasm)).toBe(true);
  expect(readDynamicBytes(stream).equals(nicknameAbi)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from([]))).toBe(true);
}
