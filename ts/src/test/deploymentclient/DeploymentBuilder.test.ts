/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { VersionInterval } from "../../main/deploymentclient/DeploymentTypes";
import { AbiVersion } from "../../main";
import { findBinderId } from "../../main/deploymentclient/DeploymentBuilder";

const binderMap: Map<number, VersionInterval> = new Map([
  [1, { min: new AbiVersion(2, 0, 0), max: new AbiVersion(2, 0, 0) }],
  [2, { min: new AbiVersion(3, 1, 0), max: new AbiVersion(3, 4, 0) }],
]);

/**
 * Returns the binder id of a binder if its version interval includes the given binder version.
 */
test("find binder id", () => {
  const binderId = findBinderId(binderMap, new AbiVersion(2, 0, 0));
  expect(binderId).toEqual(1);
});

/** Different binder versions can give same binder id if it supports a range of versions. */
test("multiple binder versions give same id", () => {
  const binderId1 = findBinderId(binderMap, new AbiVersion(3, 1, 0));
  const binderId2 = findBinderId(binderMap, new AbiVersion(3, 3, 0));
  expect(binderId1).toEqual(2);
  expect(binderId2).toEqual(2);
});

/** If the binder version is not supported by any binders, it throws. */
test("cannot find binder id", () => {
  expect(() => findBinderId(binderMap, new AbiVersion(1, 0, 0))).toThrow(
    "No binders exists supporting version 1.0.0"
  );
  expect(() => findBinderId(binderMap, new AbiVersion(2, 1, 0))).toThrow(
    "No binders exists supporting version 2.1.0"
  );
  expect(() => findBinderId(binderMap, new AbiVersion(3, 0, 0))).toThrow(
    "No binders exists supporting version 3.0.0"
  );
  expect(() => findBinderId(binderMap, new AbiVersion(4, 0, 0))).toThrow(
    "No binders exists supporting version 4.0.0"
  );
});
