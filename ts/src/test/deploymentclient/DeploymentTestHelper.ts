/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { BlockchainClientForDeployment } from "../../main";
import {
  Contract,
  ExecutedTransaction,
  ExecutionStatus,
  Failure,
  SenderAuthenticationKeyPair,
  SentTransaction,
  SignedTransaction,
  Transaction,
  TransactionTree,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { readFileSync } from "fs";

export const nicknameAbi = loadBytes("nickname.abi");
/** Using these wasm bytes as real wasm bytes is really slow. */
export const nicknameWasm = Buffer.from("Some pub wasm bytes");
export const nicknamePbc = createNicknamePbcFile();
export const tokenAbi = loadBytes("token_v2.abi");
export const averageAbi = loadBytes("average_salary.abi");
/** Using these wasm bytes as real wasm bytes is really slow. */
export const averageZkwa = createAverageZkwa();
export const averagePbc = createAveragePbcFile();

export const pubDeployAbi = loadBytes("pub_deploy.abi");
export const pubDeployState = loadBase64Bytes("pub_deploy_state.bin");

export const realDeployAbi = loadBytes("real_deploy.abi");
export const realDeployState = loadBase64Bytes("real_deploy_state.bin");

export const upgradableAbi = loadBytes("upgradable_v2.abi");
export const upgradableWasm = loadBytes("upgradable_v2.wasm");

export function readDynamicBytes(stream: BigEndianByteInput): Buffer {
  const length = stream.readI32();
  return stream.readBytes(length);
}

export class DeploymentTransactionClientStub implements BlockchainClientForDeployment {
  private readonly executionStatus: ExecutionStatus;
  private readonly events: ExecutedTransaction[];

  constructor(executionStatus?: ExecutionStatus, events?: ExecutedTransaction[]) {
    this.executionStatus = executionStatus ?? defaultExecutionStatus(true);
    this.events = events ?? [];
  }

  getContract(address: string): Promise<Contract> {
    let abiBytes;
    let stateBytes;
    if (address === "0197a0e238e924025bad144aa0c4913e46308f9a4d") {
      abiBytes = pubDeployAbi;
      stateBytes = pubDeployState;
    } else {
      abiBytes = realDeployAbi;
      stateBytes = realDeployState;
    }
    return Promise.resolve(defaultContract(abiBytes, stateBytes));
  }

  sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction> {
    const aa = SenderAuthenticationKeyPair.fromString("aa");
    return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
  }
  send(signedTransaction: SignedTransaction): Promise<SentTransaction> {
    return Promise.resolve({
      signedTransaction,
      transactionPointer: { identifier: signedTransaction.identifier(), destinationShardId: "" },
    });
  }
  waitForSpawnedEvents(sentTransaction: SentTransaction): Promise<TransactionTree> {
    const executedTransaction: ExecutedTransaction = {
      identifier: sentTransaction.transactionPointer.identifier,
      isEvent: false,
      content: "",
      executionStatus: this.executionStatus,
    };
    return Promise.resolve(new TransactionTree(executedTransaction, this.events));
  }
}

function loadBytes(fileName: string): Buffer {
  return readFileSync("src/test-resources/deploymentclient/" + fileName);
}

function loadBase64Bytes(fileName: string): Buffer {
  return Buffer.from(
    readFileSync("src/test-resources/deploymentclient/" + fileName).toString("utf-8"),
    "base64"
  );
}

export function defaultContract(abiBytes: Buffer, stateBytes: Buffer) {
  return {
    abi: abiBytes.toString("base64"),
    serializedContract: stateBytes.toString("base64"),
    address: "",
    jar: "",
    shardId: "",
    binder: "",
    storageLength: 0,
  };
}

/**
 * Create a default execution status for tests.
 */
export function defaultExecutionStatus(success: boolean, failure?: Failure): ExecutionStatus {
  return {
    success,
    finalized: true,
    events: [],
    transactionCost: {
      allocatedForEvents: 0,
      cpu: 0,
      networkFees: {},
      paidByContract: 0,
      remaining: 0,
    },
    blockId: "0",
    failure,
  };
}

function createNicknamePbcFile() {
  return BigEndianByteOutput.serialize((out) => {
    out.writeBytes(Buffer.from("50425343", "hex")); // Header
    out.writeI8(1); // Abi identifier
    out.writeI32(nicknameAbi.length); // Abi length
    out.writeBytes(nicknameAbi);
    out.writeI8(2); // Wasm identifier
    out.writeI32(nicknameWasm.length); // Wasm length
    out.writeBytes(nicknameWasm);
  });
}

function createAverageZkwa() {
  return BigEndianByteOutput.serialize((out) => {
    const averageWasm = Buffer.from("some real wasm bytes");
    out.writeI8(2); // Wasm identifier
    out.writeI32(averageWasm.length); // Wasm length
    out.writeBytes(averageWasm);
    const averageZkbc = Buffer.from("some zkbc bytes");
    out.writeI8(3); // Zkbc identifier
    out.writeI32(averageZkbc.length); // Wasm length
    out.writeBytes(averageZkbc);
  });
}

function createAveragePbcFile() {
  return BigEndianByteOutput.serialize((out) => {
    out.writeBytes(Buffer.from("50425343", "hex")); // Header
    out.writeI8(1); // Abi identifier
    out.writeI32(averageAbi.length); // Abi length
    out.writeBytes(averageAbi);
    out.writeBytes(createAverageZkwa());
  });
}
