/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { DeploymentClient, BlockchainClientForDeployment, DeploymentBuilder } from "../../main";
import {
  Contract,
  SenderAuthenticationKeyPair,
  SentTransaction,
  SignedTransaction,
  Transaction,
  TransactionTree,
  ExecutedTransaction,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { PbcFile } from "@partisiablockchain/sections";
import fetchMock from "jest-fetch-mock";
import BN from "bn.js";
import {
  averageAbi,
  averagePbc,
  averageZkwa,
  defaultContract,
  defaultExecutionStatus,
  DeploymentTransactionClientStub,
  nicknameAbi,
  nicknamePbc,
  nicknameWasm,
  pubDeployAbi,
  pubDeployState,
  readDynamicBytes,
  tokenAbi,
} from "./DeploymentTestHelper";

/**
 * Can build the rpc of pub deploy call wth the minimum required arguments of contract bytes, abi
 * and contract initialization bytes.
 */
test("build rpc public", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .buildRpc();

  assertDefaultPubDeployRpc(deployRpc);
});

/**
 * Can build the rpc of real deploy call wth the minimum required arguments of contract bytes, abi
 * and contract initialization bytes.
 */
test("build rpc real", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(averageAbi)
    .zkwa(averageZkwa)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .buildRpc();

  assertDefaultRealDeployRpc(deployRpc);
});

/** Can build the deploy call with a pbc file instead of abi and wasm file. */
test("build pbc file pub", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .pbcFile(nicknamePbc)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .buildRpc();

  assertDefaultPubDeployRpc(deployRpc);
});

/** Can build the deploy call with a pbc file instead of abi and zkwa file. */
test("build pbc file real", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .pbcFile(PbcFile.fromBytes(averagePbc))
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .buildRpc();

  assertDefaultRealDeployRpc(deployRpc);
});

/** Can build the initialization bytes using the rpc builder pattern and the abi. */
test("build init with builder", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(tokenAbi)
    .wasm(nicknameWasm)
    .initRpc((builder) => {
      builder.addString("Token Name");
      builder.addString("TS");
      builder.addU8(4);
      builder.addU128(new BN(100));
    })
    .buildRpc();

  const expectedInitBytes = Buffer.from(
    "ffffffff0f" + // Shortname
      "0000000a546f6b656e204e616d65" + // Token Name
      "000000025453" + // TS
      "04" + // 4
      "00000000000000000000000000000064", // 100
    "hex"
  );

  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(4); // shortname
  expect(readDynamicBytes(stream).equals(nicknameWasm)).toBe(true);
  expect(readDynamicBytes(stream).equals(tokenAbi)).toBe(true);
  expect(readDynamicBytes(stream).equals(expectedInitBytes)).toBe(true);
  expect(stream.readI32()).toEqual(1); // binder id
});

/** Can specify the required stakes for the deploy call. */
test("build required stakes", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(averageAbi)
    .zkwa(averageZkwa)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .requiredStakes(42)
    .buildRpc();

  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(2); // shortname
  expect(readDynamicBytes(stream).equals(averageZkwa)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("ffffffff0f", "hex"))).toBe(true);
  expect(readDynamicBytes(stream).equals(averageAbi)).toBe(true);
  expect(stream.readI64().toNumber()).toEqual(42); // Required stakes
  expect(stream.readI32()).toEqual(0); // Default allowed jurisdictions size
  expect(stream.readI32()).toEqual(1); // binder id
});

/** Can specify the allowed jurisdictions for the deploy call. */
test("build allowed jurisdictions", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(averageAbi)
    .zkwa(averageZkwa)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .allowedJurisdictions([[42]])
    .buildRpc();

  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(2); // shortname
  expect(readDynamicBytes(stream).equals(averageZkwa)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("ffffffff0f", "hex"))).toBe(true);
  expect(readDynamicBytes(stream).equals(averageAbi)).toBe(true);
  expect(stream.readI64().toNumber()).toEqual(2000_0000); // Required stakes
  expect(stream.readI32()).toEqual(1); // Allowed jurisdictions size
  expect(stream.readI32()).toEqual(1); // Inner list size
  expect(stream.readI32()).toEqual(42); // Allowed jurisdiction
  expect(stream.readI32()).toEqual(1); // binder id
});

/** Can override the binder id used. */
test("build rpc binder id", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .binderId(99)
    .buildRpc();

  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(4); // shortname
  expect(readDynamicBytes(stream).equals(nicknameWasm)).toBe(true);
  expect(readDynamicBytes(stream).equals(nicknameAbi)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("ffffffff0f", "hex"))).toBe(true);
  expect(stream.readI32()).toEqual(99); // binder id
});

/** Can't build without an abi file. */
test("missing abi", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = deploymentClient
    .builder()
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Missing abi bytes");
});

/** Can't build without contract bytes. */
test("missing contract bytes", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Missing contract bytes");
});

/** Can't build without initialization bytes. */
test("missing init rpc", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = deploymentClient.builder().abi(nicknameAbi).wasm(nicknameWasm);

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Missing initialization bytes");
});

/** Trying to add required stakes to a public contract throws an exception. */
test("illegal required stakes", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .requiredStakes(42);

  await expect(() => deployRpc.buildRpc()).rejects.toThrow(
    "Required stakes is not allowed as argument to pub deploy"
  );
});

/** Trying to add allowed jurisdictions to a public contract throws an exception. */
test("illegal allowed jurisdictions", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .allowedJurisdictions([[42]]);

  await expect(() => deployRpc.buildRpc()).rejects.toThrow(
    "Allowed jurisdictions is not allowed as argument to pub deploy"
  );
});

/** Can build the signed deploy transaction with default gas for public deploy. */
test("build transaction pub", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .build();

  expect(deployRpc.getInner().address).toEqual("0197a0e238e924025bad144aa0c4913e46308f9a4d"); // default pub address
  expect(deployRpc.getGasCost()).toEqual(800_000); // Default pub gas cost

  assertDefaultPubDeployRpc(deployRpc.getInner().rpc);
});

/** Can build the signed deploy transaction with default gas for real deploy. */
test("build transaction real", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(averageAbi)
    .zkwa(averageZkwa)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .build();

  expect(deployRpc.getInner().address).toEqual("018bc1ccbb672b87710327713c97d43204905082cb"); // default real address
  expect(deployRpc.getGasCost()).toEqual(2_000_000); // Default real gas cost

  assertDefaultRealDeployRpc(deployRpc.getInner().rpc);
});

/** Can set the gas cost of the deploy transaction. */
test("build transaction with gas cost", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deployRpc = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .gasCost(400)
    .build();

  expect(deployRpc.getInner().address).toEqual("0197a0e238e924025bad144aa0c4913e46308f9a4d"); // default pub address
  expect(deployRpc.getGasCost()).toEqual(400); // Default pub gas cost

  assertDefaultPubDeployRpc(deployRpc.getInner().rpc);
});

/** Can override the deploy address of the deploy contract. */
test("build transaction with deploy address", async () => {
  const myDeployAddress = "000000000000000000000000000000000000000000";
  const deploymentClient = new DeploymentClient({
    getContract(address: string): Promise<Contract> {
      expect(address).toEqual(myDeployAddress);
      const abiBytes = pubDeployAbi;
      const stateBytes = pubDeployState;
      return Promise.resolve(defaultContract(abiBytes, stateBytes));
    },

    sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction> {
      const aa = SenderAuthenticationKeyPair.fromString("aa");
      return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
    },
    send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Should not be reached");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Should not be reached");
    },
  });

  const deployRpc = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .deployContractAddress(myDeployAddress)
    .build();

  expect(deployRpc.getInner().address).toEqual(myDeployAddress);
  expect(deployRpc.getGasCost()).toEqual(800_000); // Default pub gas cost

  assertDefaultPubDeployRpc(deployRpc.getInner().rpc);
});

/** Can deploy a public contract. */
test("deploy pub", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deploy = await deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .deploy();

  expect(deploy.contractAddress).toEqual("02335431c91b5d934f227203f0d14472dcb5b7b10c");
  expect(deploy.executedTransactions.transaction.identifier).toEqual(
    "746934bfc0d82f936fbe26c6335431c91b5d934f227203f0d14472dcb5b7b10c"
  );
});

/** Can deploy a zk contract. */
test("deploy real", async () => {
  const deploymentClient = new DeploymentClient(new DeploymentTransactionClientStub());
  const deploy = await deploymentClient
    .builder()
    .abi(averageAbi)
    .zkwa(averageZkwa)
    .initRpc(Buffer.from("ffffffff0f", "hex"))
    .deploy();

  expect(deploy.contractAddress).toEqual("033b42345befd96bb9d19ec3415b5709a503780c21");
  expect(deploy.executedTransactions.transaction.identifier).toEqual(
    "da3e60202007970ad9e3b0223b42345befd96bb9d19ec3415b5709a503780c21"
  );
});

/** If the client cannot get the deploy contract's state, an exception is thrown. */
test("unable to get deploy state", async () => {
  const deploymentClient = new DeploymentClient({
    async getContract(_address: string): Promise<Contract> {
      throw new Error("Error 404");
    },

    sign(_transaction: Transaction, _gasCost: number): Promise<SignedTransaction> {
      throw new Error("Should not be reached");
    },
    send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Should not be reached");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Should not be reached");
    },
  });

  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Unable to get deploy contract state");
});

/**
 * If the contract contains a binder version not present in the deploy contract's state, an error
 * is thrown.
 */
test("unknown binder version", async () => {
  const deploymentClient = new DeploymentClient({
    getContract(_address: string): Promise<Contract> {
      const abiBytes = pubDeployAbi;
      const noBindersState = Buffer.from(
        "0100000000000000001014c5f00d7c6d70c3d0919fd7f81c7b9bfe16063620",
        "hex"
      );
      return Promise.resolve(defaultContract(abiBytes, noBindersState));
    },

    sign(_transaction: Transaction, _gasCost: number): Promise<SignedTransaction> {
      throw new Error("Should not be reached");
    },
    send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Should not be reached");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Should not be reached");
    },
  });

  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));
  await expect(() => deployRpc.buildRpc()).rejects.toThrow(
    "No binders exists supporting version 10.3.0"
  );
});

/** If the deployment transaction fails an exceptions is thrown. */
test("deploy failed", async () => {
  const executionStatus = defaultExecutionStatus(false, {
    errorMessage: "Some failure message",
    stackTrace: "",
  });
  const deploymentClient = new DeploymentClient(
    new DeploymentTransactionClientStub(executionStatus)
  );

  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.deploy()).rejects.toThrow(
    "Deployment '746934bfc0d82f936fbe26c6335431c91b5d934f227203f0d14472dcb5b7b10c' failed" +
      " with cause: Some failure message"
  );
});

/** If the client can't send the deployment transaction an exception is thrown. */
test("send deployment transaction fails", async () => {
  const transactionClient: BlockchainClientForDeployment = {
    getContract(_address: string): Promise<Contract> {
      const abiBytes = pubDeployAbi;
      const stateBytes = pubDeployState;
      return Promise.resolve(defaultContract(abiBytes, stateBytes));
    },

    sign(transaction: Transaction, gasCost: number): Promise<SignedTransaction> {
      const aa = SenderAuthenticationKeyPair.fromString("aa");
      return SignedTransaction.create(aa, 1, 2, gasCost, "chain id", transaction);
    },
    async send(_signedTransaction: SignedTransaction): Promise<SentTransaction> {
      throw new Error("Error 400");
    },
    waitForSpawnedEvents(_sentTransaction: SentTransaction): Promise<TransactionTree> {
      throw new Error("Shouldn't reach");
    },
  };

  const deploymentClient = new DeploymentClient(transactionClient);

  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.deploy()).rejects.toThrow("Failed to send deployment transaction");
});

/** If a spawned event from the deployment transaction fails an exceptions is thrown. */
test("deploy event failed", async () => {
  const executionStatus = defaultExecutionStatus(true);
  const succeedingEvent: ExecutedTransaction = {
    content: "",
    identifier: "",
    isEvent: true,
    executionStatus: defaultExecutionStatus(true),
  };
  const failingEvent: ExecutedTransaction = {
    content: "",
    identifier: "",
    isEvent: true,
    executionStatus: defaultExecutionStatus(false, {
      errorMessage: "Some event failure message",
      stackTrace: "",
    }),
  };
  const deploymentClient = new DeploymentClient(
    new DeploymentTransactionClientStub(executionStatus, [succeedingEvent, failingEvent])
  );

  const deployRpc = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.deploy()).rejects.toThrow(
    "Deployment '746934bfc0d82f936fbe26c6335431c91b5d934f227203f0d14472dcb5b7b10c' failed" +
      " with cause: Some event failure message"
  );
});

/**
 * Can create a deployment client using a base url of a reader node and sender authentication. As
 * there is no reader node at the url it fails to get the deploy state.
 */
test("create with base url", async () => {
  fetchMock.enableMocks();
  const deploymentClient = DeploymentClient.createWithUrl(
    "http://localhost:9432",
    SenderAuthenticationKeyPair.fromString("aa")
  );
  const deployRpc: DeploymentBuilder = deploymentClient
    .builder()
    .abi(nicknameAbi)
    .wasm(nicknameWasm)
    .initRpc(Buffer.from("ffffffff0f", "hex"));

  await expect(() => deployRpc.buildRpc()).rejects.toThrow("Unable to get deploy contract state");
  expect(fetchMock.mock.calls[0][0]).toEqual(
    "http://localhost:9432/chain/contracts/0197a0e238e924025bad144aa0c4913e46308f9a4d"
  );
});

/** Weird index coverage. */
test("weird index coverage test", () => {
  expect(DeploymentBuilder.name).toEqual("DeploymentBuilder");
});

function assertDefaultPubDeployRpc(deployRpc: Buffer) {
  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(4); // shortname
  expect(readDynamicBytes(stream).equals(nicknameWasm)).toBe(true);
  expect(readDynamicBytes(stream).equals(nicknameAbi)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("ffffffff0f", "hex"))).toBe(true);
  expect(stream.readI32()).toEqual(1); // binder id
}

function assertDefaultRealDeployRpc(deployRpc: Buffer) {
  const stream = new BigEndianByteInput(deployRpc);
  expect(stream.readU8()).toEqual(2); // shortname
  expect(readDynamicBytes(stream).equals(averageZkwa)).toBe(true);
  expect(readDynamicBytes(stream).equals(Buffer.from("ffffffff0f", "hex"))).toBe(true);
  expect(readDynamicBytes(stream).equals(averageAbi)).toBe(true);
  expect(stream.readI64().toNumber()).toEqual(2000_0000); // Default required stakes
  expect(stream.readI32()).toEqual(0); // Default allowed jurisdictions size
  expect(stream.readI32()).toEqual(1); // binder id
}
