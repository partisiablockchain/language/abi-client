/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainClientForDeploymentImpl } from "../../main";
import {
  BlockchainAddress,
  BlockchainClientForTransaction,
  BlockchainTransactionClient,
  ChainControllerApi,
  Contract,
  ExecutedTransaction,
  GetContractRequest,
  Hash,
  InitOverrideFunction,
  SenderAuthenticationKeyPair,
  SentTransaction,
  SerializedTransaction,
  SignedTransaction,
  TransactionPointer,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { defaultExecutionStatus } from "./DeploymentTestHelper";

/** The deployment state client impl calls the wrapped chain controller's get contract method. */
test("get contract", async () => {
  const chainControllerStub = new ChainControllerStub();
  const deploymentStateClientImpl = new BlockchainClientForDeploymentImpl(
    BlockchainTransactionClient.createForCustomApi(
      new ClientForTransaction(),
      SenderAuthenticationKeyPair.fromString("aa")
    ),
    chainControllerStub
  );
  const contract = await deploymentStateClientImpl.getContract(
    "000000000000000000000000000000000000000000"
  );
  expect(contract.abi).toEqual("aa");
  expect(chainControllerStub.getContractCalledWithAddress).toEqual(
    "000000000000000000000000000000000000000000"
  );
});

/**
 * The deployment transaction client impl sign method calls the underlying transaction client's
 * sign method.
 */
test("sign", async () => {
  const chainControllerStub = new ChainControllerStub();
  const transactionClient = BlockchainTransactionClient.createForCustomApi(
    new ClientForTransaction(),
    SenderAuthenticationKeyPair.fromString("aa")
  );
  const deploymentStateClientImpl = new BlockchainClientForDeploymentImpl(
    transactionClient,
    chainControllerStub
  );

  const transaction = {
    address: "000000000000000000000000000000000000000000",
    rpc: Buffer.from([]),
  };
  const signedTransaction = await deploymentStateClientImpl.sign(transaction, 3);
  expect(signedTransaction.getGasCost()).toEqual(3);
  expect(signedTransaction.getInner()).toEqual(transaction);
  expect(signedTransaction.getNonce()).toEqual(1);
});

/**
 * The deployment transaction client impl send method calls the underlying transaction client's
 * send method.
 */
test("send", async () => {
  const chainControllerStub = new ChainControllerStub();
  const senderAuthentication = SenderAuthenticationKeyPair.fromString("aa");
  const clientForTransaction = new ClientForTransaction();
  const transactionClient = BlockchainTransactionClient.createForCustomApi(
    clientForTransaction,
    senderAuthentication
  );
  const deploymentStateClientImpl = new BlockchainClientForDeploymentImpl(
    transactionClient,
    chainControllerStub
  );

  const transaction = {
    address: "000000000000000000000000000000000000000000",
    rpc: Buffer.from([]),
  };
  const signedTransaction = await SignedTransaction.create(
    senderAuthentication,
    1,
    2,
    3,
    "4",
    transaction
  );
  const sentTransaction = await deploymentStateClientImpl.send(signedTransaction);
  expect(sentTransaction.signedTransaction).toEqual(signedTransaction);
  expect(clientForTransaction.sentTransaction?.payload).toEqual(
    signedTransaction.serialize().toString("base64")
  );
});

/**
 * The deployment transaction client impl wait method calls the underlying transaction client's
 * wait method.
 */
test("wait for transaction", async () => {
  const chainControllerStub = new ChainControllerStub();
  const senderAuthentication = SenderAuthenticationKeyPair.fromString("aa");
  const clientForTransaction = new ClientForTransaction();
  const transactionClient = BlockchainTransactionClient.createForCustomApi(
    clientForTransaction,
    senderAuthentication
  );
  const deploymentStateClientImpl = new BlockchainClientForDeploymentImpl(
    transactionClient,
    chainControllerStub
  );

  const transaction = {
    address: "000000000000000000000000000000000000000000",
    rpc: Buffer.from([]),
  };
  const signedTransaction = await SignedTransaction.create(
    senderAuthentication,
    1,
    2,
    3,
    "4",
    transaction
  );
  const sentTransaction: SentTransaction = {
    signedTransaction,
    transactionPointer: { identifier: signedTransaction.identifier(), destinationShardId: "" },
  };
  const transactionTree = await deploymentStateClientImpl.waitForSpawnedEvents(sentTransaction);
  expect(transactionTree.transaction.identifier).toEqual(signedTransaction.identifier());
  expect(transactionTree.events).toEqual([]);
});

class ChainControllerStub extends ChainControllerApi {
  getContractCalledWithAddress: string | undefined;

  override getContract(
    requestParameters: GetContractRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<Contract> {
    this.getContractCalledWithAddress = requestParameters.address;
    return Promise.resolve({
      abi: "aa",
      binder: "",
      shardId: "",
      address: "",
      storageLength: 0,
      jar: "",
    });
  }
}

class ClientForTransaction implements BlockchainClientForTransaction {
  sentTransaction: SerializedTransaction | undefined;

  getChainId(): Promise<string> {
    return Promise.resolve("chain id");
  }
  getNonce(_blockchainAddress: BlockchainAddress): Promise<number> {
    return Promise.resolve(1);
  }
  getTransaction(shardId: string, transactionId: Hash): Promise<ExecutedTransaction> {
    return Promise.resolve({
      content: "",
      identifier: transactionId,
      isEvent: false,
      executionStatus: defaultExecutionStatus(true),
    });
  }
  getLatestProductionTime(_shardId: string): Promise<number> {
    return Promise.resolve(2);
  }
  putTransaction(serializedTransaction: SerializedTransaction): Promise<TransactionPointer> {
    this.sentTransaction = serializedTransaction;
    return Promise.resolve({ identifier: "", destinationShardId: "" });
  }
}
