/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { TransactionKind, RpcReader } from "../main";
import { TestingHelper } from "./commontests-util/TestingHelper";

test("OnSecretInputOnChain invocation.", () => {
  const contractAbi = TestingHelper.loadAbiParserFromFile("icrc.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onSecretInputOnChain-" + "f6c3b0a19a057c56919c1875256641cd2c3af1589c5669a4c42bc65dc2f6bc79.txt"
  );

  const rpcFn = RpcReader.create(invocationBytes, contractAbi, TransactionKind.Action).readRpc();
  expect(rpcFn.contractInvocation!.name).toEqual("on_secret_input");
  expect(rpcFn.arguments()[2].vecValue().values().length).toEqual(448);
});
