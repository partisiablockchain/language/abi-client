/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import * as ZkClassification from "./generated-contracts/ZkClassification";
import { AbiBitInput, AbiByteInput, BlockchainAddress } from "../../main";

/** Codegenned contracts can create secret inputs. */
test("secret input", () => {
  const secretInput = ZkClassification.addInputSample(
    { rawId: 1 },
    BlockchainAddress.fromString("000000000000000000000000000000000000000000")
  ).secretInput({ values: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] });
  expect(secretInput.publicRpc.length).toEqual(26);
  const publicRpc = AbiByteInput.createBigEndian(secretInput.publicRpc);
  expect(publicRpc.readShortnameString()).toEqual("41");
  expect(publicRpc.readI32()).toEqual(1);
  expect(publicRpc.readAddress()).toEqual(
    BlockchainAddress.fromString("000000000000000000000000000000000000000000")
  );

  expect(secretInput.secretInput.length).toEqual(160);
  const input = AbiBitInput.create(secretInput.secretInput);
  expect(input.readI16()).toEqual(1);
  expect(input.readI16()).toEqual(2);
  expect(input.readI16()).toEqual(3);
  expect(input.readI16()).toEqual(4);
  expect(input.readI16()).toEqual(5);
  expect(input.readI16()).toEqual(6);
  expect(input.readI16()).toEqual(7);
  expect(input.readI16()).toEqual(8);
  expect(input.readI16()).toEqual(9);
  expect(input.readI16()).toEqual(10);
});
