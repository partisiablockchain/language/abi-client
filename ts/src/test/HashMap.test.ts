/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  HashMap,
  ScValue,
  ScValueAddress,
  ScValueBool,
  ScValueNumber,
  ScValueVector,
  TypeIndex,
} from "../main";
import BN from "bn.js";

test("HashMap simple test", () => {
  const a = new ScValueNumber(TypeIndex.u16, 42);
  const b = new ScValueNumber(TypeIndex.u16, 13);
  const c = new ScValueBool(true);
  const d = new ScValueBool(false);

  const map = new HashMap<ScValue, ScValue>();
  map.set(a, c);
  map.set(b, d);
  expect(map.size).toEqual(2);
  expect(map.get(new ScValueNumber(TypeIndex.u16, 13))).toEqual(new ScValueBool(false));
  map.set(new ScValueNumber(TypeIndex.u16, 13), c);
  expect(map.size).toEqual(2);
  expect(map.get(new ScValueNumber(TypeIndex.u16, 13))).toEqual(new ScValueBool(true));
});

test("HashMap delete entry", () => {
  const a = new ScValueNumber(TypeIndex.u16, 42);
  const b = new ScValueNumber(TypeIndex.u16, 13);
  const c = new ScValueBool(true);
  const d = new ScValueBool(false);

  const map = new HashMap<ScValue, ScValue>();
  map.set(a, c);
  map.set(b, d);
  expect(map.size).toEqual(2);
  map.delete(new ScValueNumber(TypeIndex.u16, 42));
  expect(map.size).toEqual(1);
  expect(map.get(a)).toBeUndefined();
});

test("Hashmap create from list", () => {
  const map = new HashMap<ScValue, ScValue>([
    [new ScValueNumber(TypeIndex.u16, 42), new ScValueBool(true)],
    [new ScValueNumber(TypeIndex.u16, 13), new ScValueBool(false)],
  ]);
  expect(map.size).toEqual(2);
  expect(map.get(new ScValueNumber(TypeIndex.u16, 13))).toEqual(new ScValueBool(false));
});

test("HashMap vector key", () => {
  const a = new ScValueVector([
    new ScValueNumber(TypeIndex.u32, 47),
    new ScValueNumber(TypeIndex.u32, 13),
  ]);
  const b = new ScValueVector([
    new ScValueNumber(TypeIndex.u32, 13),
    new ScValueNumber(TypeIndex.u32, 47),
  ]);

  const map = new HashMap<ScValue, ScValue>([
    [a, new ScValueBool(true)],
    [b, new ScValueBool(false)],
  ]);

  expect(map.size).toEqual(2);
});

test("HashMap foreach", () => {
  const map = new HashMap<ScValue, ScValue>([
    [new ScValueNumber(TypeIndex.u16, 1), new ScValueBool(true)],
    [new ScValueNumber(TypeIndex.u16, 2), new ScValueBool(false)],
    [new ScValueNumber(TypeIndex.u16, 3), new ScValueBool(false)],
    [new ScValueNumber(TypeIndex.u16, 4), new ScValueBool(false)],
    [new ScValueNumber(TypeIndex.u16, 5), new ScValueBool(false)],
    [new ScValueNumber(TypeIndex.u16, 6), new ScValueBool(false)],
    [new ScValueNumber(TypeIndex.u16, 7), new ScValueBool(false)],
  ]);

  const a: Array<number | BN> = [];
  map.forEach((v, k) => a.push((k as ScValueNumber).number));
  expect(a).toEqual([1, 2, 3, 4, 5, 6, 7]);

  const b: Array<number | BN> = [];
  map.set(new ScValueNumber(TypeIndex.u16, 4), new ScValueBool(true));
  map.forEach((v, k) => b.push((k as ScValueNumber).number));
  expect(b).toEqual([1, 2, 3, 4, 5, 6, 7]);

  const c: Array<number | BN> = [];
  map.delete(new ScValueNumber(TypeIndex.u16, 4));
  map.set(new ScValueNumber(TypeIndex.u16, 4), new ScValueBool(true));
  map.forEach((v, k) => c.push((k as ScValueNumber).number));
  expect(c).toEqual([1, 2, 3, 5, 6, 7, 4]);
});

test("HashMap clear", () => {
  const map = new HashMap<ScValue, ScValue>([
    [new ScValueNumber(TypeIndex.u16, 42), new ScValueBool(true)],
    [new ScValueNumber(TypeIndex.u16, 13), new ScValueBool(false)],
  ]);
  map.clear();
  expect(map.size).toEqual(0);
});

test("HashMap has", () => {
  const map = new HashMap<ScValue, ScValue>([
    [new ScValueNumber(TypeIndex.u16, 42), new ScValueBool(true)],
    [new ScValueNumber(TypeIndex.u16, 13), new ScValueBool(false)],
  ]);
  expect(map.has(new ScValueNumber(TypeIndex.u16, 42))).toEqual(true);
  expect(map.has(new ScValueNumber(TypeIndex.u16, 41))).toEqual(false);
});

test("HashMap iterators", () => {
  const map = new HashMap<ScValue, ScValue>([
    [new ScValueNumber(TypeIndex.u16, 42), new ScValueBool(true)],
    [new ScValueNumber(TypeIndex.u16, 13), new ScValueBool(false)],
  ]);
  map.entries();
  map[Symbol.iterator]();
  expect(() => map.keys()).toThrowError("Not implemented");
  expect(() => map.values()).toThrowError("Not implemented");
});

test("HashMap address key", () => {
  const a = new ScValueAddress(Buffer.from("020101010101010101010101010101010101010101", "hex"));
  const b = new ScValueAddress(Buffer.from("030101010101010101010101010101010101010101", "hex"));
  const c = new ScValueAddress(Buffer.from("020101010101010101010101010101010101010101", "hex"));

  const map = new HashMap<ScValue, ScValue>([
    [a, new ScValueBool(true)],
    [b, new ScValueBool(false)],
    [c, new ScValueBool(false)],
  ]);
  expect(map.size).toEqual(2);
  expect(map.get(a)).toEqual(new ScValueBool(false));
});

test("HashMap many entries", () => {
  const length = 10000;
  const map = new HashMap<ScValue, ScValue>();
  for (let i = 0; i < length; i++) {
    const buffer = Buffer.alloc(21);
    buffer.writeUInt32LE(i);
    const key = new ScValueAddress(buffer);
    const value = new ScValueNumber(TypeIndex.u32, i);
    map.set(key, value);
  }
  for (let i = 0; i < length; i++) {
    const j = length - i - 1;
    const buffer = Buffer.alloc(21);
    buffer.writeUInt32LE(j);
    const key = new ScValueAddress(buffer);
    const value = new ScValueNumber(TypeIndex.u32, j);
    expect(map.get(key)).toEqual(value);
  }
});
