/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiParser, ContractAbi, RustSyntaxStatePrinter, StateReader } from "../../main";
import { readFileSync } from "fs";
import { TestingHelper } from "./TestingHelper";

export const StateReaderHelper = {
  wrap(hex: string, contractAbi?: ContractAbi) {
    const contract = contractAbi ?? null;

    return StateReader.create(Buffer.from(hex, "hex"), contract);
  },

  stringify(abiFile: string, stateFile: string): string {
    return this.stringifyBytes(abiFile, this.readStateFile(stateFile));
  },

  stringifyBytes(abiFile: string, bytes: Buffer): string {
    const abi = new AbiParser(TestingHelper.readBinaryFile(abiFile)).parseAbi();
    const reader = StateReader.create(bytes, abi.chainComponent as ContractAbi);
    const struct = reader.readStateValue(abi.chainComponent.stateType).structValue();
    return RustSyntaxStatePrinter.printState(struct);
  },

  readStateFile(fileName: string): Buffer {
    return readFileSync("src/test-resources/states/" + fileName);
  },
};
