/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  JsonRpcConverter,
  JsonValueConverter,
  RpcValue,
  ScValue,
  ScValueStruct,
  StateReader,
} from "../../main";
import { StateReaderHelper } from "./StateReaderHelper";
import { TestingHelper } from "./TestingHelper";

export const ValueHelper = {
  assertValueJson(scValue: ScValue, expected: string) {
    const actual = JsonValueConverter.toJson(scValue);
    const expectedJson = JSON.parse(expected);
    expect(actual).toEqual(expectedJson);
    const actualString = JsonValueConverter.toJsonString(scValue);
    const actualJson = JSON.parse(actualString);
    expect(actual).toEqual(actualJson);
  },

  assertRpcJson(rpcValueFn: RpcValue, expected: string) {
    const actual = JsonRpcConverter.toJson(rpcValueFn);
    const expectedJson = JSON.parse(expected);
    expect(actual).toEqual(expectedJson);
    const actualString = JsonRpcConverter.toJsonString(rpcValueFn);
    const actualJson = JSON.parse(actualString);
    expect(actual).toEqual(actualJson);
  },

  loadState(abiFile: string, stateFile: string): ScValueStruct {
    const bytes = StateReaderHelper.readStateFile(stateFile);
    const parser = TestingHelper.loadAbiParserFromFile(abiFile);
    const abi = parser.parseContractAbi();
    const reader = StateReader.create(bytes, abi);
    return reader.readStateValue(abi.stateType).structValue();
  },
};
