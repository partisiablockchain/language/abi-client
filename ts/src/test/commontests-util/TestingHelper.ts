/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteOutput, LittleEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { readdirSync, readFileSync } from "fs";
import {
  AbiParser,
  ArgumentAbi,
  AvlTreeMapTypeSpec,
  EnumVariant,
  ExecutedTransaction,
  FieldAbi,
  RpcContractBuilder,
  MapTypeSpec,
  NamedTypeRef,
  OptionTypeSpec,
  SetTypeSpec,
  SimpleTypeIndex,
  SimpleTypeSpec,
  SizedArrayTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  StateBytes,
  TransactionKind,
  BinderInvocation,
  ContractAbi,
} from "../../main";
import { ContractInvocationKind } from "../../main/model/Abi";

export const TestingHelper = {
  transactionPayloadFromExecutedTransaction(executedTransaction: ExecutedTransaction): Buffer {
    return Buffer.from(executedTransaction.transactionPayload, "base64");
  },

  readBinaryFile(fileName: string): Buffer {
    return readFileSync("src/test-resources/abi-contracts/" + fileName);
  },
  readPayloadFile(fileName: string): Buffer {
    return bytesFromHex(
      readFileSync("src/test-resources/payloads/" + fileName, { encoding: "utf-8" })
    );
  },

  loadAbiParserFromFile(inputFile: string) {
    return new AbiParser(this.readBinaryFile(inputFile));
  },

  abiParserFromBytes(bytes: Buffer): AbiParser {
    return new AbiParser(bytes);
  },

  createBuilderFromFile(filename: string, actionName: string): RpcContractBuilder {
    return new RpcContractBuilder(this.getContractAbiFromFile(filename), actionName);
  },

  getContractAbiFromFile(filename: string) {
    return this.loadAbiParserFromFile(filename).parseAbi().chainComponent as ContractAbi;
  },

  getFileAbiFromFile(filename: string) {
    return this.loadAbiParserFromFile(filename).parseAbi();
  },

  simpleTypeSpec(typeIndex: SimpleTypeIndex): SimpleTypeSpec {
    return { typeIndex };
  },

  mapTypeSpec(keyType: TypeSpec, valueType: TypeSpec): MapTypeSpec {
    return { typeIndex: TypeIndex.Map, keyType, valueType };
  },

  vecTypeSpec(valueType: TypeSpec): VecTypeSpec {
    return { typeIndex: TypeIndex.Vec, valueType };
  },

  setTypeSpec(valueType: TypeSpec): SetTypeSpec {
    return { typeIndex: TypeIndex.Set, valueType };
  },

  optionTypeSpec(valueType: TypeSpec): OptionTypeSpec {
    return { typeIndex: TypeIndex.Option, valueType };
  },

  sizedByteArrayTypeSpec(length: number): SizedArrayTypeSpec {
    return this.sizedArrayTypeSpec(this.simpleTypeSpec(TypeIndex.u8), length);
  },

  sizedArrayTypeSpec(valueType: TypeSpec, length: number): SizedArrayTypeSpec {
    return { typeIndex: TypeIndex.SizedArray, valueType, length };
  },

  namedTypeRef(index: number): NamedTypeRef {
    return { typeIndex: TypeIndex.Named, index };
  },

  enumVariant(discriminant: number, def: NamedTypeRef): EnumVariant {
    return { discriminant, def };
  },

  fieldAbi(name: string, type: TypeSpec): FieldAbi {
    return { name, type };
  },

  argumentAbi(name: string, type: TypeSpec): ArgumentAbi {
    return { name, type };
  },

  avlTreeMapTypeSpec(keyType: TypeSpec, valueType: TypeSpec): AvlTreeMapTypeSpec {
    return { typeIndex: TypeIndex.AvlTreeMap, keyType, valueType };
  },

  stateBytes(state: Buffer, avlTrees?: Buffer): StateBytes {
    return { state, avlTrees };
  },

  binderInvocation(
    kind: TransactionKind,
    name: string,
    shortname: number | null,
    argumentAbis: ArgumentAbi[],
    contractCallId: number | null
  ): BinderInvocation {
    return { kind, name, shortname, arguments: argumentAbis, contractCallKindId: contractCallId };
  },

  functionKind(kindId: number, name: string, hasShortname: boolean): ContractInvocationKind {
    return { kindId, name, hasShortname };
  },

  loadExecutedTransactions(): ExecutedTransaction[] {
    const transactionsDir = "src/test-resources/transactions/";
    const transactions: ExecutedTransaction[] = [];
    readdirSync(transactionsDir).forEach((file) => {
      const executedTransaction: ExecutedTransaction = JSON.parse(
        readFileSync(transactionsDir + file, "utf-8")
      );
      transactions.push(executedTransaction);
    });
    return transactions;
  },

  // Required for objects containing BN as fields, since they cannot directly be compared with
  // .toEqual.
  /* eslint-disable @typescript-eslint/ban-types */
  deepEquality(actual: Object, expected: Object): boolean {
    const actualEntries = Object.entries(actual);
    const expectedEntries = Object.entries(expected);

    // Equal amount of fields
    if (actualEntries.length !== expectedEntries.length) {
      return false;
    }

    for (let i = 0; i < actualEntries.length; i++) {
      // Entries consist of "key, value" and we wish to compare values thus [1]
      const actualVal = actualEntries[i][1];
      const expectedVal = expectedEntries[i][1];

      if (BN.isBN(actualVal) && BN.isBN(expectedVal)) {
        if (!actualVal.eq(expectedVal)) {
          return false;
        }
      } else {
        if (actualVal !== expectedVal) {
          return false;
        }
      }
    }
    return true;
  },
};

export function requireNonNull<T>(obj: T | undefined | null): T {
  if (obj === undefined || obj === null) {
    throw new Error("Null pointer / undefined exception");
  }
  return obj;
}

export function castNamedTypeRef(obj: TypeSpec): NamedTypeRef {
  return obj as NamedTypeRef;
}

export function bytesFromHex(hex: string): Buffer {
  return Buffer.from(hex, "hex");
}

export function bytesFromBase64(base64: string): Buffer {
  return Buffer.from(base64, "base64");
}

export function bytesFromStringBe(string: string) {
  const writer = new BigEndianByteOutput();
  writer.writeString(string);
  return writer.toBuffer();
}

export function bytesFromStringLe(string: string) {
  const writer = new LittleEndianByteOutput();
  writer.writeString(string);
  return writer.toBuffer();
}

export function concatBytes(...bytes: Buffer[]): Buffer {
  return Buffer.concat(bytes);
}

export function list<T>(...elements: T[]): T[] {
  return elements;
}

export function arraycopy(
  src: Buffer,
  srcPos: number,
  dest: Buffer,
  destPos: number,
  length: number
) {
  src.copy(dest, destPos, srcPos, length + srcPos);
}
