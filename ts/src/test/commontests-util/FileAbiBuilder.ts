/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiVersion, ContractInvocationKind, NamedTypeSpec, TypeSpec } from "../../main";
import { FnAbiBuilder } from "./FnAbiBuilder";
import { readFileSync } from "fs";
import { BinderInvocationBuilder } from "./BinderInvocationBuilder";
import { StructTypeBuilder } from "./StructTypeBuilder";
import { EnumTypeBuilder } from "./EnumTypeBuilder";

/**
 * Helper class to handle calls from converted java class, and loads cached test .abi file.
 */
export class FileAbiBuilder {
  abiName: string;

  constructor(name: string) {
    this.abiName = name;
  }

  binderVersion(_version: AbiVersion): FileAbiBuilder {
    return this;
  }

  clientVersion(_version: AbiVersion): FileAbiBuilder {
    return this;
  }

  addFunctionKind(_kind: ContractInvocationKind): FileAbiBuilder {
    return this;
  }

  addFnAbi(_builder: FnAbiBuilder): FileAbiBuilder {
    return this;
  }

  addBinderInvocation(_builder: BinderInvocationBuilder): FileAbiBuilder {
    return this;
  }

  addNamedType(_builder: NamedTypeSpec | StructTypeBuilder | EnumTypeBuilder): FileAbiBuilder {
    return this;
  }

  stateType(_type: TypeSpec): FileAbiBuilder {
    return this;
  }

  build(): Buffer {
    return readFileSync("src/test-resources/cached-references/" + this.abiName);
  }

  buildBinderAbi(): Buffer {
    return this.build();
  }
}
