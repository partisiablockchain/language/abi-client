/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ContractInvocationKind, TypeSpec } from "../../main";

/**
 * Helper class to handle calls from converted java class.
 */
export class FnAbiBuilder {
  kind(_kind: ContractInvocationKind): FnAbiBuilder {
    return this;
  }

  name(_name: string): FnAbiBuilder {
    return this;
  }

  shortname(_shortname: string | Buffer): FnAbiBuilder {
    return this;
  }

  addArgument(_name: string, _type: TypeSpec): FnAbiBuilder {
    return this;
  }

  secretArgument(_name: string, _type: TypeSpec): FnAbiBuilder {
    return this;
  }
}
