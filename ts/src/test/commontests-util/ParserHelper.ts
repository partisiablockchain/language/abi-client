/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AbiParser,
  BinderType,
  ContractAbi,
  ContractInvocation,
  ContractInvocationKind,
  ImplicitBinderAbi,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
} from "../../main";
import { NamedTypeSpec } from "../../main/";

export const ParserHelper = {
  assertFieldType(struct: NamedTypeSpec, index: number, fieldName: string, type: TypeSpec) {
    expect(struct instanceof StructTypeSpec);
    const fieldAbi = (struct as StructTypeSpec).fields[index];
    expect(fieldAbi.name).toEqual(fieldName);
    expect(fieldAbi.type).toEqual(type);
  },

  assertActionType(
    action: ContractInvocation | undefined,
    index: number,
    argName: string,
    type: TypeSpec
  ) {
    const argument = action?.arguments[index];
    expect(argument?.name).toEqual(argName);
    expect(argument?.type).toEqual(type);
  },

  assertClientVersionFail(
    binderMajor: number,
    binderMinor: number,
    clientMajor: number,
    clientMinor: number
  ) {
    const bytesOne = Buffer.from([
      0x50,
      0x42,
      0x43,
      0x41,
      0x42,
      0x49,
      binderMajor,
      binderMinor,
      0x00,
      clientMajor,
      clientMinor,
      0x00,
    ]);

    const parserOne = new AbiParser(bytesOne);
    const expectedMessage = `Unsupported Version ${clientMajor}.${clientMinor}.0 for Version Client.`;

    expect(() => parserOne.parseAbi()).toThrowError(expectedMessage);
  },

  fnAbiWithKind(kind: ContractInvocationKind): ContractInvocation {
    return new ContractInvocation(kind, "name", Buffer.from("abcdefab0f", "hex"), [
      { name: "arg", type: { typeIndex: TypeIndex.i32 } },
    ]);
  },

  contractFromKind(kind: ContractInvocationKind): ContractAbi {
    return new ContractAbi(
      BinderType.Public,
      [],
      [],
      [],
      [
        new ContractInvocation(ImplicitBinderAbi.DefaultKinds.ACTION, "name", Buffer.from([1]), []),
        new ContractInvocation(
          ImplicitBinderAbi.DefaultKinds.INIT,
          "initialize",
          Buffer.from([0]),
          []
        ),
        new ContractInvocation(kind, "secret", Buffer.from([2]), []),
      ],
      { typeIndex: TypeIndex.Named, index: 0 }
    );
  },
};
