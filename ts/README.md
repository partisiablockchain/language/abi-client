# TypeScript ABI Client

Download through [npm](https://www.npmjs.com/package/@partisiablockchain/abi-client).

## Testing

First run scripts:

```bash
bash ../scripts/generate-ts-contracts.sh
bash ../scripts/generate-ts-commontests.sh
```

Then use jest:

```bash
npm run test
```
